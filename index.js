/** @format */

import {AppRegistry} from 'react-native';
import Storage from 'react-native-storage';
import AsyncStorage from '@react-native-community/async-storage';
import 'es6-symbol/implement';
import App from './src/index';
// import App from './App';
import axios from 'axios';
// import firebase from 'firebase';
import { gettoken, temp } from './src/dependency/UtilityFunctions';

console.ignoredYellowBox = ['Warning: Each', 'Warning: Failed'];

// console.log(loggedinUserdetails());
// console.log('loggedinUserdetails');

import { Client } from 'bugsnag-react-native';
// const bugsnag = new Client("7460a60b5058d113478a8bb09469c010");

// bugsnag.notify(new Error("Test error"));
//bugsnag.notify(Error(`testing ${new Date().toString()}`))
 
// import asyncToGenerator from 'async-to-generator'

// babelHelpers.asyncToGenerator = asyncToGenerator
// let MyToken=temp();
// temp().then(values => { 
//     console.log(values);
//     console.log('MyToken>>>>');
// });
// console.log(MyToken);
// console.log('MyToken');

let storage = new Storage({
    // maximum capacity, default 1000  
    size: 10000,
  
    // Use AsyncStorage for RN, or window.localStorage for web. 
    // If not set, data would be lost after reload. 
    storageBackend: AsyncStorage,
    
    // expire time, default 1 day(1000 * 3600 * 24 milliseconds). 
    // can be null, which means never expire. 
    // defaultExpires: 1000 * 3600 * 24,
    defaultExpires: null,
    
    // cache data in the memory. default is true. 
    enableCache: true,
    
    // if data was not found in storage or expired, 
    // the corresponding sync method will be invoked and return  
    // the latest data. 
    sync : {
        // we'll talk about the details later. 
    }
  }) 

  global.storage=storage;
 


  // axios.defaults.baseURL = 'https://www.hidden-history.com/api/';
  axios.defaults.baseURL = BASEURL;
  // axios.defaults.baseURL = 'http://api2.hidden-history.com';
  // axios.defaults.baseURL = 'https://hidden.tima.com.ng/';

    // axios.defaults.headers.common['Authorization'] = temp();
  // headers: { 'content-type': 'application/x-www-form-urlencoded' },
    axios.defaults.headers.post['Content-Type'] = 'application/json';
    // axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
    
    axios.interceptors.request.use(request => {
        console.log(request);
        // Edit request config
        return request;
    }, error => {
        console.log(error);
        return Promise.reject(error);
    });
    
    axios.interceptors.response.use(response => {
        console.log(response);
        // Edit request config
        return response;
    }, error => {
        console.log(error);
        return Promise.reject(error);
    });
  
 
   
 
import {name as appName} from './app.json';
import { BASEURL, UserLoginToken } from './src/dependency/Config';

AppRegistry.registerComponent(appName, () => App);

 
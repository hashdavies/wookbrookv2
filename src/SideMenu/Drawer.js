
const Dimensions = require('Dimensions');
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  View, ScrollView,
  StatusBar, Image, StyleProvider, TouchableOpacity, Keyboard, KeyboardAvoidingView, Animated, ImageBackground
} from 'react-native';
import { NavigationActions } from 'react-navigation';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import AntDesign from 'react-native-vector-icons/AntDesign';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
// import ProfileImg from '../assets/images/profile/dp2.png';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import Octicons from 'react-native-vector-icons/Octicons';
import Feather from 'react-native-vector-icons/Feather';
import * as Animatable from 'react-native-animatable';
import {
  Button,
  Container,
  Header, Content, List, ListItem, Text, Icon, Left, Body, Right, Switch, CardItem, Thumbnail, Card
} from 'native-base';
import email from 'react-native-email';
import Communications from 'react-native-communications';
// import {Communicator} from '../dependency/UtilityFunctions';
import { inject, observer } from 'mobx-react/native'

import styles from '../StyleSheet/DrawerStyle';
import Miniuser1 from '../assets/workbrookAssets/miniUser.png';
import UserCup from '../assets/workbrookAssets/cup2.png';
import Recruiter from '../assets/workbrookAssets/recruiter.png';
import Insights from '../assets/workbrookAssets/insights.png';
import Payments from '../assets/workbrookAssets/payments.png';
import Saved from '../assets/workbrookAssets/saved.png';
import League from '../assets/workbrookAssets/league.png';
import Invite from '../assets/workbrookAssets/invite.png';
import Circle from '../assets/workbrookAssets/circle.png';
// import ThemeStore from '../../stores/Theme';
import DashboardStore from '../stores/Dashboard';
// import StyleSheetFactory from '../../../utils/theming';
import { getusertype, loggedinUserdetails, getLodgedInDetailsProfile } from '../dependency/UtilityFunctions';
import { onSignOut } from '../auth';
import { CustomAsset } from '../../utils/assets';
import { SvgUri } from 'react-native-svg';
import { AnimatedCircularProgress } from 'react-native-circular-progress';



// import DashboardStore from "../stores/Dashboard";
// import StyleSheetFactory from '../../utils/theming';
// import {inject, observer } from 'mobx-react/native'
// import ThemeStore from '../stores/Theme'

// import dateFormat from 'dateformat'
// import DropdownMenu from 'react-native-dropdown-menu'
// import BackgroundImage from '../screens/Background'
// import { onSignOut } from "";

// const mybackgroundimage = '../images/whitebg.jpg';
const window = Dimensions.get('window');

@observer
class Drawer extends Component {
  constructor(props) {
    super(props)
    this.state = {
      currentdate: "",
      taskcount: 0,
      name: '',
      usertype: '',
      mydetails: {
        // usertype: 'Edves'
      }
    }
    this.SwitchAccount = this.SwitchAccount.bind(this)
    // this._selectPhotoTapped=this._selectPhotoTapped.bind(this)

  }
  navigateToScreen = (route) => () => {
    const navigateAction = NavigationActions.navigate({
      routeName: route
    });
    this.props.navigation.dispatch(navigateAction);
  }
  SwitchAccount = () => {
    //  alert("jeu")
    this.closedrawer();
    DashboardStore._ToggleSwitchModal(true);

  }

  async componentDidMount() {

    // this.gettaskcount();
    // this.getmysettings();
    // let currentusertype = await getusertype();
    // let userdetails = await loggedinUserdetails();
    // console.log('userdetails');
    // console.log(userdetails);
    // console.log(currentusertype)
    // this.setState({
    //   usertype: currentusertype,
    //   mydetails: userdetails
    // })
    DashboardStore.__GetWbLeague_List();


    let currentusertype = await getusertype();

    let InnerProfile = await getLodgedInDetailsProfile();
    let isAvailable = InnerProfile.isAvailable;
    if (isAvailable === true) {

      let this_userprofile = InnerProfile.storeData;
      console.log(this_userprofile)
      console.log("InnerProfile")
      this.setState({
        mydetails: this_userprofile,
        usertype: currentusertype,

      })

      DashboardStore._UpdateLoggedInUserProfile(this_userprofile);
    }

  }


  closedrawer = () => {
    this.props.navigation.toggleDrawer()

  };
  // goto_settingspage = () => {
  //   // this.props.navigation.navigate("Settings");
  //   this.navigateToScreen('Settings')
  // };
  logout = () => {
    onSignOut().then(() => this.props.navigation.navigate("SignedOut"))
  };
  // goto_categorypage = () => {
  //   this.props.navigation.navigate("Projects");
  // };

  // goto_homepage = () => {
  //   this.props.navigation.navigate("Home");
  // };
  // goto_passedTaskPage = () => {
  //   this.props.navigation.navigate("PassedTask");
  // };
  // goto_upcomingTaskPage = () => {
  //   this.props.navigation.navigate("upcoming");
  // };
  render() {
    const { usertype, mydetails } = this.state;
    const { WbLeagueList,LoggedInUserProfile } = DashboardStore;
console.warn(mydetails);
let profileProgress= parseInt(LoggedInUserProfile.profileCompletionLevelPercent);
    let Fullname = LoggedInUserProfile.first_name == null || LoggedInUserProfile.last_name == null ? LoggedInUserProfile.username : `${LoggedInUserProfile.first_name}  ${LoggedInUserProfile.last_name} `

    let MyShortBrokkers = WbLeagueList.GeneralList.slice(0, 4).map((item, index) => {
      let Part_name = item.first_name == null || item.last_name == null ? item.username : `${item.first_name}  ${item.last_name} `

      return (
        <ListItem avatar style={styles.ListItemWrap} key={index}>
          <Left style={styles.ListLeftSty}>
            <Text style={[styles.NameStyInd2, { color: '#1E93E2', marginRight: 10 }]}>{index += 1}.</Text>
            <Thumbnail
              source={{ uri: item.image_url }}
              style={styles.listImagesty1}
            />
          </Left>
          <Body style={styles.ListBodySty}>
            <Text style={[styles.NameStyInd2, { color: '#2F2F2F' }]}>{Part_name}</Text>
          </Body>
          <Right style={styles.ListRightSty}>
            <Text style={[styles.NameStyInd2, { color: '#1E93E2', }]}>{item.totalPoint} pts</Text>
          </Right>
        </ListItem>
      )


    });

    return (

      <ImageBackground
        //  source={require(mybackgroundimage)}

        style={styles.container}
      >

        <View style={{ flex: 1, backgroundColor: '#F5F5F5', }}>
          {/* <View style={styles.CommentContentWrap}>*/}
          <View style={styles.SidebarView}>
            <CardItem style={styles.SidebarimageWrap}>
              <Left>
                {/* <Thumbnail source={{ uri: mydetails.image_url }} /> */}
                <AnimatedCircularProgress
                  size={52}
                  width={2}
                  fill={profileProgress}
                  tintColor="#5ABC7A"
                  backgroundColor="#F5F5F5">
                  {
                    (fill) => (
                      <Image
                        source={{ uri: LoggedInUserProfile.image_url }}
                        style={{ height: 45, width: 45, borderRadius: 60 / 2 }}
                      />
                    )
                  }
                </AnimatedCircularProgress>
                <Body>
                  <Text style={styles.NameStyInd1}>{Fullname}</Text>
                  <Text style={styles.NameStyInd2}>Profile completed {profileProgress}%</Text>
                  <Button style={styles.ViewPfSty}
                    onPress={() => { this.props.navigation.navigate("profile") }}

                  >
                    <Text style={[styles.NameStyInd2, { color: '#1E93E2', textAlign: 'left', paddingLeft: 0, }]}>
                      View Profile
                      </Text>
                  </Button>
                </Body>
              </Left>
            </CardItem>
          </View>

          <ScrollView>
            <View style={{ paddingTop: 25, backgroundColor: '#fff', }}>
              {/* =======================Cup Indicator=================== */}
              <View style={styles.CupViewSty}>
                <SvgUri
                  width='44px'
                  height='40px'
                  uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1590509060/Mobile_Assets/cup_litl8j.svg"
                ></SvgUri>
                {/* <Image source={UserCup} style={styles.cupSize} /> */}
                <Text style={styles.cupText1Sz1}>WorkBrook League</Text>
                {
                  mydetails.location !== null ?
                  <Text style={styles.cupText1Sz2}>Top 3 in {mydetails.location !== null ? mydetails.location : ''}</Text>
:
<Text style={styles.cupText1Sz2}>complete your profile to see your ranking</Text>

                }
                {/* =======================winners List===================== */}
                <List style={styles.ListWrap}>
                  {MyShortBrokkers}
                  {/* <ListItem avatar style={styles.ListItemWrap}>
                    <Left style={styles.ListLeftSty}>
                      <Text style={[styles.NameStyInd2, { color: '#1E93E2', marginRight: 10 }]}>1.</Text>
                      <Thumbnail source={Miniuser1} style={styles.listImagesty1} />
                    </Left>
                    <Body style={styles.ListBodySty}>
                      <Text style={[styles.NameStyInd2, { color: '#2F2F2F' }]}>Kumar Pratik</Text>
                    </Body>
                    <Right style={styles.ListRightSty}>
                      <Text style={[styles.NameStyInd2, { color: '#1E93E2', }]}>543pts</Text>
                    </Right>
                  </ListItem> */}
                  {/*                   
                  <ListItem avatar style={styles.ListItemWrap}>
                    <Left style={styles.ListLeftSty}>
                      <Text style={[styles.NameStyInd2, { color: '#1E93E2', marginRight: 10 }]}>2.</Text>
                      <Thumbnail source={Miniuser1} style={styles.listImagesty1} />
                    </Left>
                    <Body style={styles.ListBodySty}>
                      <Text style={[styles.NameStyInd2, { color: '#2F2F2F' }]}>Kumar Pratik</Text>
                    </Body>
                    <Right style={styles.ListRightSty}>
                      <Text style={[styles.NameStyInd2, { color: '#1E93E2', }]}>543pts</Text>
                    </Right>
                  </ListItem>
                 */}
                  {/* <ListItem avatar style={styles.ListItemWrap}>
                    <Left style={styles.ListLeftSty}>
                      <Text style={[styles.NameStyInd2, { color: '#1E93E2', marginRight: 10 }]}>3.</Text>
                      <Thumbnail source={Miniuser1} style={styles.listImagesty1} />
                    </Left>
                    <Body style={styles.ListBodySty}>
                      <Text style={[styles.NameStyInd2, { color: '#2F2F2F' }]}>Kumar Pratik</Text>
                    </Body>
                    <Right style={styles.ListRightSty}>
                      <Text style={[styles.NameStyInd2, { color: '#1E93E2', }]}>543pts</Text>
                    </Right>
                  </ListItem>
                
                 */}
                </List>
                <Button style={styles.ListBtnSty}>
                  <Text style={[styles.NameStyInd2, { color: "#FAFAFA" }]}>Complete your profile (10pts)</Text>
                </Button>

                <List style={[styles.ListWrap2,]}>
                  <ListItem avatar style={[styles.ListItemWrapCircle, { marginLeft: 0, paddingLeft: 20, }]}>
                    <Left style={styles.ListLeftSty}>
                      <View style={{ width: 20, }}>
                        <SvgUri
                          width="11px"
                          height="11px"
                          uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593602698/Mobile_Assets/dot_n0m4cb.svg"
                        />
                      </View>
                      <Text style={[styles.cupText1Sz2, { marginLeft: 10, }]}>
                        Earn points by completing tasks on the app

                        {/* {JSON.stringify(mydetails.isRecruiter)} */}
                      </Text>
                    </Left>
                  </ListItem>
                  <ListItem avatar style={[styles.ListItemWrapCircle, { marginLeft: 0, paddingLeft: 20,}]}>
                    <Left style={styles.ListLeftSty}>
                      <View style={{ width: 20,lineHeight:40 }}>
                        <SvgUri
                          width="11px"
                          height="11px"
                          uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593602698/Mobile_Assets/dot_n0m4cb.svg"
                        />
                      </View>
                      <Text style={[styles.cupText1Sz2, { marginLeft: 10, }]}>
                        Complete your profile to 100%
                      </Text>
                    </Left>
                  </ListItem>
                  <ListItem avatar style={[styles.ListItemWrapCircle, { marginLeft: 0, paddingLeft: 20, }]}>
                    <Left style={styles.ListLeftSty}>
                      <View style={{ width: 20, }}>
                        <SvgUri
                          width="11px"
                          height="11px"
                          uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593602698/Mobile_Assets/dot_n0m4cb.svg"
                        />
                      </View>

                      <Text style={[styles.cupText1Sz2, { marginLeft: 10 }]}>
                        Refer an opportunity to at least 5 friends daily
                      </Text>
                    </Left>
                  </ListItem>
                  <ListItem avatar style={[styles.ListItemWrapCircle, { marginLeft: 0, paddingLeft: 20,  }]}>
                    <Left style={styles.ListLeftSty}>
                      <View style={{ width: 20, }}>
                        <SvgUri
                          width="11px"
                          height="11px"
                          uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593602698/Mobile_Assets/dot_n0m4cb.svg"
                        />
                      </View>

                      <Text style={[styles.cupText1Sz2, { marginLeft: 10 }]}>
                        Post a job/opportunity
                      </Text>
                    </Left>
                  </ListItem>
                  <ListItem avatar style={[styles.ListItemWrapCircle, { marginLeft: 0, paddingLeft: 20, }]}>
                    <Left style={styles.ListLeftSty}>
                      <View style={{ width: 20, }}>
                        <SvgUri
                          width="11px"
                          height="11px"
                          uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593602698/Mobile_Assets/dot_n0m4cb.svg"
                        />
                      </View>
                      <Text style={[styles.cupText1Sz2, { marginLeft: 10 }]}>
                        Apply for a job/opportunity
                      </Text>
                    </Left>
                  </ListItem>
                  <ListItem avatar style={[styles.ListItemWrapCircle, { marginLeft: 0, paddingLeft: 20, }]}>
                    <Left style={styles.ListLeftSty}>
                      <View style={{ width: 20, }}>
                        <SvgUri
                          width="11px"
                          height="11px"
                          uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593602698/Mobile_Assets/dot_n0m4cb.svg"
                        />

                      </View>

                      <Text style={[styles.cupText1Sz2, { marginLeft: 10 }]}>
                        Convert your earned points to real money
                      </Text>
                    </Left>
                  </ListItem>

                </List>



                {/* =======================winners List===================== */}
              </View>

              <List style={[styles.ListWrap2,]}>
                {
                  mydetails.isRecruiter === 0 ?
                    <ListItem avatar style={[styles.ListItemWrap, { marginLeft: 0, paddingLeft: 20, height: 50 }]}>
                      <TouchableOpacity
                        onPress={() => { this.props.navigation.navigate("RecruiterForm") }}
                      >
                        <Left style={styles.ListLeftSty}>
                          <View style={{ width: 30, }}>
                            <SvgUri
                              width="23px"
                              height="18.04px"
                              uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593602698/Mobile_Assets/recruiter_t4bnbh.svg"
                            />
                            {/* <Thumbnail square source={Recruiter} style={styles.recruiter} /> */}
                          </View>
                          <Text style={[styles.NameStyInd4, { marginLeft: 25 }]}>
                            Become a recruiter { JSON.stringify(mydetails.isRecruiter)}
                    </Text>
                        </Left>
                      </TouchableOpacity>

                    </ListItem>
                    :
                    <ListItem avatar style={[styles.ListItemWrap, { marginLeft: 0, paddingLeft: 20, height: 50 }]}>
                      <TouchableOpacity
                        onPress={() => { this.props.navigation.navigate("RecruiterDashboard") }}
                        // onPress={() => { this.props.navigation.navigate("RecruiterForm") }}

                      >
                        <Left style={styles.ListLeftSty}>
                          <View style={{ width: 30, }}>
                            <SvgUri
                              width="23px"
                              height="18.04px"
                              uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593602698/Mobile_Assets/recruiter_t4bnbh.svg"
                            />

                          </View>
                          <Text style={[styles.NameStyInd4, { marginLeft: 25 }]}>
                            Recruiter Dashboard
                    </Text>
                        </Left>
                      </TouchableOpacity>

                    </ListItem>
                }


                <ListItem avatar style={[styles.ListItemWrap, { marginLeft: 0, paddingLeft: 20, height: 50 }]}>
                  <TouchableOpacity
                    onPress={() => { this.props.navigation.navigate("Insights") }}
                  >
                    <Left style={styles.ListLeftSty}>
                      <View style={{ width: 30, }}>
                        <SvgUri
                          width="23px"
                          height="17px"
                          uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593602698/Mobile_Assets/insights_vbpqu9.svg"
                        />
                      </View>
                      <Text style={[styles.NameStyInd3, { marginLeft: 25 }]}>
                        Insights
                      </Text>
                    </Left>

                  </TouchableOpacity>

                </ListItem>
                {/* <ListItem avatar style={[styles.ListItemWrap, { marginLeft: 0, paddingLeft: 20, height: 50 }]}>
                  <TouchableOpacity
                    onPress={() => { this.props.navigation.navigate("Payment") }}
                  >
                    <Left style={styles.ListLeftSty}>
                      <View style={{ width: 30, }}>
                        <SvgUri
                          width="21px"
                          height="17px"
                          uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593602698/Mobile_Assets/payments_aohvvt.svg"
                        />
                      </View>

                      <Text style={[styles.NameStyInd3, { marginLeft: 25 }]}>
                        Payments
                      </Text>
                    </Left>
                  </TouchableOpacity>
                </ListItem> */}

                <ListItem avatar style={[styles.ListItemWrap, { marginLeft: 0, paddingLeft: 20, height: 50 }]}>
                  <TouchableOpacity
                    onPress={() => { this.props.navigation.navigate("Saved") }}
                  >
                    <Left style={styles.ListLeftSty}>
                      <View style={{ width: 30, }}>
                        <SvgUri
                          width="14px"
                          height="20px"
                          uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593602698/Mobile_Assets/saves_tezsl9.svg"
                        />
                      </View>

                      <Text style={[styles.NameStyInd3, { marginLeft: 25 }]}>
                        Saved
                      </Text>
                    </Left>
                  </TouchableOpacity>
                </ListItem>
                <ListItem avatar style={[styles.ListItemWrap, { marginLeft: 0, paddingLeft: 20, height: 50 }]}>

                  <TouchableOpacity
                    onPress={() => { this.props.navigation.navigate("League") }}
                  >
                    <Left style={styles.ListLeftSty}>
                      <View style={{ width: 30, }}>
                        <SvgUri
                          width="21px"
                          height="18px"
                          uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593602700/Mobile_Assets/workbrook_a8kylw.svg"
                        />
                      </View>
                      <Text style={[styles.NameStyInd3, { marginLeft: 25 }]}>
                        Workbrook League
                      </Text>
                    </Left>
                  </TouchableOpacity>
                </ListItem>
                <ListItem avatar style={[styles.ListItemWrap, { marginLeft: 0, paddingLeft: 20, height: 50 }]}>
                  <TouchableOpacity
                    onPress={() => { this.props.navigation.navigate("Invite") }}
                  >
                    <Left style={styles.ListLeftSty}>
                      <View style={{ width: 30, }}>
                        <SvgUri
                          width="22px"
                          height="18px"
                          uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593602698/Mobile_Assets/invite_gnkdz2.svg"
                        />
                      </View>

                      <Text style={[styles.NameStyInd3, { marginLeft: 25 }]}>
                        Invite a friend
                      </Text>
                    </Left>
                  </TouchableOpacity>
                </ListItem>
                <ListItem avatar style={[styles.ListItemWrap, { marginLeft: 0, paddingLeft: 20, height: 50 }]}>
                  <TouchableOpacity
                    onPress={() => { this.props.navigation.navigate("Subscription") }}
                  >
                    <Left style={styles.ListLeftSty}>
                      <View style={{ width: 30, }}>
                        <SvgUri
                          width="22px"
                          height="22px"
                          uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593602700/Mobile_Assets/subscription_mrmhnu.svg"
                        />
                      </View>

                      <Text style={[styles.NameStyInd3, { marginLeft: 25 }]}>
                        Subscriptions
                      </Text>
                    </Left>
                  </TouchableOpacity>
                </ListItem>
                <ListItem avatar style={[styles.ListItemWrap, { marginLeft: 0, paddingLeft: 20, height: 50 }]}>
                  <TouchableOpacity
                    onPress={() => { this.props.navigation.navigate("MyWallet") }}
                  >
                    <Left style={styles.ListLeftSty}>
                      <View style={{ width: 30, }}>
                        <SvgUri
                          width="22px"
                          height="21.04px"
                          uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593611531/Mobile_Assets/wallet_lkacdq.svg"
                        />
                      </View>

                      <Text style={[styles.NameStyInd3, { marginLeft: 25 }]}>
                        My Wallet
                      </Text>
                    </Left>
                  </TouchableOpacity>
                </ListItem>
                <ListItem avatar style={[styles.ListItemWrap, { marginLeft: 0, paddingLeft: 20, height: 50 }]}>
                  <TouchableOpacity
                    // onPress={() => { this.props.navigation.navigate("Shared") }}
                    onPress={() => { this.props.navigation.navigate("SharedJobs") }}
                  >
                    <Left style={styles.ListLeftSty}>
                      <View style={{ width: 30, }}>
                        <SvgUri
                          width="22px"
                          height="24px"
                          uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1598628697/Mobile_Assets/sharedIcon_lred2o.svg"
                        />
                      </View>

                      <Text style={[styles.NameStyInd3, { marginLeft: 25 }]}>
                      Shortlist 
                      </Text>
                    </Left>
                  </TouchableOpacity>
                </ListItem>

              </List>

              {/* =======================Cup Indicator=================== */}
              <List>
                <View style={styles.itemwrap}>
                  <ListItem icon> 
                    <Left>
                      <Thumbnail large source={CustomAsset.switchAccount}
                        style={styles.img_icon}

                      />
                    </Left>
                    <Body style={{ borderBottomColor: 'transparent' }}>
                      <TouchableOpacity onPress={this.SwitchAccount}>
                        <Text style={styles.menutext}>Switch Account</Text>
                      </TouchableOpacity>
                    </Body>

                  </ListItem>

                </View>

              </List>
              
            </View>
          </ScrollView>

        </View>
        <View style={styles.downbotton}>
          <Button full style={{ backgroundColor: 'red', borderRadius: 2, }} onPress={this.logout}>
            <Text>Logout</Text>
          </Button>
        </View>

        {/* </View> */}


      </ImageBackground>

    );
  }
}
export default Drawer;




  // https://pusher.com/tutorials/geolocation-sharing-react-native/

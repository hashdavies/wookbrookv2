
const Dimensions = require('Dimensions');
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  View, ScrollView,
  StatusBar, Image, StyleProvider, TouchableOpacity, Keyboard, KeyboardAvoidingView, Animated, ImageBackground
} from 'react-native';
import { NavigationActions } from 'react-navigation';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import AntDesign from 'react-native-vector-icons/AntDesign';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
// import ProfileImg from '../assets/images/profile/dp2.png';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import Octicons from 'react-native-vector-icons/Octicons';
import Feather from 'react-native-vector-icons/Feather';
import * as Animatable from 'react-native-animatable';
import {
  Button,
  Container,
  Header, Content, List, ListItem, Text, Icon, Left, Body, Right, Switch, CardItem, Thumbnail, Card
} from 'native-base';
import email from 'react-native-email';
import Communications from 'react-native-communications';
// import {Communicator} from '../dependency/UtilityFunctions';
import { inject, observer } from 'mobx-react/native'

import styles from '../StyleSheet/DrawerStyle';
import Miniuser1 from '../assets/workbrookAssets/miniUser.png';
import UserCup from '../assets/workbrookAssets/cup2.png';
import Recruiter from '../assets/workbrookAssets/recruiter.png';
import Insights from '../assets/workbrookAssets/insights.png';
import Payments from '../assets/workbrookAssets/payments.png';
import Saved from '../assets/workbrookAssets/saved.png';
import League from '../assets/workbrookAssets/league.png';
import Invite from '../assets/workbrookAssets/invite.png';
import Circle from '../assets/workbrookAssets/circle.png';
// import ThemeStore from '../../stores/Theme';
import DashboardStore from '../stores/Dashboard';
// import StyleSheetFactory from '../../../utils/theming';
import { getusertype, loggedinUserdetails } from '../dependency/UtilityFunctions';
import { onSignOut } from '../auth';
import { CustomAsset } from '../../utils/assets';
import { SvgUri } from 'react-native-svg';


// import DashboardStore from "../stores/Dashboard";
// import StyleSheetFactory from '../../utils/theming';
// import {inject, observer } from 'mobx-react/native'
// import ThemeStore from '../stores/Theme'

// import dateFormat from 'dateformat'
// import DropdownMenu from 'react-native-dropdown-menu'
// import BackgroundImage from '../screens/Background'
// import { onSignOut } from "";

// const mybackgroundimage = '../images/whitebg.jpg';
const window = Dimensions.get('window');

@observer
class Drawer extends Component {
  constructor(props) {
    super(props)
    this.state = {
      currentdate: "",
      taskcount: 0,
      name: '',
      usertype: '',
      mydetails: {
        usertype: 'Edves'
      }
    }
    this.SwitchAccount = this.SwitchAccount.bind(this)
    // this._selectPhotoTapped=this._selectPhotoTapped.bind(this)

  }
  navigateToScreen = (route) => () => {
    const navigateAction = NavigationActions.navigate({
      routeName: route
    });
    this.props.navigation.dispatch(navigateAction);
  }
  SwitchAccount = () => {
    //  alert("jeu")
    this.closedrawer();
    DashboardStore._ToggleSwitchModal(true);

  }

  async componentDidMount() {

    // this.gettaskcount();
    // this.getmysettings();
    let currentusertype = await getusertype();
    let userdetails = await loggedinUserdetails();
    console.log('userdetails');
    console.log(userdetails);
    console.log(currentusertype)
    this.setState({
      usertype: currentusertype,
      mydetails: userdetails
    })
  }


  closedrawer = () => {
    this.props.navigation.toggleDrawer()

  };
  // goto_settingspage = () => {
  //   // this.props.navigation.navigate("Settings");
  //   this.navigateToScreen('Settings')
  // };
  logout = () => {
    onSignOut().then(() => this.props.navigation.navigate("SignedOut"))
  };
  // goto_categorypage = () => {
  //   this.props.navigation.navigate("Projects");
  // };

  // goto_homepage = () => {
  //   this.props.navigation.navigate("Home");
  // };
  // goto_passedTaskPage = () => {
  //   this.props.navigation.navigate("PassedTask");
  // };
  // goto_upcomingTaskPage = () => {
  //   this.props.navigation.navigate("upcoming");
  // };
  render() {
    // this.props.navigation.navigate('SingleArticle' );
    const { usertype, mydetails } = this.state;
    return (

      <ImageBackground
        //  source={require(mybackgroundimage)}

        style={styles.container}
      >

        <View style={{ flex: 1, backgroundColor: '#F5F5F5', }}>
          {/* <View style={styles.CommentContentWrap}>*/}
          <View style={styles.SidebarView}>
            <CardItem style={styles.SidebarimageWrap}>
              <Left>
                <Thumbnail source={Miniuser1} />
                <Body>
                  <Text style={styles.NameStyInd1}>Tomzy Adex</Text>
                  {/* <Text style={styles.NameStyInd2}>Profile completed 75%</Text> */}
                  <Button style={styles.ViewPfSty}>
                    <Text style={[styles.NameStyInd2, { color: '#1E93E2', textAlign: 'left', paddingLeft: 0, }]}>
                      View Profile
                      </Text>
                  </Button>
                </Body>
              </Left>
            </CardItem>
          </View>

          <ScrollView>
            <View style={{ paddingTop: 25, backgroundColor: '#fff', }}>
              {/* =======================Cup Indicator=================== */}
              <View style={styles.CupViewSty}>
              <SvgUri
                  width='44px'
                  height='40px'
                  uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1590509060/Mobile_Assets/cup_litl8j.svg"
                ></SvgUri>
                {/* <Image source={UserCup} style={styles.cupSize} /> */}
                <Text style={styles.cupText1Sz1}>WorkBrook League</Text>
                <Text style={styles.cupText1Sz2}>Top 3 in Lagos, Nigeria</Text>
                {/* =======================winners List===================== */}
                <List style={styles.ListWrap}>
                  <ListItem avatar style={styles.ListItemWrap}>
                    <Left style={styles.ListLeftSty}>
                      <Text style={[styles.NameStyInd2, { color: '#1E93E2', marginRight: 10 }]}>1.</Text>
                      <Thumbnail source={Miniuser1} style={styles.listImagesty1} />
                    </Left>
                    <Body style={styles.ListBodySty}>
                      <Text style={[styles.NameStyInd2, { color: '#2F2F2F' }]}>Kumar Pratik</Text>
                    </Body>
                    <Right style={styles.ListRightSty}>
                      <Text style={[styles.NameStyInd2, { color: '#1E93E2', }]}>543pts</Text>
                    </Right>
                  </ListItem>
                  <ListItem avatar style={styles.ListItemWrap}>
                    <Left style={styles.ListLeftSty}>
                      <Text style={[styles.NameStyInd2, { color: '#1E93E2', marginRight: 10 }]}>2.</Text>
                      <Thumbnail source={Miniuser1} style={styles.listImagesty1} />
                    </Left>
                    <Body style={styles.ListBodySty}>
                      <Text style={[styles.NameStyInd2, { color: '#2F2F2F' }]}>Kumar Pratik</Text>
                    </Body>
                    <Right style={styles.ListRightSty}>
                      <Text style={[styles.NameStyInd2, { color: '#1E93E2', }]}>543pts</Text>
                    </Right>
                  </ListItem>
                  <ListItem avatar style={styles.ListItemWrap}>
                    <Left style={styles.ListLeftSty}>
                      <Text style={[styles.NameStyInd2, { color: '#1E93E2', marginRight: 10 }]}>3.</Text>
                      <Thumbnail source={Miniuser1} style={styles.listImagesty1} />
                    </Left>
                    <Body style={styles.ListBodySty}>
                      <Text style={[styles.NameStyInd2, { color: '#2F2F2F' }]}>Kumar Pratik</Text>
                    </Body>
                    <Right style={styles.ListRightSty}>
                      <Text style={[styles.NameStyInd2, { color: '#1E93E2', }]}>543pts</Text>
                    </Right>
                  </ListItem>
                </List>
                <Button style={styles.ListBtnSty}>
                  <Text style={[styles.NameStyInd2, { color: "#FAFAFA" }]}>Refer a job to a friend</Text>
                </Button>

                {/* =======================winners List===================== */}
              </View>
              <List style={[styles.ListWrap2,]}>
                <ListItem avatar style={[styles.ListItemWrap, { marginLeft: 0, paddingLeft: 20, height: 40 }]}>
                  <Left style={styles.ListLeftSty}>
                    <View style={{ width: 30, }}>
                      <Thumbnail square source={Recruiter} style={styles.recruiter} />
                    </View>
                    <Text style={[styles.NameStyInd3, { marginLeft: 25 }]}>
                      Become a recruiter
                      </Text>
                  </Left>
                </ListItem>
                <ListItem avatar style={[styles.ListItemWrap, { marginLeft: 0, paddingLeft: 20, height: 40 }]}>
                  <Left style={styles.ListLeftSty}>
                    <View style={{ width: 30, }}> 
                      <Thumbnail square source={Insights} style={styles.recruiter} />
                    </View>
                    <Text style={[styles.NameStyInd3, { marginLeft: 25 }]}>
                      Insights
                      </Text>
                  </Left>
                </ListItem>
                <ListItem avatar style={[styles.ListItemWrap, { marginLeft: 0, paddingLeft: 20, height: 40 }]}>
                  <Left style={styles.ListLeftSty}>
                    <View style={{ width: 30, }}>
                      <Thumbnail square source={Payments} style={styles.recruiter} />
                    </View>

                    <Text style={[styles.NameStyInd3, { marginLeft: 25 }]}>
                      Payments
                      </Text>
                  </Left>
                </ListItem>
                <ListItem avatar style={[styles.ListItemWrap, { marginLeft: 0, paddingLeft: 20, height: 40 }]}>
                  <Left style={styles.ListLeftSty}>
                    <View style={{ width: 30, }}>
                      <Thumbnail square source={Saved} style={[styles.recruiter, { height: 20, width: 14 }]} />
                    </View>

                    <Text style={[styles.NameStyInd3, { marginLeft: 25 }]}>
                      Saved
                      </Text>
                  </Left>
                </ListItem>
                <ListItem avatar style={[styles.ListItemWrap, { marginLeft: 0, paddingLeft: 20, height: 40 }]}>
                  <Left style={styles.ListLeftSty}>
                    <View style={{ width: 30, }}>
                      <Thumbnail square source={League} style={[styles.recruiter,]} />
                    </View>
                    <Text style={[styles.NameStyInd3, { marginLeft: 25 }]}>
                      Workbrook League
                      </Text>
                  </Left>
                </ListItem>
                <ListItem avatar style={[styles.ListItemWrap, { marginLeft: 0, paddingLeft: 20, height: 40 }]}>
                  <Left style={styles.ListLeftSty}>
                    <View style={{ width: 30, }}>
                      <Thumbnail square source={Invite} style={styles.recruiter} />
                    </View>

                    <Text style={[styles.NameStyInd3, { marginLeft: 25 }]}>
                      Invite a friend
                      </Text>
                  </Left>
                </ListItem>

              </List>

              {/* =======================Cup Indicator=================== */}
              <List>

                <View style={styles.itemwrap}>
                  <ListItem icon>
                    <Left>
                      <Thumbnail large source={CustomAsset.switchAccount}
                        style={styles.img_icon}

                      />
                    </Left>
                    <Body style={{ borderBottomColor: 'transparent' }}>
                      <TouchableOpacity onPress={this.SwitchAccount}>
                        <Text style={styles.menutext}>Switch Account</Text>
                      </TouchableOpacity>
                    </Body>

                  </ListItem>

                </View>

              </List>
            </View>
          </ScrollView>

        </View>
        <View style={styles.downbotton}>
          <Button full style={{ backgroundColor: 'red', borderRadius: 2, }} onPress={this.logout}>
            <Text>Logout</Text>
          </Button>
        </View>

        {/* </View> */}


      </ImageBackground>

    );
  }
}
export default Drawer;




  // https://pusher.com/tutorials/geolocation-sharing-react-native/
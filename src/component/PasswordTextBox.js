import React from 'react';
import { View, ScrollView, StyleSheet, ImageBackground, Image ,Keyboard} from 'react-native';

import { Item, Input, Icon, Label } from 'native-base';
import Ionicons from 'react-native-vector-icons/Ionicons';

class PasswordTextBox extends React.Component {
    state = {
        icon: "eye-slash",
        password: true
    };

    _changeIcon() {
        this.setState(prevState => ({
            icon: prevState.icon === 'eye' ? 'eye-slash' : 'eye',
            password: !prevState.password
        }));
    }

    render() {
        const { label, icon, onChange,placeholder,InputWrapper,InputStyle } = this.props;
        return (
            // <View style={styles.formStyle}>
            // <Label style={styles.text2}>{placeholder || 'Password'}</Label>
            //    <Item regular style={styles.form1}>
     
            //     <Input 
            //     secureTextEntry={this.state.password}
            //     placeholder={placeholder || 'Password'} 
            //     onChangeText={(e) => onChange(e)}
            //     onSubmitEditing={Keyboard.dismiss}
            //      />
            //     <Icon name={this.state.icon} onPress={() => this._changeIcon()} />
              
                
            //  </Item>
            //  </View>
                <Item regular style={InputWrapper || [styles.loginBtn1, { backgroundColor: '#E5E5E5', marginBottom: 5, }]}>
                <Input
                {...this.props}
                placeholder={placeholder || 'Password'} 
                secureTextEntry={this.state.password}
                  style={InputStyle || styles.inputSty}
                  onChangeText={(e) => onChange(e)}
                  onSubmitEditing={Keyboard.dismiss}
                 />
            <Icon type="FontAwesome" name={this.state.icon} onPress={() => this._changeIcon()} />

              </Item>
        );
    }
}

export default PasswordTextBox;
const styles = StyleSheet.create({
    inputfield_wrapper_style: {
        // marginVertical: 5,
        borderRadius: 15,
        borderStyle: 'solid',
        borderWidth: 2,
        borderColor: '#000',
        marginBottom: 12,
      },
      inputfieldstyle: {
        height: 40,
        // alignItems:'center',
        // textAlign:'center'
        // overflow: 'hidden'
      },
      loginBtn1: {
        height: 56,
        borderRadius: 5,
        marginBottom: 15,
        elevation: 0,
      },
      inputSty: {
        // color: "#BCBCBC",
        fontSize: 14,
        fontWeight: "300",
      },
  });
  
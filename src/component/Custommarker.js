import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,Image,
  View,Text,TouchableOpacity
   
} from 'react-native';
import FastImage from 'react-native-fast-image'
import UserAvatar from 'react-native-user-avatar';
// import {Icon,Badge } from 'native-base';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { Container, Header, Content, Button, Thumbnail,Form, Item, Input, Label,Badge,Icon ,Textarea} from 'native-base';
// var deleteItem
// deletehandle,ShowcolourPicker
import { CustomAsset } from '../../utils/assets';
import SvgUri from 'react-native-svg-uri';
const Custommarker = (props) =>{
  // console.log(props.Marker_icon )
  // console.log(props.coordinate)
  // console.log('props')
  let marker_icon=props.Marker_icon;
    return (
    
        
           
  
  
              <View style={styles.container}>
              {/* <View style={styles.imageWrap}>
<Image style={styles.img} source = {image} />   
  </View> */}
  {marker_icon ==='' ?
      //    <View style={styles.imageWrap}>
      // <FastImage
      //   style={styles.img}
      //   source={{
      //     uri:CustomAsset.Mapicon,
      //     priority: FastImage.priority.high,
      //     cache:FastImage.priority.immutable,
      //   }}
      //  />
      //  </View>

      // before

      //  <View style={styles.imageWrap}>
       <View style={styles.img}>
       {/* <UserAvatar size="40"
      name= 'museum ji'
       colors={['#f1c274','#e36441','#3e516f','#087071','#f88d01','#f75a4a','#935e42','#a83c87']}
       />  */}
       <Image 
style={styles.img} 
source = {{
  uri:'https://res.cloudinary.com/custocrypt/image/upload/v1560706865/HHPNG%20image/Hidden_History_Location_Pin_NEW.png',
  //uri:marker_icon,
}
}
 />  
          </View>
      :
 
     <View style={styles.imageWrap}>
      
    {/* <FastImage
      style={styles.img}
      source={{
        // uri:'https://lh3.googleusercontent.com/TC7N05ay44MJZvnEicVuAmsDxu_WdSJx2p9Nvfvc4V-iNCcgQI9i755-I1RuI-AHLw=s360',
        // uri:'https://res.cloudinary.com/custocrypt/image/upload/v1560706865/HHPNG%20image/Hidden_History_Location_Pin_NEW.png',
        uri:marker_icon,
        priority: FastImage.priority.high,
        // cache:FastImage.priority.immutable,
      }}
     />  */}

 <Image 
 style={styles.img} 
 source = {{
   // uri:'https://res.cloudinary.com/custocrypt/image/upload/v1560706865/HHPNG%20image/Hidden_History_Location_Pin_NEW.png',
   uri:marker_icon,
  //  uri:'https://cdn.mapmarker.io/api/v1/pin?size=55&background=%23F44E3B&icon=fa-star&color=%23FFFFFF&voffset=0&hoffset=-0.5&',
 }
 }
  />  

       
      </View>
      }

   
  
 
        </View>
       
     );
}
 

export default Custommarker;


const styles = StyleSheet.create({
  container: {
    flex: 1,
    // flexDirection:'column',
    // justifyContent: 'center',
    // backgroundColor: '#F5FCFF'
    // backgroundColor: '#fff'
  }, 
  btnwrap: {
    flexDirection: 'row',
    marginRight:'auto',
    marginLeft:'auto',
    width:'100%',
    marginTop:15,
  },
  imageWrap: {
    width:50,
    height:50,
    // borderRadius: 50,
    // alignItems: 'center',
    // justifyContent:'center',
    // backgroundColor:'#2c3f50',
    flex:1,

        },
        img: {
          // height: 40,
          // // borderRadius: 40,
          // width:40,
         // flex: 1,
    aspectRatio: 1.0, 
    // resizeMode: 'contain',
    width: 50,
    height:50,
    resizeMode: 'contain',
         
        },
        // marker: {
        //   width: 8,
        //   height: 8,
        //   borderRadius: 4,
        //   backgroundColor: "rgba(130,4,150, 0.9)",
        //   // position: "relative",
        // },
        ring: {
          width: 24,
          height: 24,
          borderRadius: 12,
          backgroundColor: "rgba(130,4,150, 0.3)",
          // position: "absolute",
          borderWidth: 1,
          borderColor: "rgba(130,4,150, 0.5)",
        },
        markerWrap: {
          alignItems: "center",
          justifyContent: "center",
        },
        triangle: {
          width: 0,
          height: 0,
          backgroundColor: 'transparent',
          // backgroundColor: 'red',
          borderStyle: 'solid',
          borderLeftWidth: 10,
          borderRightWidth: 10,
          borderBottomWidth: 20,
          borderLeftColor: 'transparent',
          borderRightColor: 'transparent',
          borderBottomColor: '#2c3f50',
          marginLeft:15,
          marginTop:-2,
        },
        triangleDown: {
          transform: [
            {rotate: '180deg'}
          ]
        }
});
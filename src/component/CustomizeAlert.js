import React, { Component } from "react";
import { 
  View,
  Text,
  StyleSheet,Platform
  } from "react-native";
  import {StatusBar} from 'react-native';
  import { withNavigation } from 'react-navigation';
import { Left, Right, Icon, Title } from 'native-base';
// import {loggedinUserdetails} from '../dependency/UtilityFunctions'; 
import { Container, Header, Content, Button,Thumbnail,Form, Item, Input, Label,Body,CheckBox,Badge } from 'native-base';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import EvilIcons from 'react-native-vector-icons/MaterialIcons';
import { IconStyles, AppColor, APP_STYLES } from '../../utils/AppStyles';
import StyleSheetFactory from '../../utils/theming';
import {inject, observer } from 'mobx-react/native';
import ThemeStore from '../stores/Theme';
import DashboardStore from "../stores/Dashboard";
import Dialog from "react-native-dialog";


@observer
class CustomizeAlert extends Component{
  constructor(props) {
    super(props);
    this.state = {
      loggedin_userid:'',
      fullname:'',
      Myid:'',
      NotificationCount:0
      
     
    }
    this._handleAlertDialogue = this._handleAlertDialogue.bind(this);

   }
   componentDidMount(){
    // this.Getdetails();
   // DashboardStore._UpdateNotification(true,'');
   }
  //  _UpdateNotification

  _handleAlertDialogue = () => {
   DashboardStore._UpdateNotification(false,'');
  };

  render(){
    let myStyleSheet = StyleSheetFactory.getSheet()

    const {NotificationCount}=this.state;
    const {icon2render}=this.props
    let title= `Welcome  ${this.state.fullname}`;
    return (
      <View>
      {/* <TouchableOpacity onPress={this.showDialog}>
        <Text>Show Dialog</Text>
      </TouchableOpacity> */}
      <Dialog.Container visible={DashboardStore.AlertPayload.showlert}>
        <Dialog.Title>{DashboardStore.AlertPayload.AlertTitle===''? 'System Notification':DashboardStore.AlertPayload.AlertTitle}</Dialog.Title>
        <Dialog.Description>
          {/* Do you want to delete this account? You cannot undo this action. */}
          {DashboardStore.AlertPayload.AlertText}

        </Dialog.Description>
        <Dialog.Button label="Okay" onPress={this._handleAlertDialogue} />
        {/* <Dialog.Button label="Proceed" onPress={this.ProceedNow} /> */}
      </Dialog.Container>
    </View>
    
    );
  }
}
 
export default withNavigation(CustomizeAlert);
const styles = StyleSheet.create({
  container:{
  flex:1,
  alignItems:'center',
  justifyContent:'center'
  },
  iconsizeNcolour: {
    fontSize:20,
    color:'#fff',
  
    },
  headerBody: {
  ...Platform.select({
      ios: {
        marginLeft:-150,
      },
      android: {
        marginLeft:-8,
      }
    }),
  
  },
  headerBody_2: {
  ...Platform.select({
      ios: {
        marginLeft:-80,
      },
      android: {
        marginLeft:-80,
      }
    }),
  
  }

});
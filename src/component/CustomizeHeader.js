import React, { Component } from "react";
import { 
  View,
  Text,
  StyleSheet,Platform
  } from "react-native";
  import {StatusBar} from 'react-native';
  import { withNavigation } from 'react-navigation';
import { Left, Right, Icon, Title } from 'native-base';
// import {loggedinUserdetails} from '../dependency/UtilityFunctions'; 
import { Container, Header, Content, Button,Thumbnail,Form, Item, Input, Label,Body,CheckBox,Badge } from 'native-base';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import EvilIcons from 'react-native-vector-icons/MaterialIcons';
import { IconStyles, AppColor, APP_STYLES } from '../../utils/AppStyles';
import StyleSheetFactory from '../../utils/theming';
import {inject, observer } from 'mobx-react/native'
import ThemeStore from './../stores/Theme'


@observer
class CustomizeHeader extends Component{
  constructor(props) {
    super(props);
    this.state = {
      loggedin_userid:'',
      fullname:'',
      Myid:'',
      NotificationCount:0
      
     
    }
   }
   componentDidMount(){
    // this.Getdetails();
   }




  render(){
    let myStyleSheet = StyleSheetFactory.getSheet()

    const {NotificationCount}=this.state;
    const {icon2render}=this.props
    let title= `Welcome  ${this.state.fullname}`;
    return (
      // <Header style={{backgroundColor:'#000',borderBottomColor:'#000',borderBottomWidth:2,fontSize:14,zIndex:10,position:'absolute',}}>
      <Header style={[myStyleSheet.background,{borderTopColor:'#ccc',borderTopWidth:0.5,}]}>
      <StatusBar
  backgroundColor={ThemeStore.APP_STYLES._defalutColor}
  barStyle="light-content"
  animated={true}
  StatusBarAnimation='slide'
/>
        <Left>
          {
            this.props.leftside=='menu'?
<Button transparent  onPress={() => {this.props.navigation.toggleDrawer()}}>
  
          <MaterialCommunityIcons name="menu"  size={20} color='#fff' />
          </Button>

            :
<Button transparent  onPress={ ()=> {this.props.navigation.goBack()}}>
  
<Ionicons name={icon2render ? icon2render:'md-arrow-back'}  size={30} color='#fff' />
 
</Button>

          }
          

   
           
         
   
        </Left>
        <Body style={this.props.ShowNotification ? styles.headerBody:styles.headerBody_2}>
          <Title style={[{ fontWeight: 'bold', fontSize: 20, }, AppColor.WhiteColour]}> {this.props.title || title}</Title>
          


        </Body>

        {
            this.props.ShowNotification===true ?

       <Right>  
         <Button transparent>
            <EvilIcons name='search' style={styles.iconsizeNcolour}/>
          </Button>  

           
         
        </Right>
        :null
          }
      </Header>
    );
  }
}
 
export default withNavigation(CustomizeHeader);
const styles = StyleSheet.create({
  container:{
  flex:1,
  alignItems:'center',
  justifyContent:'center'
  },
  iconsizeNcolour: {
    fontSize:20,
    color:'#fff',
  
    },
  headerBody: {
  ...Platform.select({
      ios: {
        marginLeft:-150,
      },
      android: {
        marginLeft:-8,
      }
    }),
  
  },
  headerBody_2: {
  ...Platform.select({
      ios: {
        marginLeft:-80,
      },
      android: {
        marginLeft:-80,
      }
    }),
  
  }

});
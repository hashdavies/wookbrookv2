import React, { Component } from 'react';
import {
  StyleSheet,
  ImageBackground,
  Keyboard,
  TouchableOpacity,
  ScrollView,
  Image,
  FlatList,
} from 'react-native';
import {
  Container,
  Text,
  Header,
  Content,
  Textarea,
  Button,
  View,
  Thumbnail,
  Form,
  Item,
  Input,
  Label,
  Badge,
  Icon,
  Footer,
  FooterTab,
  Picker,
  Item as FormItem,
} from 'native-base';

import * as Animatable from 'react-native-animatable';
import Entypo from 'react-native-vector-icons/Entypo';
import Feather from 'react-native-vector-icons/Feather';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
 import Ionicons from 'react-native-vector-icons/Ionicons';
 import TimeAgo from 'react-native-timeago';

import { CustomAsset } from '../../utils/assets';
import { IconStyles, AppColor, APP_STYLES } from '../../utils/AppStyles';
import styles from "../StyleSheet/NoticeStyle";
import { generate_id} from '../dependency/UtilityFunctions'; 


const NoticeData = (props) =>{
  let timestamp = "2015-06-21T06:24:44.124Z";
  // console.warn("here")
  // console.warn(props.data.AssociatedUsers)
   let participant_data=props.data.AssociatedUsers;  
const participant_images=  
participant_data.map(function(item,index){
  return   <View key={generate_id} style={styles.singleparticipant}>
  <ImageBackground
    animation="bounceInDown"
    duration={500}
    borderRadius={15}
    style={styles.participant_Dp}
    source={{uri: item.ProfilePicture}}
  >
    <Animatable.View
      animation="bounceIn"
      duration={600}
      delay={1001}
      style={[styles.indicator, styles.unread_indicator]}
    >
      <Text />
    </Animatable.View>
  </ImageBackground>
</View>
})
 
    return (
      <TouchableOpacity onPress={props.SingleNotice}>
      <View style={styles.innerpage}>
      <Animatable.View
        animation="zoomInDown"
        duration={600}
        delay={100}
        style={styles.list_item_wrapper}
      >
        <View style={{ flexDirection: 'row' }}>
          <View style={{ width: 50, flexDirection: 'column' }}>
            <View
              style={{ alignItems: 'center', justifyContent: 'center', marginBottom: 4 }}
            >
              <Ionicons name="md-alarm" style={styles.iconsizeNcolour} />
            </View>
            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
              <Text style={styles.time_ago}>
              {/* New ! */}
              <TimeAgo time={props.data.DateSent} />
              </Text>
               
            </View>
          </View>

          <View style={{ width: 20, flexDirection: 'column', flex: 1 }}>
            <View style={styles.notice_title_wrapper}>
              <Text style={[styles.notice_title_,AppColor.BlackColour]} numberOfLines={1}>
                              {props.data.Title}
              </Text>
            </View>

            <View style={styles.notice_short_content_wrapper}>
              <Text style={[styles.notice_short_content,AppColor.BlackColour]} numberOfLines={3}>
              {props.data.Message}
              </Text>
            </View>
          </View>
        </View>
        <View style={{ flexDirection: 'row' }}>
          <View style={{ width: 20, flexDirection: 'row', flex: 1 }}>
            <View style={styles.paticipant_imgwrap}>
            {participant_images}
        
            </View>
          </View>

          <View style={{alignItems: 'center', width: 20,marginBottom:25,marginRight:15,}}>
            <Animatable.View
              animation="bounceIn"
              duration={600}
              delay={1001}
              style={[styles.rightIndicator, styles.unread_indicator]}
            >
              <Text />
            </Animatable.View>
          </View>
        </View>
      </Animatable.View>
    </View>
    </TouchableOpacity>

     
    
    );
}
 

export default NoticeData;


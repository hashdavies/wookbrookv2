import React, { Component } from "react";
import { 
  View,
  Text,
  StyleSheet
  } from "react-native";
  import {StatusBar} from 'react-native';
  import { withNavigation } from 'react-navigation';
import { Left, Right, Icon, Title } from 'native-base';
 import { Container, Header, Content, Button,Thumbnail,Form, Item, Input, Label,Body,CheckBox,Badge } from 'native-base';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import SpinnerButton from 'react-native-spinner-button';

class SpinButton extends Component{
  constructor(props) {
    super(props);
    this.state = {
       defaultLoading:false
    };
   }
   componentDidMount(){
    }
 


  render(){
 
    return (
      <SpinnerButton
      buttonStyle={this.props.style||styles.buttonStyle}
       isLoading={this.props.loaderState||this.state.defaultLoading}
      onPress={this.props.onPressAction}
      indicatorCount={10}
    >
      <Text style={this.props.buttonTextStyle||styles.buttonText}>{this.props.buttonText}</Text>
    </SpinnerButton>
   );
  }
}
 
export default withNavigation(SpinButton);
const styles = StyleSheet.create({
  buttonStyle:{ 
    backgroundColor: '#121212',
     borderStyle: 'solid', 
     borderRadius: 3, 
     elevation: 0,
      height: 45 
    },
    buttonText:{ 
    fontWeight:'bold',
    padding: 20,
      textTransform:'capitalize',
       padding: 20, 
      fontSize: 16,
      color:'#fff'
    },

});
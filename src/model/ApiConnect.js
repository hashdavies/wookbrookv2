import qs from 'qs';
import axios from 'axios'
import { PersistData, ShowNotification, temp } from '../dependency/UtilityFunctions';
import DashboardStore from '../stores/Dashboard';


class ApiConnect {

  constructor() {

    // var asyncWrapperForSuper = async () => {
    // super();
    // this.value = await temp();
    // console.log("fff")
    // console.log( this.value)
    // };
    // asyncWrapperForSuper();
  }



  // async getValue(value) {
  //   var result = await temp();
  //   // return this.value + ' ' + result;
  // }


  // let access=temp().then;
  //   temp().then(values => { 
  //     console.log(values);
  //     console.log('MyToken>>>>');
  //     this.AccessToken = values;
  //   console.log(this.AccessToken);
  //   console.log('this.AccessToken');
  // });




  FetchAllIntrest = (pageNumber, pagesize) => new Promise(async (resolve, reject) => {
    // let usertoken=DashboardStore.UserLoginToken;
    let axiosInstance = DashboardStore.AxiosInstance;
    // this.loader_visible = true
    //  await   axiosInstance.get(`user/interests/${pageNumber}/${pagesize}`)
    // api/v1/settings/profile/skills/list/1/10
    await axiosInstance.get(`settings/profile/skills/list/${pageNumber}/${pagesize}`)
      .then(response => {
        console.log(response)
        console.log(response)
        resolve(response);

      })
      .catch(error => {
        let errorDatatype = typeof error
        console.log({ error });
        console.log(errorDatatype);
        if (errorDatatype !== 'object') {
          console.log("not object")
          resolve(false);
        }
        else {
          let Response = error.response;
          console.log("it id  object")
          console.log(Response)
          resolve(Response);
        }
      });



  });



  SaveMyIntrest = () => new Promise(async (resolve, reject) => {
    let SelectedSystemIntrest = DashboardStore.IntrestSettings.SelectedSystemIntrest
    if (SelectedSystemIntrest.length < 0) {
      ShowNotification("You must select Atleast one intrest.");
    }

    else {
      DashboardStore._ToggleProcessing(true)
      let params = {
        skills: SelectedSystemIntrest,
        // interests: SelectedSystemIntrest,
      }
      console.log(params)
      console.log('user/interests/update');
      let axiosInstance = DashboardStore.AxiosInstance;
      // await axiosInstance.post('user/interests/update', params)
      await axiosInstance.post('settings/profile/skills/create', params)
        .then(response => {
          console.log(response)
          resolve(response);
        })
        .catch(error => {
          let errorDatatype = typeof error
          console.log({ error });
          console.log(errorDatatype);
          if (errorDatatype !== 'object') {
            console.log("not object")
            resolve(false);
          }
          else {

            let Response = error.response;
            console.log("it id  object")
            console.log(Response)
            resolve(Response);
          }
        });



    }


  });
  UpdateProfile = () => new Promise(async (resolve, reject) => {
    let ExistingProfile = DashboardStore.profileupdatedata
    console.log(ExistingProfile)
    if (Object.keys(ExistingProfile).length === 0 && ExistingProfile.constructor === Object) {
      // alert("Empty");
      ShowNotification("Can not proceed, please again later", true);
    }

    else {
      let first_name = ExistingProfile.first_name
      let last_name = ExistingProfile.last_name
      // let gender=parseInt(ExistingProfile.gender)
      let gender = ExistingProfile.gender * 1;
      let dob = ExistingProfile.dob
      let location = ExistingProfile.location
      let industry = ExistingProfile.industry 
      let image_url = ExistingProfile.image_url
      let resume = ExistingProfile.resume
      let links = ExistingProfile.links
      let isShowOnProfile = parseInt(ExistingProfile.isShowOnProfile)
      let about = ExistingProfile.about
      let state = ExistingProfile.state
      let country = ExistingProfile.country
      let phone = ExistingProfile.phone
      // let location = `${state} , ${country} `
      // let Ind=[]
  //  Ind.push(industry);
        DashboardStore._ToggleProcessing(true)
      let params = {
        first_name,
        last_name,
        gender,
        dob,
        location,
        industry:industry,
        image_url,
        resume,
        links,
        isShowOnProfile,
        about,
        state,
        country,
        phone,
      }
      console.log(params)
      console.log('settings/profile/update');
      let axiosInstance = DashboardStore.AxiosInstance;
      await axiosInstance.post('settings/profile/update', params)
        .then(response => {
          console.log(response)
          resolve(response);
        })
        .catch(error => {
          let errorDatatype = typeof error
          console.log({ error });
          console.log(errorDatatype);
          if (errorDatatype !== 'object') {
            console.log("not object")
            resolve(false);
          }
          else {

            let Response = error.response;
            console.log("it id  object")
            console.log(Response)
            resolve(Response);
          }
        });



    }


  });
  processSkillManagement = () => new Promise(async (resolve, reject) => {
    let SkillManagement = DashboardStore.SkillManagement
    console.log(SkillManagement)
    if (Object.keys(SkillManagement).length === 0 && SkillManagement.constructor === Object) {
      // alert("Empty");
      ShowNotification("Can not proceed, please again later", true);
    }

    else {
      let companyName = SkillManagement.companyName
      let title = SkillManagement.title
      let startdate = SkillManagement.startdate
      let enddate = SkillManagement.enddate
      let achievement = SkillManagement.achievement
      let current_id = SkillManagement.current_id
      if (companyName === '') {
        ShowNotification('Please enter your company name', true);
      }
      else if (title === '') {
        ShowNotification('Please enter job title', true);

      }
      else if (startdate === '') {
        ShowNotification('Please choose start date', true);

      }
      else if (achievement === '') {
        ShowNotification('Please tell us your achievement', true);

      }
      else {
        DashboardStore._ToggleProcessing(true)
        let ApiURL = null;
        if (current_id === '') {
          ApiURL = "settings/profile/experience/create";
        }
        else {
          ApiURL = `settings/profile/experience/${current_id}/update`
        }
        let params = {
          "campany_name": companyName,
          "title": title,
          "start_date": startdate,
          "end_date": enddate,
          "achievements": achievement
        }


        console.log(params)
        console.log(ApiURL);
        let axiosInstance = DashboardStore.AxiosInstance;
        await axiosInstance.post(ApiURL, params)
          .then(response => {
            console.log(response)
            resolve(response);
          })
          .catch(error => {
            DashboardStore._ToggleProcessing(false)
            let errorDatatype = typeof error
            console.log({ error });
            console.log(errorDatatype);
            if (errorDatatype !== 'object') {
              console.log("not object")
              resolve(false);
            }
            else {

              let Response = error.response;
              console.log("it id  object")
              console.log(Response)
              resolve(Response);
            }
          });


      }


    }


  });

  processEducationManagement = () => new Promise(async (resolve, reject) => {
    let EducationManagement = DashboardStore.EducationManagement
    console.log(EducationManagement)
    if (Object.keys(EducationManagement).length === 0 && EducationManagement.constructor === Object) {
      // alert("Empty");
      ShowNotification("Can not proceed, please again later", true);
    }

    else {

      let school = EducationManagement.school
      let degree = EducationManagement.degree
      let field_of_study = EducationManagement.field_of_study
      let start_date = EducationManagement.start_date
      let end_date = EducationManagement.end_date
      let description = EducationManagement.description
      let current_id = EducationManagement.current_id

      if(description==''){
        description="nill";
      }

      if (school === '') {
        ShowNotification('Please enter your school', true);
      }
      else if (degree === '') {
        ShowNotification('Please enter degree name', true);

      }
      else if (field_of_study === '') {
        ShowNotification('Whats your field of study ?', true);

      }
      else if (start_date === '') {
        ShowNotification('Please tell us year of enrolment', true);

      }
      else if (end_date === '') {
        ShowNotification('Please tell us when you graduate', true);

      }
      else {
        DashboardStore._ToggleProcessing(true)
        let ApiURL = null;
        if (current_id === '') {
          ApiURL = "settings/profile/education/create";
        }
        else {
          ApiURL = `settings/profile/education/${current_id}/update`

        }
        let params = {
          school,
          degree,
          field_of_study,
          start_date,
          end_date,
          description,
        }


        console.log(params)
        console.log(ApiURL);
        let axiosInstance = DashboardStore.AxiosInstance;
        await axiosInstance.post(ApiURL, params)
          .then(response => {
            console.log(response)
            resolve(response);
          })
          .catch(error => {
            DashboardStore._ToggleProcessing(false)
            let errorDatatype = typeof error
            console.log({ error });
            console.log(errorDatatype);
            if (errorDatatype !== 'object') {
              console.log("not object")
              resolve(false);
            }
            else {

              let Response = error.response;
              console.log("it id  object")
              console.log(Response)
              resolve(Response);
            }
          });


      }


    }


  });

  FetchAllMyExperience = (pageNumber, pagesize) => new Promise(async (resolve, reject) => {
    // let usertoken=DashboardStore.UserLoginToken;
    let axiosInstance = DashboardStore.AxiosInstance;
    // this.loader_visible = true
    await axiosInstance.get(`settings/profile/experience`)
      .then(response => {
        console.log(response)
        console.log(response)
        resolve(response);

      })
      .catch(error => {
        let errorDatatype = typeof error
        console.log({ error });
        console.log(errorDatatype);
        if (errorDatatype !== 'object') {
          console.log("not object")
          resolve(false);
        }
        else {
          let Response = error.response;
          console.log("it id  object")
          console.log(Response)
          resolve(Response);
        }
      });



  });

  FetchAllMyEducation = (pageNumber, pagesize) => new Promise(async (resolve, reject) => {
    // let usertoken=DashboardStore.UserLoginToken;
    let axiosInstance = DashboardStore.AxiosInstance;
    // this.loader_visible = true
    await axiosInstance.get(`settings/profile/education`)
      .then(response => {
        console.log(response)
        console.log(response)
        resolve(response);

      })
      .catch(error => {
        let errorDatatype = typeof error
        console.log({ error });
        console.log(errorDatatype);
        if (errorDatatype !== 'object') {
          console.log("not object")
          resolve(false);
        }
        else {
          let Response = error.response;
          console.log("it id  object")
          console.log(Response)
          resolve(Response);
        }
      });



  });


  FetchAllMyKeywords = (pageNumber, pagesize) => new Promise(async (resolve, reject) => {
    // let usertoken=DashboardStore.UserLoginToken;
    let axiosInstance = DashboardStore.AxiosInstance;
    // this.loader_visible = true
    await axiosInstance.get(`settings/profile/skills`)
      .then(response => {
        console.log(response)
        console.log(response)
        resolve(response);

      })
      .catch(error => {
        let errorDatatype = typeof error
        console.log({ error });
        console.log(errorDatatype);
        if (errorDatatype !== 'object') {
          console.log("not object")
          resolve(false);
        }
        else {
          let Response = error.response;
          console.log("it id  object")
          console.log(Response)
          resolve(Response);
        }
      });



  });

  GetIndustries = (pageNumber, pagesize) => new Promise(async (resolve, reject) => {
    // let usertoken=DashboardStore.UserLoginToken;
    let axiosInstance = DashboardStore.AxiosInstance;
    // this.loader_visible = true
    await axiosInstance.get(`admin/industry/${pageNumber}/${pagesize}`)
      .then(response => {
        console.log(response)
        console.log(response)
        resolve(response);

      })
      .catch(error => {
        let errorDatatype = typeof error
        console.log({ error });
        console.log(errorDatatype);
        if (errorDatatype !== 'object') {
          console.log("not object")
          resolve(false);
        }
        else {
          let Response = error.response;
          console.log("it id  object")
          console.log(Response)
          resolve(Response);
        }
      });



  });

  ALLSkillWithOutFilter = (pageNumber, pagesize) => new Promise(async (resolve, reject) => {
    // let usertoken=DashboardStore.UserLoginToken;
    let axiosInstance = DashboardStore.AxiosInstance;
    // this.loader_visible = true
    await axiosInstance.get(`settings/profile/skills/all/${pageNumber}/${pagesize}`)
      .then(response => {
        console.log(response)
        console.log(response)
        resolve(response);

      })
      .catch(error => {
        let errorDatatype = typeof error
        console.log({ error });
        console.log(errorDatatype);
        if (errorDatatype !== 'object') {
          console.log("not object")
          resolve(false);
        }
        else {
          let Response = error.response;
          console.log("it id  object")
          console.log(Response)
          resolve(Response);
        }
      });



  });

  JobsLevels = () => new Promise(async (resolve, reject) => {
    // let usertoken=DashboardStore.UserLoginToken;
    let axiosInstance = DashboardStore.AxiosInstance;
    // this.loader_visible = true
    await axiosInstance.get(`jobs/levels`)
      .then(response => {
        console.log(response)
        console.log(response)
        resolve(response);

      })
      .catch(error => {
        let errorDatatype = typeof error
        console.log({ error });
        console.log(errorDatatype);
        if (errorDatatype !== 'object') {
          console.log("not object")
          resolve(false);
        }
        else {
          let Response = error.response;
          console.log("it id  object")
          console.log(Response)
          resolve(Response);
        }
      });



  });

  ProccessJobPosting = () => new Promise(async (resolve, reject) => {
    let JobPosting = DashboardStore.JobPosting
    console.log(JobPosting)
    if (Object.keys(JobPosting).length === 0 && JobPosting.constructor === Object) {
      // alert("Empty");
      ShowNotification("Can not proceed, please again later", true);
    }

    else {
      // const{JobPosting}=DashboardStore
      let current_id = JobPosting.current_id;
      let companyName = JobPosting.companyName;
      let industry = JobPosting.industry;
      let aboutCompany = JobPosting.aboutCompany;
      let location = JobPosting.location;
      let numberOfStaff = JobPosting.numberOfStaff;
      let roleTitle = JobPosting.roleTitle;
      let jobLevel = JobPosting.jobLevel;
      let wordForApplicant = JobPosting.wordForApplicant;
      let JobIntrests = JobPosting.intrest;
      let isCompanyRepresentative = JobPosting.isCompanyRepresentative;
      let isToRefTimeLineOrToHireRecruter = JobPosting.isToRefTimeLineOrToHireRecruter;
      let additional_files = JobPosting.additional_files;
      let job_type =  JobPosting.job_type; 



      if (additional_files.length < 1) {
        let message = "Job Banner missing";
        ShowNotification(message, true);
      }
      else if (companyName === '') {
        let message = "Company name is required";
        ShowNotification(message, true);
      }
      else if (industry === '') {
        let message = "Company  industry is required";
        ShowNotification(message, true);
      }
      else if (JobIntrests.length < 1) {
        let message = "Select 1 or More Skills required (Intrest) for the job";
        ShowNotification(message, true);
      }
      else if (aboutCompany === '') {
        let message = "Tell us briefly about the company";
        ShowNotification(message, true);
      }
      else if (location === '') {
        let message = "Job location is required";
        ShowNotification(message, true);
      }
      else if (numberOfStaff === '') {
        let message = "Number of staff is required";
        ShowNotification(message, true);
      }
      else if (roleTitle === '') {
        let message = "Role title is required";
        ShowNotification(message, true);
      }
      else if (jobLevel === '') {
        let message = "Job Entry level is required";
        ShowNotification(message, true);
      }
      else if (wordForApplicant === '') {
        let message = "Tell us about the application";
        ShowNotification(message, true);
      }
      else if (job_type === '') {
        let message = "Job type required ";
        ShowNotification(message, true);
      }
      else {

        DashboardStore._ToggleProcessing(true)
        let ApiURL = null;
        if (current_id === '') {
          ApiURL = "jobs/create";
        }
        else {
          // ApiURL=`settings/profile/education/${current_id}/update`
          ApiURL = `jobs/${current_id}/update`
        }
        let params = {
          "job_type": job_type,
          "campany_name": companyName,
          "industry": industry,
          "about_company": aboutCompany,
          // "no_of_staff": parseInt(numberOfStaff),
          "no_of_staff":numberOfStaff,
          "job_level": parseInt(jobLevel),
          "role_title": roleTitle,
          "reports_to": "Line Mgr",
          "applicant_benefit": wordForApplicant,
          "skills_required": JobIntrests,
          "location": location,
          "additional_files": additional_files,
          "isCompanyRepresentative": parseInt(isCompanyRepresentative),
          "isToRefTimeLineOrToHireRecruter": parseInt(isToRefTimeLineOrToHireRecruter)
        }


        console.log(params)
        console.log(ApiURL);
        let axiosInstance = DashboardStore.AxiosInstance;
        await axiosInstance.post(ApiURL, params)
          .then(response => {
            console.log(response)
            resolve(response);
          })
          .catch(error => {
            DashboardStore._ToggleProcessing(false)
            let errorDatatype = typeof error
            console.log({ error });
            console.log(errorDatatype);
            if (errorDatatype !== 'object') {
              console.log("not object")
              resolve(false);
            }
            else {

              let Response = error.response;
              console.log("it id  object")
              console.log(Response)
              resolve(Response);
            }
          });


      }


    }


  });

  GetJobsOppurtunites = (pageNumber, pagesize) => new Promise(async (resolve, reject) => {
    // let usertoken=DashboardStore.UserLoginToken;
    let axiosInstance = DashboardStore.AxiosInstance;
    // this.loader_visible = true
    await axiosInstance.get(`jobs/${pageNumber}/${pagesize}`)
      .then(response => {
        console.log(response)
        console.log(response)
        resolve(response);

      })
      .catch(error => {
        let errorDatatype = typeof error
        console.log({ error });
        console.log(errorDatatype);
        if (errorDatatype !== 'object') {
          console.log("not object")
          resolve(false);
        }
        else {
          let Response = error.response;
          console.log("it id  object")
          console.log(Response)
          resolve(Response);
        }
      });



  });

  ApplyForJob = () => new Promise(async (resolve, reject) => {
    let SingleJobOffer = DashboardStore.JobOppurtunity.SingleJobOffer
    console.log(SingleJobOffer)
    if (Object.keys(SingleJobOffer).length === 0 && SingleJobOffer.constructor === Object) {
      // alert("Empty");
      ShowNotification("Can not proceed, please again later", true);
    }

    else {
      // const{SingleJobOffer}=DashboardStore
      let current_id = SingleJobOffer.current_id;
      let job_id = SingleJobOffer.id;
      // let refCode = SingleJobOffer.refCode;
      let refCode = "4037419628";




      if (job_id === '') {
        let message = "job id is required";
        ShowNotification(message, true);
      }

      else {

        DashboardStore._ToggleProcessing(true)
        let ApiURL = null;
        ApiURL = "jobs/application/create";

        let params = {
          "job_id": job_id,
          "refCode": refCode,

        }


        console.log(params)
        console.log(ApiURL);
        let axiosInstance = DashboardStore.AxiosInstance;
        await axiosInstance.post(ApiURL, params)
          .then(response => {
            console.log(response)
            resolve(response);
          })
          .catch(error => {
            DashboardStore._ToggleProcessing(false)
            let errorDatatype = typeof error
            console.log({ error });
            console.log(errorDatatype);
            if (errorDatatype !== 'object') {
              console.log("not object")
              resolve(false);
            }
            else {

              let Response = error.response;
              console.log("it id  object")
              console.log(Response)
              resolve(Response);
            }
          });


      }


    }


  });


  GetRecruiters = (pageNumber, pagesize) => new Promise(async (resolve, reject) => {
    // let usertoken=DashboardStore.UserLoginToken;
    let axiosInstance = DashboardStore.AxiosInstance;
    // this.loader_visible = true
    let CurrentJobId =   DashboardStore.Recruiters.CurrentJobId;
    // alert(CurrentJobId);
    await axiosInstance.get(`recruiter/${CurrentJobId}/${pageNumber}/${pagesize}`)
      .then(response => {
        console.log(response)
        console.log(response)
        resolve(response);

      })
      .catch(error => {
        let errorDatatype = typeof error
        console.log({ error });
        console.log(errorDatatype);
        if (errorDatatype !== 'object') {
          console.log("not object")
          resolve(false);
        }
        else {
          let Response = error.response;
          console.log("it id  object")
          console.log(Response)
          resolve(Response);
        }
      });



  });
  LikeUnLike = (job_id) => new Promise(async (resolve, reject) => {
    if (job_id === '') {
      let message = "job id is required";
      ShowNotification(message, true);
    }

    else {

      DashboardStore._ToggleProcessing(true)
      let ApiURL = null;
      ApiURL = "jobs/like/likeUnLike";

      let params = {
        "job_id": job_id,

      }


      console.log(params)
      console.log(ApiURL);
      let axiosInstance = DashboardStore.AxiosInstance;
      await axiosInstance.post(ApiURL, params)
        .then(response => {
          console.log(response)
          resolve(response);
        })
        .catch(error => {
          DashboardStore._ToggleProcessing(false)
          let errorDatatype = typeof error
          console.log({ error });
          console.log(errorDatatype);
          if (errorDatatype !== 'object') {
            console.log("not object")
            resolve(false);
          }
          else {

            let Response = error.response;
            console.log("it id  object")
            console.log(Response)
            resolve(Response);
          }
        });


    }




  });

  SaveJobForLater = (job_id) => new Promise(async (resolve, reject) => {
    if (job_id === '') {
      let message = "job id is required";
      ShowNotification(message, true);
    }

    else {

      DashboardStore._ToggleProcessing(true)
      let ApiURL = null;
      ApiURL = "jobs/saved/create";

      let params = {
        "job_id": job_id,

      }


      console.log(params)
      console.log(ApiURL);
      let axiosInstance = DashboardStore.AxiosInstance;
      await axiosInstance.post(ApiURL, params)
        .then(response => {
          console.log(response)
          resolve(response);
        })
        .catch(error => {
          DashboardStore._ToggleProcessing(false)
          let errorDatatype = typeof error
          console.log({ error });
          console.log(errorDatatype);
          if (errorDatatype !== 'object') {
            console.log("not object")
            resolve(false);
          }
          else {

            let Response = error.response;
            console.log("it id  object")
            console.log(Response)
            resolve(Response);
          }
        });


    }




  });


  ProccessBecomeRecruiter = () => new Promise(async (resolve, reject) => {
    let BecomeRecruiter = DashboardStore.BecomeRecruiter
    console.log(BecomeRecruiter)
    if (Object.keys(BecomeRecruiter).length === 0 && BecomeRecruiter.constructor === Object) {
      // alert("Empty");
      ShowNotification("Can not proceed, please again later", true);
    }

    else {
      // const{JobPosting}=DashboardStore
      // let current_id=JobPosting.current_id;


      let fullname = BecomeRecruiter.fullname;
      let email = BecomeRecruiter.email;
      let phone = BecomeRecruiter.phone;
      let summary = BecomeRecruiter.summary;
      let location = BecomeRecruiter.location;
      let profilePicture = BecomeRecruiter.profilePicture;
      let averageAmount = BecomeRecruiter.averageAmount;
      let coordinate = BecomeRecruiter.coordinate;
      let industry = BecomeRecruiter.industry;
      let industryRecruitementExperience = BecomeRecruiter.industryRecruitementExperience;
      let linkdnUrl = BecomeRecruiter.linkdnUrl;
      let additionalFiles = BecomeRecruiter.additionalFiles;



      //    if(additional_files.length < 1){
      //     let message="Job Banner missing";
      //     ShowNotification(message, true);
      //  }
      if (fullname === '') {
        let message = "Full name is required";
        ShowNotification(message, true);
      }
      else if (email === '') {
        let message = "Email  address is required";
        ShowNotification(message, true);
      }

      else if (phone === '') {
        let message = "phone Number is required";
        ShowNotification(message, true);
      }
      else if (summary === '') {
        let message = "Tell us about yourself";
        ShowNotification(message, true);
      }
      else if (profilePicture === '') {
        let message = "profile picture is required";
        ShowNotification(message, true);
      }
      else if (industry.length < 1) {
        let message = "Choose 1 or more industry";
        ShowNotification(message, true);
      }
      else if (industryRecruitementExperience === '') {
        let message = "Years of Experience is required";
        ShowNotification(message, true);
      }

      else {
        // let d=[];
        // let h = d.push(profilePicture);
        // additionalFiles=h;
        DashboardStore._ToggleProcessing(true)
        let ApiURL = "recruiter/create";
        // if(current_id===''){
        //   ApiURL="jobs/create";
        // }
        // else{
        //   // ApiURL=`settings/profile/education/${current_id}/update`
        //   ApiURL=`jobs/${current_id}/update`

        // }
        let params = {
          "fullname": fullname,
          "email": email,
          // "email":"lol45383@smlmail.com",
          "phone": phone,
          "summary": summary,
          "profilePicture": profilePicture,
          "averageAmount": averageAmount,
          // "coordinate":coordinate,
          "coordinate": "3.455,6.78888",
          "industry": industry,
          "location": location,
          "industryRecruitementExperience": industryRecruitementExperience,
          "linkdnUrl": linkdnUrl,
          "additionalFiles": additionalFiles
        }


        console.log(params)
        console.log(ApiURL);
        let axiosInstance = DashboardStore.AxiosInstance;
        await axiosInstance.post(ApiURL, params)
          .then(response => {
            console.log(response)
            resolve(response);
          })
          .catch(error => {
            DashboardStore._ToggleProcessing(false)
            let errorDatatype = typeof error
            console.log({ error });
            console.log(errorDatatype);
            if (errorDatatype !== 'object') {
              console.log("not object")
              resolve(false);
            }
            else {

              let Response = error.response;
              console.log("it id  object")
              console.log(Response)
              resolve(Response);
            }
          });


      }


    }


  });

  GetUserIndustries = (pageNumber, pagesize) => new Promise(async (resolve, reject) => {
    // let usertoken=DashboardStore.UserLoginToken;
    let axiosInstance = DashboardStore.AxiosInstance;
    // this.loader_visible = true
    await axiosInstance.get(`industry`)
      .then(response => {
        console.log(response)
        console.log(response)
        resolve(response);

      })
      .catch(error => {
        let errorDatatype = typeof error
        console.log({ error });
        console.log(errorDatatype);
        if (errorDatatype !== 'object') {
          console.log("not object")
          resolve(false);
        }
        else {
          let Response = error.response;
          console.log("it id  object")
          console.log(Response)
          resolve(Response);
        }
      });



  });


  FilterRecuiterSearch = () => new Promise(async (resolve, reject) => {
    let FilterRecruiter = DashboardStore.FilterRecruiter
    console.log(FilterRecruiter)
    if (Object.keys(FilterRecruiter).length === 0 && FilterRecruiter.constructor === Object) {
      // alert("Empty");
      ShowNotification("Can not proceed, please again later", true);
    }

    else {
      // const{JobPosting}=DashboardStore
      // let current_id=JobPosting.current_id;


      let industry = FilterRecruiter.industry;
      let location = FilterRecruiter.location;
      let industryRecruitementExperience = FilterRecruiter.industryRecruitementExperience;




      //    if(additional_files.length < 1){
      //     let message="Job Banner missing";
      //     ShowNotification(message, true);
      //  }
      //  if(fullname===''){
      //       let message="Full name is required";
      //       ShowNotification(message, true);
      //    }
      //   else if(email===''){
      //       let message="Email  address is required";
      //       ShowNotification(message, true);
      //    }

      //   else if(phone===''){
      //       let message="phone Number is required";
      //       ShowNotification(message, true);
      //    }
      //   else if(summary===''){
      //       let message="Tell us about yourself";
      //       ShowNotification(message, true);
      //    }
      //   else if(profilePicture===''){
      //       let message="profile picture is required";
      //       ShowNotification(message, true);
      //    }
      //    else if(industry.length < 1){
      //     let message="Choose 1 or more industry";
      //     ShowNotification(message, true);
      //  }
      //   else if(industryRecruitementExperience===''){
      //       let message="Years of Experience is required";
      //       ShowNotification(message, true);
      //    }

      // else{
      // let d=[];
      // let h = d.push(profilePicture);
      // additionalFiles=h;
      DashboardStore._ToggleProcessing(true)
      let ApiURL = "recruiter/filter";
      // if(current_id===''){
      //   ApiURL="jobs/create";
      // }
      // else{
      //   // ApiURL=`settings/profile/education/${current_id}/update`
      //   ApiURL=`jobs/${current_id}/update`

      // }
      let params = {
        "industry": industry,
        "location": location,
        "industryRecruitementExperience": industryRecruitementExperience
      }


      console.log(params)
      console.log(ApiURL);
      let axiosInstance = DashboardStore.AxiosInstance;
      await axiosInstance.post(ApiURL, params)
        .then(response => {
          console.log(response)
          resolve(response);
        })
        .catch(error => {
          DashboardStore._ToggleProcessing(false)
          let errorDatatype = typeof error
          console.log({ error });
          console.log(errorDatatype);
          if (errorDatatype !== 'object') {
            console.log("not object")
            resolve(false);
          }
          else {

            let Response = error.response;
            console.log("it id  object")
            console.log(Response)
            resolve(Response);
          }
        });


      // }


    }


  });


  ProccessJobPosting2Recruiters = () => new Promise(async (resolve, reject) => {
    let JobPosting = DashboardStore.JobPosting
    console.log(JobPosting)
    if (Object.keys(JobPosting).length === 0 && JobPosting.constructor === Object) {
      // alert("Empty");
      ShowNotification("Can not proceed, please again later", true);
    }

    else {
      // const{JobPosting}=DashboardStore
      let current_id = JobPosting.current_id;
      let companyName = JobPosting.companyName; //
      let industry = JobPosting.industry;//
      let aboutCompany = JobPosting.aboutCompany; //
      let location = JobPosting.location;
      let numberOfStaff = JobPosting.numberOfStaff; //
      let roleTitle = JobPosting.roleTitle;//
      let jobLevel = JobPosting.jobLevel; //
      let wordForApplicant = JobPosting.wordForApplicant;
      let JobIntrests = JobPosting.intrest;
      let isCompanyRepresentative = JobPosting.isCompanyRepresentative;
      let isToRefTimeLineOrToHireRecruter = JobPosting.isToRefTimeLineOrToHireRecruter;
      let additional_files = JobPosting.additional_files;
      let SelectedRecuiter = JobPosting.SelectedRecuiter;
      let This_selectedRecruiters = DashboardStore.JobPosting.SelectedRecuiter.slice();
      let job_type =  JobPosting.job_type; 

      //  {
      //   "campany_name":"Eko Laundty 4",
      //   "industry":"Building",
      //   "about_company":"Goog Company",
      //   "no_of_staff":10,
      //   "job_level":1,
      //   "role_title":"Developer",

      //   "reports_to":"Line Mgr",

      //   "applicant_benefit":"story",
      //   "skills_required":["Law","Coding 3"],
      //   "location":"Loagos",
      //   "additional_files":["Link1","Link1","Link1"],
      //   "isCompanyRepresentative":1,
      //   "isToRefTimeLineOrToHireRecruter":1,
      //     "refererIdOne":4,
      //     "refererIdTwo":3

      // }

      if (additional_files.length < 1) {
        let message = "Job Banner missing";
        ShowNotification(message, true);
      }
      else if (companyName === '') {
        let message = "Company name is required";
        ShowNotification(message, true);
      }
      else if (industry === '') {
        let message = "Company  industry is required";
        ShowNotification(message, true);
      }
      else if (JobIntrests.length < 1) {
        let message = "Select 1 or More Skills required (Intrest) for the job";
        ShowNotification(message, true);
      }
      else if (This_selectedRecruiters.length < 2) {
        let message = "Please select 2 recruiters";
        ShowNotification(message, true);
      }
      else if (aboutCompany === '') {
        let message = "Tell us briefly about the company";
        ShowNotification(message, true);
      }
      else if (location === '') {
        let message = "Job location is required";
        ShowNotification(message, true);
      }
      else if (numberOfStaff === '') {
        let message = "Number of staff is required";
        ShowNotification(message, true);
      }
      else if (roleTitle === '') {
        let message = "Number of staff is required";
        ShowNotification(message, true);
      }
      else if (jobLevel === '') {
        let message = "Job Entry level is required";
        ShowNotification(message, true);
      }
      else if (wordForApplicant === '') {
        let message = "Tell us about the application";
        ShowNotification(message, true);
      }
      else if (job_type === '') {
        let message = "Job type required ";
        ShowNotification(message, true);
      }
      else {
        console.log(This_selectedRecruiters);
        let refererIdOne=parseInt(This_selectedRecruiters[0]);
        let refererIdTwo=parseInt(This_selectedRecruiters[1]);
        DashboardStore._ToggleProcessing(true)
        let ApiURL = null;
        if (current_id === '') {
          ApiURL = "jobs/create";
        }
        else {
          // ApiURL=`settings/profile/education/${current_id}/update`
          ApiURL = `jobs/${current_id}/update`

        }
        let params = {
          // "campany_name":companyName,
          // "industry":industry,
          // "about_company":aboutCompany,
          // "no_of_staff":parseInt(numberOfStaff),
          // "job_level":parseInt(jobLevel),
          // "role_title":roleTitle,
          // "reports_to":"Line Mgr",
          // "applicant_benefit":wordForApplicant,
          // "skills_required":JobIntrests,
          // "location":location,
          // "additional_files":additional_files,
          // "isCompanyRepresentative":isCompanyRepresentative,
          // "isToRefTimeLineOrToHireRecruter":isToRefTimeLineOrToHireRecruter

          "job_type": job_type,
          "campany_name": companyName,
          "industry": industry,
          "about_company": aboutCompany,
          "no_of_staff": parseInt(numberOfStaff),
          "job_level": parseInt(jobLevel),

          "role_title": roleTitle,
          "reports_to": "Line Mgr",
          "applicant_benefit": wordForApplicant,
          "skills_required": JobIntrests,
          "location": location,
          "additional_files": additional_files,
          "isCompanyRepresentative": isCompanyRepresentative,
          "isToRefTimeLineOrToHireRecruter": 2,
          "refererIdOne": refererIdOne,
          "refererIdTwo": refererIdTwo
        }


        console.log(params)
        console.log(ApiURL);
        let axiosInstance = DashboardStore.AxiosInstance;
        await axiosInstance.post(ApiURL, params)
          .then(response => {
            console.log(response)
            resolve(response);
          })
          .catch(error => {
            DashboardStore._ToggleProcessing(false)
            let errorDatatype = typeof error
            console.log({ error });
            console.log(errorDatatype);
            if (errorDatatype !== 'object') {
              console.log("not object")
              resolve(false);
            }
            else {

              let Response = error.response;
              console.log("it id  object")
              console.log(Response)
              resolve(Response);
            }
          });


      }


    }


  });


  GetJobOffers = (pageNumber, pagesize) => new Promise(async (resolve, reject) => {
    // let usertoken=DashboardStore.UserLoginToken;
    let axiosInstance = DashboardStore.AxiosInstance;
    // this.loader_visible = true
    await axiosInstance.get(`insiight/jobOffers/${pageNumber}/${pagesize}`)
      .then(response => {
        console.log(response)
        console.log(response)
        resolve(response);

      })
      .catch(error => {
        let errorDatatype = typeof error
        console.log({ error });
        console.log(errorDatatype);
        if (errorDatatype !== 'object') {
          console.log("not object")
          resolve(false);
        }
        else {
          let Response = error.response;
          console.log("it id  object")
          console.log(Response)
          resolve(Response);
        }
      });



  });
  GetJobApplicant = (ApplicationStagestage,pageNumber, pagesize) => new Promise(async (resolve, reject) => {
    // let BecomeRecruiter = DashboardStore.BecomeRecruiter
    const{singleJob}=DashboardStore.JobOffers;
    console.log(singleJob)
    if (Object.keys(singleJob).length === 0 && singleJob.constructor === Object) {
      // alert("Empty");
      ShowNotification("Can not proceed, please again later", true);
      resolve(false);
    }
    else {
      let axiosInstance = DashboardStore.AxiosInstance;
      let jobId =singleJob.id;
      // alert(jobId);
      // insiight / jobOffers / 1 / applicants / 1 / 10
      // insiight/jobOffers/6/applicants/*/1/10
      await axiosInstance.get(`insiight/jobOffers/${jobId}/applicants/${ApplicationStagestage}/${pageNumber}/${pagesize}`)
        .then(response => {
          console.log(response)
          console.log(response)
          resolve(response);

        })
        .catch(error => {
          let errorDatatype = typeof error
          console.log({ error });
          console.log(errorDatatype);
          if (errorDatatype !== 'object') {
            console.log("not object")
            resolve(false);
          }
          else {
            let Response = error.response;
            console.log("it id  object")
            console.log(Response)
            resolve(Response);
          }
        });

    }



  });


  UpdateApplicationLevel = () => new Promise(async (resolve, reject) => {
    let ProcessJobApplication = DashboardStore.ProcessJobApplication
    console.log(ProcessJobApplication)
    if (Object.keys(ProcessJobApplication).length === 0 && ProcessJobApplication.constructor === Object) {
      // alert("Empty");
      ShowNotification("Can not proceed, please again later", true);
    }
    else {

      let stage_code = ProcessJobApplication.stage_code
      let defaultMessage = ProcessJobApplication.defaultMessage
      let job_id = ProcessJobApplication.job_id
      let Applicant_id = ProcessJobApplication.Applicant_id
      let additional_files = ProcessJobApplication.additional_files
       

      DashboardStore._ToggleProcessing(true)
      let ApiURL = null;
      ApiURL = `insiight/jobOffers/${job_id}/stage/${Applicant_id}/create`;

      let params = {
     "stage_code":stage_code,
  "custom_message":defaultMessage,
  "additional_files":additional_files

      }


      console.log(params)
      console.log(ApiURL);
      let axiosInstance = DashboardStore.AxiosInstance;
      await axiosInstance.post(ApiURL, params)
        .then(response => {
          console.log(response)
          resolve(response);
        })
        .catch(error => {
          DashboardStore._ToggleProcessing(false)
          let errorDatatype = typeof error
          console.log({ error });
          console.log(errorDatatype);
          if (errorDatatype !== 'object') {
            console.log("not object")
            resolve(false);
          }
          else {

            let Response = error.response;
            console.log("it id  object")
            console.log(Response)
            resolve(Response);
          }
        });


    }




  });
  RejectApplication = () => new Promise(async (resolve, reject) => {
    let ProcessJobApplication = DashboardStore.ProcessJobApplication
    console.log(ProcessJobApplication)
    if (Object.keys(ProcessJobApplication).length === 0 && ProcessJobApplication.constructor === Object) {
      // alert("Empty");
      ShowNotification("Can not proceed, please again later", true);
    }
    else {

      let stage_code = ProcessJobApplication.stage_code
      let defaultMessage = ProcessJobApplication.defaultMessage
      let job_id = ProcessJobApplication.job_id
      let Applicant_id = ProcessJobApplication.Applicant_id
      // let additional_files = ProcessJobApplication.additional_files
       

      DashboardStore._ToggleProcessing(true)
      let ApiURL = null;
      // ApiURL = `insiight/jobOffers/${job_id}/stage/${Applicant_id}/create`;
      ApiURL = `insiight/jobOffers/${job_id}/applicant/${Applicant_id}/reject`;
      // {{DEV}}api/v1/insiight/jobOffers/18/applicant/71/reject
      let params = {
     "stage_code":stage_code,
  "custom_message":defaultMessage,
  // "additional_files":additional_files
      }


      console.log(params)
      console.log(ApiURL);
      let axiosInstance = DashboardStore.AxiosInstance;
      await axiosInstance.post(ApiURL, params)
        .then(response => {
          console.log(response)
          resolve(response);
        })
        .catch(error => {
          DashboardStore._ToggleProcessing(false)
          let errorDatatype = typeof error
          console.log({ error });
          console.log(errorDatatype);
          if (errorDatatype !== 'object') {
            console.log("not object")
            resolve(false);
          }
          else {

            let Response = error.response;
            console.log("it id  object")
            console.log(Response)
            resolve(Response);
          }
        });


    }




  });
  GetReferalStatistics = () => new Promise(async (resolve, reject) => {
    const{ReferalStatistics}=DashboardStore;
    console.log(ReferalStatistics)
    if (Object.keys(ReferalStatistics).length === 0 && ReferalStatistics.constructor === Object) {
      // alert("Empty");
      ShowNotification("Can not proceed, please again later", true);
      resolve(false);
    }
    else {
      let axiosInstance = DashboardStore.AxiosInstance;
      await axiosInstance.get(`insiight/referrals/stats`)
        .then(response => {
          console.log(response)
          console.log(response)
          resolve(response);

        })
        .catch(error => {
          let errorDatatype = typeof error
          console.log({ error });
          console.log(errorDatatype);
          if (errorDatatype !== 'object') {
            console.log("not object")
            resolve(false);
          }
          else {
            let Response = error.response;
            console.log("it id  object")
            console.log(Response)
            resolve(Response);
          }
        });

    }



  });
  GetMyrefarals = (pageNumber, pagesize) => new Promise(async (resolve, reject) => {
    const{ReferalStatistics}=DashboardStore;
    console.log(ReferalStatistics)
    if (Object.keys(ReferalStatistics).length === 0 && ReferalStatistics.constructor === Object) {
      // alert("Empty");
      ShowNotification("Can not proceed, please again later", true);
      resolve(false);
    }
    else {
      let axiosInstance = DashboardStore.AxiosInstance;
       await axiosInstance.get(`insiight/referrals/${pageNumber}/${pagesize}`)
        .then(response => {
          console.log(response)
          console.log(response)
          resolve(response);

        })
        .catch(error => {
          let errorDatatype = typeof error
          console.log({ error });
          console.log(errorDatatype);
          if (errorDatatype !== 'object') {
            console.log("not object")
            resolve(false);
          }
          else {
            let Response = error.response;
            console.log("it id  object")
            console.log(Response)
            resolve(Response);
          }
        });

    }



  });
  GetApplicationStatistics = () => new Promise(async (resolve, reject) => {
    const{ApplicationsStatistics}=DashboardStore;
    console.log(ApplicationsStatistics)
    if (Object.keys(ApplicationsStatistics).length === 0 && ApplicationsStatistics.constructor === Object) {
      // alert("Empty");
      ShowNotification("Can not proceed, please again later", true);
      resolve(false);
    }
    else {
      let axiosInstance = DashboardStore.AxiosInstance;
      await axiosInstance.get(`insiight/applications/stats`)
        .then(response => {
          console.log(response)
          console.log(response)
          resolve(response);

        })
        .catch(error => {
          let errorDatatype = typeof error
          console.log({ error });
          console.log(errorDatatype);
          if (errorDatatype !== 'object') {
            console.log("not object")
            resolve(false);
          }
          else {
            let Response = error.response;
            console.log("it id  object")
            console.log(Response)
            resolve(Response);
          }
        });

    }



  });
  GetMyApplicationList = (pageNumber, pagesize) => new Promise(async (resolve, reject) => {
    const{MyApplications}=DashboardStore;
    console.log(MyApplications)
    if (Object.keys(MyApplications).length === 0 && MyApplications.constructor === Object) {
      // alert("Empty");
      ShowNotification("Can not proceed, please again later", true);
      resolve(false);
    }
    else {
      let axiosInstance = DashboardStore.AxiosInstance;
       await axiosInstance.get(`insiight/applications/${pageNumber}/${pagesize}`)
        .then(response => {
          console.log(response)
          console.log(response)
          resolve(response);

        })
        .catch(error => {
          let errorDatatype = typeof error
          console.log({ error });
          console.log(errorDatatype);
          if (errorDatatype !== 'object') {
            console.log("not object")
            resolve(false);
          }
          else {
            let Response = error.response;
            console.log("it id  object")
            console.log(Response)
            resolve(Response);
          }
        });

    }



  });
  UpdateNotificationSettings = (payload) => new Promise(async (resolve, reject) => {
    // let ProcessJobApplication = DashboardStore.ProcessJobApplication
    console.log(payload)
    if (Object.keys(payload).length === 0 && payload.constructor === Object) {
      // alert("Empty");
      ShowNotification("Can not proceed, please again later", true);
    }
    else {
 
      DashboardStore._ToggleProcessing(true)
      let ApiURL = null;
      ApiURL = `settings/profile/notification/update`;

      let params =  payload


      console.log(params)
      console.log(ApiURL);
      let axiosInstance = DashboardStore.AxiosInstance;
      await axiosInstance.post(ApiURL, params)
        .then(response => {
          console.log(response)
          resolve(response);
        })
        .catch(error => {
          DashboardStore._ToggleProcessing(false)
          let errorDatatype = typeof error
          console.log({ error });
          console.log(errorDatatype);
          if (errorDatatype !== 'object') {
            console.log("not object")
            resolve(false);
          }
          else {

            let Response = error.response;
            console.log("it id  object")
            console.log(Response)
            resolve(Response);
          }
        });


    }




  });
  ChangePassword = ( ) => new Promise(async (resolve, reject) => {
    let ChangePassword = DashboardStore.ChangePassword
    console.log(ChangePassword)
    if (Object.keys(ChangePassword).length === 0 && ChangePassword.constructor === Object) {
      // alert("Empty");
      ShowNotification("Can not proceed, please again later", true);
    }
    else {
 let currentpassword=ChangePassword.currentpassword
 let newpassword=ChangePassword.newpassword
 let confirmpasssword=ChangePassword.confirmpasssword
 if(currentpassword===""){
  ShowNotification("Current Password is required", true);
return;
 }
 if(newpassword==="") {
  ShowNotification("New Password is required", true);
return ;
 }
 if(confirmpasssword==="") {
  ShowNotification("New Password is required", true);
return ;
 }
 if(confirmpasssword !== newpassword) {
  ShowNotification("Password not match", true);
return ;
 }
      DashboardStore._ToggleProcessing(true)
      let ApiURL = null;
      ApiURL = `user/changePassword`;

      let params =  {
        current_password:currentpassword,
        new_password:newpassword,
        confirm_new_password:confirmpasssword
      }


      console.log(params)
      console.log(ApiURL);
      let axiosInstance = DashboardStore.AxiosInstance;
      await axiosInstance.post(ApiURL, params)
        .then(response => {
          console.log(response)
          resolve(response);
        })
        .catch(error => {
          DashboardStore._ToggleProcessing(false)
          let errorDatatype = typeof error
          console.log({ error });
          console.log(errorDatatype);
          if (errorDatatype !== 'object') {
            console.log("not object")
            resolve(false);
          }
          else {

            let Response = error.response;
            console.log("it id  object")
            console.log(Response)
            resolve(Response);
          }
        });


    }




  });
  GetNotificationSettings = () => new Promise(async (resolve, reject) => {
    const{ReferalStatistics}=DashboardStore;
    console.log(ReferalStatistics)
    if (Object.keys(ReferalStatistics).length === 0 && ReferalStatistics.constructor === Object) {
      // alert("Empty");
      ShowNotification("Can not proceed, please again later", true);
      resolve(false);
    }
    else {
      let axiosInstance = DashboardStore.AxiosInstance;
       await axiosInstance.get(`settings/profile/notification`)
        .then(response => {
          console.log(response)
          console.log(response)
          resolve(response);

        })
        .catch(error => {
          let errorDatatype = typeof error
          console.log({ error });
          console.log(errorDatatype);
          if (errorDatatype !== 'object') {
            console.log("not object")
            resolve(false);
          }
          else {
            let Response = error.response;
            console.log("it id  object")
            console.log(Response)
            resolve(Response);
          }
        });

    }



  });
  GetOtherProfile = () => new Promise(async (resolve, reject) => {
    const{OtherProfile}=DashboardStore;
    console.log(OtherProfile)
    if (Object.keys(OtherProfile).length === 0 && OtherProfile.constructor === Object) {
      // alert("Empty");
      ShowNotification("Can not proceed, please again later", true);
      resolve(false);
    }
    else {
      let  UserId=OtherProfile.User_id;
      if(UserId===""){
        ShowNotification("User id not found ", true);

        return false;
      }
      this.LogProfileVisit(UserId);
      let axiosInstance = DashboardStore.AxiosInstance;
       await axiosInstance.get(`settings/profile/${UserId}/others`)
        .then(response => {
          console.log(response)
          console.log(response)
          resolve(response);

        })
        .catch(error => {
          let errorDatatype = typeof error
          console.log({ error });
          console.log(errorDatatype);
          if (errorDatatype !== 'object') {
            console.log("not object")
            resolve(false);
          }
          else {
            let Response = error.response;
            console.log("it id  object")
            console.log(Response)
            resolve(Response);
          }
        });

    }



  });
  GetWbLeaguePointAggregation = () => new Promise(async (resolve, reject) => {
    const{WbLeague}=DashboardStore;
    console.log(WbLeague)
    if (Object.keys(WbLeague).length === 0 && WbLeague.constructor === Object) {
      // alert("Empty");
      ShowNotification("Can not proceed, please again later", true);
      resolve(false);
    }
    else {
      // let  UserId=OtherProfile.User_id;
      // if(UserId===""){
      //   ShowNotification("User id not found ", true);

      //   return false;
      // }
      let axiosInstance = DashboardStore.AxiosInstance;
       await axiosInstance.get(`wbLeague/pointAggregation`)
        .then(response => {
          console.log(response)
          console.log(response)
          resolve(response);

        })
        .catch(error => {
          let errorDatatype = typeof error
          console.log({ error });
          console.log(errorDatatype);
          if (errorDatatype !== 'object') {
            console.log("not object")
            resolve(false);
          }
          else {
            let Response = error.response;
            console.log("it id  object")
            console.log(Response)
            resolve(Response);
          }
        });

    }



  });
  GetWbLeaguemyPosition = () => new Promise(async (resolve, reject) => {
    const{WbLeague}=DashboardStore;
    console.log(WbLeague)
    if (Object.keys(WbLeague).length === 0 && WbLeague.constructor === Object) {
      // alert("Empty");
      ShowNotification("Can not proceed, please again later", true);
      resolve(false);
    }
    else {
      // let  UserId=OtherProfile.User_id;
      // if(UserId===""){
      //   ShowNotification("User id not found ", true);

      //   return false;
      // }
      let axiosInstance = DashboardStore.AxiosInstance;
       await axiosInstance.get(`wbLeague/myPosition`)
        .then(response => {
          console.log(response)
          console.log(response)
          resolve(response);

        })
        .catch(error => {
          let errorDatatype = typeof error
          console.log({ error });
          console.log(errorDatatype);
          if (errorDatatype !== 'object') {
            console.log("not object")
            resolve(false);
          }
          else {
            let Response = error.response;
            console.log("it id  object")
            console.log(Response)
            resolve(Response);
          }
        });

    }



  });
  GetWbLeague_List = () => new Promise(async (resolve, reject) => {
    const{WbLeague}=DashboardStore;
    console.log(WbLeague)
    if (Object.keys(WbLeague).length === 0 && WbLeague.constructor === Object) {
      // alert("Empty");
      ShowNotification("Can not proceed, please again later", true);
      resolve(false);
    }
    else {
 
      let axiosInstance = DashboardStore.AxiosInstance;
       await axiosInstance.get(`wbLeague`)
        .then(response => {
          console.log(response)
          console.log(response)
          resolve(response);

        })
        .catch(error => {
          let errorDatatype = typeof error
          console.log({ error });
          console.log(errorDatatype);
          if (errorDatatype !== 'object') {
            console.log("not object")
            resolve(false);
          }
          else {
            let Response = error.response;
            console.log("it id  object")
            console.log(Response)
            resolve(Response);
          }
        });

    }



  });

  WithDrawMyApplication = () => new Promise(async (resolve, reject) => {
    let WithdrawApplication = DashboardStore.WithdrawApplication
    console.log(WithdrawApplication)
    if (Object.keys(WithdrawApplication).length === 0 && WithdrawApplication.constructor === Object) {
      // alert("Empty");
      ShowNotification("Can not proceed, please again later", true);
    }
    else {
    
      let message = WithdrawApplication.message
      let job_id = WithdrawApplication.job_id
   
       

      DashboardStore._ToggleProcessing(true)
      let ApiURL = null;
     ApiURL = `insiight/applications/${job_id}/wthdraw`;
      // {{DEV}}api/v1/insiight/jobOffers/18/applicant/71/reject
      let params = {
     "message":message,
       }


      console.log(params)
      console.log(ApiURL);
      let axiosInstance = DashboardStore.AxiosInstance;
      await axiosInstance.post(ApiURL, params)
        .then(response => {
          console.log(response)
          resolve(response);
        })
        .catch(error => {
          DashboardStore._ToggleProcessing(false)
          let errorDatatype = typeof error
          console.log({ error });
          console.log(errorDatatype);
          if (errorDatatype !== 'object') {
            console.log("not object")
            resolve(false);
          }
          else {

            let Response = error.response;
            console.log("it id  object")
            console.log(Response)
            resolve(Response);
          }
        });


    }




  });

  GetCountry_List = () => new Promise(async (resolve, reject) => {
    const{WbLeague}=DashboardStore;
    console.log(WbLeague)
    if (Object.keys(WbLeague).length === 0 && WbLeague.constructor === Object) {
      // alert("Empty");
      ShowNotification("Can not proceed, please again later", true);
      resolve(false);
    }
    else {
 
      let axiosInstance = DashboardStore.AxiosInstance;
       await axiosInstance.get(`country`)
        .then(response => {
          console.log(response)
          console.log(response)
          resolve(response);

        })
        .catch(error => {
          let errorDatatype = typeof error
          console.log({ error });
          console.log(errorDatatype);
          if (errorDatatype !== 'object') {
            console.log("not object")
            resolve(false);
          }
          else {
            let Response = error.response;
            console.log("it id  object")
            console.log(Response)
            resolve(Response);
          }
        });

    }



  });
  GetStateInCountry_List = (countryCode) => new Promise(async (resolve, reject) => {
    const{WbLeague}=DashboardStore;
    console.log(WbLeague)
    if (Object.keys(WbLeague).length === 0 && WbLeague.constructor === Object) {
      // alert("Empty");
      ShowNotification("Can not proceed, please again later", true);
      resolve(false);
    }
    else {
 
      let axiosInstance = DashboardStore.AxiosInstance;
       await axiosInstance.get(`country/${countryCode}/states`)
        .then(response => {
          console.log(response)
          console.log(response)
          resolve(response);

        })
        .catch(error => {
          let errorDatatype = typeof error
          console.log({ error });
          console.log(errorDatatype);
          if (errorDatatype !== 'object') {
            console.log("not object")
            resolve(false);
          }
          else {
            let Response = error.response;
            console.log("it id  object")
            console.log(Response)
            resolve(Response);
          }
        });

    }



  });
  GetMyInviteList = (pageNumber,pagesize) => new Promise(async (resolve, reject) => {
    const{InviteList}=DashboardStore;
    console.log(InviteList)
    if (Object.keys(InviteList).length === 0 && InviteList.constructor === Object) {
      // alert("Empty");
      ShowNotification("Can not proceed, please again later", true);
      resolve(false);
    }
    else {
 
      let axiosInstance = DashboardStore.AxiosInstance;
       await axiosInstance.get(`invitees/${pageNumber}/${pagesize}`)
        .then(response => {
          console.log(response)
          console.log(response)
          resolve(response);

        })
        .catch(error => {
          let errorDatatype = typeof error
          console.log({ error });
          console.log(errorDatatype);
          if (errorDatatype !== 'object') {
            console.log("not object")
            resolve(false);
          }
          else {
            let Response = error.response;
            console.log("it id  object")
            console.log(Response)
            resolve(Response);
          }
        });

    }



  });
  SearchMyInviteList = (query) => new Promise(async (resolve, reject) => {
    const{InviteList}=DashboardStore;
    console.log(InviteList)
    if (Object.keys(InviteList).length === 0 && InviteList.constructor === Object) {
      // alert("Empty");
      ShowNotification("Can not proceed, please again later", true);
      resolve(false);
    }
    else {
 
      let axiosInstance = DashboardStore.AxiosInstance;
       await axiosInstance.get(`invitees/${query}/search`)
        .then(response => {
          console.log(response)
          console.log(response)
          resolve(response);

        })
        .catch(error => {
          let errorDatatype = typeof error
          console.log({ error });
          console.log(errorDatatype);
          if (errorDatatype !== 'object') {
            console.log("not object")
            resolve(false);
          }
          else {
            let Response = error.response;
            console.log("it id  object")
            console.log(Response)
            resolve(Response);
          }
        });

    }



  });
  ChangeApplicatView = (job_id,Applicant_id) => new Promise(async (resolve, reject) => {
    // let WithdrawApplication = DashboardStore.WithdrawApplication
    // console.log(WithdrawApplication)
    // if (Object.keys(WithdrawApplication).length === 0 && WithdrawApplication.constructor === Object) {
    //   // alert("Empty");
    //   ShowNotification("Can not proceed, please again later", true);
    // }
    // else {
      // DashboardStore._ToggleProcessing(true)
      let ApiURL = null;
      ApiURL = `insiight/jobOffers/${job_id}/applicant/${Applicant_id}/view`;
      // {{DEV}}api/v1/insiight/jobOffers/18/applicant/71/reject
      let params = {
    //  "message":message,
       }


      console.log(params)
      console.log(ApiURL);
      let axiosInstance = DashboardStore.AxiosInstance;
      await axiosInstance.post(ApiURL, params)
        .then(response => {
          console.log(response)
          resolve(response);
        })
        .catch(error => {
          DashboardStore._ToggleProcessing(false)
          let errorDatatype = typeof error
          console.log({ error });
          console.log(errorDatatype);
          if (errorDatatype !== 'object') {
            console.log("not object")
            resolve(false);
          }
          else {

            let Response = error.response;
            console.log("it id  object")
            console.log(Response)
            resolve(Response);
          }
        });


    // }




  });
  ViewMyInvitesLogs = (pageNumber,pagesize) => new Promise(async (resolve, reject) => {
    const{SingleInvite}=DashboardStore.InviteList;
    console.log(InviteList)
    if (Object.keys(SingleInvite).length === 0 && SingleInvite.constructor === Object) {
      // alert("Empty");
      ShowNotification("Can not proceed, please again later", true);
      resolve(false);
    }
    else {
 let user_id=SingleInvite.user_id;
      let axiosInstance = DashboardStore.AxiosInstance;
       await axiosInstance.get(`invitees/activityLog/${user_id}/${pageNumber}/${pagesize}`)
        .then(response => {
          console.log(response)
          console.log(response)
          resolve(response);

        })
        .catch(error => {
          let errorDatatype = typeof error
          console.log({ error });
          console.log(errorDatatype);
          if (errorDatatype !== 'object') {
            console.log("not object")
            resolve(false);
          }
          else {
            let Response = error.response;
            console.log("it id  object")
            console.log(Response)
            resolve(Response);
          }
        });

    }



  });
  GetReruitersRequest = (pageNumber,pagesize) => new Promise(async (resolve, reject) => {
    const{RecruiterDashBoard}=DashboardStore;
    console.log(RecruiterDashBoard)
    if (Object.keys(RecruiterDashBoard).length === 0 && RecruiterDashBoard.constructor === Object) {
      // alert("Empty");
      ShowNotification("Can not proceed, please again later", true);
      resolve(false);
    }
    else {
//  let user_id=SingleInvite.user_id;
      let axiosInstance = DashboardStore.AxiosInstance;
       await axiosInstance.get(`recruiter/jobs/${pageNumber}/${pagesize}`)
        .then(response => {
          console.log(response)
          console.log(response)
          resolve(response);

        })
        .catch(error => {
          let errorDatatype = typeof error
          console.log({ error });
          console.log(errorDatatype);
          if (errorDatatype !== 'object') {
            console.log("not object")
            resolve(false);
          }
          else {
            let Response = error.response;
            console.log("it id  object")
            console.log(Response)
            resolve(Response);
          }
        });

    }



  });

  AcceptOrDeclinedJobOfferNow = (Job_status) => new Promise(async (resolve, reject) => {
    let SingleRequest = DashboardStore.RecruiterDashBoard.SingleRequest
    console.log(SingleRequest)
    if (Object.keys(SingleRequest).length === 0 && SingleRequest.constructor === Object) {
      // alert("Empty");
      ShowNotification("Can not proceed, please again later", true);
    }

    else {

      let job_id = SingleRequest.job_id
       
      if (job_id === '') {
        ShowNotification('Job id is missing', true);
      }
      else if (Job_status === '') {
        ShowNotification('Job Status is missing', true);

      }
  
      else {
        DashboardStore._ToggleProcessing(true)
        let ApiURL = null;
        // if (current_id === '') {
          ApiURL = "recruiter/jobs/acceptOrDecline";
        // }
        // else {
        //   ApiURL = `settings/profile/education/${current_id}/update`

        // }
        let params = {
          "job_id":parseInt(job_id),
          "acceptOrDecline":parseInt(Job_status)
        }


        console.log(params)
        console.log(ApiURL);
        let axiosInstance = DashboardStore.AxiosInstance;
        await axiosInstance.post(ApiURL, params)
          .then(response => {
            console.log(response)
            resolve(response);
          })
          .catch(error => {
            DashboardStore._ToggleProcessing(false)
            let errorDatatype = typeof error
            console.log({ error });
            console.log(errorDatatype);
            if (errorDatatype !== 'object') {
              console.log("not object")
              resolve(false);
            }
            else {

              let Response = error.response;
              console.log("it id  object")
              console.log(Response)
              resolve(Response);
            }
          });


      }


    }


  });
  GetJobRecruiters = (pageNumber, pagesize) => new Promise(async (resolve, reject) => {
    // let BecomeRecruiter = DashboardStore.BecomeRecruiter
    const{singleJob}=DashboardStore.JobOffers;
    console.log(singleJob)
    if (Object.keys(singleJob).length === 0 && singleJob.constructor === Object) {
      // alert("Empty");
      ShowNotification("Can not proceed, please again later", true);
      resolve(false);
    }
    else {
      let axiosInstance = DashboardStore.AxiosInstance;
      let jobId =singleJob.id;
          await axiosInstance.get(`insiight/jobOffers/${jobId}/recruiters/${pageNumber}/${pagesize}`)
        .then(response => {
          console.log(response)
          console.log(response)
          resolve(response);

        })
        .catch(error => {
          let errorDatatype = typeof error
          console.log({ error });
          console.log(errorDatatype);
          if (errorDatatype !== 'object') {
            console.log("not object")
            resolve(false);
          }
          else {
            let Response = error.response;
            console.log("it id  object")
            console.log(Response)
            resolve(Response);
          }
        });

    }



  });
  HireMoreRecruiter = () => new Promise(async (resolve, reject) => {
    let singleJob = DashboardStore.JobOffers.singleJob
    console.log(singleJob);
 
    if (Object.keys(singleJob).length === 0 && singleJob.constructor === Object) {
      // alert("Empty");
      ShowNotification("Can not proceed, please again later", true);
    }

    else {
      let This_selectedRecruiters = DashboardStore.JobPosting.SelectedRecuiter.slice();

      let job_id = singleJob.id
       
      if (job_id === '') {
        ShowNotification('Job id is missing', true);
      }
      else if (This_selectedRecruiters.length < 2) {
        let message = "Please select 2 recruiters";
        ShowNotification(message, true);
      }
  
      else {
        let refererIdOne=parseInt(This_selectedRecruiters[0]);
        let refererIdTwo=parseInt(This_selectedRecruiters[1]);
        DashboardStore._ToggleProcessing(true)
        let ApiURL = null;
            ApiURL = `jobs/${job_id}/jobToRecruiter`;
      
        let params = {
          "refererIdOne": refererIdOne,
          "refererIdTwo": refererIdTwo
        }


        console.log(params)
        console.log(ApiURL);
        let axiosInstance = DashboardStore.AxiosInstance;
        await axiosInstance.post(ApiURL, params)
          .then(response => {
            console.log(response)
            resolve(response);
          })
          .catch(error => {
            DashboardStore._ToggleProcessing(false)
            let errorDatatype = typeof error
            console.log({ error });
            console.log(errorDatatype);
            if (errorDatatype !== 'object') {
              console.log("not object")
              resolve(false);
            }
            else {

              let Response = error.response;
              console.log("it id  object")
              console.log(Response)
              resolve(Response);
            }
          });


      }


    }


  });
  GetReruitersInsight = (pageNumber,pagesize) => new Promise(async (resolve, reject) => {
    const{RecruiterDashBoard}=DashboardStore;
    console.log(RecruiterDashBoard)
    if (Object.keys(RecruiterDashBoard).length === 0 && RecruiterDashBoard.constructor === Object) {
      // alert("Empty");
      ShowNotification("Can not proceed, please again later", true);
      resolve(false);
    }
    else {
//  let user_id=SingleInvite.user_id;
      let axiosInstance = DashboardStore.AxiosInstance;
      // recruiter/insight/1/3
       await axiosInstance.get(`recruiter/insight/${pageNumber}/${pagesize}`)
        .then(response => {
          console.log(response)
          console.log(response)
          resolve(response);

        })
        .catch(error => {
          let errorDatatype = typeof error
          console.log({ error });
          console.log(errorDatatype);
          if (errorDatatype !== 'object') {
            console.log("not object")
            resolve(false);
          }
          else {
            let Response = error.response;
            console.log("it id  object")
            console.log(Response)
            resolve(Response);
          }
        });

    }



  });
  GetShortlistedApplicant = (pageNumber,pagesize) => new Promise(async (resolve, reject) => {
    // let BecomeRecruiter = DashboardStore.BecomeRecruiter
    const{SingleAcceptedJob}=DashboardStore.RecruiterInsight;
    console.log(SingleAcceptedJob)
    console.log("SingleAcceptedJob")
    if (Object.keys(SingleAcceptedJob).length === 0 && SingleAcceptedJob.constructor === Object) {
      // alert("Empty");
      ShowNotification("Can not proceed, please again later", false);
      resolve(false);
    }
    else {
      let axiosInstance = DashboardStore.AxiosInstance;
      let jobId =SingleAcceptedJob.job_id;
 
           await axiosInstance.get(`recruiter/insight/${jobId}/applicants/shortlisted/${pageNumber}/${pagesize}`)
        .then(response => {
          console.log(response)
          console.log(response)
          resolve(response);

        })
        .catch(error => {
          let errorDatatype = typeof error
          console.log({ error });
          console.log(errorDatatype);
          if (errorDatatype !== 'object') {
            console.log("not object")
            resolve(false);
          }
          else {
            let Response = error.response;
            console.log("it id  object")
            console.log(Response)
            resolve(Response);
          }
        });

    }



  });
  ShortListOrDeclineApplicant = (job_id,user_id) => new Promise(async (resolve, reject) => {
    let SingleRequest = DashboardStore.RecruiterDashBoard.SingleRequest
    console.log(SingleRequest)
    if (job_id=='' ||  user_id=='') {
      // alert("Empty");
      ShowNotification("Can not proceed, please again later", true);
    }

    else {

      // let job_id = SingleRequest.job_id
       
      if (job_id === '') {
        ShowNotification('Job id is missing', true);
      }
      else if (user_id === '') {
        ShowNotification('User id    is missing', true);

      }
  
      else {
        DashboardStore._ToggleProcessing(true)
        let ApiURL = null;
        // if (current_id === '') {
          ApiURL = `recruiter/insight/${job_id}/applicants/${user_id}/shortlist`;
        // }
        // else {
        //   ApiURL = `settings/profile/education/${current_id}/update`

        // }
        let params = {
          "job_id":parseInt(job_id),
          "acceptOrDecline":parseInt(0)
        }


        console.log(params)
        console.log(ApiURL);
        let axiosInstance = DashboardStore.AxiosInstance;
        await axiosInstance.post(ApiURL, params)
          .then(response => {
            console.log(response)
            resolve(response);
          })
          .catch(error => {
            DashboardStore._ToggleProcessing(false)
            let errorDatatype = typeof error
            console.log({ error });
            console.log(errorDatatype);
            if (errorDatatype !== 'object') {
              console.log("not object")
              resolve(false);
            }
            else {

              let Response = error.response;
              console.log("it id  object")
              console.log(Response)
              resolve(Response);
            }
          });


      }


    }


  });
  SendShortlistedApplicant = () => new Promise(async (resolve, reject) => {
    let SingleAcceptedJob = DashboardStore.RecruiterInsight.SingleAcceptedJob
    console.log(SingleAcceptedJob)
    let job_id = SingleAcceptedJob.job_id

    if (job_id=='' ||  job_id==null) {
      // alert("Empty");
      ShowNotification("Can not proceed, please again later", true);
    }

    else {

      // let job_id = SingleAcceptedJob.job_id
       
      if (job_id === '') {
        ShowNotification('Job id is missing', true);
      }
      
  
      else {
        DashboardStore._ToggleProcessing(true)
        let ApiURL = null;
        // if (current_id === '') {
           ApiURL = `recruiter/insight/${job_id}/applicants/shortlisted/send`;
        // }
        // else {
        //   ApiURL = `settings/profile/education/${current_id}/update`

        // }
        let params = {
          // "job_id":parseInt(job_id),
          // "acceptOrDecline":parseInt(0)
        }


        console.log(params)
        console.log(ApiURL);
        let axiosInstance = DashboardStore.AxiosInstance;
        await axiosInstance.post(ApiURL, params)
          .then(response => {
            console.log(response)
            resolve(response);
          })
          .catch(error => {
            DashboardStore._ToggleProcessing(false)
            let errorDatatype = typeof error
            console.log({ error });
            console.log(errorDatatype);
            if (errorDatatype !== 'object') {
              console.log("not object")
              resolve(false);
            }
            else {

              let Response = error.response;
              console.log("it id  object")
              console.log(Response)
              resolve(Response);
            }
          });


      }


    }


  });
  GetSavedJobs = (pageNumber,pagesize) => new Promise(async (resolve, reject) => {
     const{SingleAcceptedJob}=DashboardStore.RecruiterInsight;
    console.log(SingleAcceptedJob)
    if (pageNumber==='' || pagesize==='') {
       ShowNotification("Can not proceed, please again later", false);
      resolve(false);
    }
    else {
      let axiosInstance = DashboardStore.AxiosInstance;   
        // await axiosInstance.get(`jobs/application/${pageNumber}/${pagesize}`)
        await axiosInstance.get(`jobs/saved/${pageNumber}/${pagesize}`)
        .then(response => {
          console.log(response)
          console.log(response)
          resolve(response);

        })
        .catch(error => {
          let errorDatatype = typeof error
          console.log({ error });
          console.log(errorDatatype);
          if (errorDatatype !== 'object') {
            console.log("not object")
            resolve(false);
          }
          else {
            let Response = error.response;
            console.log("it id  object")
            console.log(Response)
            resolve(Response);
          }
        });

    }



  });

  deleteEducationManagement = () => new Promise(async (resolve, reject) => {
    let EducationManagement = DashboardStore.EducationManagement
    console.log(EducationManagement)
    if (Object.keys(EducationManagement).length === 0 && EducationManagement.constructor === Object) {
      // alert("Empty");
      ShowNotification("Can not proceed, please again later", true);
    }

    else {

      // let school = EducationManagement.school
      // let degree = EducationManagement.degree
      // let field_of_study = EducationManagement.field_of_study
      // let start_date = EducationManagement.start_date
      // let end_date = EducationManagement.end_date
      // let description = EducationManagement.description
      let current_id = EducationManagement.current_id
      if (current_id === '') {
        ShowNotification('Id is required', true);
      }
      else {
        DashboardStore._ToggleProcessing(true)
        let ApiURL = null;
        ApiURL = `settings/profile/education/${current_id}/delete`
        let params = {
     
        }


        console.log(params)
        console.log(ApiURL);
        let axiosInstance = DashboardStore.AxiosInstance;
        await axiosInstance.post(ApiURL, params)
          .then(response => {
            console.log(response)
            resolve(response);
          })
          .catch(error => {
            DashboardStore._ToggleProcessing(false)
            let errorDatatype = typeof error
            console.log({ error });
            console.log(errorDatatype);
            if (errorDatatype !== 'object') {
              console.log("not object")
              resolve(false);
            }
            else {

              let Response = error.response;
              console.log("it id  object")
              console.log(Response)
              resolve(Response);
            }
          });


      }


    }


  });
  deleteSkillManagement = () => new Promise(async (resolve, reject) => {
    let SkillManagement = DashboardStore.SkillManagement
    console.log(SkillManagement)
    if (Object.keys(SkillManagement).length === 0 && SkillManagement.constructor === Object) {
      // alert("Empty");
      ShowNotification("Can not proceed, please again later", true);
    }

    else {
 
      let current_id = SkillManagement.current_id
      if (current_id === '') {
        ShowNotification('Field id is required ', true);
      }
      
      else {
        DashboardStore._ToggleProcessing(true)
        let ApiURL = null;
        ApiURL = `settings/profile/experience/${current_id}/delete`

        let params = {
        
        }


        console.log(params)
        console.log(ApiURL);
        let axiosInstance = DashboardStore.AxiosInstance;
        await axiosInstance.post(ApiURL, params)
          .then(response => {
            console.log(response)
            resolve(response);
          })
          .catch(error => {
            DashboardStore._ToggleProcessing(false)
            let errorDatatype = typeof error
            console.log({ error });
            console.log(errorDatatype);
            if (errorDatatype !== 'object') {
              console.log("not object")
              resolve(false);
            }
            else {

              let Response = error.response;
              console.log("it id  object")
              console.log(Response)
              resolve(Response);
            }
          });


      }


    }


  });
  GetNotification = (pageNumber,pagesize) => new Promise(async (resolve, reject) => {
    const{SingleAcceptedJob}=DashboardStore.RecruiterInsight;
   console.log(SingleAcceptedJob)
   if (pageNumber==='' || pagesize==='') {
      ShowNotification("Can not proceed, please again later", false);
     resolve(false);
   }
   else {
     let axiosInstance = DashboardStore.AxiosInstance;   
       // await axiosInstance.get(`jobs/application/${pageNumber}/${pagesize}`)
       await axiosInstance.get(`settings/profile/notification/log/${pageNumber}/${pagesize}`)
        .then(response => {
         console.log(response)
         console.log(response)
         resolve(response);

       })
       .catch(error => {
         let errorDatatype = typeof error
         console.log({ error });
         console.log(errorDatatype);
         if (errorDatatype !== 'object') {
           console.log("not object")
           resolve(false);
         }
         else {
           let Response = error.response;
           console.log("it id  object")
           console.log(Response)
           resolve(Response);
         }
       });

   }



 });
 GetMyRecruiterProfile = () => new Promise(async (resolve, reject) => {
  //   const{SingleAcceptedJob}=DashboardStore.RecruiterInsight;
  //  console.log(SingleAcceptedJob)
  //  if (pageNumber==='' || pagesize==='') {
  //     ShowNotification("Can not proceed, please again later", false);
  //    resolve(false);
  //  }
  //  else {
     let axiosInstance = DashboardStore.AxiosInstance;   
       // await axiosInstance.get(`jobs/application/${pageNumber}/${pagesize}`)
       await axiosInstance.get(`recruiter/own`)
        .then(response => {
         console.log(response)
         console.log(response)
         resolve(response);

       })
       .catch(error => {
         let errorDatatype = typeof error
         console.log({ error });
         console.log(errorDatatype);
         if (errorDatatype !== 'object') {
           console.log("not object")
           resolve(false);
         }
         else {
           let Response = error.response;
           console.log("it id  object")
           console.log(Response)
           resolve(Response);
         }
       });

  //  }



 });
 UpdateRecruiterProfile = () => new Promise(async (resolve, reject) => {
  // let ExistingProfile = DashboardStore.profileupdatedata
  // console.log(ExistingProfile)
  // const { RecruiterProfile } = DashboardStore.MyRecruiterProfile;
  let RecruiterProfile=DashboardStore.RecruiterProfile;
  console.log(RecruiterProfile)

  if (Object.keys(RecruiterProfile).length === 0 && RecruiterProfile.constructor === Object) {
    // alert("Empty");
    ShowNotification("Can not proceed, please again later", true);
  }

  else {
    let fullname = RecruiterProfile.fullname
    let email = RecruiterProfile.email
    // let gender=parseInt(RecruiterProfile.gender)
    let phone = RecruiterProfile.phone ;
    let summary = RecruiterProfile.summary
    let coordinate = RecruiterProfile.coordinate
    let industry = RecruiterProfile.industry
    let location = RecruiterProfile.location
    let industryRecruitementExperience = RecruiterProfile.industryRecruitementExperience
    let linkdnUrl = RecruiterProfile.linkdnUrl
    let additionalFiles = RecruiterProfile.additionalFiles
    let profilePicture = RecruiterProfile.profilePicture
   // let isShowOnProfile = parseInt(ExistingProfile.isShowOnProfile)
    // let about = ExistingProfile.about
    // let state = ExistingProfile.state
    // let country = ExistingProfile.country
    // let location = `${state} , ${country} `

      DashboardStore._ToggleProcessing(true)
    let params = {
      "fullname":fullname,
      "email":email,
      "phone":phone,
      "summary":summary,
      "coordinate":coordinate,
      "industry":industry,
      "location":location,
      "industryRecruitementExperience":industryRecruitementExperience,
      "linkdnUrl":linkdnUrl,
      "additionalFiles":additionalFiles,
      "profilePicture":profilePicture,

    }
    console.log(params)
    console.log('recruiter/update');
    let axiosInstance = DashboardStore.AxiosInstance;
    await axiosInstance.post('recruiter/update', params)
      .then(response => {
        console.log(response)
        resolve(response);
      })
      .catch(error => {
        let errorDatatype = typeof error
        console.log({ error });
        console.log(errorDatatype);
        if (errorDatatype !== 'object') {
          console.log("not object")
          resolve(false);
        }
        else {

          let Response = error.response;
          console.log("it id  object")
          console.log(Response)
          resolve(Response);
        }
      });



  }


});
ProccessJobPosting2Recruiters_update = () => new Promise(async (resolve, reject) => {
  let JobPosting = DashboardStore.JobPosting
  console.log(JobPosting)
  if (Object.keys(JobPosting).length === 0 && JobPosting.constructor === Object) {
    // alert("Empty");
    ShowNotification("Can not proceed, please again later", true);
  }

  else {
    // job_type
    // const{JobPosting}=DashboardStore
    let current_id = JobPosting.current_id;
    let companyName = JobPosting.companyName; //
    let industry = JobPosting.industry;//
    let aboutCompany = JobPosting.aboutCompany; //
    let location = JobPosting.location;
    let numberOfStaff = JobPosting.numberOfStaff; //
    let roleTitle = JobPosting.roleTitle;//
    let jobLevel = JobPosting.jobLevel; //
    let wordForApplicant = JobPosting.wordForApplicant;
    let JobIntrests = JobPosting.intrest;
    let isCompanyRepresentative = JobPosting.isCompanyRepresentative;
    let isToRefTimeLineOrToHireRecruter = JobPosting.isToRefTimeLineOrToHireRecruter;
    let additional_files = JobPosting.additional_files;
    let job_type =  JobPosting.job_type; 

    // let SelectedRecuiter = JobPosting.SelectedRecuiter;
    // let This_selectedRecruiters = DashboardStore.JobPosting.SelectedRecuiter.slice();

    //  {
    //   "campany_name":"Eko Laundty 4",
    //   "industry":"Building",
    //   "about_company":"Goog Company",
    //   "no_of_staff":10,
    //   "job_level":1,
    //   "role_title":"Developer",

    //   "reports_to":"Line Mgr",

    //   "applicant_benefit":"story",
    //   "skills_required":["Law","Coding 3"],
    //   "location":"Loagos",
    //   "additional_files":["Link1","Link1","Link1"],
    //   "isCompanyRepresentative":1,
    //   "isToRefTimeLineOrToHireRecruter":1,
    //     "refererIdOne":4,
    //     "refererIdTwo":3

    // }

    if (additional_files.length < 1) {
      let message = "Job Banner missing";
      ShowNotification(message, true);
    }
    else if (companyName === '') {
      let message = "Company name is required";
      ShowNotification(message, true);
    }
    else if (industry === '') {
      let message = "Company  industry is required";
      ShowNotification(message, true);
    }
    else if (JobIntrests.length < 1) {
      let message = "Select 1 or More Skills required (Intrest) for the job";
      ShowNotification(message, true);
    }
    // else if (This_selectedRecruiters.length < 2) {
    //   let message = "Please select 2 recruiters";
    //   ShowNotification(message, true);
    // }
    else if (aboutCompany === '') {
      let message = "Tell us briefly about the company";
      ShowNotification(message, true);
    }
    else if (location === '') {
      let message = "Job location is required";
      ShowNotification(message, true);
    }
    else if (numberOfStaff === '') {
      let message = "Number of staff is required";
      ShowNotification(message, true);
    }
    else if (roleTitle === '') {
      let message = "Number of staff is required";
      ShowNotification(message, true);
    }
    else if (jobLevel === '') {
      let message = "Job Entry level is required";
      ShowNotification(message, true);
    }
    else if (wordForApplicant === '') {
      let message = "Tell us about the application";
      ShowNotification(message, true);
    }
    else if (job_type === '') {
      let message = "Job type required ";
      ShowNotification(message, true);
    }
    else {
      // console.log(This_selectedRecruiters);
      // let refererIdOne=parseInt(This_selectedRecruiters[0]);
      // let refererIdTwo=parseInt(This_selectedRecruiters[1]);
      DashboardStore._ToggleProcessing(true)
      let ApiURL = null;
      if (current_id === '') {
        ApiURL = "jobs/create";
      }
      else {
        // ApiURL=`settings/profile/education/${current_id}/update`
        ApiURL = `jobs/${current_id}/update`

      }
      let params = {
        // "campany_name":companyName,
        // "industry":industry,
        // "about_company":aboutCompany,
        // "no_of_staff":parseInt(numberOfStaff),
        // "job_level":parseInt(jobLevel),
        // "role_title":roleTitle,
        // "reports_to":"Line Mgr",
        // "applicant_benefit":wordForApplicant,
        // "skills_required":JobIntrests,
        // "location":location,
        // "additional_files":additional_files,
        // "isCompanyRepresentative":isCompanyRepresentative,
        // "isToRefTimeLineOrToHireRecruter":isToRefTimeLineOrToHireRecruter
        "job_type": job_type,
        "campany_name": companyName,
        "industry": industry,
        "about_company": aboutCompany,
        "no_of_staff": parseInt(numberOfStaff),
        "job_level": parseInt(jobLevel),

        "role_title": roleTitle,
        "reports_to": "Line Mgr",
        "applicant_benefit": wordForApplicant,
        "skills_required": JobIntrests,
        "location": location,
        "additional_files": additional_files,
        "isCompanyRepresentative": parseInt(isCompanyRepresentative),
        "isToRefTimeLineOrToHireRecruter": 2,
        // "refererIdOne": refererIdOne,
        // "refererIdTwo": refererIdTwo
      }


      console.log(params)
      console.log(ApiURL);
      let axiosInstance = DashboardStore.AxiosInstance;
      await axiosInstance.post(ApiURL, params)
        .then(response => {
          console.log(response)
          resolve(response);
        })
        .catch(error => {
          DashboardStore._ToggleProcessing(false)
          let errorDatatype = typeof error
          console.log({ error });
          console.log(errorDatatype);
          if (errorDatatype !== 'object') {
            console.log("not object")
            resolve(false);
          }
          else {

            let Response = error.response;
            console.log("it id  object")
            console.log(Response)
            resolve(Response);
          }
        });


    }


  }


});
CloseJobOffer = () => new Promise(async (resolve, reject) => {
  let JobPosting = DashboardStore.JobPosting
  console.log(JobPosting)
  if (Object.keys(JobPosting).length === 0 && JobPosting.constructor === Object) {
    // alert("Empty");
    ShowNotification("Can not proceed, please again later", true);
  }

  else {

    // const{JobPosting}=DashboardStore
    let current_id = JobPosting.current_id;
     
    if (current_id ==="") {
      let message = "Job id missing";
      ShowNotification(message, true);
    }
    else {
      ShowNotification("Processing please wait ...", true);

   let params={};
      DashboardStore._ToggleProcessing(true)
      let ApiURL = null;
      ApiURL = `jobs/${current_id}/delete`
       console.log(ApiURL);
      let axiosInstance = DashboardStore.AxiosInstance;
      await axiosInstance.post(ApiURL, params)
        .then(response => {
          console.log(response)
          resolve(response);
        })
        .catch(error => {
          DashboardStore._ToggleProcessing(false)
          let errorDatatype = typeof error
          console.log({ error });
          console.log(errorDatatype);
          if (errorDatatype !== 'object') {
            console.log("not object")
            resolve(false);
          }
          else {
            let Response = error.response;
            console.log("it id  object")
            console.log(Response)
            resolve(Response);
          }
        });
    }
  }
});




GetMyCurrentProfile = () => new Promise(async (resolve, reject) => {
  // let usertoken=DashboardStore.UserLoginToken;
  let axiosInstance = DashboardStore.AxiosInstance;
  // this.loader_visible = true
  await axiosInstance.get(`settings/profile`)
    .then(response => {
      console.log(response)
      console.log(response)
      resolve(response);

    })
    .catch(error => {
      let errorDatatype = typeof error
      console.log({ error });
      console.log(errorDatatype);
      if (errorDatatype !== 'object') {
        console.log("not object")
        resolve(false);
      }
      else {
        let Response = error.response;
        console.log("it id  object")
        console.log(Response)
        resolve(Response);
      }
    });



});
GetJobLikes = (pageNumber, pagesize) => new Promise(async (resolve, reject) => {
   let axiosInstance = DashboardStore.AxiosInstance;
  let JoblikeObject = DashboardStore.JoblikeObject
  console.log(JoblikeObject)
  if (Object.keys(JoblikeObject).length === 0 && JoblikeObject.constructor === Object) {
    // alert("Empty");
    ShowNotification("Can not proceed, please again later", true);
    return false;
  }
  let jobId=JoblikeObject.JobId;
  await axiosInstance.get(`jobs/like/${jobId}/${pageNumber}/${pagesize}`)
    .then(response => {
      console.log(response)
      console.log(response)
      resolve(response);

    })
    .catch(error => {
      let errorDatatype = typeof error
      console.log({ error });
      console.log(errorDatatype);
      if (errorDatatype !== 'object') {
        console.log("not object")
        resolve(false);
      }
      else {
        let Response = error.response;
        console.log("it id  object")
        console.log(Response)
        resolve(Response);
      }
    });



});
GetJobSuggestionsBasedOnSkill = (pageNumber, pagesize) => new Promise(async (resolve, reject) => {
   let axiosInstance = DashboardStore.AxiosInstance;
  let JobSeggestions = DashboardStore.JobSeggestions
  // console.log(JobSeggestions)
  if (Object.keys(JobSeggestions).length === 0 && JobSeggestions.constructor === Object) {
    // alert("Empty");
    ShowNotification("Can not proceed, please again later", true);
    return false;
  }
  // let jobId=JoblikeObject.JobId;
  await axiosInstance.get(`jobs/suggesstions/${pageNumber}/${pagesize}`)
    .then(response => {
      console.log(response)
      console.log(response)
      resolve(response);

    })
    .catch(error => {
      let errorDatatype = typeof error
      console.log({ error });
      console.log(errorDatatype);
      if (errorDatatype !== 'object') {
        console.log("not object")
        resolve(false);
      }
      else {
        let Response = error.response;
        console.log("it id  object")
        console.log(Response)
        resolve(Response);
      }
    });



});
AddCardToSystem = (StripeToken) => new Promise(async (resolve, reject) => {
   if (StripeToken==="") {
    ShowNotification("Card token missing");
  }
  else {
    DashboardStore._ToggleProcessing(true)
    let params = {
    	stripeToken:StripeToken

    }
    console.log(params)
    console.log('wallet/card/create');
    let axiosInstance = DashboardStore.AxiosInstance;
     await axiosInstance.post('wallet/card/create', params)
      .then(response => {
        console.log(response)
        resolve(response);
      })
      .catch(error => {
        let errorDatatype = typeof error
        console.log({ error });
        console.log(errorDatatype);
        if (errorDatatype !== 'object') {
          console.log("not object")
          resolve(false);
        }
        else {

          let Response = error.response;
          console.log("it id  object")
          console.log(Response)
          resolve(Response);
        }
      });



  }


});
GetMycardList = (pageNumber, pagesize) => new Promise(async (resolve, reject) => {
  let axiosInstance = DashboardStore.AxiosInstance;
  let CardsInSystem = DashboardStore.CardsInSystem
  if (Object.keys(CardsInSystem).length === 0 && CardsInSystem.constructor === Object) {
   // alert("Empty");
   ShowNotification("Can not proceed, please again later", true);
   return false;
 }
 // let jobId=JoblikeObject.JobId; wallet/card/1/10
//  await axiosInstance.get(`wallet/account/${pageNumber}/${pagesize}`)
 await axiosInstance.get(`wallet/card/${pageNumber}/${pagesize}`)
   .then(response => {
     console.log(response)
     console.log(response)
     resolve(response);

   })
   .catch(error => {
     let errorDatatype = typeof error
     console.log({ error });
     console.log(errorDatatype);
     if (errorDatatype !== 'object') {
       console.log("not object")
       resolve(false);
     }
     else {
       let Response = error.response;
       console.log("it id  object")
       console.log(Response)
       resolve(Response);
     }
   });



});
 
GetTransactionHistory = (pageNumber, pagesize) => new Promise(async (resolve, reject) => {
  let axiosInstance = DashboardStore.AxiosInstance;
 let TransactionHistory = DashboardStore.TransactionInHistory
  if (Object.keys(TransactionHistory).length === 0 && TransactionHistory.constructor === Object) {
    ShowNotification("Can not proceed, please again later", true);
   return false;
 }
 // let jobId=JoblikeObject.JobId;wallet/payment
 await axiosInstance.get(`wallet/payment/${pageNumber}/${pagesize}`)
   .then(response => {
     console.log(response)
     console.log(response)
     resolve(response);

   })
   .catch(error => {
     let errorDatatype = typeof error
     console.log({ error });
     console.log(errorDatatype);
     if (errorDatatype !== 'object') {
       console.log("not object")
       resolve(false);
     }
     else {
       let Response = error.response;
       console.log("it id  object")
       console.log(Response)
       resolve(Response);
     }
   });



});
GetAccountBalance = () => new Promise(async (resolve, reject) => {
  let axiosInstance = DashboardStore.AxiosInstance;
 let WalletBalance = DashboardStore.WalletBalance
  if (Object.keys(WalletBalance).length === 0 && WalletBalance.constructor === Object) {
    ShowNotification("Can not proceed, please again later", true);
   return false;
 }
 // let jobId=JoblikeObject.JobId;
 await axiosInstance.get(`/wallet/balance`)
   .then(response => {
     console.log(response)
     console.log(response)
     resolve(response);

   })
   .catch(error => {
     let errorDatatype = typeof error
     console.log({ error });
     console.log(errorDatatype);
     if (errorDatatype !== 'object') {
       console.log("not object")
       resolve(false);
     }
     else {
       let Response = error.response;
       console.log("it id  object")
       console.log(Response)
       resolve(Response);
     }
   });



});
ProcessAddFundtoAccount = () => new Promise(async (resolve, reject) => {
  let FundWallet = DashboardStore.FundWallet
  console.log(FundWallet)
  if (Object.keys(FundWallet).length === 0 && FundWallet.constructor === Object) {
    // alert("Empty");
    ShowNotification("Can not proceed, please again later", true);
  }

  else {
    let amount = FundWallet.amount;
    let card_id = FundWallet.card_id;
 

    if (amount < 5) {
      let message = "Amount can not be less than $5";
      ShowNotification(message, true);
    }
    
    else {
 
      DashboardStore._ToggleProcessing(true)
      let ApiURL = null;
      ApiURL = `wallet/payment/fundWallet`

      let params = {
        "amount":parseFloat(amount),
        "card_id":parseInt(card_id)
        
      }


      console.log(params)
      console.log(ApiURL);
      let axiosInstance = DashboardStore.AxiosInstance;
      await axiosInstance.post(ApiURL, params)
        .then(response => {
          console.log(response)
          resolve(response);
        })
        .catch(error => {
          DashboardStore._ToggleProcessing(false)
          let errorDatatype = typeof error
          console.log({ error });
          console.log(errorDatatype);
          if (errorDatatype !== 'object') {
            console.log("not object")
            resolve(false);
          }
          else {

            let Response = error.response;
            console.log("it id  object")
            console.log(Response)
            resolve(Response);
          }
        });


    }


  }


});
SetcardAsDefault = (card_id) => new Promise(async (resolve, reject) => {
   console.log(card_id)
  if (card_id==="") {
    // alert("Empty");
    ShowNotification("Can not proceed, please again later", true);
  }

  else {
  
 
      DashboardStore._ToggleProcessing(true)
      let ApiURL = null;
      ApiURL = `wallet/card/setDefault/${card_id}`

      let params = {
        // "amount":parseFloat(amount),
        // "card_id":parseInt(card_id)
        
      }


      console.log(params)
      console.log(ApiURL);
      let axiosInstance = DashboardStore.AxiosInstance;
      await axiosInstance.post(ApiURL, params)
        .then(response => {
          console.log(response)
          resolve(response);
        })
        .catch(error => {
          DashboardStore._ToggleProcessing(false)
          let errorDatatype = typeof error
          console.log({ error });
          console.log(errorDatatype);
          if (errorDatatype !== 'object') {
            console.log("not object")
            resolve(false);
          }
          else {

            let Response = error.response;
            console.log("it id  object")
            console.log(Response)
            resolve(Response);
          }
        });


 

  }


});
GetSubscriptionConfig = (pageNumber, pagesize) => new Promise(async (resolve, reject) => {
  let axiosInstance = DashboardStore.AxiosInstance;
 let SubscriptionConfig = DashboardStore.SubscriptionConfig
  if (Object.keys(SubscriptionConfig).length === 0 && SubscriptionConfig.constructor === Object) {
    ShowNotification("Can not proceed, please again later", true);
   return false;
 }
 // let jobId=JoblikeObject.JobId;
 await axiosInstance.get(`subscription/config/${pageNumber}/${pagesize}`)
   .then(response => {
     console.log(response)
     console.log(response)
     resolve(response);

   })
   .catch(error => {
     let errorDatatype = typeof error
     console.log({ error });
     console.log(errorDatatype);
     if (errorDatatype !== 'object') {
       console.log("not object")
       resolve(false);
     }
     else {
       let Response = error.response;
       console.log("it id  object")
       console.log(Response)
       resolve(Response);
     }
   });



});

SubscribeWithWallet = () => new Promise(async (resolve, reject) => {
const{SingleSubConfig} = DashboardStore.SubscriptionConfig
  console.log(SingleSubConfig)
  if (Object.keys(SingleSubConfig).length === 0 && SingleSubConfig.constructor === Object) {
    // alert("Empty");
    ShowNotification("Can not proceed, please again later", true);
  }

  else {
    let subscription_config_id = SingleSubConfig.id;
    // let card_id = SingleSubConfig.card_id;
 

    if (subscription_config_id==='' ) {
      let message = "Some Data missing...";
      ShowNotification(message, true);
    }
    
    else {
 
      DashboardStore._ToggleProcessing(true)
      let ApiURL = null;
      ApiURL = `subscription/create`

      let params = {
        "subscription_config_id":parseInt(subscription_config_id),
        "isPaymentFromWallet":1,
        "card_id":0
        
      }


      console.log(params)
      console.log(ApiURL);
      let axiosInstance = DashboardStore.AxiosInstance;
      await axiosInstance.post(ApiURL, params)
        .then(response => {
          console.log(response)
          resolve(response);
        })
        .catch(error => {
          DashboardStore._ToggleProcessing(false)
          let errorDatatype = typeof error
          console.log({ error });
          console.log(errorDatatype);
          if (errorDatatype !== 'object') {
            console.log("not object")
            resolve(false);
          }
          else {

            let Response = error.response;
            console.log("it id  object")
            console.log(Response)
            resolve(Response);
          }
        });


    }


  }


});
SubscribeWithcard = (Card_id) => new Promise(async (resolve, reject) => {
const{SingleSubConfig} = DashboardStore.SubscriptionConfig
  console.log(SingleSubConfig)
  if (Object.keys(SingleSubConfig).length === 0 && SingleSubConfig.constructor === Object) {
    // alert("Empty");
    ShowNotification("Can not proceed, please again later", true);
  }

  else {
    let subscription_config_id = SingleSubConfig.id;
    // let card_id = SingleSubConfig.card_id;
 

    if (Card_id==='' ) {
      let message = "Card Missing ";
      ShowNotification(message, true);
    }
    
    else {
 
      DashboardStore._ToggleProcessing(true)
      let ApiURL = null;
      ApiURL = `subscription/create`

      let params = {
        "subscription_config_id":parseInt(subscription_config_id),
        "isPaymentFromWallet":0,
        "card_id":parseInt(Card_id)
        
      }


      console.log(params)
      console.log(ApiURL);
      let axiosInstance = DashboardStore.AxiosInstance;
      await axiosInstance.post(ApiURL, params)
        .then(response => {
          console.log(response)
          resolve(response);
        })
        .catch(error => {
          DashboardStore._ToggleProcessing(false)
          let errorDatatype = typeof error
          console.log({ error });
          console.log(errorDatatype);
          if (errorDatatype !== 'object') {
            console.log("not object")
            resolve(false);
          }
          else {

            let Response = error.response;
            console.log("it id  object")
            console.log(Response)
            resolve(Response);
          }
        });


    }


  }


});
GetActiveSubscription = (pageNumber, pagesize) => new Promise(async (resolve, reject) => {
  let axiosInstance = DashboardStore.AxiosInstance;
 let Subscribtions = DashboardStore.Subscribtions
  if (Object.keys(Subscribtions).length === 0 && Subscribtions.constructor === Object) {
    ShowNotification("Can not proceed, please again later", true);
   return false;
 }
 // let jobId=JoblikeObject.JobId;
 await axiosInstance.get(`subscription/${pageNumber}/${pagesize}`)
   .then(response => {
     console.log(response)
     console.log(response)
     resolve(response);

   })
   .catch(error => {
     let errorDatatype = typeof error
     console.log({ error });
     console.log(errorDatatype);
     if (errorDatatype !== 'object') {
       console.log("not object")
       resolve(false);
     }
     else {
       let Response = error.response;
       console.log("it id  object")
       console.log(Response)
       resolve(Response);
     }
   });



});
LogProfileVisit = (User_id) => new Promise(async (resolve, reject) => {
   if (User_id==='') {
    // alert("Empty");
    ShowNotification("Can not proceed, please again later", true);
    resolve(false);
  }
  else {
    console.log("log profile visit ")
    let axiosInstance = DashboardStore.AxiosInstance;
     await axiosInstance.post(`insiight/applications/${User_id}/profileVisit`)
      .then(response => {
        console.log(response)
        console.log(response)
        resolve(response);

      })
      .catch(error => {
        let errorDatatype = typeof error
        console.log({ error });
        console.log(errorDatatype);
        if (errorDatatype !== 'object') {
          console.log("not object")
          resolve(false);
        }
        else {
          let Response = error.response;
          console.log("it id  object")
          console.log(Response)
          resolve(Response);
        }
      });

  }



});
SearchRecuiterAvailable = (query) => new Promise(async (resolve, reject) => {
  let FilterRecruiter = DashboardStore.FilterRecruiter
  console.log(FilterRecruiter)
  if (query=='') {
     ShowNotification("Can not proceed, please again later", true);
  }
  else {
    DashboardStore._ToggleProcessing(false)
     let CurrentJobId =   DashboardStore.Recruiters.CurrentJobId;

    let ApiURL = `recruiter/0/${query}/${CurrentJobId}/search`
    console.log(ApiURL);
    let axiosInstance = DashboardStore.AxiosInstance;
    await axiosInstance.get(ApiURL)
      .then(response => {
        console.log(response)
        resolve(response);
      })
      .catch(error => {
        DashboardStore._ToggleProcessing(false)
        let errorDatatype = typeof error
        console.log({ error });
        console.log(errorDatatype);
        if (errorDatatype !== 'object') {
          console.log("not object")
          resolve(false);
        }
        else {

          let Response = error.response;
          console.log("it id  object")
          console.log(Response)
          resolve(Response);
        }
      });


    // }


  }


});

SearchAllAppUsers = (showloader) => new Promise(async (resolve, reject) => {
  let AppUsersList = DashboardStore.AppUsersList
  console.log(AppUsersList)
  if (Object.keys(AppUsersList).length === 0 && AppUsersList.constructor === Object) {
    ShowNotification("Can not proceed, please again later", true);
   return false;
 }
  else {
    DashboardStore._ToggleProcessing(showloader)
     let queryString =   DashboardStore.AppUsersList.queryString;
    
    let ApiURL = `/insiight/jobOffers/stage/${queryString}/usersList`
    console.log(ApiURL);
    let axiosInstance = DashboardStore.AxiosInstance;
    await axiosInstance.get(ApiURL)
      .then(response => {
        console.log(response)
        resolve(response);
      })
      .catch(error => {
        DashboardStore._ToggleProcessing(false)
        let errorDatatype = typeof error
        console.log({ error });
        console.log(errorDatatype);
        if (errorDatatype !== 'object') {
          console.log("not object")
          resolve(false);
        }
        else {

          let Response = error.response;
          console.log("it id  object")
          console.log(Response)
          resolve(Response);
        }
      });


    // }


  }


});
SendShortListToUser = () => new Promise(async (resolve, reject) => {
  const{Shortlistdata} = DashboardStore.AppUsersList
    console.log(Shortlistdata)
    if (Object.keys(Shortlistdata).length === 0 && Shortlistdata.constructor === Object) {
      // alert("Empty");
      ShowNotification("Can not proceed, please again later", true);
    }
  
    else {
     
   
        DashboardStore._ToggleProcessing(true)
        let ApiURL = null;
        ApiURL = `insiight/jobOffers/sendShortlist`
  
        let params = {
       
          ...Shortlistdata
        }
        // let jj = { ...Shortlistdata, ...newvalue }

  
        console.log(params)
        console.log(ApiURL);
        let axiosInstance = DashboardStore.AxiosInstance;
        await axiosInstance.post(ApiURL, params)
          .then(response => {
            console.log(response)
            resolve(response);
          })
          .catch(error => {
            DashboardStore._ToggleProcessing(false)
            let errorDatatype = typeof error
            console.log({ error });
            console.log(errorDatatype);
            if (errorDatatype !== 'object') {
              console.log("not object")
              resolve(false);
            }
            else {
  
              let Response = error.response;
              console.log("it id  object")
              console.log(Response)
              resolve(Response);
            }
          });
  
  
   
  
    }
  
  
  });
GetRecieversJobs = (pageNumber, pagesize) => new Promise(async (resolve, reject) => {
  let axiosInstance = DashboardStore.AxiosInstance;
 let SharedJobs = DashboardStore.MySharedJobs
  if (Object.keys(SharedJobs).length === 0 && SharedJobs.constructor === Object) {
    ShowNotification("Can not proceed, please again later", true);
   return false;
 }
 DashboardStore._ToggleProcessing(true)

 await axiosInstance.get(`insiight/jobOffers/recieversJobs/${pageNumber}/${pagesize}`)
   .then(response => {
     console.log(response)
     console.log(response)
     resolve(response);

   })
   .catch(error => {
     let errorDatatype = typeof error
     console.log({ error });
     console.log(errorDatatype);
     if (errorDatatype !== 'object') {
       console.log("not object")
       resolve(false);
     }
     else {
       let Response = error.response;
       console.log("it id  object")
       console.log(Response)
       resolve(Response);
     }
   });



});
GetSharedApplicantList = (Joblevel,pageNumber, pagesize) => new Promise(async (resolve, reject) => {
  let axiosInstance = DashboardStore.AxiosInstance;
//  let  = DashboardStore.MySingleSharedJob
 const{SingleSharedJob}=DashboardStore.MySharedJobs;
  if (Object.keys(SingleSharedJob).length === 0 && SingleSharedJob.constructor === Object) {
    ShowNotification("Can not proceed, please again later", true);
   return false;
 }
 let jobId=SingleSharedJob.id;
 DashboardStore._ToggleProcessing(true)

 await axiosInstance.get(`insiight/jobOffers/recieversJobsApplicant/${jobId}/${Joblevel}/${pageNumber}/${pagesize}`)
   .then(response => {
     console.log(response)
     console.log(response)
     resolve(response);

   })
   .catch(error => {
     let errorDatatype = typeof error
     console.log({ error });
     console.log(errorDatatype);
     if (errorDatatype !== 'object') {
       console.log("not object")
       resolve(false);
     }
     else {
       let Response = error.response;
       console.log("it id  object")
       console.log(Response)
       resolve(Response);
     }
   });



});

SuggestStageToMoveApplicant = () => new Promise(async (resolve, reject) => {
  const{SendPayload} = DashboardStore.SharedApplicant;
    console.log(SendPayload)
    if (Object.keys(SendPayload).length === 0 && SendPayload.constructor === Object) {
      // alert("Empty");
      ShowNotification("Can not proceed, please again later", true);
    }
  
    else {
     
   
        DashboardStore._ToggleProcessing(true);
        let ApiURL = null;
        ApiURL = `insiight/jobOffers/recieversMoveApplicantAction`
  
        let params = {
       
          ...SendPayload
        }
        // let jj = { ...Shortlistdata, ...newvalue }

  
        console.log(params)
        console.log(ApiURL);
        let axiosInstance = DashboardStore.AxiosInstance;
        await axiosInstance.post(ApiURL, params)
          .then(response => {
            console.log(response)
            resolve(response);
          })
          .catch(error => {
            DashboardStore._ToggleProcessing(false)
            let errorDatatype = typeof error
            console.log({ error });
            console.log(errorDatatype);
            if (errorDatatype !== 'object') {
              console.log("not object")
              resolve(false);
            }
            else {
  
              let Response = error.response;
              console.log("it id  object")
              console.log(Response)
              resolve(Response);
            }
          });
  
  
   
  
    }
  
  
  });

  AcceptOrRejectList = (acceptOrDecline) => new Promise(async (resolve, reject) => {
    const{SingleAcceptedJob}=DashboardStore.RecruiterInsight;
    console.log(SingleAcceptedJob)
    console.log("SingleAcceptedJob")
    if (Object.keys(SingleAcceptedJob).length === 0 && SingleAcceptedJob.constructor === Object) {
      // alert("Empty");
      ShowNotification("Can not proceed, please again later", false);
      resolve(false);
    }
    else {
      ShowNotification("Processing....", true);

      let axiosInstance = DashboardStore.AxiosInstance;
      let jobId =SingleAcceptedJob.job_id;

      // let job_id = SingleRequest.job_id
       
      if (acceptOrDecline === '') {
        ShowNotification('Action is required', true);
      }
      else {
        DashboardStore._ToggleProcessing(true)
        let ApiURL = null;
           ApiURL = `recruiter/insight/applicants/acceptOrRejectShortlist`;
      
        let params = {
          "job_id":parseInt(jobId),
          "acceptOrDecline":parseInt(acceptOrDecline)

        }


        console.log(params)
        console.log(ApiURL);
         await axiosInstance.post(ApiURL, params)
          .then(response => {
            console.log(response)
            resolve(response);
          })
          .catch(error => {
            DashboardStore._ToggleProcessing(false)
            let errorDatatype = typeof error
            console.log({ error });
            console.log(errorDatatype);
            if (errorDatatype !== 'object') {
              console.log("not object")
              resolve(false);
            }
            else {

              let Response = error.response;
              console.log("it id  object")
              console.log(Response)
              resolve(Response);
            }
          });


      }


    }


  });
  SubmitRateAndReview  = (data) => new Promise(async (resolve, reject) => {
    const{SingleAcceptedJob}=DashboardStore.RecruiterInsight;
    console.log(SingleAcceptedJob)
    console.log("SingleAcceptedJob")
    if (Object.keys(SingleAcceptedJob).length === 0 && SingleAcceptedJob.constructor === Object) {
      // alert("Empty");
      ShowNotification("Can not proceed, please again later", false);
      resolve(false);
    }
    else {
      ShowNotification("Processing....", true);

      let axiosInstance = DashboardStore.AxiosInstance;
      let jobId =SingleAcceptedJob.job_id;
 
  
        DashboardStore._ToggleProcessing(true)
        let ApiURL = null;
           ApiURL = `recruiter/insight/analytics/writeReview`;
      
        let params = {
        
          "recruiter_id":parseInt(2),
          "job_id":parseInt(jobId),
          "star_rating":parseInt(data.star_rating),
          "review_message":data.review_message
// ...data
        }


        console.log(params)
        console.log(ApiURL);
         await axiosInstance.post(ApiURL, params)
          .then(response => {
            console.log(response)
            resolve(response);
          })
          .catch(error => {
            DashboardStore._ToggleProcessing(false)
            let errorDatatype = typeof error
            console.log({ error });
            console.log(errorDatatype);
            if (errorDatatype !== 'object') {
              console.log("not object")
              resolve(false);
            }
            else {

              let Response = error.response;
              console.log("it id  object")
              console.log(Response)
              resolve(Response);
            }
          });


 

    }


  });
  GetActivityTracker = (UserId) => new Promise(async (resolve, reject) => {
    let axiosInstance = DashboardStore.AxiosInstance;
  //  let  = DashboardStore.MySingleSharedJob
   const{MyAnalytics}=DashboardStore;
    if (Object.keys(MyAnalytics).length === 0 && MyAnalytics.constructor === Object) {
      console.log("Can not proceed, please again later");
      ShowNotification("Can not proceed, please again later", true);
     return false;
   }
  //  let jobId=SingleSharedJob.id;
  //  DashboardStore._ToggleProcessing(true)
  
   await axiosInstance.get(`recruiter/insight/analytics/totalRequestAndAcceptance/${UserId}`)
     .then(response => {
       console.log(response)
       console.log(response)
       resolve(response);
  
     })
     .catch(error => {
       let errorDatatype = typeof error
       console.log({ error });
       console.log(errorDatatype);
       if (errorDatatype !== 'object') {
         console.log("not object")
         resolve(false);
       }
       else {
         let Response = error.response;
         console.log("it id  object")
         console.log(Response)
         resolve(Response);
       }
     });
  
  
  
  });
  GetcompletedJobsLevel = (UserId) => new Promise(async (resolve, reject) => {
    let axiosInstance = DashboardStore.AxiosInstance;
  //  let  = DashboardStore.MySingleSharedJob
   const{MyAnalytics}=DashboardStore;
    if (Object.keys(MyAnalytics).length === 0 && MyAnalytics.constructor === Object) {
      console.log("Can not proceed, please again later");
      ShowNotification("Can not proceed, please again later", true);
     return false;
   }
  //  let jobId=SingleSharedJob.id;
  //  DashboardStore._ToggleProcessing(true)
  
   await axiosInstance.get(`recruiter/insight/analytics/completedJobsLevel/${UserId}`)
     .then(response => {
       console.log(response)
       console.log(response)
       resolve(response);
  
     })
     .catch(error => {
       let errorDatatype = typeof error
       console.log({ error });
       console.log(errorDatatype);
       if (errorDatatype !== 'object') {
         console.log("not object")
         resolve(false);
       }
       else {
         let Response = error.response;
         console.log("it id  object")
         console.log(Response)
         resolve(Response);
       }
     });
  
  
  
  });
  GetshortlistAndInterviewRatio = (UserId) => new Promise(async (resolve, reject) => {
    let axiosInstance = DashboardStore.AxiosInstance;
  //  let  = DashboardStore.MySingleSharedJob
   const{MyAnalytics}=DashboardStore;
    if (Object.keys(MyAnalytics).length === 0 && MyAnalytics.constructor === Object) {
      console.log("Can not proceed, please again later");
      ShowNotification("Can not proceed, please again later", true);
     return false;
   }
  //  let jobId=SingleSharedJob.id;
  //  DashboardStore._ToggleProcessing(true)
  
   await axiosInstance.get(`recruiter/insight/analytics/shortlistAndInterviewRatio/${UserId}`)
     .then(response => {
       console.log(response)
       console.log(response)
       resolve(response);
  
     })
     .catch(error => {
       let errorDatatype = typeof error
       console.log({ error });
       console.log(errorDatatype);
       if (errorDatatype !== 'object') {
         console.log("not object")
         resolve(false);
       }
       else {
         let Response = error.response;
         console.log("it id  object")
         console.log(Response)
         resolve(Response);
       }
     });
  
  
  
  });
  SearchForJobs = (query) => new Promise(async (resolve, reject) => {
    let FilterRecruiter = DashboardStore.FilterRecruiter
    console.log(FilterRecruiter)
    if (query=='') {
       ShowNotification("Can not proceed, please again later", true);
    }
    else {
      DashboardStore._ToggleProcessing(false)
      //  let CurrentJobId =   DashboardStore.Recruiters.CurrentJobId;
  
      // let ApiURL = `recruiter/0/${query}/${CurrentJobId}/search`
      let ApiURL = `jobs/${query}/search`
      console.log(ApiURL);
      let axiosInstance = DashboardStore.AxiosInstance;
      await axiosInstance.get(ApiURL)
        .then(response => {
          console.log(response)
          resolve(response);
        })
        .catch(error => {
          DashboardStore._ToggleProcessing(false)
          let errorDatatype = typeof error
          console.log({ error });
          console.log(errorDatatype);
          if (errorDatatype !== 'object') {
            console.log("not object")
            resolve(false);
          }
          else {
  
            let Response = error.response;
            console.log("it id  object")
            console.log(Response)
            resolve(Response);
          }
        });
  
  
      // }
  
  
    }
  
  
  });
  HireApplicant = (applicant_id) => new Promise(async (resolve, reject) => {
    // const{SendPayload} = DashboardStore.SharedApplicant;
    //   console.log(SendPayload)
    const{SingleAcceptedJob}=DashboardStore.RecruiterInsight;

      if (Object.keys(SingleAcceptedJob).length === 0 && SingleAcceptedJob.constructor === Object) {
      // if (applicant_id==='') {
        // alert("Empty");
        ShowNotification("Can not proceed, please again later", true);
      }
      else {
       
        ShowNotification("Processing please wait ...", true);
          DashboardStore._ToggleProcessing(true);
          let ApiURL = null;
          ApiURL = `recruiter/insight/applicants/hireApplicant`;
          let jobId =SingleAcceptedJob.job_id;
          let params = {
            "job_id":jobId,
            "applicant_id":applicant_id
            // ...SendPayload
          }
          // let jj = { ...Shortlistdata, ...newvalue }
          console.log(params);
          console.log(ApiURL);
          let axiosInstance = DashboardStore.AxiosInstance;
          await axiosInstance.post(ApiURL, params)
            .then(response => {
              console.log(response);
              resolve(response);
            })
            .catch(error => {
              DashboardStore._ToggleProcessing(false);
              let errorDatatype = typeof error
              console.log({ error });
              console.log(errorDatatype);
              if (errorDatatype !== 'object') {
                console.log("not object")
                resolve(false);
              }
              else {
                let Response = error.response;
                console.log("it id  object")
                console.log(Response)
                resolve(Response);
              }
            });
    
    
     
    
      }
    
    
    });
    GetUserRateAndReviews = (UserId) => new Promise(async (resolve, reject) => {
      let axiosInstance = DashboardStore.AxiosInstance;
    //  let  = DashboardStore.MySingleSharedJob
     const{MyAnalytics}=DashboardStore;
      if (Object.keys(MyAnalytics).length === 0 && MyAnalytics.constructor === Object) {
        console.log("Can not proceed, please again later");
        ShowNotification("Can not proceed, please again later", true);
       return false;
     }
    //  let jobId=SingleSharedJob.id;
    //  DashboardStore._ToggleProcessing(true)
    
     await axiosInstance.get(`recruiter/insight/analytics/reviews/${UserId}`)
       .then(response => {
         console.log(response)
         console.log(response)
         resolve(response);
    
       })
       .catch(error => {
         let errorDatatype = typeof error
         console.log({ error });
         console.log(errorDatatype);
         if (errorDatatype !== 'object') {
           console.log("not object")
           resolve(false);
         }
         else {
           let Response = error.response;
           console.log("it id  object")
           console.log(Response)
           resolve(Response);
         }
       });
    
    
    
    });
    GettimeToCompleteShortlist = (UserId) => new Promise(async (resolve, reject) => {
      let axiosInstance = DashboardStore.AxiosInstance;
    //  let  = DashboardStore.MySingleSharedJob
     const{MyAnalytics}=DashboardStore;
      if (Object.keys(MyAnalytics).length === 0 && MyAnalytics.constructor === Object) {
        console.log("Can not proceed, please again later");
        ShowNotification("Can not proceed, please again later", true);
       return false;
     }
    //  let jobId=SingleSharedJob.id;
    //  DashboardStore._ToggleProcessing(true)
    
     await axiosInstance.get(`recruiter/insight/analytics/timeToCompleteShortlist/${UserId}`)
       .then(response => {
         console.log(response)
         console.log(response)
         resolve(response);
    
       })
       .catch(error => {
         let errorDatatype = typeof error
         console.log({ error });
         console.log(errorDatatype);
         if (errorDatatype !== 'object') {
           console.log("not object")
           resolve(false);
         }
         else {
           let Response = error.response;
           console.log("it id  object")
           console.log(Response)
           resolve(Response);
         }
       });
    
    
    
    });
    RecruiterComplitedByIndustry = (UserId,pageNumber, pagesize) => new Promise(async (resolve, reject) => {
      let axiosInstance = DashboardStore.AxiosInstance;
    //  let  = DashboardStore.MySingleSharedJob
     const{MyAnalytics}=DashboardStore;
      if (Object.keys(MyAnalytics).length === 0 && MyAnalytics.constructor === Object) {
        console.log("Can not proceed, please again later");
        ShowNotification("Can not proceed, please again later", true);
       return false;
     }
    //  let jobId=SingleSharedJob.id;
    //  DashboardStore._ToggleProcessing(true)
    
     await axiosInstance.get(`recruiter/insight/analytics/recruiterComplitedByIndustry/${UserId}/${pageNumber}/${pagesize}`)
       .then(response => {
         console.log(response)
         console.log(response)
         resolve(response);
    
       })
       .catch(error => {
         let errorDatatype = typeof error
         console.log({ error });
         console.log(errorDatatype);
         if (errorDatatype !== 'object') {
           console.log("not object")
           resolve(false);
         }
         else {
           let Response = error.response;
           console.log("it id  object")
           console.log(Response)
           resolve(Response);
         }
       });
    
    
    
    });
////////////////////////////////////End of valid request ////////////////////////////////////////////
  GetPersistData = () => new Promise((resolve, reject) => {

    global.storage.load({
      key: 'key',
    }).then(ret => {
      resolve(ret);

    }).catch(err => {
      // any exception including data not found  
      // goes to catch() 
      console.log(err.message);
      switch (err.name) {
        case 'NotFoundError':
          // alert("You have not register before")
          resolve("kikkk");
          break;
        case 'ExpiredError':
          // TODO 
          resolve('jhjhjhj');
          break;
      }
    });
  });






}

export default ApiConnect 
import qs from 'qs';
import axios from 'axios'
import { PersistData ,ShowNotification} from '../dependency/UtilityFunctions';


class OnboardingApi {


 

      VerifyAccount =  (token,fcm_token) => new Promise( async (resolve, reject) => {
    
          
        // ShowNotification("Fetching your fields"); 
        this.loader_visible = true
     await   axios.get(`user/activate/${token}/${fcm_token}`)
          .then(response => {
            console.log(response)
            console.log(response)
            resolve(response);
    
          })
            .catch(error => {
              let errorDatatype=typeof error
              console.log({error});
              console.log(errorDatatype);
              if(errorDatatype !=='object'){
console.log("not object")
resolve(false);
              }
              else{
                let Response=error.response;
                console.log("it id  object")
                console.log(Response)
                resolve(Response);
              }
    });
    
    
    
    });
     
    UserNormalRegistration =  (data) => new Promise( async (resolve, reject) => {
 
          console.log(data);
          ShowNotification("Please Wait...."); 
          this.loader_visible = true
       await   axios.post('user/register', data)
            .then(response => {
              console.log(response)
              resolve(response);
            })
              .catch(error => {
                let errorDatatype=typeof error
                console.log({error});
                console.log(errorDatatype);
                if(errorDatatype !=='object'){
console.log("not object")
resolve(false);
                }
                else{
                  let Response=error.response;
                  console.log("it id  object")
                  console.log(Response)
                  resolve(Response);
                }
      }); 
      });
      RegisterUserNow_SocialMedia =  (data) => new Promise( async (resolve, reject) => {
 
          console.log(data);
          ShowNotification("Please Wait...."); 
          this.loader_visible = true
       await   axios.post('user/socialMediaRegister', data)
            .then(response => {
              console.log(response)
              resolve(response);
            })
              .catch(error => {
                let errorDatatype=typeof error
                console.log({error});
                console.log(errorDatatype);
                if(errorDatatype !=='object'){
console.log("not object")
resolve(false);
                }
                else{
                  let Response=error.response;
                  console.log("it id  object")
                  console.log(Response)
                  resolve(Response);
                }
      }); 
      });
      SendResetPwdCode =  (data) => new Promise( async (resolve, reject) => {
 
          console.log(data);
          ShowNotification("Please Wait...."); 
          this.loader_visible = true
       await   axios.post('user/forgotPassword', data)
            .then(response => {
              console.log(response)
              resolve(response);
            })
              .catch(error => {
              let errorDatatype=typeof error
                console.log({error});
                console.log(errorDatatype);
                if(errorDatatype !=='object'){
console.log("not object")
resolve(false);
                }
                else{
                  let Response=error.response;
                  console.log("it id  object")
                  console.log(Response)
                  resolve(Response);
                }
        // console.warn(`error ${error}`);
        
      }); 
      });
      ResetPasswordNow =  (data) => new Promise( async (resolve, reject) => {
 
          console.log(data);
          ShowNotification("Please Wait...."); 
          this.loader_visible = true
       await   axios.post('user/resetPassword', data)
            .then(response => {
              console.log(response)
              resolve(response);
            })
              .catch(error => {
                let errorDatatype=typeof error
                console.log({error});
                console.log(errorDatatype);
                if(errorDatatype !=='object'){
console.log("not object")
resolve(false);
                }
                else{
                  let Response=error.response;
                  console.log("it id  object")
                  console.log(Response)
                  resolve(Response);
                }
      }); 
      });

    LoginUserNow =  (data) => new Promise( async (resolve, reject) => {
 
          console.log(data);
          ShowNotification("Please Wait...."); 
          this.loader_visible = true
       await   axios.post('user/login', data)
            .then(response => {
              console.log(response)
              resolve(response);
            })
              .catch(error => {
                let errorDatatype=typeof error
                console.log({error});
                console.log(errorDatatype);
                if(errorDatatype !=='object'){
console.log("not object")
resolve(false);
                }
                else{
                  let Response=error.response;
                  console.log("it id  object")
                  console.log(Response)
                  resolve(Response);
                }
      }); 
      });

////////////////////////////////


       LoadUserData =  (mobi) => new Promise( async (resolve, reject) => {
  
        if (mobi === '') {
          ShowNotification("Phone Number Empty"); 
          // Toast.show('Email is required', Toast.SHORT, Toast.BOTTOM, ToastStyle);
        }
       
    
        else {
          // request_services{service_id, session_id, sessionType_id, lattitude_log, lattitude_lat}
          let params = {
            mobile: mobi,
          
          }
          console.log(params);
          ShowNotification("Loading Your Data,Please Wait ....."); 
          this.loader_visible = true
       await   axios.post('loaddata', qs.stringify(params))
            .then(response => {
              console.log(response)
              resolve(response);
        

            })
              .catch(error => {
        // console.warn(`error ${error}`);
        // this.Categories = [];
        // this._ToggleLoader(null, false)
        resolve(false);
      });
        }
    
    
      });

      GetPersistData = () => new Promise((resolve, reject) => {
   
        global.storage.load({
            key: 'key',
          }).then(ret => {
            resolve(ret);        
      
          }).catch(err => {
            // any exception including data not found  
            // goes to catch() 
            console.warn(err.message);
            switch (err.name) {
                case 'NotFoundError':
                // alert("You have not register before")
                resolve("kikkk");
                    break;
                case 'ExpiredError':
                    // TODO 
                    resolve('jhjhjhj');
                    break;
            }
          });
      });
  
  
  
  
      // async AuthenticateUser(PhoneNumber) {
        Fetch_fields =  (field) => new Promise( async (resolve, reject) => {
      
            
            ShowNotification("Fetching your fields"); 
            this.loader_visible = true
         await   axios.get(`auth/field/${field}`)
              .then(response => {
                console.log(response)
                console.log(response)
                resolve(response);
  
              })
                .catch(error => {
          // console.warn(`error ${error}`);
          // this.Categories = [];
          // this._ToggleLoader(null, false)
          resolve(false);
        });
        
      
      
        });
  

}

export default OnboardingApi 
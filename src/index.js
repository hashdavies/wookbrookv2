import { Linking } from 'react-native'
import React, { Component } from 'react';
import { isSignedIn } from "./auth";
import { SignedOut, SignedIn,createRootNavigator } from "./router";
import {loggedinUserdetails,getLodgedInDetailsProfile, temp,dynamicEventLink} from './dependency/UtilityFunctions'
import GlobalFont from 'react-native-global-font'
import SplashScreen from 'react-native-splash-screen'
import AsyncStorage from '@react-native-community/async-storage';
 
import DashBordStore from './stores/Dashboard'
import { observer } from 'mobx-react/native'
import Stripe from 'react-native-stripe-api';
// import firebase from '@react-native-firebase/app';
import {firebase} from '@react-native-firebase/dynamic-links';
// import '@react-native-firebase/dynamic-links';
// import '@react-native-firebase/messaging';

import {
  SafeAreaView,
  StyleSheet,
  Alert
} from 'react-native';
import DashboardStore from './stores/Dashboard';
import AccountStore from './stores/Account';
// import { Provider } from 'unstated'
// export default class App extends React.Component {
//   render() {
//     return <SignedIn />;
//   }
// } 

// app/index.js

// import { isSignedIn } from "./auth";
// console.log(loggedinUserdetails());
//Generate unique user ID here
const SENDER_UID = 'USER1234';
//build the link
const link = `https://www.wookbrook.app?invitedBy=${SENDER_UID}`;
const dynamicLinkDomain = 'https://wookbrookapp.page.link';
@observer
export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      signedIn: false,
      checkedSignIn: false
    };
    this._handleOpenURL=this._handleOpenURL.bind(this);
  }
 

 

  _handleOpenURL = event => {
console.log("hhhhhhghghghghgghghghhgh")
      // dynamicLinks().onLink(url => {
      //    // Your custom logic here 
      //    conssole.log("shey man ")
      //    conssole.log(url)
      // });
      dynamicLinks()
      .getInitialLink()
      .then(link => {
        // alert("hey")
        console.log("hey")
        console.log(link);
        // if (link.url === 'https://invertase.io/offer') {
        //   // ...set initial route as offers screen
        // }
      });
  };
  async GenerateStripeToken() {
    try{
      const apiKey = 'pk_test_9Mkk2IizHw9TE3cSOzKbLxoV00xALT39G0';
      const client = new Stripe(apiKey);
      const token = await client.createToken({
        number: '4242424242424242' ,
        exp_month: '09', 
        exp_year: '21', 
        cvc: '111',
        address_zip: '12345'
      });
      console.log("Stripe Token")
      console.log(token)
      console.log("Stripe Token")
    } catch (error) {
      Alert.alert('Opps', error);
  }
  
    // Create a Stripe token with new card infos

  }
  async checkPermission() {
    //  console.warn("Checking for fcm token ")
    const enabled = await firebase.messaging().hasPermission();
    // alert(enabled+"fire permission")
    if (enabled) {
        this.getToken();
    } else {
        this.requestPermission();
    }
  }
  async  HandleDynamicLinking() {
    let url = await firebase.dynamicLinks().getInitialLink();
    console.log('incoming url', url);
// let url = await firebase.links().getInitialLink();
// firebase.links()
//     .getInitialLink()
//     .then((url) => {
//         if (url) {
//             // app opened from a url
//                 //   alert("yes url")

//             console.log('incoming url', url); //incoming url https://www.deeplinkdemo.com?invitedby=USER1234

//         } else {
          
//            // app NOT opened from a url
//                 //  alert("No url")
//                 console.log('No Url'); //incoming url https://www.deeplinkdemo.com?invitedby=USER1234


//         }
//     });

    // if (url) {
    //   alert("yes url")
    //   // const ID = this.getParameterFromUrl(url, "invitedBy");
    //   // console.log('ID', ID); //ID USER1234
    // }
    // else{
    //   alert("No url")

    // }



    // try {

    //   firebase
    //   .dynamicLinks()
    //   .getInitialLink()
    //   .then(link => {
    //     console.log("Innitial Links>>>",link)
    //     console.log("this is my link ")
    //     // do something with the link
    //   })
//     const dynamicLink = await Linking.getInitialURL()
// const url = await fetch(dynamicLink).then(res => res.url)
// // parse url here
// console.log("parsed url")
// console.log(url)
// let url = await firebase.links().getInitialLink();
//   console.log('incoming url', url);
  // let url =  firebase.links().getInitialLink();
  //   console.log('incoming url', url); //incoming url https://www.deeplinkdemo.com?invitedby=USER1234
  //   if (url) {
  //     alert("yes url")
  //     // const ID = this.getParameterFromUrl(url, "invitedBy");
  //     // console.log('ID', ID); //ID USER1234
  //   }
  //   else{
  //     alert("No url")

  //   }


//    } catch (error) {
//     // Alert.alert('Opps', error.message);
//     console.log("Catch ")
//     console.log(error)
// }
  }
  async GenerateDynamicLink() {
    // const DynamicLink = new firebase.links.DynamicLink(link, dynamicLinkDomain);
    // //get the generatedLink
    // const generatedLink = await firebase.links().createDynamicLink(DynamicLink);
    // console.log('created link', generatedLink);
    let path="signup?referralCode=foo&referralType=bar";
    let GeneratedURL=dynamicEventLink(path);
    console.log('created link', GeneratedURL);

  }
  async PrepareAxiosInstance() {
    let UserloginToken=await temp();
    // let loggedinUserdetails=await loggedinUserdetails();
    console.log(UserloginToken);
    DashboardStore._getAxiosInstance(UserloginToken);
    console.log("UserloginToken");
    DashboardStore.__GetMyCurrentProfile();

   }
   async   buildLink() {
    const link = await dynamicLinks().buildLink({
      link: 'https://invertase.io',
      // domainUriPrefix is created in your Firebase console
      domainUriPrefix: 'https://wookbrookapp.page.link',
      // optional set up which updates Firebase analytics campaign
      // "banner". This also needs setting up before hand
      analytics: {
        campaign: 'banner',
      },
    });
  console.log(link)
  console.log("link>>>>>>>>>>>>>>>>>>>>");
    return link;
  }
  //  Get the code
    //3
  async getToken() {
    let fcmToken = await AsyncStorage.getItem('fcmToken');
    // alert(fcmToken)
    if (!fcmToken) {
        fcmToken = await firebase.messaging().getToken();
        if (fcmToken) {
          console.log(fcmToken);
          // console.warn('??????'+fcmToken)
          // console.warn('fcmToken')
         // DashBordStore.UpdateFcmToken(fcmToken);
             // user has a device token
             AccountStore._updateParameter(fcmToken,"token","fcmToken");

            await AsyncStorage.setItem('fcmToken', fcmToken);
        }
    }
    else{
      AccountStore._updateParameter(fcmToken,"token","fcmToken");

     // DashBordStore.UpdateFcmToken(fcmToken);
      // console.warn("<<<<<"+fcmToken);
      // console.log(fcmToken);
    }
  }
  
    //2
  async requestPermission() {
    try {
        await firebase.messaging().requestPermission();
        // User has authorised
        this.getToken();
    } catch (error) {
        // User has rejected permissions
        console.warn('permission rejected');
    }
  }





  async  componentDidMount() {
    Linking.addEventListener('url', this._handleOpenURL);

// this.GenerateDynamicLink();
// this.buildLink();
this.HandleDynamicLinking();
// this._handleOpenURL();
// console.log(loggedinUserdetails());
// this.GenerateStripeToken();
  //fcm token
  // this.checkPermission();
  // this.createNotificationListeners(); //add this line

this.PrepareAxiosInstance();
    // let fontName = 'CircularStdFont'
  //   let fontName = 'Poppins-Light'
  //  GlobalFont.applyGlobal(fontName)

    loggedinUserdetails().then( 
      async (resp)  => {  
  let isloggedin =resp.isAvailable;
  console.log(resp);
  if(isloggedin===true){
    let userdetails=resp.storeData
    console.log(userdetails)
let InnerProfile= await getLodgedInDetailsProfile();
let isAvailable =InnerProfile.isAvailable;
if(isAvailable===true){

let this_userprofile=InnerProfile.storeData;
console.log(this_userprofile)
console.log("InnerProfile")

    
    DashboardStore._UpdateLoggedInUserProfile(this_userprofile);
 }

    // console.log("userdetails")
  // let usertype = resp.usertype
  // let fullname = resp.fullname
  // let userid = resp.userid
  
  // console.warn(usertype);
  // console.warn('usertype logged in <<>>');
  // global.usertype= usertype;
  // global.userid= userid;
  // global.fullname= fullname;
   
  }
  else{
    console.warn('ddddddusertype lddkdkdddkdkdkdkdkdkdkdkdkdk');
  }
    
  }
    )
    .catch((error) => {
      console.warn(`get usertpe function ${error}`);
    });

    getLodgedInDetailsProfile().then(
      (resp) => {  
  let isAvailable =resp.isAvailable;
  console.warn('Profileff lddkdkdddkdkdkdkdkdkdkdkdkdk');
  console.log(resp);
  if(isAvailable===true){
    global.profile = resp.storeData || {};
   } 
  else{
    console.warn('Profile lddkdkdddkdkdkdkdkdkdkdkdkdk');
  }
    
  }
    )
    .catch((error) => {
      console.warn(`get usertpe function ${error}`);
    });

    isSignedIn()
      .then(res => this.setState({ signedIn: res, checkedSignIn: true }))
      // .catch(err => alert("ccAn error occurred"));
      .catch(err => console.warn(err));
      // .catch(err => console.warn(err));
      SplashScreen.hide();

  }
 //Remove listeners allocated in createNotificationListeners()
componentWillUnmount() {
  // this.notificationListener();
  // this.notificationOpenedListener();
  Linking.removeEventListener('url', this._handleOpenURL);

  }

  // show the message pushed

  // Handle Notification 

async createNotificationListeners() {
  /*
  * Triggered when a particular notification has been received in foreground
  * */
  this.notificationListener = firebase.messaging().onNotification((notification) => {
      const { title, body } = notification;
      // this.showAlert(title, body+'foreground');
      this.showAlert('Notification', 'Explore the new content');
      // const action = notificationOpen.action;
      // const notification= notificationOpen.notification;
      var seen = [];
      // alert(JSON.stringify(notification.data, function(key, val) {
      //     if (val != null && typeof val == "object") {
      //         if (seen.indexOf(val) >= 0) {
      //             return;
      //         }
      //         seen.push(val);
      //     }
      //     return val;
      // }));
  

  });

  /*
  * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
  * */
  this.notificationOpenedListener = firebase.messaging().onNotificationOpened((notificationOpen) => {
      const { title, body } = notificationOpen.notification;
      // this.showAlert(title, body+'background');
      this.showAlert('Notification', 'Explore the new content');
  });

  /*
  * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
  * */
  const notificationOpen = await firebase.messaging().getInitialNotification();
  if (notificationOpen) {
    console.warn(notificationOpen)
//     console.warn('notificationOpen')
      const { title, body } = notificationOpen.notification;
//       // console.warn(JSON.stringify(notificationOpen));
//       // SONE.parse(JSONE.stringify(test));
// //  JSON.parse(JSON.stringify(notificationOpen))
      // this.showAlert(title, body+'closed');
      this.showAlert('Notification', 'Explore the new content');
const action = notificationOpen.action;
const notification= notificationOpen.notification;
var seen = [];
// alert(JSON.stringify(notification.data, function(key, val) {
//     if (val != null && typeof val == "object") {
//         if (seen.indexOf(val) >= 0) {
//             return;
//         }
//         seen.push(val);
//     }
//     return val;
// }));
   }
  /*
  * Triggered for data only payload in foreground
  * */
  this.messageListener = firebase.messaging().onMessage((message) => {
    //process data message
    console.warn(JSON.stringify(message));
    console.warn('payload in foreground>>>>');
  });
}

showAlert(title, body) {
  Alert.alert(
    title, body,
    [
        { text: 'OK', onPress: () => console.log('OK Pressed') },
    ],
    { cancelable: false },
  );
}
   

  
  render() {
    const { checkedSignIn, signedIn } = this.state;

    // If we haven't checked AsyncStorage yet, don't render anything (better ways to do this)
    if (!checkedSignIn) {
      return null;
    }

    // if (signedIn) {
    //   return <SignedIn />;
    // } else {
    //   return <SignedOut />;
    // }
    const Layout = createRootNavigator(signedIn);
return(
 
   <SafeAreaView style={styles.safeArea}>
<Layout />
   </SafeAreaView>
 
  
)
     

  }
}
const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: '#ddd'
  }
})

//https://hiddentao.com/archives/2017/03/10/get-custom-fonts-working-in-react-native/


// var config = {
//   databaseURL: "<database-url>",
//   projectId: "<project-id>",
// };

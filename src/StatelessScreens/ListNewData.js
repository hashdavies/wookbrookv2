import React, { Component } from 'react';
 import {
  StyleSheet,
  ImageBackground,
  Keyboard,
  TouchableOpacity,
  ScrollView,
  Image,
  FlatList,
  ActivityIndicator
} from 'react-native';
import GridView from 'react-native-super-grid';
import Feather from 'react-native-vector-icons/Feather';
import Entypo from 'react-native-vector-icons/Entypo';
import {
  Container,
  Text,
  Header,
  Content,
  Textarea,
  Button,
  View,
  Thumbnail,
  Form,
  Item,
  Input,
  Label,
  Badge,
  Icon,
  Footer,
  FooterTab,
  Picker,
  Item as FormItem,
} from 'native-base';
 import styles from "../StyleSheet/NewsListStyle";
import { generate_id} from '../dependency/UtilityFunctions'; 
import { IMAGEURL } from '../constants/Constants';



const ListNewData = (props) =>{
  // console.warn(props.data);
  

    return (

      <FlatList
      data={props.data}
      renderItem={({ item }) => (
        <React.Fragment>
           <TouchableOpacity onPress={()=>props.SingleNews(item)}>
        {item.PictureFileName !== null ? 
        <View style={styles.singlecommentWrap}>
          <View style={styles.imageWrap}>
            <Image style={styles.img} 
           source={{ uri: IMAGEURL+item.PictureFileName}}
            />
          </View>
          <View style={styles.newgridContentWrap}>
            <View style={{ flexDirection: 'row' }}>
              <View style={styles.news_grid_view_title_wrapper}>
                <Text style={styles.news_grid_view_title_} numberOfLines={1}>
                {item.HeadLine}
                
                                </Text>
              </View>
            </View>
            <View style={{ flexDirection: 'row' }}>
              <View style={styles.news_grid_view_short_content_wrapper}>
                <Text style={styles.news_grid_view_short_content} numberOfLines={3}>
                {item.Story}

                </Text>
              </View>
            </View>
          </View>
        </View>
      :
      <View style={styles.singlecommentWrap}>
      <View style={styles.imageWrap}>
        <Image style={styles.img} 
        // source={CustomAsset.displayPicture}
        source={{ uri: 'https://reactnativecode.com/wp-content/uploads/2017/10/Guitar.jpg' }}
        />
      </View>
      <View style={styles.newgridContentWrap}>
        <View style={{ flexDirection: 'row' }}>
          <View style={styles.news_grid_view_title_wrapper}>
            <Text style={styles.news_grid_view_title_} numberOfLines={1}>
            {item.HeadLine}
            </Text>
          </View>
        </View>
        <View style={{ flexDirection: 'row' }}>
          <View style={styles.news_grid_view_short_content_wrapper}>
            <Text style={styles.news_grid_view_short_content} numberOfLines={3}>
            
            {item.Story}

            </Text>
          </View>
        </View>
      </View>
    </View>
    
        }
              </TouchableOpacity>
        </React.Fragment>
      )}
  
      keyExtractor={(item) => generate_id.toString()} 
  />
 
 
    );
}
 

export default ListNewData;


import React, { Component } from 'react';
import {
  StyleSheet,
  ImageBackground,
  Keyboard,
  TouchableOpacity,
  ScrollView, 
  Image,
  FlatList,
} from 'react-native';
import {
  Container,
  Text,
  Header,
  Content,
  Textarea,
  Button,
  View,
  Thumbnail,
  Form,
  Item,
  Input,
  Label,
  Badge,
  Icon,
  Footer,
  FooterTab,
  Picker,
  Item as FormItem,
} from 'native-base';

import * as Animatable from 'react-native-animatable';
import Entypo from 'react-native-vector-icons/Entypo';
 import Feather from 'react-native-vector-icons/Feather';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
 import TimeAgo from 'react-native-timeago';

import { CustomAsset } from '../../utils/assets';
import { IconStyles, AppColor, APP_STYLES } from '../../utils/AppStyles';
import styles from "../StyleSheet/CaseStyle";
import { generate_id} from '../dependency/UtilityFunctions'; 
import CaseStore from '../stores/Case';


const CaseData = (props) =>{
 let casestatus=props.data.Status;

 let status_display='';
 if (casestatus==='Assigned'){
  status_display=
  <React.Fragment>
<View style={{ width: 30, paddingTop: 5 }}>
            <Entypo name="folder" style={[styles.SmallIcon, styles.BlackColour]} />
          </View>

          <View style={styles.case_status_text}>
            <Text style={[styles.case_status, styles.BlackColour]}>{props.data.Status}</Text>
          </View>
  </React.Fragment>
 }
  else  if (casestatus==='Registered'){
    status_display=
    <React.Fragment>
  <View style={{ width: 30, paddingTop: 5 }}>
              <Entypo name="folder" style={[styles.SmallIcon, styles.BlackColour]} />
            </View>
  
            <View style={styles.case_status_text}>
              <Text style={[styles.case_status, styles.BlackColour]}>{props.data.Status}</Text>
            </View>
    </React.Fragment>
  }
  else  if (casestatus==='Transferred'){
    status_display=
    <React.Fragment>
  <View style={{ width: 30, paddingTop: 5 }}>
              <Entypo name="folder" style={[styles.SmallIcon, styles.BlackColour]} />
            </View>
  
            <View style={styles.case_status_text}>
              <Text style={[styles.case_status, styles.BlackColour]}>{props.data.Status}</Text>
            </View>
    </React.Fragment>
  }
  else  if (casestatus==='Adjourned'){
    status_display=
    <React.Fragment>
  <View style={{ width: 30, paddingTop: 5 }}>
              <Entypo name="folder" style={[styles.SmallIcon, styles.BlackColour]} />
            </View>
  
            <View style={styles.case_status_text}>
              <Text style={[styles.case_status, styles.BlackColour]}>{props.data.Status}</Text>
            </View>
    </React.Fragment>
  }
  else  if (casestatus==='Ongoing'){
    status_display=
    <React.Fragment>
  <View style={{ width: 30, paddingTop: 5 }}>
              <Entypo name="folder" style={[styles.SmallIcon, styles.BlackColour]} />
            </View>
  
            <View style={styles.case_status_text}>
              <Text style={[styles.case_status, styles.BlackColour]}>{props.data.Status}</Text>
            </View>
    </React.Fragment>
  }
  else  if (casestatus==='Mention'){
    status_display=
    <React.Fragment>
  <View style={{ width: 30, paddingTop: 5 }}>
              <Entypo name="folder" style={[styles.SmallIcon, styles.BlackColour]} />
            </View>
  
            <View style={styles.case_status_text}>
              <Text style={[styles.case_status, styles.BlackColour]}>{props.data.Status}</Text>
            </View>
    </React.Fragment>
  }
  else  if (casestatus==='Hearing'){
    status_display=
    <React.Fragment>
  <View style={{ width: 30, paddingTop: 5 }}>
              <Entypo name="folder" style={[styles.SmallIcon, styles.BlackColour]} />
            </View>
  
            <View style={styles.case_status_text}>
              <Text style={[styles.case_status, styles.BlackColour]}>{props.data.Status}</Text>
            </View>
    </React.Fragment>
  }
  else  if (casestatus==='Continuation'){
    status_display=
    <React.Fragment>
  <View style={{ width: 30, paddingTop: 5 }}>
              <Entypo name="folder" style={[styles.SmallIcon, styles.BlackColour]} />
            </View>
  
            <View style={styles.case_status_text}>
              <Text style={[styles.case_status, styles.BlackColour]}>{props.data.Status}</Text>
            </View>
    </React.Fragment>
  }
  else  if (casestatus==='ReportOfService'){
    status_display=
    <React.Fragment>
  <View style={{ width: 30, paddingTop: 5 }}>
              <Entypo name="folder" style={[styles.SmallIcon, styles.BlackColour]} />
            </View>
  
            <View style={styles.case_status_text}>
              <Text style={[styles.case_status, styles.BlackColour]}>{props.data.Status}</Text>
            </View>
    </React.Fragment>
  }
  else  if (casestatus==='FurtherMention'){
    status_display=
    <React.Fragment>
  <View style={{ width: 30, paddingTop: 5 }}>
              <Entypo name="folder" style={[styles.SmallIcon, styles.BlackColour]} />
            </View>
  
            <View style={styles.case_status_text}>
              <Text style={[styles.case_status, styles.BlackColour]}>{props.data.Status}</Text>
            </View>
    </React.Fragment>
  }
  else  if (casestatus==='Closed'){
    status_display=
    <React.Fragment>
  <View style={{ width: 30, paddingTop: 5 }}>
              <Entypo name="lock" style={[styles.SmallIcon, styles.RedColor]} />
            </View>
  
            <View style={styles.case_status_text}>
              <Text style={[styles.case_status, styles.BlackColour]}>{props.data.Status}</Text>
            </View>
    </React.Fragment>
  }

  else{
    status_display=
    <React.Fragment>
  <View style={{ width: 30, paddingTop: 5 }}>
              <Entypo name="folder" style={[styles.SmallIcon, styles.BlackColour]} />
            </View>
  
            <View style={styles.case_status_text}>
              <Text style={[styles.case_status, styles.BlackColour]}>{props.data.Status}</Text>
            </View>
    </React.Fragment>
  }
 
 
    return (
      <TouchableOpacity onPress={props.SingleCase}  
      
 onLongPress={ global.usertype==='ChiefJudge' || global.usertype==='JudicialStaff' ?   props.longpressfunction:null} >
{/* //  onLongPress={  props.longpressfunction} > */}
       
    
    {/* <View style={[styles.listitem, CaseStore.selectedCases.includes(props.data.Id)===true ? styles.case_selected :styles.case_selected ]}> */}
    <View style={[styles.listitem, CaseStore.selectedCases.includes(props.data.Id)===true ? styles.case_selected :styles.uncase_selected ]}>
      <View style={{ flex: 1, flexDirection: 'column' }}>
        <View style={styles.case_title_wrapper}>
          <Text style={styles.case_title_}>{props.data.Title}</Text>
        </View>
        <View style={{ alignItems: 'flex-start', flexDirection: 'row' }}>
         {status_display}
        </View>
      </View>

      <View style={{ width: 40, flexDirection: 'column' }}>
        <View
          style={{ alignItems: 'center', justifyContent: 'center', marginBottom: 4 }}
        >
          <FontAwesome name="legal" style={[styles.MidiumIcon, styles.BlackColour]} />
        </View>
      </View>
    </View>
    
      </TouchableOpacity>

     
    
    );
}
 

export default CaseData;


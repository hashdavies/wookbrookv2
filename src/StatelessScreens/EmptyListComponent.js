import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,Image,
  View,Text,TouchableOpacity
   
} from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

// import {Icon,Badge } from 'native-base';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { Container, Header, Content, Button, Thumbnail,Form, Item, Input, Label,Badge,Icon ,Textarea} from 'native-base';
import {text_font_family,header_text_font_family} from '../dependency/UtilityFunctions'
const EmptyListComponent = (props) =>{
    return (
                
      <View style={{justifyContent:'center',alignItems:'center',flex:1}}>
         {/* <View style={{alignItems:'center',justifyContent:'center',paddingBottom:19}}>
    <MaterialIcons name="hourglass-empty"   style={[styles.edit_iconsizeNcolour]}/>
      </View>  */}
      <Text style={{color:'#000',fontSize:20,fontFamily:text_font_family()}}>
      {props.message}
        </Text>
        {/* <View style={{marginTop:16,}}>
           <Button iconLeft rounded  style={{backgroundColor:'#636363',height:30,width:40,}}
           
           onPress={props.onclickhandler}>
           <MaterialCommunityIcons name='reload' style={styles.iconsizeNcolour}/>
            <Text style={{marginLeft:12,color:'#fff'}}>Reload</Text>
          </Button>
           </View> */}
      </View>
            
     );
}
 

export default EmptyListComponent;


const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems:'center',
    justifyContent: 'center',
    backgroundColor:'#555f61',
  }, 
  edit_iconsizeNcolour: {
    fontSize:40,
    color:'#F75A4A',
  
    },
  // messagewrap: {
  //       alignItems: 'center',
  //   justifyContent:'center',
  //   backgroundColor:'#2c3f50',

  //       },
        Text: {
        color:'#fff',
        fontSize:18,         
        },
        iconsizeNcolour: {
          fontSize: 15,
          color: '#fff',
          paddingLeft:12,
        },
         
});
import React, { Component } from 'react';
 import {
  StyleSheet,
  ImageBackground,
  Keyboard,
  TouchableOpacity,
  ScrollView,
  Image,
  FlatList,
  ActivityIndicator
} from 'react-native';
import GridView from 'react-native-super-grid';
import Feather from 'react-native-vector-icons/Feather';
import Entypo from 'react-native-vector-icons/Entypo';
import {
  Container,
  Text,
  Header,
  Content,
  Textarea,
  Button,
  View,
  Thumbnail,
  Form,
  Item,
  Input,
  Label,
  Badge,
  Icon,
  Footer,
  FooterTab,
  Picker,
  Item as FormItem,
} from 'native-base';
 import styles from "../StyleSheet/NewsStyle";
import { generate_id} from '../dependency/UtilityFunctions'; 
import { IMAGEURL } from '../constants/Constants';



const GridNewData = (props) =>{
  // console.warn(props.data);
  
  // PictureFileName
  // {dashStore.userNews.length >0 ? 

  //   <GridNewData
  //   data={dashStore.userNews}
  //   />
    
  //   :  
  //   ErrorComponentDisplay
    
  //   } 

    return (
      <GridView
      itemDimension={130}
      items={props.data}
      style={styles.gridView}
      renderItem={item => (
        <React.Fragment>
           <TouchableOpacity onPress={()=>props.SingleNews(item)}>
        {item.PictureFileName !== null ? 

<ImageBackground
style={styles.backgroundImage}

source={{ uri: IMAGEURL+item.PictureFileName}}
>
<View style={[styles.overlay_content]}>
 

  <View style={styles.news_listview_title_wrapper}>
    <Text style={styles.news_listview_title_} numberOfLines={1}>
     {item.HeadLine}
    </Text>
  </View>

  <View style={styles.news_listview_short_content_wrapper}>
    <Text style={styles.news_listview_short_content} numberOfLines={5}>
    {item.Story}
    </Text>
  </View>
</View>
</ImageBackground>
          
          :  
          <ImageBackground
          style={styles.backgroundImage}
          
          source={{ uri: 'https://reactnativecode.com/wp-content/uploads/2017/10/Guitar.jpg' }}
        >
          <View style={[styles.overlay_content]}>
           

            <View style={styles.news_listview_title_wrapper}>
              <Text style={styles.news_listview_title_} numberOfLines={1}>
               {item.HeadLine}
              </Text>
            </View>

            <View style={styles.news_listview_short_content_wrapper}>
              <Text style={styles.news_listview_short_content} numberOfLines={5}>
              {item.Story}
              </Text>
            </View>
          </View>
        </ImageBackground>  
          }
      
      </TouchableOpacity>
        
        </React.Fragment>
      )}
    />
 
    );
}
 

export default GridNewData;


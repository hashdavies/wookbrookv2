import {
  StyleSheet,
  ImageBackground,
  Keyboard,
  TouchableOpacity,
  FlatList,
   Alert,
  ScrollView,
  Image,
} from 'react-native';

import { observable, action } from 'mobx';
import qs from 'qs';
import axios from 'axios'
 
import Toast from 'react-native-toast-native';
import ToastStyle from "../StyleSheet/ToastStyle";
import {loggedinUserdetails,} from '../dependency/UtilityFunctions'

 
import { CHANNEL } from '../constants/Constants';
import { PersistData,generate_id,arrayToObject } from '../dependency/UtilityFunctions';

import {NavigationActions,navigation} from 'react-navigation';
import { withNavigation } from 'react-navigation';

import { onSignIn } from "../auth";



class Diary {
    @observable newCaseData = {
      case_title:'',
      case_number:'',  
      Type:'',  
      Description:'',
      }
    @observable myNotes = [];
    @observable Notes = [];
    @observable myChatlist = [];
    @observable previousChat = [];
    @observable disabled = false
    @observable loader_visible = false
    
    @observable color = '#F0BA00'
  
  
    @action  _ToggleLoader = (status) => {
this.loader_visible=status;

    }

  
    @action  SaveNote2DB  (params) {
       
                
      console.warn(params);
    this.loader_visible=true
       axios.post('/note/add', qs.stringify(params))
            .then( response => {
                this.loader_visible=false    
            
              console.warn(response)
              let resp=response.data;
             if(resp.StatusCode === '00') {
            let message =resp.Message
            Toast.show(message, Toast.SHORT, Toast.BOTTOM,ToastStyle);
               }
              else {
                this.loader_visible=false    
                console.warn('error');
                let resp=response.data;
                console.warn(resp);
                let message =resp.Message
                Toast.show(message, Toast.SHORT, Toast.BOTTOM,ToastStyle);
                   this._ToggleLoader(false)
                
              }           
            })
            .catch(err => console.warn(err)) 

  
     
  };
    






    GetMyNotes = () => {
      console.warn("get my note")
     let userid=global.userid;
       this._ToggleLoader(true)
           console.warn(userid);
              axios.get( `user/${userid}/notes`)
              
                       .then( response => {
                         let resp=response.data;
                         console.warn(resp);
                      if(resp.StatusCode === '00') {
                     let Original_data=resp.Data;
                    console.warn(Original_data)
                    this.Notes= Original_data;
                    const FormattedData = Original_data.map(singleData => {
                      let DateTimeArray = singleData.ReminderDate.split('T');
       
                    return { 
                      _id:singleData.Id,
                      _date: DateTimeArray[0],
                        customStyles: {
                          container: {
                            backgroundColor: 'green',
                          },
                          text: {
                            color: 'white',
                          },
                        },
                   

                    }

                    });
             
           const Re_FormattedData = arrayToObject(FormattedData);
     this.myNotes=Re_FormattedData;

   console.warn(Re_FormattedData);
           
                this._ToggleLoader(false)
                         }
                         else {
                             console.warn('error');
                             let resp=response.data;
                             console.warn(resp);
                             let message =resp.Message
                             Toast.show(message, Toast.SHORT, Toast.BOTTOM,ToastStyle);
                             this.myNotes=[]
                            this._ToggleLoader(false)
                         }  
                       } )
                       .catch(error => {
                           console.warn(`error ${error}`);
                           this.myNotes=[];
                            this._ToggleLoader(false)
                       });
          }


    


           
 


     
}

const DiaryStore = new Diary()
export default DiaryStore
 
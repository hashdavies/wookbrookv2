import { observable, action } from 'mobx';
import { runInAction } from "mobx";
import qs from 'qs';
import axios from 'axios'
import { PersistData, ShowNotification,Uniqueid, PasswordCheck } from '../dependency/UtilityFunctions';
// import { PersistData ,ShowNotification,Uniqueid} from '../../dependency/UtilityFunctions';

// import * as DatabaseFunction from '../Database/allSchemas';
// import * as DatabaseSchema from '../Database/allSchemas';

import firebase from '@react-native-firebase/app';

// import Toast from 'react-native-toast-native';
// import ToastStyle from "../StyleSheet/ToastStyle";
// import { CHANNEL } from '../constants/Constants';
// import { PersistData } from '../dependency/UtilityFunctions';

// import {NavigationActions,navigation} from 'react-navigation';
// import { withNavigation } from 'react-navigation';

// import { onSignIn } from "../auth";


import OnboardingApi from '../model/OnboardingApi'
import DashboardStore from './Dashboard';
class Account {


  @observable todos = [];

  constructor(ApiModel) {
    this.apiCall = ApiModel;
  }
  ////////////////////////////////Variables/////////////
  @observable payload_regInfo = {
    "username":"",
    "email":"",
    "platform_uid":"",
    "platform":"",
    "last_name":"",
    "first_name":"",
   
  }
  @observable regInfo_Normal = {
    "username":"",
    "email":"",
    "password":"",
    "isPasswordError":false
  }
  @observable payload_verifyAccount = {
    "token":""
  }
  @observable payload_loginInfo = {
    "email":"",
    "password":"",
    "signup_type":1
   
  }
  @observable payload_forgetPwd = {
    "email":"",
    "ServerMessage":"Please wait...",
  }
  @observable payload_resetPwd = {
    "password":"",
    "code":""
  }
  @observable errorDisplay = {
    "message":"",
    "title":""
  }
  @observable fcmToken = {
    "token":"",
   
  }



  @observable signInData = {
    user: null,
    message: '',
    codeInput: '',
    phoneNumber: '+44',
    confirmResult: null,

  }
  @observable navigationState = {

  }
  @observable LoadingDataPayload = {
    readyToLoadData:false,
    isdataready:false,
    loadingDataMessage:'Preparing Your View, Please Wait....'
  }
  @observable isProcessing = false;
  @observable readyToLoadData = false;
  @observable loggedinUserData = [];
  // @observable currentMobileNumber = '+2347065873900';
  @observable currentUserMobile = {
    currentMobileNumber_normal:'',
    currentMobileNumber_inter:''
  };
  ////////////////////////////////Variables/////////////



  
  @observable loggedinUserdetails = {
    Reg_Role: 'fuck you',
  }
  @observable AllCities = [];
  @observable disabled = false
  @observable loader_visible = false
  @observable color = '#F0BA00'
  @observable notifications = true
  @observable redirectmode = false
  @observable resendVerificationCode = false

  ////////////////////////////////Actions/////////////


  @action _ToggleProcessing = (status = false) => {
    this.isProcessing = status;
  }
  @action _updateUserModile = (normal,international) => {
 this.currentUserMobile.currentMobileNumber_inter=international
 this.currentUserMobile.currentMobileNumber_normal=normal
  }
  @action UpdateNavigationState = (navstate) => {
    console.log(navstate)
    this.navigationState = navstate;
  }
  @action onChangeText = (key, value, type) => {
    // console.log(value)
    this[type][key] = value;
   
    if(key==='password'){
      let response=PasswordCheck(value);
      this[type]['isPasswordError']=response;
    }
    // console.warn(accountStore.regInfo)

  }
  @action _updateParameter = (value,Feild,parent) => {
    // console.warn("news toggle view")
    this[parent][Feild] = value;

  }
  // @action _updateErrorDisplay = (value,Feild,parent) => {
  //   // console.warn("news toggle view")
  //   this[parent][Feild] = value;

  // }
  ////////////////////////////////Actions/////////////










  
  @action _ToggleLoader = (status) => {
    this.loader_visible = status;
  }
  @action _UpdateCities = (data) => {
    this.AllCities = data;
  }
 

 

  saveData = (userid, usertype, fullname,pix) => {
    console.warn("Enter to set local storage ")
    let Mydata={
            isloggedin: true,
            userid: userid,
            usertype: usertype,
            fullname: fullname,
            profilepix: pix,
          }
          console.log(Mydata)
    global.storage.save({
      key: 'Loggedinstate',   // Note: Do not use underscore("_") in key! 
      data: Mydata,

      // if not specified, the defaultExpires will be applied instead. 
      // if set to null, then it will never expire. 
      expires: null
    });
    console.log(Mydata)
    console.log(Mydata+'llll>>>><<<')
    this.redirectmode = true
    AccountStore.LoadingDataPayload.loadingDataMessage='Done Loading your data';
    AccountStore.LoadingDataPayload.isdataready=true;
    
    // AccountStore.navigationState.navigate("SignedIn");
    // AccountStore.navigationState.navigate("Home");
  }
  // saveData = (userid, usertype, fullname) => {
  //   let Mydata={
  //           isloggedin: true,
  //           userid: userid,
  //           usertype: usertype,
  //           fullname: fullname,
  //         }
  //         console.log(Mydata)
   
  // //  accountStore._ToggleLoader(false)
  //  // this.redirectmode = true
  //     // this.navigateToScreen("SignedIn");
  //   //    this.props.navigation.navigate("SignedIn");
  //   AccountStore.navigationState.navigate("SignedIn");
  // }

  

  _LoginNow = async () => {
     const {
      email,
    password,
    signup_type
     
    } =AccountStore.payload_loginInfo

    const{fcmToken}=AccountStore;
     if(email===''){
      ShowNotification('Email or Username is required', true);

    }
    else if(password===''){
      ShowNotification('Password is required', true);

    }
    else{
 
      let data={
        "email":email,
        "password":password,
        "signup_type":signup_type,
        "fcm_token":fcmToken.token
      }

      console.log(data);
      // return;
      AccountStore._ToggleProcessing(true)
      const result = await this.apiCall.LoginUserNow(data);
  
      console.log(result)
      AccountStore._ToggleProcessing(false)
      if (result !== false) {
        let response = result;
        if (response.status === 200 || response.status === 201) {
          let resp = response.data;
  
          let ApiStatus = resp.status
          if (ApiStatus === true) {
           let message = resp.message;
            console.log("success???" + message)
            ShowNotification(message, true);
            let data = resp.data;
            let token = data.access_token;
            let profile = data.profile;
            console.log(profile)
            let token_data={
              accessToken:token,
              isloggedin:true,
              usertype:'User'
            }
  let data2save=   Object.assign({}, profile, token_data);
  console.log(data2save)
  console.log(token_data)
  global.profile = profile;   
  this.PasistLoginRecord(profile,data2save) 
  let loginSession=`Bearer ${token};`
  DashboardStore._getAxiosInstance(loginSession);
  ShowNotification("Preparing your view.....", true);
  setTimeout(() => {
    AccountStore.navigationState.navigate("SignedIn");

  }, 1000);
         
          
          }
          else {
            //   this.Categories = []
            let message = resp.message;
            console.log("heree" + message)
            ShowNotification(message, true);
          }
  
          // this._ToggleLoader('Categories',false)
  
        }
        else {
          let resp=response.data;
          //  console.warn(resp);
           let message =resp.message
           ShowNotification(message, true);
      
        }
      }
      else {
        ShowNotification('Generic Error Occured', true);
  
      }

    }


  }

  RegisterUserNow = async () => {
    //   alert("ji")
    const {
      username,
      email,
      platform_uid,
      platform
    } =AccountStore.regInfo
  console.log(AccountStore.regInfo)
    if(platform_uid===''){
      ShowNotification('Platform id is required', true);

    }
  else if(platform===''){
      ShowNotification('Platform is required', true);
    }
  else if(email===''){
      ShowNotification('Email Address is required', true);
    }

    else{
      let data={
        "phone":Mobile,
        "firstname":firstname,
        "lastname":lastname,
        "email":email,
        "dob":dob,
        "field":field,
        "yearOfGrad":yearOfGrad,
        "gender":gender,
        "yearOfHouse":yearOfHouse,
        "yearOfExp":yearOfExp,
        "password":password,
        "practclass":practclass
      }

      console.log(data);
      // return;
      AccountStore._ToggleProcessing(true)
      const result = await this.apiCall.RegisterPractitionalNow(data);
  
      console.log(result)
      AccountStore._ToggleProcessing(false)
      if (result !== false) {
        let response = result;
        if (response.status === 200 || response.status === 201) {
          let resp = response.data;
  
          let ApiStatus = resp.success
          if (ApiStatus === true) {
            console.log(resp)
            let data = resp.data;
            let token = resp.token;
            console.log(data)
            let token_data={
              accessToken:token,
              isloggedin:true,
            }
  let data2save=   Object.assign({}, data, token_data);
  console.log(data2save)
  global.profile = data;    
  PersistData('loggedinUserdetails', data);
  PersistData('Loggedinstate', data2save);
  
            // AccountStore.LoadingDataPayload.readyToLoadData = true;
            // AccountStore.readyToLoadData = true;
    // PersistData('myCurrentChild',);
            runInAction(() => {
  
              // AccountStore.LoadingDataPayload.readyToLoadData = true;
              AccountStore._ToggleLoader(false)
              AccountStore.redirectmode = true
              setTimeout(() => {
                // AccountStore.loggedinUserData = data;
                // this.SaveUserData(data);
              }, 2000);
         
            });
          
          }
          else {
            //   this.Categories = []
            let message = resp.error;
            console.log("heree" + message)
            ShowNotification(message, true);
          }
  
          // this._ToggleLoader('Categories',false)
  
        }
        else {
          ShowNotification('An Error Occured.', true);
          let resp=response.data;
          //  console.warn(resp);
           let message =resp.message
           ShowNotification(message, true);
        }
      }
      else {
        ShowNotification('Generic Error Occured', true);
  
      }

    }


  }
  RegisterUserNow_NormalReg = async () => {
    //   alert("ji")
    const {
      username,
      email,
      password,
      isPasswordError
    } =AccountStore.regInfo_Normal

  console.log(AccountStore.regInfo_Normal)
  let verify = validateEmail(email);
    if(username===''){
      ShowNotification('Username id is required', true);

    }
  else if(password===''){
      ShowNotification('Password is required', true);
    }
  else if(email===''){
      ShowNotification('Email Address is required', true);
    }
  else if(verify !== true){
      ShowNotification('Enter a valid Email Address', true);
    }
  else if(isPasswordError===true){
      ShowNotification('Password Must contain UPPER and lowercase and numbers and length > 7 ', true);
    }
  else{
      let data={
      	"username":username,
	"email":email,
	"password":password
      }

      console.log(data);
      // return;
      AccountStore._ToggleProcessing(true)
      const result = await this.apiCall.UserNormalRegistration(data);
  
      console.log(result)
      AccountStore._ToggleProcessing(false)
      if (result !== false) {
        let response = result;
        if (response.status === 200 || response.status === 201) {
          let resp = response.data;
  
          let ApiStatus = resp.status
          if (ApiStatus === true) {
           let message = resp.message;
            console.log("success???" + message)
            ShowNotification(message, true);

            setTimeout(() => {
              // AccountStore.loggedinUserData = data;
              // this.SaveUserData(data);
                  AccountStore.navigationState.navigate("AccountVerificaation");

            }, 2000);
            // console.log(resp)
            // let data = resp.data;
            // let token = resp.token;
            // console.log(data)
            // let token_data={
            //   accessToken:token,
            //   isloggedin:true,
            // }
  // let data2save=   Object.assign({}, data, token_data);
  // console.log(data2save)
  // global.profile = data;    
  // PersistData('loggedinUserdetails', data);
  // PersistData('Loggedinstate', data2save);
  
            // AccountStore.LoadingDataPayload.readyToLoadData = true;
            // AccountStore.readyToLoadData = true;
    // PersistData('myCurrentChild',);
            // runInAction(() => {
  
            //   // AccountStore.LoadingDataPayload.readyToLoadData = true;
            //   AccountStore._ToggleLoader(false)
            //   AccountStore.redirectmode = true
            
         
            // });
          
          }
          else {
            //   this.Categories = []
            let message = resp.message;
            console.log("heree" + message)
            ShowNotification(message, true);
          }
  
          // this._ToggleLoader('Categories',false)
  
        }
        else {
          let resp=response.data;
          //  console.warn(resp);
           let message =resp.message
           ShowNotification(message, true);
        }
      }
      else {
        ShowNotification('Generic Error Occured', true);
  
      }

    }


  }

  _RegisterUserNow_SocialMedia = async () => {
  const {
      username,
      email,
      platform_uid,
      platform,
      last_name,
      first_name,
    } =AccountStore.payload_regInfo
  console.log(AccountStore.payload_regInfo)
    if(platform_uid===''){
      ShowNotification('Platform id is required', true);

    }
  else if(platform===''){
      ShowNotification('Platform is required', true);
    }
  else if(email===''){
      ShowNotification('Email Address is required', true);
    }
  else if(username===''){
      ShowNotification('Username is required', true);
    }

    else{
      let data={
        "username":username,
        "email":email,
        "platform_uid":platform_uid,
        "platform":platform,
        "first_name":first_name,
        "last_name":last_name,
      }

      console.log(data);
      // return;
      AccountStore._ToggleProcessing(true)
      const result = await this.apiCall.RegisterUserNow_SocialMedia(data);
  
      console.log(result)
      AccountStore._ToggleProcessing(false)
      if (result !== false) {
        let response = result;
        if (response.status === 200 || response.status === 201) {
          let resp = response.data;
  
          let ApiStatus = resp.status
          if (ApiStatus === true) {
            let message = resp.message;
             console.log("success???" + message)
             ShowNotification(message, true);
             let data = resp.data;
             let token = data.access_token;
             let profile = data.profile;
             console.log(profile)
             let token_data={
               accessToken:token,
               isloggedin:true,
               usertype:'User'
             }
   let data2save=   Object.assign({}, profile, token_data);
   console.log(data2save)
   console.log(token_data)
   global.profile = profile;   
   this.PasistLoginRecord(profile,data2save)
   let loginSession=`Bearer ${token};`
   DashboardStore._getAxiosInstance(loginSession); 
   ShowNotification("Preparing your view.....", true);

   setTimeout(() => {
     AccountStore.navigationState.navigate("SignedIn");
 
   }, 1000);
          
           
           }
           else {
             //   this.Categories = []
             let message = resp.message;
             console.log("heree" + message)
             ShowNotification(message, true);
           }
          // this._ToggleLoader('Categories',false)
  
        }
        else {
          let resp=response.data;
          //  console.warn(resp);
           let message =resp.message
           ShowNotification(message, true);
        }
      }
      else {
        ShowNotification('Generic Error Occured', true);
  
      }

    }


  }

  _SendResetPwdCode = async () => {
    //   alert("ji")
    const {
    
      email,
   
    } =AccountStore.payload_forgetPwd

  console.log(AccountStore.payload_forgetPwd)
  let verify = validateEmail(email);
 
  if(email===''){
      ShowNotification('Email Address is required', true);
    }
  else if(verify !== true){
      ShowNotification('Enter a valid Email Address', true);
    }
 
  else{
      let data={
 	"email":email,
 
      }

      console.log(data);
      // return;
      AccountStore._ToggleProcessing(true)
      const result = await this.apiCall.SendResetPwdCode(data);
    // ghghghhghghgh
      console.log(result)
      AccountStore._ToggleProcessing(false)
      if (result !== false) {
        let response = result;
        if (response.status === 200 || response.status === 201) {
          let resp = response.data;
  
          let ApiStatus = resp.status
          if (ApiStatus === true) {
           let message = resp.message;
            console.log("success???" + message)
            ShowNotification(message, true);
AccountStore.payload_forgetPwd.ServerMessage=message;
            setTimeout(() => {
              // AccountStore.loggedinUserData = data;
              // this.SaveUserData(data);
                  AccountStore.navigationState.navigate("ResetPassword");

            }, 2000);
            // console.log(resp)
            // let data = resp.data;
            // let token = resp.token;
            // console.log(data)
            // let token_data={
            //   accessToken:token,
            //   isloggedin:true,
            // }
  // let data2save=   Object.assign({}, data, token_data);
  // console.log(data2save)
  // global.profile = data;    
  // PersistData('loggedinUserdetails', data);
  // PersistData('Loggedinstate', data2save);
  
            // AccountStore.LoadingDataPayload.readyToLoadData = true;
            // AccountStore.readyToLoadData = true;
    // PersistData('myCurrentChild',);
            // runInAction(() => {
  
            //   // AccountStore.LoadingDataPayload.readyToLoadData = true;
            //   AccountStore._ToggleLoader(false)
            //   AccountStore.redirectmode = true
            
         
            // });
          
          }
          else {
            //   this.Categories = []
            let message = resp.message;
            console.log("heree" + message)
            ShowNotification(message, true);
          }
  
          // this._ToggleLoader('Categories',false)
  
        }
        else {
          // ShowNotification('An Error Occured.', true);
          //  console.warn('error');
           let resp=response.data;
          //  console.warn(resp);
           let message =resp.message
           ShowNotification(message, true);
          //  Toast.show(message, Toast.SHORT, Toast.BOTTOM,styletoest);
          // this.Categories = []
          // this._ToggleLoader(null, false)
          // return [];
          // resolve(false);
        }
      }
      else {
        ShowNotification('Generic Error Occured', true);
  
      }

    }


  }
  _ResetPasswordNow = async () => {
    //   alert("ji")
    const {
    
      password,
      code
   
    } =AccountStore.payload_resetPwd
    const{fcmToken}=AccountStore;   
  if(code===''){
      ShowNotification('Verification code is required', true);
    }
  else   if(password===''){
    ShowNotification('New Password  is required', true);
  }
 
  else{
    
      let data={
        "password":password,
        "code":code,
        "fcm_token":fcmToken.token
        
 
      }

      console.log(data);
      // return;
      AccountStore._ToggleProcessing(true)
      const result = await this.apiCall.ResetPasswordNow(data);
  
      console.log(result)
      AccountStore._ToggleProcessing(false)
      if (result !== false) {
        let response = result;
        if (response.status === 200 || response.status === 201) {
          let resp = response.data;
  
          let ApiStatus = resp.status
          if (ApiStatus === true) {
           let message = resp.message;
            console.log("success???" + message)
            ShowNotification(message, true);

            setTimeout(() => {
              // AccountStore.loggedinUserData = data;
              // this.SaveUserData(data);
                  AccountStore.navigationState.navigate("Login");

            }, 2000);
            // console.log(resp)
            // let data = resp.data;
            // let token = resp.token;
            // console.log(data)
            // let token_data={
            //   accessToken:token,
            //   isloggedin:true,
            // }
  // let data2save=   Object.assign({}, data, token_data);
  // console.log(data2save)
  // global.profile = data;    
  // PersistData('loggedinUserdetails', data);
  // PersistData('Loggedinstate', data2save);
  
            // AccountStore.LoadingDataPayload.readyToLoadData = true;
            // AccountStore.readyToLoadData = true;
    // PersistData('myCurrentChild',);
            // runInAction(() => {
  
            //   // AccountStore.LoadingDataPayload.readyToLoadData = true;
            //   AccountStore._ToggleLoader(false)
            //   AccountStore.redirectmode = true
            
         
            // });
          
          }
          else {
            //   this.Categories = []
            let message = resp.message;
            console.log("heree" + message)
            ShowNotification(message, true);
          }
  
          // this._ToggleLoader('Categories',false)
  
        }
        else {
          let resp=response.data;
          //  console.warn(resp);
           let message =resp.message
           ShowNotification(message, true);
        }
      }
      else {
        ShowNotification('Email does not exist, Kindly register', true);
  
      }

    }


  }

 

  VerifyUserAccount = async () => {
    const {
      token,
    } =AccountStore.payload_verifyAccount

  // console.log(AccountStore.regInfo_Normal)
  const{fcmToken}=AccountStore;
  let fcm_token=fcmToken.token;
    if(token===''){
      ShowNotification('Verification  token is required', true);

    }
    else{
      AccountStore._ToggleProcessing(true)
      const result = await this.apiCall.VerifyAccount(token,fcm_token);
  
      console.log(result)
      AccountStore._ToggleProcessing(false)
      if (result !== false) {
        let response = result;
        if (response.status === 200 || response.status === 201) {
          let resp = response.data;
          let ApiStatus = resp.status
          if (ApiStatus === true) {
            let message = resp.message;
             console.log("success???" + message)
             ShowNotification(message, true);
             let data = resp.data;
             let token = data.access_token;
             let profile = data.profile;
             console.log(profile)
             let token_data={
               accessToken:token,
               isloggedin:true,
               usertype:'User'
             }
   let data2save=   Object.assign({}, profile, token_data);
   console.log(data2save)
   console.log(token_data)
   global.profile = profile;   
   this.PasistLoginRecord(profile,data2save) 
   let loginSession=`Bearer ${token};`
   DashboardStore._getAxiosInstance(loginSession);
   ShowNotification("Preparing your view.....", true);
      setTimeout(() => {
     AccountStore.navigationState.navigate("SignedIn");
 
   }, 1000);
          
           
           }
           else {
             //   this.Categories = []
             let message = resp.message;
             console.log("heree" + message)
             ShowNotification(message, true);
           }
  
        }
        else {
          let resp=response.data;
          //  console.warn(resp);
           let message =resp.message
           ShowNotification(message, true);
       
        }
      }
      else {
        ShowNotification('Invalid Token', true);
  
      }
    }
    
   
    
   
   
   }



/////////////////////////////////////////////
AuthenticateUser = async (MobileNumber) => {
  //   alert("ji")
  AccountStore._ToggleProcessing(true)
  // const result = await this.apiCall.AuthenticateUser(MobileNumber);

  // console.log(result)
  // if (result === true) {
  //   // this.signIn(AccountStore.currentMobileNumber);
  //   this.signIn(AccountStore.currentUserMobile.currentMobileNumber_inter);
  // }
  // else {

  //   AccountStore._ToggleProcessing(false)
  //   ShowNotification(result, true);
    
  // }

  // runInAction(() => {
  //     console.log(result)
  // });

  // this.signIn(AccountStore.currentUserMobile.currentMobileNumber_inter);


}
  Fetch_Currespondingfields = async () => {
    const {
    practclass,
     } =AccountStore.regInfo
    AccountStore._ToggleProcessing(true)
    const result = await this.apiCall.Fetch_fields(practclass);

    console.log(result)
    AccountStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.success
        if (ApiStatus === true) {
          console.log(resp)
          let data = resp.data;
          let token = resp.token;
          console.log(data)
          runInAction(() => {

            // AccountStore.LoadingDataPayload.readyToLoadData = true;
            AccountStore._ToggleLoader(false)
            AccountStore.regInfo.availablefields=data;
            // AccountStore.redirectmode = true
            // setTimeout(() => {
            //   // AccountStore.loggedinUserData = data;
            //   // this.SaveUserData(data);
            // }, 2000);
       
          });
        
        }
        else {
          //   this.Categories = []
          let message = resp.error;
          console.log("heree" + message)
          AccountStore.regInfo.availablefields=[];

          ShowNotification(message, true);
        }

        // this._ToggleLoader('Categories',false)

      }
      else {
        AccountStore.regInfo.availablefields=[];

        ShowNotification('An Error Occured.', true);
        //  console.warn('error');
        //  let resp=response.data;
        //  console.warn(resp);
        //  let message =resp.Message
        //  Toast.show(message, Toast.SHORT, Toast.BOTTOM,styletoest);
        // this.Categories = []
        // this._ToggleLoader(null, false)
        // return [];
        // resolve(false);
      }
    }
    else {
      AccountStore.regInfo.availablefields=[];

      ShowNotification('Generic Error Occured', true);

    }
  }

  LoadUserData = async (MobileNumber) => {
    //   alert("ji")
    AccountStore._ToggleProcessing(true);
    const result = await this.apiCall.LoadUserData(MobileNumber);

    console.log(result)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let data = resp.data;
          console.log(data)
          // AccountStore.LoadingDataPayload.readyToLoadData = true;
          AccountStore.readyToLoadData = true;

          runInAction(() => {

            // AccountStore.LoadingDataPayload.readyToLoadData = true;
       
            setTimeout(() => {
              AccountStore.loggedinUserData = data;
              this.SaveUserData(data);
            }, 2000);
       
          });
        
        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
        }

        // this._ToggleLoader('Categories',false)

      }
      else {
        ShowNotification('An Error Occured.', true);
        //  console.warn('error');
        //  let resp=response.data;
        //  console.warn(resp);
        //  let message =resp.Message
        //  Toast.show(message, Toast.SHORT, Toast.BOTTOM,styletoest);
        // this.Categories = []
        // this._ToggleLoader(null, false)
        // return [];
        // resolve(false);
      }
    }
    else {
      ShowNotification('Generic Error Occured', true);

    }

    // runInAction(() => {
    //     console.log(result)
    // });
  }

  SaveUserData = async (data) => {
    // runInAction(() => {
    //   this.LoadingDataPayload.readyToLoadData = true;
    //   // this.SaveUserData(data);
    // });
    console.log("Entry here ooo")
this.DeleteExistingRecord();




    // let allData = this.loggedinUserData
    let allData = data;
if(allData.length >0){

  let parentarray=[];
var currentLocalSession_Userid=allData[0].useridx;
var currentLocalSession_Usertype=allData[0].usertype;
var currentLocalSession_Fullname=allData[0].firstnamex +'  '+ allData[0].lastnamex +" "+allData[0].middlenamex;
var currentLocalSession_ProfilePix=allData[0].profilePix;
var SchoolLogo=allData[0].SchoolLogo;


  let m = allData.map(function (item, index) {
    console.log(item)
    //AccountStore.LoadingDataPayload.loadingDataMessage='Loading Data';
    let schooluniqueidx = item.schooluniqueidx;
    let firstnamex = item.firstnamex;
    let middlenamex = item.middlenamex;
    let lastnamex = item.lastnamex;
    let profilePix = item.profilePix;
   // let schooluniqueidx = item.schooluniqueidx;
    let subdomain = item.subdomain;
    let deploymentidx = item.deploymentidx;
    let theme = item.theme;
    let usertype = item.usertype;
    let SchoolName = item.SchoolName;
    let mobilex = item.mobilex;
    let useridx = item.useridx;
    let emailx = item.emailx;
    var SchoolLogo=item.SchoolLogo;
    let mSchoolFullDetails=JSON.stringify(item.SchoolFullDetails);

    /////////////////////

    let Termlist = item.termList;
    let SessionList = item.sessionList;
    

    ////////////////////////////

    if(usertype==='parent'){

      let MYCHILD = item.MYCHILD;

 let checkifparent=parentarray.includes(useridx);  
if(checkifparent===false){

  // let firstnamex = item.firstnamex;
  // let middlenamex = item.middlenamex;
  // let lastnamex = item.lastnamex;
  // P_fname: firstnamex,
  // P_lname: lastnamex,
  // P_mname: middlenamex,

  parentarray.push(useridx)
  
  let fullname= firstnamex+' ' + middlenamex +' ' + lastnamex
  let userimage="https://res.cloudinary.com/custocrypt/image/upload/v1572951314/Edves%20App/male.png";
  let Userlist_ = {   
    // id: index,
    id: Uniqueid(),
    schooluniqueidx: schooluniqueidx,      
    subdomain: subdomain,      
    deploymentidx: deploymentidx,      
    theme: theme,      
    SchoolName:SchoolName, 
    staffName:fullname, 
    schoolLogo:SchoolLogo, 
    staffimage:profilePix, 
    user_id: useridx,
    phone: mobilex,
    usertype: usertype,
    Dob: '',
    Gender: '',
    SchoolFullDetails: mSchoolFullDetails,
  creationDate: new Date(),
  updatedDate: new Date()


  };


  console.log(Userlist_);
  DatabaseFunction.insertNew(Userlist_, DatabaseSchema.User_Schema_Name).then(
    (resp) => {
      console.log(resp)
      // Toast.show("Hey Task Created Succesfully", Toast.SHORT, Toast.BOTTOM,styletoest);
      // this.props.navigation.navigate('Home');
      console.log("inserted Userlist_");
    }
  )
    .catch((error) => {
      console.log(`Insert new row error ${error}`);
    });

}
else{
  console.log("Parent available in the array")
}

console.log(parentarray)
console.log('parentarray')





      MYCHILD.map(function (item, index) {
       // console.log(mSchoolFullDetails)
       let current_term= item.current_term;
       let current_session= item.current_session;
       let current_class=item.current_class;
       if(current_term ===null){
        current_term='';
       }
       if(current_session ===null){
        current_session='';
       }
       if(current_class ===null){
        current_class='';
       }
        let MYCHILDData = {   
          // id: index,
          id: Uniqueid(),
          P_id: useridx,
          P_email: emailx,
          P_fname: firstnamex,
          P_lname: lastnamex,
          P_mname: middlenamex,
          schooluniqueidx: schooluniqueidx,

          subdomain: subdomain,
          deploymentidx: deploymentidx,
          theme: theme,
          SchoolName: SchoolName,
          admissionnumber: item.admissionnumber,
          schoolidx: item.schoolidx,
          S_surname: item.surname,
          S_lname: item.firstname,
          S_mname: item.middlename,
          S_gendar: item.sex,
          StudentProfilePix: item.studentpassport,   
        SchoolFullDetails: mSchoolFullDetails,  
          current_class: current_class,
          current_term:current_term,
          current_session:current_session,
          parentemail: item.parentemail,
          phonenumber: item.phonenumber,
          dateofbirth: item.dateofbirth,
          balance: 0,
          ParentBalance: 0,

          creationDate: new Date(),
          updatedDate: new Date()

        };


        console.log(MYCHILDData);
        DatabaseFunction.insertNew(MYCHILDData, DatabaseSchema.Student_Schema_Name).then(
          (resp) => {
            console.log(resp)
            // Toast.show("Hey Task Created Succesfully", Toast.SHORT, Toast.BOTTOM,styletoest);
            // this.props.navigation.navigate('Home');
            console.log("inserted Student");
          }
        )
          .catch((error) => {
            console.log(`Insert new row error ${error}`);
          });

      })

    }
    if(usertype==='educator'){

      let firstnamex = item.firstnamex;
      let middlenamex = item.middlenamex;
      let lastnamex = item.lastnamex;
      let fullname= firstnamex+' ' + middlenamex +' ' + lastnamex
      let Userlist_ = {   
        // id: index,
        id: Uniqueid(),
        schooluniqueidx: schooluniqueidx,      
        subdomain: subdomain,      
        deploymentidx: deploymentidx,      
        theme: theme,      
        SchoolName:SchoolName, 
        staffName:fullname, 
        staffimage:profilePix, 
        schoolLogo:SchoolLogo, 
        user_id: useridx,
        phone: mobilex,
        usertype: usertype,
        Dob: '',
        Gender: '',
        SchoolFullDetails: mSchoolFullDetails,
      creationDate: new Date(),
      updatedDate: new Date()


      };


      console.log(Userlist_);
      DatabaseFunction.insertNew(Userlist_, DatabaseSchema.User_Schema_Name).then(
        (resp) => {
          console.log(resp)
          // Toast.show("Hey Task Created Succesfully", Toast.SHORT, Toast.BOTTOM,styletoest);
          // this.props.navigation.navigate('Home');
          console.log("inserted Userlist_");
        }
      )
        .catch((error) => {
          console.log(`Insert new row error ${error}`);
        });



     
   
    


      let classList = item.classList;
      classList.map(function (item, index) {
       // console.log(mSchoolFullDetails)
      //  let current_term= item.current_term;
      //  let current_session= item.current_session;
      //  let current_class=item.current_class;
      //  if(current_term ===null){
      //   current_term='';
      //  }
      //  if(current_session ===null){
      //   current_session='';
      //  }
      //  if(current_class ===null){
      //   current_class='';
      //  }
        let ClassList__ = {   
          // id: index,
          id: Uniqueid(),
        idx: schooluniqueidx,
        deploymentidx: item.schoolid,
        name: item.classname,
        classteacher: '',
        creationDate: new Date(),
        updatedDate: new Date()
 

        };


        console.log(ClassList__);
        DatabaseFunction.insertNew(ClassList__, DatabaseSchema.Class_Schema_Name).then(
          (resp) => {
            console.log(resp)
            // Toast.show("Hey Task Created Succesfully", Toast.SHORT, Toast.BOTTOM,styletoest);
            // this.props.navigation.navigate('Home');
            console.log("inserted ClassList__");
          }
        )
          .catch((error) => {
            console.log(`Insert ne ${error}`);
          });

      })
      let subjectList = item.subjectList;
      subjectList.map(function (item, index) {
       // console.log(mSchoolFullDetails)
      //  let current_term= item.current_term;
      //  let current_session= item.current_session;
      //  let current_class=item.current_class;
      //  if(current_term ===null){
      //   current_term='';
      //  }
      //  if(current_session ===null){
      //   current_session='';
      //  }
      //  if(current_class ===null){
      //   current_class='';
      //  }
      AccountStore.LoadingDataPayload.loadingDataMessage='Loading Subject list'
        let SubjectList__ = {   
          // id: index,
          id: Uniqueid(),
        // idx: item.schoolid,
        idx: schooluniqueidx,
        //deploymentidx: 'string',
        subjectname:item.name,
        subjecttabu: item.tabu,
        creationDate: new Date(),
        updatedDate: new Date()
   };


        console.log(SubjectList__);
        DatabaseFunction.insertNew(SubjectList__, DatabaseSchema.Subject_Schema_Name).then(
          (resp) => {
            console.log(resp)
            // Toast.show("Hey Task Created Succesfully", Toast.SHORT, Toast.BOTTOM,styletoest);
            // this.props.navigation.navigate('Home');
            console.log("inserted SubjectList__");
          }
        )
          .catch((error) => {
            console.log(`Insert ne ${error}`);
          });

      })

    }

    Termlist.map(function (item, index) {
      AccountStore.LoadingDataPayload.loadingDataMessage='Loading Term list'
      let TermData = {
        id: Uniqueid(),
        idx: schooluniqueidx,
        name: item.term,
        creationDate: new Date(),
        updatedDate: new Date()

      };
      console.log(TermData);
      DatabaseFunction.insertNew(TermData, DatabaseSchema.Term_Schema_Name).then(
        (resp) => {
          console.log(resp)
          // Toast.show("Hey Task Created Succesfully", Toast.SHORT, Toast.BOTTOM,styletoest);
          // this.props.navigation.navigate('Home');
          console.log("inserted");
        }
      )
        .catch((error) => {
          console.log(`Insert Term new row error ${error}`);
        });

    })

    SessionList.map(function (item, index) {
      AccountStore.LoadingDataPayload.loadingDataMessage='Loading Session list'
      let SessionData = {
        id: Uniqueid(),
        idx: schooluniqueidx,
        name: item.year,
        creationDate: new Date(),
        updatedDate: new Date()

      };
      console.log(SessionData);
      DatabaseFunction.insertNew(SessionData, DatabaseSchema.Session_Schema_Name).then(
        (resp) => {
          console.log(resp)
          // Toast.show("Hey Task Created Succesfully", Toast.SHORT, Toast.BOTTOM,styletoest);
          // this.props.navigation.navigate('Home');
          console.log("inserted Session");
        }
      )
        .catch((error) => {
          console.log(`Insert new row error ${error}`);
        });

    })


    //     SessionList.map(function(item,index){
    //  let SessionData = {
    //       id: index,      
    //       idx: schooluniqueidx,
    //       name: item.term,
    //      creationDate: new Date(),
    //       updatedDate:  new Date()

    //   };
    //   console.log(TermData);
    //      DatabaseFunction.insertNew(SessionData,DatabaseSchema.Session_Schema_Name).then(
    //     (resp) => {
    //       console.log(resp)
    //       // Toast.show("Hey Task Created Succesfully", Toast.SHORT, Toast.BOTTOM,styletoest);
    //       // this.props.navigation.navigate('Home');
    //       console.log("inserted Session");
    //     }
    //   )
    //   .catch((error) => {
    //     console.log(`Insert new row error ${error}`);
    // });

    //     })

  


  })
  // @action _ToggleProcessing = (status = false) => {
  //   this.isProcessing = status;
  // }
AccountStore._ToggleProcessing(false);
  global.current_usertype=currentLocalSession_Usertype;
  global.current_userid=currentLocalSession_Userid;
  global.current_userFullname=currentLocalSession_Fullname;
  global.current_userProfilePix=currentLocalSession_ProfilePix;
  global.SchoolLogo=SchoolLogo;
  // this.saveData(currentLocalSession_Userid,currentLocalSession_Usertype,currentLocalSession_Fullname,currentLocalSession_ProfilePix)
  this.saveData(currentLocalSession_Userid,currentLocalSession_Usertype,currentLocalSession_Fullname,SchoolLogo)



}




    //     const newtask = {
    //       id: not_id,      
    //       name: taskname,
    //       cat_id:     category,
    //       time:     tasktime,
    //       // date:    this.state.taskdate,
    //       date:    d,
    //       priority:    this.state.taskpriority,
    //       creationDate: new Date(),
    //       updatedDate:  new Date()

    //   };

    //   DatabaseFunction.insertNew(newtask,DatabaseSchema.TermListSchema).then(
    //     (resp) => {
    //       // Toast.show("Hey Task Created Succesfully", Toast.SHORT, Toast.BOTTOM,styletoest);
    //       // this.props.navigation.navigate('Home');
    //       console.log("inserted")
    //     }
    //   )
    //   .catch((error) => {
    //     console.warn(`Insert new row error ${error}`);
    // });

  }


  signIn = (phoneNumber) => {
    // const { phoneNumber } = this.state;
    // this.setState({ message: 'Sending code ...',defaultLoading:true });
    ShowNotification('Sending code ...', true);

    firebase.auth().signInWithPhoneNumber(phoneNumber)
      .then(confirmResult => {
         
        AccountStore.signInData.confirmResult = confirmResult;
       // AccountStore.signInData.message = 'Code has been sent!';
        ShowNotification('Verification Code has been sent!', true);
        console.log(confirmResult)
        console.log('confirmResult')
        console.warn(confirmResult)
        AccountStore._ToggleProcessing()
        // this.setState({ confirmResult, message: 'Code has been sent!' ,defaultLoading: false})
        // this.props.navigation.navigate("VerificationPage");
      })
      .catch(error => {
        AccountStore._ToggleProcessing()

        console.log(error);
        console.warn(error);
        //  this.setState({ message: `Sign In With Phone Number Error: ${error.message}` })
        ShowNotification(`Sign In With Phone Number Error: ${error.message}`, true);
      });
  };
  Resend_signIn_Code = (phoneNumber) => {
    // const { phoneNumber } = this.state;
    // this.setState({ message: 'Sending code ...',defaultLoading:true });
    ShowNotification('Resending   code ...', true);

    firebase.auth().signInWithPhoneNumber(phoneNumber)
      .then(confirmResult => {
         
        AccountStore.signInData.confirmResult = confirmResult;
        AccountStore.resendVerificationCode = true;
       // AccountStore.signInData.message = 'Code has been sent!';
        ShowNotification('Verification Code has been sent!', true);
        console.log(confirmResult)
        console.log('confirmResult')
        console.warn(confirmResult)
        AccountStore._ToggleProcessing()
        AccountStore.resendVerificationCode = false;
        // this.setState({ confirmResult, message: 'Code has been sent!' ,defaultLoading: false})
        // this.props.navigation.navigate("VerificationPage");
      })
      .catch(error => {
        AccountStore._ToggleProcessing()

        console.log(error);
        console.warn(error);
        //  this.setState({ message: `Sign In With Phone Number Error: ${error.message}` })
        ShowNotification(`Sign In With Phone Number Error: ${error.message}`, true);
      });
  };

  DeleteExistingRecord = () => {
    
let TableNames=[
  DatabaseSchema.Term_Schema_Name,
  DatabaseSchema.Session_Schema_Name,
  DatabaseSchema.Class_Schema_Name,
  DatabaseSchema.Student_Schema_Name,
  DatabaseSchema.School_Schema_Name,
  DatabaseSchema.User_Schema_Name,
  DatabaseSchema.Subject_Schema_Name,
  DatabaseSchema.SchoolEvent_Schema_Name,
];

 
    DatabaseFunction.deleteAllLists(TableNames).then(
      (resp) => {
        console.log(resp)
        console.log("Delete  Data");
      }
    )
      .catch((error) => {
        console.log(`Deleting errors ${error}`);
      });
  };
  PasistLoginRecord = (data,data2save) => {
    // alert("here")
    PersistData('loggedinUserdetails', data);
    PersistData('Loggedinstate', data2save);
   
  };








}
const onboardingApi = new OnboardingApi();
// const todoStore = new TodoStore(todoApi);

const AccountStore = new Account(onboardingApi)
export default AccountStore
// export default withNavigation(accountStore);

function validateEmail(email) {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}
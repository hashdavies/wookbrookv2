import {
  StyleSheet,
  ImageBackground,
  Keyboard,
  TouchableOpacity,
  FlatList,
   Alert, 
  ScrollView,
  Image,
} from 'react-native';

import { observable, action } from 'mobx';
import qs from 'qs';
import axios from 'axios'
 
import Toast from 'react-native-toast-native';
import ToastStyle from "../StyleSheet/ToastStyle";
import {loggedinUserdetails,} from '../dependency/UtilityFunctions'

 
import { CHANNEL } from '../constants/Constants';
import { PersistData,generate_id } from '../dependency/UtilityFunctions';

import {NavigationActions,navigation} from 'react-navigation';
import { withNavigation } from 'react-navigation';

import { onSignIn } from "../auth";
// import firebase from 'firebase';

class Chat {
    @observable newCaseData = {
      case_title:'',
      case_number:'',  
      Type:'',  
      Description:'',
      }
    @observable myContact = [];
    @observable myChatlist = [];
    @observable previousChat = [];
    @observable disabled = false
    @observable loader_visible = false
    @observable appendnow = false
    
    @observable color = '#F0BA00'
  
  
    @action  _ToggleLoader = (status) => {
this.loader_visible=status;

    }
    @action  _updateappend = (status) => {
this.appendnow=status;

    }

    
    GetMyContact = () => {
      console.warn("get contact")
     let userid=global.userid;
       this._ToggleLoader(true)
           console.warn(userid);
              axios.get( `/chat/list?id=${userid}`)
                       .then( response => {
                         let resp=response.data;
                         console.warn(resp);
                      if(resp.StatusCode === '00') {
                     let data=resp.Data;
                   //  console.warn(data)
            this.myContact=data
           
                this._ToggleLoader(false)
                         }
                         else {
                             console.warn('error');
                             let resp=response.data;
                             console.warn(resp);
                             let message =resp.Message
                             Toast.show(message, Toast.SHORT, Toast.BOTTOM,ToastStyle);
                             this.myContact=[]
                            this._ToggleLoader(false)
                         }  
                       } )
                       .catch(error => {
                           console.warn(`error ${error}`);
                           this.myContact=[];
                            this._ToggleLoader(false)
                       });
          }
        
    GetMyChatlist = () => {
      console.warn("get chatlist")
     let userid=global.userid;
    //  let userid=2126;
       this._ToggleLoader(true)
           console.warn(userid);
              axios.get( `/chat/lastreceivedorsentmessages/${userid}`)
                       .then( response => {
                         let resp=response.data;
                         console.warn(resp);
                      if(resp.StatusCode === '00') {
                     let data=resp.Data;
                   //  console.warn(data)
            this.myChatlist=data
           
                this._ToggleLoader(false)
                         }
                         else {
                             console.warn('error');
                             let resp=response.data;
                             console.warn(resp);
                             let message =resp.Message
                             Toast.show(message, Toast.SHORT, Toast.BOTTOM,ToastStyle);
                             this.myChatlist=[]
                            this._ToggleLoader(false)
                         }  
                       } )
                       .catch(error => {
                           console.warn(`error ${error}`);
                           this.myChatlist=[];
                            this._ToggleLoader(false)
                       });
          }
        

          loadChatold  (RecipientId) {
let params={
  // "SenderId": global.userid,
  // "RecipientId": RecipientId,
  "SenderId": 7,
  "RecipientId": 1,
  "take":4          
          }
        
          console.warn(params);
        this.loader_visible=true
           axios.post('/chat/load', qs.stringify(params))
                .then( response => {
                    this.loader_visible=false    
                
                  console.warn(response)
                  let resp=response.data;
                 if(resp.StatusCode === '00') {
                  let Sdata=resp.Data;
                  console.warn(Sdata);
                  let id=0;
                  const formattedMessagedata = Sdata.map(singleData => {
                    let DateTimeArray = singleData.TimeSent.split('T');
                    let Dateaarray = DateTimeArray[0].split('-');
                    let Timearray = DateTimeArray[1].split(':');
                    //  console.warn(Dateaarray);
                    //  console.warn(Timearray); 
                    id ++;
                    return {
                      _id: id,
                      text: singleData.Message,
                      // createdAt: new Date(Date.UTC(2017, 7, 30, 18, 20, 0)),
                      createdAt: new Date(
                        Date.UTC(
                          Dateaarray[0],
                          Dateaarray[1],
                          Dateaarray[2],
                          Timearray[0],
                          Timearray[1],
                          0,
                        ),
                      ),
                      user: {
                        _id: singleData.SenderId,
                        name: 'Dav',
                      },
                      // image: ("http://i.imgur.com/XP2BE7q.jpg")
                      // image: ("../images/fourh.jpg")
                    };
                  });

                this.previousChat=formattedMessagedata;
                console.warn(formattedMessagedata)
                console.warn(this.previousChat)
                  }
                  else {
                    this.loader_visible=false    
                    console.warn('error');
                    let resp=response.data;
                    console.warn(resp);
                    let message =resp.Message
                    Toast.show(message, Toast.SHORT, Toast.BOTTOM,ToastStyle);
                      //  this._ToggleLoader(false)
                      this.previousChat=[];
                  }           
                })
                .catch(err => console.warn(err)) 
 
      
         
      };

      SaveChat2DB  (params) {
        // let params={
        //   "Message": "Hello awayu",
        //   "SenderId": 2087,
        //   "RecipientId": 2171
                  
        //           }
                
                  // console.warn(params);
                // this.loader_visible=true
                   axios.post('/chat/add', qs.stringify(params))
                        .then( response => {
                            this.loader_visible=false    
                        
                          console.warn(response)
                          let resp=response.data;
                         if(resp.StatusCode === '00') {
                        let message =resp.Message
                        Toast.show(message, Toast.SHORT, Toast.BOTTOM,ToastStyle);
                           }
                          else {
                            this.loader_visible=false    
                            // console.warn('error');
                            let resp=response.data;
                            // console.warn(resp);
                            let message =resp.Message
                            Toast.show(message, Toast.SHORT, Toast.BOTTOM,ToastStyle);
                              //  this._ToggleLoader(false)
                            
                          }           
                        })
                        .catch(err => console.warn(err)) 
         
              
                 
              };


           
 


     
}

const ChatStore = new Chat()
export default ChatStore
 
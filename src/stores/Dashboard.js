import { observable, action } from 'mobx';
import qs from 'qs';
import axios from 'axios'
import { runInAction } from "mobx";

import Toast from 'react-native-toast-native';
import ToastStyle from "../StyleSheet/ToastStyle";
import { CHANNEL } from '../constants/Constants';
import { PersistData, ShowNotification } from '../dependency/UtilityFunctions';
import { loggedinUserdetails, } from '../dependency/UtilityFunctions'

import { NavigationActions, navigation } from 'react-navigation';
import { withNavigation } from 'react-navigation';

import { onSignIn } from "../auth";
import ApiConnect from '../model/ApiConnect';
import { BASEURL } from '../dependency/Config';

class Dashboard {

  constructor(ApiModel) {
    this.apiCall = ApiModel;
  }


  @observable isJobApplySuccessful = false;
  @observable isGoBack = false;
  @observable isProcessing = false;

  @observable hasSetIntrest = true;
  @observable isModalVisible = false;
  @observable isAlertVisible = false;
  @observable isHomeModalVisible = false;
  @observable isReferModalVisible = false;
  @observable isApplyModalVisible = false;
  @observable isSuccessModalVisible = false;
  @observable isSuccessPostModalVisible = false;
  @observable newreferalJobSuccess = false;
  @observable AxiosInstance = {};
  @observable LoadersActivity = {
    JobBasedIntrest: false
  };
  @observable IntrestSettings = {
    AllSystemIntrest: [],
    SelectedSystemIntrest: [],
    ErrorMessage: ''
  };
  @observable LoggedInUserProfile = {};
  @observable profileupdatedata = {};
  @observable navigationState = {}
  @observable SkillManagement = {
    companyName: '',
    title: '',
    startdate: '',
    enddate: '',
    achievement: '',
    current_id: '',
    HeaderText: 'Add New Experience',
  }
  @observable EducationManagement = {
    school: "",
    degree: "",
    field_of_study: "",
    start_date: "",
    end_date: "",
    description: "",
    current_id: '',
    HeaderText: 'Add New Education',
  }
  @observable ListMyExperience = {
    Myexperience: [],
    ErrorMessage: 'Loading'
  };
  @observable ListMyEducation = {
    MyEducation: [],
    ErrorMessage: 'Loading'
  };
  @observable ListMyKeywords = {
    Keywords: [],
    ErrorMessage: 'Loading...'
  };
  @observable SocialLinks = {
    LinkedIn: '',
    Portfolio: '',
    Facebook: '',
    Twitter: '',
  };
  @observable IntrestSettings = {
    AllSystemIntrest: [],
    SelectedSystemIntrest: [],
    ErrorMessage: ''
  };
  @observable SystemJoblevel = {
    Joblevel: [],
    ErrorMessage: ''
  };
  @observable ALLSkillWithOutFilter = {
    ALLSkill: [],
    ErrorMessage: ''
  };
  @observable AllIndustries = {
    Industry: [],
    IndustryV2: [],
    ErrorMessage: ''
  };
  @observable JobPosting = {
    // companyName: 'Xpat Integrated Systems',
    // industry: '',
    // aboutCompany: 'Christine is from Omaha, Nebraska where she works as a full stack Ruby on Rails engineer at Flywheel, WordPress hosting for creatives. She has 12 years experience as a Java software engineer with a focus on APIs and micro-services and took the plunge into Ruby just this May. ',
    // location: 'Abuja',
    // numberOfStaff: '',
    // roleTitle: 'Software Engineer',
    // jobLevel: '',
    // wordForApplicant: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation.',
    companyName: '',
    industry: '',
    aboutCompany: '',
    location: '',
    numberOfStaff: '',
    roleTitle: '',
    jobLevel: '',
    wordForApplicant: '',
    intrest: [],
    additional_files: [],
    current_id: '',
    isCompanyRepresentative: 1,
    isToRefTimeLineOrToHireRecruter: 1,
    HeaderText: 'New Post',
    SelectedRecuiter: [],
    job_type: '',
  }
  @observable JobOppurtunity = {
    JobsOffer: [],
    SingleJobOffer: {},
    ErrorMessage: ''
  };
  @observable Recruiters = {
    AvailableRecruiters: [],
    CurrentJobId:0,
    SingleRecruiter: {},
    ErrorMessage: 'No Data Found'
  };
  @observable BecomeRecruiter = {
    fullname: "",
    email: "",
    phone: "",
    summary: "",
    profilePicture: "https://res.cloudinary.com/workbrook-hash/image/upload/v1597880343/Mobile_Assets/userNew_xw8bir.png",
    averageAmount: "",
    coordinate: "",
    industry: [],
    location: "",
    industryRecruitementExperience: "",
    linkdnUrl: "",
    additionalFiles: ["https://res.cloudinary.com/workbrook-hash/image/upload/v1597880343/Mobile_Assets/userNew_xw8bir.png"],
    HeaderText: 'Become a recruiter',
  }
  @observable FilterRecruiter = {
    industry: "",
    location: "",
    industryRecruitementExperience: "",
    HeaderText: 'Filters',
  }
  @observable JobOffers = {
    totalApplicant: 0,
    jobList: [],
    singleJob: {},
    HeaderText: 'Filters',
    ErrorMessage: '',
  }
  @observable JobApplicants = {
    ApplicantList: [],
    TotalCounter: 0,
    singleApplicant: {},
    HeaderText: 'Showing All Applicant',
    ErrorMessage: '',
  }
  @observable ProcessJobApplication = {
    //  ApplicantList:[],
    stage_code: 0,
    custom_message: "",
    HeaderText: 'View Applicant',
    ErrorMessage: '',
    buttonText: '',
    defaultMessage: '',
    job_id: '',
    Applicant_id: '',
    additional_files: []
  }


  @observable ReferalStatistics = {
    clicksByReferral: 0,
    appliedForJob: 0,
    gotEmployed: 0,
    ErrorMessage: ''
  }
  @observable MyRefarals = {
    referalsList: [],
    ErrorMessage: ''
  }
  @observable ApplicationsStatistics = {
    profileVisit: 0,
    successfulReferral: 0,
    cvViews: 0,
    ErrorMessage: ''
  }
  @observable MyApplications = {
    ApplicationsList: [],
    singleApplication: {},
    ErrorMessage: ''
  }
  @observable ChangePassword = {
    currentpassword: '',
    newpassword: '',
    confirmpasssword: ''
  }
  @observable NotificationSettings_ = {
    all_notification: false,
    email_notification: false,
    sms_notification: false,
    workbrook_updates: false,
    ErrorMessage: ''
  }
  @observable OtherProfile = {
    ProfileData: {},
    skills: [],
    experience: [],
    education: [],
    User_id: '5',
    ErrorMessage: ''
  }
  @observable WithdrawApplication = {
    message: "",
    job_id: '',
    // skills:[],
    // experience:[],
    // education:[],
    // User_id:'5',
    // ErrorMessage:''
  }
  @observable WbLeague = {
    myPosition: {},
    data: {},
    ErrorMessage: ''
  }
  @observable WbLeaguePointAggregation = {
    // profilePoint:0,
    // referralPoint_count:0,
    // referralPoint_totalRefPoint:0,
    grandTotal: 0,
    execRef: {},
    graduateRef: {},
    invitedAFriend: {},
    mGRef: {},
    nMRef: {},
    profilePoint: {},
    sMRef: {},
    signUP: {},
    ErrorMessage: ''
  }
  @observable WbLeagueList = {
    GeneralList: [],
    ErrorMessage: ''
  }
  @observable StaticData = {
    CountryList: [{ label: 'State 1', value: 'State1' }],
    StateInCOuntry: [],
    ErrorMessage: ''
  }
  @observable InviteList = {
    Myinvites: [],
    ErrorMessage: '',
    SingleInvite: {}
  }
  @observable RecruiterDashBoard = {
    Myrequest: [],
    ErrorMessage: '',
    SingleRequest: {}
  }

  @observable RedirectMode = {
    ResetToken: false,
    LoginPage: false,
    withdrawApplication: false,
    RecruiterAvailable: false,
    AccountSettings: false,
    UpdateApplicantstage: false,
    SubscriptionModal: false,
    rejectApplicant: false,
    CurrentRedirect: "",
  }

  @observable JobRecruiters = {
    RecruiterList: [],
    TotalCounter: 0,
    singleApplicant: {},
    HeaderText: 'Showing All Applicant',
    ErrorMessage: '',
  }
  @observable RecruiterInsight = {
    MyAcceptedJobs: [],
    ErrorMessage: '',
    SingleAcceptedJob: {},
    isNavigationGoback:false
  }
  @observable ShortlistedApplicant = {
    MyShorlisted: [],
    ErrorMessage: '',
    isNavigationGoback:false,
  }
  @observable MySavedJobs = {
    MySavedjoblist: [],
    ErrorMessage: '',
  }
  @observable SystemNotification = {
    MyNotification: [],
    ErrorMessage: '',
  }
  @observable MyRecruiterProfile = {
    RecruiterProfile: {},
    ErrorMessage: '',
  }
  @observable MaxFileUpload = {
    // Genaral:2000000
    Genaral: 3
  }

  @observable RecruiterProfile = {}
  @observable JoblikeObject = {
    JobId: '',
    mylikes:[],  
    ErrorMessage: '',

  }
  @observable JobSeggestions = {
    MyJobs:[],  
    ErrorMessage: '',

  }
  @observable CardsInSystem = {
    Mycards:[],  
    ErrorMessage: '',

  }
  @observable TransactionInHistory = {
    TransactionHistory:[],  
    ErrorMessage: '',

  }
  @observable WalletBalance = {
    Balance:[],  
    ErrorMessage: '',

  }
  @observable FundWallet = {
    amount:'',
    card_id:''

  }
  @observable SubscriptionConfig = {
    subscriptions:[],
    subscriptions_jobs:[],
    subscriptions_recruiter:[],
    SingleSubConfig:{},
    ErrorMessage: '',
  }
  @observable Subscribtions = {
    MyactiveSub:[],
     ErrorMessage: '',
  }

  @observable AppUsersList = {
    Userlist: [],
    queryString: 0,
  Shortlistdata:{},
    showSuccessReplyModal:false,
  }
  @observable MySharedJobs = {
    MyJobList: [],
    SingleSharedJob: {},

    // queryString: 0,
  
    // showSuccessReplyModal:false,
  }
  @observable SharedApplicant = {
    SharedApplicantList: [],
    ErrorMessage:'',
    SendPayload:{},
    showSuccessReplyModal:false
    // queryString: 0,
  
    // showSuccessReplyModal:false,
  }
  @observable SharedApplicant = {
    SharedApplicantList: [],
    ErrorMessage:'',
    SendPayload:{},
    showSuccessReplyModal:false
    // queryString: 0,
   }
  @observable MyAnalytics = {
    totalRequestAndAcceptance: {
      "total_request": "0",
      "total_accepted": "0",
      "acceptance_rate": 0
    },
    completedJobsLevel:{
      "completed_executiive": "0",
      "completed_seniorManager": "0",
      "completed_manager": "0",
      "completed_nonManager": "0",
      "completed_graduate": ""
    },
    shortlistAndInterviewRatio:{
      "total": "",
      "shorlistedCount": "",
      "interviewsCount": "0",
      "shortlistAndInterviewPercentage": "0.0000"
    },
    reviews:[],
    average_rating:0,
    timeToCompleteShortlist: {
      "average_days": 29
    },
    recruiterComplitedByIndustry: [],
    ErrorMessage:'',
 
  }
  @observable UserAnalytics = {
    totalRequestAndAcceptance: {
      "total_request": "0",
      "total_accepted": "0",
      "acceptance_rate": 0
    },
    completedJobsLevel:{
      "completed_executiive": "0",
      "completed_seniorManager": "0",
      "completed_manager": "0",
      "completed_nonManager": "0",
      "completed_graduate": ""
    },
    shortlistAndInterviewRatio:{
      "total": "",
      "shorlistedCount": "",
      "interviewsCount": "0",
      "shortlistAndInterviewPercentage": "0.0000"
    },
    currentUser:null,
    reviews:[],
    average_rating:0,
    timeToCompleteShortlist: {
      "average_days": 29
    },
  recruiterComplitedByIndustry: [],

    ErrorMessage:'',
 
  }
  @observable JobpostedMessage = "Your job was successfully posted."

  @observable JobSearch = {
    SearchList: [],
    // CurrentJobId:0,
    // SingleRecruiter: {},
    ErrorMessage: 'No Data Found'
  };
  /////////////////////////////////END OF DECLEARATION////////////////////////////////////////
  @action _ToggleLoader = (Loader_type = null, status) => {
    if (Loader_type === null) {
      // console.warn("news loaderr")
      this.Pagelodders['loader_visible'] = status;
    }
    else {
      console.log("enter here")
      this.Pagelodders[Loader_type] = status;
    }

    // dashboardStore
  }
  @action _updateAxiosInstance = (instance) => {
    this.AxiosInstance = instance;
  }
  @action _UpdateUserSetIntrest = (status) => {
    this.hasSetIntrest = status;
  }
  @action _UpdateUserSetIntrest = (status) => {
    this.hasSetIntrest = status;
  }
  @action _getAxiosInstance = (UserLoginToken) => {
    var instance = axios.create({
      baseURL: BASEURL,
      timeout: 5000,
      headers: {
        'Authorization': UserLoginToken,
        // 'Authorization': "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJ3b3JrYnJvb2submV0IiwiYXVkIjoid29ya2Jyb29rLm5ldCIsImlhdCI6MTU4NzEzMDc5NiwibmJmIjoxNTg3MTMwNzk2LCJleHAiOjE1ODcxMzQzOTYsImVtYWlsIjoiSWFtZGF2aWRAZ21haWwuY29tIiwidXNlcm5hbWUiOiJJYW1kYXZpZCIsImlkIjoiMjYifQ.y-aSQzUgC9Ah6aZWACPcuxO2cTaPThLb2AcqSkJZ7w4",
        'Content-Type': "application/json",

      }

    });
    this._updateAxiosInstance(instance);
  }
  @action UpdateNavigationState = (navstate) => {
    console.log(navstate)
    this.navigationState = navstate;
  }

  @action _ToggleProcessing = (status = false) => {
    this.isProcessing = status;
  }
  @action _ToggleModalVisible = (status = false) => {
    this.isModalVisible = status;
  }
  @action _ToggleHomeModal = (status = false) => {
    this.isHomeModalVisible = status;
  }
  @action _ToggleReferModal = (status = false) => {
    this.isReferModalVisible = status;
  }
  @action _ToggleApplyModal = (status = false) => {
    this.isApplyModalVisible = status;
  }
  @action _ToggleAlertModal = (status) => {
    this.isAlertVisible = status;
  }
  @action _ToggleSucessModal = (status = false) => {
    this.isSuccessModalVisible = status;
  }
  @action _ToggleSucessPostModal = (status = false) => {
    this.isSuccessPostModalVisible = status;
  }
  @action _ToggleModal___ = (Parent = "newreferalJobSuccess", status = false) => {
    this[Parent] = status;
  }
  @action _UpdateLoginToken = (token) => {
    this.UserLoginToken = token;
  }
  @action _UpdateLoggedInUserProfile = (Profile) => {
    this.LoggedInUserProfile = Profile;
    this.profileupdatedata = Profile;
  }
  @action __ToggleIntrestArray = (user_intrest) => {
    console.log(user_intrest)
    if (user_intrest === 'empty') {
      DashboardStore.IntrestSettings.SelectedSystemIntrest = [];
    }
    else {
      let thisArray = DashboardStore.IntrestSettings.SelectedSystemIntrest;
      var check = thisArray.includes(user_intrest);
      if (check === true) {
        console.warn("true")
        thisArray.splice(thisArray.indexOf(user_intrest), 1);
        DashboardStore.IntrestSettings.SelectedSystemIntrest = thisArray;
      }
      else {
        console.warn("false")
        thisArray[thisArray.length] = user_intrest;
        DashboardStore.IntrestSettings.SelectedSystemIntrest = thisArray;

      }

    }
    console.log(DashboardStore.IntrestSettings.SelectedSystemIntrest.slice());
    // CaseStore.currentLawyerId=user_intrest;

  }
  @action __ToggleRecruiterArray = (recruiter_id) => {
    console.log(recruiter_id)
    if (recruiter_id === 'empty') {

      DashboardStore.JobPosting.SelectedRecuiter = [];
    }
    else {
      let thisArray = DashboardStore.JobPosting.SelectedRecuiter;
      var check = thisArray.includes(recruiter_id);
      if (check === true) {
        console.warn("true")
        thisArray.splice(thisArray.indexOf(recruiter_id), 1);
        DashboardStore.JobPosting.SelectedRecuiter = thisArray;
        let message = "Recruiter Removed";
        ShowNotification(message);
      }
      else {
        console.warn("false")
        thisArray[thisArray.length] = recruiter_id;
        DashboardStore.JobPosting.SelectedRecuiter = thisArray;
        let message = "Recruiter Selected";
        ShowNotification(message);
      }

    }
    console.log(DashboardStore.JobPosting.SelectedRecuiter.slice());
    // CaseStore.currentLawyerId=recruiter_id;

  }

  @action onChangeText = (key, value, type) => {
    console.log(value)


    if (key === 'password') {
      // this[type][key] = value;
      let response = PasswordCheck(value);
      this[type]['isPasswordError'] = response;
    }
    if (key === 'achievement') {
      if (this[type][key].length > 100) {
        return;
      }
      // else{
      //   // this[type][key] = value;
      // }
    }
    this[type][key] = value;
    // console.warn(accountStore.regInfo)

  }
  @action _updateParameterV2 = (value, Feild, parent) => {
    let newvalue = { [Feild]: value }
    let jj = { ...DashboardStore[parent], ...newvalue }
    console.log(jj)
    console.log("<<<<jj>>>>>");
    DashboardStore[parent] = jj;
    // console.log(DashboardStore[parent]);

  }
  @action _UpdateDefaultValue = () => {
    this.isGoBack = false;
  }
  @action __ResetJobpostingData = () => {
    let JobPosting = {
      companyName: '',
      industry: '',
      aboutCompany: '',
      location: '',
      numberOfStaff: '',
      roleTitle: '',
      jobLevel: '',
      wordForApplicant: '',
      intrest: [],
      additional_files: [],
      current_id: '',
      isCompanyRepresentative: 1,
      isToRefTimeLineOrToHireRecruter: 1,
      HeaderText: 'New Post',
      SelectedRecuiter: [],
    }
    DashboardStore.JobPosting = JobPosting;

  }
  @action _ResetRedirectMode2Default = () => {
    DashboardStore._updateParameterV2("David_Nill", "CurrentRedirect", "RedirectMode");
  }
  ////////////////////////////////////////////Request//////////////////////

  _FetchAllIntrest = async (pageNumber, pagesize) => {

    DashboardStore._ToggleProcessing(true)
    const result = await this.apiCall.FetchAllIntrest(pageNumber, pagesize);

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let data = resp.data;
          console.log(data)
          let suggestedIntrest = data[0];
          console.log("data>>>")

          runInAction(() => {

            DashboardStore.IntrestSettings.AllSystemIntrest = suggestedIntrest;
            // DashboardStore._ToggleLoader(false)
            //  setTimeout(() => {
            //   // DashboardStore.loggedinUserData = data;
            //   // this.SaveUserData(data);
            // }, 2000);

          });

        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
        }

        // this._ToggleLoader('Categories',false)

      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);


    }




  }
  __SaveMyIntrest = async () => {


    const result = await this.apiCall.SaveMyIntrest();

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
          //  DashboardStore._UpdateUserSetIntrest(true);
          DashboardStore.__FetchAllMyKeywords();
          DashboardStore.ProceedWithIntrestRecord();
          DashboardStore.navigationState.navigate("profile");

          // runInAction(() => {

          //   // DashboardStore.IntrestSettings.AllSystemIntrest = suggestedIntrest;


          // });

        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
        }

        // this._ToggleLoader('Categories',false)

      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);


    }




  }
  ProceedWithIntrestRecord = () => {
    // alert("start")
    // this.props.navigation.navigate("Landingpage");
    // this.props.navigation.navigate("Landingpage");

    global.storage.save({
      key: 'hasSetIntrest',   // Note: Do not use underscore("_") in key! 
      data: {
        hasSetIntrest: true,
        when: 'today'

      },

      // if not specified, the defaultExpires will be applied instead. 
      // if set to null, then it will never expire. 
      expires: null
    });

    DashboardStore._UpdateUserSetIntrest(true);

  }

  __UpdateProfile = async () => {


    const result = await this.apiCall.UpdateProfile();

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let message = resp.message;
          let dataResponse = resp.data;
          console.log(message);
          console.log(dataResponse);
          let previousProfiledata = DashboardStore.LoggedInUserProfile
          console.log(previousProfiledata)
          let NewData2pasist = Object.assign({}, previousProfiledata, dataResponse);
          console.log(NewData2pasist)

          //  setTimeout(() => {
          ShowNotification(message, false);
          //        // AccountStore.navigationState.navigate("profile");
          //  }, 1000);
          DashboardStore._UpdateLoggedInUserProfile(NewData2pasist);
          PersistData('loggedinUserdetails', NewData2pasist);
          //  DashboardStore.navigationState.navigation.goBack();
          DashboardStore.navigationState.navigate("profile");
          DashboardStore.__GetMyCurrentProfile();

          //  DashboardStore._UpdateUserSetIntrest(true);
          // DashboardStore.ProceedWithIntrestRecord();

          // runInAction(() => {

          //   // DashboardStore.IntrestSettings.AllSystemIntrest = suggestedIntrest;


          // });

        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
        }

        // this._ToggleLoader('Categories',false)

      }
      else {

        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        console.log("heree<><tobi" + message)
        ShowNotification(message, false);

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);


    }




  }
  __UpdateProfile_external = async () => {


    const result = await this.apiCall.UpdateProfile();

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let message = resp.message;
          let dataResponse = resp.data;
          console.log(message)
          console.log(dataResponse)
          let previousProfiledata = DashboardStore.LoggedInUserProfile
          console.log(previousProfiledata)
          let NewData2pasist = Object.assign({}, previousProfiledata, dataResponse);
          console.log(NewData2pasist)

          //  setTimeout(() => {
          //   ShowNotification(message, true);
          //        // AccountStore.navigationState.navigate("profile");
          //  }, 1000);
          DashboardStore._UpdateLoggedInUserProfile(NewData2pasist);
          PersistData('loggedinUserdetails', NewData2pasist);
          let CurrentRedirect = DashboardStore.RedirectMode.CurrentRedirect;
          //   DashboardStore._UpdateDefaultValue();
          DashboardStore._updateParameterV2(true, CurrentRedirect, "RedirectMode");
          DashboardStore.__GetMyCurrentProfile();
          //  DashboardStore.navigationState.navigation.goBack();
          //  DashboardStore.navigationState.navigate("profile");
          //  DashboardStore._UpdateUserSetIntrest(true);
          // DashboardStore.ProceedWithIntrestRecord();

          // runInAction(() => {

          //   // DashboardStore.IntrestSettings.AllSystemIntrest = suggestedIntrest;


          // });
          ShowNotification(message, false);

        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
          DashboardStore._ResetRedirectMode2Default();

        }

        // this._ToggleLoader('Categories',false)

      }
      else {

        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        console.log("heree<><tobi" + message)
        ShowNotification(message, false);
        DashboardStore._ResetRedirectMode2Default();


      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      DashboardStore._ResetRedirectMode2Default();


    }




  }

  __processSkillManagement = async () => {


    const result = await this.apiCall.processSkillManagement();

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let message = resp.message;
          let dataResponse = resp.data;
          console.log(message)
          console.log(dataResponse)
          //  setTimeout(() => {

          //            // AccountStore.navigationState.navigate("ResetPassword");
          //      }, 1000);
          ShowNotification(message, true);
          //  DashboardStore.navigationState.navigation.goBack();
          DashboardStore.__FetchAllMyExperience();

          DashboardStore.navigationState.navigate("profile");
        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
        }

        // this._ToggleLoader('Categories',false)

      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);


    }




  }
  __processEducationManagement = async () => {


    const result = await this.apiCall.processEducationManagement();

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let message = resp.message;
          let dataResponse = resp.data;
          console.log(message)
          console.log(dataResponse)
          //  setTimeout(() => {

          //            // AccountStore.navigationState.navigate("ResetPassword");
          //      }, 1000);
          ShowNotification(message, true);
          //  DashboardStore.navigationState.navigation.goBack();
          DashboardStore.__FetchAllMyEducation();

          DashboardStore.navigationState.navigate("profile");
        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
        }

        // this._ToggleLoader('Categories',false)

      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);


    }




  }

  __FetchAllMyExperience = async (pageNumber, pagesize) => {

    DashboardStore._ToggleProcessing(true)
    const result = await this.apiCall.FetchAllMyExperience();

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let data = resp.data;
          console.log(data)
          let thisrecord = data;
          console.log("data>>>")

          runInAction(() => {

            DashboardStore.ListMyExperience.Myexperience = thisrecord;
            // DashboardStore._ToggleLoader(false)
            //  setTimeout(() => {
            //   // DashboardStore.loggedinUserData = data;
            //   // this.SaveUserData(data);
            // }, 2000);

          });

        }
        else {
          //   this.Categories = []


          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
          DashboardStore.ListMyExperience.Myexperience = [];
          DashboardStore.ListMyExperience.ErrorMessage = message;
        }

        // this._ToggleLoader('Categories',false)

      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        DashboardStore.ListMyExperience.Myexperience = [];
        DashboardStore.ListMyExperience.ErrorMessage = message;

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      DashboardStore.ListMyExperience.Myexperience = [];
      DashboardStore.ListMyExperience.ErrorMessage = "Sorry Error Occured";

    }




  }
  __FetchAllMyEducation = async (pageNumber, pagesize) => {

    DashboardStore._ToggleProcessing(true)
    const result = await this.apiCall.FetchAllMyEducation();

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let data = resp.data;
          console.log(data)
          let thisrecord = data;
          console.log("data>>>")

          runInAction(() => {

            DashboardStore.ListMyEducation.MyEducation = thisrecord;

          });

        }
        else {
          //   this.Categories = []


          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
          DashboardStore.ListMyEducation.MyEducation = [];
          DashboardStore.ListMyEducation.ErrorMessage = message;
        }

        // this._ToggleLoader('Categories',false)

      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        DashboardStore.ListMyEducation.MyEducation = [];
        DashboardStore.ListMyEducation.ErrorMessage = message;

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      DashboardStore.ListMyEducation.MyEducation = [];
      DashboardStore.ListMyEducation.ErrorMessage = "Sorry Error Occured";

    }




  }

  __FetchAllMyKeywords = async () => {

    DashboardStore._ToggleProcessing(true)
    const result = await this.apiCall.FetchAllMyKeywords();

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let data = resp.data;
          console.log(data)
          let thisrecord = [];
          if (data.length > 0) {
            let myskill = data[0]["skills"];
            console.log(myskill)
            thisrecord = myskill;
          }
          else {
            thisrecord = [];
          }

          console.log("data>>>")

          runInAction(() => {

            DashboardStore.ListMyKeywords.Keywords = thisrecord;
            // DashboardStore._ToggleLoader(false)
            //  setTimeout(() => {
            //   // DashboardStore.loggedinUserData = data;
            //   // this.SaveUserData(data);
            // }, 2000);

          });

        }
        else {
          //   this.Categories = []


          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
          DashboardStore.ListMyKeywords.Keywords = [];
          DashboardStore.ListMyKeywords.ErrorMessage = message;
        }

        // this._ToggleLoader('Categories',false)

      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        DashboardStore.ListMyKeywords.Keywords = [];
        DashboardStore.ListMyKeywords.ErrorMessage = message;

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      DashboardStore.ListMyKeywords.Keywords = [];
      DashboardStore.ListMyKeywords.ErrorMessage = "Sorry Error Occured";

    }




  }
  __GetIndustries = async (pageNumber, pagesize) => {

    DashboardStore._ToggleProcessing(true)
    const result = await this.apiCall.GetIndustries(pageNumber, pagesize);

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let data = resp.data;
          console.log(data)
          console.log("GetIndustries")
          //  let thisrecord=[];
          //  if(data.length>0){
          //    let myskill=data[0]["skills"];
          //    console.log(myskill)
          //   thisrecord=myskill;
          //  }
          //  else{
          //   thisrecord=[];
          //  }

          console.log("data>>>")

          runInAction(() => {

            DashboardStore.ListMyKeywords.Keywords = thisrecord;
            // DashboardStore._ToggleLoader(false)
            //  setTimeout(() => {
            //   // DashboardStore.loggedinUserData = data;
            //   // this.SaveUserData(data);
            // }, 2000);

          });

        }
        else {
          //   this.Categories = []


          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
          DashboardStore.ListMyKeywords.Keywords = [];
          DashboardStore.ListMyKeywords.ErrorMessage = message;
        }

        // this._ToggleLoader('Categories',false)

      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        DashboardStore.ListMyKeywords.Keywords = [];
        DashboardStore.ListMyKeywords.ErrorMessage = message;

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      DashboardStore.ListMyKeywords.Keywords = [];
      DashboardStore.ListMyKeywords.ErrorMessage = "Sorry Error Occured";

    }




  }
  __ALLSkillWithOutFilter = async (pageNumber, pagesize) => {

    DashboardStore._ToggleProcessing(true)
    const result = await this.apiCall.ALLSkillWithOutFilter(pageNumber, pagesize);

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let data = resp.data;
          console.log(data)
          let thisrecord = data[0];
          const FormattedData = thisrecord.map(singleData => {

            // let obj2 = Object.assign({}, obj1);
            return {

              id: singleData.title,
              name: singleData.title,

            }

          });
          console.log("ALLSkillWithOutFilter")

          console.log("data>>>")
          runInAction(() => {
            DashboardStore.ALLSkillWithOutFilter.ALLSkill = FormattedData;
          });

        }
        else {
          //   this.Categories = []


          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
          DashboardStore.ALLSkillWithOutFilter.ALLSkill = [];
          DashboardStore.ALLSkillWithOutFilter.ErrorMessage = message;
        }

        // this._ToggleLoader('Categories',false)

      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        DashboardStore.ALLSkillWithOutFilter.ALLSkill = [];
        DashboardStore.ALLSkillWithOutFilter.ErrorMessage = message;

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      DashboardStore.ALLSkillWithOutFilter.ALLSkill = [];
      DashboardStore.ALLSkillWithOutFilter.ErrorMessage = "Sorry Error Occured";

    }




  }
  __JobsLevels = async () => {

    DashboardStore._ToggleProcessing(true)
    const result = await this.apiCall.JobsLevels();

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let data = resp.data;
          console.log(data)
          console.log("JobsLevels")
          //  let thisrecord=[];
          //  if(data.length>0){
          //    let myskill=data[0]["skills"];
          //    console.log(myskill)
          //   thisrecord=myskill;
          //  }
          //  else{
          //   thisrecord=[];
          //  }
          // SystemJoblevel
          const FormattedData = data.map(singleData => {

            // let obj2 = Object.assign({}, obj1);
            return {

              id: singleData.id,
              name: singleData.title,

            }

          });
          console.log("data>>>")

          runInAction(() => {
            DashboardStore.SystemJoblevel.Joblevel = FormattedData;
          });

        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
          DashboardStore.SystemJoblevel.Joblevel = [];
          DashboardStore.SystemJoblevel.ErrorMessage = message;
        }
      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        DashboardStore.SystemJoblevel.Joblevel = [];
        DashboardStore.SystemJoblevel.ErrorMessage = message;

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      DashboardStore.SystemJoblevel.Joblevel = [];
      DashboardStore.SystemJoblevel.ErrorMessage = "Sorry Error Occured";
    }
  }

  __ProccessJobPosting = async () => {

   // DashboardStore._ToggleModal___("newreferalJobSuccess", true);

    const result = await this.apiCall.ProccessJobPosting();

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let message = resp.message;
          let dataResponse = resp.data;
          console.log(message)
          console.log(dataResponse)
           setTimeout(() => {

            DashboardStore._ToggleModal___("newreferalJobSuccess", true);
          }, 1000);


          ShowNotification(message, false);
          // DashboardStore.navigationState.navigation.goBack();
          // DashboardStore.isGoBack=true;

          //  DashboardStore.navigationState.navigate("profile");
        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, false);
        }

        // this._ToggleLoader('Categories',false)

      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, false);

      }
    }
    else {
      ShowNotification('Generic Error Occured', false);


    }




  }



  __ApplyForJob = async () => {


    const result = await this.apiCall.ApplyForJob();

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let message = resp.message;
          let dataResponse = resp.data;
          console.log(message)
          console.log(dataResponse)

          ShowNotification(message, false);
          //  DashboardStore.navigationState.navigation.goBack();
          DashboardStore.isJobApplySuccessful = true;
          DashboardStore._ToggleHomeModal(false)
          DashboardStore._ToggleApplyModal(false)
        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, false);
          DashboardStore.isJobApplySuccessful = false
          DashboardStore._ToggleHomeModal(false)
          DashboardStore._ToggleApplyModal(false)
          // DashboardStore._ToggleAlertModal(true)
        }

        // this._ToggleLoader('Categories',false)

      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        // let message = resp.message
        let message ="You've already applied to this job";
        ShowNotification(message, true);
        DashboardStore.isJobApplySuccessful = false
        DashboardStore._ToggleHomeModal(false)
        DashboardStore._ToggleApplyModal(false)
        DashboardStore._ToggleAlertModal(true)

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      DashboardStore.isJobApplySuccessful = false
      DashboardStore._ToggleHomeModal(false)
      DashboardStore._ToggleApplyModal(false)

    }




  }


  __GetJobsOppurtunites = async (pageNumber, pagesize) => {

    DashboardStore._ToggleProcessing(true)
    DashboardStore._updateParameterV2(true, "JobBasedIntrest", "LoadersActivity");

    const result = await this.apiCall.GetJobsOppurtunites(pageNumber, pagesize);

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    DashboardStore._updateParameterV2(false, "JobBasedIntrest", "LoadersActivity");

    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let data = resp.data;
          console.log(data)
          console.log("My jobs ")
          let thisOffers = data[0];
          console.log("GetJobsOppurtunites")
          //  let thisrecord=[];
          //  if(data.length>0){
          //    let myskill=data[0]["skills"];
          //    console.log(myskill)
          //   thisrecord=myskill;
          //  }
          //  else{
          //   thisrecord=[];
          //  }
          // SystemJoblevel

          console.log("data>>>")

          runInAction(() => {
            DashboardStore.JobOppurtunity.JobsOffer = thisOffers;
          });

        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
          DashboardStore.JobOppurtunity.JobsOffer = [];
          DashboardStore.JobOppurtunity.ErrorMessage = message;
        }
      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        DashboardStore.JobOppurtunity.JobsOffer = [];
        DashboardStore.JobOppurtunity.ErrorMessage = message;

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      DashboardStore.JobOppurtunity.JobsOffer = [];
      DashboardStore.JobOppurtunity.ErrorMessage = "Sorry Error Occured";
    }
  }
  __GetRecruiters = async (pageNumber, pagesize,showloader=true) => {

    DashboardStore._ToggleProcessing(showloader);
    const result = await this.apiCall.GetRecruiters(pageNumber, pagesize);

    console.log(result)
    DashboardStore._ToggleProcessing(false);
    DashboardStore.Recruiters.CurrentJobId=0;
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let data = resp.data;
          //  console.log(data)
          let thisData = data[0];
          console.log("GetRecruiters")
          runInAction(() => {
            DashboardStore.Recruiters.AvailableRecruiters = thisData;
          });

        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
          DashboardStore.Recruiters.AvailableRecruiters = [];
          DashboardStore.Recruiters.ErrorMessage = message;
        }
      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        DashboardStore.Recruiters.AvailableRecruiters = [];
        DashboardStore.Recruiters.ErrorMessage = message;

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      DashboardStore.Recruiters.AvailableRecruiters = [];
      DashboardStore.Recruiters.ErrorMessage = "Sorry Error Occured";
    }
  }
  __LikeUnLike = async (jobId) => {


    const result = await this.apiCall.LikeUnLike(jobId);

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let message = resp.message;
          let dataResponse = resp.data;
          console.log(message)
          console.log(dataResponse)

          ShowNotification(message, false);
          //  DashboardStore.navigationState.navigation.goBack();
          // DashboardStore.isJobApplySuccessful = true
          DashboardStore.__GetJobsOppurtunites(1, 30);

        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, false);
          // DashboardStore.isJobApplySuccessful = false
        }

        // this._ToggleLoader('Categories',false)

      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        // DashboardStore.isJobApplySuccessful = false
      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      // DashboardStore.isJobApplySuccessful = false

    }




  }
  __SaveJobForLater = async (jobId) => {


    const result = await this.apiCall.SaveJobForLater(jobId);

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let message = resp.message;
          let dataResponse = resp.data;
          console.log(message)
          console.log(dataResponse)
          let customMessage = "successfully saved";
          ShowNotification(customMessage, true);
          DashboardStore.__GetJobsOppurtunites(1, 30);

          //  DashboardStore.navigationState.navigation.goBack();
          // DashboardStore.isJobApplySuccessful = true
        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, false);
          // DashboardStore.isJobApplySuccessful = false
        }

        // this._ToggleLoader('Categories',false)

      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        // DashboardStore.isJobApplySuccessful = false
      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      // DashboardStore.isJobApplySuccessful = false

    }




  }

  __ProccessBecomeRecruiter = async () => {


    const result = await this.apiCall.ProccessBecomeRecruiter();

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let message = resp.message;
          let dataResponse = resp.data;
          console.log(message)
          console.log(dataResponse)
          //  setTimeout(() => {

          //            // AccountStore.navigationState.navigate("ResetPassword");
          //      }, 1000);
          ShowNotification(message, false);
          DashboardStore.__GetMyCurrentProfile();
          // DashboardStore.navigationState.navigation.goBack()
          //  DashboardStore.navigationState.navigate("profile");
        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, false);
        }

        // this._ToggleLoader('Categories',false)

      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, false);

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);


    }




  }

  __GetUserIndustries = async () => {

    DashboardStore._ToggleProcessing(true)
    const result = await this.apiCall.GetUserIndustries();

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let data = resp.data;
          console.log(data)
          console.log("JobsLevels")
          //  let thisrecord=[];
          //  if(data.length>0){
          //    let myskill=data[0]["skills"];
          //    console.log(myskill)
          //   thisrecord=myskill;
          //  }
          //  else{
          //   thisrecord=[];
          //  }
          // SystemJoblevel
          const FormattedData = data.map(singleData => {
            // let obj2 = Object.assign({}, obj1);
            return {
              id: singleData.title,
              name: singleData.title,
            }

          });
          console.log("data>>>")

          runInAction(() => {
            DashboardStore.AllIndustries.Industry = FormattedData;
          });

        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
          DashboardStore.AllIndustries.Industry = [];
          DashboardStore.AllIndustries.ErrorMessage = message;
        }
      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        DashboardStore.AllIndustries.Industry = [];
        DashboardStore.AllIndustries.ErrorMessage = message;

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      DashboardStore.AllIndustries.Industry = [];
      DashboardStore.AllIndustries.ErrorMessage = "Sorry Error Occured";
    }
  }
  __GetUserIndustriesV2 = async () => {

    DashboardStore._ToggleProcessing(true)
    const result = await this.apiCall.GetUserIndustries();

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let data = resp.data;
          console.log(data)
          console.log("JobsLevels")
          //  let thisrecord=[];
          //  if(data.length>0){
          //    let myskill=data[0]["skills"];
          //    console.log(myskill)
          //   thisrecord=myskill;
          //  }
          //  else{
          //   thisrecord=[];
          //  }
          // SystemJoblevel
          const FormattedData = data.map(singleData => {
            // let obj2 = Object.assign({}, obj1);
            return {
              label: singleData.title,
              value: singleData.title,
            }
          });
          console.log("data>>>")

          runInAction(() => {
            DashboardStore.AllIndustries.IndustryV2 = FormattedData;
          });

        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
          DashboardStore.AllIndustries.IndustryV2 = [];
          DashboardStore.AllIndustries.ErrorMessage = message;
        }
      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        DashboardStore.AllIndustries.IndustryV2 = [];
        DashboardStore.AllIndustries.ErrorMessage = message;

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      DashboardStore.AllIndustries.IndustryV2 = [];
      DashboardStore.AllIndustries.ErrorMessage = "Sorry Error Occured";
    }
  }


  __FilterRecuiterSearch = async () => {

    DashboardStore._ToggleProcessing(true)
    const result = await this.apiCall.FilterRecuiterSearch();

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let data = resp.data;
          console.log(data)
          // let thisData = data[0];
          console.log("FilterRecuiterSearch")
          runInAction(() => {
            DashboardStore.Recruiters.AvailableRecruiters = data;
            DashboardStore.isGoBack = true;

          });

        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
          DashboardStore.Recruiters.AvailableRecruiters = [];
          DashboardStore.Recruiters.ErrorMessage = message;
        }
      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        DashboardStore.Recruiters.AvailableRecruiters = [];
        DashboardStore.Recruiters.ErrorMessage = message;

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      DashboardStore.Recruiters.AvailableRecruiters = [];
      DashboardStore.Recruiters.ErrorMessage = "Sorry Error Occured";
    }
  }
  __ProccessJobPosting2Recruiters = async () => {


    const result = await this.apiCall.ProccessJobPosting2Recruiters();

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let message = resp.message;
          let dataResponse = resp.data;
          console.log(message)
          console.log(dataResponse)
          //  setTimeout(() => {

          //            // AccountStore.navigationState.navigate("ResetPassword");
          //      }, 1000);
          ShowNotification(message, false);
          // DashboardStore.navigationState.navigation.goBack();
          // DashboardStore.isGoBack=true;
          DashboardStore.JobpostedMessage="Your job was successfully sent to the recruiters"
          DashboardStore._ToggleSucessPostModal(true);
          setTimeout(() => {

            DashboardStore._ToggleModal___("newreferalJobSuccess", true);
          }, 1000);
          //  DashboardStore.navigationState.navigate("profile");
        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, false);
        }

        // this._ToggleLoader('Categories',false)

      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);


    }




  }

  __GetJobOffers = async (pageNumber, pagesize) => {

    DashboardStore._ToggleProcessing(true)
    const result = await this.apiCall.GetJobOffers(pageNumber, pagesize);

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let data = resp.data;
          console.log(data)
          console.log("data my jobs insight ")
          // let thisData = data[0];
          let joblisting = data.jobs;
          let totalApplicants = data.total_applicants;
          runInAction(() => {
            DashboardStore.JobOffers.jobList = joblisting;
            DashboardStore.JobOffers.totalApplicant = totalApplicants;
            DashboardStore.JobOffers.ErrorMessage = joblisting.length < 1 ? 'No Record Found' : '';

            // DashboardStore.isGoBack=true;

          });

        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
          DashboardStore.JobOffers.jobList = [];
          DashboardStore.JobOffers.totalApplicant = 0;
          DashboardStore.JobOffers.ErrorMessage = message;
        }
      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        DashboardStore.JobOffers.jobList = [];
        DashboardStore.JobOffers.totalApplicant = 0;
        DashboardStore.JobOffers.ErrorMessage = message;

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      DashboardStore.JobOffers.jobList = [];
      DashboardStore.JobOffers.totalApplicant = 0;
      DashboardStore.JobOffers.ErrorMessage = "Sorry Error Occured";
    }
  }
  __GetJobApplicant = async (ApplicationStagestage, pageNumber, pagesize) => {

    DashboardStore._ToggleProcessing(true)
    const result = await this.apiCall.GetJobApplicant(ApplicationStagestage, pageNumber, pagesize);

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let data = resp.data;
          console.log(data)
          // let thisData = data[0];

          let Applicants = data.Applicants;
          let NewAplicant=[];
            NewAplicant =  Applicants.filter(function(data) {
            return data.isViewed  ==="0";
          });
            let Applicants_count = NewAplicant.count;
           runInAction(() => {
            DashboardStore.JobApplicants.ApplicantList = Applicants;
            DashboardStore.JobApplicants.TotalCounter = Applicants_count;
            DashboardStore.JobApplicants.ErrorMessage = Applicants.length < 1 ? 'No Applicant  Found' : '';

            // DashboardStore.isGoBack=true;

          });

        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
          DashboardStore.JobApplicants.ApplicantList = [];
          DashboardStore.JobApplicants.ErrorMessage = message;
        }
      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        DashboardStore.JobApplicants.ApplicantList = [];
        DashboardStore.JobApplicants.ErrorMessage = message;

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      DashboardStore.JobApplicants.ApplicantList = [];
      DashboardStore.JobApplicants.ErrorMessage = "Sorry Error Occured";
    }
  }
  __UpdateApplicationLevel = async (jobId) => {


    const result = await this.apiCall.UpdateApplicationLevel(jobId);

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let message = resp.message;
          let dataResponse = resp.data;
          console.log(message)
          console.log(dataResponse)
          ShowNotification(message);
          //  DashboardStore.navigationState.navigation.goBack();
          //  DashboardStore.isGoBack=true;
          let CurrentRedirect = DashboardStore.RedirectMode.CurrentRedirect;
          DashboardStore._updateParameterV2(true, CurrentRedirect, "RedirectMode");

        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, false);
          DashboardStore._ResetRedirectMode2Default();
        }

        // this._ToggleLoader('Categories',false)

      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        DashboardStore._ResetRedirectMode2Default();
      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      DashboardStore._ResetRedirectMode2Default();

    }




  }
  __RejectApplication = async (jobId) => {


    const result = await this.apiCall.RejectApplication(jobId);

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let message = resp.message;
          let dataResponse = resp.data;
          console.log(message)
          console.log(dataResponse)
          ShowNotification(message);   
          //  DashboardStore.navigationState.navigation.goBack();
          // DashboardStore.isGoBack = true;
          DashboardStore.__GetJobApplicant(5, 1, 30);
          // let CurrentRedirect = DashboardStore.RedirectMode.CurrentRedirect;
          DashboardStore._updateParameterV2(true, "rejectApplicant", "RedirectMode");

        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, false);
          // DashboardStore.isJobApplySuccessful = false
        }

        // this._ToggleLoader('Categories',false)

      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        // DashboardStore.isJobApplySuccessful = false
      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      // DashboardStore.isJobApplySuccessful = false

    }




  }
  __GetReferalStatistics = async () => {

    // DashboardStore._ToggleProcessing(true)
    const result = await this.apiCall.GetReferalStatistics();

    console.log(result)
    // DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let data = resp.data;
          console.log(data)
          // let thisData = data[0];
          let clicksByReferral = data.clicksByReferral;
          let appliedForJob = data.appliedForJob;
          let gotEmployed = data.gotEmployed;
          runInAction(() => {
            DashboardStore.ReferalStatistics.clicksByReferral = clicksByReferral;
            DashboardStore.ReferalStatistics.appliedForJob = appliedForJob;
            DashboardStore.ReferalStatistics.gotEmployed = gotEmployed;

            // DashboardStore.isGoBack=true;

          });

        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
          DashboardStore.ReferalStatistics.clicksByReferral = 0;
          DashboardStore.ReferalStatistics.createdAProfile = 0;
          DashboardStore.ReferalStatistics.appliedForJobs = 0;
          DashboardStore.ReferalStatistics.ErrorMessage = message;
        }
      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        DashboardStore.ReferalStatistics.clicksByReferral = 0;
        DashboardStore.ReferalStatistics.createdAProfile = 0;
        DashboardStore.ReferalStatistics.appliedForJobs = 0;
        DashboardStore.ReferalStatistics.ErrorMessage = message;

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      DashboardStore.ReferalStatistics.clicksByReferral = 0;
      DashboardStore.ReferalStatistics.createdAProfile = 0;
      DashboardStore.ReferalStatistics.appliedForJobs = 0;
      DashboardStore.ReferalStatistics.ErrorMessage = "Sorry Error Occured";
    }
  }
  __GetMyrefarals = async (pageNumber, pagesize) => {

    DashboardStore._ToggleProcessing(true)
    const result = await this.apiCall.GetMyrefarals(pageNumber, pagesize);

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let data = resp.data;
          console.log(data)
          // let thisData = data[0];
          let referalsList = data.referreds;
          runInAction(() => {
            DashboardStore.MyRefarals.referalsList = referalsList;
          });

        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
          DashboardStore.MyRefarals.referalsList = [];
          DashboardStore.MyRefarals.ErrorMessage = message;
        }
      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        DashboardStore.MyRefarals.referalsList = [];
        DashboardStore.MyRefarals.ErrorMessage = message;

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      DashboardStore.MyRefarals.referalsList = [];
      DashboardStore.MyRefarals.ErrorMessage = "Sorry Error Occured";
    }
  }
  __GetApplicationStatistics = async () => {

    // DashboardStore._ToggleProcessing(true)
    const result = await this.apiCall.GetApplicationStatistics();

    console.log(result)
    // DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let data = resp.data;
          console.log(data)
          // let thisData = data[0];
          let profileVisit = data.profileVisit;
          let successfulReferral = data.successfulReferral;
          let cvViews = data.cvViews;
          runInAction(() => {
            DashboardStore.ApplicationsStatistics.profileVisit = profileVisit;
            DashboardStore.ApplicationsStatistics.successfulReferral = successfulReferral;
            DashboardStore.ApplicationsStatistics.cvViews = cvViews;


          });

        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
          DashboardStore.ApplicationsStatistics.profileVisit = 0;
          DashboardStore.ApplicationsStatistics.successfulReferral = 0;
          DashboardStore.ApplicationsStatistics.cvViews = 0;
          DashboardStore.ApplicationsStatistics.ErrorMessage = message;
        }
      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        DashboardStore.ApplicationsStatistics.profileVisit = 0;
        DashboardStore.ApplicationsStatistics.successfulReferral = 0;
        DashboardStore.ApplicationsStatistics.cvViews = 0;
        DashboardStore.ApplicationsStatistics.ErrorMessage = message;

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      DashboardStore.ApplicationsStatistics.profileVisit = 0;
      DashboardStore.ApplicationsStatistics.successfulReferral = 0;
      DashboardStore.ApplicationsStatistics.cvViews = 0;
      DashboardStore.ApplicationsStatistics.ErrorMessage = "Sorry Error Occured";
    }
  }
  __GetMyApplicationList = async (pageNumber, pagesize) => {

    DashboardStore._ToggleProcessing(true)
    const result = await this.apiCall.GetMyApplicationList(pageNumber, pagesize);

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let data = resp.data;
          console.log(data)
          console.log("my application list")
          // let thisData = data[0];
          let ApplicationsList = data.applications;
          runInAction(() => {
            DashboardStore.MyApplications.ApplicationsList = ApplicationsList;
          });

        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
          DashboardStore.MyApplications.ApplicationsList = [];
          DashboardStore.MyApplications.ErrorMessage = message;
        }
      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        DashboardStore.MyApplications.ApplicationsList = [];
        DashboardStore.MyApplications.ErrorMessage = message;

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      DashboardStore.MyApplications.ApplicationsList = [];
      DashboardStore.MyApplications.ErrorMessage = "Sorry Error Occured";
    }
  }
  __UpdateNotificationSettings = async (payload) => {


    const result = await this.apiCall.UpdateNotificationSettings(payload);

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let message = resp.message;
          let dataResponse = resp.data;
          console.log(message)
          console.log(dataResponse)
          ShowNotification(message);
          //  DashboardStore.navigationState.navigation.goBack();
          DashboardStore.isGoBack = true;
          DashboardStore.__GetNotificationSettings();

        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, false);
          // DashboardStore.isJobApplySuccessful = false
        }

        // this._ToggleLoader('Categories',false)

      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        // DashboardStore.isJobApplySuccessful = false
      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      // DashboardStore.isJobApplySuccessful = false

    }




  }
  __ChangePassword = async () => {


    const result = await this.apiCall.ChangePassword();

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let message = resp.message;
          let dataResponse = resp.data;
          console.log(message)
          console.log(dataResponse)
          ShowNotification(message);
          //  DashboardStore.navigationState.navigation.goBack();
          DashboardStore.isGoBack = true;

        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, false);


          // DashboardStore.isJobApplySuccessful = false
        }

        // this._ToggleLoader('Categories',false)

      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        // DashboardStore.isGoBack=true;
        ShowNotification(message, true);
        // DashboardStore.isJobApplySuccessful = false

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      // DashboardStore.isJobApplySuccessful = false

    }




  }
  __GetNotificationSettings = async () => {

    DashboardStore._ToggleProcessing(true)
    const result = await this.apiCall.GetNotificationSettings();

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let data = resp.data;
          console.log(data)

          let all_notification = null;
          let email_notification = null;
          let sms_notification = null;
          let workbrook_updates = null;

          all_notification = data.all_notification == "1" ? true : false;
          email_notification = data.email_notification == "1" ? true : false;
          sms_notification = data.sms_notification == "1" ? true : false;
          workbrook_updates = data.workbrook_updates == "1" ? true : false;

          // referalsList=[];
          runInAction(() => {
            DashboardStore.NotificationSettings_.all_notification = all_notification;
            DashboardStore.NotificationSettings_.email_notification = email_notification;
            DashboardStore.NotificationSettings_.sms_notification = sms_notification;
            DashboardStore.NotificationSettings_.workbrook_updates = workbrook_updates;
          });

        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
          DashboardStore.NotificationSettings_.referalsList = [];
          DashboardStore.NotificationSettings_.ErrorMessage = message;
        }
      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        DashboardStore.NotificationSettings_.referalsList = [];
        DashboardStore.NotificationSettings_.ErrorMessage = message;

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      DashboardStore.NotificationSettings_.referalsList = [];
      DashboardStore.NotificationSettings_.ErrorMessage = "Sorry Error Occured";
    }
  }
  __GetOtherProfile = async () => {

    DashboardStore._ToggleProcessing(true)
    const result = await this.apiCall.GetOtherProfile();

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let data = resp.data;
          console.log(data)
          // referalsList=[];
          let thisprofile = data[0]
          console.log(thisprofile)
          runInAction(() => {
            DashboardStore.OtherProfile.ProfileData = thisprofile;
            DashboardStore.OtherProfile.skills = thisprofile.skills;
            DashboardStore.OtherProfile.experience = thisprofile.experience;
            DashboardStore.OtherProfile.education = thisprofile.education;
          });

        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
          DashboardStore.OtherProfile.ProfileData = {};
          DashboardStore.OtherProfile.skills = []
          DashboardStore.OtherProfile.experience = []
          DashboardStore.OtherProfile.education = []
          DashboardStore.OtherProfile.ErrorMessage = message;
        }
      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        DashboardStore.OtherProfile.ProfileData = {};
        DashboardStore.OtherProfile.skills = []
        DashboardStore.OtherProfile.experience = []
        DashboardStore.OtherProfile.education = []
        DashboardStore.OtherProfile.ErrorMessage = message;

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      DashboardStore.OtherProfile.ProfileData = {};
      DashboardStore.OtherProfile.skills = []
      DashboardStore.OtherProfile.experience = []
      DashboardStore.OtherProfile.education = []
      DashboardStore.OtherProfile.ErrorMessage = "Sorry Error Occured";
    }
  }
  __GetWbLeaguemyPosition = async () => {

    DashboardStore._ToggleProcessing(true)
    const result = await this.apiCall.GetWbLeaguemyPosition();

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let data = resp.data;
          let thisprofile = data[0]
          console.log(data)
          console.log("???data???")
          let positioninstate = thisprofile.positionInmyState;
          if (positioninstate == null) {
            positioninstate = {};
          }
          runInAction(() => {
            DashboardStore.WbLeague.data = thisprofile;
            DashboardStore.WbLeague.myPosition = positioninstate;
          });

        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
          DashboardStore.WbLeague.data = {};
          DashboardStore.WbLeague.myPosition = {};
          DashboardStore.WbLeague.ErrorMessage = message;
        }
      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        DashboardStore.WbLeague.data = {};
        DashboardStore.WbLeague.myPosition = {};
        DashboardStore.WbLeague.ErrorMessage = message;

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      DashboardStore.WbLeague.data = {};
      DashboardStore.WbLeague.myPosition = {};
      DashboardStore.WbLeague.ErrorMessage = "Sorry Error Occured";
    }
  }
  __GetWbLeaguePointAggregation = async () => {

    DashboardStore._ToggleProcessing(true)
    const result = await this.apiCall.GetWbLeaguePointAggregation();

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let data = resp.data;
          console.log(data)
          console.log("???data???")
          //  grandTotal:0,
          //  execRef:0,
          //  graduateRef:0,
          //  invitedAFriend:0,
          //  mGRef:0,
          //  nMRef:0,
          //  profilePoint:0,
          //  sMRef:0,
          //  signUP:0,


          let execRef = data.execRef;
          let graduateRef = data.graduateRef;
          let invitedAFriend = data.invitedAFriend;
          let mGRef = data.mGRef;
          let nMRef = data.nMRef;
          let profilePoint = data.profilePoint;
          let sMRef = data.sMRef;
          let signUP = data.signUP;

          let execRefAmount = execRef.amount == null ? 0 : parseFloat(execRef.amount);
          let graduateRefAmount = graduateRef.amount == null ? 0 : parseFloat(graduateRef.amount);
          let invitedAFriendAmount = invitedAFriend.amount == null ? 0 : parseFloat(invitedAFriend.amount);
          let mGRefAmount = mGRef.amount == null ? 0 : parseFloat(mGRef.amount);
          let nMRefAmount = nMRef.amount == null ? 0 : parseFloat(nMRef.amount);
          let profilePointAmount = profilePoint.amount == null ? 0 : parseFloat(profilePoint.amount);
          let sMRefAmount = sMRef.amount == null ? 0 : parseFloat(sMRef.amount);
          let signUPAmount = signUP.amount == null ? 0 : parseFloat(signUP.amount);
          let Granttotal = execRefAmount + graduateRefAmount + invitedAFriendAmount + mGRefAmount + nMRefAmount + profilePointAmount + sMRefAmount + signUPAmount;
          //             let referralPoint_count=data.referralPoint.count;
          //             let referralPoint_totalRefPoint=data.referralPoint.totalRefPoint==null ? 0 : data.referralPoint.totalRefPoint;
          // let grandTotal= parseInt(referralPoint_totalRefPoint)  +  parseInt(profilePoint) ;
          runInAction(() => {
            DashboardStore.WbLeaguePointAggregation.execRef = execRef;
            DashboardStore.WbLeaguePointAggregation.graduateRef = graduateRef;
            DashboardStore.WbLeaguePointAggregation.graduateRef = graduateRef;
            DashboardStore.WbLeaguePointAggregation.invitedAFriend = invitedAFriend;
            DashboardStore.WbLeaguePointAggregation.mGRef = mGRef;
            DashboardStore.WbLeaguePointAggregation.nMRef = nMRef;
            DashboardStore.WbLeaguePointAggregation.profilePoint = profilePoint;
            DashboardStore.WbLeaguePointAggregation.sMRef = sMRef;
            DashboardStore.WbLeaguePointAggregation.signUP = signUP;
            // DashboardStore.WbLeaguePointAggregation.profilePoint = profilePoint;           
            // DashboardStore.WbLeaguePointAggregation.referralPoint_totalRefPoint = referralPoint_totalRefPoint;           
            DashboardStore.WbLeaguePointAggregation.grandTotal = Granttotal;
          });
          // execRef: {count: "0", execRefPoint: null, amount: null}
          // graduateRef: {count: "0", graduateRefPoint: null, amount: null}
          // invitedAFriend: {count: "0", invitedAFriendPoint: null, amount: null}
          // mGRef: {count: "0", mGRefPoint: null, amount: null}
          // nMRef: {count: "0", nMGRefPoint: null, amount: null}
          // profilePoint: {profileCompletedPoint: null, amount: null}
          // sMRef: {count: "0", sMRefPoint: null, amount: null}
          // signUP:
        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
          DashboardStore.WbLeaguePointAggregation.profilePoint = 0;
          DashboardStore.WbLeaguePointAggregation.referralPoint_count = 0;
          DashboardStore.WbLeaguePointAggregation.referralPoint_totalRefPoint = 0;
          DashboardStore.WbLeaguePointAggregation.grandTotal = 0;
          DashboardStore.WbLeague.ErrorMessage = message;
        }
      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        DashboardStore.WbLeaguePointAggregation.profilePoint = 0;
        DashboardStore.WbLeaguePointAggregation.referralPoint_count = 0;
        DashboardStore.WbLeaguePointAggregation.referralPoint_totalRefPoint = 0;
        DashboardStore.WbLeaguePointAggregation.grandTotal = 0;
        DashboardStore.WbLeague.ErrorMessage = message;

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      DashboardStore.WbLeaguePointAggregation.profilePoint = 0;
      DashboardStore.WbLeaguePointAggregation.referralPoint_count = 0;
      DashboardStore.WbLeaguePointAggregation.referralPoint_totalRefPoint = 0;
      DashboardStore.WbLeaguePointAggregation.grandTotal = 0;
      DashboardStore.WbLeague.ErrorMessage = "Sorry Error Occured";
    }
  }
  __GetWbLeague_List = async () => {

    DashboardStore._ToggleProcessing(true)
    const result = await this.apiCall.GetWbLeague_List();

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let data = resp.data;
          console.log(data)

          runInAction(() => {
            DashboardStore.WbLeagueList.GeneralList = data;
          });

        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
          DashboardStore.WbLeagueList.GeneralList = [];
          DashboardStore.WbLeague.ErrorMessage = message;
        }
      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        DashboardStore.WbLeagueList.GeneralList = [];
        DashboardStore.WbLeague.ErrorMessage = message;

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      DashboardStore.WbLeagueList.GeneralList = [];
      DashboardStore.WbLeague.ErrorMessage = "Sorry Error Occured";
    }
  }
  __WithDrawMyApplication = async () => {
    const result = await this.apiCall.WithDrawMyApplication();
    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;
        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let message = resp.message;
          let dataResponse = resp.data;
          console.log(message);
          console.log(dataResponse);
          ShowNotification(message, false);
          //  DashboardStore.navigationState.navigation.goBack();
          //  DashboardStore.isGoBack=true;
          DashboardStore._updateParameterV2(false, "withdrawApplication", "RedirectMode");
          DashboardStore.__GetMyApplicationList(1, 30);
        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, false);
          // DashboardStore.isJobApplySuccessful = false
          // DashboardStore._updateParameterV2(false, "withdrawApplication", "RedirectMode");

        }
        // this._ToggleLoader('Categories',false)
      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        // DashboardStore.isJobApplySuccessful = false
      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      // DashboardStore.isJobApplySuccessful = false
    }
  }
  __GetCountry_List = async () => {

    DashboardStore._ToggleProcessing(true)
    const result = await this.apiCall.GetCountry_List();

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let data = resp.data;
          console.log(data)

          // const FormattedData = data.slice(0, 3).map((singleData,index) => {
          const FormattedData = data.map((singleData, index) => {

            // let obj2 = Object.assign({}, obj1);
            return {


              // id: index,
              label: singleData.name,
              value: singleData.name,
              key: singleData.id
            }

          });
          // var yanch=[];
          // let temparry=data.slice(0, 3);
          // y.push(FormattedData);
          //  console.log(y)
          // for (let index = 0; index < temparry.length; index++) {
          // //  const element = array[index];
          //   let thisrecord={ 
          //     label: 'State'+index, 
          //     value: 'State1',
          //     key:index
          //   }
          //   console.log(thisrecord)
          //   yanch.push[thisrecord]
          // }
          //  console.log(yanch)

          console.log("FormattedData><<cntry")
          runInAction(() => {
            DashboardStore.StaticData.CountryList = FormattedData;
          });

        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
          DashboardStore.StaticData.CountryList = [];
          DashboardStore.StaticData.ErrorMessage = message;
        }
      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        DashboardStore.StaticData.CountryList = [];
        DashboardStore.StaticData.ErrorMessage = message;

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      DashboardStore.StaticData.CountryList = [];
      DashboardStore.StaticData.ErrorMessage = "Sorry Error Occured";
    }
  }
  __GetStateInCountry_List = async (countryCode) => {

    DashboardStore._ToggleProcessing(true)
    const result = await this.apiCall.GetStateInCountry_List(countryCode);

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let data = resp.data;
          console.log(data)

          // const FormattedData = data.slice(0, 3).map((singleData,index) => {
          const FormattedData = data.map((singleData, index) => {

            // let obj2 = Object.assign({}, obj1);
            return {


              // id: index,
              label: singleData.name,
              value: singleData.name,
              key: singleData.id
            }

          });
          // var yanch=[];
          // let temparry=data.slice(0, 3);
          // y.push(FormattedData);
          //  console.log(y)
          // for (let index = 0; index < temparry.length; index++) {
          // //  const element = array[index];
          //   let thisrecord={ 
          //     label: 'State'+index, 
          //     value: 'State1',
          //     key:index
          //   }
          //   console.log(thisrecord)
          //   yanch.push[thisrecord]
          // }
          //  console.log(yanch)

          console.log("FormattedData><<cntry")
          runInAction(() => {
            DashboardStore.StaticData.StateInCOuntry = FormattedData;
          });

        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
          DashboardStore.StaticData.StateInCOuntry = [];
          DashboardStore.StaticData.ErrorMessage = message;
        }
      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        DashboardStore.StaticData.StateInCOuntry = [];
        DashboardStore.StaticData.ErrorMessage = message;

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      DashboardStore.StaticData.StateInCOuntry = [];
      DashboardStore.StaticData.ErrorMessage = "Sorry Error Occured";
    }
  }
  __GetMyInviteList = async (pageNumber, pagesize) => {

    DashboardStore._ToggleProcessing(true)
    const result = await this.apiCall.GetMyInviteList(pageNumber, pagesize);

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let data = resp.data;
          console.log(data)
          let thisRecord = data[0];

          runInAction(() => {
            DashboardStore.InviteList.Myinvites = thisRecord;
          });

        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
          DashboardStore.InviteList.Myinvites = [];
          DashboardStore.InviteList.ErrorMessage = message;
        }
      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        DashboardStore.InviteList.Myinvites = [];
        DashboardStore.InviteList.ErrorMessage = message;

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      DashboardStore.InviteList.Myinvites = [];
      DashboardStore.InviteList.ErrorMessage = "Sorry Error Occured";
    }
  }
  __SearchMyInviteList = async (query) => {

    DashboardStore._ToggleProcessing(true)
    const result = await this.apiCall.SearchMyInviteList(query);

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let data = resp.data;
          console.log(data)
          let thisRecord = data[0];

          runInAction(() => {
            DashboardStore.InviteList.Myinvites = thisRecord;
          });

        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
          DashboardStore.InviteList.Myinvites = [];
          DashboardStore.InviteList.ErrorMessage = message;
        }
      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        DashboardStore.InviteList.Myinvites = [];
        DashboardStore.InviteList.ErrorMessage = message;

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      DashboardStore.InviteList.Myinvites = [];
      DashboardStore.InviteList.ErrorMessage = "Sorry Error Occured";
    }
  }
  __ChangeApplicatView = async (job_id, Applicant_id) => {


    const result = await this.apiCall.ChangeApplicatView(job_id, Applicant_id);

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let message = resp.message;
          let dataResponse = resp.data;
          console.log(message)
          console.log(dataResponse)
          ShowNotification(message);
          //  DashboardStore.navigationState.navigation.goBack();
          //  DashboardStore.isGoBack=true;

        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, false);
          // DashboardStore.isJobApplySuccessful = false
        }

        // this._ToggleLoader('Categories',false)

      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        // DashboardStore.isJobApplySuccessful = false
      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      // DashboardStore.isJobApplySuccessful = false

    }




  }
  __ViewMyInvitesLogs = async (pageNumber, pagesize) => {

    DashboardStore._ToggleProcessing(true)
    const result = await this.apiCall.ViewMyInvitesLogs(pageNumber, pagesize);

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let data = resp.data;
          //  console.log(data)
          let thisData = data[0];
          console.log("GetRecruiters")
          runInAction(() => {
            DashboardStore.Recruiters.AvailableRecruiters = thisData;
          });

        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
          DashboardStore.Recruiters.AvailableRecruiters = [];
          DashboardStore.Recruiters.ErrorMessage = message;
        }
      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        DashboardStore.Recruiters.AvailableRecruiters = [];
        DashboardStore.Recruiters.ErrorMessage = message;

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      DashboardStore.Recruiters.AvailableRecruiters = [];
      DashboardStore.Recruiters.ErrorMessage = "Sorry Error Occured";
    }
  }

  __GetReruitersRequest = async (pageNumber, pagesize) => {

    DashboardStore._ToggleProcessing(true)
    const result = await this.apiCall.GetReruitersRequest(pageNumber, pagesize);

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let data = resp.data;
          console.log(data)
          let thisrecord = data[0];

          console.log("All recruiters Job")

          console.log("data>>>")
          runInAction(() => {
            DashboardStore.RecruiterDashBoard.Myrequest = thisrecord;
          });

        }
        else {
          //   this.Categories = []


          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
          DashboardStore.RecruiterDashBoard.Myrequest = [];
          DashboardStore.RecruiterDashBoard.ErrorMessage = message;
        }

        // this._ToggleLoader('Categories',false)

      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        DashboardStore.RecruiterDashBoard.Myrequest = [];
        DashboardStore.RecruiterDashBoard.ErrorMessage = message;

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      DashboardStore.RecruiterDashBoard.Myrequest = [];
      DashboardStore.RecruiterDashBoard.ErrorMessage = "Sorry Error Occured";

    }




  }
  __AcceptOrDeclinedJobOfferNow = async (Job_status) => {
    const result = await this.apiCall.AcceptOrDeclinedJobOfferNow(Job_status);
    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;
        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let message = resp.message;
          let dataResponse = resp.data;
          console.log(message);
          console.log(dataResponse);
          ShowNotification(message);
          // DashboardStore.isGoBack=true;
          DashboardStore.__GetReruitersRequest(1, 30);
          DashboardStore.__GetReruitersInsight(1, 30);

        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, false);
        }
        // this._ToggleLoader('Categories',false)
      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        // DashboardStore.isJobApplySuccessful = false
      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      // DashboardStore.isJobApplySuccessful = false
    }




  }
  __GetJobRecruiters = async (pageNumber, pagesize) => {

    DashboardStore._ToggleProcessing(true)
    const result = await this.apiCall.GetJobRecruiters(pageNumber, pagesize);

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let data = resp.data;
          console.log(data)
          let thisData = data.recruiters;
          //  let Applicants=[];
          let RecruitersCount = thisData.length;
          runInAction(() => {
            DashboardStore.JobRecruiters.RecruiterList = thisData;
            DashboardStore.JobRecruiters.TotalCounter = RecruitersCount;
            DashboardStore.JobRecruiters.ErrorMessage = RecruitersCount < 1 ? 'No Recruiter   Found' : '';

            // DashboardStore.isGoBack=true;

          });

        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
          DashboardStore.JobRecruiters.RecruiterList = [];
          DashboardStore.JobRecruiters.ErrorMessage = message;
        }
      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        DashboardStore.JobRecruiters.RecruiterList = [];
        DashboardStore.JobRecruiters.ErrorMessage = message;

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      DashboardStore.JobRecruiters.RecruiterList = [];
      DashboardStore.JobRecruiters.ErrorMessage = "Sorry Error Occured";
    }
  }
  __HireMoreRecruiter = async () => {
    const result = await this.apiCall.HireMoreRecruiter();
    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;
        let ApiStatus = resp.status
        if (ApiStatus === true) {
          DashboardStore.JobPosting.SelectedRecuiter = [];
          console.log(resp)
          let message = resp.message;
          let dataResponse = resp.data;
          console.log(message);
          console.log(dataResponse);
          ShowNotification(message);
          // DashboardStore.isGoBack=true;
          DashboardStore._ToggleSucessPostModal(true);
          DashboardStore.__GetJobOffers(1, 10);
        }
        else {

          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, false);
        }
        // this._ToggleLoader('Categories',false)
      }
      else {

        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        // DashboardStore.isJobApplySuccessful = false


      }
    }
    else {
      DashboardStore.JobPosting.SelectedRecuiter = [];

      ShowNotification('Generic Error Occured', true);
      // DashboardStore.isJobApplySuccessful = false
    }




  }
  __GetReruitersInsight = async (pageNumber, pagesize) => {

    DashboardStore._ToggleProcessing(true)
    const result = await this.apiCall.GetReruitersInsight(pageNumber, pagesize);

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let data = resp.data;
          console.log(data)
          // let thisrecord = data[0];
          let thisrecord = []
          let sizeofrecord = parseInt(data.total_item_count);
          console.log(sizeofrecord);
          if (sizeofrecord < 1) {
            thisrecord = []
          }
          else {

            thisrecord = data.jobs;
          }



          console.log("All accepted Job")
          console.log(thisrecord)

          console.log("data>>>")
          runInAction(() => {
            DashboardStore.RecruiterInsight.MyAcceptedJobs = thisrecord;
            DashboardStore.RecruiterInsight.ErrorMessage = thisrecord.length > 1 ? '' : 'No record found'

          });

        }
        else {
          //   this.Categories = []


          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
          DashboardStore.RecruiterInsight.MyAcceptedJobs = [];
          DashboardStore.RecruiterInsight.ErrorMessage = message;
        }

        // this._ToggleLoader('Categories',false)

      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        DashboardStore.RecruiterInsight.MyAcceptedJobs = [];
        DashboardStore.RecruiterInsight.ErrorMessage = message;

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      DashboardStore.RecruiterInsight.MyAcceptedJobs = [];
      DashboardStore.RecruiterInsight.ErrorMessage = "Sorry Error Occured";

    }




  }
  __GetShortlistedApplicant = async (pageNumber, pagesize) => {

    DashboardStore._ToggleProcessing(true)
    const result = await this.apiCall.GetShortlistedApplicant(pageNumber, pagesize);

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let data = resp.data;
          console.log(data)
          let thisData = data.shorlisted;
          //  let Applicants=[];
          let Counter = thisData.length;
          runInAction(() => {
            DashboardStore.ShortlistedApplicant.MyShorlisted = thisData;
            // DashboardStore.ShortlistedApplicant.TotalCounter = RecruitersCount;
            DashboardStore.ShortlistedApplicant.ErrorMessage = Counter < 1 ? 'No Shortlisted' : '';

            // DashboardStore.isGoBack=true;

          });

        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
          DashboardStore.ShortlistedApplicant.MyShorlisted = [];
          DashboardStore.ShortlistedApplicant.ErrorMessage = message;
        }
      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        DashboardStore.ShortlistedApplicant.MyShorlisted = [];
        DashboardStore.ShortlistedApplicant.ErrorMessage = message;

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      DashboardStore.ShortlistedApplicant.MyShorlisted = [];
      DashboardStore.ShortlistedApplicant.ErrorMessage = "Sorry Error Occured";
    }
  }
  __ShortListOrDeclineApplicant = async (job_id, user_id) => {
    ShowNotification("Processing....", true);

    const result = await this.apiCall.ShortListOrDeclineApplicant(job_id, user_id);
    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;
        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let message = resp.message;
          let dataResponse = resp.data;
          console.log(message);
          console.log(dataResponse);
          ShowNotification(message);
          // DashboardStore.isGoBack=true;
          DashboardStore.__GetReruitersInsight(1, 30);
        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, false);
        }
        // this._ToggleLoader('Categories',false)
      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        // DashboardStore.isJobApplySuccessful = false
      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      // DashboardStore.isJobApplySuccessful = false
    }




  }
  __SendShortlistedApplicant = async () => {
    ShowNotification("Processing....", true);


    const result = await this.apiCall.SendShortlistedApplicant();

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let message = resp.message;
          let dataResponse = resp.data;
          console.log(message)
          console.log(dataResponse)
          //  setTimeout(() => {

          //            // AccountStore.navigationState.navigate("ResetPassword");
          //      }, 1000);
          ShowNotification(message, false);
          // DashboardStore.navigationState.navigation.goBack();
          DashboardStore.isGoBack = true;
          DashboardStore.__GetReruitersInsight(1, 30);

          //  DashboardStore.navigationState.navigate("profile");
        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          DashboardStore.isGoBack = true;

          ShowNotification(message, false);
        }

        // this._ToggleLoader('Categories',false)

      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);


    }




  }
  __GetSavedJobs = async (pageNumber, pagesize) => {

    DashboardStore._ToggleProcessing(true)
    const result = await this.apiCall.GetSavedJobs(pageNumber, pagesize);

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let data = resp.data;
          console.log(data)
          console.log("Saved jobs ")
          let thisData = data[0];
          //  let Applicants=[];
          let Counter = thisData.length;
          runInAction(() => {
            DashboardStore.MySavedJobs.MySavedjoblist = thisData;
            DashboardStore.MySavedJobs.ErrorMessage = Counter < 1 ? 'List Empty' : '';

            // DashboardStore.isGoBack=true;

          });

        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
          DashboardStore.MySavedJobs.MySavedjoblist = [];
          DashboardStore.MySavedJobs.ErrorMessage = message;
        }
      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        DashboardStore.MySavedJobs.MySavedjoblist = [];
        DashboardStore.MySavedJobs.ErrorMessage = message;

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      DashboardStore.MySavedJobs.MySavedjoblist = [];
      DashboardStore.MySavedJobs.ErrorMessage = "Sorry Error Occured";
    }
  }

  __deleteEducationManagement = async () => {


    const result = await this.apiCall.deleteEducationManagement();

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let message = resp.message;
          let dataResponse = resp.data;
          console.log(message)
          console.log(dataResponse)
          //  setTimeout(() => {

          //            // AccountStore.navigationState.navigate("ResetPassword");
          //      }, 1000);
          ShowNotification(message, true);
          //  DashboardStore.navigationState.navigation.goBack();
          DashboardStore.__FetchAllMyEducation();

          DashboardStore.navigationState.navigate("profile");
        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
        }

        // this._ToggleLoader('Categories',false)

      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);


    }




  }
  __deleteSkillManagement = async () => {


    const result = await this.apiCall.deleteSkillManagement();

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let message = resp.message;
          let dataResponse = resp.data;
          console.log(message)
          console.log(dataResponse)
          //  setTimeout(() => {

          //            // AccountStore.navigationState.navigate("ResetPassword");
          //      }, 1000);
          ShowNotification(message, true);
          //  DashboardStore.navigationState.navigation.goBack();
          DashboardStore.__FetchAllMyExperience();

          DashboardStore.navigationState.navigate("profile");
        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
        }

        // this._ToggleLoader('Categories',false)

      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);


    }




  }
  __GetNotification = async (pageNumber, pagesize) => {

    DashboardStore._ToggleProcessing(true)
    const result = await this.apiCall.GetNotification(pageNumber, pagesize);

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let data = resp.data;
          console.log(data)
          console.log("Saved jobs ")
          let thisData = data.notifications;
          //  let Applicants=[];
          let Counter = data.total_count;
          runInAction(() => {
            DashboardStore.SystemNotification.MyNotification = thisData;
            DashboardStore.SystemNotification.ErrorMessage = Counter < 1 ? 'List Empty' : '';

            // DashboardStore.isGoBack=true;

          });

        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
          DashboardStore.SystemNotification.MyNotification = [];
          DashboardStore.SystemNotification.ErrorMessage = message;
        }
      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        DashboardStore.SystemNotification.MyNotification = [];
        DashboardStore.SystemNotification.ErrorMessage = message;

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      DashboardStore.SystemNotification.MyNotification = [];
      DashboardStore.SystemNotification.ErrorMessage = "Sorry Error Occured";
    }
  }
  __GetMyRecruiterProfile = async () => {

    DashboardStore._ToggleProcessing(true)
    const result = await this.apiCall.GetMyRecruiterProfile();

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let data = resp.data;
          console.log(data)
          console.log("recruiter profile")
          let thisData = data;
          //  let Applicants=[];
          //  let Counter=data.total_count;  
          runInAction(() => {
            // DashboardStore.MyRecruiterProfile.RecruiterProfile = thisData;
            DashboardStore.RecruiterProfile = thisData;
            // DashboardStore.SystemNotification.ErrorMessage = Counter < 1 ? 'List Empty': '';

            // DashboardStore.isGoBack=true;

          });

        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
          DashboardStore.RecruiterProfile = {};
          //  DashboardStore.SystemNotification.ErrorMessage = message;
        }
      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        DashboardStore.RecruiterProfile = {};
        // DashboardStore.SystemNotification.ErrorMessage = message;

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      DashboardStore.RecruiterProfile = {};
      //  DashboardStore.SystemNotification.ErrorMessage = "Sorry Error Occured";
    }
  }

  __UpdateRecruiterProfile = async () => {


    const result = await this.apiCall.UpdateRecruiterProfile();

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let message = resp.message;
          let dataResponse = resp.data;
          console.log(message);
          console.log(dataResponse);
          // let previousProfiledata = DashboardStore.LoggedInUserProfile
          // console.log(previousProfiledata)
          // let NewData2pasist = Object.assign({}, previousProfiledata, dataResponse);
          // console.log(NewData2pasist)

          //  setTimeout(() => {
          ShowNotification(message, false);
          DashboardStore.__GetMyRecruiterProfile();

          //        // AccountStore.navigationState.navigate("profile");
          //  }, 1000);
          // DashboardStore._UpdateLoggedInUserProfile(NewData2pasist);
          // PersistData('loggedinUserdetails', NewData2pasist);
          //  DashboardStore.navigationState.navigation.goBack();
          // DashboardStore.navigationState.navigate("profile");
          //  DashboardStore._UpdateUserSetIntrest(true);
          // DashboardStore.ProceedWithIntrestRecord();

          // runInAction(() => {

          //   // DashboardStore.IntrestSettings.AllSystemIntrest = suggestedIntrest;


          // });

        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
        }

        // this._ToggleLoader('Categories',false)

      }
      else {

        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        console.log("heree<><tobi" + message)
        ShowNotification(message, false);

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);


    }




  }
  __ProccessJobPosting2Recruiters_update = async () => {


    const result = await this.apiCall.ProccessJobPosting2Recruiters_update();

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let message = resp.message;
          let dataResponse = resp.data;
          console.log(message)
          console.log(dataResponse)
          //  setTimeout(() => {

          //            // AccountStore.navigationState.navigate("ResetPassword");
          //      }, 1000);
          ShowNotification(message, false);
          // DashboardStore.navigationState.navigation.goBack();
          // DashboardStore.isGoBack=true;
          // DashboardStore._ToggleSucessPostModal(true);
          DashboardStore._ToggleModal___("newreferalJobSuccess", true);

          //  DashboardStore.navigationState.navigate("profile");
        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, false);
        }

        // this._ToggleLoader('Categories',false)

      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, false);

      }
    }
    else {
      ShowNotification('Generic Error Occured', false);


    }




  }
  __CloseJobOffer = async () => {


    const result = await this.apiCall.CloseJobOffer();

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let message = resp.message;
          let dataResponse = resp.data;
          console.log(message)
          console.log(dataResponse)
          //  setTimeout(() => {
          //            // AccountStore.navigationState.navigate("ResetPassword");
          //      }, 1000);
          ShowNotification(message, false);
          DashboardStore.__GetJobOffers(1, 50);

          // DashboardStore.navigationState.navigation.goBack();
          // DashboardStore.isGoBack=true;
          // DashboardStore._ToggleSucessPostModal(true);

          //  DashboardStore.navigationState.navigate("profile");
        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, false);
        }

        // this._ToggleLoader('Categories',false)

      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
    }
  }
  __GetMyCurrentProfile = async () => {

console.log("get my profileeee")
    const result = await this.apiCall.GetMyCurrentProfile();

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let message = resp.message;
          let dataResponse = resp.data;
          console.log(message)
          console.log(dataResponse)
          let previousProfiledata = DashboardStore.LoggedInUserProfile
          console.log(previousProfiledata)
          let NewData2pasist = Object.assign({}, previousProfiledata, dataResponse);
          console.log(NewData2pasist)

          DashboardStore._UpdateLoggedInUserProfile(NewData2pasist);
          PersistData('loggedinUserdetails', NewData2pasist);



        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
        }

        // this._ToggleLoader('Categories',false)

      }
      else {

        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        console.log("heree<><tobi" + message)
        ShowNotification(message, false);

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);


    }




  }
   __GetJobLikes = async (pageNumber, pagesize) => {

    DashboardStore._ToggleProcessing(true)
    const result = await this.apiCall.GetJobLikes(pageNumber, pagesize);

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let data = resp.data;
          console.log(data)
          console.log("Saved jobs ")
          let thisData = data[0];
          //  let Applicants=[];
          let Counter = thisData.length;
          runInAction(() => {
            DashboardStore.JoblikeObject.mylikes = thisData;
            DashboardStore.JoblikeObject.ErrorMessage = Counter < 1 ? 'List Empty' : '';

            // DashboardStore.isGoBack=true;

          });

        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
          DashboardStore.JoblikeObject.mylikes  = [];
          DashboardStore.JoblikeObject.ErrorMessage = message;
        }
      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        DashboardStore.JoblikeObject.mylikes  = [];
        DashboardStore.JoblikeObject.ErrorMessage = message;

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      DashboardStore.JoblikeObject.mylikes  = [];
      DashboardStore.JoblikeObject.ErrorMessage = "Sorry Error Occured";
    }
  }
   __GetJobSuggestionsBasedOnSkill = async (pageNumber, pagesize) => {

    DashboardStore._ToggleProcessing(true)
    const result = await this.apiCall.GetJobSuggestionsBasedOnSkill(pageNumber, pagesize);

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let data = resp.data;
          console.log(data)
          console.log("Saved jobs ")
          let thisData = data[0];
          //  let Applicants=[];
          let Counter = thisData.length;
          runInAction(() => {
            DashboardStore.JobSeggestions.MyJobs = thisData;
            DashboardStore.JobSeggestions.ErrorMessage = Counter < 1 ? 'List Empty' : '';

            // DashboardStore.isGoBack=true;

          });

        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
          DashboardStore.JobSeggestions.MyJobs  = [];
          DashboardStore.JobSeggestions.ErrorMessage = message;
        }
      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        DashboardStore.JobSeggestions.MyJobs  = [];
        DashboardStore.JobSeggestions.ErrorMessage = message;

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      DashboardStore.JobSeggestions.MyJobs  = [];
      DashboardStore.JobSeggestions.ErrorMessage = "Sorry Error Occured";
    }
  }
  __AddCardToSystem = async (StripeToken) => {
    ShowNotification("Saving Card Details....", true);


    const result = await this.apiCall.AddCardToSystem(StripeToken);

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let message = resp.message;
          let dataResponse = resp.data;
          console.log(message)
          console.log(dataResponse)
         
          ShowNotification(message, false);
          //  DashboardStore.__GetReruitersInsight(1, 30);

         }
        else {
           let message = resp.message;
          console.log("heree" + message)
          DashboardStore.isGoBack = true;

          ShowNotification(message, false);
        }
      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);


    }




  }
  __GetMycardList = async (pageNumber, pagesize) => {

    DashboardStore._ToggleProcessing(true)
    const result = await this.apiCall.GetMycardList(pageNumber, pagesize);

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let data = resp.data;
          console.log(data)
          console.log("Get card  list ")
          let thisData = data[0];
          //  let Applicants=[];
          let Counter = thisData.length;
          runInAction(() => {
            DashboardStore.CardsInSystem.Mycards = thisData;
            DashboardStore.CardsInSystem.ErrorMessage = Counter < 1 ? 'List Empty' : '';

            // DashboardStore.isGoBack=true;

          });

        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
          DashboardStore.CardsInSystem.Mycards  = [];
          DashboardStore.CardsInSystem.ErrorMessage = message;
        }
      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        DashboardStore.CardsInSystem.Mycards  = [];
        DashboardStore.CardsInSystem.ErrorMessage = message;

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      DashboardStore.CardsInSystem.Mycards  = [];
      DashboardStore.CardsInSystem.ErrorMessage = "Sorry Error Occured";
    }
  }
 
  __GetTransactionHistory = async (pageNumber, pagesize) => {
    console.log("GetTransactionHistoryrrrr")

    DashboardStore._ToggleProcessing(true)
    const result = await this.apiCall.GetTransactionHistory(pageNumber, pagesize);

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let data = resp.data;
          console.log(data)
          console.log("GetTransactionHistory")
          let thisData = data[0];
          //  let Applicants=[];
          let Counter = thisData.length;
          runInAction(() => {
            DashboardStore.TransactionInHistory.TransactionHistory = thisData;
            DashboardStore.TransactionInHistory.ErrorMessage = Counter < 1 ? 'List Empty' : '';

            // DashboardStore.isGoBack=true;

          });

        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
          DashboardStore.TransactionInHistory.TransactionHistory  = [];
          DashboardStore.TransactionInHistory.ErrorMessage = message;
        }
      }
      else {
        let resp = response.data;
         console.log(resp);
        let message = resp.message
        ShowNotification(message, true);
        DashboardStore.TransactionInHistory.TransactionHistory  = [];
        DashboardStore.TransactionInHistory.ErrorMessage = message;

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      DashboardStore.TransactionInHistory.TransactionHistory  = [];
      DashboardStore.TransactionInHistory.ErrorMessage = "Sorry Error Occured";
    }
  }
  __GetAccountBalance = async () => {

    DashboardStore._ToggleProcessing(true)
    const result = await this.apiCall.GetAccountBalance();

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let data = resp.data;
          console.log(data)
          console.log("Saved jobs ")
          let thisData = data[0];
          //  let Applicants=[];
          // let Counter = thisData.length;
          console.log(thisData)
          console.log("Just here")
          runInAction(() => {
            DashboardStore.WalletBalance.Balance = thisData["balance"];
            // DashboardStore.WalletBalance.Balance = 77;
            // DashboardStore.WalletBalance.ErrorMessage = Counter < 1 ? 'List Empty' : '';

            // DashboardStore.isGoBack=true;

          });

        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
          DashboardStore.WalletBalance.Balance  = 0
          DashboardStore.WalletBalance.ErrorMessage = message;
        }
      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        DashboardStore.WalletBalance.Balance  = 0
        DashboardStore.WalletBalance.ErrorMessage = message;

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      DashboardStore.WalletBalance.Balance  = 0
      DashboardStore.WalletBalance.ErrorMessage = "Sorry Error Occured";
    }
  }
  __ProcessAddFundtoAccount = async () => {


    const result = await this.apiCall.ProcessAddFundtoAccount();

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let message = resp.message;
          let dataResponse = resp.data;
          console.log(message)
          console.log(dataResponse)
       
          ShowNotification(message, false);
          DashboardStore.__GetAccountBalance();
DashboardStore.FundWallet.amount='';
        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
        }

        // this._ToggleLoader('Categories',false)

      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, false);

      }
    }
    else {
      ShowNotification('Generic Error Occured', false);


    }




  }
  __SetcardAsDefault = async (card_id) => {


    const result = await this.apiCall.SetcardAsDefault(card_id);

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let message = resp.message;
          let dataResponse = resp.data;
          console.log(message)
          console.log(dataResponse)
       
          ShowNotification(message, false);
          DashboardStore.__GetMycardList(1,30);
         }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
        }

        // this._ToggleLoader('Categories',false)

      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, false);

      }
    }
    else {
      ShowNotification('Generic Error Occured', false);


    }




  }
  __GetSubscriptionConfig = async (pageNumber, pagesize) => {

    DashboardStore._ToggleProcessing(true)
    const result = await this.apiCall.GetSubscriptionConfig(pageNumber, pagesize);

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let data = resp.data;
          console.log(data)
          console.log("Saved jobs ");
          let thisData = data["subscriptions"];
          //  let Applicants=[];
          // let Counter = thisData.length;
          console.log(thisData)
          console.log("Just here")
          var PostJobs =  thisData.filter(function(data) {
            return data.category  ==="job";
          });
          var RecruiterSub =  thisData.filter(function(data) {
            return data.category  ==="Recruiter";
          });
          runInAction(() => {
            DashboardStore.SubscriptionConfig.subscriptions = thisData;
            DashboardStore.SubscriptionConfig.subscriptions_jobs = PostJobs;
            DashboardStore.SubscriptionConfig.subscriptions_recruiter = RecruiterSub;
             // DashboardStore.SubscriptionConfig.ErrorMessage = Counter < 1 ? 'List Empty' : '';
          });

        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
          DashboardStore.SubscriptionConfig.subscriptions=[]
          DashboardStore.SubscriptionConfig.ErrorMessage = message;
        }
      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        DashboardStore.SubscriptionConfig.subscriptions=[]
        DashboardStore.SubscriptionConfig.ErrorMessage = message;

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      DashboardStore.SubscriptionConfig.subscriptions=[]
      DashboardStore.SubscriptionConfig.ErrorMessage = "Sorry Error Occured";
    }
  }
  
  __SubscribeWithWallet = async () => {


    const result = await this.apiCall.SubscribeWithWallet();

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let message = resp.message;
          let dataResponse = resp.data;
          console.log(message)
          console.log(dataResponse)
       
          ShowNotification(message, false);
           DashboardStore._updateParameterV2(false, "SubscriptionModal", "RedirectMode");
          DashboardStore.__GetSubscriptionConfig(1,10);
          DashboardStore.__GetAccountBalance();
        }
        else {
          DashboardStore._updateParameterV2(false, "SubscriptionModal", "RedirectMode");

           let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
        }

 
      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, false);
        DashboardStore._updateParameterV2(false, "SubscriptionModal", "RedirectMode");

      }
    }
    else {
      ShowNotification('Generic Error Occured', false);
      DashboardStore._updateParameterV2(false, "SubscriptionModal", "RedirectMode");


    }




  }
  __SubscribeWithcard = async (Card_id) => {


    const result = await this.apiCall.SubscribeWithcard(Card_id);

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let message = resp.message;
          let dataResponse = resp.data;
          console.log(message)
          console.log(dataResponse)
       
          ShowNotification(message, false);
           DashboardStore._updateParameterV2(false, "SubscriptionModal", "RedirectMode");
          DashboardStore.__GetSubscriptionConfig(1,10);
          DashboardStore.__GetAccountBalance();
        }
        else {
          DashboardStore._updateParameterV2(false, "SubscriptionModal", "RedirectMode");

           let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
        }

 
      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, false);
        DashboardStore._updateParameterV2(false, "SubscriptionModal", "RedirectMode");

      }
    }
    else {
      ShowNotification('Generic Error Occured', false);
      DashboardStore._updateParameterV2(false, "SubscriptionModal", "RedirectMode");


    }




  }
  __GetActiveSubscription = async (pageNumber, pagesize) => {

    DashboardStore._ToggleProcessing(true)
    const result = await this.apiCall.GetActiveSubscription(pageNumber, pagesize);

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let data = resp.data;
          console.log(data)
          console.log("Saved jobs ")
          let thisData = data["subscriptions"];
          //  let Applicants=[];
          // let Counter = thisData.length;
          console.log(thisData)
          console.log("Just here")
          runInAction(() => {
            DashboardStore.Subscribtions.MyactiveSub = thisData;
            // DashboardStore.Subscribtions.Balance = 77;
            // DashboardStore.Subscribtions.ErrorMessage = Counter < 1 ? 'List Empty' : '';

            // DashboardStore.isGoBack=true;

          });

        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
          DashboardStore.Subscribtions.MyactiveSub  = 0
          DashboardStore.Subscribtions.ErrorMessage = message;
        }
      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        DashboardStore.Subscribtions.MyactiveSub  = 0
        DashboardStore.Subscribtions.ErrorMessage = message;

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      DashboardStore.Subscribtions.MyactiveSub  = 0
      DashboardStore.Subscribtions.ErrorMessage = "Sorry Error Occured";
    }
  }
  __SearchRecuiterAvailable = async (query) => {

    // DashboardStore._ToggleProcessing(false)
    const result = await this.apiCall.SearchRecuiterAvailable(query);

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    DashboardStore.Recruiters.CurrentJobId=0;
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let data = resp.data;
          console.log(data)
          // let thisData = data[0];
          console.log("FilterRecuiterSearch")
          runInAction(() => {
            DashboardStore.Recruiters.AvailableRecruiters = data;
            // DashboardStore.isGoBack = true;

          });

        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
          DashboardStore.Recruiters.AvailableRecruiters = [];
          DashboardStore.Recruiters.ErrorMessage = message;
        }
      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        DashboardStore.Recruiters.AvailableRecruiters = [];
        DashboardStore.Recruiters.ErrorMessage = message;

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      DashboardStore.Recruiters.AvailableRecruiters = [];
      DashboardStore.Recruiters.ErrorMessage = "Sorry Error Occured";
    }
  }
  __SearchAllAppUsers = async (showloader=true) => {

    // DashboardStore._ToggleProcessing(false)
    const result = await this.apiCall.SearchAllAppUsers(showloader);

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    // DashboardStore.Recruiters.CurrentJobId=0;
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let data = resp.data;
          console.log(data)
          // let thisData = data[0];
          console.log("SearchAllAppUsers")
          runInAction(() => {
            DashboardStore.AppUsersList.Userlist = data;
            // DashboardStore.isGoBack = true;

          });

        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
          DashboardStore.AppUsersList.Userlist = [];
          DashboardStore.AppUsersList.ErrorMessage = message;
        }
      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        DashboardStore.AppUsersList.Userlist = [];
        DashboardStore.AppUsersList.ErrorMessage = message;

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      DashboardStore.AppUsersList.Userlist = [];
      DashboardStore.AppUsersList.ErrorMessage = "Sorry Error Occured";
    }
  }
  __SendShortListToUser = async () => {


    const result = await this.apiCall.SendShortListToUser();

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let message = resp.message;
          let dataResponse = resp.data;
          console.log(message)
          console.log(dataResponse)
       
          ShowNotification(message, false);
           DashboardStore._updateParameterV2(true, "showSuccessReplyModal", "AppUsersList");
          // DashboardStore.__GetSubscriptionConfig(1,10);
          // DashboardStore.__GetAccountBalance();
        }
        else {
          DashboardStore._updateParameterV2(false, "showSuccessReplyModal", "AppUsersList");

           let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
        }

 
      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, false);
        DashboardStore._updateParameterV2(false, "showSuccessReplyModal", "AppUsersList");

      }
    }
    else {
      ShowNotification('Generic Error Occured', false);
      DashboardStore._updateParameterV2(false, "showSuccessReplyModal", "AppUsersList");


    }




  }
  __GetRecieversJobs = async (pageNumber, pagesize) => {

    console.log("GetRecieversJobs")

     const result = await this.apiCall.GetRecieversJobs(pageNumber, pagesize);

    console.log(result)
    DashboardStore._ToggleProcessing(false)
     if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let data = resp.data;
          console.log(data)
          let thisData = data.jobs;
          console.log("GetRecieversJobs")
          console.log(thisData)
          runInAction(() => {
            DashboardStore.MySharedJobs.MyJobList = thisData;
            // DashboardStore.isGoBack = true;

          });

        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          console.log("GetRecieversJobs")

          ShowNotification(message, true);
          DashboardStore.MySharedJobs.MyJobList = [];
          DashboardStore.MySharedJobs.ErrorMessage = message;
        }
      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        DashboardStore.MySharedJobs.MyJobList = [];
        DashboardStore.MySharedJobs.ErrorMessage = message;

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      DashboardStore.MySharedJobs.MyJobList = [];
      DashboardStore.MySharedJobs.ErrorMessage = "Sorry Error Occured";
    }
  }
  __GetSharedApplicantList = async (Joblevel,pageNumber, pagesize) => {

    console.log("GetSharedApplicantList")

     const result = await this.apiCall.GetSharedApplicantList(Joblevel,pageNumber, pagesize);

    console.log(result)
    DashboardStore._ToggleProcessing(false)
     if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let data = resp.data;
          console.log(data)
          let thisData = data.jobs;
          console.log("GetRecieversJobs")
          console.log(thisData)
          runInAction(() => {
            DashboardStore.SharedApplicant.SharedApplicantList = thisData;
            // DashboardStore.isGoBack = true;

          });

        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          console.log("GetRecieversJobs")

          ShowNotification(message, true);
          DashboardStore.SharedApplicant.SharedApplicantList = [];
          DashboardStore.SharedApplicant.ErrorMessage = message;
        }
      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        DashboardStore.SharedApplicant.SharedApplicantList = [];
        DashboardStore.SharedApplicant.ErrorMessage = message;

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      DashboardStore.SharedApplicant.SharedApplicantList = [];
      DashboardStore.SharedApplicant.ErrorMessage = "Sorry Error Occured";
    }
  }
  __SuggestStageToMoveApplicant = async () => {


    const result = await this.apiCall.SuggestStageToMoveApplicant();

    console.log(result)
    DashboardStore._ToggleProcessing(false);
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let message = resp.message;
          let dataResponse = resp.data;
          console.log(message)
          console.log(dataResponse)
       
          ShowNotification(message, false);
           DashboardStore._updateParameterV2(true, "showSuccessReplyModal", "SharedApplicant");
          // DashboardStore.__GetSubscriptionConfig(1,10);
          // DashboardStore.__GetAccountBalance();
        }
        else {
          DashboardStore._updateParameterV2(false, "showSuccessReplyModal", "SharedApplicant");

           let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
        }
      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, false);
        DashboardStore._updateParameterV2(false, "showSuccessReplyModal", "SharedApplicant");

      }
    }
    else {
      ShowNotification('Generic Error Occured', false);
      DashboardStore._updateParameterV2(false, "showSuccessReplyModal", "SharedApplicant");


    }




  }
  __AcceptOrRejectList = async (acceptOrDecline) => {

    const result = await this.apiCall.AcceptOrRejectList(acceptOrDecline);
    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;
        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let message = resp.message;
          let dataResponse = resp.data;
          console.log(message);
          console.log(dataResponse);
          ShowNotification(message);
          // DashboardStore.isGoBack=true;
          DashboardStore.__GetJobOffers(1, 50);
          DashboardStore._updateParameterV2(true, "isNavigationGoback", "ShortlistedApplicant");

        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, false);
        }
        // this._ToggleLoader('Categories',false)
      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        // DashboardStore.isJobApplySuccessful = false
      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      // DashboardStore.isJobApplySuccessful = false
    }




  }
  __SubmitRateAndReview = async (data) => {

    const result = await this.apiCall.SubmitRateAndReview (data);
    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;
        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let message = resp.message;
          let dataResponse = resp.data;
          console.log(message);
          console.log(dataResponse);
          ShowNotification(message);
          // DashboardStore.isGoBack=true;
          DashboardStore.__GetJobOffers(1, 50);
          DashboardStore._updateParameterV2(true, "isNavigationGoback", "ShortlistedApplicant");
 
        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, false);
          DashboardStore._updateParameterV2(false, "isNavigationGoback", "ShortlistedApplicant");

        }
        // this._ToggleLoader('Categories',false)
      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        DashboardStore._updateParameterV2(false, "isNavigationGoback", "ShortlistedApplicant");

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      DashboardStore._updateParameterV2(false, "isNavigationGoback", "ShortlistedApplicant");

    }




  }
  __GetActivityTracker= async (UserId,parentclass) => {

    console.log("__GetActivityTracker");

     const result = await this.apiCall.GetActivityTracker(UserId);

    console.log(result);
    console.log("<<<<<<>>>>>>");

    DashboardStore._ToggleProcessing(false)
     if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let data = resp.data;
          console.log(data)
          let thisData = data;
          console.log("__GetActivityTracker")
          console.log(thisData)
          runInAction(() => {
            DashboardStore[parentclass]["totalRequestAndAcceptance"] = thisData;
            // DashboardStore.isGoBack = true;

          });

        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          console.log("GetRecieversJobs")

          ShowNotification(message, true);
          DashboardStore[parentclass]["totalRequestAndAcceptance"]  = {};
          // DashboardStore.SharedApplicant.ErrorMessage = message;
        }
      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        DashboardStore[parentclass]["totalRequestAndAcceptance"]  = {};
        // DashboardStore.SharedApplicant.ErrorMessage = message;

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      DashboardStore[parentclass]["totalRequestAndAcceptance"]  = {};
      // DashboardStore.SharedApplicant.ErrorMessage = "Sorry Error Occured";
    }
  }
  __GetcompletedJobsLevel= async (UserId,parentclass) => {
    console.log("__GetActivityTracker");
     const result = await this.apiCall.GetcompletedJobsLevel(UserId);
    console.log(result);
    console.log("<<<<<<>>>>>>");

    DashboardStore._ToggleProcessing(false)
     if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let data = resp.data;
          console.log(data)
          let thisData = data;
          console.log("__GetActivityTracker")
          console.log(thisData)
          runInAction(() => {
            DashboardStore[parentclass]["completedJobsLevel"] = thisData;
            // DashboardStore.isGoBack = true;
          });
        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          console.log("GetRecieversJobs")
          ShowNotification(message, true);
          DashboardStore[parentclass]["completedJobsLevel"]  = {};
          // DashboardStore.SharedApplicant.ErrorMessage = message;
        }
      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        DashboardStore[parentclass]["completedJobsLevel"]  = {};
        // DashboardStore.SharedApplicant.ErrorMessage = message;

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      DashboardStore[parentclass]["completedJobsLevel"]  = {};
      // DashboardStore.SharedApplicant.ErrorMessage = "Sorry Error Occured";
    }
  }
  __GetshortlistAndInterviewRatio= async (UserId,parentclass) => {
    console.log("__GetActivityTracker");
     const result = await this.apiCall.GetshortlistAndInterviewRatio(UserId);
    console.log(result);
    console.log("<<<<<<>>>>>>");

    DashboardStore._ToggleProcessing(false)
     if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let data = resp.data;
          console.log(data)
          let thisData = data[0];
          console.log("__GetActivityTracker")
          console.log(thisData)
          runInAction(() => {
            DashboardStore[parentclass]["shortlistAndInterviewRatio"] = thisData;
            // DashboardStore.isGoBack = true;
          });
        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          console.log("GetRecieversJobs")
          ShowNotification(message, true);
          DashboardStore[parentclass]["shortlistAndInterviewRatio"]  = {};
          // DashboardStore.SharedApplicant.ErrorMessage = message;
        }
      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        DashboardStore[parentclass]["shortlistAndInterviewRatio"]  = {};
        // DashboardStore.SharedApplicant.ErrorMessage = message;

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      DashboardStore[parentclass]["shortlistAndInterviewRatio"]  = {};
      // DashboardStore.SharedApplicant.ErrorMessage = "Sorry Error Occured";
    }
  }
  __SearchForJobs = async (query) => {

     const result = await this.apiCall.SearchForJobs(query);

    console.log(result)
    DashboardStore._ToggleProcessing(false)
    DashboardStore.Recruiters.CurrentJobId=0;
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let data = resp.data;
          console.log(data)
          // let thisData = data[0];
          console.log("FilterRecuiterSearch")
          runInAction(() => {
            DashboardStore.JobSearch.SearchList = data;
            DashboardStore.isGoBack = true;

          });

        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, true);
          DashboardStore.JobSearch.SearchList = [];
          DashboardStore.JobSearch.ErrorMessage = message;
        }
      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        DashboardStore.JobSearch.SearchList = [];
        DashboardStore.JobSearch.ErrorMessage = message;

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      DashboardStore.JobSearch.SearchList = [];
      DashboardStore.JobSearch.ErrorMessage = "Sorry Error Occured";
    }
  }
  __HireApplicant = async (applicant_id) => {

    const result = await this.apiCall.HireApplicant(applicant_id);
    console.log(result)
    DashboardStore._ToggleProcessing(false)
    if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;
        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let message = resp.message;
          let dataResponse = resp.data;
          console.log(message);
          console.log(dataResponse);
          ShowNotification(message);
          // DashboardStore.isGoBack=true;
   
        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          ShowNotification(message, false);
          // DashboardStore._updateParameterV2(false, "isNavigationGoback", "ShortlistedApplicant");
        }
        // this._ToggleLoader('Categories',false)
      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        // DashboardStore._updateParameterV2(false, "isNavigationGoback", "ShortlistedApplicant");

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      // DashboardStore._updateParameterV2(false, "isNavigationGoback", "ShortlistedApplicant");

    }




  }
  __GetUserRateAndReviews= async (UserId,parentclass) => {
    console.log("__GetUserRateAndReviews");
     const result = await this.apiCall.GetUserRateAndReviews(UserId);
    console.log(result);
    console.log("<<<<<<>>>>>>");

    DashboardStore._ToggleProcessing(false)
     if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let data = resp.data;
          console.log(data)
          let thisData = data.rating;
          let average_rating = data.average_rating;
          console.log("__GetActivityTracker")
          console.log(thisData)
          runInAction(() => {
            DashboardStore[parentclass]["reviews"] = thisData;
            DashboardStore[parentclass]["average_rating"] = average_rating;
            // DashboardStore.isGoBack = true;
          });
        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          console.log("GetRecieversJobs")
          ShowNotification(message, true);
          DashboardStore[parentclass]["reviews"]  = [];
          DashboardStore[parentclass]["average_rating"]  = 0;
          // DashboardStore.SharedApplicant.ErrorMessage = message;
        }
      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        DashboardStore[parentclass]["reviews"]  = [];
        DashboardStore[parentclass]["average_rating"]  = 0;
        // DashboardStore.SharedApplicant.ErrorMessage = message;

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      DashboardStore[parentclass]["reviews"]  = [];
      DashboardStore[parentclass]["average_rating"]  = 0;

      // DashboardStore.SharedApplicant.ErrorMessage = "Sorry Error Occured";
    }
  }
  __GettimeToCompleteShortlist= async (UserId,parentclass) => {

    console.log("GettimeToCompleteShortlist");

     const result = await this.apiCall.GettimeToCompleteShortlist(UserId);

    console.log(result);
    console.log("<<<<<<>>>>>>");

    DashboardStore._ToggleProcessing(false)
     if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let data = resp.data;
          console.log(data)
          let thisData = data;
          console.log("__GetActivityTracker")
          console.log(thisData)
          runInAction(() => {
            DashboardStore[parentclass]["timeToCompleteShortlist"] = thisData;
            // DashboardStore.isGoBack = true;

          });

        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          console.log("GetRecieversJobs")

          ShowNotification(message, true);
          DashboardStore[parentclass]["timeToCompleteShortlist"]  = {};
          // DashboardStore.SharedApplicant.ErrorMessage = message;
        }
      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        DashboardStore[parentclass]["timeToCompleteShortlist"]  = {};
        // DashboardStore.SharedApplicant.ErrorMessage = message;

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      DashboardStore[parentclass]["timeToCompleteShortlist"]  = {};
      // DashboardStore.SharedApplicant.ErrorMessage = "Sorry Error Occured";
    }
  }
  __RecruiterComplitedByIndustry= async (UserId,parentclass,pageNumber, pagesize) => {

    console.log("RecruiterComplitedByIndustry");

     const result = await this.apiCall.RecruiterComplitedByIndustry(UserId,pageNumber, pagesize);

    console.log(result);
    console.log("<<<<<<>>>>>>");

    DashboardStore._ToggleProcessing(false)
     if (result !== false) {
      let response = result;
      if (response.status === 200 || response.status === 201) {
        let resp = response.data;

        let ApiStatus = resp.status
        if (ApiStatus === true) {
          console.log(resp)
          let data = resp.data;
          console.log(data)
          let thisData = data.data;
          console.log("__GetActivityTracker")
          console.log(thisData)
          runInAction(() => {
            DashboardStore[parentclass]["recruiterComplitedByIndustry"] = thisData;
            // DashboardStore.isGoBack = true;

          });

        }
        else {
          //   this.Categories = []
          let message = resp.message;
          console.log("heree" + message)
          console.log("GetRecieversJobs")

          ShowNotification(message, true);
          DashboardStore[parentclass]["recruiterComplitedByIndustry"]  = []
          // DashboardStore.SharedApplicant.ErrorMessage = message;
        }
      }
      else {
        let resp = response.data;
        //  console.warn(resp);
        let message = resp.message
        ShowNotification(message, true);
        DashboardStore[parentclass]["recruiterComplitedByIndustry"]  = []
        // DashboardStore.SharedApplicant.ErrorMessage = message;

      }
    }
    else {
      ShowNotification('Generic Error Occured', true);
      DashboardStore[parentclass]["recruiterComplitedByIndustry"]  = [nooo];
      // DashboardStore.SharedApplicant.ErrorMessage = "Sorry Error Occured";
    }
  }
  //////////////////////////////////////////////////

  



























}

const apiConnect = new ApiConnect();
// const todoStore = new TodoStore(todoApi);



const DashboardStore = new Dashboard(apiConnect)
export default DashboardStore
// export default withNavigation(this);
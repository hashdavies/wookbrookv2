import {
  StyleSheet,
  ImageBackground,
  Keyboard,
  TouchableOpacity,
  FlatList,
   Alert,
  ScrollView,
  Image,
} from 'react-native';

import { observable, action } from 'mobx';
import qs from 'qs';
import axios from 'axios'
 
import Toast from 'react-native-toast-native';
import {loggedinUserdetails,} from '../dependency/UtilityFunctions'

import ToastStyle from "../StyleSheet/ToastStyle";
import { CHANNEL } from '../constants/Constants';
import { PersistData } from '../dependency/UtilityFunctions';

import {NavigationActions,navigation} from 'react-navigation';
import { withNavigation } from 'react-navigation';

import { onSignIn } from "../auth";

class Case {
    @observable newCaseData = {
      case_title:'',
      case_number:'',  
      Type:'',  
      Description:'',
      }
    @observable selectedCases = [];
    // @observable selectedUsers = [2100,1081,2103];
    @observable selectedUsers = [];
    @observable caseStatuses = [];
    @observable userCases = [];
    @observable allJudges = [];
    @observable allLawyers = [];
    @observable caseStatus = [];
    @observable userCaseNotes = [];
    @observable userSingleCase = {};
    @observable disabled = false
    @observable loader_visible = false
    @observable longpress_active = false
    @observable redirectmode = false;
    @observable singlecaseId = '';
    @observable color = '#F0BA00'
  
  
    @action  _ToggleLoader = (status) => {
this.loader_visible=status;

    }
    @action  ChangeSinglecaseId = (newid) => {
this.singlecaseId=newid;

    }
  
    @action  _ToggleLongpress_status = (status) => {
this.longpress_active=status;
    }
    @action  onChangeText = (key, value) => {
      this.newCaseData[key]=value;
      // if(key==='Reg_Role'){
      //     this.newCaseData.Reg_Role=value;
      // }
      // else if(key==='Reg_firstname'){
      //     this.newCaseData.Reg_firstname=value;
      // }
      // else if(key==='Reg_lastname'){
      //     this.newCaseData.Reg_lastname=value;
      // }
      
      
      // else{
      //     console.warn("Not possible");
      // }
              
              // console.warn(key+value);
              // console.warn(this.newCaseData);
              
          }
  
    @action  async  _LoadUserCases  (status) {
 
        loggedinUserdetails().then(
            (resp) => {
          
       let isloggedin =resp.isloggedin;
       if(isloggedin===true){
        let Myid = resp.userid
       
        console.warn(Myid);
         
        let params={
            UserId: Myid,
            status: status
          
          }
          // console.warn(params);
          Toast.show('Loading...', Toast.SHORT, Toast.BOTTOM,styletoest);
        this.loader_visible=true
          axios.post('/user/cases', qs.stringify(params))
                .then(response => {
                    this.loader_visible=false    
                    console.warn(response);
                    let resp=response.data;
                //   console.warn(resp)
                  if(resp.StatusCode === '00') {
                    let data=resp.Data;   
                       console.warn(data);         
           
       this.userCases=data;
       
    }
                         else {
                           console.warn('error');
                           let resp=response.data;
                           console.warn(resp);
                           let message =resp.Message
                           Toast.show(message, Toast.SHORT, Toast.BOTTOM,styletoest);
                           
                             CaseStore._ToggleLoader(false)
                           
                         }              
                })
                .catch(err => console.warn(err)) 
       }
          
   }
          )
          .catch((error) => {
            console.warn(`user id ${error}`);
          });






      
      
         
      };

       GetSingleData = (caseid) => {
           
   this._ToggleLoader(true)
        console.warn(caseid);
          axios.get( `/case/${caseid}`)
                    .then( response => {
                      let resp=response.data;
                      // console.warn(resp);
                   if(resp.StatusCode === '00') {
                  let data=resp.Data;
                 console.warn(data)
                 console.warn(data.JudicialDivision.Area)
                 console.warn(data.JudicialDivision)
               
         this.userSingleCase=data
        
             this._ToggleLoader(false)
                      }
                      else {
                          console.warn('error');
                          let resp=response.data;
                          console.warn(resp);
                          let message =resp.Message
                          Toast.show(message, Toast.SHORT, Toast.BOTTOM,styletoest);
                          this.userSingleCase=[]
                         this._ToggleLoader(false)
                      }  
                    } )
                    .catch(error => {
                        console.warn(`error ${error}`);
                        this.userSingleCase=[];
                         this._ToggleLoader(false)
                    });
       }

      GetCaseNote = (caseid) => {
   console.warn("GetCaseNote")
          
//    this._ToggleLoader(true)
        console.warn(caseid);
          axios.get( `/case/${caseid}/quicknotifications`)
                    .then( response => {
                      let resp=response.data;
                      // console.warn(resp);
                   if(resp.StatusCode === '00') {
                  let data=resp.Data;
                //  console.warn(data)
         this.userCaseNotes=data
        
             this._ToggleLoader(false)
                      }
                      else {
                          console.warn('error');
                          let resp=response.data;
                          console.warn(resp);
                          let message =resp.Message
                          Toast.show(message, Toast.SHORT, Toast.BOTTOM,styletoest);
                          this.userCaseNotes=[]
                        //  this._ToggleLoader(false)
                      }  
                    } )
                    .catch(error => {
                        console.warn(`error ${error}`);
                        this.userCaseNotes=[];
                        //  this._ToggleLoader(false)
                    });
       }
      GetCaseStatus = () => {
   console.warn("GetCaseStatus")
          
//    this._ToggleLoader(true)
        
          axios.get( `/case/statuses`)
                    .then( response => {
                      let resp=response.data;
                      // console.warn(resp);
                   if(resp.StatusCode === '00') {
                  let data=resp.Data;
                console.warn(data)
         this.caseStatuses=data
        
            // this._ToggleLoader(false)
                      }
                      else {
                          console.warn('error');
                          let resp=response.data;
                          console.warn(resp);
                          let message =resp.Message
                          Toast.show(message, Toast.SHORT, Toast.BOTTOM,styletoest);
                          this.caseStatuses=[]
                        //  this._ToggleLoader(false)
                      }  
                    } )
                    .catch(error => {
                        console.warn(`error ${error}`);
                        this.caseStatuses=[];
                        //  this._ToggleLoader(false)
                    });
       }
    
       LoadCaseStatus = () => {

        axios.get( '/case/statuses')
                  .then( response => {
                    let resp=response.data;
                    // console.warn(resp);
                 if(resp.StatusCode === '00') {
                  // console.warn('done');
               let data=resp.Data;
              this.caseStatus=data;
           
           this._ToggleLoader(false);
                    }
                    else {
                      console.warn('error');
                      Toast.show(response.data.message, Toast.SHORT, Toast.BOTTOM,styletoest);
                   
                       this._ToggleLoader(false);
                    }  
                  } )
                  .catch(error => {
                      console.warn(`error loading status ${error}`);
    
                       this._ToggleLoader(false)
                  });
      };
    

        AddCaseNote  (notedata) {
// let params={
//             UserId: Myid,
//             status: status
          
//           }
        
          console.warn(notedata);
        this.loader_visible=true
           axios.post('/quicknotification/add', qs.stringify(notedata))
                .then( response => {
                    this.loader_visible=false    
                
                  console.warn(response)
                  let resp=response.data;
                 if(resp.StatusCode === '00') {
                let message =resp.Message
                Toast.show(message, Toast.SHORT, Toast.BOTTOM,styletoest);
this.GetCaseNote(notedata.CaseId);
                  }
                  else {
                    this.loader_visible=false    
                    console.warn('error');
                    let resp=response.data;
                    console.warn(resp);
                    let message =resp.Message
                    Toast.show(message, Toast.SHORT, Toast.BOTTOM,styletoest);
                      //  this._ToggleLoader(false)
                    
                  }           
                })
                .catch(err => console.warn(err)) 
 
      
         
      };
      @action  AssignCase  (casedata) {

        
          console.warn(casedata);
        this.loader_visible=true
           axios.post('/case/assign', qs.stringify(casedata))
                .then( response => {
                    this.loader_visible=false    
                
                  console.warn(response)
                  let resp=response.data;
                 if(resp.StatusCode === '00') {
                let message =resp.Message
                Toast.show(message, Toast.SHORT, Toast.BOTTOM,styletoest);
                this.selectedCases= [];
              }
                  else {
                    console.warn('error');
                    let resp=response.data;
                    console.warn(resp);
                    let message =resp.Message
                    Toast.show(message, Toast.SHORT, Toast.BOTTOM,styletoest);
                       this._ToggleLoader(false)
                       this.selectedCases= [];
                  }           
                })
                .catch(err => {
                  console.warn(err)
                  this._ToggleLoader(false)
                }) 
 
      
         
      };
      @action  RemoveLawyer_Case = (item) => {
        console.warn(item);
        Alert.alert(
          'Remove',
          `Remove  ${item.FirstName + " "+item.LastName} From this case`,
          [
               {
                  text: 'No', onPress: () => { },//Do nothing
                  style: 'cancel'
              },
              {
                  text: 'Yes', onPress: () => {
                    const params={      
         "CaseId":this.singlecaseId,
	"RemoverUserId":global.userid, 
	"LawyerUserId":item.Id
                    }
                    console.warn(params);
                    this.RemoveLawyerNow(params);
                  }
              },
          ],
          { cancelable: true }
      );
  
      }
      @action  RemoveLawyerNow  (Data) {

        
          console.warn(Data);
        this.loader_visible=true
           axios.post('/case/removelawyer', qs.stringify(Data))
                .then( response => {
                    this.loader_visible=false    
                
                  console.warn(response)
                  let resp=response.data;
                 if(resp.StatusCode === '00') {
                let message =resp.Message
                Toast.show(message, Toast.SHORT, Toast.BOTTOM,styletoest);
                
              }
              else {
                    console.warn('error');
                    let resp=response.data;
                    console.warn(resp);
                    let message =resp.Message
                    Toast.show(message, Toast.SHORT, Toast.BOTTOM,styletoest);
                       this._ToggleLoader(false)
                       
                  }           
                })
                .catch(err => {
                  console.warn(err)
                  this._ToggleLoader(false)
                }) 
 
      
         
      };
      @action  ReAssignCase  (casedata) {

        
          console.warn(casedata);
        this.loader_visible=true
           axios.post('/case/reassign', qs.stringify(casedata))
                .then( response => {
                    this.loader_visible=false    
                
                  console.warn(response)
                  let resp=response.data;
                 if(resp.StatusCode === '00') {
                let message =resp.Message
                Toast.show(message, Toast.SHORT, Toast.BOTTOM,styletoest);
                   
              }
                  else {
                    console.warn('error');
                    let resp=response.data;
                    console.warn(resp);
                    let message =resp.Message
                    Toast.show(message, Toast.SHORT, Toast.BOTTOM,styletoest);
                       this._ToggleLoader(false)
                    
                  }           
                })
                .catch(err =>                  
                  {
                    console.warn(err)
                    console.warn(`${err} error`)
                    this._ToggleLoader(false)
                  }
                  
                  ) 
 
      
         
      };
      @action  NotifyLwayer  (casedata) {

        
          console.warn(casedata);
        this.loader_visible=true
           axios.post('/case/notify', qs.stringify(casedata))
                .then( response => {
                    this.loader_visible=false    
                
                  console.warn(response)
                  let resp=response.data;
                 if(resp.StatusCode === '00') {
                let message =resp.Message
                Toast.show(message, Toast.SHORT, Toast.BOTTOM,styletoest);
                this.selectedUsers= [];
              }
                  else {
                    console.warn('error');
                    let resp=response.data;
                    console.warn(resp);
                    let message =resp.Message
                    Toast.show(message, Toast.SHORT, Toast.BOTTOM,styletoest);
                       this._ToggleLoader(false)
                       this.selectedUsers= [];
                  }           
                })
                .catch(err => console.warn(err)) 
 
      
         
      };
      
      @action  ToggleCaseArray = (caseid) => {
        if(caseid==='empty'){
          this.selectedCases= [];
        }
        else{
          if(this.longpress_active===true){
            var check = this.selectedCases.includes(caseid);
            if(check===true){
        console.warn("true")
        this.selectedCases.splice(this.selectedCases.indexOf(caseid), 1);
        if(this.selectedCases.length<= 0){
        this._ToggleLongpress_status(false);
        }
            }
            else{
              console.warn("false")
              this.selectedCases[this.selectedCases.length] = caseid;
            }
        console.warn(this.selectedCases);
          }
          else{
            console.warn("longpree")
          }
        }

     
            }

      @action  ToggleLawyerArray = (userid) => {
        if(userid==='empty'){
          this.selectedUsers= [];
        }
        else{
          var check = this.selectedUsers.includes(userid);
          if(check===true){
      console.warn("true")
      this.selectedUsers.splice(this.selectedUsers.indexOf(userid), 1);
       
          }
          else{
            console.warn("false")
            this.selectedUsers[this.selectedUsers.length] = userid;
          }
      console.warn(this.selectedUsers);
        }
 
  
            }


            Getalljudges = (JudicialDivisionId) => {
              console.warn("get judges")
               this._ToggleLoader(true)
                   console.warn(JudicialDivisionId);
                     axios.get( `/users/judges/${JudicialDivisionId}`)
                               .then( response => {
                                 let resp=response.data;
                                 // console.warn(resp);
                              if(resp.StatusCode === '00') {
                             let data=resp.Data;
                           //  console.warn(data)
                    this.allJudges=data
                   
                        this._ToggleLoader(false)
                                 }
                                 else {
                                     console.warn('error');
                                     let resp=response.data;
                                     console.warn(resp);
                                     let message =resp.Message
                                     Toast.show(message, Toast.SHORT, Toast.BOTTOM,styletoest);
                                     this.allJudges=[]
                                    this._ToggleLoader(false)
                                 }  
                               } )
                               .catch(error => {
                                   console.warn(`error ${error}`);
                                   this.allJudges=[];
                                    this._ToggleLoader(false)
                               });
                  }

                  @action   GetallLawyers = () => {
              console.warn("get Lawyers");
               this._ToggleLoader(true);
                      axios.get( `/user/lawyersandlawstudents`)
                               .then( response => {
                                 let resp=response.data;
                               if(resp.StatusCode === '00') {
                             let data=resp.Data;
                             var onlyLawyers =  data.filter(function(sData) {
                              return sData.Category == 'lawyer';
                               });
                     this.allLawyers=onlyLawyers                   
                        this._ToggleLoader(false)
                                 }
                                 else {
                                     console.warn('error');
                                     let resp=response.data;
                                     console.warn(resp);
                                     let message =resp.Message
                                     Toast.show(message, Toast.SHORT, Toast.BOTTOM,styletoest);
                                     this.allLawyers=[]
                                    this._ToggleLoader(false)
                                 }  
                               } )
                               .catch(error => {
                                   console.warn(`error ${error}`);
                                   this.allLawyers=[];
                                    this._ToggleLoader(false)
                               });
                  }
               

                  @action  async CreateNewCase  (casedata) {

        
                    // console.warn(casedata);
                  this.loader_visible=true
                    await  axios.post('/case/register', qs.stringify(casedata))
                          .then( response => {
                              this.loader_visible=false    
                          
                            console.warn(response)
                            let resp=response.data;
                           if(resp.StatusCode === '00') {
                          let message =resp.Message
                          Toast.show(message, Toast.SHORT, Toast.BOTTOM,styletoest);
                             this.newCaseData= {
                              case_title:'',
                              case_number:'',  
                              Type:'',  
                              Description:'',
                              }
                              // this.newCaseData.case_title='';
                              console.warn(this.newCaseData)
                              this.redirectmode=true;
                        }
                            else {
                              console.warn('error');
                              let resp=response.data;
                              console.warn(resp);
                              let message =resp.Message
                              Toast.show(message, Toast.SHORT, Toast.BOTTOM,styletoest);
                                 this._ToggleLoader(false)
                              
                            }           
                          })
                          .catch(err => console.warn(err)) 
           
                
                   
                };
                





























     
}

const CaseStore = new Case()
export default CaseStore
// export default withNavigation(CaseStore);
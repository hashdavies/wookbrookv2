import {
  StyleSheet,
  ImageBackground,
  Keyboard,
  TouchableOpacity,
  FlatList,
   Alert,
  ScrollView,
  Image,
} from 'react-native';

 

import { observable, action } from 'mobx';
import qs from 'qs';
import axios from 'axios'
 
import Toast from 'react-native-toast-native';
import ToastStyle from "../StyleSheet/ToastStyle";
import {loggedinUserdetails,} from '../dependency/UtilityFunctions'

 
import { CHANNEL } from '../constants/Constants';
import { PersistData,generate_id,arrayToObject,ReduceName2FN_LN } from '../dependency/UtilityFunctions';
 import {NavigationActions,navigation} from 'react-navigation';
import { withNavigation } from 'react-navigation';

import { onSignIn } from "../auth";



class CourtLocation {
    
    @observable allCourt = [];
 
    @observable disabled = false
    @observable loader_visible = false
    
    @observable color = '#F0BA00'
  
  
    @action  _ToggleLoader = (status) => {
this.loader_visible=status;

    }

  
    @action  SaveNote2DB  (params) {
       
                
      console.warn(params);
    this.loader_visible=true
       axios.post('/note/add', qs.stringify(params))
            .then( response => {
                this.loader_visible=false    
            
              console.warn(response)
              let resp=response.data;
             if(resp.StatusCode === '00') {
            let message =resp.Message
            Toast.show(message, Toast.SHORT, Toast.BOTTOM,ToastStyle);
               }
              else {
                this.loader_visible=false    
                console.warn('error');
                let resp=response.data;
                console.warn(resp);
                let message =resp.Message
                Toast.show(message, Toast.SHORT, Toast.BOTTOM,ToastStyle);
                   this._ToggleLoader(false)
                
              }           
            })
            .catch(err => console.warn(err)) 

  
     
  };
    






    GetallCourt = () => {
      console.warn("GetallCourt")
            this._ToggleLoader(true)
          //  console.warn(userid);
              axios.get( `courts`)
              
                       .then( response => {
                         let resp=response.data;
                         console.warn(resp);
                      if(resp.StatusCode === '00') {
                     let Original_data=resp.Data;
                    console.warn(Original_data)
                    const FormattedData = Original_data.map(singleData => {
                       
                      // let obj2 = Object.assign({}, obj1);
                    return { 
                      _id:singleData.Id,
                      bannerName: ReduceName2FN_LN(singleData.Name),
                      Name:singleData.Name,
                      Address:singleData.Address,
                      // coordinates: new AnimatedRegion({
                      //   latitude: parseFloat(singleData.Latitude),
                      //   longitude: parseFloat(singleData.Longitude)
                      // }),
                      coordinate: {
                        latitude: parseFloat(singleData.Latitude),
                        longitude: parseFloat(singleData.Longitude)
                      }, 
                 

                    }

                    });
     this.allCourt=FormattedData;

   console.warn(FormattedData);
           
                this._ToggleLoader(false)
                         }
                         else {
                             console.warn('error');
                             let resp=response.data;
                             console.warn(resp);
                             let message =resp.Message
                             Toast.show(message, Toast.SHORT, Toast.BOTTOM,ToastStyle);
                             this.allCourt=[]
                            this._ToggleLoader(false)
                         }  
                       } )
                       .catch(error => {
                           console.warn(`error ${error}`);
                           this.allCourt=[];
                            this._ToggleLoader(false)
                       });
          }


    


           
 


     
}

const CourtLocationStore = new CourtLocation()
export default CourtLocationStore
 
import axios from "axios";
import qs from "qs";
import Spinner from "react-native-loading-spinner-overlay";
import React, { Component } from "react";
import {
  StyleSheet,
  PixelRatio,
  ActivityIndicator,
  Alert,
  Platform,
} from "react-native";

// import UUIDGenerator from 'react-native-uuid-generator';
import { PermissionsAndroid } from "react-native";
import { NavigationActions, navigation } from "react-navigation";
import { withNavigation } from "react-navigation";
import Communications from "react-native-communications";
import Snackbar from "react-native-snackbar";

import Toast from "react-native-toast-native";
import ToastStyle from "../StyleSheet/ToastStyle";
import { View } from "native-base";
import AnimatedLoader from "react-native-animated-loader";
import SkeletonPlaceholder from "react-native-skeleton-placeholder";
import DocumentPicker from "react-native-document-picker";
import RNFetchBlob from "react-native-fetch-blob";
import RNFS from "react-native-fs";
import firebase from "@react-native-firebase/app";
import dynamicLinks from "@react-native-firebase/dynamic-links";

///////////////////// Google Sign in

import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes,
} from "react-native-google-signin";
import AccountStore from "../stores/Account";
///////////////////////////////////////// Google Sign in
import ImagePicker_crop from "react-native-image-crop-picker";

export const ReturnPartOfText = (text, length) => {
  let size = length;
  if (text.length < length) {
    size = text.length;
  }
  let capitalize = text.slice(0, 1).toUpperCase() + text.slice(1, size);
  return capitalize;
};
export const myspiner = (state, text = null) => {
  let spin = null;
  if (state === true) {
    spin = (
      <View style={styles.Spinnercontainer}>
        <AnimatedLoader
          visible={state}
          // overlayColor="rgba(255,255,255,0.75)"
          overlayColor="rgba(0.00,0.00,0.00,0.1)"
          //overlayColor="000000"
          // source={require("./loader.json")}
          // source={{ uri:'https://assets7.lottiefiles.com/datafiles/QeC7XD39x4C1CIj/data.json'}}
          animationStyle={styles.lottie}
          speed={1}
        />
      </View>
    );
  }
  return spin;
};
export const SkeletonLoader = (state, text = null) => {
  let spin = (
    <SkeletonPlaceholder>
      <View
        style={{
          backgroundColor: "#fff",
          flex: 1,
          justifyContent: "flex-end",
          marginBottom: 0,
          padding: 19,
          height: 140,
        }}
      >
        <View style={styles.displayTitle}></View>
        <View style={styles.displayTitle}></View>
        <View style={styles.displayTitle}></View>
      </View>
    </SkeletonPlaceholder>
  );

  return spin;
};

export const MapLoader = (state, text = null) => {
  let spin = (
    <View style={styles.Spinnercontainer}>
      <Spinner
        visible={state}
        textContent={text ? text : "Please Wait..."}
        textStyle={{ color: "#fff" }}
        animation={"fade"}
        color={"#bd393b"}
        overlayColor={"#000"}
        cancelable={true}
      />
    </View>
  );
  return spin;
};
export const Communicator = (action, param) => {
  if (param === "") {
    // Toast.show('Not available', Toast.SHORT, Toast.BOTTOM,styletoest)
    ShowNotification("Not Available", true);
  } else if (action === "") {
    // Toast.show('empty Action', Toast.SHORT, Toast.BOTTOM,styletoest)
    ShowNotification("Not Available", true);
  } else {
    if (action === "phone") {
      Communications.phonecall(param, true);
    } else if (action === "web") {
      Communications.web(param);
    } else if (action === "message") {
    }
  }
};

export const ShowNotification = (Text, nodelay = true) => {
  // delay= false;
  if (nodelay) {
    Snackbar.show({
      title: Text,
      duration: Snackbar.LENGTH_LONG,
    });
  } else {
    setTimeout(() => {
      Snackbar.show({
        title: Text,
        duration: Snackbar.LENGTH_LONG,
      });
    }, 1000);
  }
};

export const activityspiner = (state) => {
  let spin = (
    <View style={styles.activityIndicatorcontainer}>
      <ActivityIndicator
        animating={state}
        color="#bc2b78"
        size="large"
        style={styles.activityIndicator}
      />
    </View>
  );

  return spin;
};

export const PersistData = (Mykey, data) => {
  global.storage.save({
    key: Mykey, // Note: Do not use underscore("_") in key!
    data: {
      isAvailable: true,
      storeData: data,
    },

    // if not specified, the defaultExpires will be applied instead.
    // if set to null, then it will never expire.
    expires: null,
  });
};

export const GetPersistData = (key) =>
  new Promise((resolve, reject) => {
    global.storage
      .load({
        key: key,
      })
      .then((ret) => {
        if (ret.isAvailable === true) {
          let data = ret.storeData;
          resolve(data);
        } else {
          resolve(false);
        }
      })
      .catch((err) => {
        // any exception including data not found
        // goes to catch()
        console.warn(err.message);
        switch (err.name) {
          case "NotFoundError":
            // alert("You have not register before")
            resolve(false);
            break;
          case "ExpiredError":
            // TODO
            resolve(false);
            break;
        }
      });
  });
export const loggedinUserdetails = () =>
  new Promise((resolve, reject) => {
    global.storage
      .load({
        key: "Loggedinstate",
      })
      .then((ret) => {
        resolve(ret);
      })
      .catch((err) => {
        // any exception including data not found
        // goes to catch()
        console.warn(err.message);
        switch (err.name) {
          case "NotFoundError":
            // alert("You have not register before")
            resolve({});
            break;
          case "ExpiredError":
            // TODO
            resolve({});
            break;
        }
      });
  });
export const gettoken = () =>
  new Promise((resolve, reject) => {
    // let data= await GetPersistData('Loggedinstate');
    // console.log(data)
    // console.log("soriioo")
    global.storage
      .load({
        key: "Loggedinstate",
      })
      .then(async (ret) => {
        // resolve( ret);
        if (ret.isAvailable === true) {
          if (ret.storeData.isloggedin === true) {
            let token = await ret.storeData.accessToken;
            console.log(token);
            console.log("token");
            let loginSession = `Bearer ${token};`;
            resolve(loginSession);
          } else {
            resolve("jinadBABA");
          }
        } else {
          resolve("jinadBABA");
        }
      })
      .catch((err) => {
        // any exception including data not found
        // goes to catch()
        console.warn(err.message);
        switch (err.name) {
          case "NotFoundError":
            // alert("You have not register before")
            resolve("jinadBABA");
            break;
          case "ExpiredError":
            // TODO
            resolve("jinadBABA");
            break;
        }
      });
    // resolve("jinad")
  });
export const getusertype = () =>
  new Promise((resolve, reject) => {
    global.storage
      .load({
        key: "Loggedinstate",
      })
      .then((ret) => {
        resolve(ret);
      })
      .catch((err) => {
        // any exception including data not found
        // goes to catch()
        console.warn(err.message);
        switch (err.name) {
          case "NotFoundError":
            // alert("You have not register before")
            resolve({});
            break;
          case "ExpiredError":
            // TODO
            resolve({});
            break;
        }
      });
  });
export const getLodgedInDetailsProfile = () =>
  new Promise((resolve, reject) => {
    global.storage
      .load({
        key: "loggedinUserdetails",
      })
      .then((ret) => {
        resolve(ret);
      })
      .catch((err) => {
        // any exception including data not found
        // goes to catch()
        console.warn(err.message);
        switch (err.name) {
          case "NotFoundError":
            //      alert("You have not register before")
            resolve({});
            break;
          case "ExpiredError":
            //    alert("You have not register dd")
            resolve({});
            break;
        }
      });
  });
export const YOUR_CLOUDINARY_NAME = () => {
  return "workbrook-hash";
};
export const YOUR_CLOUDINARY_PRESET = () => {
  return "awcal2vq";
};
export const YOUR_CLOUDINARY_PRESET_JobBanner = () => {
  return "dlqiv9hp";
};
export const YOUR_CLOUDINARY_PRESET_Document = () => {
  return "qfhydrgx";
};
export const YOUR_CLOUDINARY_PRESET_JobApplicationFiles = () => {
  return "JobApplicationFiles";
};
//  export const temp = async () => {
export const temp = () =>
  new Promise((resolve, reject) => {
    global.storage
      .load({
        key: "Loggedinstate",
      })
      .then(async (ret) => {
        // resolve( ret);
        console.log("jijij");
        if (ret.isAvailable === true) {
          if (ret.storeData.isloggedin === true) {
            var token = await ret.storeData.accessToken;
            console.log(token);
            console.log("token");
            let loginSession = `Bearer ${token}`;
            // let loginSession=`Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVlNTNkMzg1ZDAzMzIyNGU4YTRkNWFkZSIsImlhdCI6MTU4MjU1MTk0MSwiZXhwIjoxNTg1MTQzOTQxfQ.x_1I6p3CTU7x4vU5Ez6dEuTQuxDTFtnqNFIDz87XQ-0`;
            console.log(loginSession);
            resolve(loginSession);
            // return
          } else {
            resolve("jinadBABA");
          }
        } else {
          resolve("jinadBABA");
        }
      })
      .catch((err) => {
        // any exception including data not found
        // goes to catch()
        console.warn(err.message);
        switch (err.name) {
          case "NotFoundError":
            // alert("You have not register before")
            resolve("jinadBABA");
            break;
          case "ExpiredError":
            // TODO
            resolve("jinadBABA");
            break;
        }
      });
  });

export async function request_device_location_runtime_permission() {
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      {
        title: "wookbrook need Location Permission",
        message: "wookbrook App needs access to your location ",
      }
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      // Alert.alert("Location Permission Granted.");
      console.log("Location Permission Granted.");
      return true;
    } else {
      console.log("Location Permission Not Granted.");
      // Alert.alert("Location Permission Not Granted");
      return false;
    }
  } catch (err) {
    console.warn(err);
  }
}

export const AppInlineLoader = (Status) => {
  try {
    // Check If Loading
    if (Status) {
      return <ActivityIndicator />;
    } else {
      return null;
    }
  } catch (error) {
    console.log(error);
    return null;
  }
};

export const combinedatentimeplusdiffrentoffset = (date, time) => {
  //  console.warn("po")
  //  console.warn(time)
  //  console.warn(date)
  //  console.warn("po")
  let d = new Date();
  let Offset = d.getTimezoneOffset();
  Offset = parseInt(Offset) / 60;
  let timeArray = time.split(":");
  let hour = timeArray[0];
  hour = parseInt(hour) + Offset;
  let minute = timeArray[1];
  let ampm = hour >= 12 ? "PM" : "AM";
  if (hour > 12) {
    hour = hour - 12;
  }
  //  var timeString = hour + ':' + minute + ':00 '+ ampm +' UTC';
  var timeString = hour + ":" + minute + ":00 " + ampm + " UTC";

  //  let timeString = time.getHours() + ':' + time.getMinutes() + ':00';
  //  let timeString = "";
  var dateArray = date.split("-");
  // var year = date.getFullYear();
  // var month = date.getMonth() + 1; // Jan is 0, dec is 11
  // var day = date.getDate();
  var dateArray = date.split("-");
  let day = dateArray[2];
  let month = dateArray[1];
  let year = dateArray[0];
  var dateString = "" + month + "/" + day + "/" + year;
  // console.warn(dateString)
  // console.warn("hey")
  let h = dateString + " " + timeString;
  // console.warn(h.toISOString())
  var combined = new Date(dateString + " " + timeString);

  // console.warn(combined)
  // console.warn("combined")

  // console.warn(arrayOfStrings)
  // console.warn(arrayOfStrings[0])
  // return combined;
  var datetime = new Date(h).toISOString();
  // console.warn(datetime)
  return datetime;
  // return h;
};

export const RefreshProfile = () => {
  let userid = global.userid;
  axios
    .get(`user/${userid}`)
    .then((response) => {
      // this.setState({
      //   loader_visible:false
      //  })
      let resp = response.data;
      console.warn(resp);
      if (resp.StatusCode === "00") {
        // console.warn('done');
        let data = resp.Data;
        //  console.warn(global.profile)
        //  delete global.profile;
        //  console.warn(global.profile)
        global.profile = data;
        PersistData("loggedinUserdetails", data);
      } else {
        console.warn("error");
        let resp = response.data;
        console.warn(resp);
        let message = resp.Message;
        Toast.show(message, Toast.SHORT, Toast.BOTTOM, ToastStyle);
      }
    })
    .catch((error) => {
      Toast.show(
        "Unable to refresh page",
        Toast.SHORT,
        Toast.BOTTOM,
        ToastStyle
      );
    });
};

export const requestCameraPermission = async () => {
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.CAMERA,
      {
        title: "wookbrook Camera Permission",
        message:
          "wookbrook App needs access to your camera " +
          "so you can update your profile picture",
      }
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      console.log("You can use the camera");
    } else {
      console.log("Camera permission denied");
    }
  } catch (err) {
    console.warn(err);
  }
};
export const ReduceName2FN_LN = (text) => {
  var stringArray = text.split(" ");
  let fn = stringArray[0];
  let ln;
  if (stringArray[1] == null || stringArray[1] == undefined) {
    ln = "";
  } else {
    ln = stringArray[1];
  }
  let ReturnedString = fn + "  " + ln;
  return ReturnedString;
};

export const arrayToObject = (array) =>
  array.reduce((obj, item) => {
    obj[item._date] = item;
    return obj;
  }, {});

export const _DocumentPicker = async () => {
  try {
    const res = await DocumentPicker.pick({
      // type: [DocumentPicker.types.pdf, DocumentPicker.types.plainText],
      type: [DocumentPicker.types.pdf],
    });
    console.log(
      res.uri,
      res.type, // mime type
      res.name,
      res.size
    );
    const source = {
      uri: res.uri,
      type: res.type,
      name: res.name,
    };
    return res;
    // this.setState({
    //   documentName: res.name,
    //   isfileSelected: true,
    //   selectedDoc: source
    // })

    // setTimeout(() => {
    //   // this._uploadFileNow(res);
    //   this.cloudinaryUpload(source)

    // }, 1000);
  } catch (err) {
    if (DocumentPicker.isCancel(err)) {
      ShowNotification("Action canceled", true);
      return "cancel";
      // User cancelled the picker, exit any dialogs or menus and move on
    } else {
      // throw err;
      ShowNotification("An error occured", true);
      console.log(err);
      return false;
    }
  }
};
// export const  cloudinaryUpload = async (selectedDoc) => {
export const cloudinaryUpload = async (selectedDoc, upload_preset = "") =>
  new Promise((resolve, reject) => {
    // const { selectedDoc } = this.state;
    // let file=
    // DashboardStore._ToggleProcessing(true)
    let My_upload_preset =
      upload_preset === "" ? YOUR_CLOUDINARY_PRESET_Document() : upload_preset;
    console.log("here man");
    ShowNotification("Uploading your file...", true);

    const data = new FormData();
    data.append("file", selectedDoc);
    data.append("upload_preset", My_upload_preset);
    data.append("cloud_name", "workbrook-hash");
    fetch("https://api.cloudinary.com/v1_1/workbrook-hash/upload", {
      // https://api.cloudinary.com/v1_1/workbrook-hash/image/upload
      method: "post",
      body: data,
    })
      .then((res) => res.json())
      .then((data) => {
        console.log("<>>><<");
        console.log(data);
        let DocUrl = data.secure_url;
        // DashboardStore._updateParameterV2(DocUrl, "resume", "profileupdatedata");
        ShowNotification("Done Uploading ....", true);
        // DashboardStore.__UpdateProfile();
        // this.SavetoDatabase(result)
        // setPhoto(data.secure_url)
        resolve(DocUrl);
        // return DocUrl
      })
      .catch((err) => {
        console.log(err);
        ShowNotification("An Error Occured While Uploading", true);
        Alert.alert("An Error Occured While Uploading");
        resolve(false);
      });
    // }
  });
// export const getusertypeold = () => {

//  return  loggedinUserdetails().then(
//     (resp) => {
// let isloggedin =resp.isloggedin;
// console.log(resp);
// if(isloggedin===true){
// let usertype = resp.usertype
// console.warn(usertype);
// console.warn('usertype lddkdkdddkdkdkdkdkdkdkdkdkdk');
// // global.usertype= usertype;
// return 'Usertypemi';

// }
// else{
//   return 'notin';
// }

// }
//   )
//   .catch((error) => {
//     console.warn(`get usertpe function ${error}`);
//   });

// }

export const currentTime = () => {
  const now = new Date();
  // console.warn(now)
  let hour = now.getHours();
  let minute = now.getMinutes();
  var timeString = hour + ":" + minute + ":00";
  return timeString;
};

export const ConvertToCurrency = (value) => {
  let newvalue = parseInt(value).toLocaleString();
  return newvalue;
};
export const GenerateInviteLink = (value = "Invite") => {
  let link = "https://workbrook.com/764-_getbs(k355%buUD567GIOHCNIE8897";
  return link;
};

export const ShowCurrentDate = () => {
  let date = new Date().getDate();
  let month = new Date().getMonth() + 1;
  let year = new Date().getFullYear();
  if (date < 10) {
    date = `0${date}`;
  }
  if (month < 10) {
    month = `0${month}`;
  }

  return (currentdate = year + "-" + month + "-" + date);
};
export const generate_id = () => {
  let unique_id = Math.floor(Date.now() / 1000);

  //   UUIDGenerator.getRandomUUID((uuid) => {
  //   console.warn(uuid);
  //   return uuid;
  // });
};

export const GetNameForImage = (text) => {
  var stringArray = text.split(" ");

  let ReturnedString = stringArray[0];
  return ReturnedString;
};

export const header_text_font_family = () => {
  return "Avenir";
};
export const ShareOptions = (url, title, message) => {
  const icon = "data:<data_type>/<file_extension>;base64,<base64_data>";
  const options = {
    title,
    subject: title,
    message: `${message} ${url}`,
  };

  return options;
};
export const ShareOptions_old = (url, title, message) => {
  const icon = "data:<data_type>/<file_extension>;base64,<base64_data>";
  const options = Platform.select({
    ios: {
      activityItemSources: [
        {
          // For sharing url with custom title.
          placeholderItem: { type: "url", content: url },
          item: {
            default: { type: "url", content: url },
          },
          subject: {
            default: title,
          },
          linkMetadata: { originalUrl: url, url, title },
        },
        {
          // For sharing text.
          placeholderItem: { type: "text", content: message },
          item: {
            default: { type: "text", content: message },
            message: null, // Specify no text to share via Messages app.
          },
          linkMetadata: {
            // For showing app icon on share preview.
            title: message,
          },
        },
        {
          // For using custom icon instead of default text icon at share preview when sharing with message.
          placeholderItem: {
            type: "url",
            content: icon,
          },
          item: {
            default: {
              type: "text",
              content: `${message} ${url}`,
            },
          },
          linkMetadata: {
            title: message,
            icon: icon,
          },
        },
      ],
    },
    default: {
      title,
      subject: title,
      message: `${message} ${url}`,
    },
  });

  return options;
};
export const generateMyUniqueRefrence = (invitationcode, job_id) => {
  let myRefrenceNumber = "";
  let path = `refers?referralCode=${invitationcode}&optionbar=${job_id}`;
  myRefrenceNumber = dynamicEventLink(path);
  // dynamicEventLink(path);
  return myRefrenceNumber;
};
export const generateMyUniqueJobId = (RecruiterId, job_id) => {
  let myRefrenceNumber = "";
  let path = `joboffer?uuid=${RecruiterId}&optionbar=${job_id}`;
  myRefrenceNumber = dynamicEventLink(path);
  // dynamicEventLink(path);
  return myRefrenceNumber;
};
export const generateMyUniqueInvitationId = (InvitationNumber) => {
  let myRefrenceNumber = "";
  let path = `Invite?mineuuid=${InvitationNumber}`;
  myRefrenceNumber = dynamicEventLink(path);
  // dynamicEventLink(path);
  return myRefrenceNumber;
};
export const text_font_family = () => {
  //  return 'CenturyGothic';
  return "Avenir";
};
export const PasswordCheck = (PassKey) => {
  var strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})");
  if (strongRegex.test(PassKey)) {
    return false;
  } else {
    return true;
  }
};

export const Capitalize_first_character = (text) => {
  let capitalize = text.slice(0, 1).toUpperCase() + text.slice(1, text.length);
  return capitalize;
};

export const _signIn_with_Google = async () => {
  try {
    await GoogleSignin.hasPlayServices();
    //       const isSignedIn = await GoogleSignin.isSignedIn();
    //       if(isSignedIn){
    // console.log("yes ")
    //       }
    //       else{
    //         console.log("nooo ")
    //       }
    const userInfo = await GoogleSignin.signIn();
    console.log(userInfo);
    let profile = userInfo["user"];
    let email = profile.email;
    let familyName = profile.familyName;
    let givenName = profile.givenName;
    let UserName = profile.id;
    let platform_id = profile.id;
    let platform = "Google";

    // value,Feild,parent
    // "username":"",
    // "email":"",
    // "platform_uid":"",
    // "platform":""

    AccountStore._updateParameter(
      platform_id,
      "platform_uid",
      "payload_regInfo"
    );
    AccountStore._updateParameter(platform, "platform", "payload_regInfo");
    AccountStore._updateParameter(email, "email", "payload_regInfo");
    AccountStore._updateParameter(platform_id, "username", "payload_regInfo");
    AccountStore._updateParameter(UserName, "username", "payload_regInfo");
    AccountStore._updateParameter(givenName, "first_name", "payload_regInfo");

    AccountStore._updateParameter(familyName, "last_name", "payload_regInfo");

    AccountStore._RegisterUserNow_SocialMedia();
    _signOut_GoogleAccount();
    // console.log(email)
    // console.log(email)
    // console.log("userInfo");

    // photo: "https://lh3.googleusercontent.com/a-/AOh14GgP-fgNmr7R9LDL-DwVqHhu-pTTZw4_vIlWfW4FPQ"
    // email: "iyamalaika@gmail.com"
    // familyName: "David"
    // givenName: "Jinad"
    // name: "Jinad David"
    // id: "101496660815957053665"

    // this.setState({ userInfo, error: null });
  } catch (error) {
    console.log(error);
    switch (error.code) {
      case statusCodes.SIGN_IN_CANCELLED:
        // sign in was cancelled
        // Alert.alert('cancelled');
        AccountStore._updateParameter(
          "Action cancelled",
          "message",
          "errorDisplay"
        );

        break;
      case statusCodes.IN_PROGRESS:
        // operation (eg. sign in) already in progress
        // Alert.alert('in progress');
        AccountStore._updateParameter(
          "Action in progress",
          "message",
          "errorDisplay"
        );

        break;
      case statusCodes.PLAY_SERVICES_NOT_AVAILABLE:
        // android only
        // Alert.alert('play services not available or outdated');
        AccountStore._updateParameter(
          "play services not available or outdated",
          "message",
          "errorDisplay"
        );

        break;
      default:
        // Alert.alert('Something went wrong', error.toString());
        AccountStore._updateParameter(
          "Something went wrong",
          "message",
          "errorDisplay"
        );

      // this.setState({
      //   error,
      // });
    }
  }
};

export const _signOut_GoogleAccount = async () => {
  try {
    await GoogleSignin.revokeAccess();
    await GoogleSignin.signOut();

    // this.setState({ userInfo: null, error: null });
  } catch (error) {
    AccountStore._updateParameter(error.toString(), "message", "errorDisplay");

    this.setState({
      error,
    });
  }
};
export const ParseDate2string = (mydate, split) => {
  var parts = mydate.split(split);
  console.log(parts);
  var mydate = new Date(parts[0], parts[1] - 1, parts[2]);
  let thisdate__ = mydate.toDateString();
  console.log(thisdate__);
  var String_parts = thisdate__.split(" ");
  console.log(String_parts);
  return String_parts;
};
export const GetPartDate = async (StringDate, split) => {
  console.log(StringDate);
  var String_parts = StringDate.split(split);
  console.log(String_parts);
  // var mydate = new Date(parts[0], parts[1] - 1, parts[2]);
  // let thisdate__=mydate.toDateString();
  // console.log(thisdate__)
  // return  thisdate__;
};

export const dynamicEventLink = async (pathParam) => {
  // signup?referralCode=foo&referralType=bar
  const identifier = "com.greenharbourgroup.wookbrookapp";
  // const firebaseDynamicLinkPrefix = 'https://workbrook.app/t';
  const firebaseDynamicLinkPrefix = "https://wookbrookapp.page.link";
  // const firebaseDynamicLinkPrefix = 'myapp.page.link';
  const deepLink = `https://wookbrookapp.page.link/${pathParam}`;

  //   const shareLink =
  //   new firebase.links.DynamicLink(
  //     deepLink,
  //     firebaseDynamicLinkPrefix
  //   ).android.setPackageName(identifier)
  //   .ios.setBundleId(identifier);
  // console.log(shareLink)
  // console.log("<>>linkkkkk<>"+shareLink)

  // const dymcLink = await firebase
  // .links()
  // .createDynamicLink(shareLink)
  // .then(url => {
  //     // ...
  //     console.log('LINK', url);
  //     return url;
  // })

  // const dymcLink = await firebase.links()
  //     .createShortDynamicLink(link, `UNGUESSABLE`)
  //     .then((url) => decodeURIComponent(url));

  // return dymcLink;
  return deepLink;
};
export const dynamicEventLink__ = async (id) => {
  const link = new firebase.links.DynamicLink(
    `https://workbrook.app/t/${id}?param1=foo&param2=bar`,
    "workbrook.app/t"
  ).android
    .setPackageName("com.greenharbourgroup.wookbrookapp")
    .ios.setBundleId("com.greenharbourgroup.wookbrookapp");

  const dymcLink = await firebase
    .links()
    .createDynamicLink(link)
    .then((url) => {
      console.log("from utility");
      console.log(url);
      console.log("from utility");
      // ...
    });
};

export const pickSingleWithCamera = (cropping, mediaType = "photo") =>
  new Promise(async (resolve, reject) => {
    console.log("reach here");
    await ImagePicker_crop.openCamera({
      cropping: cropping,
      width: 100,
      height: 100,
      // includeExif: true,
      mediaType,
    })
      .then((image) => {
        console.log("received image", image);
        // return image
        resolve(image);
        // this.setState({
        //   image: {
        //     uri: image.path,
        //     width: image.width,
        //     height: image.height,
        //     mime: image.mime,
        //   },
        //   images: null,
        // });
      })
      .catch((e) => alert(e));

    // }
  });
export const pickSingleGallery = (cropping, mediaType = "photo") =>
  new Promise(async (resolve, reject) => {
    console.log("reach here");
    await ImagePicker_crop.openPicker({
      width: 500,
      height: 500,
      cropping: cropping,
      cropperCircleOverlay: false,
      sortOrder: "none",
      compressImageMaxWidth: 1000,
      compressImageMaxHeight: 1000,
      compressImageQuality: 1,
      // compressVideoPreset: 'MediumQuality',
      // includeExif: false,
      // cropperStatusBarColor: 'white',
      // cropperToolbarColor: 'white',
      // cropperActiveWidgetColor: 'white',
      // cropperToolbarWidgetColor: '#3498DB',
      mediaType,
    })
      .then((image) => {
        console.log("received image", image);
        resolve(image);
        // this.setState({
        //   image: {
        //     uri: image.path,
        //     width: image.width,
        //     height: image.height,
        //     mime: image.mime,
        //   },
        //   images: null,
        // });
      })
      .catch((e) => {
        console.log(e);
        Alert.alert(e.message ? e.message : e);
      });
  });

export const validateEmail = async (email) => {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
};

export const StageIdName = (stageId) => {
  let stagename = "";
  if (stageId == 1) {
    stagename = "Consider";
  } else if (stageId == 2) {
    stagename = "Test";
  } else if (stageId == 3) {
    stagename = "Interview";
  } else if (stageId == 4) {
    stagename = "hire";
  } else if (stageId == 5) {
    stagename = "Rejected";
  }
  return stagename;
};

const styles = StyleSheet.create({
  lottie: {
    width: 100,
    height: 100,
  },

  activityIndicator: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    height: 80,
  },
  activityIndicatorcontainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 70,
    backgroundColor: "#000000",
  },
  Spinnercontainer: {
    // flex: 1,
    justifyContent: "center",
    textAlign: "center",
    paddingTop: 30,
    marginBottom: 30,
    backgroundColor: "#fff",
    padding: 8,
    // top: 0, bottom: 0, left: 0, right: 0
    bottom: 50,
  },
  displayTitle: {
    // flex:1,
    paddingTop: 2,
    justifyContent: "flex-start",
    alignItems: "flex-start",
    // backgroundColor: 'rgba(0,0,0,0.4)',
    // paddingLeft:10,
  },
});

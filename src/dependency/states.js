let data = [
    {
        id: 1,
        name: 'Abia',
        city:[
            {
                id: 1,
                name: 'Aba'
            },
            {
                id: 2,
                name: 'Arochukwu'
            },
            {
                id: 3,
                name: 'Umuahia'
            },
        ]
    },
    {
        id: 2,
        name: 'Adamawa',
        city:[
            {
                id: 1,
                name: 'Jimeta'
            },
            {
                id: 2,
                name: 'Mubi'
            },
            {
                id: 3,
                name: 'Numan'
            },
            {
                id: 4,
                name: 'Yola'
            },
        ]
    },
    {
   

        id: 3,
        name: 'Akwa-Ibom',
        city:[
            {
                id: 1,
                name: 'Ikot Abasi'
            },
            {
                id: 2,
                name: 'Oron'
            },
            {
                id: 3,
                name: 'Uyo'
            }
        ]
    },
    {
   id: 4,
        name: 'Anambra',
        city:[
            {
                id: 1,
                name: 'Awka'
            },
            {
                id: 2,
                name: 'Onitsha'
            }
           
        ]
    },
    {
    
        id: 5,
        name: 'Bauchi',
        city:[
            { 
                id: 1,
                name: 'Azare'
            },
            {
                id: 2,
                name: 'Bauchi'
            },
            {
                id: 3,
                name: 'Jama′are'
            },
            {
                id: 4,
                name: 'Katagum'
            },
            {
                id: 5,
                name: 'Misau'
            },
          
        ]
    },
    {
        id: 6,
        name: 'Bayelsa',
        city:[
            {
                id: 1,
                name: 'Brass'
            },
          
        ]
    },
    {
        id: 7,
        name: 'Benue',
        city:[
            {
                id: 1,
                name: 'Makurdi'
            },
          
        ]
    },
    {
        id: 8,
        name: 'Borno',
        city:[
            {
                id: 1,
                name: 'Biu'
            },
            {
                id: 2,
                name: 'Dikwa'
            },
            {
                id: 3,
                name: 'Maiduguri'
            },
           
        ]
    },
    {
        id: 9,
        name: 'Cross-river',
        city:[
            {
                id: 1,
                name: 'Calabar'
            },
            {
                id: 2,
                name: 'Ogoja'
            },
          
        ]
    },
    {
        id: 10,
        name: 'Delta',
        city:[
            {
                id: 1,
                name: 'Asaba'
            },
            {
                id: 2,
                name: 'Burutu'
            },
            {
                id: 3,
                name: 'Koko'
            },
            {
                id: 4,
                name: 'Sapele'
            },
            {
                id: 5,
                name: 'Ughelli'
            },
            {
                id: 6,
                name: 'Warri'
            },
        ]
    },
    {
        id: 11,
        name: 'Ebonyi',
        city:[
            {
                id: 1,
                name: 'Abakaliki'
            }
        ]
    },
    {
        id: 12,
        name: 'Edo',
        city:[
            {
                id: 1,
                name: 'Benin City'
            },
         
        ]
    },
    {
        id: 13,
        name: 'Ekiti',
        city:[
            {
                id: 1,
                name: 'Ado-Ekiti'
            },
            {
                id: 2,
                name: 'Effon-Alaiye'
            },
            {
                id: 3,
                name: 'Ikere-Ekiti'
            }
        ]
    },
    {
        id: 14,
        name: 'Enugu',
        city:[
            {
                id: 1,
                name: 'Enugu'
            },
            {
                id: 2,
                name: 'Nsukka'
            }
        ]
    },
    
    {
        id: 15,
        name: 'Gombe',
        city:[
            {
                id: 1,
                name: 'Deba Habe'
            },
            {
                id: 2,
                name: 'Gombe'
            },
            {
                id: 3,
                name: 'Kumo'
            }
        ]
    },
    {
        id: 16,
        name: 'Imo',
        city:[
            {
                id: 1,
                name: 'Owerri'
            },
            
        ]
    },
    {
        id: 17,
        name: 'Jigawa',
        city:[
            {
                id: 1,
                name: 'Birnin Kudu'
            },
            {
                id: 2,
                name: 'Dutse'
            },
            {
                id: 3,
                name: 'Gumel'
            },
            {
                id: 4,
                name: 'Hadeji'
            },
            {
                id: 5,
                name: 'Kazaure'
            },
        ]
    },
    {
        id: 18,
        name: 'Kaduna',
        city:[
            {
                id: 1,
                name: 'Jemaa'
            },
            {
                id: 2,
                name: 'Kaduna'
            },
            {
                id: 3,
                name: 'Zaria'
            },
          
        ]
    },
    {
        id: 19,
        name: 'Kano',
        city:[
            {
                id: 1,
                name: 'Kano'
            },
            
        ]
    },
    {
        id: 20,
        name: 'Katsina',
        city:[
            {
                id: 1,
                name: 'Daura'
            },
            {
                id: 2,
                name: 'Katsina'
            }
        ]
    },
    {
        id: 21,
        name: 'Kebbi',
        city:[
            {
                id: 1,
                name: 'Argungu'
            },
            {
                id: 2,
                name: 'Birnin Kebbi'
            },
            {
                id: 3,
                name: 'Gwandu'
            },
            {
                id: 4,
                name: 'Yelwa'
            },
        ]
    },
    {
        id: 22,
        name: 'Kogi',
        city:[
            {
                id: 1,
                name: 'Idah'
            },
            {
                id: 2,
                name: 'Kabba'
            },
            {
                id: 3,
                name: 'Lokoja'
            },
            {
                id: 4,
                name: 'Okene'
            },
        ]
    },
    {
        id: 23,
        name: 'Kwara',
        city:[
            {
                id: 1,
                name: 'Ilorin'
            },
            {
                id: 2,
                name: 'Jabba'
            },
            {
                id: 3,
                name: 'Lafiagi'
            },
            {
                id: 4,
                name: 'Offa'
            },
            {
                id: 5,
                name: 'Pategi'
            },
        ]
    },
    {
        id: 24,
        name: 'Lagos',
        city:[
            {
                id: 1,
                name: 'Badagry'
            },
            {
                id: 2,
                name: 'Epe'
            },
            {
                id: 3,
                name: 'Ikeja'
            },
            {
                id: 4,
                name: 'Ikorodu'
            },
            {
                id: 5,
                name: 'Lagos'
            },
            {
                id: 6,
                name: 'Mushin'
            },
            {
                id: 7,
                name: 'Shomolu'
            },
        ]
    },
    {
        id: 25,
        name: 'Nassarawa',
        city:[
            {
                id: 1,
                name: 'Keffi'
            },
            {
                id: 2,
                name: 'Lafia'
            },
            {
                id: 3,
                name: 'Nasarawa'
            },
            
        ]
    },
    {
        id: 26,
        name: 'Niger',
        city:[
            {
                id: 1,
                name: 'Agaie'
            },
            {
                id: 2,
                name: 'Baro'
            },
            {
                id: 3,
                name: 'Bida'
            },
            {
                id: 4,
                name: 'Kontagora'
            },
            {
                id: 5,
                name: 'Lapai'
            },
            {
                id: 6,
                name: 'Suleja'
            },
           
        ]
    },
    {
        id: 27,
        name: 'Ogun',
        city:[
            {
                id: 1,
                name: 'Abeokuta'
            },
            {
                id: 2,
                name: 'Ijebu-Ode'
            },
            {
                id: 3,
                name: 'Ilaro'
            },
            {
                id: 4,
                name: 'Shagamu'
            },
        ]
    },
    {
        id: 28,
        name: 'Ondo',
        city:[
            {
                id: 1,
                name: 'Akure'
            },
            {
                id: 2,
                name: 'Ikare'
            },
            {
                id: 3,
                name: 'Oka-Akoko'
            },
            {
                id: 4,
                name: 'Ondo'
            },
            {
                id: 5,
                name: 'Owo'
            },
        ]
    },
    {
        id: 29,
        name: 'Osun',
        city:[
            {
                id: 1,
                name: 'Ede'
            },
            {
                id: 2,
                name: 'Ikire'
            },
            {
                id: 3,
                name: 'Ikirun'
            },
            {
                id: 4,
                name: 'Ila'
            }, {
                id: 5,
                name: 'Ile-Ife'
            },
            {
                id: 6,
                name: 'Ilesha'
            },
            {
                id: 7,
                name: 'Ilobu'
            },
            {
                id: 8,
                name: 'Inisa'
            },
            {
                id: 9,
                name: 'Iwo'
            },
            {
                id: 10,
                name: 'Oshogbo'
            },
        ]
    },
    {
        id: 30,
        name: 'Oyo',
        city:[
            {
                id: 1,
                name: 'Ibadan'
            },
            {
                id: 2,
                name: 'Iseyin'
            },
            {
                id: 3,
                name: 'Ogbomosho'
            },
            {
                id: 4,
                name: 'Oyo'
            },
            {
                id: 5,
                name: 'Saki'
            },
        ]
    },
    {
        id: 31,
        name: 'Plateau',
        city:[
            {
                id: 1,
                name: 'Bukuru'
            },
            {
                id: 2,
                name: 'Jos'
            },
            {
                id: 3,
                name: 'Vom'
            },
            {
                id: 4,
                name: 'Wase'
            },
        ]
    },
    {
        id: 32,
        name: 'Rivers',
        city:[
            {
                id: 1,
                name: 'Bonny'
            },
            {
                id: 2,
                name: 'Degema'
            },
            {
                id: 3,
                name: 'Okrika'
            },
            {
                id: 4,
                name: 'Port Harcourt'
            },
        ]
    },
    {
        id:33,
        name: 'Sokoto',
        city:[
            {
                id: 1,
                name: 'Sokoto'
            },
            
        ]
    },
    {
        id: 34,
        name: 'Taraba',
        city:[
            {
                id: 1,
                name: 'Ibi'
            },
            {
                id: 2,
                name: 'Jalingo'
            },
            {
                id: 3,
                name: 'Muri'
            },
            
        ]
    },
    {
        id: 35,
        name: 'Yobe',
        city:[
            {
                id: 1,
                name: 'Damaturu'
            },
            {
                id: 2,
                name: 'Nguru'
            },
          
        ]
    },
    {
        id: 36,
        name: 'Zamfara',
        city:[
            {
                id: 1,
                name: 'Gusua'
            },
            {
                id: 2,
                name: 'Kaura Namoda'
            },
            
        ]
    },
    {
        id: 37,
        name: 'FCT-Abuja',
        city:[
            {
                id: 1,
                name: 'Abuja'
            },
             
        ]
    },
    
];

module.exports = data;
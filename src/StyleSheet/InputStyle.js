import { StyleSheet,PixelRatio } from "react-native";
const height = 40;

// import {text_font_family,header_text_font_family} from '../dependency/UtilityFunctions'
export default StyleSheet.create({
  container: {
    marginVertical: 5,
    borderRadius: 4,
    borderStyle: 'solid',
    borderWidth: 0.9,
    borderColor:'grey',

    // borderColor:'#357d83',
    // borderColor: '#727272',
    // borderColor:'#4ab1be',
    backgroundColor: 'transparent',
    flex: 0,
    // height: 'auto',
    overflow: 'hidden'
  },
  input: {
    height: height,
    backgroundColor: '#fff',
    fontWeight: '600',
    fontSize: 11,
  },
  size: {
    input: height,
    icon: 20
  }
}); 


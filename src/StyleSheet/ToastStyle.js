  import { StyleSheet,Platform } from "react-native";


export default styletoest={
  backgroundColor: "#877c00",
  width: 300,
  height: Platform.OS === ("ios") ? 50 : 100,
  color: "#ffffff",
  fontSize: 12,
  lineHeight: 2,
  lines: 4,
  borderRadius: 15,
   justifyContent: 'center',
      alignItems: 'center',
  yOffset: 40
};

import { StyleSheet } from "react-native";

export default StyleSheet.create({

  container: {
    backgroundColor: '#F5F5F5',
    flex: 1,
  },
  containerMenuList: {
    backgroundColor: '#4b4b4b',
  },
  bottomView: {
    flexDirection: 'row',
    width: '100%',
    height: 70,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: 0
  },
  comment: {
    flex: 1,
    paddingTop: 25,
  },

  profileName: {
    alignItems: 'flex-start',
    flex: 1,

  },

  // ?==========================tomzy Sty SideBar========================
  SidebarimageWrap: {
    backgroundColor: 'transparent',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  SidebarView: {
    backgroundColor: 'transparent',
    padding: 20,
    paddingBottom: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#E5E5E5'
  },
  NameStyInd1: {
    color: '#2F2F2F',
    fontSize: 18,
    fontWeight: '600'
  },
  NameStyInd2: {
    color: '#5ABC7A',
    fontSize: 12,
    fontWeight: '600'
  },
  NameStyInd3: {
    color: '#2F2F2F',
    fontSize: 14,
    fontWeight: '600'
  },
  NameStyInd4: {
    color: '#1E93E2',
    fontSize: 14,
    fontWeight: '800'
  },
  ViewPfSty: {
    backgroundColor: 'transparent',
    textAlign: 'left',
    paddingLeft: 0,
    height: 25,
    elevation: 0,
  },
  CupViewSty: {
    display: 'flex',
    alignItems: 'center'
  },
  cupSize: {
    width: 44,
    height: 40
  },
  cupText1Sz1: {
    fontSize: 14,
    fontWeight: '600',
    color: '#1E93E2'
  },
  cupText1Sz2: {
    fontSize: 11,
    fontWeight: '300',
    color: '#2F2F2F'
  },
  listImagesty1: {
    height: 31,
    width: 31,
    borderRadius: 25 / 2

  },
  listImagesty2: {
    maxHeight: 31,
    maxWidth: 31,
  },
  recruiter: {
    maxWidth: 21.66,
    maxHeight: 16.99,
  },
  numSty: {
    height: 31,
    width: 31,
  },
  circlesty: {
    height: 11,
    width: 11,
  },
  ListWrap: {
    width: '100%',
    justifyContent: 'center',
    paddingRight: "12%",
    paddingTop: '5%',
    marginTop: '3%',
  },
  ListWrap2: {
    width: '100%',
    justifyContent: 'center',
    paddingRight: "0%",
    paddingLeft: 0,
    paddingTop: '5%',
    marginTop: '3%',
    marginLeft: 0,
    // backgroundColor:"red"
  },
  ListItemWrap: {
    width: '100%',
    borderBottomWidth: 1,
    borderBottomColor: '#E5E5E5',
    paddingBottom: 5,
    paddingTop: 5,
    // backgroundColor:"yellow"
  },
  ListItemWrapCircle: {
    width: '100%',
    paddingBottom: 5,
    paddingTop: 5,
    // backgroundColor:"yellow"
  },
  ListLeftSty: {
    alignItems: 'center',
    paddingTop: 5,
    paddingBottom: 5,
    // backgroundColor:"yellow"
  },
  ListBodySty: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    paddingRight: 0,
    borderBottomWidth: 0
  },
  ListRightSty: {
    paddingRight: 0,
    borderBottomWidth: 0
  },
  ListBtnSty: {
    height: 33,
    width: 231,
    backgroundColor: '#5ABC7A',
    marginTop: 20,
    elevation: 0,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  // ?==========================tomzy Sty SideBar========================

  imageText: {
    color: '#fff',
    // fontWeight: "bold",
    marginTop: 8,
    marginBottom: 8,
    fontSize: 19,

  },
  imageText2: {
    color: '#ccc',
    fontSize: 13
  },
  img: {
    height: 100,
    borderRadius: 50,
    width: 100,
    marginLeft: -12,
    borderColor: '#fff',
    borderWidth: 1

  },
  imageWrap: {
    width: 80,
    // justifyContent: 'center',
    alignItems: 'center'
  },
  CommentContentWrap: {
    marginTop: -30,
    flex: 1,
    // marginLeft:12
    // backgroundColor:'green'
  },
  singlecommentWrap: {
    padding: 20,
    flexDirection: 'row',
    paddingTop: 50,
    // borderBottomColor:'#ececf1',
    // borderBottomWidth:4,
  },
  commenthead: {
    paddingLeft: 20,
    borderBottomColor: '#ececf1',
    borderBottomWidth: 4,
    paddingTop: 15,
    paddingBottom: 15,
  },
  iconsize_cat: {
    fontSize: 39,
    color: '#9d9d9d',

  },
  menuiconsize: {
    fontSize: 25,
    color: '#9d9d9d',

  },
  iconsize_comment: {
    fontSize: 15,
    color: '#9d9d9d'
  },
  iconsize_priority: {
    fontSize: 15,
    color: '#d22947'
  },
  iconwrapper: {
    paddingTop: 8,
    paddingLeft: 20,
  },
  badgewrap: {
    // flex:1,
    width: 40,
    height: 40,
    justifyContent: 'flex-end',
    backgroundColor: '#fb4a5e',
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center'

  },
  downbotton: {
    height: 50,
    marginRight: 'auto',
    marginLeft: 'auto',
    width: '90%',
  },
  datepickerwrap: {
    flex: 1,
    flexDirection: 'row'
  },
  secondsection: {
    flex: 1,
    // marginBottom:51,
    // justifyContent: 'center',
    // alignItems: 'center',
    // backgroundColor: '#374046'
  },
  text: {
    color: '#726e7c',
  },
  itemwrap: {
    paddingBottom: 5,
    paddingTop: 5,
    // backgroundColor:'#f7f7f7',
  },
  container: {
    flex: 1,
    justifyContent: "center",
    // alignItems: 'center',
    width: null,
    height: null,
  },
  imgdown: {
    // flex: 1,
    aspectRatio: 0.9,
    width: '50%',
    height: 170,
    resizeMode: 'contain',

    // marginTop: -40,
  },
  menutext: {
    color: '#fff',
    fontSize: 15,
    // fontWeight: 'bold',
  },
  imgwrap: {
    // flex:0.3,
    width: '100%',
    // justifyContent: 'center',
    // marginTop:150,
    alignItems: 'center',
    // paddingTop: 150,
    // backgroundColor:'green',
  },
});

// https://pusher.com/tutorials/geolocation-sharing-react-native/
import { StyleSheet,PixelRatio } from "react-native";
import {text_font_family,header_text_font_family} from '../dependency/UtilityFunctions'
export default StyleSheet.create({
  box: {
    width: 200,
    height: 200,
    backgroundColor: 'purple',
  },
  inner: {
    position: 'relative',
    top: -50,
    width: 100,
    height: 100,
    backgroundColor: 'black',
  },
  innerpage: {
    paddingTop: '2%',
    marginLeft: 'auto',
    marginRight: 'auto',
    width: '93%',
    flex: 1,
  },
  list_item_wrapper: {
    borderStyle: 'solid',
    borderColor: '#ccc',
    borderWidth: 2,
    borderRadius: 25,
    margin: 12,
    paddingTop: 12,
    paddingBottom: 12,
    height: 150,
    flex: 1,
  },
  list_item_wrapper_case: {
    borderStyle: 'solid',
    borderColor: '#ccc',
    borderWidth: 2,
    borderRadius: 25,
    margin: 12,
    paddingTop: 12,
    paddingBottom: 12,
    // height: 150,
    flex: 1,
  },
  time_ago: {
    color: '#000',
    fontSize: 10,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  notice_title_wrapper: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  notice_title_: {
    color: '#000',
    fontSize: 20,
    textAlign: 'center',
    paddingBottom: 12,
    fontWeight: 'bold',
  },
  notice_short_content_wrapper: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingRight: 5,
    //  color: '#000'
  },
  notice_short_content: {
    //color: '#000',
    fontSize: 15,
   // textAlign: 'justify',
    paddingBottom: 12,
  },
  paticipant_imgwrap: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    height: 50,
    flex: 1,
    marginBottom:20,
    // borderRadius:50,
  },
  participant_Dp: {
    height: 30,
    borderRadius: 15,
    width: 30,
  },
  singleparticipant: {
    marginLeft: 12,
    marginBottom:25,
  },
  indicator: {
    width: 8,
    borderRadius: 4,
    height: 8,
    position: 'relative',
    top: 18,
    // left:2,
  },
  rightIndicator: {
    width: 15,
    borderRadius: 15,
    height: 15,
  },
  read_indicator: {
    backgroundColor: '#d8d8d8',
  },
  unread_indicator: {
    backgroundColor: 'green',
  },
  offline_user: {
    backgroundColor: '#8a8a8a',
  },
  online_user: {
    backgroundColor: 'green',
  },
  iconsizeNcolour: {
    fontSize: 30,
    color: '#000',
  },
  footer_icon_style: {
    fontSize: 20,
    color: '#000',
  },
  footerWrap: {
    borderStyle: 'solid',
    borderTopWidth: 1,
    borderTopColor: '#ccc',
    backgroundColor: '#FFF',
    height: 50,
  },
  footertab: {
    backgroundColor: '#FFF',
  },
 
  container: {
    flex: 1,
    alignItems: 'center',
  },


}); 


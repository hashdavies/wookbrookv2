
import { StyleSheet,PixelRatio } from "react-native";
import {text_font_family,header_text_font_family} from '../dependency/UtilityFunctions'
export default StyleSheet.create({
  container: {
    flex: 1,
  },

  singlecommentWrap: {
    padding: 10,
    flexDirection: 'row',
    backgroundColor: 'rgba(0,0,0,0.1)',
    marginLeft: 'auto',
    marginRight: 'auto',
    width: '90%',
    marginTop: 20,
  },

  img: {
    height: 90,
    width: 90,
  },
  imageWrap: {
    width: 100,
    justifyContent: 'center',
  },

  comment: {
    // flex:1,
    // alignItems:'fle'
    marginTop: 3,
    flexDirection: 'row',
    //  backgroundColor:'orange'
    //  alignItems: 'flex-start',
  },

  newgridContentWrap: {
    flex: 1,
    justifyContent: 'center',
  },
  text: {
    color: '#726e7c',
  },
  news_grid_view_title_wrapper: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 1,
  },
  news_grid_view_title_: {
    color: '#000',
    fontSize: 20,
    textAlign: 'center',
    paddingBottom: 12,
    fontWeight: 'bold',
  },
  news_grid_view_short_content_wrapper: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  news_grid_view_short_content: {
    color: '#000',
    fontSize: 15,
    textAlign: 'justify',
    paddingBottom: 12,
  },

  viewChanger_icon: {
    fontSize: 30,
    color: '#000',
  },
  viewChanger_iconwrap: {
    paddingRight: 12,
  },
}); 


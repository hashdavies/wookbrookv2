import { StyleSheet,PixelRatio } from "react-native";
import {text_font_family,header_text_font_family} from '../dependency/UtilityFunctions'
export default StyleSheet.create({
    tabsContainerStyle: {
      //custom styles
      marginTop:12,
    },
    tabStyle: {
      //custom styles
      backgroundColor: '#DC282E',
      },
    tabTextStyle: {
      //custom styles
    },
    activeTabStyle: {
      //custom styles
      },
    activeTabTextStyle: {
      //custom styles
    },
    tabBadgeContainerStyle: {
      //custom styles
    },
    activeTabBadgeContainerStyle: {
      //custom styles
    },
    tabBadgeStyle: {
      //custom styles
    },
    activeTabBadgeStyle: {
      //custom styles
    }  
  
}); 


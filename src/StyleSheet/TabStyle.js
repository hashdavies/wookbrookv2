import { StyleSheet,PixelRatio } from "react-native";
import {text_font_family,header_text_font_family} from '../dependency/UtilityFunctions'
export default StyleSheet.create({
  container: {
    flex: 1,
  },
  tabbar: {
    // backgroundColor: '#3f51b5',
    // backgroundColor: '#3e516f',
    backgroundColor: '#DC282E',
  },
  tab: {
    width: 120,
  },
  indicator: {
    backgroundColor: '#ffeb3b',
    // backgroundColor: '#000',
  },
  label: {
    color: '#fff',
    // color: '#000',
    fontWeight: '400',
}, 
  
}); 


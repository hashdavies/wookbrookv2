import { StyleSheet } from "react-native";
import {text_font_family,header_text_font_family} from '../dependency/UtilityFunctions'
export default StyleSheet.create({
  container: {
    // backgroundColor: '#e5eff1',
    backgroundColor: '#fff',
    flex: 1,
  },
  singlecommentWrap: {
    padding: 10,
    marginTop:-100,
    // justifyContent:'center',
    alignItems:'center',
    flex:1,
    },

  bottomView:{
flexDirection:'row',
    width: '70%', 
    height: 70, 
    backgroundColor: '#e5eff1', 
    justifyContent: 'center', 
    alignItems: 'center',
    position: 'absolute',
    bottom: 0
  },
  img: {
    height: 290,
    // borderRadius: 90,
    width:290,
  },
  imageWrap: {
    width:300,
    justifyContent: 'center',
  },
  activityIndicatorWrapper: {
    backgroundColor: '#FFFFFF',
    height: 100,
    width: 100,
    borderRadius: 10,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around'
},
edit_iconsizeNcolour: {
  fontSize:30,
  color:'#F75A4A',

  },
counter_timer: {
  fontSize:30,
  color:'#F75A4A',
  fontWeight:'bold'

  },
   
}); 


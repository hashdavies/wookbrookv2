
import { StyleSheet,PixelRatio } from "react-native";
import {text_font_family,header_text_font_family} from '../dependency/UtilityFunctions'
export default StyleSheet.create({
  gridView: {
    paddingTop: 15,
    flex: 1,
  },
  itemContainer: {
    justifyContent: 'flex-end',
    borderRadius: 5,
    padding: 10,
    height: 150,
  },
  itemName: {
    fontSize: 16,
    color: '#fff',
    fontWeight: '600',
  },
  itemCode: {
    fontWeight: '600',
    fontSize: 12,
    color: '#fff',
  },
  overlay_content: {
    // width:8,
    // borderRadius:8,
    // height:8,
    backgroundColor: 'rgba(0,0,0,0.8)',
    position: 'relative',
    flex: 1,
    padding: 6,
    // top: 18,
    // left:2,
  },
  backgroundImage: {
    height: 150,
    // flex: 1,
    width: null,
  },
  news_listview_title_wrapper: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 12,
  },
  news_listview_title_: {
    color: '#fff',
    fontSize: 20,
    textAlign: 'center',
    paddingBottom: 12,
    fontWeight: 'bold',
  },
  news_listview_short_content_wrapper: {
    justifyContent: 'center',
    alignItems: 'center',
    // paddingRight:5,
    //  color: '#000'
  },
  news_listview_short_content: {
    color: '#fff',
    fontSize: 15,
    textAlign: 'justify',
    paddingBottom: 12,
  },
  viewChanger_icon: {
    fontSize: 30,
    // color: '#000',
  },
  viewChanger_active: {
    color: '#000',
  },
  viewChanger_inactive: {
    color: '#ccc',
  },
  viewChanger_iconwrap: {
    paddingRight: 12,
  },

// list view style



}); 


// app/router.js

import React, { Component } from 'react';
import {
  Platform,
  Image,
  View,
  Text,
  AsyncStorage,
  BackHandler,
  StyleSheet,
} from 'react-native';
import {
  createStackNavigator, createTabNavigator, createSwitchNavigator, createDrawerNavigator, createMaterialTopTabNavigator
} from "react-navigation";
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { DrawerActions } from 'react-navigation';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import Icon from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import HomeIcon from './assets/workbrookAssets/home.png';
import Act_Home from './assets/workbrookAssets/act_home.png';
import Search from './assets/workbrookAssets/search.png';
import OfferIcon from './assets/workbrookAssets/offers.png';
import Act_OfferIcon from './assets/workbrookAssets/act_offer.png';
import Notification from './assets/workbrookAssets/notification.png';
import Act_Notification from './assets/workbrookAssets/act_notification.png';
import Settings from './assets/workbrookAssets/settings.png';
import Act_Settings from './assets/workbrookAssets/act_settings.png';

// Screens
// import AddButton from './screens/landingpage/AddButton' ;

// outside screen

// import LanguageSelect from './screens/' ;
// import LoginPage from './screens/landingpage/LoginPage' ;
// import SignUp from './screens/landingpage/SignUp' ;
// import OtpPage from './screens/landingpage/OtpPage' ;
// import RegistrationPage from './screens/landingpage/RegistrationPage' ;


// workbrook route  out Side ======================================================

// import Landingpage from './screens/login/wbbLogin';
import WBBLandingpage from './screens/landingpage/WBBLandingpage';
import Landingpage from './screens/landingpage/Landingpage';
import Login from './screens/login/Login';
import Signup from './screens/createAccount/Signup';
import ForgetPassword from './screens/forgetPassword/ForgetPassword';
import ResetPassword from './screens/forgetPassword/ResetPassword';
import AccountVerificaation from './screens/createAccount/AccountVerificaation';


// workbrook route  Inside 
import ComingSoon from './screens/general/ComingSoon'
import Offer from './screens/offer/offer';
import HomeView from './screens/home/Home';
import Newpost from './screens/home/NewPost';
import Likes from './screens/home/Likes';
import PreviewPost from './screens/home/PreviewPost';
import NotificationView from './screens/notification/Notification';
import SettingsView from './screens/settings/Settings';
import NotificationSettings from './screens/settings/Notification';
import AccountSettings from './screens/settings/AccountSettings';
import HelpSupport from './screens/settings/HelpSupport';
import AboutWorkbrook from './screens/settings/AboutWorkbrook';
import SecuritySettigs from './screens/settings/SecuritySettigs';
import SearchView from './screens/search/Search';

import Interests from './screens/interests/Interests';
import EditAbout from './screens/profile/EditAbout';
import AttachDocuments from './screens/profile/AttachDocuments';
import EditSkills from './screens/profile/EditSkills';
import EditEducation from './screens/profile/EditEducation';
import AddEducation from './screens/profile/AddEducation';
import EditExpirence from './screens/profile/EditExpirence';
import AddExpirence from './screens/profile/AddExpirence';
import EditIntro from './screens/profile/EditIntro';
import EditProfile from './screens/profile/EditProfile';
import profile from './screens/profile/profile';
import otherProfile from './screens/profile/otherProfile';
import Insights from './screens/insights/Insights';
import Recruiter from './screens/recruiter/Recruiter';
import RecruiterMaps from './screens/recruiter/RecruiterMaps';
import RecruiterForm from './screens/recruiter/RecruiterForm';
import ExistingRecruiter from './screens/recruiter/ExistingRecruiter';
import RecruiterBoard from './screens/recruiter/RecruiterBoard';
import RecruiterSearch from './screens/recruiter/RecruiterSearch';
import RecruiterAvailable from './screens/recruiter/RecruiterAvailable';
import RecruiterProfile from './screens/recruiter/RecruiterProfile';
import RecruiterLanding from './screens/recruiter/RecruiterLanding';
import RecruiterView from './screens/recruiter/RecruiterView';
import RecruiterInsights from './screens/insights/RecruiterInsights';
import ShortListedApplicants from './screens/recruiter/ShortListedApplicants';
import AllApplication from './screens/insights/AllApplication';
import ViewApplicant from './screens/insights/ViewApplicant';
import ViewApp from './screens/insights/ViewApp';
import InsightRecruiterAvailable from './screens/insights/RecruiterAvailable';
import League from './screens/league/League';
import WBPoints from './screens/league/WBPoints';
import LeagueParticipants from './screens/league/LeagueParticipants';
import MyWallet from './screens/wallet/MyWallet';
import AddFund from './screens/wallet/AddFund';
import AddCard from './screens/wallet/AddCard';
import PayWith from './screens/wallet/PayWith';
import WalletWithdraw from './screens/wallet/WalletWithdraw';
import AddWithdrawerAccount from './screens/wallet/AddWithdrawerAccount';
// import Invite from './screens/invite/Invite_old';
import Invite from './screens/invite/Invite';
import InviteLink from './screens/invite/InviteLink';
import UniqueInviteLink from './screens/invite/UniqueInviteLink';
import MyReferrals from './screens/invite/MyReferrals';
import ReferralsProfile from './screens/invite/ReferralsProfile';
import RecruiterDashboard from './screens/recruiter/RecruiterDashboard';
import UpdateRecruiterProfile from './screens/recruiter/UpdateRecruiterProfile';
import Payment from './screens/payment/Payment';
import PaymentMethod from './screens/payment/PaymentMethod';
import Saved from './screens/saved/Saved';
import Shared from './screens/shared/Shared';
import SharedJobs from './screens/shared/SharedJobs';
import ActiveTracker from './screens/recruiter/ActiveTracker';
import PerformanceTracker from './screens/recruiter/PerformanceTracker';
import Subscription from './screens/subscription/Subscription';
// import SubscribeWithCard from './screens/subscription/SubscribeWithCard';
import SubscribeWithCard from './screens/subscription/SubscribeWithCard';
// import Onboard from './screens/onboarding/onboard';
import ActiveSub from './screens/subscription/ActiveSub';
import ActiveSubRecruiter from './screens/subscription/ActiveSubRecruiter';
import { SvgUri } from 'react-native-svg';

// import Landingpage from  './screens/profile/EditExpirence'; 
// import Landingpage from  './screens/profile/EditAbout'; 
// import Landingpage from  './screens/profile/EditIntro'; 
// import Landingpage from  './screens/profile/EditProfile'; 
// import Landingpage from  './screens/profile/profile'; 
// import Landingpage from  './screens/offer/offer'; 


///////////////////////////////////////////////////////////////

// import LanguageSelect from  './screens/offer/offer'; 
// workbrook route======================================================
// New
// import LanguageSelect from  './screens/profile/EditExpirence'; 
// import EditAbout from  './screens/profile/EditAbout'; 
// import LanguageSelect from  './screens/profile/AttachDocuments'; 
// import EditSkills from  './screens/profile/EditSkills'; 
// import EditEducation from  './screens/profile/EditEducation'; 
// import EditExpirence from  './screens/profile/EditExpirence'; 
// import LanguageSelect from  './screens/profile/EditAbout'; 
// import EditIntro from  './screens/profile/EditIntro'; 
// import EditProfile from  './screens/profile/EditProfile'; 
// import profile from  './screens/profile/profile'; 
// New
// import LanguageSelect from  './screens/interests/Interests'; 
// import LanguageSelect from  './screens/landingpage/Landingpage'; 
// import LanguageSelect from  './screens/login/Login'; 
// import LanguageSelect from  './screens/createAccount/Signup'; 
// import LanguageSelect from  './screens/forgetPassword/ForgetPassword'; 










// Side menu
import Drawer from './SideMenu/Drawer';


// inside screens



// // import Myprofile from './screens/profile/Myprofile';



const noTransitionConfig = () => ({
  transitionSpec: {
    duration: 0,
    timing: Animated.timing,
    easing: Easing.step0
  }

})
const TransitionConfig = () => ({
  transitionSpec: {
    duration: 800,
    timing: Animated.timing,
    easing: Easing.out(Easing.poly(4)),
  },
  screenInterpolator: sceneProps => {
    const { layout, position, scene } = sceneProps;
    const { index } = scene;
    const width = layout.initWidth;
    const translateX = position.interpolate({
      inputRange: [index - 1, index, index + 1],
      outputRange: [width, 0, 0],
    });
    if (index <= 1) {
      return {};
    }

    return { transform: [{ translateX }] };
  }


})



export const SignedOut = createStackNavigator({

  Landingpage: {
    screen: Landingpage,
    navigationOptions: {
      title: "Welcome"
    }
  },

  Login: {
    screen: Login,
    navigationOptions: {
      title: "Choose Language"
    }
  },
  Signup: {
    screen: Signup,
    navigationOptions: {
      title: "Choose Language"
    }
  },
  ForgetPassword: {
    screen: ForgetPassword,
    navigationOptions: {
      title: "Welcome"
    },

  },
  AccountVerificaation: {
    screen: AccountVerificaation,
    navigationOptions: {
      title: "AccountVerificaation"
    },

  },
  ResetPassword: {
    screen: ResetPassword,
    navigationOptions: {
      title: "ResetPassword"
    },

  },


},
  {
    // innitial configuration for screens
    initialRouteName: 'Landingpage',
    headerMode: 'none',
    //   navigationOptions: {
    //     headerStyle: {backgroundColor: '#E73536'},
    //     // title: 'You are not logged in',
    //     headerTintColor: 'white'
    // }
  }
);



const HomePage = createMaterialBottomTabNavigator({
  Home: {
    screen: HomeView,
    navigationOptions: {
      tabBarIcon: ({ tintColor, focused }) => (
        focused ?
          <SvgUri
            width="31px"
            height="24.04px"
            uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1594200796/Mobile_Assets/homeActive_sxttdq.svg"
          />


          :
          <SvgUri
            width="31px"
            height="24.04px"
            uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1594200796/Mobile_Assets/homeNA_if7if6.svg"
          />
      )
    }
  },
  MyOffer: {
    screen: Offer,
    // screen: profile,

    // screen: CategoryList,
    navigationOptions: {
      tabBarLabel: "My Offer",
      tabBarIcon: ({ tintColor, focused }) => (
        focused ?
          <SvgUri
            width="31px"
            height="24.04px"
            uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1594200796/Mobile_Assets/offerActive_fc2med.svg"
          />
          :
          <SvgUri
            width="31px"
            height="24.04px"
            uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1594200796/Mobile_Assets/offerNA_ydtsm8.svg"
          />
      )
    }
  },
  Search: {
    screen: SearchView,
    navigationOptions: {
      tabBarIcon: ({ tintColor, focused }) => (
        focused ?
          <SvgUri
            width="31px"
            height="24.04px"
            uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1594200799/Mobile_Assets/searchNA_vjcrwf.svg"
          />
          :
          <SvgUri
            width="31px"
            height="24.04px"
            uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1594200799/Mobile_Assets/searchNA_vjcrwf.svg"
          />
      )
    }
  },

  Notifications: {
    screen: NotificationView,
    // screen: EditProfile,
    navigationOptions: {
      tabBarIcon: ({ tintColor, focused }) => (
        focused ?
          <SvgUri
            width="31px"
            height="24.04px"
            uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1594200796/Mobile_Assets/notiActive_gi8wel.svg"
          />
          :
          <SvgUri
            width="31px"
            height="24.04px"
            uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1594200796/Mobile_Assets/notiNA_qxtivr.svg"
          />
      )
    }
  },
  Settings: {
    screen: SettingsView,
    // screen: UpdateRecruiterProfile,
     navigationOptions: {
      tabBarIcon: ({ tintColor, focused }) => (
        focused ?

          <SvgUri
            width="31px"
            height="24.04px"
            uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1594200799/Mobile_Assets/settingsActive_sm1na0.svg"
          />
          :
          <SvgUri
            width="31px"
            height="24.04px"
            uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1594200799/Mobile_Assets/SettignsNA_iudn6b.svg"
          />
      )
    }
  },


},
  {
    initialRouteName: 'Home',
    activeColor: '#1E93E2',
    inactiveColor: '#404040',
    barStyle: { backgroundColor: '#fff' },
  }

)


const DrawerStack = createDrawerNavigator({
  Dashboard: {
    screen: HomePage,
  },

},

  {

    contentComponent: Drawer,  // customize drawer
    drawerBackgroundColor: '#fff',  // background colour of the drawer
    drawerPosition: 'right',  // position of the drawer
    /// style for the content in the drawaer
    contentOptions: {
      activeTintColor: '#e91e63',
      inactiveTintColor: 'green',
      itemsContainerStyle: {
        marginVertical: 0,
      },
      iconContainerStyle: {
        opacity: 1,
        width: 30,
      }
    }
    /// style for the content in the drawaer
  },

)

const SignedIn = createStackNavigator(
  {
    DrawerStack: {
      screen: DrawerStack
    },
    Interests: {
      screen: Interests,
    },
    EditAbout: {
      screen: EditAbout,
    },
    AttachDocuments: {
      screen: AttachDocuments,
    },
    EditSkills: {
      screen: EditSkills,
    },
    AddEducation: {
      screen: AddEducation,
    },
    EditEducation: {
      screen: EditEducation,
    },
    AddExpirence: {
      screen: AddExpirence,
    },
    EditExpirence: {
      screen: EditExpirence,
    },
    EditIntro: {
      screen: EditIntro,
    },
    Newpost: {
      screen: Newpost,
    },
    Likes: {
      screen: Likes,
    },
    PreviewPost: {
      screen: PreviewPost,
    },
    EditProfile_pic: {
      screen: EditProfile,
    },
    profile: {
      screen: profile,
    },
    Insights: {
      screen: Insights,
    },
    Recruiter: {
      screen: Recruiter,
    },
    RecruiterMaps: {
      screen: RecruiterMaps,
    },
    RecruiterForm: {
      screen: RecruiterForm,
    },
    ExistingRecruiter: {
      screen: ExistingRecruiter,
    },
    RecruiterBoard: {
      screen: RecruiterBoard,
    },
    RecruiterSearch: {
      screen: RecruiterSearch,
    },
    RecruiterAvailable: {
      screen: RecruiterAvailable,
    },
    RecruiterProfile: {
      screen: RecruiterProfile,
    },
    RecruiterView: {
      screen: RecruiterView,
    },
    RecruiterInsights: {
      screen: RecruiterInsights,
    },
    AllApplication: {
      screen: AllApplication,
    },
    ViewApplicant: {
      screen: ViewApplicant,
    },
    ViewApp: {
      screen: ViewApp,
    },
    NotificationSettings: {
      screen: NotificationSettings,
    },
    AccountSettings: {
      screen: AccountSettings,
    },
    SecuritySettigs: {
      screen: SecuritySettigs,
    },
    HelpSupport: {
      screen: HelpSupport,
    },
    AboutWorkbrook: {
      screen: AboutWorkbrook,
    },
    League: {
      screen: League,
    },
    WBPoints: {
      screen: WBPoints,
    },
    LeagueParticipants: {
      screen: LeagueParticipants,
    },
    otherProfile: {
      screen: otherProfile,
    },
    MyWallet: {
      screen: MyWallet,
    },
    AddFund: {
      screen: AddFund,
    },
    AddCard: {
      screen: AddCard,
    },
    PayWith: {
      screen: PayWith,
    },
    WalletWithdraw: {
      screen: WalletWithdraw,
    },
    AddWithdrawerAccount: {
      screen: AddWithdrawerAccount,
    },
    Invite: {
      screen: Invite,
    },
    InviteLink: {
      screen: InviteLink,
    },
    UniqueInviteLink: {
      screen: UniqueInviteLink,
    },
    MyReferrals: {
      screen: MyReferrals,
    },
    ReferralsProfile: {
      screen: ReferralsProfile,
    },
    InsightRecruiterAvailable: {
      screen: InsightRecruiterAvailable,
    },
    RecruiterDashboard: {
      screen: RecruiterDashboard,
    },
    RecruiterLanding: {
      screen: RecruiterLanding,
    },
    Payment: {
      screen: Payment,
    },
    PaymentMethod: {
      screen: PaymentMethod,
    },
    ShortListedApplicants: {
      screen: ShortListedApplicants,
    },
    Saved: {
      screen: Saved,
    },
    
    UpdateRecruiterProfile: {
      screen: UpdateRecruiterProfile,
    },
    Subscription: {
      screen: Subscription,
    },
    ActiveSub: {
      screen: ActiveSub,
    },
    ActiveSubRecruiter: {
      screen: ActiveSubRecruiter,
    },
    SubscribeWithCard: {
      screen: SubscribeWithCard,
    },
    Shared: {
      screen: Shared,
    },
    SharedJobs: {
      screen: SharedJobs,
    },
    ActiveTracker: {
      screen: ActiveTracker,
    },
    PerformanceTracker: {
      screen: PerformanceTracker,
    },
  },

  {

    headerMode: 'none',
    initialRouteName: 'DrawerStack',
    navigationOptions: ({ navigation }) => ({
      headerStyle: { backgroundColor: 'green' },
      // title: 'Mains',
      headerTintColor: 'white',
      gesturesEnabled: true,

      // headerLeft:   <DrawerOpen />,
    })
  }

)

export const createRootNavigator = (signedIn = false) => {
  return createSwitchNavigator(
    {
      SignedIn: {
        screen: SignedIn,

      }
      ,
      SignedOut: {
        screen: SignedOut
      }
    },

    {
      initialRouteName: signedIn ? "SignedIn" : "SignedOut",
      // mode: 'modal',
      // headerMode: 'float',
      // title: 'Main',
      navigationOptions: {
        headerStyle: {
          backgroundColor: 'red',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: 'bold',
        },
        transitionConfig: noTransitionConfig,
      },
    }
  );
};


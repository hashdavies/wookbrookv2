import React, { Component } from 'react';
import {
  Text,
  Image,
  StyleSheet,
  ImageBackground,
  Keyboard,
  TouchableOpacity, FlatList, Alert
} from 'react-native';
import { KeyboardAvoidingView } from 'react-native';
import CustomizeHeader from '../frags/CustomizeHeader';
import { withNavigation } from 'react-navigation';

import Ionicons from 'react-native-vector-icons/Ionicons';
import {
  Container,
  Header,
  Content,
  Textarea,
  Button,
  View,
  Thumbnail,
  Form,
  Item,
  Input,
  Label,
  Badge,
  Icon,
  Footer,
  Picker,
  Left,
  Right,
  Body,
  Title,
  Center,
  CardItem,
  CheckBox
} from 'native-base';
import {
  Kaede,
  Hoshi,
  Jiro,
  Isao,
  Madoka,
  Akira,
  Hideo,
  Kohana,
  Makiko,
  Sae,
  Fumi,
} from 'react-native-textinput-effects';
import Modal from 'react-native-modal';
import { myspiner, AppInlineLoader } from '../../dependency/UtilityFunctions';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import User from "../../assets/workbrookAssets/user4.png";

import accountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import { ScrollView } from 'react-native-gesture-handler';
import imageUser from '../../assets/workbrookAssets/user2.png';
import DatePicker from 'react-native-datepicker'
// import Autocomplete from 'native-base-autocomplete'
import Autocomplete from 'react-native-autocomplete-input';
import { SvgUri } from 'react-native-svg';
import DashboardStore from '../../stores/Dashboard';
import EmptyListComponent from '../../StatelessScreens/EmptyListComponent';
import RadioGroup from 'react-native-radio-button-group';
const API = 'https://swapi.co/api';

var radiogroup_options = [
  {
    id: 0,
    label: 'The process was taking too long'
    // labelView: (
    //   <Text style={{flex: 1}}>
    //    The process was taking too long
    //   </Text>
    // )
  },
  { id: 1, label: 'I was not contacted by the employer' },
  { id: 2, label: 'I don’t like the job anymore' },
  { id: 3, label: 'Others' },
];
@observer
class PostAJob extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      isModalVisible: false,
      isModalOfferVisible: false,
      isModalWithdrawAppVisible: false,
      withdrawApplication: false,
      withdrawReason: "",

    }
    this.toggleModal = this.toggleModal.bind(this);
    this.toggleModalWtDrawal = this.toggleModalWtDrawal.bind(this);
    this.toggleModalWithdrawApp = this.toggleModalWithdrawApp.bind(this);
    this.toggleModalOffer = this.toggleModalOffer.bind(this);
    this.SubscribetoPackageNow = this.SubscribetoPackageNow.bind(this)
    this.PayWithWallet = this.PayWithWallet.bind(this)
    this.PayWithCard = this.PayWithCard.bind(this)

  }
  toggleModal = (item) => {
    this.setState({ isModalVisible: !this.state.isModalVisible });
  };
  toggleModalOffer = () => {
    this.setState({ isModalOfferVisible: !this.state.isModalOfferVisible });
  };
  toggleModalWtDrawal = () => {
    this.setState({ isModalWithdrawAppVisible: !this.state.isModalWithdrawAppVisible });
  };
  toggleModalWithdrawApp = () => {
    // this.setState({ isModalVisible: false });
    // this.setState(
    //   {
    //     withdrawApplication: false
    //   }
    // );
     DashboardStore._updateParameterV2(false, "SubscriptionModal", "RedirectMode");
  };
  PayWithWallet = () => {
    Alert.alert(
      'Pay with Wallet?',
      `Are you sure you want to proceed.`,
      [
           {
              text: 'No', onPress: () => { },//Do nothing
              style: 'cancel'
          },
          {
              text: 'Yes', onPress: () => {
                    DashboardStore.__SubscribeWithWallet();
                }
          },
      ],
      { cancelable: true }
  );
  };
  PayWithCard = () => {
    DashboardStore._updateParameterV2(false, "SubscriptionModal", "RedirectMode");

      
            this.props.navigation.navigate("SubscribeWithCard")
  };


  componentDidMount() {
DashboardStore.__GetSubscriptionConfig(1,10);
DashboardStore.__GetAccountBalance();

  }
  SubscribetoPackageNow = (item) => {
    // let jobid=item.id;
            DashboardStore._updateParameterV2(item, "SingleSubConfig", "SubscriptionConfig");
            DashboardStore._updateParameterV2(true, "SubscriptionModal", "RedirectMode");

            // this.setState(
            //   {
            //     withdrawApplication: true
            //   }
            // );
            // this.props.navigation.navigate("SubscribeWithCard")
        };

  render() {
    // SubscriptionConfig.subscriptions_jobs
    const{SubscriptionConfig}=DashboardStore;
    let ErrorComponentDisplay = null;
    
    if (DashboardStore.isProcessing === true) {
        ErrorComponentDisplay = AppInlineLoader(DashboardStore.isProcessing)
    
    }
    else {
        ErrorComponentDisplay = <EmptyListComponent
            message={SubscriptionConfig.ErrorMessage}
            onclickhandler={this.ReloadPageData}
        />
    }
    let AllSubscriptionConfig =
        <FlatList
            data={SubscriptionConfig.subscriptions_jobs}
            // Render Items
            renderItem={({ item, index }) => {
                //   let Applicants=item.applicants
                // let posterName=  `${poster.first_name}  ${poster.last_name} `
                // let poster = item.created_by_profile
     
                return (

                  <View style={styles.successCard2}>

                  <View style={styles.xLayer2}>
                    <Left style={{ borderBottomWidth: 0, marginLeft: 0, flex: 1 }}>
                <Text style={[styles.Text4, { marginLeft: 0 }]}>{item.name}</Text>
                {
                  item.type==='One-off' ?
                  <Text style={[styles.Text4, { marginLeft: 0 }]} numberOfLines={1} >
                  {item.type}
    </Text>
    :
    item.type==='Monthly' ?
    <Text style={[styles.Text4, { color: "#1E93E2" }]} numberOfLines={1} >
    {item.type}
</Text>
:
<Text style={[styles.Text4, { color: "#5ABC7A" }]} numberOfLines={1} >
{item.type}
</Text>
                }
                
                    </Left>
                    <Right style={[styles.morestyIdc, { borderBottomWidth: 0, width: 65, flex: 0, alignItems:"flex-end", justifyContent: "center"  }]}>
                      <View style={styles.moreBtnSty}
                      >
                        <Text style={styles.Text4}>
                        $ {item.amount}
            </Text>
                      </View>
                    </Right>
                  </View>
      
                  <View style={styles.ViewText1}>
                    <Text style={styles.Text5}>
{
  item.description
}        </Text>
                  </View>
                  <TouchableOpacity style={[styles.BtnPay]}
                                    onPress={()=>this.SubscribetoPackageNow(item)}

                  >
                    <Text style={styles.Text6}>Pay Now ( $ {item.amount})</Text>
                  </TouchableOpacity>
                </View>
      
                )
            }}
            // Item Key
            keyExtractor={(item, index) => String(index)}
            // Header (Title)
            // ListHeaderComponent={this.renderHeader}
            // Footer (Activity Indicator)
            // ListFooterComponent={this.renderFooter()}
            // On End Reached (Takes a function)
            onEndReached={this.retrieveMore}
        // How Close To The End Of List Until Next Data Request Is Made
        // onEndReachedThreshold={0}
        // Refreshing (Set To True When End Reached)
        // refreshing={this.state.refreshing}
        />
    return (
      <View style={{ flex: 1, backgroundColor: '#fff' }}>
        <ScrollView
          contentContainerStyle={{
            paddingLeft: 30,
            paddingRight: 30,
            paddingBottom: 30,
            paddingTop: 30
          }}
        >
          <TouchableOpacity style={styles.xLayer} 
          onPress={() => { this.props.navigation.navigate("ActiveSubRecruiter") }}
          >
            <Left style={{ borderBottomWidth: 0, marginLeft: 0, flex: 1 }}>
              <Text style={[styles.Text4, { marginLeft: 0 }]}>My Active Subcriptions</Text>
            </Left>
            <Right style={[styles.morestyIdc, { borderBottomWidth: 0, width: 40, flex: 0 }]}>
              <View style={styles.moreBtnSty}
              >
                <SvgUri
                  width="7px"
                  height="15px"
                  uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1595409221/Mobile_Assets/arrblkfor_rpzf7o.svg" />
              </View>
            </Right>
          </TouchableOpacity>


          {
                    SubscriptionConfig.subscriptions_jobs.length > 0 ?
                   <React.Fragment>
     
                     {AllSubscriptionConfig}
     
                   </React.Fragment>
                   :
                   ErrorComponentDisplay
               }



{/* 
          <View style={styles.successCard2}>
          
            <View style={styles.xLayer2}>
              <Left style={{ borderBottomWidth: 0, marginLeft: 0, flex: 1 }}>
                <Text style={[styles.Text4, { marginLeft: 0 }]}>Post-A-Job (One-off)</Text>

              </Left>
              <Right style={[styles.morestyIdc, { borderBottomWidth: 0, width: 40, flex: 0 }]}>
                <View style={styles.moreBtnSty}
                >
                  <Text style={styles.Text4}>
                    $99
                  </Text>
                </View>
              </Right>
            </View>
            <View style={styles.ViewText1}>
              <Text style={styles.Text5}>
                This package allows you to post a job on the timeline just once. The payment expires after the job has been published on the timeline.
              </Text>
            </View>
           
            <TouchableOpacity style={[styles.BtnPay]}
              onPress={this.toggleModalWithdrawApp}
            >
              <Text style={styles.Text6}>Pay Now ($99.00)</Text>
            </TouchableOpacity>

          </View>

          <View style={styles.successCard2}>

            <View style={styles.xLayer2}>
              <Left style={{ borderBottomWidth: 0, marginLeft: 0, flex: 1 }}>
                <Text style={[styles.Text4, { marginLeft: 0 }]}>Post-A-Job/Hire-A-Recruiter</Text>
                <Text style={[styles.Text4, { color: "#1E93E2" }]} numberOfLines={1} >
                  Monthly
                </Text>
              </Left>
              <Right style={[styles.morestyIdc, { borderBottomWidth: 0, width: 40, flex: 0 }]}>
                <View style={styles.moreBtnSty}
                >
                  <Text style={styles.Text4}>
                    $299
                  </Text>
                </View>
              </Right>
            </View>

            <View style={styles.ViewText1}>
              <Text style={styles.Text5}>
                This package allows you to post as many times as you want to the timeline within a month i.e. 30 days. You will also be granted access to a total of 3 recruiters in a month.
              </Text>
            </View>
  
            <TouchableOpacity style={[styles.BtnPay]}>
              <Text style={styles.Text6}>Pay Now ($299.00)</Text>
            </TouchableOpacity>
          </View>

          <View style={styles.successCard2}>

            <View style={styles.xLayer2}>
              <Left style={{ borderBottomWidth: 0, marginLeft: 0, flex: 1 }}>
                <Text style={[styles.Text4, { marginLeft: 0 }]}>Post-A-Job/Hire-A-Recruiter</Text>
                <Text style={[styles.Text4, { color: "#5ABC7A" }]} numberOfLines={1} >
                  Yearly
    </Text>
              </Left>
              <Right style={[styles.morestyIdc, { borderBottomWidth: 0, width: 40, flex: 0 }]}>
                <View style={styles.moreBtnSty}
                >
                  <Text style={styles.Text4}>
                  $2,999
      </Text>
                </View>
              </Right>
            </View>

            <View style={styles.ViewText1}>
              <Text style={styles.Text5}>
                This package allows you to post as many times as you want to the timeline within a month i.e. 30 days. You will also be granted access to a total of 3 recruiters in a month.
  </Text>
            </View>
            <TouchableOpacity style={[styles.BtnPay]}>
              <Text style={styles.Text6}>Pay Now ($2,999.00)</Text>
            </TouchableOpacity>
          </View> */}


        </ScrollView>
        {/* modal views */}
        <Modal style={styles.MContainer} isVisible={this.state.isModalVisible}>

          <View style={styles.Mwrapper}>
            <Button onPress={() => this.toggleModal()} style={{ backgroundColor: "transparent", position: 'absolute', right: 10, top: -30, padding: 20, elevation: 0 }}>
              <SvgUri
                width="19px"
                height="19px"
                uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593711047/Mobile_Assets/closeWht_oohxti.svg" />
            </Button>
            <View style={styles.firstView} >
              {/* <Button full style={styles.actionBtn}>
                <Text style={styles.actionBtnText}>
                  View the job poster’s profile
              </Text>
              </Button> */}
              <Button
                full
                style={[styles.actionBtn, { borderTopWidth: 1, borderTopColor: "#E5E5E5", borderBottomWidth: 1, borderBottomColor: "#E5E5E5" }]}
                onPress={this.GotoMyProfile}
              >
                <Text style={styles.actionBtnText}>
                  Edit your profile
              </Text>
              </Button>
              <Button full style={styles.actionBtn}
                onPress={this.toggleModalWithdrawApp}
              >
                <Text style={[styles.actionBtnText, { color: "#FF5964" }]}>
                  Withdraw application
              </Text>
              </Button>
            </View>
            <Button style={[styles.btnCancel, { marginTop: 15 }]} title="Hide modal" onPress={() => this.toggleModal()}>
              <Text style={[styles.btnCancelText,]}>Cancel</Text>
            </Button>
          </View>
        </Modal>

        {/* edit offer*/}

        <Modal style={styles.MContainer} isVisible={this.state.isModalOfferVisible}>

          <View style={styles.Mwrapper}>
            <Button onPress={this.toggleModalOffer} style={{ backgroundColor: "transparent", position: 'absolute', right: 10, top: -30, padding: 20, elevation: 0 }}>
              <SvgUri
                width="19px"
                height="19px"
                uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593711047/Mobile_Assets/closeWht_oohxti.svg" />
            </Button>
            <View style={styles.firstView} >
              <Button full style={styles.actionBtn}>
                <Text style={styles.actionBtnText}>
                  Edit the offer
              </Text>
              </Button>
              <Button full style={styles.actionBtn}>
                <Text style={[styles.actionBtnText, { color: "#FF5964" }]}>
                  Close the offer
              </Text>
              </Button>
            </View>
            <Button style={[styles.btnCancel, { marginTop: 15 }]} title="Hide modal" onPress={this.toggleModalOffer}>
              <Text style={[styles.btnCancelText,]}>Cancel</Text>
            </Button>
          </View>
        </Modal>

        {/* edit offer*/}

        {/* Withdraw App  modal*/}
        <Modal style={styles.MContainer} isVisible={DashboardStore.RedirectMode.SubscriptionModal}>
          <View style={styles.Mwrapper}>
            <Button onPress={this.toggleModalWithdrawApp} style={{ backgroundColor: "transparent", position: 'absolute', right: 10, top: -30, padding: 20, elevation: 0 }}>
              <SvgUri
                width="19px"
                height="19px"
                uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593711047/Mobile_Assets/closeWht_oohxti.svg" />
            </Button>
            <View style={styles.ModalView2} >
              <TouchableOpacity style={[styles.BtnPay]}
                onPress={this.PayWithCard}
              >
                <Text style={styles.Text6}>Pay with Card</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[styles.btnCancel, { marginTop: 15, color: 'green' }]} title="Hide modal"
                onPress={this.PayWithWallet}>
                <Text style={[styles.btnCancelText,]}>{DashboardStore.isProcessing === true ? 'Processing...' : `MyWallet ($ ${DashboardStore.WalletBalance.Balance})`}</Text>
              </TouchableOpacity>
              <Button full style={styles.actionBtn2}>
                <Text style={[styles.actionBtnText, { color: "#FF5964" }]}>
                  {/* {
                    parseFloat(SubscriptionConfig.SingleSubConfig.amount) > parseFloat(DashboardStore.WalletBalance.Balance) ?
                    "Insufficient Funds"
                    :
                    null
                  } */}
                  {/* Withdraw application */}
              </Text>
              </Button>

            </View>

          </View>
        </Modal>
        {/* Withdraw App modal*/}

        {/* modal views */}
      </View>
    );
  }
}
export default withNavigation(PostAJob);

const inputStyles = {
  size: 100,
  color: '#FFFFFF',
};

const height = 40;

const styles = StyleSheet.create({
  Text1: {
    fontSize: 10,
    fontWeight: '500',
    color: "#2F2F2F",
    textAlign: 'center'
  },
  Text1_2: {
    fontSize: 10,
    fontWeight: 'bold',
    color: "#2F2F2F",
    textAlign: 'center'
  },
  Text3: {
    fontSize: 12,
    fontWeight: '500',
    color: "#FAFAFA",
    textAlign: 'center'
  },
  Text7: {
    fontSize: 12,
    fontWeight: '500',
    color: "#2F2F2F",
    textAlign: 'center'
  },
  Text4: {
    fontSize: 13,
    fontWeight: '600',
    color: "#2F2F2F",
  },
  Text6: {
    fontSize: 14,
    fontWeight: '500',
    color: "#FAFAFA",
  },
  Text4_2: {
    fontSize: 12,
    fontWeight: 'normal',
    color: "#676767",
  },
  Text5: {
    fontSize: 11,
    fontWeight: '300',
    color: "#2F2F2F",
  },
  Text2: {
    fontSize: 18,
    fontWeight: '500',
    color: "#1E93E2",
  },
  likeTabsty: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  viewPerTabSty: {
    display: 'flex',
    alignItems: 'center',
    maxWidth: 79,
    textAlign: 'center',
  },
  successCard: {
    display: 'flex',
    alignItems: 'center',
    textAlign: 'center',
    justifyContent: "center",
    backgroundColor: '#F3F3F3',
    height: 100,
    // elevation: 1,
    marginTop: 30,
  },
  successCard2: {
    borderTopWidth: 2,
    borderTopColor: "#1E93E2",
    display: 'flex',
    // justifyContent: 'space-between',
    textAlign: 'center',
    backgroundColor: '#F3F3F3',
    // height: 100,
    marginTop: 17,
    paddingBottom: 12,
    paddingLeft: 18,
    paddingRight: 18,
    paddingTop: 12,
  },
  cardinnerSty: {
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    textAlign: 'center',
    justifyContent: "center",
    backgroundColor: '#1E93E2',
    height: 24,
  },
  morestyIdc: {
    display: 'flex',
    justifyContent: 'flex-end',
    alignItems: 'center',
    width: 20
  },
  moreBtnSty: {
    // width: 20,
    borderRadius: 5,
    borderWidth: 0,
    borderColor: '#1E93E2',
    display: 'flex',
    justifyContent: 'center',
    // alignItems: 'center',
  },
  xLayer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: 14,
    paddingLeft: 19,
    paddingRight: 19,
    paddingTop: 14,
  },
  xLayer2: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: 14,
    paddingLeft: 0,
    paddingRight: 0,
    paddingTop: 0,
  },
  ViewText1: {
    display: 'flex',
    flexDirection: 'row',
    paddingBottom: 14,
    paddingLeft: 0,
    paddingRight: 0,
    paddingTop: 0,
  },
  // ======================= modal style ==============================
  MContainer: {
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
  },
  Mwrapper: {
    // height: 360,
    width: '98%',
    // backgroundColor: '#fff',
    padding: 15,
    display: 'flex',
    justifyContent: 'space-around',
    // alignItems: 'center'
  },
  firstView: {
    backgroundColor: '#FAFAFA',
    borderRadius: 5
  },
  ModalView2: {
    backgroundColor: '#FAFAFA',
    borderRadius: 3,
    paddingBottom: 21,
    paddingLeft: 15,
    paddingRight: 15,
    paddingTop: 21,

  },
  actionBtn: {
    backgroundColor: '#FAFAFA',
    borderRadius: 5,
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    height: 43,
  },
  actionBtn2: {
    backgroundColor: '#FAFAFA',
    borderRadius: 5,
    elevation: 0,
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    height: 43,
  },
  checkBoxWrap: {
    borderTopWidth: 1,
    borderTopColor: '#E5E5E5',
    paddingTop: 14,
    paddingLeft: 20,
    paddingRight: 20,
    paddingBottom: 14,
    width: '90%'
    // display: 'flex',
    // justifyContent: 'center',
    // flexDirection: 'row',
    // alignItems: 'center',
    // height: 43,
  },
  actionBtnText: {
    fontSize: 12,
    color: '#1E93E2',
  },
  btnCancel: {
    backgroundColor: '#FF5964',
    borderRadius: 5,
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    height: 43,
    elevation: 0,
  },
  BtnPay: {
    backgroundColor: '#1E93E2',
    borderRadius: 3,
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: 11,
    paddingBottom: 11,
    paddingLeft: 10,
    paddingRight: 10,
    elevation: 0,
  },
  btnCancelText: {
    fontSize: 12,
    color: '#FAFAFA',
    fontWeight: 'bold',
  },
  checkBoxText: {
    fontSize: 11,
    color: '#2F2F2F',
    marginLeft: 20
  },
  CheckBoxBody: {
    display: 'flex',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10,
  },
  // ======================= modal style ==============================

});

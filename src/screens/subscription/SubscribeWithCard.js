import React, { Component } from 'react';
import {
    Text,
    Image,
    StyleSheet,
    ImageBackground,
    Keyboard,
    TouchableOpacity,
    FlatList,
    Alert
} from 'react-native';
import { KeyboardAvoidingView } from 'react-native';
import CustomizeHeader from '../frags/CustomizeHeader';

import Ionicons from 'react-native-vector-icons/Ionicons';
import {
    Container,
    Header,
    Content,
    Textarea,
    Button,
    View,
    Thumbnail,
    Form,
    Item,
    Input,
    Label,
    Badge,
    Icon,
    Footer,
    Picker,
    Left,
    Right,
    Body,
    Title,
    Center,
    CardItem,
    CheckBox,
    ListItem,
    Card,
} from 'native-base';
import {
    Kaede,
    Hoshi,
    Jiro,
    Isao,
    Madoka,
    Akira,
    Hideo,
    Kohana,
    Makiko,
    Sae,
    Fumi,
} from 'react-native-textinput-effects';
import Modal from 'react-native-modal';
import { myspiner, AppInlineLoader } from '../../dependency/UtilityFunctions';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import User from "../../assets/workbrookAssets/user4.png";
import { withNavigation } from 'react-navigation';
import accountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import { ScrollView } from 'react-native-gesture-handler';
import imageUser from '../../assets/workbrookAssets/user2.png';
import DatePicker from 'react-native-datepicker'
// import Autocomplete from 'native-base-autocomplete'
import Autocomplete from 'react-native-autocomplete-input';
// import Balance from './Balance';
import GridView from 'react-native-gridview';
import { SvgUri } from 'react-native-svg';
import DashboardStore from '../../stores/Dashboard';
import EmptyListComponent from '../../StatelessScreens/EmptyListComponent';
const API = 'https://swapi.co/api';

@observer
class SubscribeWithCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            isModalVisible: false,
            isModalViewApplicants: false,
        }
        this.SubscribewithNow=this.SubscribewithNow.bind(this);

    }
    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible });
    };
    toggleModalViewApplicants = () => {
        this.setState({ isModalViewApplicants: !this.state.isModalViewApplicants });
    };

    componentDidMount() {
        DashboardStore.__GetMycardList(1,30);

    }
    SubscribewithNow = (item) => {
        let thisCardId =item.id;
        // DashboardStore._updateParameterV2(thisCardId, "card_id", "FundWallet");
        // this.props.navigation.navigate("AddFund")
        Alert.alert(
            'Pay with this card?',
            `Are you sure you want to proceed.`,
            [
                 {
                    text: 'No', onPress: () => { },//Do nothing
                    style: 'cancel'
                },
                {
                    text: 'Yes', onPress: () => {
                        DashboardStore.__SubscribeWithcard(thisCardId);
                    }
                },
            ],
            { cancelable: true }
        );
      };
    render() {
const{CardsInSystem}=DashboardStore;
let ErrorComponentDisplay = null;

if (DashboardStore.isProcessing === true) {
    ErrorComponentDisplay = AppInlineLoader(DashboardStore.isProcessing)

}
else {
    ErrorComponentDisplay = <EmptyListComponent
        message={DashboardStore.CardsInSystem.ErrorMessage}
        onclickhandler={this.ReloadPageData}
    />
}
let CardLists =
    <FlatList
        data={CardsInSystem.Mycards}
        // Render Items
        renderItem={({ item, index }) => {
         
 
            return (
         
                <TouchableOpacity 
                // onPress={() => { this.props.navigation.navigate("AddFund") }}
                onPress={()=>this.SubscribewithNow(item)}

                >
           <Card button style={styles.CardSty2} >
               <View style={{ width: "100%", display: 'flex', flexDirection: 'row', justifyContent: "space-between" , }}>
                   <Left style={{ flex: 2 , display:"flex", flexDirection: "row", alignItems: "center"}}>
                     {
                         item.brand==="Visa" ?
                         <SvgUri

                         width="42.58"
                         height="26.61"
                         uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1592684286/Mobile_Assets/visacard_cigin6.svg"
                         >
                     </SvgUri>
                     :
                     <SvgUri

                     width="42.58"
                     height="26.61"
                     uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1592684286/Mobile_Assets/masterCard_unpe0g.svg"
                 >
                 </SvgUri>
                     }
                     
                    
                       <Text style={[styles.Text6, { color: "#2F2F2F" , marginRight:5}]}>
                               **** **** **** {item.last4}
                       </Text>
                   </Left>
                   <Right style={{ flex: 1, }}>
                       <ListItem style={{ borderBottomColor: "transparent", width: '100%' }}>
                           <CheckBox style={styles.checkBoxSty}
                           
                           checked={
                              item.isDefault==="1" ?true :false
                               
                               } />
                       </ListItem>
                   </Right>
               </View>
               <View style={{ width: "100%", display: 'flex', flexDirection: 'row', justifyContent: "space-between" }}>
                   <Left style={{ flex: 2 }}>
                       <Text style={styles.Text4}>
                           {/* Adekunle Ciroma Chukwu */}
                           {item.exp_month < 10  ? `0${item.exp_month} / ${item.exp_year}`: `${item.exp_month} / ${item.exp_year}`}
                       </Text>
                   </Left>
               </View>
           </Card>
           </TouchableOpacity>
         
         
            )
        }}
        // Item Key
        keyExtractor={(item, index) => String(index)}
        // Header (Title)
        // ListHeaderComponent={this.renderHeader}
        // Footer (Activity Indicator)
        // ListFooterComponent={this.renderFooter()}
        // On End Reached (Takes a function)
        onEndReached={this.retrieveMore}
    // How Close To The End Of List Until Next Data Request Is Made
    // onEndReachedThreshold={0}
    // Refreshing (Set To True When End Reached)
    // refreshing={this.state.refreshing}
    />
        return (
            <View style={{ flex: 1, backgroundColor: '#fff' }}>
                <CustomizeHeader
                    leftside='WhiteArr'
                    title='Subscribe'
                    rightside='empty'
                    statedColor='#1E93E2'
                    icon='md-close'
                />
                <ScrollView>
                 {/* <View style={styles.cupHolder}>
                <Balance/>
                    <View style={styles.buttonList}>
                        <Button style={[styles.btnAddSty, { backgroundColor: '#FFF' }]}>
                            <Text style={[styles.Text7, { color: '#1E93E2' }]}>ADD FUNDS</Text>
                        </Button>
                        <Button style={styles.btnAddSty}
                         onPress={() => { this.props.navigation.navigate("WalletWithdraw") }}
                        >
                            <Text style={styles.Text7}>WITHDRAW </Text>
                        </Button>
                    </View>
                </View> */}
                {
                      CardsInSystem.Mycards.length > 0 ?
                      <View style={styles.titleView}>
                      <Text style={styles.Text6}>
                      Choose a card to pay with
                      </Text>
                  </View>
                  :
                  <View style={styles.titleView}>
                  <Text style={styles.Text6}>
                 Add new card
                  </Text>
              </View>
                }
             

                <ScrollView
                    contentContainerStyle={{
                        paddingLeft: 26,
                        paddingRight: 26,
                        paddingBottom: 26,
                        paddingTop: 26
                    }}
                >

                   {
                //        CardsInSystem.Mycards.length < 1 ?
                //        <Button full style={styles.btnAddCard}
                //        onPress={() => { this.props.navigation.navigate("AddCard") }}
                // >
                           
                //     <Text style={[styles.Text7, {color:"#1E93E2"}]}>+ ADD CARD</Text>
                //        </Button>


                //    } 
                   CardsInSystem.Mycards.length > 0 ?
                   <React.Fragment>
     
                     {CardLists}
     
                   </React.Fragment>
                   :
                //    ErrorComponentDisplay
                <Button full style={styles.btnAddCard}
                onPress={() => { this.props.navigation.navigate("AddCard") }}
         >
                    
             <Text style={[styles.Text7, {color:"#1E93E2"}]}>+ ADD CARD</Text>
                </Button>
               }
                        


                        {/* <TouchableOpacity 
                         onPress={() => { this.props.navigation.navigate("AddFund") }}
                         >
                    <Card button style={styles.CardSty2} >
                        <View style={{ width: "100%", display: 'flex', flexDirection: 'row', justifyContent: "space-between" , }}>
                            <Left style={{ flex: 2 , display:"flex", flexDirection: "row", alignItems: "center"}}>
                                <SvgUri

                                    width="42.58"
                                    height="26.61"
                                    uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1592684286/Mobile_Assets/masterCard_unpe0g.svg"
                                >
                                </SvgUri>
                                <Text style={[styles.Text6, { color: "#2F2F2F" , marginRight:5}]}>
                                    **** **** **** 3446
                                </Text>
                            </Left>
                            <Right style={{ flex: 1, }}>
                                <ListItem style={{ borderBottomColor: "transparent", width: '100%' }}>
                                    <CheckBox style={styles.checkBoxSty} checked={true} />
                                </ListItem>
                            </Right>
                        </View>
                        <View style={{ width: "100%", display: 'flex', flexDirection: 'row', justifyContent: "space-between" }}>
                            <Left style={{ flex: 2 }}>
                                <Text style={styles.Text4}>
                                    Adekunle Ciroma Chukwu
                                </Text>
                            </Left>
                        </View>
                    </Card>
                    </TouchableOpacity>
                   */}
                  
                  
                  
                  {/* <Card style={styles.CardSty2}>
                        <View style={{ width: "100%", display: 'flex', flexDirection: 'row', justifyContent: "space-between" , }}>
                            <Left style={{ flex: 2 , display:"flex", flexDirection: "row", alignItems: "center"}}>
                                <SvgUri

                                    width="42.58"
                                    height="26.61"
                                    uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1592684286/Mobile_Assets/visacard_cigin6.svg"
                                >
                                </SvgUri>
                                <Text style={[styles.Text6, { color: "#2F2F2F" , marginRight:17}]}>
                                    **** **** **** 3446
                                </Text>
                            </Left>
                            <Right style={{ flex: 1, }}>
                                <ListItem style={{ borderBottomColor: "transparent", width: '100%' }}>
                                    <CheckBox style={styles.checkBoxSty} checked={true} />
                                </ListItem>
                            </Right>
                        </View>
                        <View style={{ width: "100%", display: 'flex', flexDirection: 'row', justifyContent: "space-between" }}>
                            <Left style={{ flex: 2 }}>
                                <Text style={styles.Text4}>
                                    Adekunle Ciroma Chukwu
                                </Text>
                            </Left>
                        </View>
                    </Card> */}

                 
                    <TouchableOpacity style={styles.typeofCard}
                    onPress={() => { this.props.navigation.navigate("AddCard") }}
                   
                    >
                        <Text style={[styles.Text4, {color: "#1E93E2"}]}>+ Add Card</Text>
                    </TouchableOpacity>



                </ScrollView>
                </ScrollView>
          
            </View>
        );
    }
}
export default withNavigation(SubscribeWithCard);

const inputStyles = {
    size: 100,
    color: '#FFFFFF',
};

const height = 40;

const styles = StyleSheet.create({
    Text1: {
        fontSize: 10,
        fontWeight: '500',
        color: "#2F2F2F",
        textAlign: 'center'
    },
    Text1_3: {
        fontSize: 10,
        fontWeight: '500',
        color: "#1E93E2",
        textAlign: 'center'
    },
    Text1_4: {
        fontSize: 10,
        fontWeight: '500',
        color: "#FF5964",
        textAlign: 'center'
    },
    Text1_2: {
        fontSize: 10,
        fontWeight: 'bold',
        color: "#2F2F2F",
        textAlign: 'center'
    },
    Text3: {
        fontSize: 12,
        fontWeight: '500',
        color: "#FAFAFA",
        textAlign: 'center'
    },
    Text7: {
        fontSize: 13,
        fontWeight: '500',
        color: "#FFF",
        textAlign: 'center'
    },
    Text4: {
        fontSize: 13,
        fontWeight: '500',
        color: "#2F2F2F",
    },
    Text4_2: {
        fontSize: 12,
        fontWeight: 'normal',
        color: "#676767",
    },
    Text5: {
        fontSize: 11,
        fontWeight: '300',
        color: "#2F2F2F",
    },
    Text6: {
        fontSize: 14,
        fontWeight: '500',
        color: "#1E93E2",
    },
    Text8: {
        fontSize: 12,
        fontWeight: '600',
        color: "#1E93E2",
    },
    Text9: {
        fontSize: 6,
        fontWeight: '500',
        color: "#2F2F2F",
    },
    Text2: {
        fontSize: 18,
        fontWeight: '500',
        color: "#1E93E2",
    },
    likeTabsty: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    viewPerTabSty: {
        display: 'flex',
        alignItems: 'center',
        maxWidth: 79,
        textAlign: 'center',
    },
    successCard: {
        display: 'flex',
        alignItems: 'center',
        textAlign: 'center',
        justifyContent: "center",
        backgroundColor: '#F3F3F3',
        height: 100,
        // elevation: 1,
        marginTop: 30,
    },
    successCard2: {
        display: 'flex',
        // justifyContent: 'space-between',
        textAlign: 'center',
        // backgroundColor: '#F3F3F3',
        // height: 100,
        // marginTop: 17,
    },
    CardSty2: {
        display: 'flex',
        textAlign: 'center',
        paddingLeft:20,
        paddingBottom:20,
        paddingTop:20,
        // backgroundColor: '#F3F3F3',
        // height: 100,
        marginBottom: 20,
    },
    typeofCard: {
        display: 'flex',
        flexDirection: "row",
        justifyContent: 'flex-end',
        textAlign: 'center',
        // backgroundColor: '#F3F3F3',
    },
    cardinnerSty: {
        width: '100%',
        display: 'flex',
        alignItems: 'flex-end',
        textAlign: 'right',
        justifyContent: "center",
        backgroundColor: '#2f2f2f',
        height: 24,
        paddingRight: 12,
        paddingLeft: 12,
    },
    morestyIdc: {
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'center',
        width: 20
    },
    moreBtnSty: {
        height: 20,
        width: 20,
        borderRadius: 5,
        borderWidth: 0,
        borderColor: '#1E93E2',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    copylinkSty: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: 18,
    },
    xLayer: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingBottom: 14,
        paddingLeft: 19,
        paddingRight: 19,
        paddingTop: 14,
    },
    cupHolder: {
        display: "flex",
        justifyContent: "center",
        alignItems: 'center',
        paddingTop: 14,
        paddingBottom: 14,
        paddingLeft: 30,
        paddingRight: 30,
        // marginBottom: 40,
        backgroundColor: '#1E93E2',
    },
    buttonList: {
        display: "flex",
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%'
    },
    btnAddSty: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: 138,
        height: 45,
        borderWidth: 1,
        borderColor: '#FFFFFF',
        borderRadius: 3,
        backgroundColor: 'transparent',
        elevation: 0,
    },
    titleView: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: "100%",
        height: 55,
        elevation: 1,
        borderBottomWidth: 1,
        borderBottomColor: '#FFFFFF',
        backgroundColor: '#F7F7F7',
        elevation: 0,
    },
    btnAddCard: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        // height: 41,
        paddingTop: 15,
        paddingBottom: 15,
        borderWidth:1,
        borderColor: "#1E93E2",
        backgroundColor: 'transparent',
        borderRadius: 3,
        elevation: 0,
        marginBottom: 10,
    },
    // ======================= modal style ==============================
    MContainer: {
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
    },
    Mwrapper: {
        // height: 360,
        width: '98%',
        // backgroundColor: '#fff',
        padding: 15,
        display: 'flex',
        justifyContent: 'space-around',
        // alignItems: 'center'
    },
    firstView: {
        backgroundColor: '#FAFAFA',
        borderRadius: 5,
        padding: 25,
    },
    actionBtn: {
        backgroundColor: '#FAFAFA',
        borderRadius: 5,
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
        height: 43,
    },
    actionBtn2: {
        backgroundColor: '#FAFAFA',
        borderRadius: 5,
        elevation: 0,
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
        height: 43,
    },
    codeWrap: {
        paddingTop: 14,
        paddingBottom: 14,
        // display: 'flex',
        // justifyContent: 'center',
        // flexDirection: 'row',
        // alignItems: 'center',
        // height: 43,
    },
    checkBoxWrap: {
        borderTopWidth: 1,
        borderTopColor: '#E5E5E5',
        paddingTop: 14,
        paddingLeft: 20,
        paddingRight: 20,
        // paddingBottom: 14,
        // display: 'flex',
        // justifyContent: 'center',
        // flexDirection: 'row',
        // alignItems: 'center',
        // height: 43,
    },
    actionBtnText: {
        fontSize: 12,
        color: '#1E93E2',
    },
    btnCancel: {
        backgroundColor: '#FF5964',
        borderRadius: 5,
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
        height: 43,
        elevation: 0,
    },
    btnCancelText: {
        fontSize: 12,
        color: '#FAFAFA',
        fontWeight: 'bold',
    },
    checkBoxText: {
        fontSize: 11,
        color: '#2F2F2F',
        marginLeft: 20
    },
    CheckBoxBody: {
        display: 'flex',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 10,
    },

    // ======================= modal style ==============================
    userFlex: {
        display: 'flex',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        alignItems: 'center',
        flexWrap: 'wrap',
        paddingLeft: 10,
        paddingRight: 10,
        // marginTop: 10,
    },
    successCard2Bottom: {
        display: 'flex',
        justifyContent: 'space-between',
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 19,
        paddingRight: 19,
        marginTop: 29,
        marginBottom: 17,
    },

    copyViewBtnWrap: {
        display: 'flex',
        alignItems: 'center',
        paddingLeft: 19,
        paddingRight: 19,
        marginTop: 20,
        marginBottom: 17,
    },
    card2Bottom: {
        height: 28,
        backgroundColor: '#5ABC7A',
        borderRadius: 5,
        elevation: 0,
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 14,
        paddingRight: 14
    },
    userFlexImageWrap: {
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 5,
        width: '16.6%'
    },
    userFlexImage: {
        width: 32,
        height: 34,
        borderRadius: 34 / 2,
    },
    ViewBtnSty: {
        height: 23,
        width: 50,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#1E93E2',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    ViewBtnRmSty: {
        height: 23,
        width: 50,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#FF5964',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 2,
    },
    copyVisitBtn: {
        height: 41,
        width: '100%',
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#1E93E2',
        display: 'flex',
        justifyContent: 'center',
        marginBottom: 15,
        alignItems: 'center',
        elevation: 0
    },
    codeDisplaySty: {
        borderWidth: 1,
        borderColor: "#E5E5E5",
        borderRadius: 3,
        padding: 15,
    },
    groupBtnWrap: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: "center"
    },
    changeView: {
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
    },
    checkBoxSty: {
       width: 27,
       height: 27,
       borderRadius: 27 / 2, 
       display: 'flex',
       justifyContent: "center",
       alignItems: "center",
    //    flexDirection: "row",
    },
});

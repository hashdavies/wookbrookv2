import React, { Component } from 'react';
import {
  Text,
  Image,
  StyleSheet,
  ImageBackground,
  Keyboard,
  TouchableOpacity, FlatList
} from 'react-native';
import { KeyboardAvoidingView } from 'react-native';
import CustomizeHeader from '../frags/CustomizeHeader';
import { withNavigation } from 'react-navigation';

import Ionicons from 'react-native-vector-icons/Ionicons';
import {
  Container,
  Header,
  Content,
  Textarea,
  Button,
  View,
  Thumbnail,
  Form,
  Item,
  Input,
  Label,
  Badge,
  Icon,
  Footer,
  Picker,
  Left,
  Right,
  Body,
  Title,
  Center,
  CardItem,
  CheckBox
} from 'native-base';
import {
  Kaede,
  Hoshi,
  Jiro,
  Isao,
  Madoka,
  Akira,
  Hideo,
  Kohana,
  Makiko,
  Sae,
  Fumi,
} from 'react-native-textinput-effects';
import Modal from 'react-native-modal';
import { myspiner, AppInlineLoader } from '../../dependency/UtilityFunctions';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import User from "../../assets/workbrookAssets/user4.png";

import accountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import { ScrollView } from 'react-native-gesture-handler';
import imageUser from '../../assets/workbrookAssets/user2.png';
import DatePicker from 'react-native-datepicker'
// import Autocomplete from 'native-base-autocomplete'
import Autocomplete from 'react-native-autocomplete-input';
import { SvgUri } from 'react-native-svg';
import DashboardStore from '../../stores/Dashboard';
import EmptyListComponent from '../../StatelessScreens/EmptyListComponent';
import RadioGroup from 'react-native-radio-button-group';
const API = 'https://swapi.co/api';

var radiogroup_options = [
  {
    id: 0,
    label: 'The process was taking too long'
    // labelView: (
    //   <Text style={{flex: 1}}>
    //    The process was taking too long
    //   </Text>
    // )
  },
  { id: 1, label: 'I was not contacted by the employer' },
  { id: 2, label: 'I don’t like the job anymore' },
  { id: 3, label: 'Others' },
];
@observer
class Recruiter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      isModalVisible: false,
      isModalOfferVisible: false,
      isModalWithdrawAppVisible: false,
      withdrawApplication: false,
      withdrawReason: "",

    }
    this.toggleModal = this.toggleModal.bind(this);
    this.toggleModalWtDrawal = this.toggleModalWtDrawal.bind(this);
    this.toggleModalWithdrawApp = this.toggleModalWithdrawApp.bind(this);
    this.toggleModalOffer = this.toggleModalOffer.bind(this);

  }
  toggleModal = (item) => {
    this.setState({ isModalVisible: !this.state.isModalVisible });
  };
  toggleModalOffer = () => {
    this.setState({ isModalOfferVisible: !this.state.isModalOfferVisible });
  };
  toggleModalWtDrawal = () => {
    this.setState({ isModalWithdrawAppVisible: !this.state.isModalWithdrawAppVisible });
  };
  toggleModalWithdrawApp = () => {
    // this.setState({ isModalVisible: false });
    this.setState(
      {
        withdrawApplication: !this.state.withdrawApplication
      }
    );
  };


  componentDidMount() {

  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: '#fff' }}>
        <ScrollView
          contentContainerStyle={{
            paddingLeft: 30,
            paddingRight: 30,
            paddingBottom: 30,
            paddingTop: 30
          }}
        >
          <TouchableOpacity style={styles.xLayer}
            onPress={()=> { this.props.navigation.navigate("ActiveSubRecruiter") }}
          >
            <Left style={{ borderBottomWidth: 0, marginLeft: 0, flex: 1 }}>
              <Text style={[styles.Text4, { marginLeft: 0 }]}>My Active Subcriptions</Text>
            </Left>
            <Right style={[styles.morestyIdc, { borderBottomWidth: 0, width: 40, flex: 0 }]}>
              <View style={styles.moreBtnSty}
              >
                <SvgUri
                  width="7px"
                  height="15px"
                  uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1595409221/Mobile_Assets/arrblkfor_rpzf7o.svg" />
              </View>
            </Right>
          </TouchableOpacity>
          <View style={styles.successCard2}>
            {/* <View style={styles.cardinnerSty}>
              <Text style={styles.Text3}>You are being considered for the role</Text>
            </View> */}
            <View style={styles.xLayer2}>
              <Left style={{ borderBottomWidth: 0, marginLeft: 0, flex: 1 }}>
                <Text style={[styles.Text4, { marginLeft: 0 }]}>Hire a recruiter (One-off)</Text>

              </Left>
              <Right style={[styles.morestyIdc, { borderBottomWidth: 0, width: 40, flex: 0 }]}>
                <View style={styles.moreBtnSty}
                >
                  <Text style={styles.Text4}>
                    $99
                  </Text>
                </View>
              </Right>
            </View>
            <View style={styles.ViewText1}>
              {/* <Text style={styles.Text5}>
                This package allows you to post a job on the timeline just once. The payment expires after the job has been published on the timeline.
              </Text> */}
            </View>
           
            <TouchableOpacity style={[styles.BtnPay]}
              onPress={this.toggleModalWithdrawApp}
            >
              <Text style={styles.Text6}>Pay Now ($99.00)</Text>
            </TouchableOpacity>

          </View>

          <View style={styles.successCard2}>

            <View style={styles.xLayer2}>
              <Left style={{ borderBottomWidth: 0, marginLeft: 0, flex: 1 }}>
                <Text style={[styles.Text4, { marginLeft: 0 }]}>Recruiter Subscription</Text>
                <Text style={[styles.Text4, { color: "#1E93E2" }]} numberOfLines={1} >
                  Monthly
                </Text>
              </Left>
              <Right style={[styles.morestyIdc, { borderBottomWidth: 0, width: 40, flex: 0 }]}>
                <View style={styles.moreBtnSty}
                >
                  <Text style={styles.Text4}>
                  $59
                  </Text>
                </View>
              </Right>
            </View>

            <View style={styles.ViewText1}>
              {/* <Text style={styles.Text5}>
                This package allows you to post as many times as you want to the timeline within a month i.e. 30 days. You will also be granted access to a total of 3 recruiters in a month.
              </Text> */}
            </View>
            {/* <View style={styles.ViewText1}>
            <SvgUri
              width="7px"
              height="15px"
              uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1590639365/Mobile_Assets/arrforblue_wau5kk.svg" />
              <Text style={[styles.Text5, {marginLeft: 11}]}>
              Unlimited posts to the timeline
              </Text>
            </View> */}
            {/* <View style={styles.ViewText1}>
            <SvgUri
              width="7px"
              height="15px"
              uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1590639365/Mobile_Assets/arrforblue_wau5kk.svg" />
              <Text style={[styles.Text5, {marginLeft: 11}]}>
              Access to 3 recruiters
              </Text>
            </View> */}
            <TouchableOpacity style={[styles.BtnPay]}>
              <Text style={styles.Text6}>Pay Now ($59.00)</Text>
            </TouchableOpacity>
          </View>

          <View style={styles.successCard2}>

            <View style={styles.xLayer2}>
              <Left style={{ borderBottomWidth: 0, marginLeft: 0, flex: 1 }}>
                <Text style={[styles.Text4, { marginLeft: 0 }]}>Recruiter Subscription</Text>
                <Text style={[styles.Text4, { color: "#5ABC7A" }]} numberOfLines={1} >
                  Yearly
    </Text>
              </Left>
              <Right style={[styles.morestyIdc, { borderBottomWidth: 0, width: 40, flex: 0 }]}>
                <View style={styles.moreBtnSty}
                >
                  <Text style={styles.Text4}>
                  $719
      </Text>
                </View>
              </Right>
            </View>

            <View style={styles.ViewText1}>
              {/* <Text style={styles.Text5}>
                This package allows you to post as many times as you want to the timeline within a month i.e. 30 days. You will also be granted access to a total of 3 recruiters in a month.
  </Text> */}
            </View>
            <TouchableOpacity style={[styles.BtnPay]}>
              <Text style={styles.Text6}>Pay Now ($719.00)</Text>
            </TouchableOpacity>
          </View>
          <View style={[styles.ViewText1, {marginTop: 25}]}>
              <SvgUri
                width="7px"
                height="15px"
                uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1590639365/Mobile_Assets/arrforblue_wau5kk.svg" />

              <Text style={[styles.Text5, { marginLeft: 11 }]}>
              You can only send emails to 2 recruiters per recruiter access- if a recruiter declines you get your points back. But you cant transfer points to another role- 2 email per access per role i.e. each contact a recruiter is 50pts.
              </Text>
            </View>


        </ScrollView>
        {/* modal views */}
        <Modal style={styles.MContainer} isVisible={this.state.isModalVisible}>

          <View style={styles.Mwrapper}>
            <Button onPress={() => this.toggleModal()} style={{ backgroundColor: "transparent", position: 'absolute', right: 10, top: -30, padding: 20, elevation: 0 }}>
              <SvgUri
                width="19px"
                height="19px"
                uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593711047/Mobile_Assets/closeWht_oohxti.svg" />
            </Button>
            <View style={styles.firstView} >
              {/* <Button full style={styles.actionBtn}>
                <Text style={styles.actionBtnText}>
                  View the job poster’s profile
              </Text>
              </Button> */}
              <Button
                full
                style={[styles.actionBtn, { borderTopWidth: 1, borderTopColor: "#E5E5E5", borderBottomWidth: 1, borderBottomColor: "#E5E5E5" }]}
                onPress={this.GotoMyProfile}
              >
                <Text style={styles.actionBtnText}>
                  Edit your profile
              </Text>
              </Button>
              <Button full style={styles.actionBtn}
                onPress={this.toggleModalWithdrawApp}
              >
                <Text style={[styles.actionBtnText, { color: "#FF5964" }]}>
                  Withdraw application
              </Text>
              </Button>
            </View>
            <Button style={[styles.btnCancel, { marginTop: 15 }]} title="Hide modal" onPress={() => this.toggleModal()}>
              <Text style={[styles.btnCancelText,]}>Cancel</Text>
            </Button>
          </View>
        </Modal>

        {/* edit offer*/}

        <Modal style={styles.MContainer} isVisible={this.state.isModalOfferVisible}>

          <View style={styles.Mwrapper}>
            <Button onPress={this.toggleModalOffer} style={{ backgroundColor: "transparent", position: 'absolute', right: 10, top: -30, padding: 20, elevation: 0 }}>
              <SvgUri
                width="19px"
                height="19px"
                uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593711047/Mobile_Assets/closeWht_oohxti.svg" />
            </Button>
            <View style={styles.firstView} >
              <Button full style={styles.actionBtn}>
                <Text style={styles.actionBtnText}>
                  Edit the offer
              </Text>
              </Button>
              <Button full style={styles.actionBtn}>
                <Text style={[styles.actionBtnText, { color: "#FF5964" }]}>
                  Close the offer
              </Text>
              </Button>
            </View>
            <Button style={[styles.btnCancel, { marginTop: 15 }]} title="Hide modal" onPress={this.toggleModalOffer}>
              <Text style={[styles.btnCancelText,]}>Cancel</Text>
            </Button>
          </View>
        </Modal>

        {/* edit offer*/}

        {/* Withdraw App  modal*/}
        <Modal style={styles.MContainer} isVisible={this.state.withdrawApplication}>
          <View style={styles.Mwrapper}>
            <Button onPress={this.toggleModalWithdrawApp} style={{ backgroundColor: "transparent", position: 'absolute', right: 10, top: -30, padding: 20, elevation: 0 }}>
              <SvgUri
                width="19px"
                height="19px"
                uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593711047/Mobile_Assets/closeWht_oohxti.svg" />
            </Button>
            <View style={styles.ModalView2} >
              <TouchableOpacity style={[styles.BtnPay]}
                onPress={this.toggleModalWithdrawApp}
              >
                <Text style={styles.Text6}>Pay with Card</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[styles.btnCancel, { marginTop: 15, color: 'green' }]} title="Hide modal"
                onPress={this.SubmitWithdrawal}>
                <Text style={[styles.btnCancelText,]}>{DashboardStore.isProcessing === true ? 'Processing...' : 'MyWallet ($70.00)'}</Text>
              </TouchableOpacity>
              <Button full style={styles.actionBtn2}>
                <Text style={[styles.actionBtnText, { color: "#FF5964" }]}>
                  Withdraw application
              </Text>
              </Button>

            </View>

          </View>
        </Modal>
        {/* Withdraw App modal*/}

        {/* modal views */}
      </View>
    );
  }
}
export default withNavigation(Recruiter);

const inputStyles = {
  size: 100,
  color: '#FFFFFF',
};

const height = 40;

const styles = StyleSheet.create({
  Text1: {
    fontSize: 10,
    fontWeight: '500',
    color: "#2F2F2F",
    textAlign: 'center'
  },
  Text1_2: {
    fontSize: 10,
    fontWeight: 'bold',
    color: "#2F2F2F",
    textAlign: 'center'
  },
  Text3: {
    fontSize: 12,
    fontWeight: '500',
    color: "#FAFAFA",
    textAlign: 'center'
  },
  Text7: {
    fontSize: 12,
    fontWeight: '500',
    color: "#2F2F2F",
    textAlign: 'center'
  },
  Text4: {
    fontSize: 13,
    fontWeight: '600',
    color: "#2F2F2F",
  },
  Text6: {
    fontSize: 14,
    fontWeight: '500',
    color: "#FAFAFA",
  },
  Text4_2: {
    fontSize: 12,
    fontWeight: 'normal',
    color: "#676767",
  },
  Text5: {
    fontSize: 11,
    fontWeight: '300',
    color: "#2F2F2F",
  },
  Text2: {
    fontSize: 18,
    fontWeight: '500',
    color: "#1E93E2",
  },
  likeTabsty: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  viewPerTabSty: {
    display: 'flex',
    alignItems: 'center',
    maxWidth: 79,
    textAlign: 'center',
  },
  successCard: {
    display: 'flex',
    alignItems: 'center',
    textAlign: 'center',
    justifyContent: "center",
    backgroundColor: '#F3F3F3',
    height: 100,
    // elevation: 1,
    marginTop: 30,
  },
  successCard2: {
    borderTopWidth: 2,
    borderTopColor: "#1E93E2",
    display: 'flex',
    // justifyContent: 'space-between',
    textAlign: 'center',
    backgroundColor: '#F3F3F3',
    // height: 100,
    marginTop: 17,
    paddingBottom: 12,
    paddingLeft: 18,
    paddingRight: 18,
    paddingTop: 12,
  },
  cardinnerSty: {
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    textAlign: 'center',
    justifyContent: "center",
    backgroundColor: '#1E93E2',
    height: 24,
  },
  morestyIdc: {
    display: 'flex',
    justifyContent: 'flex-end',
    alignItems: 'center',
    width: 20
  },
  moreBtnSty: {
    // width: 20,
    borderRadius: 5,
    borderWidth: 0,
    borderColor: '#1E93E2',
    display: 'flex',
    justifyContent: 'center',
    // alignItems: 'center',
  },
  xLayer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: 14,
    paddingLeft: 19,
    paddingRight: 19,
    paddingTop: 14,
  },
  xLayer2: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: 14,
    paddingLeft: 0,
    paddingRight: 0,
    paddingTop: 0,
  },
  ViewText1: {
    display: 'flex',
    flexDirection: 'row',
    paddingBottom: 14,
    paddingLeft: 0,
    paddingRight: 0,
    paddingTop: 0,
  },
  // ======================= modal style ==============================
  MContainer: {
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
  },
  Mwrapper: {
    // height: 360,
    width: '98%',
    // backgroundColor: '#fff',
    padding: 15,
    display: 'flex',
    justifyContent: 'space-around',
    // alignItems: 'center'
  },
  firstView: {
    backgroundColor: '#FAFAFA',
    borderRadius: 5
  },
  ModalView2: {
    backgroundColor: '#FAFAFA',
    borderRadius: 3,
    paddingBottom: 21,
    paddingLeft: 15,
    paddingRight: 15,
    paddingTop: 21,

  },
  actionBtn: {
    backgroundColor: '#FAFAFA',
    borderRadius: 5,
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    height: 43,
  },
  actionBtn2: {
    backgroundColor: '#FAFAFA',
    borderRadius: 5,
    elevation: 0,
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    height: 43,
  },
  checkBoxWrap: {
    borderTopWidth: 1,
    borderTopColor: '#E5E5E5',
    paddingTop: 14,
    paddingLeft: 20,
    paddingRight: 20,
    paddingBottom: 14,
    width: '90%'
    // display: 'flex',
    // justifyContent: 'center',
    // flexDirection: 'row',
    // alignItems: 'center',
    // height: 43,
  },
  actionBtnText: {
    fontSize: 12,
    color: '#1E93E2',
  },
  btnCancel: {
    backgroundColor: '#FF5964',
    borderRadius: 5,
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    height: 43,
    elevation: 0,
  },
  BtnPay: {
    backgroundColor: '#1E93E2',
    borderRadius: 3,
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: 11,
    paddingBottom: 11,
    paddingLeft: 10,
    paddingRight: 10,
    elevation: 0,
  },
  btnCancelText: {
    fontSize: 12,
    color: '#FAFAFA',
    fontWeight: 'bold',
  },
  checkBoxText: {
    fontSize: 11,
    color: '#2F2F2F',
    marginLeft: 20
  },
  CheckBoxBody: {
    display: 'flex',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10,
  },
  // ======================= modal style ==============================

});

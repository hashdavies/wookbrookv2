import React, { Component, Fragment } from 'react';
import { Text, StyleSheet, ImageBackground, Keyboard, TouchableOpacity, Alert } from 'react-native';
import { KeyboardAvoidingView, Image, ScrollView } from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons';
import Checker from '../../assets/workbrookAssets/checker.png';
import {
    Container,
    Header,
    Content,
    Textarea,
    Button,
    View,
    Thumbnail,
    Form,
    Item,
    Input,
    Label,
    Badge,
    Icon,
    Footer,
    Picker,
    Item as FormItem,
    Left,
    Body,
    CardItem,
    Card,
    Right,
    ListItem,
    List,
    CheckBox,
} from 'native-base';
import {
    Kaede,
    Hoshi,
    Jiro,
    Isao,
    Madoka,
    Akira,
    Hideo,
    Kohana,
    Makiko,
    Sae,
    Fumi,
} from 'react-native-textinput-effects';
import CustomizeHeader from '../frags/CustomizeHeader';

import { myspiner, temp, ShowNotification } from '../../dependency/UtilityFunctions';
import Logo from '../../assets/workbrookAssets/logo.png';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import RBSheet from "react-native-raw-bottom-sheet";
import Interests from '../interests/Interests';
import Noti from "../../assets/workbrookAssets/noti.png";
import UserBl from "../../assets/workbrookAssets/userBl.png";
import ArrForward from "../../assets/workbrookAssets/arrfr.png";
import Shield from "../../assets/workbrookAssets/shield.png";
import Quest from "../../assets/workbrookAssets/quest.png";
import Info from "../../assets/workbrookAssets/info.png";
import Plus from "../../assets/workbrookAssets/plus.png";
import Remark from "../../assets/workbrookAssets/remark.png";
import Like from "../../assets/workbrookAssets/like.png";
import MoreIcon from "../../assets/workbrookAssets/more2.png";
import Profile from "../../assets/images/profile/dp2.png";
import cardPix from "../../assets/images/background/bg3_old.jpg";
import RNPickerSelect from 'react-native-picker-select';
import TagInput from 'react-native-tags-input';

import RNFetchBlob from 'react-native-fetch-blob'
import ImagePicker from 'react-native-image-picker';
import CloseModal from "../../assets/workbrookAssets/whitecloss.png";
import SuccessIcon from "../../assets/workbrookAssets/successPost.png";
import Modal from 'react-native-modal';
import AccountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import { text } from 'react-native-communications';
import DashboardStore from '../../stores/Dashboard';
import { hasSetIntrest } from '../../auth';
import Autocomplete from 'react-native-autocomplete-input';
import MultiSelect from 'react-native-multiple-select';
import { YOUR_CLOUDINARY_PRESET, YOUR_CLOUDINARY_NAME, requestCameraPermission, YOUR_CLOUDINARY_PRESET_JobBanner } from '../../dependency/UtilityFunctions';



items = [{
    id: 'Ondo',
    name: 'Ondo',
}, {
    id: 'Ogun',
    name: 'Ogun',
}, {
    id: 'Calabar',
    name: 'Calabar',
}, {
    id: 'Kaduna',
    name: 'Kaduna',
}, {
    id: 'Abuja',
    name: 'Abuja',
}];
let NumberOfStaff = [{
    id: '2',
    name: '1-10',
}, {
    id: '3',
    name: '4',
}, {
    id: '21-30',
    name: '21-30',
}, {
    id: '30-50',
    name: '30-50',
}, {
    id: '50 and Above',
    name: '50 and Above',
}];
let defaultindustries = [{
    id: 'Tech',
    name: 'Tech',
}, {
    id: 'business',
    name: 'business',
}, {
    id: '16hbajsabsd',
    name: 'Calabar',
}, {
    id: 'sdhyaysdj',
    name: 'Kaduna',
}, {
    id: 'suudydjsjd',
    name: 'Abuja',
}];
@observer
class ExistingRecruiter extends Component {
    constructor(props) {
        super(props);
        this.mapRef = null;
        this.state = {
            email: '',
            password: '',
            errorloading: false,
            AmCompanyRep: true,
            selectedItems: [],
            industry: [],
            numberOfStaff: [],
            jobLevel: [],
            JobIntrests: [],
            tags: {
                tag: '',
                tagsArray: []
            },

            avatarSource: null,
            cover_pix: null,
            isImagefromdevice: false,
            loader_visible: false,
            showsaveBtn: false,
            uploading: false,
            file2upload: null,


        }
        selectedItems = [];

        this.changeAmCompanyRep = this.changeAmCompanyRep.bind(this)
        this.toggleSuccessPostModalShow = this.toggleSuccessPostModalShow.bind(this)
    }
    onSelectedItemsChange = selectedItems => {
        this.setState({ selectedItems });
    };
    async componentDidMount() {
        DashboardStore.UpdateNavigationState(this.props.navigation);

        let UserloginToken = await temp();
        // let loggedinUserdetails=await loggedinUserdetails();
        console.log(UserloginToken);
        DashboardStore._getAxiosInstance(UserloginToken);
        //  DashboardStore.__GetIndustries(1,10);
        DashboardStore.__ALLSkillWithOutFilter(1, 30);
        DashboardStore.__JobsLevels();

    }

    onSelectedItemsChange = selectedItems => {
        this.setState({ selectedItems });

    };
    changeAmCompanyRep = () => {
        const { AmCompanyRep } = this.state;
        console.log(AmCompanyRep)
        this.setState({
            AmCompanyRep: !AmCompanyRep
        })
        // this.setState({ selectedItems });

    };
    toggleSuccessPostModalShow = () => {
        DashboardStore._ToggleSucessPostModal(true)
    };
    toggleSuccessPostModalClose = () => {
        DashboardStore._ToggleSucessPostModal(false)
    };
    render() {
        // const { selectedItems } = this.state;
        const { selectedItems, JobIntrests, uploading } = this.state;
        const { isImagefromdevice, avatarSource, loader_visible, showsaveBtn } = this.state;
        let renderImage;

        if (isImagefromdevice === true) {
            renderImage = <Thumbnail square
                large
                source={avatarSource}
                style={{ height: 195, width: '100%', }}

            />
        }
        return (

            <Container style={styles.container}>
                {myspiner(DashboardStore.isProcessing)}
                <CustomizeHeader
                    leftside='cancel'
                    title='Become a recruiter'
                    rightside='empty'
                    statedColor='#000'
                    icon='md-close'
                    savehandler={this.proceednow}
                />

                <ScrollView
                    contentContainerStyle={{}}
                >
                    <View style={styles.imageHolder}>
                        <Thumbnail square
                            large
                            source={Profile}
                            style={{ height: 53, width: 51, borderRadius: 53/2 }}
                        />
                    </View>
                    <View style={styles.recruiterInfoWrap}>
                        <Text style={styles.recruiterInfoText1Wrap}>Full Name</Text>
                        <Text style={styles.recruiterInfoText2Wrap}>Jane Kuchnoska</Text>
                    </View>
                    <View style={styles.recruiterInfoWrap}>
                        <Text style={styles.recruiterInfoText1Wrap}>Email</Text>
                        <Text style={styles.recruiterInfoText2Wrap}>first@gmail.com</Text>
                    </View>
                    <View style={styles.recruiterInfoWrap}>
                        <Text style={styles.recruiterInfoText1Wrap}>Phone Number</Text>
                        <Text style={styles.recruiterInfoText2Wrap}>+234 7034 943 432</Text>
                    </View>
                    <View style={styles.recruiterInfoWrap}>
                        <Text style={styles.recruiterInfoText1Wrap}>Location</Text>
                        <Text style={styles.recruiterInfoText2Wrap}>Lagos, Nigeria</Text>
                    </View>
                    <View style={styles.recruiterInfoWrap}>
                        <Text style={styles.recruiterInfoText1Wrap}>Industry</Text>
                        <Text style={styles.recruiterInfoText2Wrap}>Marketing & Communications</Text>
                    </View>

                    <View style={styles.bottomIndBorder}>
                        <MultiSelect
                            hideTags
                            items={NumberOfStaff}
                            uniqueKey="id"
                            ref={(component) => { this.multiSelect = component }}
                            // onSelectedItemsChange={this.onSelectedItemsChange}
                            onSelectedItemsChange={(numberOfStaff) => {
                                // alert("hey man")
                                console.log(numberOfStaff)
                                this.setState({
                                    numberOfStaff
                                })
                                // console.log()
                                let item = numberOfStaff[0];
                                DashboardStore._updateParameterV2(item, "numberOfStaff", "JobPosting");

                            }
                            }

                            selectedItems={this.state.numberOfStaff}
                            selectText="Industry recruitment experience"
                            searchInputPlaceholderText="Search Number of staff..."
                            onChangeInput={(text) => console.log(text)}
                            tagRemoveIconColor="#FF5964"
                            tagBorderColor="#CCC"
                            tagTextColor="#CCC"
                            selectedItemTextColor="#CCC"
                            selectedItemIconColor="#CCC"
                            itemTextColor="#fff"
                            textColor="#A7A7A7"
                            displayKey="name"
                            searchInputStyle={{ color: '#CCC', borderWidth: 0, borderColor: "#ccc", fontWeight: "300" }}
                            styleItemsContainer={{ borderWidth: 1, borderColor: '#1E93E2', backgroundColor: "#1E93E2" }}
                            submitButtonColor="#CCC"
                            submitButtonText="Submit"
                            itemFontSize={10}
                            fontSize={10}
                            styleDropdownMenuSubsection={{ borderBottomWidth: 0,  backgroundColor: 'transparent', height: 35, paddingLeft: 5, paddingBottom: 0, marginBottom: -20, fontSize: 10, fontWeight: "300" }}
                            selectedItemIconColor={'#fff'}

                            hideSubmitButton={true}
                            single={true}
                        />
                    </View>

                    <Item style={[styles.bottomIndBorder, { height: 55 }]}>
                        <Input
                            placeholder="Recruiter profile summary (100 words)"
                            placeholderLabel
                            placeholderTextColor="#A7A7A7"
                            style={styles.cardTextJob3}

                            defaultValue={DashboardStore.JobPosting.aboutCompany}
                            onChangeText={(aboutCompany) => DashboardStore.onChangeText('aboutCompany', aboutCompany, 'JobPosting')}

                        />

                    </Item>
                    <Item style={[styles.bottomIndBorder, { height: 55 }]}>
                        <Input
                            placeholder="LinkedIn URL (optional)"
                            placeholderLabel
                            placeholderTextColor="#A7A7A7"
                            style={styles.cardTextJob3}
                            defaultValue={DashboardStore.JobPosting.roleTitle}
                            onChangeText={(roleTitle) => DashboardStore.onChangeText('roleTitle', roleTitle, 'JobPosting')}
                        />
                    </Item>

                    <View style={[styles.bottomIndBorderNon, { marginTop: 30 }]}>
                        <Left style={{

                            display: 'flex',
                            flexDirection: 'row',
                            alignItems: 'center',
                        }}>
                            <CheckBox
                                checked={this.state.AmCompanyRep}
                                onPress={this.changeAmCompanyRep}
                            />
                            <Body style={[{ marginLeft: 10 }]}>
                                <Text style={[styles.cardTextJob1, { marginLeft: 10 }]}>
                                    I am not bound by any contractual terms from any company, recruitment agency or brand.
                                </Text>
                            </Body>
                        </Left>
                    </View>
                    <Button transparent style={styles.termConditionWrap}>
                        <Text style={[styles.cardTextJob2_2, { marginRight: 2 }]}>
                            By clicking “become a recruiter’ you are agreeing to our
                        </Text>
                        <Text style={[styles.cardTextJob2_2, { color: '#1E93E2' }]}>
                            Terms & Conditions
                        </Text>
                    </Button>
                    <Button
                        full
                        style={[styles.btnNewPost, { backgroundColor: '#1E93E2', elevation: 0, marginTop: 4, marginBottom: 20 }]}
                        onPress={() => { this.props.navigation.navigate("MyOffer") }}
                    >
                        <Text style={styles.cardTextJob4_1}>
                            Become a recruiter
                                </Text>
                    </Button>

                </ScrollView>

                <Modal style={styles.MContainer} isVisible={DashboardStore.isSuccessPostModalVisible} >
                    <Button onPress={this.toggleSuccessPostModalClose} style={{ backgroundColor: "transparent", position: 'absolute', right: 10, top: 40, padding: 20, elevation: 0 }}>
                        <Image source={CloseModal} style={{ width: 19, height: 19, }} />
                    </Button>
                    <View style={[styles.Mwrapper, { height: 380 }]}>
                        <View style={{ paddingLeft: 29, paddingRight: 29, display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                            <Image source={SuccessIcon} style={{ width: 129, height: 129, marginBottom: 17 }} />
                            <Text style={styles.cardText1, { fontSize: 16, color: '#5ABC7A', textAlign: 'center', fontWeight: '600' }}>
                                Congratulations!
                                        </Text>
                            <Text style={styles.cardText1, { fontSize: 16, color: '#5ABC7A', textAlign: 'center', fontWeight: '600' }}>

                                Your job was successfully posted.
                            </Text>
                        </View>

                    </View>
                </Modal>

            </Container>


        );
    }
}
export default ExistingRecruiter;

const styles = StyleSheet.create({
    container: {
        backgroundColor: "white",
        display: "flex",
        flex: 1,
        justifyContent: "flex-start",
    },
    imageHolder: {
        display: "flex",
        alignItems: "flex-start",
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 15,
        paddingBottom: 25

    },
    recruiterInfoWrap: {
        display: "flex",
        alignItems: "flex-start",
        flexDirection: 'row',
        paddingLeft: 15,
        paddingRight: 15,
        marginTop: 10
    },
    recruiterInfoText1Wrap: {
        display: "flex",
        alignItems: "flex-start",
        flexDirection: 'row',
        color: '#A7A7A7',
        fontSize: 10,
        fontWeight: '300',
        width: 100,
        paddingLeft: 5,
    },
    recruiterInfoText2Wrap: {
        flex: 1,
        display: "flex",
        alignItems: "flex-start",
        flexDirection: 'row',
        paddingLeft: 5,
        // paddingRight: 15,
        color: '#3C3C3C',
        fontSize: 12,
        fontWeight: 'normal',
    },
    btnNewPost: {
        height: 38,
        elevation: 0,
        minWidth: '90%',
        marginLeft: 15,
        marginRight: 15,
    },
    IconSetsty1: {
        maxWidth: 16,
        maxHeight: 17,
    },
    IconSetsty: {
        maxWidth: 16,
        maxHeight: 16,
    },
    iconWrapSet: {
        width: 20,
        marginRight: 10
    },
    IconSetsty2: {
        width: 8,
        height: 15,

    },
    bottomIndBorderNon: {
        marginLeft: 15,
        marginRight: 15,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        // borderBottomWidth: 1,
        // borderColor: '#E5E5E5'
    },
    bottomIndBorder: {
        marginLeft: 15,
        marginRight: 15,
        borderBottomWidth: 1,
        borderColor: '#E5E5E5',
    },
    rmPadding: {
        paddingTop: 0,
        paddingRight: 0,
        paddingBottom: 0,
        paddingLeft: 0,
    },
    rmPaddingLR: {
        paddingLeft: 0,
        paddingRight: 0,
    },

    cardTextJob1: {
        color: "#2F2F2F",
        fontSize: 11,
    },
    cardTextJob2: {
        color: "#676767",
        fontSize: 9,
        fontWeight: '300',
    },
    cardTextJob2_2: {
        color: "#2F2F2F",
        fontSize: 9,
        fontWeight: '500',
    },
    cardTextJob3: {
        color: "#2F2F2F",
        fontSize: 11,
        fontWeight: '300',
        marginBottom: -20,
    },
    cardTextJob4: {
        color: "#1E93E2",
        fontSize: 11,
        fontWeight: '500',
    },
    cardTextJob4_1: {
        color: "#fff",
        fontSize: 12,
        fontWeight: '500',
    },
    cardTextJob5: {
        color: "#1E93E2",
        fontSize: 13,
        fontWeight: '600',
    },
    cardTextJob6: {
        color: "#1E93E2",
        fontSize: 11,
        fontWeight: '500',
    },
    cardTextJob7: {
        fontSize: 12,
        color: '#2F2F2F',
    },
    Mwrapper: {
        height: 510,
        width: '90%',
        backgroundColor: '#fff',
        // padding: 15,
        display: 'flex',
        justifyContent: 'space-around',
        // alignItems: 'center'
    },
    cardText1: {
        color: "#2F2F2F",
        fontWeight: '500',
        fontSize: 12,
    },
    MContainer: {
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
    },
    termConditionWrap: {
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
    },


});

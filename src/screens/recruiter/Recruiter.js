import React, { Component } from 'react';
import {
  Text,
  Image,
  StyleSheet,
  ImageBackground,
  Keyboard,
  TouchableOpacity
} from 'react-native';
import { KeyboardAvoidingView } from 'react-native';
import CustomizeHeader from '../frags/CustomizeHeader';

import Ionicons from 'react-native-vector-icons/Ionicons';
import {
  Container,
  Header,
  Content,
  Textarea,
  Button,
  View,
  Thumbnail,
  Form,
  Item,
  Input,
  Label,
  Badge,
  Icon,
  Footer,
  Picker,
  Left,
  Right,
  Body,
  Title,
  Center,
  CardItem
} from 'native-base';

import { myspiner } from '../../dependency/UtilityFunctions';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import User from "../../assets/workbrookAssets/user4.png";

import accountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import { ScrollView } from 'react-native-gesture-handler';
import imageUser from '../../assets/workbrookAssets/user2.png';
import RecruitImage from '../../assets/workbrookAssets/recui.png';
import DatePicker from 'react-native-datepicker'
// import Autocomplete from 'native-base-autocomplete'
import Autocomplete from 'react-native-autocomplete-input';

const API = 'https://swapi.co/api';
@observer
class Recruiter extends Component {
  constructor(props) {
    super(props);
    this.state = {

    }
  }

  componentDidMount() {
  }

  render() {

    return (
      <Container style={[{ height: '100%', width: '100%' }]}>
        <ScrollView
          style={[{ flex: 1 }]}
          showsVerticalScrollIndicator={false}
          showsHorizontalScrollIndicator={false}
        >
          {/* <CustomizeHeader
            leftside='back'
            title='Insights'
            rightside='empty'
            statedColor='#1E93E2'
            icon='md-close'
          /> */}
          <Container style={[styles.profileWrapinner, { backgroundColor: "#FFF" }]}>
            <View style={styles.layer1}>
              <Text style={styles.Text1}>Hire a recruiter</Text>
              <Text style={styles.Text2}>We make the recruitment process easier</Text>
            </View>

            <Image source={RecruitImage} style={{ height: 292, width: 292, }} />

            <View style={[styles.layer1, { backgroundColor: '#FFF' }]}>
              <Button full style={styles.TakemeBtn}
               onPress={() => { this.props.navigation.navigate("RecruiterForm") }}
              >
                <Text style={styles.Text3}>Take me to recruiters</Text>
              </Button>
              <Button full transparent style={[styles.dothislaterBtn, { marginTop: 9, }]}
                
                onPress={() => { this.props.navigation.navigate("RecruiterProfile") }}
              >
                <Text style={[styles.Text3, { color: '#1E93E2' }]}>I'll do this later</Text>
              </Button>
            </View>
          </Container>

        </ScrollView>
      </Container>
    );
  }
}
export default Recruiter;

const inputStyles = {
  size: 100,
  color: '#FFFFFF',
};

const height = 40;

const styles = StyleSheet.create({
  Text1: {
    fontSize: 40,
    fontWeight: '600',
    color: "#1E93E2",
  },
  Text2: {
    fontSize: 12,
    fontWeight: 'normal',
    color: "#1E93E2",
  },
  Text3: {
    fontSize: 14,
    fontWeight: 'normal',
    color: "#FAFAFA",
  },
  profileWrapinner: {
    width: '100%',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingTop: 65,
    paddingLeft: 34,
    paddingRight: 34,
    paddingBottom: 25,
    minHeight: '95%',
  },
  layer1: {
    display: 'flex',
    // justifyContent: 'center',
    alignItems: 'center',
    width: '100%'
  },
  TakemeBtn: {
    height: 56,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#5ABC7A',
    borderRadius: 5,
    elevation: 0,
  },
  dothislaterBtn: {
    height: 17,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    elevation: 0,
  },



});

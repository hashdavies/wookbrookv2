import React, { Component, Fragment } from 'react';
import { Text, StyleSheet, ImageBackground, Keyboard, TouchableOpacity, Alert, FlatList, ActivityIndicator } from 'react-native';
import { KeyboardAvoidingView, Image, ScrollView } from 'react-native';
import { StackActions } from 'react-navigation';

import Ionicons from 'react-native-vector-icons/Ionicons';
import Checker from '../../assets/workbrookAssets/checker.png';
import {
    Container,
    Header,
    Content,
    Textarea,
    Button,
    View,
    Thumbnail,
    Form,
    Item,
    Input,
    Label,
    Badge,
    Icon,
    Footer,
    Picker,
    Item as FormItem,
    Left,
    Body,
    CardItem,
    Card,
    Right,
    ListItem,
    List,
    CheckBox,
} from 'native-base';
import {
    Kaede,
    Hoshi,
    Jiro,
    Isao,
    Madoka,
    Akira,
    Hideo,
    Kohana,
    Makiko,
    Sae,
    Fumi,
} from 'react-native-textinput-effects';
import CustomizeHeader from '../frags/CustomizeHeader';

import { myspiner, temp, ShowNotification, AppInlineLoader, GetPersistData } from '../../dependency/UtilityFunctions';
import Logo from '../../assets/workbrookAssets/logo.png';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import RBSheet from "react-native-raw-bottom-sheet";
import Interests from '../interests/Interests';
import Noti from "../../assets/workbrookAssets/noti.png";
import UserBl from "../../assets/workbrookAssets/userBl.png";
import ArrForward from "../../assets/workbrookAssets/arrfr.png";
import Shield from "../../assets/workbrookAssets/shield.png";
import Quest from "../../assets/workbrookAssets/quest.png";
import Info from "../../assets/workbrookAssets/info.png";
import Plus from "../../assets/workbrookAssets/plus.png";
import Remark from "../../assets/workbrookAssets/remark.png";
import Like from "../../assets/workbrookAssets/like.png";
import MoreIcon from "../../assets/workbrookAssets/more2.png";
import Profile from "../../assets/images/profile/dp2.png";
import cardPix from "../../assets/images/background/bg3_old.jpg";
import RNPickerSelect from 'react-native-picker-select';
import TagInput from 'react-native-tags-input';

import RNFetchBlob from 'react-native-fetch-blob'
import ImagePicker from 'react-native-image-picker';
import CloseModal from "../../assets/workbrookAssets/whitecloss.png";
import SuccessIcon from "../../assets/workbrookAssets/successPost.png";
import Modal from 'react-native-modal';
import AccountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import { text } from 'react-native-communications';
import DashboardStore from '../../stores/Dashboard';
import { hasSetIntrest } from '../../auth';
import Autocomplete from 'react-native-autocomplete-input';
import MultiSelect from 'react-native-multiple-select';
import { YOUR_CLOUDINARY_PRESET, YOUR_CLOUDINARY_NAME, requestCameraPermission, YOUR_CLOUDINARY_PRESET_JobBanner } from '../../dependency/UtilityFunctions';
import EmptyListComponent from '../../StatelessScreens/EmptyListComponent';
import { SvgUri } from 'react-native-svg';
import RecruiterLanding from './RecruiterLanding';


items = [{
    id: '92iijs7yta',
    name: 'Ondo',
}, {
    id: 'a0s0a8ssbsd',
    name: 'Ogun',
}, {
    id: '16hbajsabsd',
    name: 'Calabar',
}, {
    id: 'sdhyaysdj',
    name: 'Kaduna',
}, {
    id: 'suudydjsjd',
    name: 'Abuja',
}];

@observer
class RecruiterAvailable extends Component {
    constructor(props) {
        super(props);
        this.mapRef = null;
        this.state = {
            email: '',
            password: '',
            requestMode: ''

        }
        selectedItems = [];
this.CheckRecruiterFullDetails=this.CheckRecruiterFullDetails.bind(this)
this.Post2RecruiterTimeline=this.Post2RecruiterTimeline.bind(this)
this.HireMoreRecruiters=this.HireMoreRecruiters.bind(this)
this.SearchRecruiterAvailable=this.SearchRecruiterAvailable.bind(this)
 
    }

    SearchRecruiterAvailable = (query) => {
        // alert("hey");
        let stringlenght = query.length;
        if (stringlenght < 4) {
            DashboardStore.__GetRecruiters(1, 30, false);

            return;
        }
        DashboardStore.__SearchRecuiterAvailable(query)
    }

    onSelectedItemsChange = selectedItems => {
        this.setState({ selectedItems });
    };
    CheckRecruiterFullDetails = (item, page) => {

        DashboardStore._updateParameterV2(item, "SingleRecruiter", "Recruiters");
        this.props.navigation.navigate(page);

    };
    toggleSuccessPostModalClose = () => {




        const { requestMode } = this.state;
        if (requestMode === "HireNewRecruiter") {
            DashboardStore._ToggleSucessPostModal(false)
            this.props.navigation.goBack();
            DashboardStore.__GetJobOffers(1, 30);
        }
        else {
            DashboardStore._ToggleSucessPostModal(false)
            this.props.navigation.dispatch(StackActions.popToTop());
        }




        // const popAction = StackActions.pop({
        //     n: 0,
        //   });

        //   this.props.navigation.dispatch(popAction);
        DashboardStore._UpdateDefaultValue();

    };
    async componentDidMount() {
        let IsTermAccepted = await GetPersistData("TermANdConditionAccepted");
        DashboardStore._updateParameterV2(IsTermAccepted, "RecruiterAvailable", "RedirectMode");
        DashboardStore.JobPosting.SelectedRecuiter = [];
        // console.log(IsTermAccepted);
        // console.log("IsTermAccepted");
        //   alert(IsTermAccepted);
        // if(){

        // }

        let UserloginToken = await temp();
        // let loggedinUserdetails=await loggedinUserdetails();
        let data = this.props.navigation.getParam('data', '');

        this.setState({
            requestMode: data
        })




        console.log(UserloginToken);
        DashboardStore._getAxiosInstance(UserloginToken);
        console.log("UserloginToken");
        DashboardStore.__GetRecruiters(1, 30);

    }
    Post2RecruiterTimeline = () => {

        // const { JobIntrests, AmCompanyRep, showsaveBtn } = this.state;
        const { JobPosting } = DashboardStore
        let This_selectedRecruiters = DashboardStore.JobPosting.SelectedRecuiter.slice();
        console.log(This_selectedRecruiters);
        // if(showsaveBtn===false){
        //        let message="Please choose job banner";
        //             ShowNotification(message, true);
        // }
        // else{
        //   let rr= await  this.cloudinaryUpload();
        //    console.log("proceed")
        // }

        // const{JobPosting}=DashboardStore
        //     let companyName=JobPosting.companyName;
        //    let  industry=JobPosting.industry;
        //     let aboutCompany=JobPosting.aboutCompany;
        //    let  location=JobPosting.location;
        //     let numberOfStaff=JobPosting.numberOfStaff;
        //    let  roleTitle=JobPosting.roleTitle;
        //    let  jobLevel=JobPosting.jobLevel;
        //   let   wordForApplicant=JobPosting.wordForApplicant;


        //    "additional_files":["Link1","Link1","Link1"],
        //   "isCompanyRepresentative":1,
        //   "isToRefTimeLineOrToHireRecruter":2

        if (This_selectedRecruiters.length < 2) {
            let message = "Please select up to 2 Recruiters";
            ShowNotification(message, true);
        }
        else if (This_selectedRecruiters.length > 2) {
            let message = "You can only Select 2 Recruiters";
            ShowNotification(message, true);
        }
        // else if(industry===''){
        //     let message="Company  industry is required";
        //     ShowNotification(message, true);
        //  }
        // else if(aboutCompany===''){
        //     let message="Tell us briefly about the company";
        //     ShowNotification(message, true);
        //  }
        // else if(location===''){
        //     let message="Job location is required";
        //     ShowNotification(message, true);
        //  }
        // else if(numberOfStaff===''){
        //     let message="Number of staff is required";
        //     ShowNotification(message, true);
        //  }
        // else if(roleTitle===''){
        //     let message="Number of staff is required";
        //     ShowNotification(message, true);
        //  }
        // else if(jobLevel===''){
        //     let message="Job Entry level is required";
        //     ShowNotification(message, true);
        //  }
        // else if(wordForApplicant===''){
        //     let message="Tell us about the application";
        //     ShowNotification(message, true);
        //  }
        else {


            let isCompanyRepresentative = 1;
            // DashboardStore._updateParameterV2(JobIntrests, "intrest", "JobPosting");
            DashboardStore._updateParameterV2(isCompanyRepresentative, "isCompanyRepresentative", "JobPosting");




            console.log(JobPosting);
            DashboardStore.__ProccessJobPosting2Recruiters();
        }

    };
    HireMoreRecruiters = () => {
        //          const { JobOffers } = DashboardStore;
        // console.log(JobOffers);
        // console.log("JobOffers");
        //  return;
        let This_selectedRecruiters = DashboardStore.JobPosting.SelectedRecuiter.slice();
        console.log(This_selectedRecruiters);


        if (This_selectedRecruiters.length < 2) {
            let message = "Please select up to 2 Recruiters";
            ShowNotification(message, true);
        }
        else if (This_selectedRecruiters.length > 2) {
            let message = "You can only Select 2 Recruiters";
            ShowNotification(message, true);
        }
        // else if(industry===''){
        //     let message="Company  industry is required";
        //     ShowNotification(message, true);
        //  }
        // else if(aboutCompany===''){
        //     let message="Tell us briefly about the company";
        //     ShowNotification(message, true);
        //  }
        // else if(location===''){
        //     let message="Job location is required";
        //     ShowNotification(message, true);
        //  }
        // else if(numberOfStaff===''){
        //     let message="Number of staff is required";
        //     ShowNotification(message, true);
        //  }
        // else if(roleTitle===''){
        //     let message="Number of staff is required";
        //     ShowNotification(message, true);
        //  }
        // else if(jobLevel===''){
        //     let message="Job Entry level is required";
        //     ShowNotification(message, true);
        //  }
        // else if(wordForApplicant===''){
        //     let message="Tell us about the application";
        //     ShowNotification(message, true);
        //  }
        else {


            // let isCompanyRepresentative = 1;
            // DashboardStore._updateParameterV2(JobIntrests, "intrest", "JobPosting");
            // DashboardStore._updateParameterV2(isCompanyRepresentative, "isCompanyRepresentative", "JobPosting");




            // console.log(JobPosting);
            DashboardStore.__HireMoreRecruiter();
        }

    };
    render() {
        const { selectedItems, requestMode } = this.state;
        const { isGoBack, isProcessing, RedirectMode, JobRecruiters } = DashboardStore;

        // if(isGoBack===true){
        //     // this.props.navigation.goBack();
        //     const popAction = StackActions.pop({
        //         n: 1,
        //       });

        //       this.props.navigation.dispatch(popAction);
        //     DashboardStore._UpdateDefaultValue();
        // }

        let ErrorComponentDisplay = null;
        console.log(DashboardStore.JobPosting);

        if (DashboardStore.isProcessing === true) {
            ErrorComponentDisplay = AppInlineLoader(DashboardStore.isProcessing)

        }
        else {
            ErrorComponentDisplay = <EmptyListComponent
                message={DashboardStore.Recruiters.ErrorMessage}
                onclickhandler={this.LoadAvailableProffesions}
            />
        }
        let AvailableRecruiters = DashboardStore.Recruiters.AvailableRecruiters.slice();
        let This_selectedRecruiters = DashboardStore.JobPosting.SelectedRecuiter.slice();

        let AllRecruiters =
            <FlatList
                // Data
                data={AvailableRecruiters}
                // Render Items
                renderItem={({ item }) => {
                    console.log(This_selectedRecruiters);
                    console.log("This_selectedRecruiters");
                    return (

                        <List style={{ borderBottomWidth: 1, borderBottomColor: '#E5E5E5', paddingBottom: 15 }}>
                            <ListItem avatar style={{ display: 'flex', justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center', }}>
                                <Left>
                                    {
                                        item.profilePicture === "" ?
                                            <Thumbnail source={Profile} />
                                            :
                                            <Thumbnail source={{ uri: item.profilePicture }} />
                                    }
                                </Left>
                                <Body style={{ borderBottomWidth: 0, }}>
                                    <Text style={styles.cardTextJob4}>{item.fullname}</Text>
                                    <Text style={styles.cardTextJob5} numberOfLines={3} >{item.summary}</Text>
                                </Body>
                                <Right style={[styles.btnStyIdc, { borderBottomWidth: 0 }]}>
                                    <Button transparent style={styles.ViewBtnSty}
                                        // onPress={() => { this.props.navigation.navigate("RecruiterProfile") }}
                                        onPress={() => { this.CheckRecruiterFullDetails(item, "RecruiterProfile") }}
                                    >
                                        <Text style={[styles.cardTextJob5, { color: '#1E93E2' }]}>VIEW</Text>

                                    </Button>
                                    {
                                        This_selectedRecruiters.includes(item.id) === false ? <Fragment></Fragment> : <CheckBox
                                            style={{ marginTop: 2, marginLeft: -20 }}
                                            checked={true}
                                        />

                                    }

                                </Right>



                            </ListItem>
                        </List>
                    )
                }}
                // Item Key
                keyExtractor={(item, index) => String(index)}
            // Header (Title)
            // ListHeaderComponent={this.renderHeader}
            // Footer (Activity Indicator)
            // ListFooterComponent={this.renderFooter()}
            // On End Reached (Takes a function)
            // onEndReached={this.retrieveMore}
            // How Close To The End Of List Until Next Data Request Is Made
            // onEndReachedThreshold={0}
            // Refreshing (Set To True When End Reached)
            // refreshing={this.state.refreshing}
            />
        return (
            RedirectMode.RecruiterAvailable !== true ?
                <RecruiterLanding />
                :
                <Container style={styles.container}>
                    <CustomizeHeader
                        leftside='WhiteArr'
                        title={This_selectedRecruiters.length > 0 ? `${This_selectedRecruiters.length} Recruiter(s) Selected` : 'Available Recruiters'}
                        rightside='empty'
                        statedColor='#000'
                        icon='md-close'
                    // savehandler={this.proceednow}
                    />

                    <ScrollView
                        contentContainerStyle={{

                        }}
                    >
                        <View style={styles.headerWrap}>
                            <Text style={styles.cardTextJob1}>Recruiters
                        {/* {JSON.stringify( DashboardStore.Recruiters.AvailableRecruiters)} */}

                            </Text>
                            {/* <Button transparent style={{ display: 'flex', flexDirection: 'row', justifyContent: 'flex-start', }}
                         onPress={() => { this.props.navigation.navigate("RecruiterSearch") }}
                        >
                            <Text style={styles.cardTextJob3}>in Lagos, State   </Text>
                            <Text style={[styles.cardTextJob3, { color: '#1E93E2' }]}>Change Filters</Text>
                        </Button> */}
                        </View>
                        <View style={{ paddingLeft: 15, paddingRight: 15 }}>
                            {/* <Label style={styles.cardTextJob2}>Experience</Label> */}
                            <Item regular style={styles.inputStyleWrap}>

                                <Input
                                    placeholder="Search by (name,location )"
                                    onChangeText={(query) => this.SearchRecruiterAvailable(query)}
 
                                />
                            </Item>
                        </View>

                        {
                            DashboardStore.Recruiters.AvailableRecruiters.length > 0 ?
                                AllRecruiters
                                :
                                ErrorComponentDisplay

                        }


                        {/* <List style={{ borderBottomWidth: 1, borderBottomColor: '#E5E5E5', paddingBottom: 15 }}>
                        <ListItem avatar style={{ display: 'flex', justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center', }}>
                            <Left>
                                <Thumbnail source={Profile} />
                            </Left>
                            <Body style={{ borderBottomWidth: 0, }}>
                                <Text style={styles.cardTextJob4}>Kumar Pratik</Text>
                                <Text style={styles.cardTextJob5} numberOfLines={1} >Doing what you like will always keep you happy . . .</Text>
                            </Body>
                            <Right style={[styles.btnStyIdc, { borderBottomWidth: 0 }]}>
                                <Button transparent style={styles.ViewBtnSty}
                                onPress={() => { this.props.navigation.navigate("RecruiterProfile") }}
                                >
                                    <Text style={[styles.cardTextJob5, { color: '#1E93E2' }]}>VIEW</Text>
                                </Button>
                            </Right>
                        </ListItem>
                    </List> */}

                        {/* <List style={{ borderBottomWidth: 1, borderBottomColor: '#E5E5E5', paddingBottom: 15 }}>
                        <ListItem avatar style={{ display: 'flex', justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center', }}>
                            <Left>
                                <Thumbnail source={Profile} />
                            </Left>
                            <Body style={{ borderBottomWidth: 0, }}>
                                <Text style={styles.cardTextJob4}>Kumar Pratik</Text>
                                <Text style={styles.cardTextJob5}>Doing what you like will always keep you happy . . .</Text>
                            </Body>
                            <Right style={[styles.btnStyIdc, { borderBottomWidth: 0 }]}>
                                <Button transparent style={styles.ViewBtnSty}
                                onPress={() => { this.props.navigation.navigate("RecruiterProfile") }}
                                >
                                    <Text style={[styles.cardTextJob5, { color: '#1E93E2' }]}>VIEW</Text>
                                </Button>
                            </Right>
                        </ListItem>
                    </List>
                  */}
                        {requestMode === 'HireNewRecruiter' ?
                            <Button transparent full style={styles.serachbtnSty}
                                onPress={this.HireMoreRecruiters}
                            >
                                <Text style={styles.cardTextJob2_3}>

                                    {isProcessing === true ? 'Processing...' : '  Hire Recruiter'}
                                </Text>
                            </Button>
                            :
                            <Button transparent full style={styles.serachbtnSty}
                                onPress={this.Post2RecruiterTimeline}
                            >
                                <Text style={styles.cardTextJob2_3}> {isProcessing === true ? 'Processing...' : 'Submit Job'}   </Text>
                            </Button>

                        }

                    </ScrollView>


                    <Modal style={styles.MContainer} isVisible={DashboardStore.isSuccessPostModalVisible} >
                    {/* <Modal style={styles.MContainer} isVisible={true} > */}
                        <Button onPress={this.toggleSuccessPostModalClose} style={{ backgroundColor: "transparent", position: 'absolute', right: 10, top: 40, padding: 20, elevation: 0 }}>
                            <SvgUri
                                width="19px"
                                height="19px"
                                uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1594146750/Mobile_Assets/close_white_iocsmo.svg"
                            />
                        </Button>
                        <View style={[styles.Mwrapper, { height: 350 }]}>
                            <View style={{ paddingLeft: 29, paddingRight: 29, display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                                <SvgUri
                                    width="129px"
                                    height="129px"
                                    uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1589394608/Mobile_Assets/thankyou_l8tqh1.svg"
                                />
                                {/* <Text style={styles.cardText1, { fontSize: 16, color: '#5ABC7A', textAlign: 'center', fontWeight: '600', marginTop: 17 }}>
                                Congratulations!
                                        </Text> */}
                                <Text style={styles.cardText1, { fontSize: 16, color: '#5ABC7A', textAlign: 'center', fontWeight: '600', marginTop: 12 }}>

                                    {/* Your job was successfully posted. */}
                                    {DashboardStore.JobpostedMessage}
                                </Text>
                            </View>

                        </View>
                    </Modal>

                </Container>


        );
    }
}
export default RecruiterAvailable;

const styles = StyleSheet.create({
    MContainer: {
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
    },
    container: {
        backgroundColor: "white",
        display: "flex",
        flex: 1,
        justifyContent: "flex-start",
    },

    rmPadding: {
        paddingTop: 0,
        paddingRight: 0,
        paddingBottom: 0,
        paddingLeft: 0,
    },
    rmPaddingLR: {
        paddingLeft: 0,
        paddingRight: 0,
    },

    cardTextJob1: {
        color: "#2F2F2F",
        fontSize: 36,
        fontWeight: '600',

    },
    cardTextJob2: {
        color: "#BCBCBC",
        fontSize: 14,
        fontWeight: '600',
        marginBottom: 5,
    },
    cardTextJob2_2: {
        color: "#fff",
        fontSize: 14,
        fontWeight: '600',
    },
    cardTextJob3: {
        color: "#2F2F2F",
        fontSize: 13,
        fontWeight: '500',
    },
    cardTextJob4: {
        fontSize: 12,
        color: '#2F2F2F',
        fontWeight: '600'
    },

    cardTextJob5: {
        color: "#2F2F2F",
        fontSize: 10,
        fontWeight: '500',
    },
    cardTextJob7: {
        color: "#1E93E2",
        fontSize: 11,
        fontWeight: '500',
    },
    cardTextJob4_1: {
        color: "#fff",
        fontSize: 12,
        fontWeight: '500',
    },

    cardTextJob6: {
        color: "#1E93E2",
        fontSize: 11,
        fontWeight: '500',
    },


    cardText1: {
        color: "#2F2F2F",
        fontWeight: '500',
        fontSize: 12,
    },
    inputStyleWrap: {
        height: 38,
        backgroundColor: '#E5E5E5',
        borderRadius: 5,
    },
    headerWrap: {
        marginBottom: 13,
        marginTop: 20,
        paddingLeft: 25,
        paddingRight: 25,
    },
    serachbtnSty: {
        height: 54,
        borderRadius: 4,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#5ABC7A',
        marginTop: 40,
        marginLeft: 20,
        marginRight: 20,
        marginBottom: 15,
    },
    ViewBtnSty: {
        // height: 23,
        width: 50,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#1E93E2',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    btnStyIdc: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    btnStyIddc2: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    Mwrapper: {
        height: 510,
        width: '90%',
        backgroundColor: '#fff',
        // padding: 15,
        display: 'flex',
        justifyContent: 'space-around',
        // alignItems: 'center'
    },

});

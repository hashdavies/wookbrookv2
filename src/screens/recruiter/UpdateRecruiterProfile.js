import React, { Component, Fragment } from 'react';
import { Text, StyleSheet, ImageBackground, Keyboard, TouchableOpacity, Alert } from 'react-native';
import { KeyboardAvoidingView, Image, ScrollView } from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons';
import Checker from '../../assets/workbrookAssets/checker.png';
import {
    Container,
    Header,
    Content,
    Textarea,
    Button,
    View,
    Thumbnail,
    Form,
    Item,
    Input,
    Label,
    Badge,
    Icon,
    Footer,
    Picker,
    Item as FormItem,
    Left,
    Body,
    CardItem,
    Card,
    Right,
    ListItem,
    List,
    CheckBox,
} from 'native-base';
import {
    Kaede,
    Hoshi,
    Jiro,
    Isao,
    Madoka,
    Akira,
    Hideo,
    Kohana,
    Makiko,
    Sae,
    Fumi,
} from 'react-native-textinput-effects';
import CustomizeHeader from '../frags/CustomizeHeader';

import { myspiner, temp, ShowNotification, cloudinaryUpload, getLodgedInDetailsProfile } from '../../dependency/UtilityFunctions';
import Logo from '../../assets/workbrookAssets/logo.png';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import RBSheet from "react-native-raw-bottom-sheet";
import Interests from '../interests/Interests';
import Noti from "../../assets/workbrookAssets/noti.png";
import UserBl from "../../assets/workbrookAssets/userBl.png";
import ArrForward from "../../assets/workbrookAssets/arrfr.png";
import Shield from "../../assets/workbrookAssets/shield.png";
import Quest from "../../assets/workbrookAssets/quest.png";
import Info from "../../assets/workbrookAssets/info.png";
import Plus from "../../assets/workbrookAssets/plus.png";
import Remark from "../../assets/workbrookAssets/remark.png";
import Like from "../../assets/workbrookAssets/like.png";
import MoreIcon from "../../assets/workbrookAssets/more2.png";
import Profile from "../../assets/images/profile/dp2.png";
import cardPix from "../../assets/images/background/bg3_old.jpg";
import RNPickerSelect from 'react-native-picker-select';
import TagInput from 'react-native-tags-input';

import RNFetchBlob from 'react-native-fetch-blob'
import ImagePicker from 'react-native-image-picker';
import CloseModal from "../../assets/workbrookAssets/whitecloss.png";
import SuccessIcon from "../../assets/workbrookAssets/successPost.png";
import Modal from 'react-native-modal';
import AccountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import { text } from 'react-native-communications';
import DashboardStore from '../../stores/Dashboard';
import { hasSetIntrest } from '../../auth';
import Autocomplete from 'react-native-autocomplete-input';
import MultiSelect from 'react-native-multiple-select';
import { YOUR_CLOUDINARY_PRESET, YOUR_CLOUDINARY_NAME, requestCameraPermission, YOUR_CLOUDINARY_PRESET_JobBanner } from '../../dependency/UtilityFunctions';
import { SvgUri } from 'react-native-svg';
import GooglePlacesInput from '../frags/GooglePlacesInput';
// import CountryCurrencyPicker from 'react-native-country-currency-picker';


items = [{
    id: 'Ondo',
    name: 'Ondo',
}, {
    id: 'Ogun',
    name: 'Ogun',
}, {
    id: 'Calabar',
    name: 'Calabar',
}, {
    id: 'Kaduna',
    name: 'Kaduna',
}, {
    id: 'Abuja',
    name: 'Abuja',
}];

let industryRecruitementExperience = [
    {
        id: '1-3 yrs',
        name: '1-3 yrs',
    }, {
        id: '4-9 yrs',
        name: '4-9 yrs',
    }, {
        id: '10-15 yrs',
        name: '10-15 yrs',
    }, {
        id: '16 & above',
        name: '16 & above',
    }
];
let curencies = [
    {
        id: 'USD',
        name: 'USD',
    }, {
        id: 'Naira',
        name: 'Naira',
    }, {
        id: 'EURO',
        name: 'EURO',
    },
    //  {
    //     id: '10 - above',
    //     name: '10 - above',
    // }
];

let defaultindustries = [{
    id: 'Tech',
    name: 'Tech',
}, {
    id: 'business',
    name: 'business',
}, {
    id: '16hbajsabsd',
    name: 'Calabar',
}, {
    id: 'sdhyaysdj',
    name: 'Kaduna',
}, {
    id: 'suudydjsjd',
    name: 'Abuja',
}];
@observer
class UpdateRecruiterProfile extends Component {
    constructor(props) {
        super(props);
        this.mapRef = null;
        this.state = {
            recruiterIndustry: [],
            industryRecruitementExperience: [],
            curency: [],
            uploading: false,
            BondWithCompany: false,
            recruiterCharge: '',




            email: '',
            password: '',
            errorloading: false,
            selectedItems: [],
            industry: [],
            jobLevel: [],
            JobIntrests: [],
            tags: {
                tag: '',
                tagsArray: []
            },

            avatarSource: null,
            cover_pix: null,
            isImagefromdevice: false,
            loader_visible: false,
            showsaveBtn: false,
            file2upload: null,


        }
        selectedItems = [];

        this.UpdateRecruiterProfile = this.UpdateRecruiterProfile.bind(this)
        this.changeBondWithCompany = this.changeBondWithCompany.bind(this)
        this._selectPhotoTapped = this._selectPhotoTapped.bind(this)
        this._uploadImage = this._uploadImage.bind(this)
        this.toggleSuccessPostModalShow = this.toggleSuccessPostModalShow.bind(this)
    }
    onSelectedItemsChange = selectedItems => {
        this.setState({ selectedItems });
    };
    updateTagState = (state) => {
        this.setState({
            tags: state
        })
    };
    async UpdateRecruiterProfile() {

        const { recruiterIndustry, BondWithCompany, recruiterCharge, curency } = this.state;
        // const { RecruiterProfile } = DashboardStore
        const { RecruiterProfile } = DashboardStore;
        console.log(RecruiterProfile);
        DashboardStore.__UpdateRecruiterProfile();



    };
    async componentDidMount() {
        DashboardStore.UpdateNavigationState(this.props.navigation);

        let UserloginToken = await temp();
        // let loggedinUserdetails=await loggedinUserdetails();
        console.log(UserloginToken);
        DashboardStore._getAxiosInstance(UserloginToken);
        //  DashboardStore.__GetIndustries(1,10);
        // DashboardStore.__ALLSkillWithOutFilter(1, 30);
        DashboardStore.__GetUserIndustries();
        DashboardStore.__JobsLevels();
        this.SetDefaultValue();
        DashboardStore.UpdateNavigationState(this.props.navigation);

    }

    onSelectedItemsChange = selectedItems => {
        this.setState({ selectedItems });

    };
    SetDefaultValue = () => {
        // const { LoggedInUserProfile } = DashboardStore;

        // console.log(LoggedInUserProfile)
        // console.log("LoggedInUserProfile")
        // let Fullname = '';
        // if (LoggedInUserProfile.first_name == null || LoggedInUserProfile.last_name == null) {
        //     Fullname = `${LoggedInUserProfile.username} `;
        // }
        // else {
        //     Fullname = `${LoggedInUserProfile.first_name} ${LoggedInUserProfile.last_name} `

        // }
        const { RecruiterProfile } = DashboardStore;

        let email = RecruiterProfile.email;
        let summary = RecruiterProfile.summary;
        let location = RecruiterProfile.location;
        let Fullname = RecruiterProfile.fullname;
        // let Fullname = RecruiterProfile.fullname;
        let _industryRecruitementExperience = RecruiterProfile.industryRecruitementExperience;


        DashboardStore._updateParameterV2(email, "email", "RecruiterProfile");
        DashboardStore._updateParameterV2(Fullname, "fullname", "RecruiterProfile");
        //   DashboardStore._updateParameterV2(phone, "phone", "RecruiterProfile");
        DashboardStore._updateParameterV2(summary, "summary", "RecruiterProfile");
        DashboardStore._updateParameterV2(location, "location", "RecruiterProfile");
let h=[];
h.push[_industryRecruitementExperience]
console.log(h)
this.setState({
    industryRecruitementExperience:h
})
    };
    changeBondWithCompany = () => {
        const { BondWithCompany } = this.state;
        console.log(BondWithCompany)
        this.setState({
            BondWithCompany: !BondWithCompany
        })
        // this.setState({ selectedItems });

    };
    OnRecruiterIndustryChange = recruiterIndustry => {
        console.log(recruiterIndustry)
        this.setState({ recruiterIndustry });
        // DashboardStore._updateParameterV2(addresstosend, "location", "RecruiterProfile");
        // DashboardStore.MyRecruiterProfile.RecruiterProfile.industry=["Apparel &amp; Fashion"];
       DashboardStore.RecruiterProfile.industry=recruiterIndustry;
        // console.log

    };
    _selectPhotoTapped = async () => {
        // const options = {
        //   quality: 1.0,
        //   maxWidth: 500,
        //   maxHeight: 500,
        //   storageOptions: {
        //     skipBackup: true
        //   },
        //   mediaType:'photo',
        //   // mediaType:'video',
        // };

        const options = {
            title: 'Select',
            noData: true,
            mediaType: 'mixed',
            storageOptions: {
                skipBackup: true,
                path: 'construction-cloud'
            }
        };


        ImagePicker.showImagePicker(options, async (response) => {
            // ImagePicker.launchImageLibrary(options, (response)  => {
            console.warn('Response = ', response);

            if (response.didCancel) {
                console.warn('User cancelled photo picker');
            }
            else if (response.error) {
                console.warn('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.warn('User tapped custom button: ', response.customButton);
            }
            else {
                console.log(response)
                let fileSize=response.fileSize/1048576;
              
                let thissizeInMB=Math.round(fileSize)
                // alert(thissizeInMB)
//                 if(thissizeInMB > parseInt(DashboardStore.MaxFileUpload.Genaral)){
//                     // let acceptsize= DashboardStore.MaxFileUpload.Genaral/1024/1024;
//                     let acceptsize= DashboardStore.MaxFileUpload.Genaral
//                     ShowNotification(`File too large , file should not be more than ${acceptsize} MB`, true);

// return;
//                 }
                let Selected_image_localPath = { uri: response.uri };
                ShowNotification('Please Wait', true);
                DashboardStore.isProcessing=true;

                // You can also display the image using data:
                // let source = { uri: 'data:image/jpeg;base64,' + response.data };
                // const source = {
                //     uri: response.uri,
                //     type: response.type,
                //     name: response.fileName,
                // }
                // console.log(source)
                this.setState({
                    avatarSource: Selected_image_localPath,
                    isImagefromdevice: true,
                    showsaveBtn: true,
                    // file2upload: source,
                    uploading: true,
                });

                // this.cloudinaryUpload();

                const source = {
                    uri: response.uri,
                    type: response.type,
                    name: response.fileName,
                }
                this.setState({
                    filename: response.fileName,
                })
                console.log("Bacl from picker")
                let FileUrl = await cloudinaryUpload(source, YOUR_CLOUDINARY_PRESET());
                DashboardStore.isProcessing=false;

                if (FileUrl !== false) {

                    console.log("FileUrl");
                    console.log(FileUrl);
                    console.log("FileUrl_done");
                    let links = [];
                    // let fileUrl = data.secure_url;
                    links.push(FileUrl);
                    DashboardStore._updateParameterV2(links, "additionalFiles", "RecruiterProfile");
                    DashboardStore.RecruiterProfile.additionalFiles=links;


                    DashboardStore._updateParameterV2(FileUrl, "profilePicture", "RecruiterProfile");
                    ShowNotification('Done Saving your File', true);

                }

            }

        });
    }
    _uploadImage() {
        // this.setState({
        //   loader_visible: true
        // });
        ShowNotification('Uploading your file...', true);
        let file2upload = this.state.file2upload;
        // console.warn(file2upload)
        let source = { uri: file2upload.uri };
        this.setState({
            uploadingImg: true
        });
        uploadFile(file2upload)
            .then(response => response.json())
            .then(result => {
                console.log(result)
                let imageurl = result.secure_url;

                let links = [];
                // let fileUrl = data.secure_url;
                links.push(imageurl);
                DashboardStore._updateParameterV2(links, "additionalFiles", "profileupdatedata");


                DashboardStore._updateParameterV2(imageurl, "image_url", "profileupdatedata");
                ShowNotification('Saving to database....', true);
                DashboardStore.__UpdateProfile();
                // this.SavetoDatabase(result)
                // this.setState({
                //     avatarSource: { uri: result.secure_url },
                //     loader_visible: false
                // });
            })

    }

    async cloudinaryUpload() {
        const { file2upload } = this.state;
        // let file=
        // DashboardStore._ToggleProcessing(true)
        console.log("here man")
        ShowNotification('Uploading your file...', true);

        const data = new FormData()
        data.append('file', file2upload)
        data.append('upload_preset', YOUR_CLOUDINARY_PRESET())
        data.append("cloud_name", YOUR_CLOUDINARY_NAME())
        fetch("https://api.cloudinary.com/v1_1/workbrook-hash/upload", {
            // https://api.cloudinary.com/v1_1/workbrook-hash/image/upload
            method: "post",
            body: data
        }).then(res => res.json()).
            then(data => {
                console.log("<>>><<")
                console.log(data)
                let links = [];
                let fileUrl = data.secure_url;
                links.push(fileUrl);
                DashboardStore._updateParameterV2(links, "additional_files", "RecruiterProfile");
                DashboardStore._updateParameterV2(fileUrl, "profilePicture", "RecruiterProfile");
                // ShowNotification('Saving to database....', true);
                // DashboardStore.__UpdateProfile();
                // this.SavetoDatabase(result)
                // setPhoto(data.secure_url)
                this.setState({
                    uploading: false
                })


            }).catch(err => {
                console.log(err)
                Alert.alert("An Error Occured While Uploading")
            })
    }

    toggleSuccessPostModalShow = () => {
        DashboardStore._ToggleSucessPostModal(true)
    };
    toggleSuccessPostModalClose = () => {
        DashboardStore._ToggleSucessPostModal(false)
    };
    onflagSelect = (flags) => {
        console.log(flags)
    };
    render() {
        const { RecruiterProfile } = DashboardStore;

        // const { selectedItems } = this.state;
        const { selectedItems, JobIntrests, uploading, recruiterIndustry, BondWithCompany } = this.state;
        const { isImagefromdevice, avatarSource, loader_visible, showsaveBtn } = this.state;
        let renderImage;
        renderImage = isImagefromdevice === true ?
            <Thumbnail square
                large
                source={avatarSource}
                style={{ height: 121, width: 125, borderRadius: 125 / 2 }}
            />
            :
            renderImage =   <Thumbnail square
            large
            source={{uri:RecruiterProfile.profilePicture}}
            style={{ height: 121, width: 125, borderRadius: 125 / 2 }}
        />
        // if (isImagefromdevice === true) {
        //     renderImage = <Thumbnail square
        //         large
        //         source={avatarSource}
        //         style={{ height: 195, width: '100%', }}

        //     />

        // else{
        //     renderImage = <SvgUri
        //     width="121px"
        //     width="125px"
        //     uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593099630/Mobile_Assets/recruiterImg_peutqe.svg"
        //     />
        // }
        // }
        return (

            <Container style={styles.container}>
                {myspiner(DashboardStore.isProcessing)}
                <CustomizeHeader
                    leftside='cancel'
                    title={"Update Profile"}
                    rightside='empty'
                    statedColor='#000'
                    icon='md-close'
                    savehandler={this.proceednow}
                />

                <ScrollView
                    contentContainerStyle={{ paddingBottom: 50, }}
                    keyboardShouldPersistTaps="handled"

                >
                    <View style={[styles.imageHolderWrapper, { backgroundColor: 'transparent', marginTop: 40, }]}
                    >
                        {renderImage}
                        <TouchableOpacity style={{ alignItems: 'center' }}
                            onPress={this._selectPhotoTapped}
                        >
                            <Text style={styles.cardTextJob6}>
                                Upload profile image
                                </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ marginTop: 32, marginBottom: 8, marginLeft: 33, marginRight: 33 }}>
                        <Text style={styles.notices}>Required fields are marked * </Text>
                    </View>
                    {/* <View style={styles.imageHolder}>
                        
                    </View> */}
                    {/* <CardItem style={[{ backgroundColor: 'transparent', }]}> */}
                    <Item stackedLabel style={[styles.bottomIndBorder, { height: 55 }]}>
                        <Label style={styles.labelSty}>Full Name<Text style={styles.asteriskSty}>*</Text></Label>
                        <Input
                            placeholder="Full name"
                            placeholderLabel
                            placeholderTextColor="#A7A7A7"
                            style={[styles.cardTextJob3,]}
                            defaultValue={RecruiterProfile.fullname}
                            onChangeText={(fullname) => DashboardStore.onChangeText('fullname', fullname, 'RecruiterProfile')}
                        />

                    </Item>
                    <Item stackedLabel style={[styles.bottomIndBorder, { height: 55 }]}>
                        <Label style={styles.labelSty}>Email Address<Text style={styles.asteriskSty}>*</Text></Label>
                        <Input
                            placeholder="Email"
                            placeholderLabel
                            placeholderTextColor="#A7A7A7"
                            style={[styles.cardTextJob3,]}
                            defaultValue={RecruiterProfile.email}
                            onChangeText={(email) => DashboardStore.onChangeText('email', email, 'RecruiterProfile')}
                            keyboardType={'email-address'}
                        />

                    </Item>
                    <Item stackedLabel style={[styles.bottomIndBorder, { height: 55 }]}>
                        <Label style={styles.labelSty}>Phone Number<Text style={styles.asteriskSty}>*</Text></Label>
                        <Input
                            placeholder="Phone Number"
                            placeholderLabel
                            placeholderTextColor="#A7A7A7"
                            style={[styles.cardTextJob3,]}
                            defaultValue={RecruiterProfile.phone}
                            onChangeText={(phone) => DashboardStore.onChangeText('phone', phone, 'RecruiterProfile')}
                            keyboardType={'number-pad'}
                        />

                    </Item>
                    {/* <Item stackedLabel style={[styles.bottomIndBorder, { height: 55 }]}> */}
                    {/* <Label style={styles.labelSty}>Location<Text style={styles.asteriskSty}>*</Text></Label> */}
                    {/* <Input
                            placeholder="Location"
                            placeholderLabel
                            placeholderTextColor="#A7A7A7"
                            style={styles.cardTextJob3}
                            defaultValue={RecruiterProfile.location}
                            onChangeText={(location) => DashboardStore.onChangeText('location', location, 'RecruiterProfile')}
                            keyboardType={'default'}
                        /> */}
                    <View stackedLabel style={[styles.bottomIndBorder, { marginTop: 20, paddingBottom: 3, }]}>
                    <Label style={styles.labelSty}>Location<Text style={styles.asteriskSty}>*</Text></Label>
                        <GooglePlacesInput
                            OnpressAction={(data, details = null) => { // 'details' is provided when fetchDetails = true
                                let latitude = details.geometry.location.lat;
                                let longitude = details.geometry.location.lng;
                                let ServiceAddress = details.formatted_address || details.description || details.name;
                                let nearestplace = details.name;
                                let addresstosend = `${ServiceAddress}   (  ${nearestplace}  )`;
                                DashboardStore._updateParameterV2(addresstosend, "location", "RecruiterProfile");
                                
                            }}
                        MydefaultLocation={RecruiterProfile.location}
                        />
                    </View>

                    {/* </Item> */}
                    <View style={[styles.bottomIndBorder, { marginTop: 20, paddingBottom: 3, }]}>
                        <Label style={[styles.labelSty, { marginBottom: -8 }]}>Industry<Text style={styles.asteriskSty}>*</Text></Label>
                        <MultiSelect
                            hideTags={false}
                            // items={DashboardStore.ALLSkillWithOutFilter.ALLSkill}
                            items={DashboardStore.AllIndustries.Industry}
                            uniqueKey="id"
                            ref={(component) => { this.multiSelect = component }}
                            onSelectedItemsChange={this.OnRecruiterIndustryChange}
                            selectedItems={RecruiterProfile.industry}
                            selectText="Industry"
                            searchInputPlaceholderText="Search Industry"
                            onChangeInput={(text) => console.log(text)}
                            tagRemoveIconColor="#FF5964"
                            tagBorderColor="#CCC"
                            tagTextColor="#CCC"
                            selectedItemTextColor="#CCC"
                            selectedItemIconColor="#CCC"
                            itemTextColor="#fff"
                            textColor="#A7A7A7"
                            tagTextColor="#A7A7A7"
                            displayKey="name"
                            searchInputStyle={{ color: '#CCC', borderWidth: 0, borderColor: "#ccc", fontWeight: "300" }}
                            styleItemsContainer={{ borderWidth: 1, borderColor: '#1E93E2', backgroundColor: "#1E93E2", fontWeight: "300" }}
                            submitButtonColor="#CCC"
                            submitButtonText="Submit"
                            itemFontSize={10}
                            fontSize={10}
                            styleDropdownMenuSubsection={{ borderBottomWidth: 0, color: '#A7A7A7', backgroundColor: 'transparent', height: 35, paddingLeft: 5, paddingBottom: 0, marginBottom: -10, fontSize: 10, fontWeight: "300" }}
                            selectedItemIconColor={'#fff'}

                            hideSubmitButton={true}
                        />
                         {/* <View style={{ fontSize: 10 }}>
                            {this.multiSelect ? this.multiSelect.getSelectedItemsExt(JobIntrests) : null}

                        </View> */}
                        <View style={{ fontSize: 10 }}>
                            {this.multiSelect ? this.multiSelect.getSelectedItemsExt(recruiterIndustry) : null}

                        </View>
                    </View>
                    <View style={[styles.bottomIndBorder, { marginTop: 20, paddingBottom: 3, }]}>
                        <Label style={[styles.labelSty, { marginBottom: -8 }]}>Recruitment Experience<Text style={styles.asteriskSty}>*</Text></Label>
                        <MultiSelect
                            hideTags
                            items={industryRecruitementExperience}
                            uniqueKey="id"
                            ref={(component) => { this.multiSelect = component }}
                            onSelectedItemsChange={(industryRecruitementExperience) => { 
                                // alert("hey man")
                                console.log(industryRecruitementExperience)
                                this.setState({
                                    industryRecruitementExperience
                                })
                                // console.log()
                                let item = industryRecruitementExperience[0];
                                DashboardStore._updateParameterV2(item, "industryRecruitementExperience", "RecruiterProfile");

                            }
                            }
                            selectedItems={this.state.industryRecruitementExperience}
                            selectText="recruitment experience"
                            searchInputPlaceholderText=""
                            onChangeInput={(text) => console.log(text)}
                            tagRemoveIconColor="#FF5964"
                            tagBorderColor="#CCC"
                            tagTextColor="#CCC"
                            selectedItemTextColor="#CCC"
                            selectedItemIconColor="#CCC"
                            itemTextColor="#fff"
                            textColor="#A7A7A7"
                            displayKey="name"
                            searchInputStyle={{ color: '#CCC', borderWidth: 0, borderColor: "#ccc", fontWeight: "300" }}
                            styleItemsContainer={{ borderWidth: 1, borderColor: '#1E93E2', backgroundColor: "#1E93E2", }}
                            submitButtonColor="#CCC"
                            submitButtonText="Submit"
                            itemFontSize={10}
                            fontSize={10}
                            styleDropdownMenuSubsection={{ borderBottomWidth: 0, backgroundColor: 'transparent', height: 35, paddingLeft: 5, paddingBottom: 0, marginBottom: -20, fontSize: 10, fontWeight: "300" }}
                            selectedItemIconColor={'#fff'}
                            hideSubmitButton={true}
                            single={true}
                            textInputProps={{ editable: false, autoFocus: false }}
                            searchIcon={null}
                        />
                    </View>




                    {/* <Item style={[styles.bottomIndBorder, { height: 55 }]}>
                        <Input
                            placeholder="Industry"
                            placeholderLabel
                            placeholderTextColor="#A7A7A7"
                            style={styles.cardTextJob3}
                            defaultValue={RecruiterProfile.location}
                        // onChangeText={(location) => DashboardStore.onChangeText('location', location, 'RecruiterProfile')}
                        />
                    </Item> */}
                    {/* </CardItem> */}
                    {/* <View style={styles.bottomIndBorder}>
                        <MultiSelect
                            hideTags
                            items={defaultindustries}
                            uniqueKey="id"
                            ref={(component) => { this.multiSelect = component }}
                            onSelectedItemsChange={(industry) => {
                                console.log(industry)
                                this.setState({ industry })
                                let item = industry[0];
                                // DashboardStore._updateParameterV2(item, "industry", "RecruiterProfile");

                            }
                                // this.onSelectedItemsChange
                            }
                            selectedItems={this.state.industry}
                            selectText="Industry"
                            searchInputPlaceholderText="Search Industry..."
                            onChangeInput={(text) => console.log(text)}
                            tagRemoveIconColor="#FF5964"
                            tagBorderColor="#CCC"
                            tagTextColor="#CCC"
                            selectedItemTextColor="#CCC"
                            itemTextColor="#fff"
                            textColor="#A7A7A7"
                            displayKey="name"
                            searchInputStyle={{ color: '#CCC', borderWidth: 0, borderColor: "#ccc", fontWeight: "300" }}
                            styleItemsContainer={{ borderWidth: 1, borderColor: '#1E93E2', backgroundColor: "#1E93E2" }}
                            submitButtonColor="#CCC"
                            submitButtonText="Submit"
                            itemFontSize={10}
                            fontSize={10}
                            styleDropdownMenuSubsection={{ borderBottomWidth: 0, backgroundColor: 'transparent', height: 35, paddingLeft: 5, paddingBottom: 0, marginBottom: -20, fontSize: 10, fontWeight: "300" }}
                            selectedItemIconColor={'#fff'}

                            hideSubmitButton={true}
                            single={true}
                        />
                    </View>
                     */}

                    <Item stackedLabel style={[styles.bottomIndBorder, { display: 'flex', flexDirection: 'column', alignItems: 'flex-start' }]}>
                        <Label style={styles.labelSty}>Recruiter Profile Summary (200 words)<Text style={styles.asteriskSty}>*</Text></Label>
                        <Textarea rowSpan={3}
                         placeholder="Summary About you"

                            defaultValue={RecruiterProfile.summary}
                            onChangeText={(summary) => DashboardStore.onChangeText('summary', summary, 'RecruiterProfile')}
                        />


                    </Item>

                    <Item stackedLabel style={[styles.bottomIndBorder, { height: 55, }]}>
                        <Label style={styles.labelSty}>Recruiter Charges<Text style={styles.asteriskSty}>*</Text></Label>
                        <Input
                            placeholder="Recruiter Charges"
                            placeholderLabel
                            placeholderTextColor="#A7A7A7"
                            style={styles.cardTextJob3}
                            // defaultValue={RecruiterProfile.linkdnUrl}
                            // onChangeText={(recruiterCharge) => this.setState({ recruiterCharge })}


                            defaultValue={RecruiterProfile.averageAmount}
                            onChangeText={(averageAmount) => DashboardStore.onChangeText('averageAmount', averageAmount, 'RecruiterProfile')}
                     
                            keyboardType={'default'}

                        />
                    </Item>


                    <Item stackedLabel style={[styles.bottomIndBorder, { height: 55 }]}>
                        <Label style={styles.labelSty}>Linkedin URL (optional)</Label>
                        <Input
                            placeholder="LinkedIn URL (optional)"
                            placeholderLabel
                            placeholderTextColor="#A7A7A7"
                            style={styles.cardTextJob3}
                            defaultValue={RecruiterProfile.linkdnUrl}
                            onChangeText={(linkdnUrl) => DashboardStore.onChangeText('linkdnUrl', linkdnUrl, 'RecruiterProfile')}
                            keyboardType={'url'}
                        />
                    </Item>

                    {/* <Item style={[styles.bottomIndBorder, { height: 55 }]}> */}
                    {/* <CountryCurrencyPicker
        //   containerStyle={styles.containerStyle}
        //   {...props}
        onValueChange={this.onflagSelect}
        /> */}
                    {/* </Item> */}


                    {/* <View style={[styles.bottomIndBorderNon, { marginTop: 30 }]}>
                        <Left style={{

                            display: 'flex',
                            flexDirection: 'row',
                            alignItems: 'center',
                        }}>
                            <CheckBox
                                checked={this.state.BondWithCompany}
                                onPress={this.changeBondWithCompany}
                            />
                            <Body style={[{ marginLeft: 10 }]}>
                                <Text style={[styles.cardTextJob1, { marginLeft: 10 }]} >
                                    <Text style={[styles.cardTextJob1, {}]}>
                                        I confirm that I have read and I understand the
                                    </Text>
                                    <Text style={[styles.cardTextJob1, { color: "#1E93E2", fontWeight: "bold" }]}>
                                        Terms & Conditions
                                    </Text>
                                    <Text style={[styles.cardTextJob1,]}>
                                        to become a recruiter on workbrook.
                                    </Text>
                                </Text>
                            </Body>
                        </Left>
                    </View> */}
                    {/* <Button transparent style={styles.termConditionWrap}>
                        <Text style={[styles.cardTextJob2_2, { marginRight: 2 }]}>
                            By clicking “become a recruiter’ you are agreeing to our
                        </Text>
                        <Text style={[styles.cardTextJob2_2, { color: '#1E93E2' }]}>
                            Terms & Conditions
                        </Text>
                    </Button> */}
                         <Button
                            full
                            style={[styles.btnNewPost, { backgroundColor: '#1E93E2', elevation: 0, marginTop: 13, marginBottom: 20, borderRadius: 4 }]}
                            onPress={this.UpdateRecruiterProfile}
                        >
                            <Text style={styles.cardTextJob4_1}>
                                Update
                        </Text>
                        </Button>
                    

 

                </ScrollView>

                {/* <Modal style={styles.MContainer} isVisible={DashboardStore.isSuccessPostModalVisible} >
                    <Button onPress={this.toggleSuccessPostModalClose} style={{ backgroundColor: "transparent", position: 'absolute', right: 10, top: 40, padding: 20, elevation: 0 }}>
                        <Image source={CloseModal} style={{ width: 19, height: 19, }} />
                    </Button>
                    <View style={[styles.Mwrapper, { height: 380 }]}>
                        <View style={{ paddingLeft: 29, paddingRight: 29, display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                            <Image source={SuccessIcon} style={{ width: 129, height: 129, marginBottom: 17 }} />
                            <Text style={styles.cardText1, { fontSize: 16, color: '#5ABC7A', textAlign: 'center', fontWeight: '600' }}>
                                Congratulations!
                                        </Text>
                            <Text style={styles.cardText1, { fontSize: 16, color: '#5ABC7A', textAlign: 'center', fontWeight: '600' }}>

                                Your job was successfully posted.
                            </Text>
                        </View>

                    </View>
                </Modal> */}

            </Container>


        );
    }
}
export default UpdateRecruiterProfile;

const styles = StyleSheet.create({
    container: {
        backgroundColor: "#FAFAFA",
        display: "flex",
        flex: 1,
        justifyContent: "flex-start",
    },
    imageHolder: {
        display: "flex",
        alignItems: "center",
        paddingLeft: 15,
        paddingRight: 15
    },
    imageHolderWrapper: {
        display: "flex",
        alignItems: "center",
    },
    btnNewPost: {
        height: 52,
        elevation: 0,
        minWidth: '90%',
        marginLeft: 15,
        marginRight: 15,
    },
    IconSetsty1: {
        maxWidth: 16,
        maxHeight: 17,
    },
    IconSetsty: {
        maxWidth: 16,
        maxHeight: 16,
    },
    iconWrapSet: {
        width: 20,
        marginRight: 10
    },
    IconSetsty2: {
        width: 8,
        height: 15,

    },
    bottomIndBorderNon: {
        marginLeft: 33,
        marginRight: 33,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        // borderBottomWidth: 1,
        // borderColor: '#E5E5E5'
    },
    bottomIndBorder: {
        marginLeft: 33,
        marginRight: 33,
        borderBottomWidth: 1,
        borderColor: '#E5E5E5',
        marginTop: 15,
    },
    bottomIndBorder_2: {
        marginLeft: 33,
        marginRight: 33,
        borderBottomWidth: 1,
        borderColor: '#E5E5E5',
    },
    rmPadding: {
        paddingTop: 0,
        paddingRight: 0,
        paddingBottom: 0,
        paddingLeft: 0,
    },
    rmPaddingLR: {
        paddingLeft: 0,
        paddingRight: 0,
    },

    cardTextJob1: {
        color: "#2F2F2F",
        fontSize: 11,
    },
    cardTextJob2: {
        color: "#676767",
        fontSize: 9,
        fontWeight: '300',
    },
    cardTextJob2_2: {
        color: "#2F2F2F",
        fontSize: 9,
        fontWeight: '500',
    },
    notices: {
        color: "#FF5964",
        fontSize: 10,
        fontWeight: '300',
    },
    cardTextJob3: {
        color: "#2F2F2F",
        fontSize: 11,
        fontWeight: '300',
        marginBottom: 0,
    },
    cardTextJob4: {
        color: "#1E93E2",
        fontSize: 11,
        fontWeight: '500',
    },
    cardTextJob4_1: {
        color: "#fff",
        fontSize: 12,
        fontWeight: '500',
    },
    cardTextJob5: {
        color: "#1E93E2",
        fontSize: 13,
        fontWeight: '600',
    },
    cardTextJob6: {
        color: "#1E93E2",
        fontSize: 11,
        fontWeight: '500',
    },
    cardTextJob7: {
        fontSize: 12,
        color: '#2F2F2F',
    },
    Mwrapper: {
        height: 510,
        width: '90%',
        backgroundColor: '#fff',
        // padding: 15,
        display: 'flex',
        justifyContent: 'space-around',
        // alignItems: 'center'
    },
    cardText1: {
        color: "#2F2F2F",
        fontWeight: '500',
        fontSize: 12,
    },
    MContainer: {
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
    },
    termConditionWrap: {
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
    },
    currencyWrap: {
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
    },
    labelSty: {
        color: "#2F2F2F",
        fontSize: 11,
        fontWeight: '500'
    },
    asteriskSty: {
        color: "#FF5964",
        fontSize: 10,
    },


});

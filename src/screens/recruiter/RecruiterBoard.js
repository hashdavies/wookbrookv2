import React, { Component, Fragment } from 'react';
import { Text, StyleSheet, ImageBackground, Keyboard, TouchableOpacity, Alert } from 'react-native';
import { KeyboardAvoidingView, Image, ScrollView } from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons';
import Checker from '../../assets/workbrookAssets/checker.png';
import {
    Container,
    Header,
    Content,
    Textarea,
    Button,
    View,
    Thumbnail,
    Form,
    Item,
    Input,
    Label,
    Badge,
    Icon,
    Footer,
    Picker,
    Item as FormItem,
    Left,
    Body,
    CardItem,
    Card,
    Right,
    ListItem,
    List,
    CheckBox,
} from 'native-base';
import {
    Kaede,
    Hoshi,
    Jiro,
    Isao,
    Madoka,
    Akira,
    Hideo,
    Kohana,
    Makiko,
    Sae,
    Fumi,
} from 'react-native-textinput-effects';
import CustomizeHeader from '../frags/CustomizeHeader';

import { myspiner, temp, ShowNotification } from '../../dependency/UtilityFunctions';
import Logo from '../../assets/workbrookAssets/logo.png';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import RBSheet from "react-native-raw-bottom-sheet";
import Interests from '../interests/Interests';
import Noti from "../../assets/workbrookAssets/noti.png";
import UserBl from "../../assets/workbrookAssets/userBl.png";
import ArrForward from "../../assets/workbrookAssets/arrfr.png";
import Shield from "../../assets/workbrookAssets/shield.png";
import Quest from "../../assets/workbrookAssets/quest.png";
import Info from "../../assets/workbrookAssets/info.png";
import Plus from "../../assets/workbrookAssets/plus.png";
import Remark from "../../assets/workbrookAssets/remark.png";
import Like from "../../assets/workbrookAssets/like.png";
import MoreIcon2 from "../../assets/workbrookAssets/more2.png";
import Profile from "../../assets/images/profile/dp2.png";
import cardPix from "../../assets/images/background/bg3_old.jpg";
import RNPickerSelect from 'react-native-picker-select';
import TagInput from 'react-native-tags-input';
import MoreIcon from "../../assets/workbrookAssets/more.png";

import RNFetchBlob from 'react-native-fetch-blob'
import ImagePicker from 'react-native-image-picker';
import CloseModal from "../../assets/workbrookAssets/whitecloss.png";
import SuccessIcon from "../../assets/workbrookAssets/successPost.png";
import Drop from "../../assets/workbrookAssets/drop.png";
import Modal from 'react-native-modal';
import AccountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import { text } from 'react-native-communications';
import DashboardStore from '../../stores/Dashboard';
import { hasSetIntrest } from '../../auth';
import Autocomplete from 'react-native-autocomplete-input';
import MultiSelect from 'react-native-multiple-select';
import { YOUR_CLOUDINARY_PRESET, YOUR_CLOUDINARY_NAME, requestCameraPermission, YOUR_CLOUDINARY_PRESET_JobBanner } from '../../dependency/UtilityFunctions';



items = [{
    id: 'Ondo',
    name: 'Ondo',
}, {
    id: 'Ogun',
    name: 'Ogun',
}, {
    id: 'Calabar',
    name: 'Calabar',
}, {
    id: 'Kaduna',
    name: 'Kaduna',
}, {
    id: 'Abuja',
    name: 'Abuja',
}];
let NumberOfStaff = [{
    id: '2',
    name: '1-10',
}, {
    id: '3',
    name: '4',
}, {
    id: '21-30',
    name: '21-30',
}, {
    id: '30-50',
    name: '30-50',
}, {
    id: '50 and Above',
    name: '50 and Above',
}];
let defaultindustries = [{
    id: 'Tech',
    name: 'Tech',
}, {
    id: 'business',
    name: 'business',
}, {
    id: '16hbajsabsd',
    name: 'Calabar',
}, {
    id: 'sdhyaysdj',
    name: 'Kaduna',
}, {
    id: 'suudydjsjd',
    name: 'Abuja',
}];
@observer
class RecruiterBoard extends Component {
    constructor(props) {
        super(props);
        this.mapRef = null;
        this.state = {
            email: '',
            password: '',
            errorloading: false,
            AmCompanyRep: true,
            selectedItems: [],
            industry: [],
            numberOfStaff: [],
            jobLevel: [],
            JobIntrests: [],
            tags: {
                tag: '',
                tagsArray: []
            },

            avatarSource: null,
            cover_pix: null,
            isImagefromdevice: false,
            loader_visible: false,
            showsaveBtn: false,
            uploading: false,
            file2upload: null,


        }
        selectedItems = [];

        this.changeAmCompanyRep = this.changeAmCompanyRep.bind(this)
        this.toggleSuccessPostModalShow = this.toggleSuccessPostModalShow.bind(this)
    }
    onSelectedItemsChange = selectedItems => {
        this.setState({ selectedItems });
    };
    async componentDidMount() {
        DashboardStore.UpdateNavigationState(this.props.navigation);

        let UserloginToken = await temp();
        // let loggedinUserdetails=await loggedinUserdetails();
        console.log(UserloginToken);
        DashboardStore._getAxiosInstance(UserloginToken);
        //  DashboardStore.__GetIndustries(1,10);
        DashboardStore.__ALLSkillWithOutFilter(1, 30);
        DashboardStore.__JobsLevels();

    }

    onSelectedItemsChange = selectedItems => {
        this.setState({ selectedItems });

    };
    changeAmCompanyRep = () => {
        const { AmCompanyRep } = this.state;
        console.log(AmCompanyRep)
        this.setState({
            AmCompanyRep: !AmCompanyRep
        })
        // this.setState({ selectedItems });

    };
    toggleSuccessPostModalShow = () => {
        DashboardStore._ToggleSucessPostModal(true)
    };
    toggleSuccessPostModalClose = () => {
        DashboardStore._ToggleSucessPostModal(false)
    };
    render() {
        // const { selectedItems } = this.state;
        const { selectedItems, JobIntrests, uploading } = this.state;
        const { isImagefromdevice, avatarSource, loader_visible, showsaveBtn } = this.state;
        let renderImage;

        if (isImagefromdevice === true) {
            renderImage = <Thumbnail square
                large
                source={avatarSource}
                style={{ height: 195, width: '100%', }}

            />
        }
        return (

            <Container style={styles.container}>
                <View style={styles.imageHolder}>
                    <Thumbnail square
                        large
                        source={Profile}
                        style={{ height: 53, width: 51, borderRadius: 53 / 2 }}
                    />
                    <Text style={styles.cardTextJob7}>Tomzy Adex</Text>
                    <View style={styles.textIcon}>
                        <Image source={Drop} style={{ marginRight: 5 }} />
                        <Text note style={styles.nameSty2}>Lagos, Nigeria</Text>
                    </View>
                </View>

                <ScrollView
                    contentContainerStyle={{ paddingLeft: 30, paddingRight: 30, paddingBottom: 30 }}
                >
                    <View style={styles.likenoti}>
                        <Text style={[styles.cardTextJob5_1, {}]}>
                            50 profile visits in the last 7 days
                        </Text>
                    </View>
                    <Card style={{ elevation: 0, paddingTop: 15, paddingBottom: 8, borderRadius: 5, backgroundColor: '#F3F3F3', borderWidth: 0, marginBottom: 18 }}>
                        <CardItem style={[styles.rmPadding, { paddingBottom: 5, backgroundColor: 'transparent', paddingLeft: 15, paddingRight: 15, }]}>
                            <Left style={[styles.rmPadding, { flex: 1, }]}>
                                <Body style={[styles.rmPadding], { paddingLeft: 0, marginLeft: 0 }}>
                                    <Text style={[styles.cardTextJob5_1, { color: '#2F2F2F' }]}>
                                        John Aradnikzsch
                                    </Text>
                                    <Text style={styles.cardTextJob3}>
                                        First Inland Revenue, Lagos
                                    </Text>
                                </Body>
                            </Left>
                            <Right>
                                <TouchableOpacity>
                                    <Image source={MoreIcon} style={{ width: 14, height: 4 }} />
                                </TouchableOpacity>
                            </Right>
                        </CardItem>
                        <CardItem style={{ display: 'flex', flexDirection: 'column', alignItems: 'flex-start', paddingLeft: 15, paddingRight: 15, backgroundColor: 'transparent', }}>
                            <Text style={styles.cardTextJob5}>Crofthills Agribusiness</Text>
                            <Text style={[styles.cardTextJob2, { fontSize: 10, fontWeight: 'normal' }]}>
                                Short description about the position, enough to convince people to apply.
                                </Text>
                            <Button transparent style={{ height: 15, elevation: 0 , marginTop : 5, marginBottom: 5}}>
                                <Text style={[styles.cardTextJob5, { color: '#1E93E2' }]}>See more...</Text>
                            </Button>
                        </CardItem>

                        <CardItem style={[styles.rmPaddingLR, { borderBottomWidth: 0, display: 'flex', flexDirection: 'row', backgroundColor: 'transparent', paddingTop: 0, paddingBottom: 0 }]}>

                            <Button full style={[styles.btnActionWrap]}>
                                <Text style={styles.cardTextJob6}>Accept</Text>
                            </Button>

                            <Button full style={[styles.btnActionWrap, { backgroundColor: "#FF5964" }]}>
                                <Text style={[styles.cardTextJob6]}> Decline </Text>
                            </Button>

                        </CardItem>
                    </Card>
                    <Card style={{ elevation: 0, paddingTop: 15, paddingBottom: 8, borderRadius: 5, backgroundColor: '#F3F3F3', borderWidth: 0, marginBottom: 18 }}>
                        <CardItem style={[styles.rmPadding, { paddingBottom: 5, backgroundColor: 'transparent', paddingLeft: 15, paddingRight: 15, }]}>
                            <Left style={[styles.rmPadding, { flex: 1, }]}>
                                <Body style={[styles.rmPadding], { paddingLeft: 0, marginLeft: 0 }}>
                                    <Text style={[styles.cardTextJob5_1, { color: '#2F2F2F' }]}>
                                        John Aradnikzsch
                                    </Text>
                                    <Text style={styles.cardTextJob3}>
                                        First Inland Revenue, Lagos
                                    </Text>
                                </Body>
                            </Left>
                            <Right>
                                <TouchableOpacity>
                                    <Image source={MoreIcon} style={{ width: 14, height: 4 }} />
                                </TouchableOpacity>
                            </Right>
                        </CardItem>
                        <CardItem style={{ display: 'flex', flexDirection: 'column', alignItems: 'flex-start', paddingLeft: 15, paddingRight: 15, backgroundColor: 'transparent', }}>
                            <Text style={styles.cardTextJob5}>Crofthills Agribusiness</Text>
                            <Text style={[styles.cardTextJob2, { fontSize: 10, fontWeight: 'normal' }]}>
                                Short description about the position, enough to convince people to apply.
                                </Text>
                            <Button transparent style={{ height: 15, elevation: 0 , marginTop : 5, marginBottom: 5}}>
                                <Text style={[styles.cardTextJob5, { color: '#1E93E2' }]}>See more...</Text>
                            </Button>
                        </CardItem>

                        <CardItem style={[styles.rmPaddingLR, { borderBottomWidth: 0, display: 'flex', flexDirection: 'row', backgroundColor: 'transparent', paddingTop: 0, paddingBottom: 0 }]}>

                            <Button full style={[styles.btnActionWrap]}>
                                <Text style={styles.cardTextJob6}>Accept</Text>
                            </Button>

                            <Button full style={[styles.btnActionWrap, { backgroundColor: "#FF5964" }]}>
                                <Text style={[styles.cardTextJob6]}> Decline </Text>
                            </Button>

                        </CardItem>
                    </Card>


                </ScrollView>

                <Modal style={styles.MContainer} isVisible={DashboardStore.isSuccessPostModalVisible} >
                    <Button onPress={this.toggleSuccessPostModalClose} style={{ backgroundColor: "transparent", position: 'absolute', right: 10, top: 40, padding: 20, elevation: 0 }}>
                        <Image source={CloseModal} style={{ width: 19, height: 19, }} />
                    </Button>
                    <View style={[styles.Mwrapper, { height: 380 }]}>
                        <View style={{ paddingLeft: 29, paddingRight: 29, display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                            <Image source={SuccessIcon} style={{ width: 129, height: 129, marginBottom: 17 }} />
                            <Text style={styles.cardText1, { fontSize: 16, color: '#5ABC7A', textAlign: 'center', fontWeight: '600' }}>
                                Congratulations!
                                        </Text>
                            <Text style={styles.cardText1, { fontSize: 16, color: '#5ABC7A', textAlign: 'center', fontWeight: '600' }}>

                                Your job was successfully posted.
                            </Text>
                        </View>

                    </View>
                </Modal>

            </Container>


        );
    }
}
export default RecruiterBoard;

const styles = StyleSheet.create({
    container: {
        backgroundColor: "white",
        display: "flex",
        flex: 1,
        justifyContent: "flex-start",
    },
    imageHolder: {
        display: "flex",
        alignItems: "center",
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 25,
        paddingBottom: 25,
        backgroundColor: '#000000',

    },
    recruiterInfoWrap: {
        display: "flex",
        alignItems: "flex-start",
        flexDirection: 'row',
        paddingLeft: 15,
        paddingRight: 15,
        marginTop: 10
    },
    recruiterInfoText1Wrap: {
        display: "flex",
        alignItems: "flex-start",
        flexDirection: 'row',
        color: '#A7A7A7',
        fontSize: 10,
        fontWeight: '300',
        width: 100,
        paddingLeft: 5,
    },
    recruiterInfoText2Wrap: {
        flex: 1,
        display: "flex",
        alignItems: "flex-start",
        flexDirection: 'row',
        paddingLeft: 5,
        // paddingRight: 15,
        color: '#3C3C3C',
        fontSize: 12,
        fontWeight: 'normal',
    },
    btnNewPost: {
        height: 38,
        elevation: 0,
        minWidth: '90%',
        marginLeft: 15,
        marginRight: 15,
    },
    IconSetsty1: {
        maxWidth: 16,
        maxHeight: 17,
    },
    IconSetsty: {
        maxWidth: 16,
        maxHeight: 16,
    },
    iconWrapSet: {
        width: 20,
        marginRight: 10
    },
    IconSetsty2: {
        width: 8,
        height: 15,

    },
    bottomIndBorderNon: {
        marginLeft: 15,
        marginRight: 15,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        // borderBottomWidth: 1,
        // borderColor: '#E5E5E5'
    },
    bottomIndBorder: {
        marginLeft: 15,
        marginRight: 15,
        borderBottomWidth: 1,
        borderColor: '#E5E5E5',
    },
    rmPadding: {
        paddingTop: 0,
        paddingRight: 0,
        paddingBottom: 0,
        paddingLeft: 0,
    },
    rmPaddingLR: {
        paddingLeft: 0,
        paddingRight: 0,
    },

    cardTextJob1: {
        color: "#2F2F2F",
        fontSize: 11,
    },
    cardTextJob2: {
        color: "#676767",
        fontSize: 9,
        fontWeight: '300',
    },
    cardTextJob2_2: {
        color: "#2F2F2F",
        fontSize: 9,
        fontWeight: '500',
    },
    cardTextJob3: {
        color: "#2F2F2F",
        fontSize: 11,
        fontWeight: '300',
    },
    cardTextJob6: {
        color: "#FAFAFA",
        fontSize: 14,
        fontWeight: '600',
    },
    cardTextJob4: {
        color: "#1E93E2",
        fontSize: 11,
        fontWeight: '500',
    },
    cardTextJob4_1: {
        color: "#fff",
        fontSize: 12,
        fontWeight: '500',
    },
    cardTextJob5: {
        color: "#2F2F2F",
        fontSize: 10,
        fontWeight: 'bold',
    },
    cardTextJob5_1: {
        color: "#000000",
        fontSize: 13,
        fontWeight: '500',
    },

    cardTextJob7: {
        fontSize: 12,
        color: '#2F2F2F',
    },
    cardTextJob7: {
        fontSize: 16,
        fontWeight: '600',
        color: '#FAFAFA',
    },
    Mwrapper: {
        height: 510,
        width: '90%',
        backgroundColor: '#fff',
        // padding: 15,
        display: 'flex',
        justifyContent: 'space-around',
        // alignItems: 'center'
    },
    cardText1: {
        color: "#2F2F2F",
        fontWeight: '500',
        fontSize: 12,
    },
    MContainer: {
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
    },
    termConditionWrap: {
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
    },
    likenoti: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        paddingBottom: 16,
        paddingTop: 16,
    },
    // =============================================== drop address ==================
    textIcon: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center'
    },
    nameSty2: {
        fontSize: 12,
        color: '#FAFAFA'
    },
    // =============================================== drop address ==================
    // =============================================== card style ========================
    rmPadding: {
        paddingTop: 0,
        paddingRight: 0,
        paddingBottom: 0,
        paddingLeft: 0,
    },
    btnActionWrap: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#5ABC7A',
        height: 54,
        elevation: 0,
        flex: 1,
    },

    // =============================================== Card style =====================


});

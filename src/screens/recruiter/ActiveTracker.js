import React, { Component, Fragment } from 'react';
import { Text, StyleSheet, ImageBackground, Keyboard, TouchableOpacity, Alert } from 'react-native';
import { KeyboardAvoidingView, Image, ScrollView } from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons';
import Checker from '../../assets/workbrookAssets/checker.png';
import {
    Container,
    Header,
    Content,
    Textarea,
    Button,
    View,
    Thumbnail,
    Form,
    Item,
    Input,
    Label,
    Badge,
    Icon,
    Footer,
    Picker,
    Item as FormItem,
    Left,
    Body,
    CardItem,
    Card,
    Right,
    ListItem,
    List,
    CheckBox,
} from 'native-base';
import CustomizeHeader from '../frags/CustomizeHeader';

import { myspiner, temp, ShowNotification } from '../../dependency/UtilityFunctions';
import Logo from '../../assets/workbrookAssets/logo.png';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import RBSheet from "react-native-raw-bottom-sheet";
import {
    LineChart,
    BarChart,
    PieChart,
    ProgressChart,
    ContributionGraph,
    StackedBarChart
  } from "react-native-chart-kit";
import Profile from "../../assets/images/profile/dp2.png";
import { Col, Row, Grid } from "react-native-easy-grid";
// import * as Progress from 'react-native-progress';
import ProgressBar from 'react-native-progress/Bar';
// import RNFetchBlob from 'react-native-fetch-blob'
import ImagePicker from 'react-native-image-picker';
import Modal from 'react-native-modal';
import AccountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import DashboardStore from '../../stores/Dashboard';
import Autocomplete from 'react-native-autocomplete-input';
import MultiSelect from 'react-native-multiple-select';
import { YOUR_CLOUDINARY_PRESET, YOUR_CLOUDINARY_NAME, requestCameraPermission, YOUR_CLOUDINARY_PRESET_JobBanner } from '../../dependency/UtilityFunctions';
import { SvgUri } from 'react-native-svg';


@observer
class ActiveTracker extends Component {
    constructor(props) {
        super(props);
        this.mapRef = null;
        this.state = {
            donshowcomponent:true
        }
        selectedItems = [];

        this.toggleSuccessPostModalShow = this.toggleSuccessPostModalShow.bind(this)
    }


    toggleSuccessPostModalShow = () => {
        DashboardStore._ToggleSucessPostModal(true)
    };
    async componentDidMount() {
     const{currentUser}=DashboardStore.UserAnalytics;
    //  alert(currentUser)
        DashboardStore.__GetActivityTracker(currentUser,"UserAnalytics");
        DashboardStore.__GetcompletedJobsLevel(currentUser,"UserAnalytics");
        DashboardStore.__GettimeToCompleteShortlist(currentUser,"UserAnalytics");
        DashboardStore.__RecruiterComplitedByIndustry(currentUser,"UserAnalytics",1,20);

        }
    render() {
        const{UserAnalytics}=DashboardStore;
        const JobCompleted = UserAnalytics.recruiterComplitedByIndustry.map((item,index) => {
            // console.log(item);
             return (
                <View style={styles.progressStatusWrap}
                key={index}
                >
                <View style={styles.progressItemsWrap}>
                    <Text style={styles.txt2}>
                        {item.title}
                    </Text>
                    <Text style={styles.txt2}>
                    {item.industry_count}
                        </Text>
                </View>
                <ProgressBar progress= {item.industry_count} borderWidth={0} unfilledColor={"#E8E9EB"} color={"#304FFD"} width={null} />
            </View>
                );
        
          });
        return (

            <Container style={styles.container}>
                {/* <CustomizeHeader
                    leftside='back'
                    title='empty'
                    rightside='empty'
                    statedColor='#fff'
                    icon='md-close'
                /> */}
                <Button transparent full style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start', paddingLeft: 15, paddingRight: 15 }} onPress={() => { this.props.navigation.goBack() }}>
                    <SvgUri
                        // style={{ marginTop: 20, marginLeft: -17 }}
                        width='11px'
                        height='22px'
                        uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1597714885/Mobile_Assets/arrBackBlk_ralvt8.svg"
                    />
                   
                </Button>
                <View style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center', paddingLeft: 25, paddingRight: 25, marginTop: 30, marginBottom: 13, width: "100%" }}>
                    <View full style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center', padding: 12, backgroundColor: "#1E93E2", borderRadius: 5, width: "100%" }}>
                        <Text style={styles.activityStatus}>
                            ACTIVITY
                   </Text>
                    </View>
                </View>

                <ScrollView
                    contentContainerStyle={{
                        paddingLeft: 25,
                        paddingRight: 25,
                        paddingBottom: 25,
                        minHeight: '90%',
                        display: 'flex',
                        justifyContent: 'space-between',
                    }}
                >
                    <View>

                    <Grid>
                            <Col>
                                <TouchableOpacity style={styles.btnCharts}>
                                    <Image
                                        source={{ uri: "https://res.cloudinary.com/workbrook-hash/image/upload/v1597489510/Mobile_Assets/barChart_ybtkip.png" }}
                                        style={{ width: 81, height: 40 }}
                                    />
                                    <View style={styles.btnChartsTextWrap}>
                                        <Text style={[styles.btnChartsText,]}>Total Requests</Text>
                                        <Text style={[styles.txt3_2,]}>  {UserAnalytics.totalRequestAndAcceptance.total_request} </Text>
                                    </View>
                                </TouchableOpacity>
                            </Col>
                            <Col>
                                <TouchableOpacity style={styles.btnCharts}>
                                    <Image
                                        source={{ uri: "https://res.cloudinary.com/workbrook-hash/image/upload/v1597721411/Mobile_Assets/ChartBarOrg_bqduta.png" }}
                                        style={{ width: 81, height: 40 }}
                                    />
                                    <View style={styles.btnChartsTextWrap}>
                                        <Text style={[styles.btnChartsText,]}>Acceptance Rate</Text>
                <Text style={[styles.txt3_2,]}>   {UserAnalytics.totalRequestAndAcceptance.acceptance_rate} %</Text>
                                    </View>
                                </TouchableOpacity>
                            </Col>
                        </Grid>
                        <View style={styles.Linechart}>
                            <Text style={[styles.txt7, { marginBottom: 19 }]}>Completed Jobs (Level)</Text>
                            <View style={styles.progressStatusWrap}>
                                <View style={styles.progressItemsWrap}>
                                    <Text style={styles.txt2}>
                                        Gradaute

                                     </Text>
                                    <Text style={styles.txt2}> 
                                    {UserAnalytics.completedJobsLevel.completed_graduate}
                                        </Text>
                                </View>
                                <ProgressBar progress={UserAnalytics.completedJobsLevel.completed_graduate} borderWidth={0} unfilledColor={"#E8E9EB"} color={"#304FFD"} width={null} />
                            </View>
                            <View style={styles.progressStatusWrap}>
                                <View style={styles.progressItemsWrap}>
                                    <Text style={styles.txt2}>
                                        Non-Manager
                                    </Text>
                                    <Text style={styles.txt2}>
                                    {UserAnalytics.completedJobsLevel.completed_nonManager}
                                        </Text>
                                </View>
                                <ProgressBar progress={UserAnalytics.completedJobsLevel.completed_nonManager} borderWidth={0} unfilledColor={"#E8E9EB"} color={"#FF965D"} width={null} />
                            </View>
                            <View style={styles.progressStatusWrap}>
                                <View style={styles.progressItemsWrap}>
                                    <Text style={styles.txt2}>
                                        Manager
                                    </Text>
                                    <Text style={styles.txt2}>
                                    {UserAnalytics.completedJobsLevel.completed_manager}

                                        </Text>
                                </View>
                                <ProgressBar progress={UserAnalytics.completedJobsLevel.completed_manager} borderWidth={0} unfilledColor={"#E8E9EB"} color={"#FFD240"} width={null} />
                            </View>
                            <View style={styles.progressStatusWrap}>
                                <View style={styles.progressItemsWrap}>
                                    <Text style={styles.txt2}>
                                        Senior Manager
                                    </Text>
                                    <Text style={styles.txt2}>
                                    {UserAnalytics.completedJobsLevel.completed_seniorManager}

                                        </Text>
                                </View>
                                <ProgressBar progress={UserAnalytics.completedJobsLevel.completed_seniorManager} borderWidth={0} unfilledColor={"#E8E9EB"} color={"#49C96D"} width={null} />
                            </View>
                            <View style={styles.progressStatusWrap}>
                                <View style={styles.progressItemsWrap}>
                                    <Text style={styles.txt2}>
                                        Executive
                                    </Text>
                                    <Text style={styles.txt2}>
                                    {UserAnalytics.completedJobsLevel.completed_executiive}
                                        </Text>
                                </View>
                                <ProgressBar progress={UserAnalytics.completedJobsLevel.completed_executiive} borderWidth={0} unfilledColor={"#E8E9EB"} color={"#FF5964"} width={null} />
                            </View>
                        </View>
 
<View style={styles.Linechart}>
<Text style={[styles.txt7, { marginBottom: 19 }]}>Completed Jobs (Industry)</Text>
 
{JobCompleted}
</View>

                    
                        <View style={styles.Linechart}>
                            <View style={styles.progressStatusWrap}>
                                <View style={styles.progressItemsWrap}>
                                    <View>
                                        <Text style={styles.txt3}>
                                            Time to Complete Shortlist
                                    </Text>
                                        <View style={styles.shortlistTime}>
                                            <Text style={styles.txt3_0}>
                                            {UserAnalytics.timeToCompleteShortlist.average_days} Days
                                    </Text>
                                            <Text style={styles.txt5}>
                                                Avg.
                                    </Text>
                                        </View>
                                    </View>
                                    <View style={styles.UnionWrap}>
                                        <SvgUri
                                            // style={{ marginTop: 20, marginLeft: -17 }}
                                            width='40px'
                                            height='40px'
                                            uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1597722161/Mobile_Assets/Union_q3rtqf.svg"
                                        />

                                    </View>
                                </View>

                            </View>
                        </View>
                       




                    </View>

                </ScrollView>

                {/* <Modal style={styles.MContainer} isVisible={DashboardStore.isSuccessPostModalVisible} >
                    <Button onPress={this.toggleSuccessPostModalClose} style={{ backgroundColor: "transparent", position: 'absolute', right: 10, top: 40, padding: 20, elevation: 0 }}>
                        <Image source={CloseModal} style={{ width: 19, height: 19, }} />
                    </Button>
                    <View style={[styles.Mwrapper, { height: 380 }]}>
                        <View style={{ paddingLeft: 29, paddingRight: 29, display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                            <Image source={SuccessIcon} style={{ width: 129, height: 129, marginBottom: 17 }} />
                            <Text style={styles.cardText1, { fontSize: 16, color: '#5ABC7A', textAlign: 'center', fontWeight: '600' }}>
                                Congratulations!
                                        </Text>
                            <Text style={styles.cardText1, { fontSize: 16, color: '#5ABC7A', textAlign: 'center', fontWeight: '600' }}>

                                Your job was successfully posted.
                            </Text>
                        </View>

                    </View>
                </Modal>   */}

            </Container>


        );
    }
}
export default ActiveTracker;

const styles = StyleSheet.create({
    container: {
        backgroundColor: "#FAFAFA",
        display: "flex",
        flex: 1,
        justifyContent: "flex-start",
    },
    btnCharts: {
        display: "flex",
        alignItems: "center",
        justifyContent: 'space-evenly',
        backgroundColor: '#fff',
        minHeight: 133,
        margin: 5,
        borderRadius: 20
    },
    Linechart: {
        display: "flex",
        // alignItems: "center",
        // justifyContent: 'space-evenly',
        backgroundColor: '#fff',
        padding: 22,
        minHeight: 100,
        margin: 5,
        borderRadius: 20
    },
    progressItemsWrap: {
        display: "flex",
        alignItems: "center",
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: "100%",
        marginBottom: 4,
    },
    shortlistTime: {
        display: "flex",
        alignItems: "flex-end",
        flexDirection: 'row',
        justifyContent: 'flex-start',
        marginTop: 5
    },
    progressStatusWrap: {
        display: "flex",
        width: '100%',
        marginBottom: 14,
    },
    btnChartsTextWrap: {
        display: "flex",
        alignItems: "center",
        justifyContent: 'center',
    },
    btnChartsText: {
        fontSize: 12,
        color: '#8A9099',
        // marginTop: 14
    },
    UnionWrap: {
        padding: 16,
        backgroundColor: "#FFF5EF",
        borderRadius: 10
    },

    activityStatus: {
        fontSize: 10,
        color: '#FAFAFA',
        fontWeight: '500'
    },
    txt2: {
        fontSize: 12,
        color: '#3F434A',
    },
    txt3: {
        fontSize: 13,
        color: '#8A9099',
    },
    txt5: {
        fontSize: 15,
        color: '#49C96D',
    },
    txt7: {
        fontSize: 17,
        color: '#3F434A',
        fontWeight: '500'
    },
    txt3_2: {
        fontSize: 32,
        color: '#3F434A',
        fontWeight: '500'
    },
    txt3_0: {
        fontSize: 30,
        color: '#3F434A',
        fontWeight: '500'
    },


});

import React, { Component, Fragment } from 'react';
import { Text, StyleSheet, ImageBackground, Keyboard, TouchableOpacity, Alert } from 'react-native';
import { KeyboardAvoidingView, Image, ScrollView } from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons';
import Checker from '../../assets/workbrookAssets/checker.png';
import {
    Container,
    Header,
    Content,
    Textarea,
    Button,
    View,
    Thumbnail,
    Form,
    Item,
    Input,
    Label,
    Badge,
    Icon,
    Footer,
    Picker,
    Item as FormItem,
    Left,
    Body,
    CardItem,
    Card,
    Right,
    ListItem,
    List,
    CheckBox,
} from 'native-base';
import {
    Kaede,
    Hoshi,
    Jiro,
    Isao,
    Madoka,
    Akira,
    Hideo,
    Kohana,
    Makiko,
    Sae,
    Fumi,
} from 'react-native-textinput-effects';
import CustomizeHeader from '../frags/CustomizeHeader';

import { myspiner, temp, ShowNotification } from '../../dependency/UtilityFunctions';
import Logo from '../../assets/workbrookAssets/logo.png';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import RBSheet from "react-native-raw-bottom-sheet";
import Interests from '../interests/Interests';
import Noti from "../../assets/workbrookAssets/noti.png";
import UserBl from "../../assets/workbrookAssets/userBl.png";
import ArrForward from "../../assets/workbrookAssets/arrfr.png";
import Shield from "../../assets/workbrookAssets/shield.png";
import Quest from "../../assets/workbrookAssets/quest.png";
import Info from "../../assets/workbrookAssets/info.png";
import Plus from "../../assets/workbrookAssets/plus.png";
import Remark from "../../assets/workbrookAssets/remark.png";
import Like from "../../assets/workbrookAssets/like.png";
import MoreIcon from "../../assets/workbrookAssets/more2.png";
import Profile from "../../assets/images/profile/dp2.png";
import cardPix from "../../assets/images/background/bg3_old.jpg";
import RNPickerSelect from 'react-native-picker-select';
import TagInput from 'react-native-tags-input';

import RNFetchBlob from 'react-native-fetch-blob'
import ImagePicker from 'react-native-image-picker';
import CloseModal from "../../assets/workbrookAssets/whitecloss.png";
import SuccessIcon from "../../assets/workbrookAssets/successPost.png";
import Modal from 'react-native-modal';
import AccountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import { text } from 'react-native-communications';
import DashboardStore from '../../stores/Dashboard';
import { hasSetIntrest } from '../../auth';
import Autocomplete from 'react-native-autocomplete-input';
import MultiSelect from 'react-native-multiple-select';
import { YOUR_CLOUDINARY_PRESET, YOUR_CLOUDINARY_NAME, requestCameraPermission, YOUR_CLOUDINARY_PRESET_JobBanner } from '../../dependency/UtilityFunctions';

let industryRecruitementExperience = [
    {
        id: '1-3 Years',
        name: '1-3 Years',
    }, {
        id: '4-6 Years',
        name: '4-6 Years',
    }, {
        id: '7-9 Years',
        name: '7-9 Years',
    }, {
        id: '10 - above',
        name: '10 - above',
    }
];
@observer
class RecruiterSearch extends Component {


    constructor(props) {
        super(props);
        this.mapRef = null;
        this.state = {
            recruiterIndustry: [],
            industryRecruitementExperience: [],

        }

    }
    async componentDidMount() {
        DashboardStore._UpdateDefaultValue();
        DashboardStore.UpdateNavigationState(this.props.navigation);

        let UserloginToken = await temp();
        // let loggedinUserdetails=await loggedinUserdetails();
        console.log(UserloginToken);
        DashboardStore._getAxiosInstance(UserloginToken);
        DashboardStore.__GetUserIndustries();
        // DashboardStore.__ALLSkillWithOutFilter(1, 30);
    //    this.SetDefaultValue();
      DashboardStore.UpdateNavigationState(this.props.navigation);

    }
    OnRecruiterIndustryChange = recruiterIndustry => {
        this.setState({ recruiterIndustry });
        // console.log
        let item = recruiterIndustry[0];
        DashboardStore._updateParameterV2(item, "industry", "FilterRecruiter");

    };

    async FilterRecruiter() {

        // const { recruiterIndustry } = this.state;
        const { FilterRecruiter } = DashboardStore
 
 
    

//              let mycurency =curency[0];
// let averageAmount=  mycurency + " " + recruiterCharge ;
//         // let isCompanyRepresentative = BondWithCompany === true ? 1 : 0;
//         DashboardStore._updateParameterV2(recruiterIndustry, "industry", "BecomeRecruiter");
//         DashboardStore._updateParameterV2(averageAmount, "averageAmount", "BecomeRecruiter");
//         // DashboardStore._updateParameterV2(isCompanyRepresentative, "isCompanyRepresentative", "BecomeRecruiter");




        console.log(FilterRecruiter);
        DashboardStore.__FilterRecuiterSearch();
    


    };

    render() {

       
        // const { selectedItems } = this.state;
        const {recruiterIndustry } = this.state;
        const {isGoBack } = DashboardStore;
        if(isGoBack===true){
            this.props.navigation.goBack();
            DashboardStore._UpdateDefaultValue();
        }

        return (

            <Container style={styles.container}>
                <CustomizeHeader
                    leftside='WhiteArr'
                    title='Available Recruiters'
                    rightside='empty'
                    statedColor='#000'
                    icon='md-close'
                    savehandler={this.proceednow}
                />

                <ScrollView
                    contentContainerStyle={{
                        paddingLeft: 25,
                        paddingRight: 25,
                    }}
                >
                    <View style={styles.headerWrap}>
                <Text style={styles.cardTextJob1}>{DashboardStore.FilterRecruiter.HeaderText}</Text>
                    </View>
                    <View style={{ marginBottom: 20 }}>
                        <Label style={styles.cardTextJob2}>Industry</Label>
                        {/* <Item regular style={styles.inputStyleWrap}>

                            <Input />
                        </Item> */}
      <View style={[styles.bottomIndBorder,]}>
                        {/* <Item style={[styles.bottomIndBorder, { height: 55 }]}> */}
                        <MultiSelect
                            hideTags={true}
                            items={DashboardStore.AllIndustries.Industry}
                            uniqueKey="id"
                            ref={(component) => { this.multiSelect = component }}
                            onSelectedItemsChange={this.OnRecruiterIndustryChange}
                            selectedItems={this.state.recruiterIndustry}
                            selectText="Industry"
                            searchInputPlaceholderText="Search Industry"
                            onChangeInput={(text) => console.log(text)}
                            tagRemoveIconColor="#FF5964"
                            tagBorderColor="#CCC"
                            tagTextColor="#CCC"
                            selectedItemTextColor="#CCC"
                            selectedItemIconColor="#CCC"
                            itemTextColor="#fff"
                            textColor="#A7A7A7"
                            tagTextColor="#A7A7A7"
                            displayKey="name"
                            searchInputStyle={{ color: '#CCC', borderWidth: 0, borderColor: "#ccc", fontWeight: "300" }}
                            styleItemsContainer={{ borderWidth: 1, borderColor: '#1E93E2', backgroundColor: "#1E93E2", fontWeight: "300" }}
                            submitButtonColor="#CCC"
                            submitButtonText="Submit"
                            itemFontSize={10}
                            fontSize={10}
                            single={true}
                            styleDropdownMenuSubsection={{ borderBottomWidth: 0, color: '#A7A7A7', backgroundColor: 'transparent', height: 35, paddingLeft: 5, paddingBottom: 0, marginBottom: -20, fontSize: 10, fontWeight: "300" }}
                            selectedItemIconColor={'#fff'}

                            hideSubmitButton={true}
                        />
                        {/* <View style={{ fontSize: 10 }}>
                            {this.multiSelect ? this.multiSelect.getSelectedItemsExt(recruiterIndustry) : null}

                        </View> */}
                    </View>


                    </View>
                    <View style={{ marginBottom: 20 }}>
                        <Label style={styles.cardTextJob2}>Experience</Label>
                        {/* <Item regular style={styles.inputStyleWrap}>

                            <Input />
                        </Item> */}
                           <View style={[styles.bottomIndBorder, {flex: 1} ]}>
                        <MultiSelect
                            hideTags
                            items={industryRecruitementExperience}
                            uniqueKey="id"
                            ref={(component) => { this.multiSelect = component }}
                             onSelectedItemsChange={(industryRecruitementExperience) => {
                                // alert("hey man")
                                console.log(industryRecruitementExperience)
                                this.setState({
                                    industryRecruitementExperience
                                })
                                console.log()
                                let item = industryRecruitementExperience[0];
                                DashboardStore._updateParameterV2(item, "industryRecruitementExperience", "FilterRecruiter");

                            }
                            }
                            selectedItems={this.state.industryRecruitementExperience}
                            selectText="Industry recruitment experience"
                            searchInputPlaceholderText=""
                            onChangeInput={(text) => console.log(text)}
                            tagRemoveIconColor="#FF5964"
                            tagBorderColor="#CCC"
                            tagTextColor="#CCC"
                            selectedItemTextColor="#CCC"
                            selectedItemIconColor="#CCC"
                            itemTextColor="#fff"
                            textColor="#A7A7A7"
                            displayKey="name"
                            searchInputStyle={{ color: '#CCC', borderWidth: 0, borderColor: "#ccc", fontWeight: "300" }}
                            styleItemsContainer={{ borderWidth: 1, borderColor: '#1E93E2', backgroundColor: "#1E93E2",}}
                            submitButtonColor="#CCC"
                            submitButtonText="Submit"
                            itemFontSize={10}
                            fontSize={10}
                            styleDropdownMenuSubsection={{ borderBottomWidth: 0, backgroundColor: 'transparent', height: 35, paddingLeft: 5, paddingBottom: 0, marginBottom: -20, fontSize: 10, fontWeight: "300" }}
                            selectedItemIconColor={'#fff'}
                            hideSubmitButton={true}
                            single={true}
                            textInputProps={{ editable: false, autoFocus: false }}
                            searchIcon={null}
                        />
                    </View>

                    </View>
                    <View style={{ marginBottom: 20 }}>
                        <Label style={styles.cardTextJob2}>Location</Label>
                        <Item regular style={styles.inputStyleWrap}>

                            <Input 
                            
                            onChangeText={(location) => DashboardStore.onChangeText('location', location, 'FilterRecruiter')}
                            keyboardType={'default'}
                            />
                        </Item>
                    </View>
                    <Button transparent full style={styles.serachbtnSty}
                            onPress={this.FilterRecruiter}
                    >
                        <Text style={styles.cardTextJob2_2}> {DashboardStore.isProcessing===true ? "processing...":'Search'} </Text>
                    </Button>



                </ScrollView>



            </Container>


        );
    }
}
export default RecruiterSearch;

const styles = StyleSheet.create({
    container: {
        backgroundColor: "white",
        display: "flex",
        flex: 1,
        justifyContent: "flex-start",
    },

    rmPadding: {
        paddingTop: 0,
        paddingRight: 0,
        paddingBottom: 0,
        paddingLeft: 0,
    },
    rmPaddingLR: {
        paddingLeft: 0,
        paddingRight: 0,
    },

    cardTextJob1: {
        color: "#2F2F2F",
        fontSize: 36,
        fontWeight: '600',

    },
    cardTextJob2: {
        color: "#BCBCBC",
        fontSize: 14,
        fontWeight: '600',
        marginBottom: 5,
    },
    cardTextJob2_2: {
        color: "#fff",
        fontSize: 14,
        fontWeight: '600',
    },

    cardTextJob3: {
        color: "#2F2F2F",
        fontSize: 11,
        fontWeight: '300',
        marginBottom: -20,
    },
    cardTextJob4: {
        color: "#1E93E2",
        fontSize: 11,
        fontWeight: '500',
    },
    cardTextJob4_1: {
        color: "#fff",
        fontSize: 12,
        fontWeight: '500',
    },
    cardTextJob5: {
        color: "#1E93E2",
        fontSize: 13,
        fontWeight: '600',
    },
    cardTextJob6: {
        color: "#1E93E2",
        fontSize: 11,
        fontWeight: '500',
    },
    cardTextJob7: {
        fontSize: 12,
        color: '#2F2F2F',
    },

    cardText1: {
        color: "#2F2F2F",
        fontWeight: '500',
        fontSize: 12,
    },
    inputStyleWrap: {
        height: 38,
        backgroundColor: '#E5E5E5',
        borderRadius: 5,
    },
    headerWrap: {
        marginBottom: 60,
        marginTop: 20,
    },
    serachbtnSty: {
        height: 54,
        borderRadius: 4,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#5ABC7A',
        marginTop: 24
    },

});

import React, { Component, Fragment } from 'react';
import { Text, StyleSheet, ImageBackground, Keyboard, TouchableOpacity, Alert } from 'react-native';
import { KeyboardAvoidingView, Image, ScrollView } from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons';
import Checker from '../../assets/workbrookAssets/checker.png';
import {
    Container,
    Header,
    Content,
    Textarea,
    Button,
    View,
    Thumbnail,
    Form,
    Item,
    Input,
    Label,
    Badge,
    Icon,
    Footer,
    Picker,
    Item as FormItem,
    Left,
    Body,
    CardItem,
    Card,
    Right,
    ListItem,
    List,
    CheckBox,
} from 'native-base';
import {
    Kaede,
    Hoshi,
    Jiro,
    Isao,
    Madoka,
    Akira,
    Hideo,
    Kohana,
    Makiko,
    Sae,
    Fumi,
} from 'react-native-textinput-effects';
import CustomizeHeader from '../frags/CustomizeHeader';

import { myspiner, temp, ShowNotification } from '../../dependency/UtilityFunctions';
import Logo from '../../assets/workbrookAssets/logo.png';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import RBSheet from "react-native-raw-bottom-sheet";
import Interests from '../interests/Interests';
import Noti from "../../assets/workbrookAssets/noti.png";
import UserBl from "../../assets/workbrookAssets/userBl.png";
import ArrForward from "../../assets/workbrookAssets/arrfr.png";
import Shield from "../../assets/workbrookAssets/shield.png";
import Quest from "../../assets/workbrookAssets/quest.png";
import Info from "../../assets/workbrookAssets/info.png";
import Plus from "../../assets/workbrookAssets/plus.png";
import Remark from "../../assets/workbrookAssets/remark.png";
import Like from "../../assets/workbrookAssets/like.png";
import MoreIcon2 from "../../assets/workbrookAssets/more2.png";
import Profile from "../../assets/images/profile/dp2.png";
import cardPix from "../../assets/images/background/bg3_old.jpg";
import RNPickerSelect from 'react-native-picker-select';
import TagInput from 'react-native-tags-input';
import MoreIcon from "../../assets/workbrookAssets/more.png";
import BlueClose from '../../assets/workbrookAssets/blueClose.png';
import Check from '../../assets/workbrookAssets/check.png';
import { Col, Row, Grid } from "react-native-easy-grid";

// import RNFetchBlob from 'react-native-fetch-blob'
import ImagePicker from 'react-native-image-picker';
import CloseModal from "../../assets/workbrookAssets/whitecloss.png";
import SuccessIcon from "../../assets/workbrookAssets/successPost.png";
import Drop from "../../assets/workbrookAssets/drop2.png";
import Modal from 'react-native-modal';
import AccountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import { text } from 'react-native-communications';
import DashboardStore from '../../stores/Dashboard';
import { hasSetIntrest } from '../../auth';
import Autocomplete from 'react-native-autocomplete-input';
import MultiSelect from 'react-native-multiple-select';
import { YOUR_CLOUDINARY_PRESET, YOUR_CLOUDINARY_NAME, requestCameraPermission, YOUR_CLOUDINARY_PRESET_JobBanner } from '../../dependency/UtilityFunctions';
import { SvgUri } from 'react-native-svg';



items = [{
    id: 'Ondo',
    name: 'Ondo',
}, {
    id: 'Ogun',
    name: 'Ogun',
}, {
    id: 'Calabar',
    name: 'Calabar',
}, {
    id: 'Kaduna',
    name: 'Kaduna',
}, {
    id: 'Abuja',
    name: 'Abuja',
}];
let NumberOfStaff = [{
    id: '2',
    name: '1-10',
}, {
    id: '3',
    name: '4',
}, {
    id: '21-30',
    name: '21-30',
}, {
    id: '30-50',
    name: '30-50',
}, {
    id: '50 and Above',
    name: '50 and Above',
}];
let defaultindustries = [{
    id: 'Tech',
    name: 'Tech',
}, {
    id: 'business',
    name: 'business',
}, {
    id: '16hbajsabsd',
    name: 'Calabar',
}, {
    id: 'sdhyaysdj',
    name: 'Kaduna',
}, {
    id: 'suudydjsjd',
    name: 'Abuja',
}];
@observer
class RecruiterProfile extends Component {
    constructor(props) {
        super(props);
        this.mapRef = null;
        this.state = {
            email: '',
            password: '',
            errorloading: false,
            AmCompanyRep: true,
            selectedItems: [],
            industry: [],
            numberOfStaff: [],
            jobLevel: [],
            JobIntrests: [],
            tags: {
                tag: '',
                tagsArray: []
            },

            avatarSource: null,
            cover_pix: null,
            isImagefromdevice: false,
            loader_visible: false,
            showsaveBtn: false,
            uploading: false,
            file2upload: null,
            ViewMode:''


        }
        selectedItems = [];

        this.changeAmCompanyRep = this.changeAmCompanyRep.bind(this)
        this.toggleSuccessPostModalShow = this.toggleSuccessPostModalShow.bind(this)
    }
    onSelectedItemsChange = selectedItems => {
        this.setState({ selectedItems });
    };
    async componentDidMount() {
        let data =this.props.navigation.getParam('data','');

this.setState({
    ViewMode:data
})
        DashboardStore.UpdateNavigationState(this.props.navigation);

        let UserloginToken = await temp();
        // let loggedinUserdetails=await loggedinUserdetails();
        console.log(UserloginToken);
        DashboardStore._getAxiosInstance(UserloginToken);
        //  DashboardStore.__GetIndustries(1,10);
        DashboardStore.__ALLSkillWithOutFilter(1, 30);
        DashboardStore.__JobsLevels();
        // ViewOnly
         const { SingleRecruiter } = DashboardStore.Recruiters;
let currentUser=SingleRecruiter.id;
        DashboardStore._updateParameterV2(currentUser, "currentUser", "UserAnalytics");

    }

    onSelectedItemsChange = selectedItems => {
        this.setState({ selectedItems });

    };
    changeAmCompanyRep = () => {
        const { AmCompanyRep } = this.state;
        console.log(AmCompanyRep)
        this.setState({
            AmCompanyRep: !AmCompanyRep
        })
        // this.setState({ selectedItems });

    };
    toggleSuccessPostModalShow = () => {
        DashboardStore._ToggleSucessPostModal(true)
    };
    toggleSuccessPostModalClose = () => {
        DashboardStore._ToggleSucessPostModal(false)
    };
    SelectRecruiter = (recruiter_id) => {
        console.log(recruiter_id)
        DashboardStore.__ToggleRecruiterArray(recruiter_id);
        this.props.navigation.goBack();


    };
    render() {
        // const { selectedItems } = this.state;
        const { selectedItems, JobIntrests, uploading,ViewMode } = this.state;
        const { isImagefromdevice, avatarSource, loader_visible, showsaveBtn } = this.state;
        let renderImage;

        if (isImagefromdevice === true) {
            renderImage = <Thumbnail square
                large
                source={avatarSource}
                style={{ height: 195, width: '100%', }}

            />
        }

        const { SingleRecruiter } = DashboardStore.Recruiters;
        console.log(SingleRecruiter)
        let Recruiter_industries= null;
         if(SingleRecruiter.industry !=null){
            Recruiter_industries=  
            SingleRecruiter.industry.map(function(item,index){
              return  (    <TouchableOpacity style={styles.btnView} key={index}>
                <Text style={[styles.cardTextJob4_2, { fontWeight: "bold" }]}>
                    {item}
            </Text>
            </TouchableOpacity>)
            })
        }
   


        return (

            <Container style={styles.container}>
                <Button transparent full style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end', paddingLeft: 15, paddingRight: 15 }} onPress={() => { this.props.navigation.goBack() }}>
                    <SvgUri
                        width="18px"
                        height="18px"
                        uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593153205/Mobile_Assets/closeBlk_elvoro.svg"
                    />
                </Button>

                <ScrollView
                    contentContainerStyle={{
                        paddingLeft: 30,
                        paddingRight: 30,
                        paddingBottom: 30,
                        minHeight: '90%',
                        display: 'flex',
                        justifyContent: 'space-between',
                    }}
                >
                    <View>
                        <View style={styles.imageHolder}>
                            {SingleRecruiter.profilePicture === ""
                                ?
                                <Thumbnail square
                                    large
                                    source={Profile}
                                    style={{ height: 132, width: 128, borderRadius: 132 / 2 }}
                                />
                                :
                                <Thumbnail square
                                    large
                                    source={{ uri: SingleRecruiter.profilePicture }}
                                    style={{ height: 132, width: 128, borderRadius: 132 / 2 }}
                                />

                            }

                            <Button style={{ width: 29, height: 29, borderRadius: 29 / 2, display: 'flex', justifyContent: 'center', alignItems: 'center', backgroundColor: '#fff', position: 'absolute', bottom: 85, right: 98 }}>
                                <SvgUri
                                    width="21px"
                                    height="21px"
                                    uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593152748/Mobile_Assets/checkersBlk_dukn3f.svg"
                                />

                            </Button>
                            <Text style={[styles.cardTextJob7,{paddingTop:15,}]}>{SingleRecruiter.fullname}</Text>
                            <View style={[styles.textIcon]}>
                                <SvgUri
                                    width="9px"
                                    height="12px"
                                    uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593152321/Mobile_Assets/dropBlk_q9q0t6.svg"
                                />

                                {/* <Image source={Drop} style={{ marginRight: 5 }} /> */}
                                <Text note style={[styles.nameSty2, { marginLeft: 5 }]}>{SingleRecruiter.location}</Text>
                            </View>
                        </View>
                        <Grid>
                            <Col>
                            <TouchableOpacity style={styles.btnCharts}
                            onPress={() => { this.props.navigation.navigate("ActiveTracker") }}
                            >
                            <Image
                                    source={{uri: "https://res.cloudinary.com/workbrook-hash/image/upload/v1597489510/Mobile_Assets/barChart_ybtkip.png"}}
                                    style={{width: 81, height: 40}}
                                />
                                <View style={styles.btnChartsTextWrap}>
                                <Text style={[styles.btnChartsText,]}>Activity</Text>
                                <Text style={[styles.btnChartsText,]}>Tracker</Text>
                                </View>
                                </TouchableOpacity>
                            </Col>
                            <Col>
                            <TouchableOpacity style={styles.btnCharts}
                            onPress={() => { this.props.navigation.navigate("PerformanceTracker") }}
                            >
                            <Image
                                    source={{uri: "https://res.cloudinary.com/workbrook-hash/image/upload/v1597489515/Mobile_Assets/ringChat_qu1tvo.png"}}
                                    style={{width: 62, height: 62}}
                                />
                                 <View style={styles.btnChartsTextWrap}>
                                <Text style={[styles.btnChartsText,]}>Performance</Text>
                                <Text style={[styles.btnChartsText,]}>Tracker</Text>
                                </View>
                                </TouchableOpacity>
                            </Col>
                        </Grid>
                        <View style={{ marginTop: 25 }}>
                            <Label style={[styles.cardTextJob6, { color: '#2F2F2F' }]}>Profile</Label>
                            <Text style={styles.cardTextJob3}>
                                {/* Your personal profile is not the place to be humble; it is your chance to be noticed and tell the potential employer exactly who you ‘really’ are and what you can do for them. */}
                                {SingleRecruiter.summary}
                                {/* {JSON.stringify(SingleRecruiter)} */}
                            </Text>
                        </View>
                        <View style={{ marginTop: 25 }}>
                            <Label style={[styles.cardTextJob6, { color: '#2F2F2F' }]}>
                                Recruitment Experience
                        </Label>
                            <Text style={styles.cardTextJob3}>
                              {SingleRecruiter.industryRecruitementExperience}
                        </Text>
                        </View>
                        <View style={{ marginTop: 25 }}>
                            <Label style={[styles.cardTextJob6, { color: '#2F2F2F' }]}>
                               Average Charges 
                        </Label>
                            <Text style={styles.cardTextJob3}>
                              {SingleRecruiter.averageAmount}
                        </Text>
                        </View>
                        <View style={{ marginTop: 25 }}>
                            <Label style={[styles.cardTextJob6, { color: '#2F2F2F' }]}>
                            Area of Expertise
                        </Label>
                            {/* <Text style={styles.cardTextJob3}>
                        {SingleRecruiter.industry==null ? 'Not Available':SingleRecruiter.industry.join(', ')}
                        </Text> */}
                            <View style={styles.btnViewWrap}>
                                {/* <TouchableOpacity style={styles.btnView}>
                                    <Text style={[styles.cardTextJob4_2, { fontWeight: "bold" }]}>
                                        Finance
                                        {JSON.stringify(SingleRecruiter)}
                                </Text>
                                </TouchableOpacity> */}
                                {/* <TouchableOpacity style={styles.btnView}>
                                    <Text style={[styles.cardTextJob4_2, { fontWeight: "bold" }]}>
                                        Marketing
                                </Text>
                                </TouchableOpacity> */}
                                {Recruiter_industries}
                            </View>
                        </View>
                        {/* <View style={{marginTop: 30}}>
                        <Label style={[styles.cardTextJob6, { color: '#2F2F2F' }]}>
                            Charges
                        </Label>
                        <Text style={styles.cardTextJob3}>
                        {SingleRecruiter.averageAmount}
                         </Text>
                    </View>  */}
                    {SingleRecruiter.linkdnUrl ==='' ?
                    null
                    :
                       <View style={{ marginTop: 25 }}>
                            <Label style={[styles.cardTextJob6, { color: '#2F2F2F' }]}>
                                LinkedIn
                        </Label>
                            <Text style={styles.cardTextJob3}>
                                {SingleRecruiter.linkdnUrl}
                         </Text>
                        </View>

                    }
                     
                    </View>
                    {
                        ViewMode==='ViewOnly' ?
                        null
                        :
                        <Button transparent full style={styles.serachbtnSty}
                        onPress={() => this.SelectRecruiter(SingleRecruiter.id)}
                    >
                        {
                                                         DashboardStore.JobPosting.SelectedRecuiter.includes(SingleRecruiter.id) === false ?  <Text style={styles.cardTextJob2_3}> Select Recruiter </Text>
                                                         :                         <Text style={styles.cardTextJob2_3}> Remove Recruiter </Text>


                        }
                        
                    </Button>
                    }
                
                </ScrollView>

                {/* <Modal style={styles.MContainer} isVisible={DashboardStore.isSuccessPostModalVisible} >
                    <Button onPress={this.toggleSuccessPostModalClose} style={{ backgroundColor: "transparent", position: 'absolute', right: 10, top: 40, padding: 20, elevation: 0 }}>
                        <Image source={CloseModal} style={{ width: 19, height: 19, }} />
                    </Button>
                    <View style={[styles.Mwrapper, { height: 380 }]}>
                        <View style={{ paddingLeft: 29, paddingRight: 29, display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                            <Image source={SuccessIcon} style={{ width: 129, height: 129, marginBottom: 17 }} />
                            <Text style={styles.cardText1, { fontSize: 16, color: '#5ABC7A', textAlign: 'center', fontWeight: '600' }}>
                                Congratulations!
                                        </Text>
                            <Text style={styles.cardText1, { fontSize: 16, color: '#5ABC7A', textAlign: 'center', fontWeight: '600' }}>

                                Your job was successfully posted.
                            </Text>
                        </View>

                    </View>
                </Modal>   */}

            </Container>


        );
    }
}
export default RecruiterProfile;

const styles = StyleSheet.create({
    container: {
        backgroundColor: "#FAFAFA",
        display: "flex",
        flex: 1,
        justifyContent: "flex-start",
    },
    imageHolder: {
        display: "flex",
        alignItems: "center",
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 25,
        paddingBottom: 25,
        backgroundColor: 'transparent',

    },
    btnCharts: {
        display: "flex",
        alignItems: "center",
        justifyContent: 'space-evenly',
        backgroundColor: '#fff',
        minHeight: 133,
        margin:5,
        borderRadius: 20
    },
    btnChartsTextWrap: {
        display: "flex",
        alignItems: "center",
        justifyContent: 'center',
    },
    btnChartsText: {
        fontSize: 12,
        color: '#8A9099',
        // marginTop: 14
    },
    recruiterInfoWrap: {
        display: "flex",
        alignItems: "flex-start",
        flexDirection: 'row',
        paddingLeft: 15,
        paddingRight: 15,
        marginTop: 10
    },
    recruiterInfoText1Wrap: {
        display: "flex",
        alignItems: "flex-start",
        flexDirection: 'row',
        color: '#A7A7A7',
        fontSize: 10,
        fontWeight: '300',
        width: 100,
        paddingLeft: 5,
    },
    recruiterInfoText2Wrap: {
        flex: 1,
        display: "flex",
        alignItems: "flex-start",
        flexDirection: 'row',
        paddingLeft: 5,
        // paddingRight: 15,
        color: '#3C3C3C',
        fontSize: 12,
        fontWeight: 'normal',
    },
    btnNewPost: {
        height: 38,
        elevation: 0,
        minWidth: '90%',
        marginLeft: 15,
        marginRight: 15,
    },
    IconSetsty1: {
        maxWidth: 16,
        maxHeight: 17,
    },
    IconSetsty: {
        maxWidth: 16,
        maxHeight: 16,
    },
    iconWrapSet: {
        width: 20,
        marginRight: 10
    },
    IconSetsty2: {
        width: 8,
        height: 15,

    },
    bottomIndBorderNon: {
        marginLeft: 15,
        marginRight: 15,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        // borderBottomWidth: 1,
        // borderColor: '#E5E5E5'
    },
    bottomIndBorder: {
        marginLeft: 15,
        marginRight: 15,
        borderBottomWidth: 1,
        borderColor: '#E5E5E5',
    },
    rmPadding: {
        paddingTop: 0,
        paddingRight: 0,
        paddingBottom: 0,
        paddingLeft: 0,
    },
    rmPaddingLR: {
        paddingLeft: 0,
        paddingRight: 0,
    },

    cardTextJob1: {
        color: "#2F2F2F",
        fontSize: 11,
    },
    cardTextJob2: {
        color: "#676767",
        fontSize: 9,
        fontWeight: '300',
    },
    cardTextJob2_2: {
        color: "#2F2F2F",
        fontSize: 9,
        fontWeight: '500',
    },
    cardTextJob2_3: {
        color: "#fff",
        fontSize: 14,
        fontWeight: '600',
    },
    cardTextJob3: {
        color: "#2F2F2F",
        fontSize: 11,
        fontWeight: '300',
    },
    serachbtnSty: {
        height: 54,
        borderRadius: 4,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#5ABC7A',
        marginTop: 24
    },
    cardTextJob6: {
        color: "#FAFAFA",
        fontSize: 14,
        fontWeight: '600',
    },
    cardTextJob4: {
        color: "#1E93E2",
        fontSize: 11,
        fontWeight: '500',
    },
    cardTextJob4_2: {
        color: "#2F2F2F",
        fontSize: 11,

    },
    cardTextJob4_1: {
        color: "#fff",
        fontSize: 12,
        fontWeight: '500',
    },
    cardTextJob5: {
        color: "#2F2F2F",
        fontSize: 10,
        fontWeight: 'bold',
    },
    cardTextJob5_1: {
        color: "#000000",
        fontSize: 13,
        fontWeight: '500',
    },

    // cardTextJob7: {
    //     fontSize: 12,
    //     color: '#2F2F2F',
    // },
    
    cardTextJob7: {
        fontSize: 16,
        fontWeight: '600',
        color: '#1E93E2',
    },
    Mwrapper: {
        height: 510,
        width: '90%',
        backgroundColor: '#fff',
        // padding: 15,
        display: 'flex',
        justifyContent: 'space-around',
        // alignItems: 'center'
    },
    cardText1: {
        color: "#2F2F2F",
        fontWeight: '500',
        fontSize: 12,
    },
    MContainer: {
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
    },
    termConditionWrap: {
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
    },
    likenoti: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        paddingBottom: 16,
        paddingTop: 16,
    },
    // =============================================== drop address ==================
    textIcon: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: "center",
        width: '100%'
    },
    nameSty2: {
        fontSize: 12,
        color: '#1E93E2'
    },
    // =============================================== drop address ==================
    // =============================================== card style ========================
    rmPadding: {
        paddingTop: 0,
        paddingRight: 0,
        paddingBottom: 0,
        paddingLeft: 0,
    },
    btnActionWrap: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#5ABC7A',
        height: 54,
        elevation: 0,
        flex: 1,
    },

    // =============================================== Card style =====================
    btnViewWrap: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: 4,
        flexWrap:'wrap',
    },
    btnView: {
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
        paddingTop: 7,
        paddingBottom: 7,
        paddingLeft: 13,
        paddingRight: 13,
        borderWidth: 1,
        borderColor: "#2F2F2F",
        borderRadius: 5,
        marginRight: 4,
        marginTop: 4,
    },

});

import React, { Component } from 'react';
import {
    Text,
    Image,
    StyleSheet,
    ImageBackground,
    Keyboard,
    TouchableOpacity, FlatList
} from 'react-native';
import { KeyboardAvoidingView, Alert } from 'react-native';
import CustomizeHeader from '../frags/CustomizeHeader';

import Ionicons from 'react-native-vector-icons/Ionicons';
import {
    Container,
    Header,
    Content,
    Textarea,
    Button,
    View,
    Thumbnail,
    Form,
    Item,
    Input,
    Label,
    Badge,
    Icon,
    Footer,
    Picker,
    Left,
    Right,
    Body,
    Title,
    Center,
    CardItem,
    CheckBox
} from 'native-base';
import {
    Kaede,
    Hoshi,
    Jiro,
    Isao,
    Madoka,
    Akira,
    Hideo,
    Kohana,
    Makiko,
    Sae,
    Fumi,
} from 'react-native-textinput-effects';
import Modal from 'react-native-modal';
import { myspiner, AppInlineLoader, generateMyUniqueJobId, ShareOptions } from '../../dependency/UtilityFunctions';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import User from "../../assets/workbrookAssets/user4.png";
import CloseModal from "../../assets/workbrookAssets/whitecloss.png";
import Share from 'react-native-share';


import accountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import { ScrollView } from 'react-native-gesture-handler';
import imageUser from '../../assets/workbrookAssets/user2.png';
import DatePicker from 'react-native-datepicker'
// import Autocomplete from 'native-base-autocomplete'
import Autocomplete from 'react-native-autocomplete-input';
import GridView from 'react-native-gridview';
import Activities from './ActiveTracker2';
import PerformanceTracker2 from './PerformanceTracker2';
import { SvgUri } from 'react-native-svg';
import { withNavigation } from 'react-navigation';
import DashboardStore from '../../stores/Dashboard';
import EmptyListComponent from '../../StatelessScreens/EmptyListComponent';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import { Clipboard } from 'react-native';

const API = 'https://swapi.co/api';

@observer
class RecruiterInsights extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            isModalVisible: false,
            isModalViewApplicants: false,
            thisUniqueCode: 'nill',
            InsightView: 'ACTIVITY',
            isCopied: false,
            singleuser:{}

        }
        this.ViewShortlistedApplicant = this.ViewShortlistedApplicant.bind(this)
        this.ShortListApplicant = this.ShortListApplicant.bind(this)
        this.UnShortListApplicant = this.UnShortListApplicant.bind(this)
        this.writeToClipboard = this.writeToClipboard.bind(this)
        this.CloseViewUniquecodeModal = this.CloseViewUniquecodeModal.bind(this)
        this._changeInsightView = this._changeInsightView.bind(this)

    }

    writeToClipboard = async () => {
        let thistext = this.state.thisUniqueCode
        await Clipboard.setString(thistext);
        this.setState({
            isCopied: true
        })
        // alert('Copied to Clipboard!');
    };


    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible });
    };
    _changeInsightView = (view) => {
        this.setState({ InsightView: view });
    };
    toggleModalViewApplicants = async (item) => {
        console.log(item)
        console.log("item brymo")
        let recruiterId = item.recruiterId;
        let job_id = item.job_id;
        let JobUniqueURl = await generateMyUniqueJobId(recruiterId, job_id);
        console.log(JobUniqueURl);
        this.setState({
            isModalViewApplicants: !this.state.isModalViewApplicants,
            thisUniqueCode: JobUniqueURl,
            isCopied: false,
            singleuser:item,

        });

    };
    CloseViewUniquecodeModal = async () => {
        //         console.log(item)
        //         console.log("item brymo")
        // let recruiterId=item.recruiterId;
        // let job_id=item.job_id;
        //         let JobUniqueURl= await generateMyUniqueJobId(recruiterId,job_id);
        //         console.log(JobUniqueURl);
        this.setState({
            isModalViewApplicants: !this.state.isModalViewApplicants,
            //  thisUniqueCode:JobUniqueURl

        });
        const { thisUniqueCode, isCopied,singleuser } = this.state;
        console.log(singleuser);
        console.log("singleuser");
        let title=`Request to submit your application for ${singleuser.role_title } from ${singleuser.fullname}`;
        // const message = `Xxx (name of recruiter) will like you to submit an application for ${singleuser.role_title } at xxx (name of company) \n Please use the link below to apply for this opportunity`;
        const message = ` ${singleuser.fullname} will like you to submit an application for ${singleuser.role_title }  \n Please use the link below to apply for this opportunity`;
        let options_ = ShareOptions(thisUniqueCode, title, message);



        Share.open(options_)
            .then((res) => {
                console.log(res)
                console.log("res")

            })
            .catch((err) => {
                err && console.log(err);
                console.log("Error from share")
            });
    };

    componentDidMount() {
        DashboardStore.__GetReruitersInsight(1, 30);
    }
    ViewShortlistedApplicant = (item) => {

        DashboardStore._updateParameterV2(item, "SingleAcceptedJob", "RecruiterInsight");
        this.props.navigation.navigate("ShortListedApplicants");

    };

    ShortListApplicant = (job_id, user_id) => {
        console.log({ job_id, user_id })

        // DashboardStore._updateParameterV2(item, "SingleAcceptedJob", "RecruiterInsight");
        // this.props.navigation.navigate("ShortListedApplicants");
        Alert.alert(
            'Shortlist this Applicant ?',
            `Are you sure you want to proceed.`,
            [
                {
                    text: 'No', onPress: () => { },//Do nothing
                    style: 'cancel'
                },
                {
                    text: 'Yes', onPress: () => {
                        // DashboardStore._updateParameterV2(item, "SingleRequest", "RecruiterDashBoard");

                        DashboardStore.__ShortListOrDeclineApplicant(job_id, user_id);
                    }
                },
            ],
            { cancelable: true }
        );
    };

    ViewUserProfile = (thisUserId) => {
        DashboardStore._updateParameterV2(thisUserId, "User_id", "OtherProfile");
        this.props.navigation.navigate("otherProfile") ;
    };
    UnShortListApplicant = (job_id, user_id) => {
        console.log({ job_id, user_id })

        // DashboardStore._updateParameterV2(item, "SingleAcceptedJob", "RecruiterInsight");
        // this.props.navigation.navigate("ShortListedApplicants");
        Alert.alert(
            'Remove this Applicant ?',
            `Are you sure you want to proceed.`,
            [
                {
                    text: 'No', onPress: () => { },//Do nothing
                    style: 'cancel'
                },
                {
                    text: 'Yes', onPress: () => {
                        // DashboardStore._updateParameterV2(item, "SingleRequest", "RecruiterDashBoard");

                        DashboardStore.__ShortListOrDeclineApplicant(job_id, user_id);
                    }
                },
            ],
            { cancelable: true }
        );
    };

    render() {
        const { RecruiterInsight } = DashboardStore;
        const { thisUniqueCode, isCopied } = this.state;

        let ErrorComponentDisplay = null;

        if (DashboardStore.isProcessing === true) {
            ErrorComponentDisplay = AppInlineLoader(DashboardStore.isProcessing)

        }
        else {
            ErrorComponentDisplay = <EmptyListComponent
                message={RecruiterInsight.ErrorMessage}
                onclickhandler={this.ReloadPageData}
            />
        }

        let RecruiterInsight_View =
            <FlatList
                // Data
                data={RecruiterInsight.MyAcceptedJobs}
                // Render Items
                renderItem={({ item }) => {

                    var job_id = item.job_id;
                    let applicants = item.applicants;
                    let Applicants_View = null;
                    if (applicants.length > 0) {
                        // Applicants_View=   applicants.map(function(single,index){
                        Applicants_View = applicants.map((single, index) => {
                                    // const MyApplications_View = MyApplications.ApplicationsList.slice(0, 3).map((item, index) => {

                            let ApplicantNAme = single.first_name == null || single.last_name == null ? single.username : `${single.first_name}  ${single.last_name} `
                            let user_id = single.user_id;
                            return (
                                <CardItem style={{ backgroundColor: 'transparent' }} key={index}>
                                    <Left>
                                        <AnimatedCircularProgress
                                            size={37}
                                            width={2}
                                            fill={30}
                                            tintColor="#5ABC7A"
                                            backgroundColor="#F5F5F5">
                                            {
                                                (fill) => (
                                                    <Thumbnail style={{ width: 29, height: 30, borderRadius: 30 / 2 }} source={{ uri: single.image_url }} />
                                                )
                                            }
                                        </AnimatedCircularProgress>


                                        <Body>
                                            <TouchableOpacity
                                               onPress={() => this.ViewUserProfile(user_id)}
                                            >
                                                <Text style={[styles.Text8, { color: '#2F2F2F' }]}>{ApplicantNAme}</Text>
                                            </TouchableOpacity>
                                        </Body>
                                    </Left>
                                    <Right>
                                        {single.isShortlisted == 0 ?
                                            <Button transparent style={styles.ViewBtnSty2}
                                                onPress={() => this.ShortListApplicant(job_id, user_id)}

                                            >
                                                <Text style={styles.Text1_3}>SHORTLIST</Text>
                                            </Button>
                                            :

                                            <Button transparent style={styles.ViewBtnSty2}
                                                onPress={() => this.UnShortListApplicant(job_id, user_id)}

                                            >
                                                <Text style={styles.Text1_3}>SHORTLISTED</Text>
                                            </Button>
                                        }

                                    </Right>
                                </CardItem>
                            )
                        })
                    }
                    else {
                        Applicants_View = <View style={{}}>
                            <Text style={{ textAlign: 'center', fontSize: 14 }}>No Applicant yet </Text>
                        </View>
                    }



                    return (
                        <View style={styles.successCard2}>
                            <View style={styles.cardinnerSty}>
                                <Text style={styles.Text3}>{`${item.total_applicant} applicants`} </Text>
                            </View>
                            <View style={styles.xLayer}>
                                <Left style={{ borderBottomWidth: 0, marginLeft: 0, flex: 1 }}>
                                    <Text style={[styles.Text4, { marginLeft: 0 }]}>{item.role_title}</Text>
                                    <Text style={styles.Text5} numberOfLines={1} >
                                        {item.location}
                                    </Text>
                                </Left>
                                <Right style={[styles.morestyIdc, { borderBottomWidth: 0, width: 20, flex: 0 }]}>
                                    <TouchableOpacity style={styles.moreBtnSty}
                                    // onPress={this.toggleModal}
                                    >
                                        <SvgUri
                                            width='14.79px'
                                            height='4px'
                                            uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1589464409/Mobile_Assets/more_xcza8v.svg"
                                        />
                                    </TouchableOpacity>
                                </Right>
                            </View>
                            <Text style={[styles.Text1, { fontWeight: 'normal', textAlign: 'left', paddingLeft: 19, paddingRight: 19, marginTop: 14, marginBottom: 12 }]}>Top applicants</Text>
                            {Applicants_View}

                            {/* <CardItem style={{ backgroundColor: 'transparent' }}>
                                <Left>
                                    <Thumbnail style={{ width: 29, height: 30, borderRadius: 30 / 2 }} source={{ uri: 'https://res.cloudinary.com/workbrook-hash/image/upload/v1589587873/Mobile_Assets/ddperson4_xg97eh.png' }} />
    
                                    <Body>
                                        <TouchableOpacity>
                                            <Text style={[styles.Text8, { color: '#2F2F2F' }]}>Jane Dokaszhuk</Text>
                                        </TouchableOpacity>
                                    </Body>
                                </Left>
                                <Right>
                                    <Button transparent style={styles.ViewBtnSty2}>
                                        <Text style={styles.Text1_3}>SHORTLIST</Text>
                                    </Button>
                                </Right>
                            </CardItem>
                            <CardItem style={{ backgroundColor: 'transparent' }}>
                                <Left>
                                    <Thumbnail style={{ width: 29, height: 30, borderRadius: 30 / 2 }} source={{ uri: 'https://res.cloudinary.com/workbrook-hash/image/upload/v1589587873/Mobile_Assets/ddperson4_xg97eh.png' }} />
    
                                    <Body>
                                        <TouchableOpacity>
                                            <Text style={[styles.Text8, { color: '#2F2F2F' }]}>Jane Dokaszhuk</Text>
                                        </TouchableOpacity>
                                    </Body>
                                </Left>
                                <Right>
                                    <Button transparent style={styles.ViewBtnSty2}
                                        onPress={() => { this.props.navigation.navigate("ViewApplicant") }}
                                    >
                                        <Text style={styles.Text1_3}>SHORTLIST</Text>
                                    </Button>
                                </Right>
                            </CardItem>
                            <CardItem style={{ backgroundColor: 'transparent' }}>
                                <Left>
                                    <Thumbnail style={{ width: 29, height: 30, borderRadius: 30 / 2 }} source={{ uri: 'https://res.cloudinary.com/workbrook-hash/image/upload/v1589587873/Mobile_Assets/ddperson4_xg97eh.png' }} />
    
                                    <Body>
                                        <TouchableOpacity>
                                            <Text style={[styles.Text8, { color: '#2F2F2F' }]}>Jane Dokaszhuk</Text>
                                        </TouchableOpacity>
                                    </Body>
                                </Left>
                                <Right>
                                    <Button transparent style={styles.ViewBtnSty2}
                                        onPress={() => { this.props.navigation.navigate("ViewApplicant") }}
                                    >
                                        <Text style={styles.Text1_3}>SHORTLISTED</Text>
                                    </Button>
                                </Right>
                            </CardItem>
                            <CardItem style={{ backgroundColor: 'transparent' }}>
                                <Left>
                                    <Thumbnail style={{ width: 29, height: 30, borderRadius: 30 / 2 }} source={{ uri: 'https://res.cloudinary.com/workbrook-hash/image/upload/v1589587873/Mobile_Assets/ddperson4_xg97eh.png' }} />
    
                                    <Body>
                                        <TouchableOpacity>
                                            <Text style={[styles.Text8, { color: '#2F2F2F' }]}>
                                                Jane Dokaszhuk
                                            </Text>
                                        </TouchableOpacity>
                                    </Body>
                                </Left>
                                <Right>
                                    <Button transparent style={styles.ViewBtnSty2}>
                                        <Text style={styles.Text1_3}>SHORTLIST</Text>
                                    </Button>
                                </Right>
                            </CardItem>
                            <CardItem style={{ backgroundColor: 'transparent' }}>
                                <Left>
                                    <Thumbnail style={{ width: 29, height: 30, borderRadius: 30 / 2 }} source={{ uri: 'https://res.cloudinary.com/workbrook-hash/image/upload/v1589587873/Mobile_Assets/ddperson4_xg97eh.png' }} />
    
                                    <Body>
                                        <TouchableOpacity>
                                            <Text style={[styles.Text8, { color: '#2F2F2F' }]}>
                                                Jane Dokaszhuk
                                            </Text>
                                        </TouchableOpacity>
                                    </Body>
                                </Left>
                                <Right>
                                    <Button transparent style={styles.ViewBtnSty2}
                                        onPress={() => { this.props.navigation.navigate("ViewApplicant") }}
                                    >
                                        <Text style={styles.Text1_3}>SHORTLIST</Text>
                                    </Button>
                                </Right>
                            </CardItem>
                            */}
                            <View style={styles.copyViewBtnWrap}>
                                <Button transparent style={styles.copyVisitBtn}
                                    // onPress={this.toggleModalViewApplicants}
                                    onPress={() => { this.toggleModalViewApplicants(item) }}

                                >

                                    <Text style={[styles.Text3, { color: '#2F2F2F' }]}>Copy Unique Application Link</Text>
                                </Button>
                                <Button style={[styles.copyVisitBtn, { backgroundColor: '#5ABC7A', borderColor: "#5ABC7A" }]}
                                    // onPress={() => { this.props.navigation.navigate("ShortListedApplicants") }}
                                    onPress={() => { this.ViewShortlistedApplicant(item) }}
                                >
                                    <Text style={[styles.Text3, { color: '#fff' }]}>
                                        View Shortlist
                                    </Text>
                                </Button>
                            </View>
                        </View>



                    )
                }}
                // Item Key
                keyExtractor={(item, index) => String(index)}
                // Header (Title)
                // ListHeaderComponent={this.renderHeader}
                // Footer (Activity Indicator)
                // ListFooterComponent={this.renderFooter()}
                // On End Reached (Takes a function)
                onEndReached={this.retrieveMore}
            // How Close To The End Of List Until Next Data Request Is Made
            // onEndReachedThreshold={0}
            // Refreshing (Set To True When End Reached)
            // refreshing={this.state.refreshing}
            />

        return (
            <View style={{ flex: 1, backgroundColor: '#fff' }}>
                {/* <CustomizeHeader
                    leftside='WhiteArr'
                    title='Recruiter Insights'
                    rightside='empty'
                    statedColor='#1E93E2'
                    icon='md-close'
                /> */}
                <View style={styles.likeTabsty}>
                    <TouchableOpacity style={[styles.BtnInsightState, { backgroundColor: this.state.InsightView == "ACTIVITY" ? "#1E93E2" : "#BCBCBC" }]}
                        onPress={() => this._changeInsightView('ACTIVITY')}
                    >
                        <Text style={styles.Text1_4}>
                            ACTIVITY
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.BtnInsightState, { backgroundColor: this.state.InsightView == "PERFORMANCE" ? "#1E93E2" : "#BCBCBC" }]}
                        onPress={() => this._changeInsightView('PERFORMANCE')}
                    >
                        <Text style={styles.Text1_4}>
                            PERFORMANCE
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.BtnInsightState, { backgroundColor: this.state.InsightView == "JOBINSIGHTS" ? "#1E93E2" : "#BCBCBC" }]}
                        onPress={() => this._changeInsightView('JOBINSIGHTS')}
                    >
                        <Text style={styles.Text1_4}>
                            JOB INSIGHTS
                        </Text>
                    </TouchableOpacity>

                </View>
                {
                    this.state.InsightView == "ACTIVITY" ?
                        <Activities />
                        :
                        this.state.InsightView == "PERFORMANCE" ?
                            <PerformanceTracker2 />
                            :
                            <ScrollView
                                contentContainerStyle={{
                                    paddingLeft: 30,
                                    paddingRight: 30,
                                    paddingBottom: 30,
                                    paddingTop: 20
                                }}
                            >
                                {/* <View style={styles.successCard2}>
                     <View style={styles.cardinnerSty}>
                         <Text style={styles.Text3}>50 applicants</Text>
                     </View>
                     <View style={styles.xLayer}>
                         <Left style={{ borderBottomWidth: 0, marginLeft: 0, flex: 1 }}>
                             <Text style={[styles.Text4, { marginLeft: 0 }]}>Office Assistant</Text>
                             <Text style={styles.Text5} numberOfLines={1} >
                                 P&G, Victoria Island
             </Text>
                         </Left>
                         <Right style={[styles.morestyIdc, { borderBottomWidth: 0, width: 20, flex: 0 }]}>
                             <TouchableOpacity style={styles.moreBtnSty}
                                 onPress={this.toggleModal}
                             >
                                 <SvgUri
                                     width='14.79px'
                                     height='4px'
                                     uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1589464409/Mobile_Assets/more_xcza8v.svg"
                                 />
                             </TouchableOpacity>
                         </Right>
                     </View>
                     <Text style={[styles.Text1, { fontWeight: 'normal', textAlign: 'left', paddingLeft: 19, paddingRight: 19, marginTop: 14, marginBottom: 12 }]}>Top applicants</Text>
                     <CardItem style={{ backgroundColor: 'transparent' }}>
                         <Left>
                             <Thumbnail style={{ width: 29, height: 30, borderRadius: 30 / 2 }} source={{ uri: 'https://res.cloudinary.com/workbrook-hash/image/upload/v1589587873/Mobile_Assets/ddperson4_xg97eh.png' }} />

                             <Body>
                                 <TouchableOpacity>
                                     <Text style={[styles.Text8, { color: '#2F2F2F' }]}>Jane Dokaszhuk</Text>
                                 </TouchableOpacity>
                             </Body>
                         </Left>
                         <Right>
                             <Button transparent style={styles.ViewBtnSty2}>
                                 <Text style={styles.Text1_3}>SHORTLIST</Text>
                             </Button>
                         </Right>
                     </CardItem>
                     <CardItem style={{ backgroundColor: 'transparent' }}>
                         <Left>
                             <Thumbnail style={{ width: 29, height: 30, borderRadius: 30 / 2 }} source={{ uri: 'https://res.cloudinary.com/workbrook-hash/image/upload/v1589587873/Mobile_Assets/ddperson4_xg97eh.png' }} />

                             <Body>
                                 <TouchableOpacity>
                                     <Text style={[styles.Text8, { color: '#2F2F2F' }]}>Jane Dokaszhuk</Text>
                                 </TouchableOpacity>
                             </Body>
                         </Left>
                         <Right>
                             <Button transparent style={styles.ViewBtnSty2}
                                 onPress={() => { this.props.navigation.navigate("ViewApplicant") }}
                             >
                                 <Text style={styles.Text1_3}>SHORTLIST</Text>
                             </Button>
                         </Right>
                     </CardItem>
                     <CardItem style={{ backgroundColor: 'transparent' }}>
                         <Left>
                             <Thumbnail style={{ width: 29, height: 30, borderRadius: 30 / 2 }} source={{ uri: 'https://res.cloudinary.com/workbrook-hash/image/upload/v1589587873/Mobile_Assets/ddperson4_xg97eh.png' }} />

                             <Body>
                                 <TouchableOpacity>
                                     <Text style={[styles.Text8, { color: '#2F2F2F' }]}>Jane Dokaszhuk</Text>
                                 </TouchableOpacity>
                             </Body>
                         </Left>
                         <Right>
                             <Button transparent style={styles.ViewBtnSty2}
                                 onPress={() => { this.props.navigation.navigate("ViewApplicant") }}
                             >
                                 <Text style={styles.Text1_3}>SHORTLISTED</Text>
                             </Button>
                         </Right>
                     </CardItem>
                     <CardItem style={{ backgroundColor: 'transparent' }}>
                         <Left>
                             <Thumbnail style={{ width: 29, height: 30, borderRadius: 30 / 2 }} source={{ uri: 'https://res.cloudinary.com/workbrook-hash/image/upload/v1589587873/Mobile_Assets/ddperson4_xg97eh.png' }} />

                             <Body>
                                 <TouchableOpacity>
                                     <Text style={[styles.Text8, { color: '#2F2F2F' }]}>
                                         Jane Dokaszhuk
                                     </Text>
                                 </TouchableOpacity>
                             </Body>
                         </Left>
                         <Right>
                             <Button transparent style={styles.ViewBtnSty2}>
                                 <Text style={styles.Text1_3}>SHORTLIST</Text>
                             </Button>
                         </Right>
                     </CardItem>
                     <CardItem style={{ backgroundColor: 'transparent' }}>
                         <Left>
                             <Thumbnail style={{ width: 29, height: 30, borderRadius: 30 / 2 }} source={{ uri: 'https://res.cloudinary.com/workbrook-hash/image/upload/v1589587873/Mobile_Assets/ddperson4_xg97eh.png' }} />

                             <Body>
                                 <TouchableOpacity>
                                     <Text style={[styles.Text8, { color: '#2F2F2F' }]}>
                                         Jane Dokaszhuk
                                     </Text>
                                 </TouchableOpacity>
                             </Body>
                         </Left>
                         <Right>
                             <Button transparent style={styles.ViewBtnSty2}
                                 onPress={() => { this.props.navigation.navigate("ViewApplicant") }}
                             >
                                 <Text style={styles.Text1_3}>SHORTLIST</Text>
                             </Button>
                         </Right>
                     </CardItem>
                     <View style={styles.copyViewBtnWrap}>
                         <Button transparent style={styles.copyVisitBtn}
                             onPress={this.toggleModalViewApplicants}>

                             <Text style={[styles.Text3, { color: '#2F2F2F' }]}>Copy Unique Application Link</Text>
                         </Button>
                         <Button style={[styles.copyVisitBtn, { backgroundColor: '#5ABC7A', borderColor: "#5ABC7A" }]}
                             onPress={() => { this.props.navigation.navigate("ShortListedApplicants") }}
                         >
                             <Text style={[styles.Text3, { color: '#fff' }]}>
                             View Shortlist
                             </Text>
                         </Button>
                     </View>
                 </View>
               */}
                                {
                                    RecruiterInsight.MyAcceptedJobs.length > 0 ?

                                        RecruiterInsight_View
                                        :
                                        ErrorComponentDisplay

                                }


                            </ScrollView>

                }

                {/* modal views */}
                <Modal style={styles.MContainer} isVisible={this.state.isModalVisible}>
                    <View style={styles.Mwrapper}>
                        <View style={styles.firstView} >
                            <Button full style={styles.actionBtn}>
                                <Text style={styles.actionBtnText}>
                                    View the job poster’s profile
              </Text>
                            </Button>
                            <Button full style={[styles.actionBtn, { borderTopWidth: 1, borderTopColor: "#E5E5E5", borderBottomWidth: 1, borderBottomColor: "#E5E5E5" }]}>
                                <Text style={styles.actionBtnText}>
                                    Edit your profile
              </Text>
                            </Button>
                            <Button full style={styles.actionBtn}
                                onPress={this.toggleModalWithdrawApp}
                            >
                                <Text style={[styles.actionBtnText, { color: "#FF5964" }]}>
                                    Withdraw application
              </Text>
                            </Button>
                        </View>
                        <Button style={[styles.btnCancel, { marginTop: 15 }]} title="Hide modal" onPress={this.toggleModal}>
                            <Text style={[styles.btnCancelText,]}>Cancel</Text>
                        </Button>
                    </View>
                </Modal>
                {/* view applicants  modal*/}
                <Modal style={styles.MContainer} isVisible={this.state.isModalViewApplicants}>
                    <Button onPress={this.CloseViewUniquecodeModal} style={{ backgroundColor: "transparent", position: 'absolute', right: 10, top: 40, padding: 20, elevation: 0 }}>
                        <SvgUri
                            width="19px"
                            height="19px"
                            uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1594146750/Mobile_Assets/close_white_iocsmo.svg"
                        />
                    </Button>

                    <View style={styles.Mwrapper}>
                        <View style={styles.firstView} >
                            <Button full style={styles.actionBtn2}>
                                <Text style={[styles.actionBtnText, { fontWeight: '600', height: 20 }]}>
                                    Unique Application Link
                                </Text>
                            </Button>
                            <View style={styles.codeWrap}>
                                <Text style={[styles.Text4_2, {
                                    width: '100%', textAlign: 'center',
                                    marginBottom: 17
                                }]}>
                                    This link will be used by the recruiter to invite prospective applicants to the app as well as to apply for the respective job offer.
                                </Text>
                                <View style={[styles.codeDisplaySty, { marginBottom: 15 }]}>
                                    <Text style={[styles.Text4, { color: '#bcbcbc' }]}>
                                        {thisUniqueCode}
                                    </Text>

                                </View>

                            </View>
                            <View style={styles.checkBoxWrap}>
                                {isCopied === true ?
                                    <Button transparent style={styles.copylinkSty}
                                        onPress={this.CloseViewUniquecodeModal}>
                                        <Text style={styles.Text6}>
                                            {/* Copied !!! Close Now  */}
                                            share link
                                            
                                             </Text>

                                    </Button>
                                    :
                                    <Button transparent style={styles.copylinkSty}
                                        onPress={this.writeToClipboard}>
                                        <Text style={styles.Text6}>
                                            Copy Link
</Text>

                                    </Button>

                                }


                            </View>
                        </View>
                    </View>
                </Modal>
                {/* Withdraw App modal*/}

                {/* modal views */}
            </View>
        );
    }
}
// export default RecruiterInsights;

export default withNavigation(RecruiterInsights);

const inputStyles = {
    size: 100,
    color: '#FFFFFF',
};

const height = 40;

const styles = StyleSheet.create({
    Text1: {
        fontSize: 10,
        fontWeight: '500',
        color: "#2F2F2F",
        textAlign: 'center'
    },
    Text1_3: {
        fontSize: 10,
        fontWeight: '500',
        color: "#1E93E2",
        textAlign: 'center'
    },
    Text1_4: {
        fontSize: 10,
        fontWeight: '500',
        color: "#fafafa",
        textAlign: 'center'
    },
    Text1_2: {
        fontSize: 10,
        fontWeight: 'bold',
        color: "#2F2F2F",
        textAlign: 'center'
    },
    Text3: {
        fontSize: 12,
        fontWeight: '500',
        color: "#FAFAFA",
        textAlign: 'center'
    },
    Text7: {
        fontSize: 12,
        fontWeight: '500',
        color: "#2F2F2F",
        textAlign: 'center'
    },
    Text4: {
        fontSize: 13,
        fontWeight: '500',
        color: "#2F2F2F",
    },
    Text4_2: {
        fontSize: 12,
        fontWeight: 'normal',
        color: "#676767",
    },
    Text5: {
        fontSize: 11,
        fontWeight: '300',
        color: "#2F2F2F",
    },
    Text6: {
        fontSize: 14,
        fontWeight: '500',
        color: "#1E93E2",
    },
    Text8: {
        fontSize: 12,
        fontWeight: '600',
        color: "#1E93E2",
    },
    Text2: {
        fontSize: 18,
        fontWeight: '500',
        color: "#1E93E2",
    },
    likeTabsty: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    viewPerTabSty: {
        display: 'flex',
        alignItems: 'center',
        maxWidth: 79,
        textAlign: 'center',
    },
    successCard: {
        display: 'flex',
        alignItems: 'center',
        textAlign: 'center',
        justifyContent: "center",
        backgroundColor: '#F3F3F3',
        height: 100,
        // elevation: 1,
        marginTop: 30,
    },
    BtnInsightState: {
        display: 'flex',
        alignItems: 'center',
        textAlign: 'center',
        justifyContent: "center",
        backgroundColor: '#BCBCBC',
        padding: 12,
        minWidth: 99,
        margin: 3,
        borderRadius: 5

        // elevation: 1,
    },
    successCard2: {
        display: 'flex',
        // justifyContent: 'space-between',
        textAlign: 'center',
        backgroundColor: '#F3F3F3',
        // height: 100,
        marginTop: 17,
    },
    cardinnerSty: {
        width: '100%',
        display: 'flex',
        alignItems: 'flex-end',
        textAlign: 'right',
        justifyContent: "center",
        backgroundColor: '#2f2f2f',
        height: 24,
        paddingRight: 12,
        paddingLeft: 12,
    },
    morestyIdc: {
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'center',
        width: 20
    },
    moreBtnSty: {
        height: 20,
        width: 20,
        borderRadius: 5,
        borderWidth: 0,
        borderColor: '#1E93E2',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    copylinkSty: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        // height: 18,
    },
    xLayer: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingBottom: 14,
        paddingLeft: 19,
        paddingRight: 19,
        paddingTop: 14,
    },
    // ======================= modal style ==============================
    MContainer: {
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
    },
    Mwrapper: {
        // height: 360,
        width: '98%',
        // backgroundColor: '#fff',
        padding: 15,
        display: 'flex',
        justifyContent: 'space-around',
        // alignItems: 'center'
    },
    firstView: {
        backgroundColor: '#FAFAFA',
        borderRadius: 5,
        padding: 25,
    },
    actionBtn: {
        backgroundColor: '#FAFAFA',
        borderRadius: 5,
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
        height: 43,
    },
    actionBtn2: {
        backgroundColor: '#FAFAFA',
        borderRadius: 5,
        elevation: 0,
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
        height: 43,
    },
    codeWrap: {
        paddingTop: 14,
        paddingBottom: 14,
        // display: 'flex',
        // justifyContent: 'center',
        // flexDirection: 'row',
        // alignItems: 'center',
        // height: 43,
    },
    checkBoxWrap: {
        borderTopWidth: 1,
        borderTopColor: '#E5E5E5',
        paddingTop: 14,
        paddingLeft: 20,
        paddingRight: 20,
        // paddingBottom: 14,
        // display: 'flex',
        // justifyContent: 'center',
        // flexDirection: 'row',
        // alignItems: 'center',
        // height: 43,
    },
    actionBtnText: {
        fontSize: 12,
        color: '#1E93E2',
    },
    btnCancel: {
        backgroundColor: '#FF5964',
        borderRadius: 5,
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
        height: 43,
        elevation: 0,
    },
    btnCancelText: {
        fontSize: 12,
        color: '#FAFAFA',
        fontWeight: 'bold',
    },
    checkBoxText: {
        fontSize: 11,
        color: '#2F2F2F',
        marginLeft: 20
    },
    CheckBoxBody: {
        display: 'flex',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 10,
    },

    // ======================= modal style ==============================
    userFlex: {
        display: 'flex',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        alignItems: 'center',
        flexWrap: 'wrap',
        paddingLeft: 10,
        paddingRight: 10,
        // marginTop: 10,
    },
    successCard2Bottom: {
        display: 'flex',
        justifyContent: 'space-between',
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 19,
        paddingRight: 19,
        marginTop: 29,
        marginBottom: 17,
    },
    copyViewBtnWrap: {
        display: 'flex',
        alignItems: 'center',
        paddingLeft: 19,
        paddingRight: 19,
        marginTop: 20,
        marginBottom: 17,
    },
    card2Bottom: {
        height: 28,
        backgroundColor: '#5ABC7A',
        borderRadius: 5,
        elevation: 0,
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 14,
        paddingRight: 14
    },
    userFlexImageWrap: {
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 5,
        width: '16.6%'
    },
    userFlexImage: {
        width: 32,
        height: 34,
        borderRadius: 34 / 2,
    },
    ViewBtnSty: {
        height: 23,
        width: 50,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#1E93E2',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    ViewBtnSty2: {
        height: 23,
        paddingBottom: 8,
        paddingTop: 8,
        paddingLeft: 10,
        paddingRight: 10,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#1E93E2',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    copyVisitBtn: {
        height: 31,
        width: '100%',
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#2F2F2F',
        display: 'flex',
        justifyContent: 'center',
        marginBottom: 6,
        alignItems: 'center',
        elevation: 0
    },
    codeDisplaySty: {
        borderWidth: 1,
        borderColor: "#E5E5E5",
        borderRadius: 3,
        padding: 15,
    },
});

import React, { Component } from 'react';
import {
    Text,
    Image,
    StyleSheet,
    ImageBackground,
    Keyboard,
    TouchableOpacity
} from 'react-native';
import { KeyboardAvoidingView } from 'react-native';
import CustomizeHeader from '../frags/CustomizeHeader';

import Ionicons from 'react-native-vector-icons/Ionicons';
import {
    Container,
    Header,
    Content,
    Textarea,
    Button,
    View,
    Thumbnail,
    Form,
    Item,
    Input,
    Label,
    Badge,
    Icon,
    Footer,
    Picker,
    Left,
    Right,
    Body,
    Title,
    Center,
    CardItem
} from 'native-base';

import { myspiner } from '../../dependency/UtilityFunctions';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import User from "../../assets/workbrookAssets/user4.png";
import MapView, { Marker, ProviderPropType, AnimatedRegion, animateToRegion, PROVIDER_GOOGLE } from 'react-native-maps';

import accountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import { ScrollView } from 'react-native-gesture-handler';
import imageUser from '../../assets/workbrookAssets/user2.png';
import DatePicker from 'react-native-datepicker'
import Drop from "../../assets/workbrookAssets/drop.png";
// import Autocomplete from 'native-base-autocomplete'
import Autocomplete from 'react-native-autocomplete-input';

const API = 'https://swapi.co/api';
@observer
class RecruiterMaps extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }

    componentDidMount() {
    }

    render() {

        return (
            <Container style={[{ height: '100%', width: '100%', backgroundColor: "yellow" }]}>
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    showsHorizontalScrollIndicator={false}
                >
                    <Container style={[styles.profileWrapinner, { backgroundColor: "#fff" }]}>
                    <CardItem style={{ width: "100%", backgroundColor: "transparent", paddingTop: 0, paddingLeft: 0, paddingRight: 0, positioon: 'absolute', top: 0, zIndex: 9999 }}>
                        <Left style={{ alignItems: 'flex-start', backgroundColor: "#1E93E2", padding: 24 }}>
                            <Thumbnail small source={{ uri: 'https://res.cloudinary.com/custocrypt/image/upload/v1569083972/BgvBscXZLT035MdzpNpHLbZmyPg2of.jpg' }} style={{ width: 58, height: 60, borderRadius: 60 / 2 }} />
                            <Body style={styles.borderBtmsty2, { marginLeft: 15, borderBottomWidth: 0, borderColor: '#E5E5E5', paddingBottom: 5 }}>
                                <Text style={[styles.minBold3, { fontWeight: '600' }]}>
                                    Tomzy Adex
                                    </Text>
                                <View style={styles.textIcon}>
                                    <Image source={Drop} style={{ marginRight: 5 }} />
                                    <Text note style={[styles.Text2, { color: '#FAFAFA' }]}>Lagos, Nigeria</Text>
                                </View>
                            </Body>
                        </Left>
                    </CardItem>
                    <MapView
                    style={styles.map}
                    provider={PROVIDER_GOOGLE}
                    showsUserLocation={true}
                    showsMyLocationButton={true}
                    loadingEnabled={true}
                    loadingBackgroundColor='#000000'
                    initialRegion={{
                        latitude: 37.78825,
                        longitude: -122.4324,
                        latitudeDelta: 0.0922,
                        longitudeDelta: 0.0421,
                      }}
                />

                    </Container>

                </ScrollView>
            </Container>
        );
    }
}
export default RecruiterMaps;

const inputStyles = {
    size: 100,
    color: '#FFFFFF',
};

const height = 40;

const styles = StyleSheet.create({
    map: {
        ...StyleSheet.absoluteFillObject,
    },
    Text1: {
        fontSize: 40,
        fontWeight: '600',
        color: "#1E93E2",
    },
    Text2: {
        fontSize: 12,
        fontWeight: 'normal',
        color: "#1E93E2",
    },
    Text3: {
        fontSize: 14,
        fontWeight: 'normal',
        color: "#FAFAFA",
    },
    profileWrapinner: {
        flex:1,
        width: '100%',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        minHeight: '95%',
        paddingLeft: 0,
        paddingRight: 0,
    },

    borderBtmsty2: {
        borderBottomWidth: 1,
        borderBottomColor: "#E5E5E5",
        paddingBottom: 5
    },
    minBold3: {
        fontSize: 16,
        color: '#FAFAFA'
    },
    textIcon: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center'
    },


});

import React, { Component } from 'react';
import {
    Text,
    Image,
    StyleSheet,
    ImageBackground,
    Keyboard,
    TouchableOpacity, FlatList
} from 'react-native';
import { KeyboardAvoidingView, Alert } from 'react-native';
import CustomizeHeader from '../frags/CustomizeHeader';

import Ionicons from 'react-native-vector-icons/Ionicons';
import {
    Container,
    Header,
    Content,
    Textarea,
    Button,
    View,
    Thumbnail,
    Form,
    Item,
    Input,
    Label,
    Badge,
    Icon,
    Footer,
    Picker,
    Left,
    Right,
    Body,
    Title,
    Center,
    CardItem,
    CheckBox
} from 'native-base';
import {
    Kaede,
    Hoshi,
    Jiro,
    Isao,
    Madoka,
    Akira,
    Hideo,
    Kohana,
    Makiko,
    Sae,
    Fumi,
} from 'react-native-textinput-effects';
import Modal from 'react-native-modal';
import { myspiner, AppInlineLoader, PersistData } from '../../dependency/UtilityFunctions';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import User from "../../assets/workbrookAssets/user4.png";

import accountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import { ScrollView } from 'react-native-gesture-handler';
import imageUser from '../../assets/workbrookAssets/user2.png';
import DatePicker from 'react-native-datepicker'
// import Autocomplete from 'native-base-autocomplete'
import Autocomplete from 'react-native-autocomplete-input';
import GridView from 'react-native-gridview';
import { SvgUri } from 'react-native-svg';
import { withNavigation } from 'react-navigation';
import DashboardStore from '../../stores/Dashboard';
import EmptyListComponent from '../../StatelessScreens/EmptyListComponent';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
const Dimensions = require('Dimensions');
let deviceWidth = Dimensions.get('window').width;
let deviceHeight = Dimensions.get('window').height;

const API = 'https://swapi.co/api';

@observer
class RecruiterLanding extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
        this.__AcceptWorkbrookRecruiter = this.__AcceptWorkbrookRecruiter.bind(this)

    }

    __AcceptWorkbrookRecruiter = () => { 
        DashboardStore.ProceedWithIntrestRecord();
        PersistData('TermANdConditionAccepted', true);
        
        DashboardStore._updateParameterV2(true, "RecruiterAvailable", "RedirectMode");

        // this.props.navigation.navigate("RecruiterForm")
      };

    render() {

        return (
            <ScrollView>
            <View style={[styles.conatainer, { backgroundColor: '#fff', width: deviceWidth, height: deviceHeight, paddingRight: 34 , paddingLeft: 34, paddingTop: 24, paddingBottom: 34}]}>
                <View style={styles.columnFlex}>
                    <Text style={styles.text}>
                        Hire a recruiter
                    </Text>
                    <Text style={[styles.text2, { marginTop: 6, }]}>
                        We make the recruitment process easier
                    </Text>
                </View>
                <SvgUri
                        width="282px"
                        height="282px"
                        uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1594148793/Mobile_Assets/recruiter_fgpmts.svg"
                    />
                <View style={[styles.columnFlex, {width: '100%', flexWrap: 'wrap', } ]}>
                    <TouchableOpacity>
                    <View style={[styles.ColumnRow, {width: '100%',marginBottom: 10} ]}>
                        <Text style={[styles.text3]}>
                            By clicking on “Let’s Go”, you confirm that I have read and I understand the <Text style={[styles.text3], { color: "#1E93E2" }}> Terms & Conditions   </Text> to hire a recruiter on workbrook.
                        </Text>
                       
                    </View>
                    </TouchableOpacity>
                    <Button full style={styles.btnLetGo}
                    onPress={  this.__AcceptWorkbrookRecruiter   }

                    >
                        <Text style={styles.text4}>
                            Let’s Go
                        </Text>
                    </Button>
                    <Button full style={styles.btnLetGo2}
                    onPress={() => { this.props.navigation.navigate("MyOffer") }}
                    >
                        <Text style={[styles.text4, {color:"#1E93E2"} ]}>
                        I’ll do this later
                        </Text>
                    </Button>


                </View>



            </View>
            
            </ScrollView>
        );
    }
}
// export default RecruiterLanding;

export default withNavigation(RecruiterLanding);

const styles = StyleSheet.create({
    conatainer: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    columnFlex: {
        display: 'flex',
        alignItems: 'center',
        flexWrap: 'wrap',
    },
    ColumnRow: {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'center',
        textAlign: 'center'
        // width: '80%',
    },
    text: {
        fontSize: 40,
        fontWeight: '600',
        color: '#000000',
        fontFamily: 'SF Pro Display',
        // fontStyle: 'normal',
    },
    text2: {
        fontSize: 12,
        color: "#000000",
        fontFamily: 'SF Pro Display',
        // fontStyle: 'normal',
    },
    text3: {
        fontSize: 11,
        color: "#2F2F2F",
        fontFamily: 'SF Pro Display',
        textAlign: 'center',
        // fontStyle: 'normal',
    },
    text4: {
        fontSize: 14,
        color: "#fafafa",
        fontFamily: 'SF Pro Display',
        // fontStyle: '600',
    },
    btnLetGo: {
        backgroundColor: "#000",
        borderRadius: 5,
        elevation: 0,
    },
    btnLetGo2: {
        backgroundColor: "#fff",
        borderRadius: 5,
        display: 'flex',
        alignItems: 'center',
        elevation: 0,
        height: 30
    },

});

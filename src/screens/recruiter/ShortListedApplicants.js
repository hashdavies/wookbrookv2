import React, { Component, Fragment } from 'react';
import { Text, StyleSheet, ImageBackground, Keyboard, TouchableOpacity, Alert, FlatList, ActivityIndicator } from 'react-native';
import { KeyboardAvoidingView, Image, ScrollView } from 'react-native';
import { StackActions } from 'react-navigation';

import Ionicons from 'react-native-vector-icons/Ionicons';
import Checker from '../../assets/workbrookAssets/checker.png';
import {
    Container,
    Header,
    Content,
    Textarea,
    Button,
    View,
    Thumbnail,
    Form,
    Item,
    Input,
    Label,
    Badge,
    Icon,
    Footer,
    Picker,
    Item as FormItem,
    Left,
    Body,
    CardItem,
    Card,
    Right,
    ListItem,
    List,
    CheckBox,
} from 'native-base';
import {
    Kaede,
    Hoshi,
    Jiro,
    Isao,
    Madoka,
    Akira,
    Hideo,
    Kohana,
    Makiko,
    Sae,
    Fumi,
} from 'react-native-textinput-effects';
import CustomizeHeader from '../frags/CustomizeHeader';

import { myspiner, temp, ShowNotification, AppInlineLoader } from '../../dependency/UtilityFunctions';
import Logo from '../../assets/workbrookAssets/logo.png';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import RBSheet from "react-native-raw-bottom-sheet";
import Interests from '../interests/Interests';
import Noti from "../../assets/workbrookAssets/noti.png";
import UserBl from "../../assets/workbrookAssets/userBl.png";
import ArrForward from "../../assets/workbrookAssets/arrfr.png";
import Shield from "../../assets/workbrookAssets/shield.png";
import Quest from "../../assets/workbrookAssets/quest.png";
import Info from "../../assets/workbrookAssets/info.png";
import Plus from "../../assets/workbrookAssets/plus.png";
import Remark from "../../assets/workbrookAssets/remark.png";
import Like from "../../assets/workbrookAssets/like.png";
import MoreIcon from "../../assets/workbrookAssets/more2.png";
import Profile from "../../assets/images/profile/dp2.png";
import cardPix from "../../assets/images/background/bg3_old.jpg";
import RNPickerSelect from 'react-native-picker-select';
import TagInput from 'react-native-tags-input';

import RNFetchBlob from 'react-native-fetch-blob'
import ImagePicker from 'react-native-image-picker';
import CloseModal from "../../assets/workbrookAssets/whitecloss.png";
import SuccessIcon from "../../assets/workbrookAssets/successPost.png";
import Modal from 'react-native-modal';
import AccountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import { text } from 'react-native-communications';
import DashboardStore from '../../stores/Dashboard';
import { hasSetIntrest } from '../../auth';
import Autocomplete from 'react-native-autocomplete-input';
import MultiSelect from 'react-native-multiple-select';
import { YOUR_CLOUDINARY_PRESET, YOUR_CLOUDINARY_NAME, requestCameraPermission, YOUR_CLOUDINARY_PRESET_JobBanner } from '../../dependency/UtilityFunctions';
import EmptyListComponent from '../../StatelessScreens/EmptyListComponent';
import { SvgUri } from 'react-native-svg';
import StarRating from 'react-native-star-rating';


items = [{
    id: '92iijs7yta',
    name: 'Ondo',
}, {
    id: 'a0s0a8ssbsd',
    name: 'Ogun',
}, {
    id: '16hbajsabsd',
    name: 'Calabar',
}, {
    id: 'sdhyaysdj',
    name: 'Kaduna',
}, {
    id: 'suudydjsjd',
    name: 'Abuja',
}];

@observer
class ShortlistedApplicants extends Component {

    constructor(props) {
        super(props);
        this.mapRef = null;
        this.state = {
            email: '',
            password: '',
            ViewMode: '',
            showRate: false,
            showSuccess: false,
            acceptShortlist: true,
            starCount: 3.5,
            UserReview:''

        }
        selectedItems = [];
        this.CheckRecruiterFullDetails = this.CheckRecruiterFullDetails.bind(this)
        this.SendShortlistedApplicant = this.SendShortlistedApplicant.bind(this)
        this.toggleShowRate = this.toggleShowRate.bind(this)
        this.toggleShowSuccess = this.toggleShowSuccess.bind(this)
    }
    toggleShowRate = () => {
        this.setState({ showRate: !this.state.showRate });
    };
    AcceptOrRejectList = (Acton) => {
        let statement= Acton===1 ? 'Accept' :'Reject'
        Alert.alert(
            `${statement} List from recruiter?`,
            `Are you sure you want to proceed.`,
            [
                {
                    text: 'No', onPress: () => { },//Do nothing
                    style: 'cancel'
                },
                {
                    text: 'Yes', onPress: () => {
                        // DashboardStore._updateParameterV2(item, "SingleRequest", "RecruiterDashBoard");

                        DashboardStore.__AcceptOrRejectList(Acton);
                    }
                },
            ],
            { cancelable: true }
        );
 
    };
    toggleShowSuccess = () => {
           this.setState({
            showRate: false,
            showSuccess: !this.state.showSuccess,

        });
    };
    SubmitRateAndReview = () => {
        const{starCount,UserReview}=this.state;
      console.log(starCount,UserReview)
    //   
   let activeRecruiter= DashboardStore.RecruiterInsight.SingleAcceptedJob.activeRecruiter
   let recruiter_id=activeRecruiter.id  || 0;
      let payload={
        "recruiter_id":recruiter_id,
        // "job_id":11,
        "star_rating":starCount,
        "review_message":UserReview
    }
 DashboardStore.__SubmitRateAndReview(payload);
 this.toggleShowRate();
    };
    onSelectedItemsChange = selectedItems => {
        this.setState({ selectedItems });
    };
    onStarRatingPress(rating) {
        this.setState({
            starCount: rating
        });
   
    }
    CheckRecruiterFullDetails = (item, page) => {

        DashboardStore._updateParameterV2(item, "SingleRecruiter", "Recruiters");
        this.props.navigation.navigate(page);

    };
    toggleSuccessPostModalClose = () => {
        this.setState({ showRate: false });
        DashboardStore._ToggleSucessPostModal(false)
        this.props.navigation.dispatch(StackActions.popToTop());
        // const popAction = StackActions.pop({
        //     n: 0,
        //   });

        //   this.props.navigation.dispatch(popAction);
        DashboardStore._UpdateDefaultValue();

    };
    async componentDidMount() {

        let data = this.props.navigation.getParam('data', '');

        this.setState({
            ViewMode: data
        },() => {
            console.log("UserloginToken");
            DashboardStore.__GetShortlistedApplicant(1, 30);
        })

        let UserloginToken = await temp();
        // let loggedinUserdetails=await loggedinUserdetails();
        console.log(UserloginToken);
        DashboardStore._getAxiosInstance(UserloginToken);
      
        // DashboardStore._updateParameterV2(newItem, "SingleAcceptedJob", "RecruiterInsight");

    }
    SendShortlistedApplicant = () => {
        Alert.alert(
            'Send Shortlisted Applicant ?',
            `Are you sure you want to proceed.`,
            [
                {
                    text: 'No', onPress: () => { },//Do nothing
                    style: 'cancel'
                },
                {
                    text: 'Yes', onPress: () => {
                        DashboardStore.__SendShortlistedApplicant();
                    }
                },
            ],
            { cancelable: true }
        );



    };
    HireApplicant = (item) => {
        // console.log(item)
        // let user_id=item.user_id;
        let user_id=item.user_id;
        Alert.alert(
            'Hire this Applicant?',
            `Are you sure you want to proceed.`,
            [
                {
                    text: 'No', onPress: () => { },//Do nothing
                    style: 'cancel'
                },
                {
                    text: 'Yes', onPress: () => {
                        DashboardStore.__HireApplicant(user_id);
                    }
                },
            ],
            { cancelable: true }
        );



    };
    render() {
        const { selectedItems, ViewMode } = this.state;
        const { isGoBack, ShortlistedApplicant } = DashboardStore;

        if (ShortlistedApplicant.isNavigationGoback === true) {
            this.props.navigation.goBack();
   
            DashboardStore._updateParameterV2(false, "isNavigationGoback", "ShortlistedApplicant");

        }

        let ErrorComponentDisplay = null;
        console.log(DashboardStore.ShortlistedApplicant);

        if (DashboardStore.isProcessing === true) {
            ErrorComponentDisplay = AppInlineLoader(DashboardStore.isProcessing)

        }
        else {
            ErrorComponentDisplay = <EmptyListComponent
                message={DashboardStore.ShortlistedApplicant.ErrorMessage}
            //  onclickhandler={this.LoadAvailableProffesions}
            />
        }
        //  let AvailableRecruiters=ShortlistedApplicant.MyShorlisted.slice();
        //  let This_selectedRecruiters=DashboardStore.JobPosting.SelectedRecuiter.slice();

        let AllRecruiters =
            <FlatList
                // Data
                data={ShortlistedApplicant.MyShorlisted}
                // Render Items
                renderItem={({ item }) => {
                    let ApplicantNAme = item.first_name == null || item.last_name == null ? item.username : `${item.first_name}  ${item.last_name} `

                    return (

                        <List style={{ borderBottomWidth: 1, borderBottomColor: '#E5E5E5', paddingBottom: 15 }}>
                            <ListItem avatar style={{ display: 'flex', justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center', }}>
                                <Left>
                                    {
                                        item.profilePicture === "" ?
                                            <Thumbnail source={Profile} style={{ width: 41, height: 42, borderRadius: 42 / 2 }} />
                                            :
                                            <Thumbnail source={{ uri: item.image_url }} style={{ width: 41, height: 42, borderRadius: 42 / 2 }} />
                                    }
                                </Left>
                                <Body style={{ borderBottomWidth: 0, }}>
                                    <Text style={styles.cardTextJob4}>{ApplicantNAme}</Text>
                                    {/* <Text style={styles.cardTextJob5} numberOfLines={3} >{item.summary}</Text> */}
                                </Body>
                                {
                                 
                                    ViewMode !="ViewOnly" ?
                                    null
                                    :
                                    item.isHired=="1" ?
                                    null
                                    :
                                    <Right style={[styles.btnStyIdc, { borderBottomWidth: 0 }]}>
                                    <Button transparent style={styles.ViewBtnSty}
                                     onPress={() => {this.HireApplicant(item)}}
                                    >
                                        <Text style={[styles.cardTextJob5, { color: '#1E93E2' }]}>Hire</Text>
                                      
                                    </Button>
                                 
                    
                                </Right>
                                }
              
                                {
                                    // This_selectedRecruiters.includes(item.id) === false ? <Fragment></Fragment> :   <Text style={[styles.cardTextJob5, { color: '#1E93E2' }]}>Added</Text>  

                                }


                            </ListItem>
                        </List>
                    )
                }}
                // Item Key
                keyExtractor={(item, index) => String(index)}
            // Header (Title)
            // ListHeaderComponent={this.renderHeader}
            // Footer (Activity Indicator)
            // ListFooterComponent={this.renderFooter()}
            // On End Reached (Takes a function)
            // onEndReached={this.retrieveMore}
            // How Close To The End Of List Until Next Data Request Is Made
            // onEndReachedThreshold={0}
            // Refreshing (Set To True When End Reached)
            // refreshing={this.state.refreshing}
            />
        return (

            <Container style={styles.container}>
                <CustomizeHeader
                    leftside='WhiteArr'
                    title={`Shortlisted Applicants`}
                    rightside='empty'
                    statedColor='#000'
                    icon='md-close'
                // savehandler={this.proceednow}
                />

                <ScrollView
                    contentContainerStyle={{

                    }}
                >
                    <View style={styles.headerWrap}>
                        <Text style={styles.cardTextJob1}>Applicants
                        {JSON.stringify( DashboardStore.RecruiterInsight.SingleAcceptedJob.isAccepted)}
                        {JSON.stringify(   DashboardStore.RecruiterInsight.SingleAcceptedJob.isReviewed)}

                        </Text>
                        {/* <Button transparent style={{ display: 'flex', flexDirection: 'row', justifyContent: 'flex-start', }}
                         onPress={() => { this.props.navigation.navigate("RecruiterSearch") }}
                        >
                            <Text style={styles.cardTextJob3}>in Lagos, State   </Text>
                            <Text style={[styles.cardTextJob3, { color: '#1E93E2' }]}>Change Filters</Text>
                        </Button> */}
                    </View>
                    <View style={{ paddingLeft: 15, paddingRight: 15 }}>
                        {/* <Label style={styles.cardTextJob2}>Experience</Label> */}
                        <Item regular style={styles.inputStyleWrap}>

                            {/* <Input
                                placeholder="Search"

                            /> */}
                        </Item>
                    </View>

                    {
                        ShortlistedApplicant.MyShorlisted.length > 0 ?
                            AllRecruiters
                            :
                            ErrorComponentDisplay

                    }

{/* 
                    <List style={{ borderBottomWidth: 1, borderBottomColor: '#E5E5E5', paddingBottom: 15 }}>
                        <ListItem avatar style={{ display: 'flex', justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center', }}>
                            <Left>
                                <Thumbnail source={Profile} />
                            </Left>
                            <Body style={{ borderBottomWidth: 0, }}>
                                <Text style={styles.cardTextJob4}>Kumar Pratik</Text>
                                <Text style={styles.cardTextJob5} numberOfLines={1} >Doing what you like will always keep you happy . . .</Text>
                            </Body>
                            <Right style={[styles.btnStyIdc, { borderBottomWidth: 0 }]}>
                                <Button transparent style={styles.ViewBtnSty}
                                    onPress={() => { this.props.navigation.navigate("RecruiterProfile") }}
                                >
                                    <Text style={[styles.cardTextJob5, { color: '#1E93E2' }]}>Hire</Text>
                                </Button>
                            </Right>
                        </ListItem>
                    </List>

                    <List style={{ borderBottomWidth: 1, borderBottomColor: '#E5E5E5', paddingBottom: 15 }}>
                        <ListItem avatar style={{ display: 'flex', justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center', }}>
                            <Left>
                                <Thumbnail source={Profile} />
                            </Left>
                            <Body style={{ borderBottomWidth: 0, }}>
                                <Text style={styles.cardTextJob4}>Kumar Pratik</Text>
                                <Text style={styles.cardTextJob5}>Doing what you like will always keep you happy . . .</Text>
                            </Body>
                            <Right style={[styles.btnStyIdc, { borderBottomWidth: 0 }]}>
                                <Button transparent style={styles.ViewBtnSty}
                                    onPress={() => { this.props.navigation.navigate("RecruiterProfile") }}
                                >
                                    <Text style={[styles.cardTextJob5, { color: '#1E93E2' }]}>Hire</Text>
                                </Button>
                            </Right>
                        </ListItem>
                    </List> */}





                    {
                        ViewMode === 'ViewOnly' ?
                            null
                            :
                            <Button transparent full style={styles.serachbtnSty}
                                onPress={this.SendShortlistedApplicant}
                            >
                                <Text style={styles.cardTextJob2_3}>Send Shortlist </Text>
                            </Button>
                    }
                </ScrollView>
                {
                        ViewMode !== 'ViewOnly' ?
                            null
                            :
                            <View style={styles.btnActionWrap}>
                            {
                                // this.state.acceptShortlist == false ?
                                DashboardStore.RecruiterInsight.SingleAcceptedJob.isShorlistAccepted==0 ?
                                    <Fragment>
                                        <TouchableOpacity style={styles.btnAction}
                                    onPress={()=>this.AcceptOrRejectList(0)}
        
                                        >
                                            <Text style={styles.cardTextJob2_3}>
                                                Reject
                                            </Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity style={styles.btnAction} 
                                           onPress={()=>this.AcceptOrRejectList(1)}
                                        
                                        >
                                            <Text style={styles.cardTextJob2_3}>
                                                Accept
                                            </Text>
                                        </TouchableOpacity>
                                    </Fragment>
                                    :
                                    DashboardStore.RecruiterInsight.SingleAcceptedJob.isReviewed==0 ?
                                    <TouchableOpacity style={[styles.btnAction, { backgroundColor: "#1E93E2" }]}
                                    onPress={this.toggleShowRate}
                                    >
                                        <Text style={styles.cardTextJob2_3}>
                                            Drop a review
                                    </Text>
                                    </TouchableOpacity>
                                    :
                                    null
                            }
        
        
        
                        </View>
          
        
                    }
              

                <Modal style={styles.MContainer} isVisible={this.state.showRate} >
                    <Button onPress={this.toggleShowRate} style={{ backgroundColor: "transparent", position: 'absolute', right: 10, top: 40, padding: 20, elevation: 0 }}>
                        <SvgUri
                            width="19px"
                            height="19px"
                            uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1594146750/Mobile_Assets/close_white_iocsmo.svg"
                        />
                    </Button>
                    <View style={[styles.Mwrapper, { height: 340 }]}>
                        <View style={{ padding: 15, display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={[styles.cardTextJob1_2, { marginBottom: 15, }]}>
                                Rate Your Experience
                            </Text>
                            <StarRating
                                disabled={false}
                                maxStars={5}
                                rating={this.state.starCount}
                                selectedStar={(rating) => this.onStarRatingPress(rating)}
                                starSize={23}
                                starStyle={{ marginBottom: 25, marginLeft: 3, }}
                            />
                            <View style={{ width: "100%" }}>
                                <Text style={[styles.cardTextJob7, { color: '#2f2f2f' }]}>Drop a review</Text>
                                <Textarea 
                                rowSpan={5} bordered placeholder="" 
                                style={styles.textAreaSty}
                                
                                onChangeText={(UserReview) => {this.setState({
                                    UserReview
                                })}}

                                />
                            </View>
                            <TouchableOpacity style={styles.BtnSubmit} onPress={this.SubmitRateAndReview}>
                                <Text style={styles.cardTextJob1_3}> Submit</Text>
                            </TouchableOpacity>
                            {/* <TouchableOpacity style={styles.BtnSubmit} onPress={this.toggleShowRate}>
                                <Text style={styles.cardTextJob1_3}> Submit</Text>
                            </TouchableOpacity> */}

                        </View>

                    </View>
                </Modal>

                <Modal style={styles.MContainer} isVisible={this.state.showSuccess} >
                    <Button onPress={this.toggleShowSuccess} style={{ backgroundColor: "transparent", position: 'absolute', right: 10, top: 90, padding: 20, elevation: 0 }}>
                        <SvgUri
                            width="19px"
                            height="19px"
                            uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1594146750/Mobile_Assets/close_white_iocsmo.svg"
                        />
                    </Button>
                    <View style={[styles.Mwrapper, { height: 220 }]}>
                        <View style={{ paddingLeft: 29, paddingRight: 29, display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                            <SvgUri
                                style={{ marginBottom: 17 }}
                                width="129px"
                                height="129px"
                                uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593736755/Mobile_Assets/success_y53teq.svg"
                            />
                            <Text style={styles.cardText1, { fontSize: 14, color: '#2f2f2f', textAlign: 'center', fontWeight: '600' }}>
                                Thank You
                            </Text>

                        </View>

                    </View>
                </Modal>

            </Container>


        );
    }
}
export default ShortlistedApplicants;

const styles = StyleSheet.create({
    MContainer: {
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
    },
    Mwrapper: {
        height: 510,
        width: '90%',
        backgroundColor: '#fff',
        borderRadius: 10,
        // padding: 15,
        display: 'flex',
        justifyContent: 'space-around',
        // alignItems: 'center'
    },
    textAreaSty: {
        backgroundColor: "#F2F2F2",
        borderRadius: 5,
        borderColor: "#F2F2F2",
        marginBottom: 13,
    },
    container: {
        backgroundColor: "white",
        display: "flex",
        flex: 1,
        justifyContent: "flex-start",
    },

    rmPadding: {
        paddingTop: 0,
        paddingRight: 0,
        paddingBottom: 0,
        paddingLeft: 0,
    },
    rmPaddingLR: {
        paddingLeft: 0,
        paddingRight: 0,
    },

    cardTextJob1: {
        color: "#2F2F2F",
        fontSize: 36,
        fontWeight: '600',

    },
    cardTextJob1_2: {
        color: "#2f2f2f",
        fontSize: 14,
        fontWeight: '600',
    },
    cardTextJob1_3: {
        color: "#FAFAFA",
        fontSize: 14,
        fontWeight: 'bold',
    },
    cardTextJob2: {
        color: "#BCBCBC",
        fontSize: 14,
        fontWeight: '600',
        marginBottom: 5,
    },
    cardTextJob2_2: {
        color: "#fff",
        fontSize: 14,
        fontWeight: '600',
    },
    cardTextJob2_3: {
        color: "#fff",
        fontSize: 12,
        fontWeight: '500',
    },
    cardTextJob3: {
        color: "#2F2F2F",
        fontSize: 13,
        fontWeight: '500',
    },
    cardTextJob4: {
        fontSize: 12,
        color: '#2F2F2F',
        fontWeight: '600'
    },

    cardTextJob5: {
        color: "#2F2F2F",
        fontSize: 10,
        fontWeight: '500',
    },
    cardTextJob7: {
        color: "#1E93E2",
        fontSize: 11,
        fontWeight: '500',
    },
    cardTextJob4_1: {
        color: "#fff",
        fontSize: 12,
        fontWeight: '500',
    },

    cardTextJob6: {
        color: "#1E93E2",
        fontSize: 11,
        fontWeight: '500',
    },


    cardText1: {
        color: "#2F2F2F",
        fontWeight: '500',
        fontSize: 12,
    },
    inputStyleWrap: {
        height: 38,
        backgroundColor: '#E5E5E5',
        borderRadius: 5,
    },
    headerWrap: {
        marginBottom: 13,
        marginTop: 20,
        paddingLeft: 25,
        paddingRight: 25,
    },
    serachbtnSty: {
        height: 39,
        borderRadius: 4,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#5ABC7A',
        marginTop: 24,
        marginLeft: 30,
        marginRight: 30,
        marginBottom: 20,
    },
    btnActionWrap: {
        display: 'flex',
        justifyContent: 'space-between',
        flexDirection: "row",
        alignItems: 'center',
    },
    btnAction: {
        borderRadius: 4,
        flex: 1,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#5ABC7A',
        margin: 5,
        paddingBottom: 10,
        paddingTop: 10,
        paddingLeft: 15,
        paddingRight: 15,
        minWidth: 100,
    },
    BtnSubmit: {
        borderRadius: 10,
        flex: 1,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#5ABC7A',
        // margin: 5,
        paddingBottom: 20,
        paddingTop: 20,
        paddingLeft: 15,
        paddingRight: 15,
        width: "100%",
    },
    ViewBtnSty: {
        height: 23,
        width: 50,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#1E93E2',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    btnStyIdc: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },

});

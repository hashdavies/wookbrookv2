import React, { Component, Fragment } from 'react';
import { Text, StyleSheet, ImageBackground, Keyboard, TouchableOpacity, Alert, FlatList } from 'react-native';
import { KeyboardAvoidingView, Image, ScrollView, Dimensions } from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons';
import Checker from '../../assets/workbrookAssets/checker.png';
import {
    Container,
    Header,
    Content,
    Textarea,
    Button,
    View,
    Thumbnail,
    Form,
    Item,
    Input,
    Label,
    Badge,
    Icon,
    Footer,
    Picker,
    Item as FormItem,
    Left,
    Body,
    CardItem,
    Card,
    Right,
    ListItem,
    List,
    CheckBox,
} from 'native-base';
import CustomizeHeader from '../frags/CustomizeHeader';

import { myspiner, temp, ShowNotification, AppInlineLoader } from '../../dependency/UtilityFunctions';
import Logo from '../../assets/workbrookAssets/logo.png';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import RBSheet from "react-native-raw-bottom-sheet";
import {
    LineChart,
    BarChart,
    PieChart,
    ProgressChart,
    ContributionGraph,
    StackedBarChart
} from "react-native-chart-kit";
import { Col, Row, Grid } from "react-native-easy-grid";
// import * as Progress from 'react-native-progress';
import ProgressBar from 'react-native-progress/Bar';
// import RNFetchBlob from 'react-native-fetch-blob'
import ImagePicker from 'react-native-image-picker';
import Modal from 'react-native-modal';
import AccountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import DashboardStore from '../../stores/Dashboard';
import Autocomplete from 'react-native-autocomplete-input';
import MultiSelect from 'react-native-multiple-select';
import { YOUR_CLOUDINARY_PRESET, YOUR_CLOUDINARY_NAME, requestCameraPermission, YOUR_CLOUDINARY_PRESET_JobBanner } from '../../dependency/UtilityFunctions';
import { SvgUri } from 'react-native-svg';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import EmptyListComponent from '../../StatelessScreens/EmptyListComponent';

const getscreenWidth = Dimensions.get("window").width;
const screenWidth = getscreenWidth - 50;
const data = {
    labels: ["Swim", "Bike", "Run"], // optional
    data: [0.4, 0.6]
};
const chartConfig = {
    backgroundGradientFrom: "transparent",
    backgroundGradientFromOpacity: 0,
    backgroundGradientTo: "transparent",
    backgroundGradientToOpacity: 0,
    color: (opacity = 1) => `rgba(241, 129, 56, ${opacity})`,
    strokeWidth: 2, // optional, default 3
    barPercentage: 0.5,
    useShadowColorFromDataset: false // optional
};

@observer
class PerformanceTracker extends Component {
    constructor(props) {
        super(props);
        this.mapRef = null;
        this.state = {
            displayProfile: false,
            fill: 60,
            fill2: 40,
            singleRate:{}

        }
        selectedItems = [];

        this.toggleSuccessPostModalShow = this.toggleSuccessPostModalShow.bind(this)
        this.toggleProfileModal = this.toggleProfileModal.bind(this)
    }

    toggleProfileModal = (item) => {
        this.setState({
            displayProfile: !this.state.displayProfile,
            singleRate:item
        })

    };
    
    toggleSuccessPostModalShow = () => {
        DashboardStore._ToggleSucessPostModal(true)
    };
    async componentDidMount() {
        DashboardStore.__GetshortlistAndInterviewRatio(0,"MyAnalytics");
        DashboardStore.__GetUserRateAndReviews(0,"MyAnalytics");
     
        }
    render() {
        const{MyAnalytics}=DashboardStore;
        const{singleRate}=this.state;
        let ErrorComponentDisplay = null;
        console.log(DashboardStore.JobPosting);

        if (DashboardStore.isProcessing === true) {
            ErrorComponentDisplay = AppInlineLoader(DashboardStore.isProcessing)

        }
        else {
            ErrorComponentDisplay = <EmptyListComponent
                // message={DashboardStore.MyAnalytics.ErrorMessage}
                message={"No rate available"}

                // onclickhandler={this.LoadAvailableProffesions}
            />
        }
   
        let ALLRatesandReviews =
            <FlatList
                // Data
                data={MyAnalytics.reviews}
                // Render Items
                renderItem={({ item }) => {
                    // console.log(This_selectedRecruiters);
                    let posterName = item.first_name == null || item.last_name == null ? item.username : `${item.first_name}  ${item.last_name} `

                     return (
                        <TouchableOpacity onPress={()=>this.toggleProfileModal(item)}>
                        <CardItem style={styles.cardItemStyle1}>
                            <Left style={{ flex: 1, justifySelf: "flex-start", display: 'flex', alignItems: 'flex-start', height: '100%', }}>
                                <Image source={{ uri: item.image_url }} style={{ width: 40, height: 40, borderRadius: 20 }} />

                            </Left>
                            <Body style={{ flex: 3 }}>
                     <Text style={[styles.txt2, { marginBottom: 7 }]}>{posterName}</Text>
                                <Text style={[styles.txt1, { marginBottom: 7 }]}>
                                  {item.review_message}
                        </Text>
                                <TouchableOpacity>
                                    <Text style={styles.txt2_12}>
                                        View
                            </Text>
                                </TouchableOpacity>


                            </Body>
                        </CardItem>
                    </TouchableOpacity>
                    
                   )
                }}
                // Item Key
                keyExtractor={(item, index) => String(index)}
            // Header (Title)
            // ListHeaderComponent={this.renderHeader}
            // Footer (Activity Indicator)
            // ListFooterComponent={this.renderFooter()}
            // On End Reached (Takes a function)
            // onEndReached={this.retrieveMore}
            // How Close To The End Of List Until Next Data Request Is Made
            // onEndReachedThreshold={0}
            // Refreshing (Set To True When End Reached)
            // refreshing={this.state.refreshing}
            />
        return (

            <Container style={styles.container}>
                {/* <CustomizeHeader
                    leftside='back'
                    title='empty'
                    rightside='empty'
                    statedColor='#fff'
                    icon='md-close'
                /> */}
                {/* <Button transparent full style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start', paddingLeft: 15, paddingRight: 15 }} onPress={() => { this.props.navigation.goBack() }}>
                    <SvgUri
                        // style={{ marginTop: 20, marginLeft: -17 }}
                        width='11px'
                        height='22px'
                        uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1597714885/Mobile_Assets/arrBackBlk_ralvt8.svg"
                    />

                </Button> */}
                {/* <View style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center', paddingLeft: 25, paddingRight: 25, marginTop: 30, marginBottom: 13, width: "100%" }}>
                    <View full style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center', padding: 12, backgroundColor: "#1E93E2", borderRadius: 5, width: "100%" }}>
                        <Text style={styles.activityStatus}>
                            PERFORMANCE
                   </Text>
                    </View>
                </View> */}

                <ScrollView
                    contentContainerStyle={{
                        paddingLeft: 25,
                        paddingRight: 25,
                        paddingBottom: 25,
                        minHeight: '90%',
                        display: 'flex',
                        justifyContent: 'space-between',
                    }}
                >
                    <View>
                        <View style={styles.chatsWrapper}>
                            <Text style={styles.txt7}>Shortlist/Interview Ratio</Text>

                            <Grid>
                                <Col style={{ display: "flex", flexDirection: 'row', justifyContent: "center", alignItems: "center", marginBottom: 15, marginTop: 15, }}>

                                    <AnimatedCircularProgress
                                        size={186}
                                        width={18}
                                        // fill={this.state.fill}
                                        fill={parseInt(MyAnalytics.shortlistAndInterviewRatio.shorlistedCount || 0)}

                                        lineCap={'round'}
                                        tintColor="#FF965D"
                                        backgroundColor="#FFF7F2">
                                        {
                                            (fill) => (
                                                <AnimatedCircularProgress
                                                    size={145}
                                                    width={18}
                                                    // fill={this.state.fill2}
                                                    fill={parseInt(MyAnalytics.shortlistAndInterviewRatio.interviewsCount || 0)}

                                                    lineCap={'round'}
                                                    tintColor="#304FFD"
                                                    backgroundColor="#EEF1FF">
                                                    {
                                                        (fill) => (
                                                            <View

                                                                style={{ height: 120, width: 120, borderRadius: 130 / 2, backgroundColor: "#FFFFFF" }}
                                                            />
                                                        )
                                                    }
                                                </AnimatedCircularProgress>
                                            )
                                        }
                                    </AnimatedCircularProgress>
                                </Col>

                            </Grid>
                            <Grid>
                                <Col style={styles.ringChartSty}>
                                    <SvgUri
                                        // style={{ marginTop: 20, marginLeft: -17 }}
                                        width='5.54px'
                                        height='5.54px'
                                        uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1597876022/Mobile_Assets/shortlist_iddlr0.svg"
                                    />
                                    <Text style={styles.Counter}>{MyAnalytics.shortlistAndInterviewRatio.shorlistedCount || 0 }</Text>
                                    <Text style={styles.dataName}>Shortlists</Text>

                                </Col>
                                <Col style={styles.ringChartSty}>
                                    <SvgUri
                                        // style={{ marginTop: 20, marginLeft: -17 }}
                                        width='5.54px'
                                        height='5.54px'
                                        uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1597876022/Mobile_Assets/interview_nyhbx9.svg"
                                    />
                                    <Text style={styles.Counter}>{MyAnalytics.shortlistAndInterviewRatio.interviewsCount || 0}</Text>
                                    <Text style={styles.dataName}>Interviews</Text>

                                </Col>
                                <Col style={[styles.ringChartSty, { borderRightWidth: 0 }]}>

                                    <Text style={styles.Counter}>{MyAnalytics.shortlistAndInterviewRatio.shortlistAndInterviewPercentage || 0}%</Text>

                                </Col>

                            </Grid>
                       
                       
                        </View>
                        <View style={styles.chatsWrapper}>
                            <View style={[styles.flexRow, { marginBottom: 19 }]}>
                                    <Text style={[styles.txt7]}>Reviews ({MyAnalytics.average_rating}</Text>
                                <SvgUri
                                    width='13px'
                                    height='12px'
                                    uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1597878389/Mobile_Assets/star_u78png.svg"
                                />
                                <Text style={[styles.txt7]}>)</Text>
                            </View>

                            {
                            MyAnalytics.reviews.length > 0 ?
                            ALLRatesandReviews
                                :
                                ErrorComponentDisplay

                        }
                       
                            
                            {/* <TouchableOpacity>
                                <CardItem style={styles.cardItemStyle1}>
                                    <Left style={{ flex: 1, justifySelf: "flex-start", display: 'flex', alignItems: 'flex-start', height: '100%', }}>
                                        <Image source={{ uri: "https://res.cloudinary.com/workbrook-hash/image/upload/v1597880343/Mobile_Assets/userNew_xw8bir.png" }} style={{ width: 40, height: 40, borderRadius: 20 }} />

                                    </Left>
                                    <Body style={{ flex: 3 }}>
                                        <Text style={[styles.txt2, { marginBottom: 7 }]}>Devon Williamson</Text>
                                        <Text style={[styles.txt1, { marginBottom: 7 }]}>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi.
                                </Text>
                                        <TouchableOpacity>
                                            <Text style={styles.txt2_12}>
                                                View
                                    </Text>
                                        </TouchableOpacity>


                                    </Body>
                                </CardItem>

                            </TouchableOpacity>
                            <TouchableOpacity>
                                <CardItem style={styles.cardItemStyle1}>
                                    <Left style={{ flex: 1, justifySelf: "flex-start", display: 'flex', alignItems: 'flex-start', height: '100%', }}>
                                        <Image source={{ uri: "https://res.cloudinary.com/workbrook-hash/image/upload/v1597880343/Mobile_Assets/userNew_xw8bir.png" }} style={{ width: 40, height: 40, borderRadius: 20 }} />

                                    </Left>
                                    <Body style={{ flex: 3 }}>
                                        <Text style={[styles.txt2, { marginBottom: 7 }]}>Devon Williamson</Text>
                                        <Text style={[styles.txt1, { marginBottom: 7 }]}>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi.
                                </Text>
                                        <TouchableOpacity>
                                            <Text style={styles.txt2_12}>
                                                View
                                    </Text>
                                        </TouchableOpacity>


                                    </Body>
                                </CardItem>
                            </TouchableOpacity> */}

                        </View>


                        {/* <View full style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center', padding: 19, backgroundColor: "#1E93E2", borderRadius: 5, width: "100%" }}>
                            <Text style={styles.activityStatus}>
                                View All Reviews
                   </Text>
                        </View> */}





                    </View>

                </ScrollView>

                <Modal style={styles.MContainer} isVisible={this.state.displayProfile} >
                    <Button onPress={()=>this.toggleProfileModal({})} style={{ backgroundColor: "transparent", position: 'absolute', right: 10, top: 40, padding: 20, elevation: 0 }}>
                        <SvgUri
                            width="19px"
                            height="19px"
                            uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593711047/Mobile_Assets/closeWht_oohxti.svg" />
                    </Button>
                    <View style={[styles.Mwrapper, { height: 390, paddingBottom: 20 }]}>

                        <Image source={{ uri:  singleRate.image_url }} style={{ width: 112, height: 112, borderRadius: 112 / 2, marginTop: -60 }} />
                        <ScrollView
                            style={{ flex: 1 }}
                        >
                            <View style={{ paddingLeft: 28, paddingRight: 28, display: 'flex', justifyContent: 'center', alignItems: 'center', paddingBottom: 10, paddingTop: 10, }}>

                                <Text style={styles.txt1, { textAlign: 'center', marginBottom: 10 }}>
                                  {singleRate.review_message}
                            </Text>
                          
                            </View>
                        </ScrollView>

                    </View>
                </Modal>

            </Container>


        );
    }
}
export default PerformanceTracker;

const styles = StyleSheet.create({
    container: {
        backgroundColor: "#FAFAFA",
        display: "flex",
        flex: 1,
        justifyContent: "flex-start",
    },
    btnCharts: {
        display: "flex",
        alignItems: "center",
        justifyContent: 'space-evenly',
        backgroundColor: '#fff',
        minHeight: 133,
        margin: 5,
        borderRadius: 20
    },
    chatsWrapper: {
        display: "flex",
        // alignItems: "center",
        // justifyContent: 'space-evenly',
        backgroundColor: '#fff',
        padding: 22,
        minHeight: 100,
        margin: 5,
        borderRadius: 20
    },
    cardItemStyle1: {
        alignItems: "flex-start",
        justifyContent: "flex-start",
        display: "flex",
        paddingRight: 3,
        paddingLeft: 3,
        borderBottomWidth: 1,
        borderBottomColor: "#E5E5E5",
    },
    progressItemsWrap: {
        display: "flex",
        alignItems: "center",
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: "100%",
        marginBottom: 4,
    },
    shortlistTime: {
        display: "flex",
        alignItems: "flex-end",
        flexDirection: 'row',
        justifyContent: 'flex-start',
        marginTop: 5
    },
    flexRow: {
        display: "flex",
        alignItems: "center",
        flexDirection: 'row',
        justifyContent: 'flex-start',
    },
    progressStatusWrap: {
        display: "flex",
        width: '100%',
        marginBottom: 14,
    },
    rowStyle: {
        backgroundColor: "#fff",

    },
    btnChartsTextWrap: {
        display: "flex",
        alignItems: "center",
        justifyContent: 'center',
    },
    btnChartsText: {
        fontSize: 12,
        color: '#8A9099',
        // marginTop: 14
    },
    UnionWrap: {
        padding: 16,
        backgroundColor: "#FFF5EF",
        borderRadius: 10
    },

    ringChartSty: {
        display: "flex",
        // flexDirection:'row', 
        justifyContent: "center",
        alignItems: "center",
        borderRightWidth: 1,
        borderColor: "#E8E9EB"
    },


    activityStatus: {
        fontSize: 10,
        color: '#FAFAFA',
        fontWeight: '500'
    },
    txt1: {
        fontSize: 10,
        color: '#8A9099',
    },
    txt2: {
        fontSize: 12,
        color: '#3F434A',
    },
    txt2_12: {
        fontSize: 12,
        color: '#1E93E2',
        fontWeight: '600',
    },
    txt3: {
        fontSize: 13,
        color: '#8A9099',
    },
    txt5: {
        fontSize: 15,
        color: '#49C96D',
    },
    txt7: {
        fontSize: 17,
        color: '#3F434A',
        fontWeight: '500'
    },
    txt3_2: {
        fontSize: 32,
        color: '#3F434A',
        fontWeight: '500'
    },
    txt3_0: {
        fontSize: 30,
        color: '#3F434A',
        fontWeight: '500'
    },
    Counter: {
        fontSize: 20,
        color: '#3F434A',
        fontWeight: '500'
    },
    dataName: {
        fontSize: 10,
        color: '#8A9099',
        // fontWeight: '500'
    },
    // Modal style =============================================================
    MContainer: {
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
    },
    Mwrapper: {
        height: 510,
        width: '90%',
        backgroundColor: '#fff',
        // padding: 15,
        display: 'flex',
        justifyContent: 'space-around',
        alignItems: 'center',
        borderRadius: 20,
    },
    homeModalwrap: {
        // height: 519,
        width: '90%',
        backgroundColor: '#fff',
        // padding: 15,
        // display: 'flex',
        justifyContent: 'space-between',
        // alignItems: 'center'
    },
    homeModalHeader: {
        height: 89,
        width: '100%',
        backgroundColor: '#1E93E2',
        padding: 15,
        display: 'flex',
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    homeModalContentWrap: {
        width: '100%',
        backgroundColor: '#fff',
        padding: 35,
        paddingBottom: 20,
        display: 'flex',
    },
    homeModalwrapText1: {
        fontSize: 60,
        fontSize: 16,
        color: '#FAFAFA',
    },
    btnSave: {
        height: 54,
        backgroundColor: '#1E93E2',
        elevation: 0,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    // Modal Style ends=========================================================A


});

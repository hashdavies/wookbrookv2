import React, { Component } from 'react';
import {
  Text,
  Image,
  StyleSheet,
  ImageBackground,
  Keyboard,
  TouchableOpacity
} from 'react-native';
import { KeyboardAvoidingView } from 'react-native';
import CustomizeHeader from '../frags/CustomizeHeader';

import Ionicons from 'react-native-vector-icons/Ionicons';
import {
  Container,
  Header,
  Content,
  Textarea,
  Button,
  View,
  Thumbnail,
  Form,
  Item,
  Input,
  Label,
  Badge,
  Icon,
  Footer,
  Picker,
  Left,
  Right,
  Body,
  Title,
  Center,
  CardItem
} from 'native-base';
import {
  Kaede,
  Hoshi,
  Jiro,
  Isao,
  Madoka,
  Akira,
  Hideo,
  Kohana,
  Makiko,
  Sae,
  Fumi,
} from 'react-native-textinput-effects';
import {
  TabView,
  TabBar,
  SceneMap,
  type NavigationState,
} from 'react-native-tab-view';
import { myspiner, getLodgedInDetailsProfile } from '../../dependency/UtilityFunctions';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import User from "../../assets/workbrookAssets/user4.png";

import accountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import { ScrollView } from 'react-native-gesture-handler';
import imageUser from '../../assets/workbrookAssets/user2.png';
import DatePicker from 'react-native-datepicker'
// import Autocomplete from 'native-base-autocomplete'
import Autocomplete from 'react-native-autocomplete-input';
import Requests from './Requests';
// import Insights from './Insights';
import Insights from './InsightsShortList';
import DashboardStore from '../../stores/Dashboard';
import { SvgUri } from 'react-native-svg';
const API = 'https://swapi.co/api';
const Dimensions = require('Dimensions');
let deviceWidth = Dimensions.get('window').width;
let deviceHeight = Dimensions.get('window').height;
const ScreenSIze = deviceWidth / 2;
let w1 = deviceWidth / 3;
let w = w1 - 12;
let t = deviceHeight / 3;
const boxsize = (t + t) / 3;
let h = deviceHeight / 3;

type State = NavigationState<{
  key: string,
  title: string,
}>;
const initialLayout = {
  height: 0,
  width: Dimensions.get('window').width,
};

@observer
class RecruiterDashboard extends Component<*, State> {
  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      routes: [
        { key: 'Requests', title: 'Requests' },
        { key: 'Insights', title: 'Insights ' },
      ],
      profile: {}
    }
  }

  async componentDidMount() {
    DashboardStore.UpdateNavigationState(this.props.navigation);
    let InnerProfile = await getLodgedInDetailsProfile();
    let isAvailable = InnerProfile.isAvailable;
    if (isAvailable === true) {

      let this_userprofile = InnerProfile.storeData;
      console.log(this_userprofile)
      console.log("InnerProfile")
      this.setState({
        profile: this_userprofile
      })
    }
    DashboardStore.__GetMyRecruiterProfile();

  }
  // Tab function Start here  
  _handleIndexChange = index => this.setState({ index });

  _renderTabBar = props => (
    <TabBar
      {...props}
      scrollEnabled
      indicatorStyle={styles.indicator}
      style={styles.tabbar}
      tabStyle={styles.tab}
      labelStyle={styles.label}
    />
  );

  _renderScene = SceneMap({
    Requests: Requests,
    Insights: Insights,
  });
  // Tab function ends here  


  render() {
    const { LoggedInUserProfile } = DashboardStore;
    let Fullname = '';
    if (LoggedInUserProfile.first_name == null || LoggedInUserProfile.last_name == null) {
      Fullname = `${LoggedInUserProfile.username} `;
    }
    else {
      Fullname = `${LoggedInUserProfile.first_name} ${LoggedInUserProfile.last_name} `

    }
    const { RecruiterProfile } = DashboardStore;
    console.log(RecruiterProfile)
    console.log("RecruiterProfile")
    let RecruiterFullname = '';
    RecruiterFullname = `${RecruiterProfile.fullname} `;

    return (
      <Container>
        {/* <CustomizeHeader
            leftside='WhiteArr'
            title='Insights'
            rightside='search'
            statedColor='#1E93E2'
            icon='md-close'
          /> */}
           <CardItem style={styles.ProfileHeader}>
        {/* <View style={[styles.profileWrapinner, { backgroundColor: "#2F2F2F" }]}> */}
        <Button transparent onPress={() => { this.props.navigation.goBack() }}>
                        <SvgUri
                          // style={{ marginTop: 20, marginLeft: -17 }}
                          width='11px'
                          height='22px'
                          uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1589394608/Mobile_Assets/arrbk_ipiscg.svg"
                        />
                        {/* <Image
                          source={WhiteArr}
                          style={{ height: 22, width: 11, }}
                        /> */}
                      </Button>
          <View style={{display: "flex", alignItems: "center"}}>
            <Image
              //  source={imageUser} 
              source={{ uri: RecruiterProfile.profilePicture }}
              style={{ height: 48, width: 48, borderRadius: 48 / 2, }} />
          
          {/* <Body style={{ marginLeft: 15 }}> */}
            <Text style={styles.Text1}>{RecruiterFullname}</Text>
            <View style={styles.textIcon}>
              <SvgUri
                width="9px"
                height="13.19px"
                uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593153738/Mobile_Assets/dropWht_u1yi2i.svg"
              />

              {/* <Image source={Drop} style={{ marginRight: 5 }} /> */}
              {/* <Text note style={[styles.nameSty2, { marginLeft: 5 }]}>{SingleRecruiter.location}</Text> */}
              <Text note style={[styles.nameSty2, { marginLeft: 5 }]}>{RecruiterProfile.location}</Text>
            </View>
          {/* </Body> */}
          </View>
          {/* <Right style={{ marginTop: -20, maxWidth: '30%' }}> */}
            <Button style={{ backgroundColor: 'transparent', elevation: 0, height: 30, padding: 10 }}
              onPress={() => { this.props.navigation.navigate("UpdateRecruiterProfile") }}
            >
              <Text style={styles.editBtnTxt}>
                Edit
              </Text>
            </Button>
          {/* </Right> */}

        </CardItem>
        <View style={styles.topperContentSide2}>
          <TabView
            //  style={[styles.container2, this.props.style]}
            navigationState={this.state}
            renderScene={this._renderScene}
            renderTabBar={this._renderTabBar}
            onIndexChange={this._handleIndexChange}
          // initialLayout={initialLayout}
          />
        </View>
      </Container>
    );
  }
}
export default RecruiterDashboard;

const inputStyles = {
  size: 100,
  color: '#FFFFFF',
};

const height = 40;

const styles = StyleSheet.create({
  Text1: {
    fontSize: 16,
    fontWeight: '600',
    color: "#FAFAFA",
  },
  profileWrap: {
    flex: 1,
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: "#fff"
  },
  ProfileHeader:{ 
    width: "100%", 
    backgroundColor: "transparent",
    display: 'flex',
    justifyContent: "space-between", 
    backgroundColor:"#000000",
},
  autocompleteContainer: {
    flex: 1,
    left: 0,
    position: 'absolute',
    right: 0,
    top: 0,
    zIndex: 1
  },
  
  profileWrapinner: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 20,
    paddingBottom: 35
  },
  profileinput: {
    marginLeft: 0,
    borderWidth: 1,
    borderColor: '#E5E5E5',
    height: 55,
    // marginBottom: 15
  },
  profileinput2: {
    marginLeft: 0,
    // borderWidth: 1,
    // borderColor: '#E5E5E5',
    height: 55,
    color: "#2F2F2F",
    fontSize: 14,
    fontWeight: '500'
    // marginBottom: 15
  },
  profileinputText: {
    height: "100%",
    color: "#2F2F2F",
    fontSize: 14,
    fontWeight: '500'
  },
  devtrop: {
    display: "flex",
    justifyContent: "flex-start",
  },
  topperContentSide2: {
    flex: 1,
    // marginTop:-20,
    // height: 300,
    width: '100%',
    paddingBottom: 15,
    // backgroundColor: '#2F2F2F',
    // paddingLeft: 10,
    // paddingRight: 10,
    
  },
  minBold2: {
    fontSize: 11,
    color: '#2F2F2F'
  },
  tabbar: {
    backgroundColor: '#000',
    height: 40,
    marginBottom: 20,
    display: 'flex',
    flexDirection: "row",
    justifyContent: 'space-between',
    alignItems: 'center',
    // elevation:0
  },
  tab: {
    width: ScreenSIze,
    display:"flex",
        flexDirection: "row",
        justifyContent:"center",
  },
  editBtnTxt: {
    fontSize: 12,
    color: '#fff',
    fontWeight: "600"
},
  label: {
    color: '#FAFAFA',
    fontWeight: '500',
    fontSize: 12,
  },
  indicator: {
    backgroundColor: '#FAFAFA',
  },
  // =============================================== drop address ==================
  textIcon: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center'
  },
  nameSty2: {
    fontSize: 12,
    color: '#fff'
  },
  // =============================================== drop address ==================
});

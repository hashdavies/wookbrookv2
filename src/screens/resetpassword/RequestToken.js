import React, { Component } from 'react';
import { Text, StyleSheet, ImageBackground, Keyboard, TouchableOpacity } from 'react-native';
import { KeyboardAvoidingView } from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons';
import {
  Container,
  Header,
  Content,
  Textarea,
  Button,
  View,
  Thumbnail,
  Form,
  Item,
  Input,
  Label,
  Badge,
  Icon,
  Footer,
  Picker,
  Item as FormItem,
} from 'native-base';
import {
  Kaede,
  Hoshi,
  Jiro,
  Isao,
  Madoka,
  Akira,
  Hideo,
  Kohana,
  Makiko,
  Sae,
  Fumi,
} from 'react-native-textinput-effects';

import { myspiner } from '../../dependency/UtilityFunctions';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';

import accountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';


@observer
class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      errorloading: false,

    }
    this._LoginHandler = this._LoginHandler.bind(this);
  }
  async  _LoginHandler() {
    const { email, password } = this.state;
    let data = {
      email,
      password
    }
    // let    result = await accountStore._LoginUserNowtest(data);
    let result = await accountStore._LoginUserNow(data);
    let re = accountStore.redirectmode;

    // console.warn(re)
    if (re === true) {
      Toast.show('Login Successfully', Toast.SHORT, Toast.BOTTOM, ToastStyle);

      this.props.navigation.navigate("SignedIn");
    }
    else {
      // console.warn('fuckkkkkk')
    }

  }
  render() {
    return (
      <ImageBackground
        source={CustomAsset.bullBlur_bg}
        style={styles.backgroundImage}
        resizeMode="cover"
        blurRadius={4}
      >

        <View style={[styles.main_wrapper]}>
          {myspiner(accountStore.loader_visible)}
          <View>
            <Text style={styles.header}>Forgot Password</Text>
          </View>
          <View padder style={[styles.wrapper]}>
 
           
            <View style={{ marginTop: 15, }}>

              <Hoshi
                label={'E-mail Address'}
                // this is used as active border color
                borderColor={'#1b1b1b'}
                // active border height
                // borderHeight={3}
                inputPadding={11}
                inputStyle={{ color: '#fff', fontSize: 20, }}
                labelStyle={{ color: '#fff', fontSize: 12, }}
                // inputStyle={[HideoStyle.input]}
                style={[HideoStyle.container]}
                // this is used to set backgroundColor of label mask.
                // please pass the backgroundColor of your TextInput container.
                // backgroundColor={'#F9F7F6'}
                returnKeyType={"next"}
                keyboardType={'email-address'}
              />
            </View>
         
            <View style={{ marginTop: 20, }}>
              <View style={{ flexDirection: 'column', marginBottom: 15 }}>
                <Button
                  iconRight
                  light
                  block
                  style={{ justifyContent: 'center', alignItems: 'center', borderRadius: 6, backgroundColor: '#5c84f1' }}
                // onPress={() => {this.props.navigation.navigate('Signup')}}
                >
                  <Text style={styles.textstyle}>Reset Password</Text>
                </Button>
              </View>
            </View>
        


          </View>
        </View>
      </ImageBackground>
    );
  }
}
export default Login;

const inputStyles = {
  size: 100,
  color: '#FFFFFF',
};

const height = 40;

HideoStyle = {
  container: {
    marginVertical: 5,
    borderRadius: 4,
    borderStyle: 'solid',
    borderWidth: 0.9,
    borderColor:'grey',

    // borderColor:'#357d83',
    // borderColor: '#727272',
    // borderColor:'#4ab1be',
    backgroundColor: 'transparent',
    flex: 0,
    // height: 'auto',
    overflow: 'hidden'
  },
  input: {
    height: height,
    backgroundColor: '#fff',
    fontWeight: '600',
    fontSize: 11,
  },
  size: {
    input: height,
    icon: 20
  },
  inputstyle:{ 
    color: '#fff',
     fontSize: 20, 

    }
}

const styles = StyleSheet.create({
  backgroundImage: {
    // height:null,
    flex: 1,
    width: null,
    // height: null,
    opacity: 2,
    // backgroundColor:'#000'
  },

  textstyle: {
    textAlign: 'center',
    flex: 1,
    color: '#fff',
    fontWeight: 'bold',
    fontSize:15,
  },

  textstyle3: {
    // textAlign: 'center',
    // flex: 1,
    color: '#4589f1',
    fontWeight: 'bold',
    // backgroundColor:'green'

  },
  iconstyle: {
    fontSize: 20,
    color: '#4589f1',
    width: 40,
    paddingLeft: 0,
    // alignItems:'center',

  },
  // Slide styles
  main_wrapper: {
    flex: 1, // Take up all screen
    //backgroundColor:'#000'
  },
  buttontext: {
    color: '#000',
    fontWeight: 'bold',
    fontSize: 17,
  },
  already: {
    // flex:1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 10,
    // height: 70,
  },
  wrapper: {
    flex: 1, // Take up all screen
    // justifyContent: 'center', // Center vertically
    // alignItems: 'center', // Center horizontally


  },
  backgroundImage: {
    // height:null,
    flex: 1,
    width: null,
    // height: null,
    opacity: 2,
  },
  inputfieldstyle: {
    // height: 40,
    // alignItems:'center',
    // textAlign:'center'
    // overflow: 'hidden'
  },
  action_btn: {
    // flex:1,
    flexDirection: 'row',
    marginTop: 30,
    // backgroundColor:'red'
  },
  canclebtnwrap: {
    flex: 1,
  },
  submitbtnwrap: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'flex-end',
    // backgroundColor:'red',
  },
  inputfield_wrapper_style: {
    // marginVertical: 5,
    borderRadius: 15,
    borderStyle: 'solid',
    borderWidth: 2,
    borderColor: '#000',
    marginBottom: 12,
  },
  // Header styles
  header: {
    color: '#fff',
    fontFamily: 'Avenir',
    fontSize: 30,
    fontWeight: 'bold',
    textAlign: 'left',
    marginTop: 50,
    marginLeft:10,
    // marginVertical: 15,
  },
  sectionhead: {
    color: '#000',
    fontSize: 14,
  },
  select_box_wrapper: {
    marginTop: 12,
    marginBottom: 12,
    borderRadius: 15,
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: '#000',
  },
  // Text below header
  text: {
    color: '#FFFFFF',
    fontFamily: 'Avenir',
    fontSize: 18,
    marginHorizontal: 40,
    textAlign: 'center',
  },
  card2: {
    padding: 16,
    paddingTop: 50,
    paddingBottom: 16,
    paddingLeft: 16,
    paddingRight: 16,
  },
  centralize_me: {
    justifyContent: 'center',
    // alignItems: 'center'
  },
});

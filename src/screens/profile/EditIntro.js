import React, { Component } from 'react';
import {
  Text,
  Image,
  StyleSheet,
  ImageBackground,
  Keyboard,
  TouchableOpacity,
} from 'react-native';
import { KeyboardAvoidingView } from 'react-native';
import CustomizeHeader from '../frags/CustomizeHeader';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {
  Container,
  Header,
  Content,
  Textarea,
  Button,
  View,
  Thumbnail,
  Form,
  Item,
  Input,
  Label,
  Badge,
  Icon,
  Footer,
  Picker,
  Left,
  Right,
  Body,
  Title,
  Center,
} from 'native-base';
import {
  Kaede,
  Hoshi,
  Jiro,
  Isao,
  Madoka,
  Akira,
  Hideo,
  Kohana,
  Makiko,
  Sae,
  Fumi,
} from 'react-native-textinput-effects';
// import Select from 'react-select';
import RNPickerSelect from 'react-native-picker-select';


import { myspiner, ShowNotification } from '../../dependency/UtilityFunctions';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Entypo from 'react-native-vector-icons/Entypo';
import User from "../../assets/workbrookAssets/user4.png";

import accountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import { ScrollView } from 'react-native-gesture-handler';
import DashboardStore from '../../stores/Dashboard';
import MultiSelect from 'react-native-multiple-select';
import GooglePlacesInput from '../frags/GooglePlacesInput';

items = [{
  id: '92iijs7yta',
  name: 'facebook',
}, {
  id: 'a0s0a8ssbsd',
  name: 'edves',
}, {
  id: '16hbajsabsd',
  name: 'anergy',
}, {
  id: 'sdhyaysdj',
  name: 'SweetSensation',
}, {
  id: 'suudydjsjd',
  name: 'Tee Princess',
}];
@observer
class EditIntro extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      errorloading: false,
      industrySelected:''

    }
    selectedItems = [];
  }
  onSelectedItemsChange = selectedItems => {
    this.setState({ selectedItems });
  };
  componentDidMount() {
    DashboardStore.UpdateNavigationState(this.props.navigation);
    DashboardStore.__GetCountry_List();
    DashboardStore.__GetUserIndustriesV2();
    DashboardStore.__GetMyCurrentProfile();
    let ___industry=DashboardStore.profileupdatedata.industry==null || DashboardStore.profileupdatedata.industry=='' ? '': DashboardStore.profileupdatedata.industry[0]

this.setState({
  industrySelected:___industry
})
  }
  proceednow = () => {
    if (DashboardStore.profileupdatedata.first_name === '') {
      ShowNotification('Please enter your first name', true);
    }
    else if (DashboardStore.profileupdatedata.last_name === '') {
      ShowNotification('Please enter your last name', true);

    }
    else if (DashboardStore.profileupdatedata.location === '') {
      ShowNotification('Please enter your location', true);

    }
    else {
      DashboardStore.__UpdateProfile();
    }
    //  alert("hey")

  }
  render() {
    const { selectedItems } = this.state;
    const { StaticData,AllIndustries } = DashboardStore;
    // let ___industry=DashboardStore.profileupdatedata.industry==null || DashboardStore.profileupdatedata.industry=='' ? '': DashboardStore.profileupdatedata.industry[0]
    return (
      <Container>
        {myspiner(DashboardStore.isProcessing)}

        <CustomizeHeader
          leftside='cancel'
          title='Edit Intro'
          rightside='save'
          statedColor='#1E93E2'
          icon='md-close'
          savehandler={this.proceednow}
        />
        <ScrollView
         keyboardShouldPersistTaps="handled"

        >
          <View style={styles.profileWrap}>
            <View style={styles.profileWrapinner}>
              {/* <Text>
                {JSON.stringify(typeof StaticData.CountryList.slice())}
                {JSON.stringify(typeof Object.values(StaticData.CountryList))}
              </Text> */}
              <Thumbnail large
                source={{ uri: DashboardStore.profileupdatedata.image_url }}
                style={{ height: 150, width: 150, borderRadius: 150 / 2 }} />
              <Button style={styles.editpenWap}
                onPress={() => this.props.navigation.navigate("EditProfile_pic")}
              >
                <FontAwesome5 name="pencil-alt" size={18} color='#1E93E2' />
              </Button>
            </View>
            <View style={{width: '100%', paddingLeft : 15, paddingRight: 15}}>
              <Text style={[styles.notices, { marginTop: 37, }]}>Required fields are marked * 
                                  
                                  {/* {JSON.stringify(DashboardStore.profileupdatedata)}  */}
                                  
                                   </Text>
            </View>


            <View style={{ width: "100%", padding: 15 }}>
              <Item stackedLabel style={[styles.profileinput, DashboardStore.profileupdatedata.first_name === '' ? styles.inputError_field : null]}>
              <Label style={styles.labelSty}>First Name<Text style={styles.asteriskSty}>*</Text></Label>
                <Input
                  style={styles.profileinputText}
                  placeholder="First Name"
                  defaultValue={DashboardStore.profileupdatedata.first_name}
                  onChangeText={(first_name) => DashboardStore.onChangeText('first_name', first_name, 'profileupdatedata')}
                />
              </Item>
              <Text style={styles.inputError}>{DashboardStore.profileupdatedata.first_name === '' ? 'Please enter your first name' : null}</Text>
              <Item stackedLabel style={[styles.profileinput, DashboardStore.profileupdatedata.first_name === '' ? styles.inputError_field : null]}>
              <Label style={styles.labelSty}>Last Name<Text style={styles.asteriskSty}>*</Text></Label>
                <Input
                  style={styles.profileinputText}
                  placeholder="Last Name"
                  defaultValue={DashboardStore.profileupdatedata.last_name}
                  onChangeText={(last_name) => DashboardStore.onChangeText('last_name', last_name, 'profileupdatedata')}
                />
              </Item>
              <Text style={styles.inputError}>{DashboardStore.profileupdatedata.first_name === '' ? 'Please enter your first name' : null}</Text>

              <Item stackedLabel style={[styles.profileinput, DashboardStore.profileupdatedata.email === '' ? styles.inputError_field : null]}>
              <Label style={styles.labelSty}>Email Address<Text style={styles.asteriskSty}>*</Text></Label>
                <Input
                  style={styles.profileinputText}
                  placeholder="Email Address"
                  defaultValue={DashboardStore.profileupdatedata.email}
                  // onChangeText={(last_name) => DashboardStore.onChangeText('last_name', last_name, 'profileupdatedata')}
                  editable={false}

                />
              </Item>
              <Text style={styles.inputError}>{DashboardStore.profileupdatedata.phone === '' ? 'Please enter your email Address' : null}</Text>
              <KeyboardAvoidingView>
              <Item stackedLabel style={[styles.profileinput, DashboardStore.profileupdatedata.phone === '' ? styles.inputError_field : null]}>
              <Label style={styles.labelSty}>Phone Number<Text style={styles.asteriskSty}>*</Text></Label>
                <Input
                  style={styles.profileinputText}
                  placeholder="Phone Number"
                  defaultValue={DashboardStore.profileupdatedata.phone}
                  onChangeText={(phone) => DashboardStore.onChangeText('phone', phone, 'profileupdatedata')}
                  keyboardType={"number-pad"}
                />
              </Item>
              </KeyboardAvoidingView>

              <View stackedLabel style={[styles.bottomIndBorder, { marginTop: 15, paddingBottom: 3, }]}>
                    <Label style={styles.labelSty}>Locations<Text style={styles.asteriskSty}>*</Text></Label>
                        <GooglePlacesInput
                            OnpressAction={(data, details = null) => { // 'details' is provided when fetchDetails = true
                            console.log(details)
                            console.log("data>>details")
                            console.log(data)
                                let latitude = details.geometry.location.lat;
                                let longitude = details.geometry.location.lng;
                                let ServiceAddress = details.formatted_address || details.description || details.name;
                                let nearestplace = details.name;
                                let addresstosend = `${ServiceAddress}   (  ${nearestplace}  )`;
                                DashboardStore._updateParameterV2(addresstosend, "location", "profileupdatedata");
                                let address_components=details.address_components;
                                address_components.forEach(element => {
                                  console.log(element);
                                  let thistype=element.types;
                                  let ThisCountryTag = thistype.includes("country"); 
                                  if(ThisCountryTag){
                                    console.log(ThisCountryTag)
                                let Mycountry=element.long_name
                                console.log(Mycountry)
                                DashboardStore._updateParameterV2(Mycountry, "country", "profileupdatedata");

                                  }
                                  let ThisStateTag = thistype.includes("administrative_area_level_1"); 
                                  if(ThisStateTag){
                                    console.log(ThisStateTag)
                                let MyState=element.long_name
                                console.log(MyState)
                                DashboardStore._updateParameterV2(MyState, "state", "profileupdatedata");

                                  }
                                 
                                });
                            }}
                        MydefaultLocation={DashboardStore.profileupdatedata.location}
                        />
                    </View>
                    <Text style={styles.inputError}>{DashboardStore.profileupdatedata.location === '' ? 'Please enter your location' : null}</Text>


              <View stackedLabel style={[styles.bottomIndBorder, { borderBottom: 1,marginTop: 5 }]}>
              <Label style={styles.labelSty}>Industry<Text style={styles.asteriskSty}>*</Text></Label>
                <RNPickerSelect
                  // useNativeAndroidPickerStyle= {false}
                  placeholder={{
                    label: 'Industry',
                    value: null,
                    color: '#2F2F2F',
                  }}
                  placeholderTextColor="#2F2F2F"
                  onValueChange={(value) => {

                    console.log(value)
                    this.setState({
                      industrySelected:value
                    })
                      let IndustryArray=[]
                      IndustryArray.push(value)
                    // DashboardStore._updateParameterV2(value, "industry", "profileupdatedata");
                    console.log(IndustryArray)
                    DashboardStore._updateParameterV2(IndustryArray, "industry", "profileupdatedata");

                  }}
                  // value={DashboardStore.profileupdatedata.industry}
                  value={this.state.industrySelected}
                  items={
                  //   [
                  //   { label: 'Engineer', value: 'Engineer' },
                  //   { label: 'Tech', value: 'Tech' },
                  //   { label: 'Business', value: 'Business' },
                  //   { label: 'Science', value: 'Science' },
                  // ]
                  AllIndustries.IndustryV2.slice()
                }
                />
                {/* <Input style={styles.profileinputText} placeholder="Industry" /> */}
              </View>
             
              <Text style={styles.inputError}>{DashboardStore.profileupdatedata.industry === '' ? 'Please choose an industry' : null}</Text>

              {/* <View stackedLabel style={[styles.bottomIndBorder, { borderBottom: 1, marginTop: 5 }]}>
              <Label style={styles.labelSty}>Country<Text style={styles.asteriskSty}>*</Text></Label>
                <RNPickerSelect
                  // useNativeAndroidPickerStyle= {false}
                  placeholder={{
                    label: 'Country',
                    value: null,
                    color: '#2F2F2F',
                  }}
                  placeholderTextColor="#2F2F2F"
                  onValueChange={(value, key) => {

                    console.log(value)
                    console.log(key)
                    DashboardStore._updateParameterV2(value, "country", "profileupdatedata");
                    DashboardStore.__GetStateInCountry_List(key);
                  }}
                  value={DashboardStore.profileupdatedata.country}
                  items={StaticData.CountryList.slice()}
                // items={[
                //   { label: 'State 1', value: 'State1'},
                //   { label: 'State 2', value: 'State2'},
                //   { label: 'State 3', value: 'State3'},
                //   { label: 'State 4', value: 'State4'},
                // ]}
                />
               </View> */}
              {/* <View stackedLabel style={[styles.bottomIndBorder, { borderBottom: 1, marginTop: 15}]}>
              <Label style={styles.labelSty}>State<Text style={styles.asteriskSty}>*</Text></Label>
                <RNPickerSelect
                  // useNativeAndroidPickerStyle= {false}
                  placeholder={{
                    label: 'State',
                    value: null,
                    color: '#2F2F2F',
                  }}
                  placeholderTextColor="#2F2F2F"
                  onValueChange={(value) => {

                    console.log(value)
                    DashboardStore._updateParameterV2(value, "state", "profileupdatedata");

                  }}
                  value={DashboardStore.profileupdatedata.state}
                  items={StaticData.StateInCOuntry.slice()}
                // items={[
                //   { label: 'State 1', value: 'State1'},
                //   { label: 'State 2', value: 'State2'},
                //   { label: 'State 3', value: 'State3'},
                //   { label: 'State 4', value: 'State4'},
                // ]}
                />
               </View> */}

            </View>

          </View>
        </ScrollView>
      </Container>
    );
  }
}
export default EditIntro;

const inputStyles = {
  size: 100,
  color: '#FFFFFF',
};

const height = 40;

const styles = StyleSheet.create({
  profileWrap: {
    flex: 1,
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: "#fff"
  },
  profileWrapinner: {
    flex: 1,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 25
  },
  profileinput: {
    marginLeft: 0,
    borderBottomColor: '#E5E5E5',
    height: 70,
    // marginBottom: 15
  },
  profileinput2: {
    marginLeft: 0,
    borderBottomColor: '#E5E5E5',
    // marginBottom: 15
  },
  bottomIndBorder: {
    marginLeft: 0,
    borderBottomWidth: 1,
    borderColor: '#E5E5E5',
    marginTop: 15,
},
  profileinputText: {
    // height: "100%",
    // color: "#2F2F2F",
    // fontSize: 13,
    // fontWeight: '500',
    color: "#2F2F2F",
        fontSize: 11,
        fontWeight: '300',
        marginBottom: 0,
  },
  editpenWap: {
    backgroundColor: '#fff',
    height: 40,
    width: 40,
    borderRadius: 20,
    display: 'flex',
    justifyContent: "center",
    alignItems: "center",
    zIndex: 99,
    marginTop: -30,
    marginLeft: 50,
    marginRight: -50
  },
  inputError: {
    color: '#FF5964',
    fontWeight: '300',
    fontSize: 11
  },
  inputError_field: {
    borderBottomColor: "#FF5964"
  },
  failedlabelSty: {
    color: "#FF5964",
    fontSize: 11,
    fontWeight: '500'
  },
  asteriskSty: {
    color: "#FF5964",
    fontSize: 10,
  },
  notices: {
    color: "#FF5964",
    fontSize: 10,
    fontWeight: '300',
  },
  labelSty: {
    color: "#2F2F2F",
    fontSize: 11,
    fontWeight: '500'
  },



});

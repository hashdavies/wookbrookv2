import React, { Component, Fragment } from 'react';
import { Text, StyleSheet, ImageBackground, Keyboard, TouchableOpacity } from 'react-native';
import { KeyboardAvoidingView, Image, ScrollView } from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons';
import Checker from '../../assets/workbrookAssets/checker.png';
import {
    Container,
    Header,
    Content,
    Textarea,
    Button,
    View,
    Thumbnail,
    Form,
    Item,
    Input,
    Label,
    Badge,
    Icon,
    Footer,
    Picker,
    Item as FormItem,
    Left,
    Right,
    Body,
    CardItem,
} from 'native-base';
import {
    Kaede,
    Hoshi,
    Jiro,
    Isao,
    Madoka,
    Akira,
    Hideo,
    Kohana,
    Makiko,
    Sae,
    Fumi,
} from 'react-native-textinput-effects';

import { myspiner, Communicator, getLodgedInDetailsProfile } from '../../dependency/UtilityFunctions';
import Logo from '../../assets/workbrookAssets/logo.png';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import RBSheet from "react-native-raw-bottom-sheet";
import Pen1 from "../../assets/workbrookAssets/editWht.png";
import Pen2 from "../../assets/workbrookAssets/editbl.png";
import Plus from "../../assets/workbrookAssets/plus.png";
import Drop from "../../assets/workbrookAssets/drop.png";
import Cup from "../../assets/workbrookAssets/cup.png";

import AccountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import { text } from 'react-native-communications';
// import {Communicator} from '../dependency/UtilityFunctions';
import { SvgUri } from 'react-native-svg';

import DashboardStore from '../../stores/Dashboard';
import EmptyListComponent from '../../StatelessScreens/EmptyListComponent';


@observer
class otherProfile extends Component {
    constructor(props) {
        super(props);
        this.mapRef = null;
        this.state = {
            email: '',
            password: '',
            errorloading: false,
            profile: {}
        }

        this._ReloadData = this._ReloadData.bind(this)
    }
    async componentDidMount() {
        DashboardStore.__GetOtherProfile();
        DashboardStore.UpdateNavigationState(this.props.navigation);
        // let InnerProfile= await getLodgedInDetailsProfile();
        // let isAvailable =InnerProfile.isAvailable;
        // if(isAvailable===true){

        // let this_userprofile=InnerProfile.storeData;
        // console.log(this_userprofile)
        // console.log("InnerProfile")
        // this.setState({
        //   profile:this_userprofile
        // })

        //     DashboardStore._UpdateProfileData(this_userprofile);
        //  }

    }
    _ReloadData = () => {
        DashboardStore.__GetOtherProfile();

    };



    render() {
        const { profile } = this.state;
        const { ProfileData, skills, experience, education } = DashboardStore.OtherProfile
        let Exper_ErrorComponentDisplay = <EmptyListComponent
            message={"No information provide"}
            onclickhandler={this._ReloadData}
        />
        let Educ_ErrorComponentDisplay = <EmptyListComponent
            message={"No information provide"}
            onclickhandler={this._ReloadData}
        />
        let Keywords_ErrorComponentDisplay = <EmptyListComponent
            message={"No information provide"}
            onclickhandler={this._ReloadData}
        />

        let MyExperience = experience.map((item, index) => {


            return (
                <CardItem style={{ width: "100%", backgroundColor: "transparent", paddingTop: 0 }} key={index}>
                    <Left style={{ alignItems: 'flex-start' }}>
                        <Thumbnail small source={{ uri: 'https://res.cloudinary.com/custocrypt/image/upload/v1569083972/BgvBscXZLT035MdzpNpHLbZmyPg2of.jpg' }} />
                        <Body style={styles.borderBtmsty2, { marginLeft: 15, borderBottomWidth: 1, borderColor: '#E5E5E5', paddingBottom: 5 }}>
                            <Text style={[styles.minBold3, { fontWeight: 'bold' }]}>{item.campany_name}</Text>

                            <Text note style={styles.minBold3}>{item.title}</Text>
                            <Text style={[styles.minBold3, { fontWeight: '300', marginTop: 3 }]}> {`${item.start_date} - ${item.end_date}`} </Text>
                            {/* <Button style={[styles.editpen, { backgroundColor: 'transparent', elevation: 0, height: 20, padding: 10 }]}
                   onPress={()=>this.__editExperience(item)}
                        >
                            <Image source={Pen2} style={{ marginRight: 5, height: 13, width: 13 }} />
                        </Button> */}
                        </Body>
                    </Left>
                </CardItem>
            )


        });
        let MyEducation = education.map((item, index) => {


            return (
                <CardItem style={{ width: "100%", backgroundColor: "transparent", paddingTop: 0 }} key={index}>
                    <Left style={{ alignItems: 'flex-start', borderBottomWidth: 1, borderColor: '#E5E5E5', paddingBottom: 5 }}>
                        <Thumbnail small source={{ uri: 'https://res.cloudinary.com/custocrypt/image/upload/v1569083972/BgvBscXZLT035MdzpNpHLbZmyPg2of.jpg' }} />
                        <Body style={styles.borderBtmsty2, { marginLeft: 15, }}>
                            <Text style={[styles.minBold3, { fontWeight: 'bold' }]}>{item.school}</Text>

                            <Text note style={styles.minBold3}>{item.field_of_study}</Text>
                            <Text style={[styles.minBold3, { fontWeight: '300', marginTop: 3 }]}>{`${item.start_date} - ${item.end_date}  (${item.degree})`} </Text>
                            {/* <Button style={[styles.editpen, { backgroundColor: 'transparent', elevation: 0, height: 20, padding: 10 }]}
                        onPress={()=>this.__editEducation(item)}
                        >
                            <Image source={Pen2} style={{ marginRight: 5, height: 13, width: 13 }} />
                        </Button> */}
                        </Body>
                    </Left>
                </CardItem>
            )


        });
        let MyKeywords = skills.map((item, index) => {


            return (
                <Button style={styles.btnChecky} key={index}>
                    <Text style={styles.btnCheckyText}>{item}</Text>
                </Button>
            )


        });






        // const{ProfileData}=DashboardStore;
        let Fullname = '';
        if (ProfileData.first_name == null || ProfileData.last_name == null) {
            Fullname = `${ProfileData.username} `;
        }
        else {
            Fullname = `${ProfileData.first_name} ${ProfileData.last_name} `

        }
        // let profileUrl =

        return (

            <Container style={styles.container}>
                <ScrollView>
                    <View style={styles.cardTopper}>
                        <View style={styles.cardContent1}>
                            <CardItem style={styles.ProfileHeader}>
                                <Button transparent onPress={() => { this.props.navigation.goBack() }}>
                                    <SvgUri
                                        // style={{ marginTop: 20, marginLeft: -17 }}
                                        width='11px'
                                        height='22px'
                                        uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1589394608/Mobile_Assets/arrbk_ipiscg.svg"
                                    />
                                    {/* <Image
                          source={WhiteArr}
                          style={{ height: 22, width: 11, }}
                        /> */}
                                </Button>
                                <View style={{ display: "flex", alignItems: "center" }}>
                                    <Image
                                        source={{ uri: ProfileData.image_url }}
                                        style={{ height: 60, width: 60, borderRadius: 60 / 2 }}
                                    />

                                    <View style={{ marginLeft: 15, display: "flex", alignItems: "center" }}>
                                        <Text style={styles.nameSty}>{Fullname}</Text>
                                        <View style={styles.textIcon2}>
                                            <SvgUri
                                                style={{ marginRight: 5 }}
                                                width="9px"
                                                height="12px"
                                                uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593153738/Mobile_Assets/dropWht_u1yi2i.svg"
                                            />
                                            <Text note style={styles.nameSty2}>{ProfileData.location == null ? "Not Availble":ProfileData.location}</Text>
                                        </View>

                                    </View>
                                </View>
                                    <View style={[styles.textIcon, {height: 18, width: 16}]}>
                                        {/* <SvgUri
                                            style={{ marginRight: 5 }}
                                            height="18px"
                                            width="16px"
                                            uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593739334/Mobile_Assets/cupWht_hx49pb.svg"
                                        /> */}
                                        {/* <Text note style={styles.nameSty2}>{ProfileData.totalPoint}</Text> */}
                                    </View>
                            </CardItem>
                        </View>
                        {/* <View style={styles.cardContent1}>
                            <CardItem style={{ width: "100%", backgroundColor: "transparent" }}>
                               
                                <Right>
                                    <View style={styles.textIcon}>
                                    <SvgUri
                                                style={{ marginRight: 5 }}
                                                height="18px"
                                                width="16px"
                                                uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593739334/Mobile_Assets/cupWht_hx49pb.svg"
                                            />
        <Text note style={styles.nameSty2}>{ProfileData.totalPoint}</Text>
                                    </View>
                                </Right>
                            </CardItem>
                        </View> */}
                    </View>
                    <View style={[styles.devtrop,]}>
                        <CardItem style={{ width: "100%", backgroundColor: "transparent" }}>
                            <Left>
                                <Text style={styles.minBold2} >About </Text>
                            </Left>
                            <Right>

                            </Right>
                        </CardItem>
                        <CardItem style={{ width: "100%", backgroundColor: "transparent", paddingTop: 0 }}>
                            <Left style={styles.borderBtmsty1}>
                                <Text style={styles.minBold3} >
                                    {ProfileData.about}
                                </Text>
                            </Left>
                        </CardItem>

                    </View>
                    <View style={styles.devtrop}>
                        <CardItem style={{ width: "100%", backgroundColor: "transparent" }}>
                            <Left>
                                <Text style={styles.minBold2} >Experience</Text>
                            </Left>

                        </CardItem>
                        {experience.length > 0 ?
                            MyExperience
                            :
                            Exper_ErrorComponentDisplay
                        }



                    </View>

                    <View style={styles.devtrop}>
                        <CardItem style={{ width: "100%", backgroundColor: "transparent" }}>
                            <Left>
                                <Text style={styles.minBold2} >Education</Text>
                            </Left>

                        </CardItem>
                        {education.length > 0 ?
                            MyEducation
                            :
                            Educ_ErrorComponentDisplay
                        }


                    </View>
                    <View style={styles.devtrop}>
                        <CardItem style={{ width: "100%", backgroundColor: "transparent" }}>
                            <Left>
                                <Text style={styles.minBold2} >Skills/Keywords</Text>
                            </Left>

                        </CardItem>
                        <CardItem style={{ width: "100%", backgroundColor: "transparent", paddingTop: 0, maxWidth: '100%' }}>
                            <Left style={{ alignItems: 'flex-start', borderBottomWidth: 1, borderColor: '#E5E5E5', paddingBottom: 15, flexWrap: 'wrap', width: '100%' }}>
                                {skills.length > 0 ?
                                    MyKeywords
                                    :
                                    Keywords_ErrorComponentDisplay
                                }

                            </Left>
                        </CardItem>
                    </View>
                    <View style={styles.devtrop}>
                        <CardItem style={{ width: "100%", backgroundColor: "transparent" }}>
                            <Left>
                                <Text style={styles.minBold2} >Resume/Attached Documents</Text>
                            </Left>

                        </CardItem>
                        {ProfileData.resume != null ?
                            <CardItem style={{ width: "100%", backgroundColor: "transparent", paddingTop: 0 }}>
                                <Left style={{ alignItems: 'flex-start', borderBottomWidth: 1, borderColor: '#E5E5E5', paddingBottom: 5 }}>
                                    <Button style={[styles.btnChecky, { borderWidth: 0, paddingLeft: 0, paddingRight: 0 }]}
                                        onPress={() => Communicator('web', ProfileData.resume)}

                                    >
                                        <Text style={[styles.btnCheckyText, { color: '#000' }]}>{`${Fullname}'s Resume`}</Text>
                                    </Button>
                                </Left>
                            </CardItem>
                            :
                            <CardItem style={{ width: "100%", backgroundColor: "transparent", paddingTop: 0 }}>
                                <Left style={{ alignItems: 'flex-start', borderBottomWidth: 1, borderColor: '#E5E5E5', paddingBottom: 5 }}>
                                    <Button style={[styles.btnChecky, { borderWidth: 0, paddingLeft: 0, paddingRight: 0 }]}

                                    >
                                        <Text style={[styles.btnCheckyText, { color: '#000' }]}>Not Available</Text>
                                    </Button>
                                </Left>
                            </CardItem>
                        }
                        {ProfileData.links != null ?

                            <CardItem style={{ width: "100%", backgroundColor: "transparent", paddingTop: 0 }}>
                                <Left style={{ alignItems: 'flex-start', borderBottomWidth: 1, borderColor: '#E5E5E5', paddingBottom: 5 }}>
                                    <Button style={[styles.btnChecky, { borderWidth: 0, paddingLeft: 0, paddingRight: 0 }]}
                                        onPress={() => Communicator('web', ProfileData.links.Facebook)}

                                    >
                                        <Text style={styles.btnCheckyText}>Facebook</Text>
                                        {/* <Text style={styles.btnCheckyText}>{JSON.stringify(ProfileData)}</Text> */}
                                    </Button>
                                </Left>
                                <Left style={{ alignItems: 'flex-start', borderBottomWidth: 1, borderColor: '#E5E5E5', paddingBottom: 5 }}>
                                    <Button style={[styles.btnChecky, { borderWidth: 0, paddingLeft: 0, paddingRight: 0 }]}
                                        onPress={() => Communicator('web', ProfileData.links.LinkedIn)}

                                    >
                                        <Text style={styles.btnCheckyText}>LinkedIn</Text>
                                    </Button>
                                </Left>
                                <Left style={{ alignItems: 'flex-start', borderBottomWidth: 1, borderColor: '#E5E5E5', paddingBottom: 5 }}>
                                    <Button style={[styles.btnChecky, { borderWidth: 0, paddingLeft: 0, paddingRight: 0 }]}
                                        onPress={() => Communicator('web', ProfileData.links.Portfolio)}

                                    >
                                        <Text style={styles.btnCheckyText}>Portfolio</Text>
                                    </Button>
                                </Left>
                                <Left style={{ alignItems: 'flex-start', borderBottomWidth: 1, borderColor: '#E5E5E5', paddingBottom: 5 }}>
                                    <Button style={[styles.btnChecky, { borderWidth: 0, paddingLeft: 0, paddingRight: 0 }]}
                                        onPress={() => Communicator('web', ProfileData.links.Twitter)}

                                    >
                                        <Text style={styles.btnCheckyText}>Twitter</Text>
                                    </Button>
                                </Left>
                            </CardItem>
                            :
                            null
                        }
                    </View>
                </ScrollView>
            </Container>


        );
    }
}
export default otherProfile;

const styles = StyleSheet.create({
    container: {
        backgroundColor: "#FAFAFA",
        display: "flex",
        flex: 1,
        justifyContent: "flex-start",
    },
    ProfileHeader: {
        width: "100%",
        backgroundColor: "transparent",
        display: 'flex',
        justifyContent: "space-between",
    },
    devtrop: {
        display: "flex",
        justifyContent: "flex-start",
    },
    cardTopper: {
        height: 157,
        backgroundColor: "#1E93E2",
        display: "flex",
        width: '100%',
        justifyContent: "space-between",
        paddingTop: 5,
        paddingBottom: 10,
    },
    cardContent1: {
        display: "flex",
        flexDirection: "row",
        width: '100%',
    },
    nameSty: {
        fontSize: 16,
        fontWeight: "600",
        color: '#fff'
    },
    nameSty2: {
        fontSize: 12,
        color: '#fff'
    },
    textIcon: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center'
    },
    textIcon2: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 5,
    },
    minBold: {
        fontSize: 10,
        fontWeight: "bold",
        color: '#fff'
    },
    minBold2: {
        fontSize: 14,
        fontWeight: "500",
        color: '#2F2F2F'
    },
    minBold3: {
        fontSize: 11,
        color: '#2F2F2F'
    },
    borderBtmsty1: {
        borderBottomWidth: 1,
        borderBottomColor: "#E5E5E5",
        paddingBottom: 5
    },
    borderBtmsty2: {
        borderBottomWidth: 1,
        borderBottomColor: "#E5E5E5",
        paddingBottom: 5
    },
    btnChecky: {
        height: 28,
        borderWidth: 1,
        borderColor: '#1E93E2',
        borderRadius: 5,
        backgroundColor: "transparent",
        elevation: 0,
        paddingLeft: 28,
        paddingRight: 28,
        marginRight: 5,
        marginBottom: 5
    },
    btnCheckyText: {
        fontSize: 11,
        fontWeight: 'bold',
        color: '#1E93E2'
    },
    editpen: {
        position: 'absolute',
        right: 0
    },

});

import React, { Component } from 'react';
import {
  Text,
  Image,
  StyleSheet,
  ImageBackground,
  Keyboard,
  TouchableOpacity, Platform
} from 'react-native';
import { KeyboardAvoidingView } from 'react-native';
import CustomizeHeader from '../frags/CustomizeHeader';
import { InteractionManager } from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons';
import {
  Container,
  Header,
  Content,
  Textarea,
  Button,
  View,
  Thumbnail,
  Form,
  Item,
  Input,
  Label,
  Badge,
  Icon,
  Footer,
  Picker,
  Item as FormItem,
  Left,
  Right,
  Body,
  Title,
  Center,
} from 'native-base';
import {
  Kaede,
  Hoshi,
  Jiro,
  Isao,
  Madoka,
  Akira,
  Hideo,
  Kohana,
  Makiko,
  Sae,
  Fumi,
} from 'react-native-textinput-effects';
import Modal from 'react-native-modal';
import RNFetchBlob from 'react-native-fetch-blob'
import ImagePicker from 'react-native-image-picker';
import { myspiner, ShowNotification, pickSingleWithCamera,pickSingleGallery ,currentTime} from '../../dependency/UtilityFunctions';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Entypo from 'react-native-vector-icons/Entypo';
import User from "../../assets/workbrookAssets/user4.png";

import accountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import DashboardStore from '../../stores/Dashboard';
import { YOUR_CLOUDINARY_PRESET, YOUR_CLOUDINARY_NAME, requestCameraPermission, RefreshProfile } from '../../dependency/UtilityFunctions';
// import ImagePicker from 'react-native-image-crop-picker';

@observer
class EditProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      errorloading: false,


      avatarSource: null,
      cover_pix: null,
      isImagefromdevice: false,
      loader_visible: false,
      showsaveBtn: false,
      imagetoUpload: null,

    }
    this._LoginHandler = this._LoginHandler.bind(this);
    this._selectPhotoTapped = this._selectPhotoTapped.bind(this)
    this._uploadImage = this._uploadImage.bind(this)
    this._SelectPhotofromCamera = this._SelectPhotofromCamera.bind(this)
    this._SelectPhotofromgallery = this._SelectPhotofromgallery.bind(this)
  }
  toggleModal = () => {

    this.setState({ isModalVisible: !this.state.isModalVisible });

  };
  async _LoginHandler() {
    const { email, password } = this.state;
    let data = {
      email,
      password
    }
    // let    result = await accountStore._LoginUserNowtest(data);
    let result = await accountStore._LoginUserNow(data);
    let re = accountStore.redirectmode;

    // console.warn(re)
    if (re === true) {
      Toast.show('Login Successfully', Toast.SHORT, Toast.BOTTOM, ToastStyle);

      this.props.navigation.navigate("SignedIn");
    }
    else {
      // console.warn('fuckkkkkk')
    }

  }
  async componentDidMount() {
    // InteractionManager.runAfterInteractions( async () => {

    // await requestCameraPermission();
    // RefreshProfile();
    //  console.warn("permiss")
    //  });
  }

  _uploadImage() {
    // this.setState({
    //   loader_visible: true
    // });
    ShowNotification('Uploading your file...', true);
    let file2upload = this.state.imagetoUpload;
    // console.warn(file2upload)
    let source = { uri: file2upload.uri };
    this.setState({
      uploadingImg: true
    });
    uploadFile(file2upload)
      .then(response => response.json())
      .then(result => {
        console.log(result)
        let imageurl = result.secure_url;
        DashboardStore._updateParameterV2(imageurl, "image_url", "profileupdatedata");
        ShowNotification('Saving to database....', true);
        DashboardStore.__UpdateProfile();
        // this.SavetoDatabase(result)
        // this.setState({
        //     avatarSource: { uri: result.secure_url },
        //     loader_visible: false
        // });
      })

  }

  _selectPhotoTapped() {
    // const options = {
    //   quality: 1.0,
    //   maxWidth: 500,
    //   maxHeight: 500,
    //   storageOptions: {
    //     skipBackup: true
    //   },
    //   mediaType:'photo',
    //   // mediaType:'video',
    // };

    const options = {
      title: 'Select',
      noData: true,
      mediaType: 'mixed',
      storageOptions: {
        skipBackup: true,
        path: 'construction-cloud'
      }
    };


    ImagePicker.showImagePicker(options, (response) => {
      console.log(response);
      // ImagePicker.launchImageLibrary(options, (response)  => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.warn('User cancelled photo picker');
      }
      else if (response.error) {
        console.warn('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.warn('User tapped custom button: ', response.customButton);
      }
      else {
        let source = { uri: response.uri };

        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          avatarSource: source,
          isImagefromdevice: true,
          showsaveBtn: true,
          imagetoUpload: response,
        });
      }

    });
  }
    // _SelectPhotofromCamera() {
      _SelectPhotofromCamera = async () => {

//     console.log("click")

   let imagedata = await pickSingleWithCamera(true);
   console.log(imagedata)
   console.log("imagedata")
   let source = { uri: imagedata.path };
   let padder=currentTime();
   let j={
     fileName:`hash ${padder}`,
     uri:imagedata.path,
     fileSize:7809
   }
   let resp={...imagedata,...j}
   this.setState({
     avatarSource: source,
     isImagefromdevice: true,
     showsaveBtn: true,
     imagetoUpload: resp,
   });
   this.toggleModal();
  }
      _SelectPhotofromgallery = async () => {

//     console.log("click")

   let imagedata = await pickSingleGallery(true);
console.log(imagedata)
console.log("imagedata")
let source = { uri: imagedata.path };
let padder=currentTime();
let j={
  fileName:`hash ${padder}`,
  uri:imagedata.path,
  fileSize:7809
}
let resp={...imagedata,...j}
this.setState({
  avatarSource: source,
  isImagefromdevice: true,
  showsaveBtn: true,
  imagetoUpload: resp,
});
this.toggleModal();
// ImagePicker.openCamera({
//   width: 300,
//   height: 400,
//   cropping: true,
// }).then(image => {
//   console.log(image);
// });
  }

  proceednow = () => {
    const { isImagefromdevice, avatarSource, loader_visible, showsaveBtn } = this.state;

    if (showsaveBtn === false) {
      ShowNotification('Select image before continue', true);
    }

    else {
      DashboardStore._ToggleProcessing(true)
      this._uploadImage();
      // DashboardStore.__UpdateProfile();
    }
    //  alert("hey")

  }
  render() {
    const { isImagefromdevice, avatarSource, loader_visible, showsaveBtn } = this.state;
    let renderImage;

    if (isImagefromdevice === true) {
      renderImage = <Thumbnail
        large
        source={avatarSource}
        style={{ height: 254, width: 254, borderRadius: 254 / 2 }}

      />
    }
    else if (DashboardStore.profileupdatedata.image_url !== null || DashboardStore.profileupdatedata.image_url !== '') {

      renderImage = <Thumbnail
        large
        source={{ uri: DashboardStore.profileupdatedata.image_url }}
        style={{ height: 254, width: 254, borderRadius: 254 / 2 }}

      />
    }
    return (
      <Container>
        <CustomizeHeader
          leftside='cancel'
          title='Edit Profile Picture'
          rightside='save'
          statedColor='#1E93E2'
          icon='md-close'
          savehandler={this.proceednow}
        />
        <View style={styles.profileWrap}>
          <View style={styles.profileWrapinner}>
            <TouchableOpacity
              onPress={this.toggleModal}
            // onPress={this._selectPhotoTapped}
            >
              {/* <Thumbnail 
            large 
            source={{uri:DashboardStore.profileupdatedata.image_url}}
            style={{ height: 254, width: 254, borderRadius: 254 / 2 }}
            
            /> */}
              {renderImage}
            </TouchableOpacity>
          </View>
          {/* <View style={styles.profileFooter}>
            <Left>
            <Button transparent >
                <FontAwesome5 name="pencil-alt" size={20} color='#fff' />
              </Button>
            </Left>
            <View>
            <Button transparent >
                <FontAwesome5 name="camera" size={20} color='#fff' />
              </Button>
            
            </View>
            <Right>
            <Button transparent>
                <FontAwesome5 name="trash-alt" size={20} color='#fff' />
              </Button>
            </Right>
          </View> */}
          <Modal style={styles.MContainer} isVisible={this.state.isModalVisible}>
            <View style={styles.Mwrapper}>
              <View style={styles.firstView} >
                <Button full style={styles.actionBtn}
                  onPress={this._SelectPhotofromCamera}
                  // _SelectPhotofromgallery
                >
                  <Text style={styles.actionBtnText}>
                    Take a photo
              </Text>
                </Button>
                <Button full style={[styles.actionBtn, { borderTopWidth: 1, borderTopColor: "#E5E5E5", borderBottomWidth: 1, borderBottomColor: "#E5E5E5" }]}
                     onPress={this._SelectPhotofromgallery}
                     
                >
                  <Text style={styles.actionBtnText}>
                    Choose from your gallery
              </Text>
                </Button>
                {/* <Button full style={styles.actionBtn}>
                  <Text style={[styles.actionBtnText, { color: "#FF5964" }]}>
                    Remove photo
              </Text>
                </Button> */}
              </View>
              <Button full style={[styles.btnCancel, { marginTop: 15 }]} title="Hide modal" onPress={this.toggleModal}>
                <Text style={[styles.btnCancelText,]}>Cancel</Text>
              </Button>
            </View>
          </Modal>

        </View>

      </Container>
    );
  }
}
export default EditProfile;

function uploadFile(file) {
  // console.warn(file)
  let fileDir = file.uri;

  const cleanFilePath = Platform.OS === 'ios' ? fileDir.replace('file://', '') : fileDir;


  return RNFetchBlob.fetch('POST', 'https://api.cloudinary.com/v1_1/' + YOUR_CLOUDINARY_NAME() + '/image/upload?upload_preset=' + YOUR_CLOUDINARY_PRESET(), {
    'Content-Type': 'multipart/form-data'
  }, [
    { name: 'file', filename: file.fileName, data: RNFetchBlob.wrap(cleanFilePath) }
  ])
}
const inputStyles = {
  size: 100,
  color: '#FFFFFF',
};

const height = 40;

const styles = StyleSheet.create({
  profileWrap: {
    flex: 1,
    display: 'flex',
    justifyContent: 'flex-end',
    alignItems: 'center',
    backgroundColor: "#FAFAFA"
  },
  Mwrapper: {
    // height: 360,
    width: '90%',
    // backgroundColor: '#fff',
    padding: 15,
    display: 'flex',
    justifyContent: 'space-around',
    // alignItems: 'center'
  },
  profileWrapinner: {
    flex: 1,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  profileFooter: {
    padding: 20,
    height: 60,
    width: "100%",
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  MContainer: {
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
  },
  firstView: {
    backgroundColor: '#FAFAFA',
    borderRadius: 5
  },
  actionBtn: {
    backgroundColor: '#FAFAFA',
    borderRadius: 5,
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    height: 43,
  },
  actionBtnText: {
    fontSize: 12,
    color: '#1E93E2',
  },
  btnCancel: {
    backgroundColor: '#FF5964',
    borderRadius: 5,
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    height: 43,
    elevation: 0,
  },
  btnCancelText: {
    fontSize: 12,
    color: '#FAFAFA',
    fontWeight: 'bold',
  },

});

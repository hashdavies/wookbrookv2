import React, { Component } from 'react';
import {
  Text,
  Image,
  StyleSheet,
  ImageBackground,
  Keyboard,
  TouchableOpacity
} from 'react-native';
import { KeyboardAvoidingView } from 'react-native';
import CustomizeHeader from '../frags/CustomizeHeader';

import Ionicons from 'react-native-vector-icons/Ionicons';
import {
  Container,
  Header,
  Content,
  Textarea,
  Button,
  View,
  Thumbnail,
  Form,
  Item,
  Input,
  Label,
  Badge,
  Icon,
  Footer,
  Picker,
  Left,
  Right,
  Body,
  Title,
  Center,
} from 'native-base';
import {
  Kaede,
  Hoshi,
  Jiro,
  Isao,
  Madoka,
  Akira,
  Hideo,
  Kohana,
  Makiko,
  Sae,
  Fumi,
} from 'react-native-textinput-effects';

import { myspiner } from '../../dependency/UtilityFunctions';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Entypo from 'react-native-vector-icons/Entypo';
import User from "../../assets/workbrookAssets/user4.png";

import accountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import { ScrollView } from 'react-native-gesture-handler';
import DashboardStore from '../../stores/Dashboard';


@observer
class EditAbout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      errorloading: false,

    }
    this._LoginHandler = this._LoginHandler.bind(this);
    this.proceednow = this.proceednow.bind(this);

  }
  componentDidMount() {
    DashboardStore.UpdateNavigationState(this.props.navigation);
  }
  async  _LoginHandler() {
    const { email, password } = this.state;
    let data = {
      email,
      password
    }
    // let    result = await accountStore._LoginUserNowtest(data);
    // let result = await accountStore._LoginUserNow(data);
    // let re = accountStore.redirectmode;

    // console.warn(re)
    // if (re === true) {
    //   Toast.show('Login Successfully', Toast.SHORT, Toast.BOTTOM, ToastStyle);

    //   this.props.navigation.navigate("SignedIn");
    // }
    // else {
    //   // console.warn('fuckkkkkk')
    // }

  }
  proceednow=() =>{
  //  alert("hey")
DashboardStore.__UpdateProfile();
  }
  render() {
    return (
      <React.Fragment>
        {  myspiner(DashboardStore.isProcessing)}
        <Container>
         
         <ScrollView>
           <CustomizeHeader
             leftside='cancel'
             title='Edit About'
             rightside='save'
             statedColor='#1E93E2'
             icon='md-close'
             savehandler={this.proceednow }
           />
           <View style={styles.profileWrap}>
             <View style={{ width: "100%", padding: 15 }}>
               <View style={styles.cardContent1}>
                 <Left>
                   <Text style={styles.minBold2} >Summary</Text>
                 </Left>
               </View>
               <Textarea 
               rowSpan={5}
                style={styles.bordedTextArea} placeholder="" 
                defaultValue={DashboardStore.profileupdatedata.about}
                onChangeText={(about) => DashboardStore.onChangeText('about', about,'profileupdatedata')}
                selectTextOnFocus={true}
                autoFocus={true}
                />
               {/* <Text style={styles.inputError}>Please enter your summary</Text> */}
             </View>
 
           </View>
         </ScrollView>
       </Container>
    
      </React.Fragment>
 
   );
  }
}
export default EditAbout;

const inputStyles = {
  size: 100,
  color: '#FFFFFF',
};

const height = 40;

const styles = StyleSheet.create({
  profileWrap: {
    flex: 1,
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: "#fff"
  },
  profileWrapinner: {
    flex: 1,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 25
  },
  profileinput: {
    marginLeft: 0,
    borderBottomColor: '#E5E5E5',
    height: 70,
    // marginBottom: 15
  },
  profileinputText: {
    height: "100%",
    color: "#2F2F2F",
    fontSize: 13,
    fontWeight: '500'
  },
  editpenWap: {
    backgroundColor: '#fff',
    height: 40,
    width: 40,
    borderRadius: 20,
    display: 'flex',
    justifyContent: "center",
    alignItems: "center",
    zIndex: 99,
    marginTop: -30,
    marginLeft: 50,
    marginRight: -50
  },
  inputError: {
    color: '#FF5964',
    fontWeight: '300',
    fontSize: 11
  },
  minBold2: {
    fontSize: 13,
    fontWeight: "500",
    color: '#2F2F2F'
  },
  cardContent1: {
    display: "flex",
    flexDirection: "row",
    width: '100%',
    // paddingLeft: 5,
    // paddingRight: 5,
  },
  bordedTextArea: {
    borderBottomColor: "#E5E5E5",
    borderBottomWidth: 1
  },



});

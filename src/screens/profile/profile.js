import React, { Component, Fragment } from 'react';
import { Text, StyleSheet, ImageBackground, Keyboard, TouchableOpacity } from 'react-native';
import { KeyboardAvoidingView, Image, ScrollView } from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons';
import Checker from '../../assets/workbrookAssets/checker.png';
import {
    Container,
    Header,
    Content,
    Textarea,
    Button,
    View,
    Thumbnail,
    Form,
    Item,
    Input,
    Label,
    Badge,
    Icon,
    Footer,
    Picker,
    Item as FormItem,
    Left,
    Right,
    Body,
    CardItem,
} from 'native-base';
import {
    Kaede,
    Hoshi,
    Jiro,
    Isao,
    Madoka,
    Akira,
    Hideo,
    Kohana,
    Makiko,
    Sae,
    Fumi,
} from 'react-native-textinput-effects';

import { myspiner, Communicator, getLodgedInDetailsProfile, ParseDate2string, GetPartDate } from '../../dependency/UtilityFunctions';
import Logo from '../../assets/workbrookAssets/logo.png';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import RBSheet from "react-native-raw-bottom-sheet";
import Pen1 from "../../assets/workbrookAssets/editWht.png";
import Pen2 from "../../assets/workbrookAssets/editbl.png";
import Plus from "../../assets/workbrookAssets/plus.png";
import Drop from "../../assets/workbrookAssets/drop.png";
import Cup from "../../assets/workbrookAssets/cup.png";

import AccountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import { text } from 'react-native-communications';
// import {Communicator} from '../dependency/UtilityFunctions';
import DashboardStore from '../../stores/Dashboard';
import EmptyListComponent from '../../StatelessScreens/EmptyListComponent';
import { SvgUri } from 'react-native-svg';


@observer
class Landing extends Component {
    constructor(props) {
        super(props);
        this.mapRef = null;
        this.state = {
            email: '',
            password: '',
            errorloading: false,
            profile: {}
        }
        this.__editExperience = this.__editExperience.bind(this)
        this.__AddNewExpeince = this.__AddNewExpeince.bind(this)
        this.__editEducation = this.__editEducation.bind(this)
        this.__AddEducation = this.__AddEducation.bind(this)
    }
    async componentDidMount() {
        DashboardStore.__FetchAllMyExperience();
        DashboardStore.__FetchAllMyEducation();
        DashboardStore.__FetchAllMyKeywords();
        DashboardStore.__GetWbLeaguePointAggregation();

        DashboardStore.UpdateNavigationState(this.props.navigation);
        let InnerProfile = await getLodgedInDetailsProfile();
        let isAvailable = InnerProfile.isAvailable;
        if (isAvailable === true) {

            let this_userprofile = InnerProfile.storeData;
            console.log(this_userprofile)
            console.log("InnerProfile")
            this.setState({
                profile: this_userprofile
            })

            DashboardStore._UpdateLoggedInUserProfile(this_userprofile);
        }

    }
    __editExperience = (item) => {

        let campany_name = item.campany_name;
        let start_date = item.start_date;
        let end_date = item.end_date;
        let id = item.id;
        let achievements = item.achievements;
        let title = item.title;

        DashboardStore._updateParameterV2(campany_name, "companyName", "SkillManagement");
        DashboardStore._updateParameterV2(title, "title", "SkillManagement");
        DashboardStore._updateParameterV2(start_date, "startdate", "SkillManagement");
        DashboardStore._updateParameterV2(end_date, "enddate", "SkillManagement");
        DashboardStore._updateParameterV2(achievements, "achievement", "SkillManagement");
        DashboardStore._updateParameterV2(id, "current_id", "SkillManagement");
        DashboardStore._updateParameterV2("Edit Experience", "HeaderText", "SkillManagement");

        this.props.navigation.navigate("EditExpirence");
    };

    __AddNewExpeince = () => {

        let campany_name = "";
        let start_date = "";
        let end_date = "";
        let id = "";
        let achievements = "";
        let title = "";

        DashboardStore._updateParameterV2(campany_name, "companyName", "SkillManagement");
        DashboardStore._updateParameterV2(title, "title", "SkillManagement");
        DashboardStore._updateParameterV2(start_date, "startdate", "SkillManagement");
        DashboardStore._updateParameterV2(end_date, "enddate", "SkillManagement");
        DashboardStore._updateParameterV2(achievements, "achievement", "SkillManagement");
        DashboardStore._updateParameterV2(id, "current_id", "SkillManagement");
        DashboardStore._updateParameterV2("Add New Experience", "HeaderText", "SkillManagement");

        this.props.navigation.navigate("EditExpirence");
    };


    __editEducation = (item) => {

        let school = item.school;
        let degree = item.degree;
        let field_of_study = item.field_of_study;
        let start_date = item.start_date;
        let end_date = item.end_date;
        let description = item.description;
        let id = item.id;

        DashboardStore._updateParameterV2(school, "school", "EducationManagement");
        DashboardStore._updateParameterV2(degree, "degree", "EducationManagement");
        DashboardStore._updateParameterV2(field_of_study, "field_of_study", "EducationManagement");
        DashboardStore._updateParameterV2(start_date, "start_date", "EducationManagement");

        DashboardStore._updateParameterV2(end_date, "end_date", "EducationManagement");
        DashboardStore._updateParameterV2(description, "description", "EducationManagement");
        DashboardStore._updateParameterV2(id, "current_id", "EducationManagement");
        DashboardStore._updateParameterV2("Edit Education", "HeaderText", "EducationManagement");

        this.props.navigation.navigate("EditEducation");

    };
    __AddEducation = () => {

        let school = "";
        let degree = "";
        let field_of_study = "";
        let start_date = "";
        let end_date = "";
        let description = "";
        let id = "";

        DashboardStore._updateParameterV2(school, "school", "EducationManagement");
        DashboardStore._updateParameterV2(degree, "degree", "EducationManagement");
        DashboardStore._updateParameterV2(field_of_study, "field_of_study", "EducationManagement");
        DashboardStore._updateParameterV2(start_date, "start_date", "EducationManagement");

        DashboardStore._updateParameterV2(end_date, "end_date", "EducationManagement");
        DashboardStore._updateParameterV2(description, "description", "EducationManagement");
        DashboardStore._updateParameterV2(id, "current_id", "EducationManagement");
        DashboardStore._updateParameterV2("Add Education", "HeaderText", "EducationManagement");

        this.props.navigation.navigate("EditEducation");
    };


    render() {
        const { profile } = this.state;
        const { WbLeague, WbLeaguePointAggregation } = DashboardStore;

        let Exper_ErrorComponentDisplay = <EmptyListComponent
            message={DashboardStore.ListMyExperience.ErrorMessage}
            onclickhandler={() => DashboardStore.__FetchAllMyExperience()}
        />
        let Educ_ErrorComponentDisplay = <EmptyListComponent
            message={DashboardStore.ListMyEducation.ErrorMessage}
            onclickhandler={() => DashboardStore.__FetchAllMyEducation()}
        />
        let Keywords_ErrorComponentDisplay = <EmptyListComponent
            message={DashboardStore.ListMyKeywords.ErrorMessage}
            onclickhandler={() => DashboardStore.__FetchAllMyKeywords()}
        />

        let MyExperience = DashboardStore.ListMyExperience.Myexperience.map((item, index) => {
            let EndingYear = '';
            let StartingYear = '';
            let start_date = item.start_date;
            let end_date = item.end_date;

            if (end_date == 'Present') {
                EndingYear = "Present";
            }
            else {
                let stringEnddate = ParseDate2string(end_date, "-");
                EndingYear = `${stringEnddate[1]} , ${stringEnddate[3]}`;

            }
            // var parts =thisdate.split('/');
            // var mydate = new Date(parts[2], parts[1] - 1, parts[0]); 
            // let thisdate__=mydate.toDateString();
            let stringStartdate = ParseDate2string(start_date, "-");
            console.log(stringStartdate)
            console.log("Experience")
            StartingYear = `${stringStartdate[1]} , ${stringStartdate[3]}`;
            // let datestring=GetPartDate(stringdate," ");






            return (
                <CardItem style={{ width: "100%", backgroundColor: "transparent", paddingTop: 0 }} key={index}>
                    <Left style={{ alignItems: 'flex-start' }}>
                        <Body style={styles.borderBtmsty2, { marginLeft: 0, borderBottomWidth: 1, borderColor: '#E5E5E5', paddingBottom: 5 }}>
                            <Text style={[styles.minBold3, { fontWeight: 'bold' }]}>{item.campany_name}</Text>

                            <Text note style={styles.minBold3}>{item.title}</Text>
                            <Text style={[styles.minBold3, { fontWeight: '300', marginTop: 3 }]}> {`${StartingYear} - ${EndingYear}`} </Text>
                            <Button style={[styles.editpen, { backgroundColor: 'transparent', elevation: 0, height: 30, padding: 10, bottom: 0 }]}
                                onPress={() => this.__editExperience(item)}
                            >
                                <Text style={[styles.editBtnTxt2,]}>
                                    Edit
                                        </Text>
                                {/* <Image source={Pen2} style={{ marginRight: 5, height: 13, width: 13 }} /> */}
                            </Button>
                        </Body>
                    </Left>
                </CardItem>
            )


        });
        let MyEducation = DashboardStore.ListMyEducation.MyEducation.map((item, index) => {
            let EndingYear = '';
            let StartingYear = '';
            let start_date = item.start_date;
            let end_date = item.end_date;

            if (end_date == 'Present') {
                EndingYear = "Present";
            }
            else {
                let stringEnddate = ParseDate2string(end_date, "-");
                EndingYear = `${stringEnddate[1]} , ${stringEnddate[3]}`;

            }
            // var parts =thisdate.split('/');
            // var mydate = new Date(parts[2], parts[1] - 1, parts[0]); 
            // let thisdate__=mydate.toDateString();
            let stringStartdate = ParseDate2string(start_date, "-");
            console.log(stringStartdate)
            StartingYear = `${stringStartdate[1]} , ${stringStartdate[3]}`;
            // let datestring=GetPartDate(stringdate," ");


            return (
                <CardItem style={{ width: "100%", backgroundColor: "transparent", paddingTop: 0 }} key={index}>
                    <Left style={{ alignItems: 'flex-start', borderBottomWidth: 1, borderColor: '#E5E5E5', paddingBottom: 5 }}>

                        <Body style={styles.borderBtmsty2, { marginLeft: 0, }}>
                            <Text style={[styles.minBold3, { fontWeight: 'bold' }]}>{item.school}</Text>

                            <Text note style={styles.minBold3}>{item.field_of_study}</Text>
                            <Text style={[styles.minBold3, { fontWeight: '300', marginTop: 3 }]}>{`${StartingYear} - ${EndingYear}  (${item.degree})`} </Text>
                            <Button style={[styles.editpen, { backgroundColor: 'transparent', elevation: 0, height: 30, padding: 10, bottom: 0 }]}
                                onPress={() => this.__editEducation(item)}
                            >
                                <Text style={[styles.editBtnTxt2,]}>
                                    Edit
                                        </Text>
                                {/* <Image source={Pen2} style={{ marginRight: 5, height: 13, width: 13 }} /> */}
                            </Button>
                        </Body>
                    </Left>
                </CardItem>
            )


        });
        let MyKeywords = DashboardStore.ListMyKeywords.Keywords.map((item, index) => {


            return (
                <Button style={styles.btnChecky} key={index}>
                    <Text style={styles.btnCheckyText}>{item}</Text>
                </Button>
            )


        });






        const { LoggedInUserProfile } = DashboardStore;
        let Fullname = '';
        if (LoggedInUserProfile.first_name == null || LoggedInUserProfile.last_name == null) {
            Fullname = `${LoggedInUserProfile.username} `;
        }
        else {
            Fullname = `${LoggedInUserProfile.first_name} ${LoggedInUserProfile.last_name} `

        }
        console.warn(LoggedInUserProfile);

        return (

            <Container style={styles.container}>
                <ScrollView>
                    <View style={styles.cardTopper}>
                        <View style={styles.cardContent1}>
                            <CardItem style={styles.ProfileHeader}>
                                <Button transparent onPress={() => { this.props.navigation.goBack() }}>
                                    <SvgUri
                                        // style={{ marginTop: 20, marginLeft: -17 }}
                                        width='11px'
                                        height='22px'
                                        uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1589394608/Mobile_Assets/arrbk_ipiscg.svg"
                                    />
                                    {/* <Image
                          source={WhiteArr}
                          style={{ height: 22, width: 11, }}
                        /> */}
                                </Button>
                                <View style={{display: "flex", alignItems: "center"}} >

                                    <TouchableOpacity onPress={() => { this.props.navigation.navigate("EditProfile_pic") }}>

                                        {/* <Image
                                            source={{ uri: 'https://res.cloudinary.com/workbrook-hash/image/upload/v1587164868/App/user-circle-03_k6j49i.png' }}
                                            style={{ height: 40.69, width: 40, borderRadius: 40 / 2 }}
                                        /> */}
                                        <Image
                                            source={{ uri: LoggedInUserProfile.image_url }}
                                            style={{ height: 40.69, width: 40, borderRadius: 40 / 2 }}
                                        />
                                        {/* <Thumbnail 
                                    large
                                    // source={{ uri: DashboardStore.profileupdatedata.image_url }} 
                                    source={{ uri: LoggedInUserProfile.image_url}} 
                                    style={{ height: 30, width: 30, borderRadius: 30 / 2}}
                                    /> */}
                                    </TouchableOpacity>
                                        <Text style={styles.nameSty}>{Fullname}</Text>
                                        <View style={styles.textIcon2}>
                                            <SvgUri
                                                style={{ marginRight: 5 }}
                                                width="9px"
                                                height="12px"
                                                uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593153738/Mobile_Assets/dropWht_u1yi2i.svg"
                                            />
                                            {/* <Image source={Drop} style={{ marginRight: 5 }} /> */}
                                            <Text note style={styles.nameSty2}>{LoggedInUserProfile.location}</Text>
                                        </View>

                                </View>
                                    <Button style={{ backgroundColor: 'transparent', elevation: 0, height: 30, padding: 10 }}
                                        onPress={() => { this.props.navigation.navigate("EditIntro") }}
                                    >
                                        <Text style={styles.editBtnTxt}>
                                            Edit
                                        </Text>
                                    </Button>
                            </CardItem>
                        </View>
                        <View style={styles.cardContent1}>
                            <CardItem style={{ width: "100%", backgroundColor: "transparent" }} button
                            >
                                <Left>
                                    <TouchableOpacity
                                        onPress={() => { this.props.navigation.navigate("RecruiterDashboard") }}

                                    >
                                        <Text style={styles.minBold} >Recruiter Dashboard</Text>

                                    </TouchableOpacity>
                                </Left>
                                <Right>
                                    <TouchableOpacity
                                        onPress={() => { this.props.navigation.navigate("LeagueParticipants") }}

                                    >
                                        <View style={styles.textIcon}>
                                            <SvgUri
                                                style={{ marginRight: 5 }}
                                                height="18px"
                                                width="16px"
                                                uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593739334/Mobile_Assets/cupWht_hx49pb.svg"
                                            />
                                            <Text note style={styles.nameSty2}> {WbLeaguePointAggregation.grandTotal}  pts</Text>
                                        </View>
                                    </TouchableOpacity>

                                </Right>
                            </CardItem>
                        </View>
                    </View>
                    <View style={[styles.devtrop,]}>
                        <CardItem style={{ width: "100%", backgroundColor: "transparent" }}>
                            <Left>
                                <Text style={styles.minBold2} >About</Text>
                            </Left>
                            <Right>
                                <Button style={{ backgroundColor: 'transparent', elevation: 0, height: 30, padding: 5 }}
                                    onPress={() => { this.props.navigation.navigate("EditAbout") }}
                                >
                                    <Text style={[styles.editBtnTxt2,]}>
                                        Edit
                                        </Text>
                                    {/* <Image source={Pen2} style={{ marginRight: 5 }} /> */}

                                </Button>
                            </Right>
                        </CardItem>
                        <CardItem style={{ width: "100%", backgroundColor: "transparent", paddingTop: 0 }}>
                            <Left style={styles.borderBtmsty1}>
                                <Text style={styles.minBold3} >
                                    {LoggedInUserProfile.about}
                                </Text>
                            </Left>
                        </CardItem>

                    </View>
                    <View style={styles.devtrop}>
                        <CardItem style={{ width: "100%", backgroundColor: "transparent" }}>
                            <Left>
                                <Text style={styles.minBold2} >Experience</Text>
                            </Left>
                            <Right>
                                <Button style={{ backgroundColor: 'transparent', elevation: 0, height: 20, padding: 5 }}

                                    onPress={this.__AddNewExpeince}
                                >
                                    <SvgUri
                                        width="16px"
                                        height="16px"
                                        uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593739979/Mobile_Assets/plusBlue_j0d0es.svg"
                                    />
                                    {/* <Image source={Plus} style={{ marginRight: 5 }} /> */}
                                </Button>
                            </Right>
                        </CardItem>
                        {DashboardStore.ListMyExperience.Myexperience.length > 0 ?
                            MyExperience
                            :
                            Exper_ErrorComponentDisplay
                        }

                        {/* <CardItem style={{ width: "100%", backgroundColor: "transparent", paddingTop: 0 }}>
                            <Left style={{ alignItems: 'flex-start' }}>
                                <Thumbnail small source={{ uri: 'https://res.cloudinary.com/custocrypt/image/upload/v1569083972/BgvBscXZLT035MdzpNpHLbZmyPg2of.jpg' }} />
                                <Body style={styles.borderBtmsty2, { marginLeft: 15, borderBottomWidth: 1, borderColor: '#E5E5E5', paddingBottom: 5 }}>
                                    <Text style={[styles.minBold3, { fontWeight: 'bold' }]}>Name of Company</Text>

                                    <Text note style={styles.minBold3}>Assistant CEO</Text>
                                    <Text style={[styles.minBold3, { fontWeight: '300', marginTop: 3 }]}> Jan 2017 - Present  | 3yrs +</Text>
                                    <Button style={[styles.editpen, { backgroundColor: 'transparent', elevation: 0, height: 20, padding: 10 }]}
                                    onPress={() => { this.props.navigation.navigate("EditExpirence") }}
                                    >
                                        <Image source={Pen2} style={{ marginRight: 5, height: 13, width: 13 }} />
                                    </Button>
                                </Body>
                            </Left>
                        </CardItem>
                        <CardItem style={{ width: "100%", backgroundColor: "transparent", paddingTop: 0 }}>
                            <Left style={{ alignItems: 'flex-start' }}>
                                <Thumbnail small source={{ uri: 'https://res.cloudinary.com/custocrypt/image/upload/v1569083972/BgvBscXZLT035MdzpNpHLbZmyPg2of.jpg' }} />
                                <Body style={styles.borderBtmsty2, { marginLeft: 15, borderBottomWidth: 1, borderColor: '#E5E5E5', paddingBottom: 5 }}>
                                    <Text style={[styles.minBold3, { fontWeight: 'bold' }]}>Name of Company</Text>

                                    <Text note style={styles.minBold3}>Assistant CEO</Text>
                                    <Text style={[styles.minBold3, { fontWeight: '300', marginTop: 3 }]}> Jan 2017 - Present  | 3yrs +</Text>
                                    <Button style={[styles.editpen, { backgroundColor: 'transparent', elevation: 0, height: 20, padding: 10 }]}
                                    onPress={() => { this.props.navigation.navigate("EditExpirence") }}
                                    
                                    >
                                        <Image source={Pen2} style={{ marginRight: 5, height: 13, width: 13 }} />
                                    </Button>
                                </Body>
                            </Left>
                        </CardItem> */}

                    </View>

                    <View style={styles.devtrop}>
                        <CardItem style={{ width: "100%", backgroundColor: "transparent" }}>
                            <Left>
                                <Text style={styles.minBold2} >Education</Text>
                            </Left>
                            <Right>
                                <Button style={{ backgroundColor: 'transparent', elevation: 0, height: 20, padding: 5 }}
                                    onPress={this.__AddEducation}
                                >
                                    <SvgUri

                                        width="16px"
                                        height="16px"
                                        uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593739979/Mobile_Assets/plusBlue_j0d0es.svg"
                                    />
                                    {/* <Image source={Plus} style={{ marginRight: 5 }} /> */}
                                </Button>
                            </Right>
                        </CardItem>
                        {DashboardStore.ListMyEducation.MyEducation.length > 0 ?
                            MyEducation
                            :
                            Educ_ErrorComponentDisplay
                        }

                        {/* <CardItem style={{ width: "100%", backgroundColor: "transparent", paddingTop: 0 }}>
                            <Left style={{ alignItems: 'flex-start', borderBottomWidth: 1, borderColor: '#E5E5E5', paddingBottom: 5 }}>
                                <Thumbnail small source={{ uri: 'https://res.cloudinary.com/custocrypt/image/upload/v1569083972/BgvBscXZLT035MdzpNpHLbZmyPg2of.jpg' }} />
                                <Body style={styles.borderBtmsty2, { marginLeft: 15, }}>
                                    <Text style={[styles.minBold3, { fontWeight: 'bold' }]}>Name of university</Text>

                                    <Text note style={styles.minBold3}>Name of university</Text>
                                    <Text style={[styles.minBold3, { fontWeight: '300', marginTop: 3 }]}> 2014 - 2020</Text>
                                    <Button style={[styles.editpen, { backgroundColor: 'transparent', elevation: 0, height: 20, padding: 10 }]}
                                    onPress={() => { this.props.navigation.navigate("EditEducation") }}
                                    >
                                        <Image source={Pen2} style={{ marginRight: 5, height: 13, width: 13 }} />
                                    </Button>
                                </Body>
                            </Left>
                        </CardItem> */}
                    </View>
                    <View style={styles.devtrop}>
                        <CardItem style={{ width: "100%", backgroundColor: "transparent" }}>
                            <Left>
                                <Text style={styles.minBold2} >Skills/Keywords</Text>
                            </Left>
                            <Right>
                                <Button style={{ backgroundColor: 'transparent', elevation: 0, height: 30, padding: 5 }}
                                    onPress={() => { this.props.navigation.navigate("EditSkills") }}
                                >
                                    <Text style={[styles.editBtnTxt2]}>
                                        Edit
                                        </Text>
                                </Button>
                            </Right>
                        </CardItem>
                        <CardItem style={{ width: "100%", backgroundColor: "transparent", paddingTop: 0, maxWidth: '100%' }}>
                            <Left style={{ alignItems: 'flex-start', borderBottomWidth: 1, borderColor: '#E5E5E5', paddingBottom: 15, flexWrap: 'wrap', width: '100%' }}>
                                {DashboardStore.ListMyKeywords.Keywords.length > 0 ?
                                    MyKeywords
                                    :
                                    Keywords_ErrorComponentDisplay
                                }
                                {/* <Button style={styles.btnChecky}>
                                    <Text style={styles.btnCheckyText}>Design</Text>
                                </Button>
                                <Button style={styles.btnChecky}>
                                    <Text style={styles.btnCheckyText}>Team Leadership</Text>
                                </Button>
                                <Button style={styles.btnChecky}>
                                    <Text style={styles.btnCheckyText}>UIUX</Text>
                                </Button>
                                <Button style={styles.btnChecky}>
                                    <Text style={styles.btnCheckyText}>Public Speaking</Text>
                                </Button> */}
                            </Left>
                        </CardItem>
                    </View>
                    <View style={styles.devtrop}>
                        <CardItem style={{ width: "100%", backgroundColor: "transparent" }}>
                            <Left>
                                <Text style={styles.minBold2} >Resume/Attached Documents</Text>
                            </Left>
                            <Right>
                                <Button style={{ backgroundColor: 'transparent', elevation: 0, height: 30, padding: 5 }}
                                    onPress={() => { this.props.navigation.navigate("AttachDocuments") }}
                                >
                                    <Text style={[styles.editBtnTxt2,]}>
                                        add
                                        </Text>
                                </Button>
                            </Right>
                        </CardItem>
                        {DashboardStore.profileupdatedata.resume != null ?
                            <CardItem style={{ width: "100%", backgroundColor: "transparent", paddingTop: 0 }}>
                                <Left style={{ alignItems: 'flex-start', borderBottomWidth: 1, borderColor: '#E5E5E5', paddingBottom: 5 }}>
                                    <Button style={[styles.btnChecky, { borderWidth: 0, paddingLeft: 0, paddingRight: 0 }]}
                                        onPress={() => Communicator('web', DashboardStore.profileupdatedata.resume)}

                                    >
                                        <Text style={styles.btnCheckyText}>{`${Fullname}'s Resume`}</Text>
                                    </Button>
                                </Left>
                            </CardItem>
                            :
                            <CardItem style={{ width: "100%", backgroundColor: "transparent", paddingTop: 0 }}>
                                <Left style={{ alignItems: 'flex-start', borderBottomWidth: 1, borderColor: '#E5E5E5', paddingBottom: 5 }}>
                                    <Button style={[styles.btnChecky, { borderWidth: 0, paddingLeft: 0, paddingRight: 0 }]}
                                        onPress={() => { this.props.navigation.navigate("AttachDocuments") }}

                                    >
                                        <Text style={[styles.btnCheckyText, { color: '#000' }]}>Upload Your Resume</Text>
                                    </Button>
                                </Left>
                            </CardItem>
                        }
                        {DashboardStore.profileupdatedata.links != null ?

                            <CardItem style={{ width: "100%", backgroundColor: "transparent", paddingTop: 0 }}>
                                <Left style={{ alignItems: 'flex-start', borderBottomWidth: 1, borderColor: '#E5E5E5', paddingBottom: 5 }}>
                                    <Button style={[styles.btnChecky, { borderWidth: 0, paddingLeft: 0, paddingRight: 0 }]}
                                        onPress={() => Communicator('web', DashboardStore.profileupdatedata.links.Facebook)}

                                    >
                                        <Text style={styles.btnCheckyText}>Facebook</Text>
                                        {/* <Text style={styles.btnCheckyText}>{JSON.stringify(DashboardStore.profileupdatedata)}</Text> */}
                                    </Button>
                                </Left>
                                <Left style={{ alignItems: 'flex-start', borderBottomWidth: 1, borderColor: '#E5E5E5', paddingBottom: 5 }}>
                                    <Button style={[styles.btnChecky, { borderWidth: 0, paddingLeft: 0, paddingRight: 0 }]}
                                        onPress={() => Communicator('web', DashboardStore.profileupdatedata.links.LinkedIn)}

                                    >
                                        <Text style={styles.btnCheckyText}>LinkedIn</Text>
                                    </Button>
                                </Left>
                                <Left style={{ alignItems: 'flex-start', borderBottomWidth: 1, borderColor: '#E5E5E5', paddingBottom: 5 }}>
                                    <Button style={[styles.btnChecky, { borderWidth: 0, paddingLeft: 0, paddingRight: 0 }]}
                                        onPress={() => Communicator('web', DashboardStore.profileupdatedata.links.Portfolio)}

                                    >
                                        <Text style={styles.btnCheckyText}>Portfolio</Text>
                                    </Button>
                                </Left>
                                <Left style={{ alignItems: 'flex-start', borderBottomWidth: 1, borderColor: '#E5E5E5', paddingBottom: 5 }}>
                                    <Button style={[styles.btnChecky, { borderWidth: 0, paddingLeft: 0, paddingRight: 0 }]}
                                        onPress={() => Communicator('web', DashboardStore.profileupdatedata.links.Twitter)}

                                    >
                                        <Text style={styles.btnCheckyText}>Twitter</Text>
                                    </Button>
                                </Left>
                            </CardItem>
                            :
                            null
                        }
                    </View>
                    {
                        LoggedInUserProfile.isRecruiter === 0 ?
                            <TouchableOpacity style={{ marginTop: 70, marginLeft: 25, marginBottom: 30 }}
                                onPress={() => { this.props.navigation.navigate("RecruiterForm") }}
                            >
                                <Text style={[styles.nameSty, { color: "#1E93E2" }]}> Become a recruiter </Text>
                            </TouchableOpacity>
                            :
                            null

                    }

                </ScrollView>
            </Container>


        );
    }
}
export default Landing;

const styles = StyleSheet.create({
    container: {
        backgroundColor: "#FAFAFA",
        display: "flex",
        flex: 1,
        justifyContent: "flex-start",
    },
    ProfileHeader:{ 
        width: "100%", 
        backgroundColor: "transparent",
        display: 'flex',
        justifyContent: "space-between", 
    },
    devtrop: {
        display: "flex",
        justifyContent: "flex-start",
    },
    cardTopper: {
        height: 157,
        backgroundColor: "#1E93E2",
        display: "flex",
        width: '100%',
        justifyContent: "space-between",
        paddingTop: 5,
        paddingBottom: 10,
    },
    cardContent1: {
        display: "flex",
        flexDirection: "row",
        width: '100%',
    },
    nameSty: {
        fontSize: 16,
        fontWeight: "600",
        color: '#fff'
    },
    nameSty2: {
        fontSize: 12,
        color: '#fff'
    },
    editBtnTxt: {
        fontSize: 12,
        color: '#fff',
        fontWeight: "600"
    },
    editBtnTxt2: {
        fontSize: 12,
        color: '#1E93E2',
        fontWeight: "600"
    },
    textIcon: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center'
    },
    textIcon2: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 5,
    },
    minBold: {
        fontSize: 10,
        fontWeight: "bold",
        color: '#fff'
    },
    minBold2: {
        fontSize: 14,
        fontWeight: "500",
        color: '#2F2F2F'
    },
    minBold3: {
        fontSize: 11,
        color: '#2F2F2F'
    },
    borderBtmsty1: {
        borderBottomWidth: 1,
        borderBottomColor: "#E5E5E5",
        paddingBottom: 5
    },
    borderBtmsty2: {
        borderBottomWidth: 1,
        borderBottomColor: "#E5E5E5",
        paddingBottom: 5
    },
    btnChecky: {
        height: 28,
        borderWidth: 1,
        borderColor: '#1E93E2',
        borderRadius: 5,
        backgroundColor: "transparent",
        elevation: 0,
        paddingLeft: 28,
        paddingRight: 28,
        marginRight: 5,
        marginBottom: 5
    },
    btnCheckyText: {
        fontSize: 11,
        fontWeight: 'bold',
        color: '#1E93E2'
    },
    editpen: {
        position: 'absolute',
        right: 0

    },

});

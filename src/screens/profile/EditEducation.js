import React, { Component } from 'react';
import {
  Text,
  Image,
  StyleSheet,
  ImageBackground,
  Keyboard,
  TouchableOpacity,Alert
} from 'react-native';
import { KeyboardAvoidingView } from 'react-native';
import CustomizeHeader from '../frags/CustomizeHeader';

import Ionicons from 'react-native-vector-icons/Ionicons';
import {
  Container,
  Header,
  Content,
  Textarea,
  Button,
  View,
  Thumbnail,
  Form,
  Item,
  Input,
  Label,
  Badge,
  Icon,
  Footer,
  Picker,
  Left,
  Right,
  Body,
  Title,
  Center,
} from 'native-base';
import {
  Kaede,
  Hoshi,
  Jiro,
  Isao,
  Madoka,
  Akira,
  Hideo,
  Kohana,
  Makiko,
  Sae,
  Fumi,
} from 'react-native-textinput-effects';

import { myspiner } from '../../dependency/UtilityFunctions';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Entypo from 'react-native-vector-icons/Entypo';
import User from "../../assets/workbrookAssets/user4.png";

import accountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import { ScrollView } from 'react-native-gesture-handler';
import DatePicker from 'react-native-datepicker'
import DashboardStore from '../../stores/Dashboard';


@observer
class EditEducation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      errorloading: false,
      date: "2016-05-15",

    }
    this._LoginHandler = this._LoginHandler.bind(this);
    this.DeleteEducation_input = this.DeleteEducation_input.bind(this);
  }
  async  _LoginHandler() {
    const { email, password } = this.state;
    let data = {
      email,
      password
    }
    // let    result = await accountStore._LoginUserNowtest(data);
    let result = await accountStore._LoginUserNow(data);
    let re = accountStore.redirectmode;

    // console.warn(re)
    if (re === true) {
      Toast.show('Login Successfully', Toast.SHORT, Toast.BOTTOM, ToastStyle);

      this.props.navigation.navigate("SignedIn");
    }
    else {
      // console.warn('fuckkkkkk')
    }

  }
  _calculateWordCount() {
    let Total=100;
    let counter= Total - DashboardStore.EducationManagement.description.length;
    if(counter <0){
      return 0;
    }
    else{
      return counter;
    }


  }
  componentDidMount() {
    DashboardStore.UpdateNavigationState(this.props.navigation);
 
  }
  proceednow=() =>{
    DashboardStore.__processEducationManagement();

   
      }
  DeleteEducation_input=() =>{
 
    Alert.alert(
      'Delete Education ?',
      `Are you sure you want to delete ?`,
      [
           {
              text: 'No', onPress: () => { },//Do nothing
              style: 'cancel'
          },
          {
              text: 'Yes', onPress: () => {
                DashboardStore.__deleteEducationManagement();

              }
          },
      ],
      { cancelable: true }
  );
   
       }
  render() {
    const {EducationManagement}=DashboardStore
    // let EducationManagement = DashboardStore.EducationManagement

    return (
      <Container>
         {  myspiner(DashboardStore.isProcessing)}
        <ScrollView>
          <CustomizeHeader
            leftside='cancel'
            title={EducationManagement.HeaderText}
            rightside='save'
            statedColor='#1E93E2'
            icon='md-close'
            savehandler={this.proceednow }
          />
          <View style={styles.profileWrap}>
            <View style={{ width: "100%", padding: 15 }}>
              <Item  stackedLabel style={[styles.bottomIndBorder, { height: 55 }]}>
                <Label style={styles.labelSty}>School/University <Text style={styles.asteriskSty}>*</Text></Label>
                <Input 
                style={styles.profileinputText} 
                placeholderTextColor={'#A7A7A7'} 
                placeholder="School/University" 
                defaultValue={DashboardStore.EducationManagement.school}
                onChangeText={(school) => DashboardStore.onChangeText('school', school,'EducationManagement')}
               
                />
              </Item>
              <Item  stackedLabel style={[styles.bottomIndBorder, { height: 55 }]}>
                <Label style={styles.labelSty}>Degree <Text style={styles.asteriskSty}>*</Text></Label>
                <Input  
                style={styles.profileinputText}
                 placeholderTextColor={'#A7A7A7'}
                 placeholder="Degree" 
                 defaultValue={DashboardStore.EducationManagement.degree}
                 onChangeText={(degree) => DashboardStore.onChangeText('degree', degree,'EducationManagement')}
                
                 />
              </Item>
              <Item  stackedLabel style={[styles.bottomIndBorder, { height: 55 }]}>
                <Label style={styles.labelSty}>Field of study <Text style={styles.asteriskSty}>*</Text></Label>
                <Input 
                style={styles.profileinputText} 
                placeholderTextColor={'#A7A7A7'}
                 placeholder="Field of study" 
                 defaultValue={DashboardStore.EducationManagement.field_of_study}
                 onChangeText={(field_of_study) => DashboardStore.onChangeText('field_of_study', field_of_study,'EducationManagement')}
                
                 />
              </Item>
              <View style={[styles.cardContent1, {marginBottom: 30, marginTop: 30, paddingLeft:0, paddingRight: 0}]}>
                <Left>
                  <View stackedLabel style={[, { height: 55 }]}>
              <Label style={styles.labelSty}>select Start date <Text style={styles.asteriskSty}>*</Text></Label>
                  <DatePicker
                    style={{ width: 150, display: 'flex',flexDirection: 'column', justifyContent:'flex-start', alignItems: 'center', }}
                    date={DashboardStore.EducationManagement.start_date}
                    mode="date"
                    placeholder="select date"
                    format="YYYY-MM-DD"
                    // minDate="2016-05-01"
                    // maxDate="2016-06-01"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    showIcon={false}
                    customStyles={{
                      dateInput: {
                        marginLeft: 5,
                        borderTopWidth: 0,
                        borderLeftWidth: 0,
                        borderRightWidth: 0,
                        borderBottomColor: '#E5E5E5',
                        display: 'flex',flexDirection: 'row', justifyContent:'flex-start', alignItems: 'center' ,
                        height: 70,

                      },
                      dateText: {
                        fontSize: 13, fontWeight: '500', textAlign: 'left', color: '#2F2F2F',
                      },
                      // ... You can check the source to find the other keys.
                    }}
                    onDateChange={(date) => { 

                      this.setState({ date: date }) 
                      DashboardStore._updateParameterV2(date,"start_date","EducationManagement");

                    
                    }}
                  />
                  </View>
                </Left>
                <Right style={{paddingLeft: 0}}><View stackedLabel style={[, { height: 55 }]}>
              <Label style={styles.labelSty}>select Start date <Text style={styles.asteriskSty}>*</Text></Label>
                <DatePicker
                    style={{ width: 150, textAlign: 'left',}}
                    date={DashboardStore.EducationManagement.end_date}
                    mode="date"
                    placeholder="select date"
                    format="YYYY-MM-DD"
                    // minDate="2016-05-01"
                    // maxDate="2016-06-01"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    showIcon={false}
                    customStyles={{
                      dateInput: {
                        marginLeft: 0,
                        borderTopWidth: 0,
                        borderLeftWidth: 0,
                        borderRightWidth: 0,
                        borderBottomColor: '#E5E5E5',
                        display: 'flex',flexDirection: 'row', justifyContent:'flex-start', alignItems: 'center' ,
                        height: 70,
                      },
                      dateText: {
                        fontSize: 13, fontWeight: '500', textAlign: 'left', color: '#2F2F2F'
                      },
                      // ... You can check the source to find the other keys.
                    }}
                    onDateChange={(date) => { 
                      this.setState({ date: date })
                      DashboardStore._updateParameterV2(date,"end_date","EducationManagement");

                    }}
                  />
                  </View>
                </Right>
              </View>
              <View style={styles.cardContent1}>
                <Left>
                  <Text style={styles.minBold2} >Additional information (100 words)</Text>
                </Left>
                <Right>
                  <View style={styles.textIcon}>
                    {this._calculateWordCount() >30 ?

<View style={[styles.iconesty, { borderColor: '#5ABC7A', }]}>
                    <Text note style={[styles.iconestyText, { color: '#5ABC7A' }]}>{this._calculateWordCount()}</Text>
</View>
                    :
                    this._calculateWordCount() <=30 ?
                    <View style={[styles.iconesty, { borderColor: '#FC9F13', }]}>
                    <Text note style={[styles.iconestyText, { color: '#FC9F13' }]}>{this._calculateWordCount()}</Text>
                  </View>
:
this._calculateWordCount() <=10 ?
<View style={[styles.iconesty, { borderColor: '#FF5964', }]}>
<Text note style={[styles.iconestyText]}>{this._calculateWordCount()}</Text>
</View>
:
null
                    }
                  
                  
                  
                  </View>
                </Right>
              </View>

              <Textarea 
              rowSpan={5}
               style={styles.bordedTextArea}
                placeholder=""
                defaultValue={DashboardStore.EducationManagement.description}
                onChangeText={(description) => DashboardStore.onChangeText('description', description,'EducationManagement')}
               
                />
                {
                  EducationManagement.current_id == '' ?
                  null
                  :
                  <TouchableOpacity style={styles.cardContent2}
                  onPress={this.DeleteEducation_input}
                  >
                    
                  <Text style={styles.deleteAcct}>
                    Delete
                        </Text>
  
                </TouchableOpacity>
                }
           
                
              {/* <Text style={styles.inputError}>Please enter your summary</Text> */}
            </View>

          </View>
        </ScrollView>
      </Container>
    );
  }
}
export default EditEducation;

const inputStyles = {
  size: 100,
  color: '#FFFFFF',
};

const height = 40;

const styles = StyleSheet.create({
  profileWrap: {
    flex: 1,
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: "#fff"
  },
  cardContent2: {
    flex: 1,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'flex-end',
    backgroundColor: "#fff",
    marginTop: 40,
    textAlign: 'right',
    marginBottom: 20,
  },
  deleteAcct: {
    color: '#FF5964',
    fontWeight: '600',
    fontSize: 14
  },
  profileWrapinner: {
    flex: 1,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 25
  },
  profileinput: {
    marginLeft: 0,
    borderBottomColor: '#E5E5E5',
    height: 70,
    // marginBottom: 15
  },
  profileinputText: {
    height: "100%",
    color: "#2F2F2F",
    fontSize: 13,
    fontWeight: '500'
  },
  editpenWap: {
    backgroundColor: '#fff',
    height: 40,
    width: 40,
    borderRadius: 20,
    display: 'flex',
    justifyContent: "center",
    alignItems: "center",
    zIndex: 99,
    marginTop: -30,
    marginLeft: 50,
    marginRight: -50
  },
  inputError: {
    color: '#FF5964',
    fontWeight: '300',
    fontSize: 11
  },
  minBold2: {
    fontSize: 13,
    fontWeight: "500",
    color: '#2F2F2F'
  },
  cardContent1: {
    display: "flex",
    flexDirection: "row",
    width: '100%',
    paddingLeft: 5,
    paddingRight: 5,
  },
  textIcon: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center'
  },
  iconesty: {
    height: 23,
    width: 23,
    borderRadius: 23 / 2,
    borderWidth: 2,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 0,
    marginLeft: 3
  },
  iconestyText: {
    color: "#FF5964",
    fontSize: 9,
    fontWeight: '500'
  },
  bordedTextArea: {
    borderBottomColor: "#E5E5E5",
    borderBottomWidth: 1
  },
  labelSty: {
    color: "#2F2F2F",
    fontSize: 11,
    fontWeight: '500'
  },
  asteriskSty: {
    color: "#FF5964",
    fontSize: 10,
  },
  bottomIndBorder: {
    // marginLeft: 33,
    // marginRight: 33,
    borderBottomWidth: 1,
    borderColor: '#E5E5E5',
    height: 70,
    marginTop: 15,
},


});

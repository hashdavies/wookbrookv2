import React, { Component } from 'react';
import {
  Text,
  Image,
  StyleSheet,
  ImageBackground,
  Keyboard,
  TouchableOpacity, Platform, Alert
} from 'react-native';
import { KeyboardAvoidingView } from 'react-native';
import CustomizeHeader from '../frags/CustomizeHeader';

import Ionicons from 'react-native-vector-icons/Ionicons';
import {
  Container,
  Header,
  Content,
  Textarea,
  Button,
  View,
  Thumbnail,
  Form,
  Item,
  Input,
  Label,
  Badge,
  Icon,
  Footer,
  Picker,
  Left,
  Right,
  Body,
  Title,
  Center,
  CardItem
} from 'native-base';
import {
  Kaede,
  Hoshi,
  Jiro,
  Isao,
  Madoka,
  Akira,
  Hideo,
  Kohana,
  Makiko,
  Sae,
  Fumi,
} from 'react-native-textinput-effects';
import Modal from 'react-native-modal';
import DocumentPicker from 'react-native-document-picker';
import RNFetchBlob from 'react-native-fetch-blob'
import RNFS from 'react-native-fs'

import { myspiner, YOUR_CLOUDINARY_NAME, YOUR_CLOUDINARY_PRESET, ShowNotification } from '../../dependency/UtilityFunctions';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import User from "../../assets/workbrookAssets/user4.png";
import CloseModal from "../../assets/workbrookAssets/darkCross.png";

import accountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import { ScrollView } from 'react-native-gesture-handler';
import DatePicker from 'react-native-datepicker'
import DashboardStore from '../../stores/Dashboard';
import { SvgUri } from 'react-native-svg';


@observer
class AttachDocuments extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      errorloading: false,
      date: "2016-05-15",
      isModalVisible: false,
      isfileSelected: false,
      documentName: '',
      selectedDoc: {},

    }
    this._LoginHandler = this._LoginHandler.bind(this);
    this._DocumentPicker = this._DocumentPicker.bind(this);
    this.__SaveLinksNow = this.__SaveLinksNow.bind(this);
  }
  async  _LoginHandler() {
    const { email, password } = this.state;
    let data = {
      email,
      password
    }
    // let    result = await accountStore._LoginUserNowtest(data);
    let result = await accountStore._LoginUserNow(data);
    let re = accountStore.redirectmode;

    // console.warn(re)
    if (re === true) {
      Toast.show('Login Successfully', Toast.SHORT, Toast.BOTTOM, ToastStyle);

      this.props.navigation.navigate("SignedIn");
    }
    else {
    }

  }
  toggleModal = () => {
    DashboardStore._ToggleModalVisible(true)
    // this.setState({ isModalVisible: !this.state.isModalVisible });
  };
  toggleModalClose = () => {
    DashboardStore._ToggleModalVisible(false)
    // this.setState({ isModalVisible: !this.state.isModalVisible });
  };

  __SaveLinksNow = () => {

    // this.setState({ 

    //   isModalVisible: !this.state.isModalVisible
    //  });
    let Facebook = DashboardStore.SocialLinks.Facebook;
    let LinkedIn = DashboardStore.SocialLinks.LinkedIn
    let Portfolio = DashboardStore.SocialLinks.Portfolio;
    let Twitter = DashboardStore.SocialLinks.Twitter
    //  if(Facebook !==''){
    //   ShowNotification('Enter a valic Facebook profile link', true);

    //  }
    console.log(DashboardStore.SocialLinks)
    //  console.log(DashboardStore.profileupdatedata.links)
    let links = DashboardStore.SocialLinks;
    DashboardStore._updateParameterV2(links, "links", "profileupdatedata");
    console.log(DashboardStore.profileupdatedata)
    DashboardStore.__UpdateProfile();
    DashboardStore._ToggleModalVisible(false)

  };
  _DocumentPicker = async () => {
    try {
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.pdf, DocumentPicker.types.plainText],
      });
      console.log(
        res.uri,
        res.type, // mime type
        res.name,
        res.size
      );
      const source = {
        uri: res.uri,
        type: res.type,
        name: res.name,
      }
      this.setState({
        documentName: res.name,
        isfileSelected: true,
        selectedDoc: source
      })


      // setTimeout(() => {
      //   // this._uploadFileNow(res);
      //   this.cloudinaryUpload(source)

      // }, 1000);

    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        // throw err;
        ShowNotification('An error occured', true);
        console.log(err)
      }
    }
  };
  cloudinaryUpload = () => {
    const { selectedDoc } = this.state;
    // let file=
    DashboardStore._ToggleProcessing(true)
    console.log("here man")
    ShowNotification('Uploading your file...', true);

    const data = new FormData()
    data.append('file', selectedDoc)
    data.append('upload_preset', 'qfhydrgx')
    data.append("cloud_name", "workbrook-hash")
    fetch("https://api.cloudinary.com/v1_1/workbrook-hash/upload", {
      // https://api.cloudinary.com/v1_1/workbrook-hash/image/upload
      method: "post",
      body: data
    }).then(res => res.json()).
      then(data => {
        console.log("<>>><<")
        console.log(data)
        let DocUrl = data.secure_url;
        DashboardStore._updateParameterV2(DocUrl, "resume", "profileupdatedata");
        ShowNotification('Saving to database....', true);
        DashboardStore.__UpdateProfile();
        // this.SavetoDatabase(result)
        // setPhoto(data.secure_url)



      }).catch(err => {
        console.log(err)
        Alert.alert("An Error Occured While Uploading")
      })
  }
  proceednow = () => {
    const { isImagefromdevice, avatarSource, loader_visible, selectedDoc, isfileSelected } = this.state;

    if (isfileSelected === false) {
      ShowNotification('Select file before continue', true);
    }

    else {
      DashboardStore._ToggleProcessing(true)
      this.cloudinaryUpload()
      // DashboardStore.__UpdateProfile();
    }
  }
  componentDidMount() {
    DashboardStore.UpdateNavigationState(this.props.navigation);
    // alert(JSON.stringify(DashboardStore.profileupdatedata))
    if (DashboardStore.profileupdatedata.links != null) {
      // alert("pooo")
      let socialObject = DashboardStore.profileupdatedata.links;
      // "Facebook" in socialObject ? alert("yess"):alert("noo")
      let Facebook = "Facebook" in socialObject ? DashboardStore.profileupdatedata.links.Facebook : ' ';
      let LinkedIn = "LinkedIn" in socialObject ? DashboardStore.profileupdatedata.links.LinkedIn : ' ';
      let Portfolio = "Portfolio" in socialObject ? DashboardStore.profileupdatedata.links.Portfolio : ' ';
      let Twitter = "Twitter" in socialObject ? DashboardStore.profileupdatedata.links.Twitter : ' ';
      // let LinkedIn=DashboardStore.profileupdatedata.links.LinkedIn;
      // let Portfolio=DashboardStore.profileupdatedata.links.Portfolio;
      // let Twitter=DashboardStore.profileupdatedata.links.Twitter;
      // alert(facebook)
      DashboardStore._updateParameterV2(Facebook, "Facebook", "SocialLinks");
      DashboardStore._updateParameterV2(LinkedIn, "LinkedIn", "SocialLinks");
      DashboardStore._updateParameterV2(Portfolio, "Portfolio", "SocialLinks");
      DashboardStore._updateParameterV2(Twitter, "Twitter", "SocialLinks");
    }
    else {
      let Facebook = "";
      let LinkedIn = "";
      let Portfolio = "";
      let Twitter = "";
      // alert(facebook)
      DashboardStore._updateParameterV2(Facebook, "Facebook", "SocialLinks");
      DashboardStore._updateParameterV2(LinkedIn, "LinkedIn", "SocialLinks");
      DashboardStore._updateParameterV2(Portfolio, "Portfolio", "SocialLinks");
      DashboardStore._updateParameterV2(Twitter, "Twitter", "SocialLinks");
    }


  }

  // _uploadFileNow(file2upload) {
  //   // this.setState({
  //   //   loader_visible: true
  //   // });
  //   ShowNotification('Uploading your file...', true);
  //   // let file2upload=this.state.imagetoUpload;
  //   // console.warn(file2upload)
  //   // let source = { uri: file2upload.uri };
  //   // this.setState({
  //   //     uploadingImg: true
  //   // });
  //   uploadFile(file2upload)
  //       .then(response => response.json())
  //       .then(result => {
  //         console.log(result)
  //         // let imageurl=result.secure_url;
  //         // DashboardStore._updateParameterV2(imageurl,"image_url","profileupdatedata");
  //         // ShowNotification('Saving to database....', true);
  //         //  DashboardStore.__UpdateProfile();
  //         // this.SavetoDatabase(result)
  //           // this.setState({
  //           //     avatarSource: { uri: result.secure_url },
  //           //     loader_visible: false
  //           // });
  //       })

  // }
  //   isValidHttpUrl=(string) =>{
  //   let url;

  //   try {
  //     url = new URL(string);
  //   } catch (_) {
  //     return false;  
  //   }

  //   return url.protocol === "http:" || url.protocol === "https:";
  // }





  render() {
    return (
      <Container>
        {myspiner(DashboardStore.isProcessing)}

        <ScrollView>
          <CustomizeHeader
            leftside='cancel'
            title='Attach Documents'
            rightside='save'
            statedColor='#1E93E2'
            icon='md-close'
            savehandler={this.proceednow}
          />
          <View style={styles.profileWrap}>

            <View style={styles.devtrop}>
              <CardItem style={{ width: "100%", backgroundColor: "transparent", paddingTop: 15, }}>
                <Left>
                  <Text style={styles.minBold2} >Resume/Related documents</Text>
                </Left>
              </CardItem>
              <CardItem style={{ width: "100%", backgroundColor: "transparent", paddingTop: 0, }}>
                <Left>
                  <Text style={styles.SkillsminBold2} >
                    Add files or links to external documents, photos, sites, videos and presentations. Note that this will replace the existing resume.
                  </Text>
                </Left>
              </CardItem>
              <CardItem style={{ width: "100%", backgroundColor: "transparent", paddingTop: 0, }}>
                <Left>
                  <Text style={[styles.SkillsminBold2, { color: 'green', fontSize: 15 }]} >
                    {this.state.documentName}
                  </Text>
                </Left>
              </CardItem>
              <CardItem style={{ width: "100%", backgroundColor: "transparent", paddingTop: 0, maxWidth: '100%' }}>
                <Left style={{ alignItems: 'flex-start', borderTopWidth: 1, borderColor: '#E5E5E5', paddingTop: 15, width: '100%', display: 'flex', flexDirection: 'row', justifyContent: 'space-between', marginTop: 30 }}>
                  <Button style={[styles.btnFile, { backgroundColor: '#1E93E2' }]}
                    onPress={this._DocumentPicker}
                  >
                    <Text style={[styles.btnCheckyText, { color: '#fff' }]}>Browse</Text>
                  </Button>
                  <Button onPress={this.toggleModal} style={[styles.btnFile, { backgroundColor: '#fff' }]}>
                    <Text style={[styles.btnCheckyText, { color: '#1E93E2' }]}>Link</Text>
                  </Button>
                </Left>
              </CardItem>
            </View>


          </View>
        </ScrollView>
        <Modal style={styles.MContainer} isVisible={DashboardStore.isModalVisible} >
          <View style={styles.Mwrapper}>
            <View>
              <Button onPress={this.toggleModalClose} style={{ backgroundColor: "transparent", position: 'absolute', right:0, top: -20, padding: 10, elevation: 0 }}>
                {/* <Image source={CloseModal} style={{ width: 11, height: 10, }} /> */}
                <SvgUri 
                width= "13px"
                height="12px"
                  uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593153205/Mobile_Assets/closeBlk_elvoro.svg"
                />
                
                </Button>
              <Item style={[styles.profileinput,]}>
                <Input
                  style={styles.profileinputText}
                  placeholderTextColor={'#A7A7A7'}
                  placeholder="LinkedIn URL"
                  defaultValue={DashboardStore.SocialLinks.LinkedIn}
                  onChangeText={(LinkedIn) => DashboardStore.onChangeText('LinkedIn', LinkedIn, 'SocialLinks')}

                />
              </Item>
              <Item style={[styles.profileinput,]}>
                <Input
                  style={styles.profileinputText}
                  placeholderTextColor={'#A7A7A7'}
                  placeholder="Portfolio URL"
                  defaultValue={DashboardStore.SocialLinks.Portfolio}
                  onChangeText={(Portfolio) => DashboardStore.onChangeText('Portfolio', Portfolio, 'SocialLinks')}

                />
              </Item>
              <Item style={[styles.profileinput,]}>
                <Input
                  style={styles.profileinputText}
                  placeholderTextColor={'#A7A7A7'}
                  placeholder="Facebook URL"
                  defaultValue={DashboardStore.SocialLinks.Facebook}
                  onChangeText={(Facebook) => DashboardStore.onChangeText('Facebook', Facebook, 'SocialLinks')}

                />
              </Item>
              <Item style={[styles.profileinput,]}>
                <Input
                  style={styles.profileinputText}
                  placeholderTextColor={'#A7A7A7'}
                  placeholder="Twitter URL"
                  defaultValue={DashboardStore.SocialLinks.Twitter}
                  onChangeText={(Twitter) => DashboardStore.onChangeText('Twitter', Twitter, 'SocialLinks')}

                />
              </Item>
            </View>
            <Button full style={[styles.btnSave, { marginTop: 15 }]} title="Hide modal" onPress={this.__SaveLinksNow}>
              <Text style={[styles.btnCheckyText, { color: '#fff' }]}>{DashboardStore.isProcessing === true ? 'Processing' : 'Save'}</Text>
            </Button>
          </View>
        </Modal>
      </Container>
    );
  }
}
export default AttachDocuments;
function isValidHttpUrl(string) {
  let url;

  try {
    url = new URL(string);
  } catch (_) {
    return false;
  }

  return url.protocol === "http:" || url.protocol === "https:";
}
function isURL(str) {
  var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
    '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.?)+[a-z]{2,}|' + // domain name
    '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
    '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
    '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
    '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
  let result = pattern.test(str);
  console.log(result)
  return result;
}
function uploadFile(file) {

  // console.warn(file)
  let fileDir = file.uri;
  const split = fileDir.split('/');
  const name = split.pop();
  const inbox = split.pop();
  const realPath = `${RNFS.TemporaryDirectoryPath}${inbox}/${name}`;

  // const cleanFilePath = Platform.OS === 'ios' ? fileDir.replace('file://', '') :fileDir;
  const cleanFilePath = Platform.OS === 'ios' ? fileDir.replace('file://', '') : fileDir.replace('content://', '');


  return RNFetchBlob.fetch('POST', 'https://api.cloudinary.com/v1_1/' + YOUR_CLOUDINARY_NAME() + '/image/upload?upload_preset=' + YOUR_CLOUDINARY_PRESET(), {
    'Content-Type': 'multipart/form-data'
  }, [
    // { name: 'file', filename: file.fileName, data: RNFetchBlob.wrap(cleanFilePath) }
    { name: 'file', filename: file.fileName, data: realPath }
  ])
}
const inputStyles = {
  size: 100,
  color: '#FFFFFF',
};

const height = 40;

const styles = StyleSheet.create({
  profileWrap: {
    flex: 1,
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: "#fff"
  },
  profileWrapinner: {
    flex: 1,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 25
  },
  profileinput: {
    marginLeft: 0,
    borderWidth: 1,
    borderColor: '#E5E5E5',
    height: 55,
    // marginBottom: 15
  },
  profileinputText: {
    height: "100%",
    color: "#2F2F2F",
    fontSize: 14,
    fontWeight: '500',
    marginBottom: -15
  },
  btnFile: {
    height: 52,
    width: "45%",
    elevation: 0,
    paddingLeft: 18,
    paddingRight: 18,
    borderWidth: 1,
    borderColor: '#1E93E2',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  SurgestKills: {
    height: 28,
    borderWidth: 1,
    borderColor: '#B4B4B4',
    borderRadius: 5,
    backgroundColor: "#B4B4B4",
    elevation: 0,
    paddingLeft: 18,
    paddingRight: 18,
    marginRight: 5,
    marginBottom: 5,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  SurgestKillsText: {
    fontSize: 11,
    fontWeight: 'bold',
    color: '#fff'

  },
  btnCheckyText: {
    fontSize: 18,
    fontWeight: '500',
  },
  SurgestKillsIcon: {
    fontSize: 11,
    fontWeight: 'bold',
    color: '#fff',
    marginLeft: 8

  },
  iconsizeNcolour: {
    fontSize: 11,
    fontWeight: 'bold',
    color: '#1E93E2',
    marginLeft: 8

  },
  SkillsminBold2: {
    fontSize: 11,
    color: '#2F2F2F'
  },
  minBold2: {
    fontSize: 13,
    fontWeight: "500",
    color: '#2F2F2F'
  },
  devtrop: {
    display: "flex",
    justifyContent: "flex-start",
  },
  // Modal style =============================================================
  MContainer: {
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
  },
  Mwrapper: {
    height: 360,
    width: '85%',
    backgroundColor: '#fff',
    padding: 15,
    display: 'flex',
    justifyContent: 'space-around',
    // alignItems: 'center'
  },
  btnSave: {
    height: 52,
    backgroundColor: '#1E93E2',
    elevation: 0,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  // Modal Style ends=========================================================

});

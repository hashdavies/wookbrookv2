import React, { Component } from 'react';
import {
  StyleSheet,
  ImageBackground,
  Keyboard,
  TouchableOpacity,
  ScrollView,
  Image,Platform,
  Dimensions,
} from 'react-native';
import axios from 'axios';
import qs from 'qs';

// Tab Dependency start
// import { TabView, SceneMap } from 'react-native-tab-view';
 
// import { TabViewAnimated, TabBar, SceneMap } from 'react-native-tab-view';
// import type {Route, NavigationState } from 'react-native-tab-view/types';
 
// Tab Dependency end

 import SavedArticle from './SavedArticle';
import Tours from './Tours';
import Marked from './Marked';
 
 
import {
  TabView,
  TabBar,
  SceneMap,
  type NavigationState,
} from 'react-native-tab-view';
 






import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";

import { InteractionManager } from 'react-native';
import Entypo from 'react-native-vector-icons/Entypo';
import Feather from 'react-native-vector-icons/Feather';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import * as Animatable from 'react-native-animatable';
import CustomizeHeader from '../frags/CustomizeHeader';
import RNFetchBlob from 'react-native-fetch-blob'
 import ImagePicker from 'react-native-image-picker';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { myspiner,YOUR_CLOUDINARY_PRESET,YOUR_CLOUDINARY_NAME,requestCameraPermission,RefreshProfile} from '../../dependency/UtilityFunctions'; 


import {
  Container,
  Text,
  Header,
  Content,
  Textarea,
  Button,
  View,
  Thumbnail,
  Form,
  Item,
  Input,
  Label,
  Badge,
  Icon,
  Footer,
  FooterTab,
  Picker,
  Item as FormItem,
} from 'native-base';

 
 
const initialLayout = {
  height: 0,
  width: Dimensions.get('window').width,
}; 
type State = NavigationState<{
  key: string,
  title: string,
}>;

import { CustomAsset } from '../../../utils/assets';

class Myprofile extends Component<*, State> {
  static title = 'Scrollable top bar';
  static backgroundColor = '#3f51b5';
  static appbarElevation = 0;

  constructor(props) {
    super(props);
    this.state = { 
      fullname: 'i',
      avatarSource: null,
      cover_pix: null,
      isImagefromdevice:false,
      loader_visible: false,
      showsaveBtn: false,
      imagetoUpload: null,
      index: 1,
      routes: [
        { key: 'Tours', title: 'Tours' },
        { key: 'SavedArticle', title: 'Saved' },
        { key: 'Marked', title: 'Marked' },
   ],

  
    }
    this._selectPhotoTapped=this._selectPhotoTapped.bind(this)
    this._uploadImage=this._uploadImage.bind(this)
  }
// Tab function Start here  
_handleIndexChange = index =>
    this.setState({
      index,
    });

  _renderTabBar = props => (
    <TabBar
      {...props}
      scrollEnabled
      indicatorStyle={styles.indicator}
      style={styles.tabbar}
      tabStyle={styles.tab}
      labelStyle={styles.label}
    />
  );

  _renderScene = SceneMap({
    Tours: Tours,
    SavedArticle: SavedArticle,
    Marked: Marked,
 });
// Tab function ends here  


  componentDidMount(){
    InteractionManager.runAfterInteractions( async () => {
    
      requestCameraPermission();
   //console.warn("permiss")
     });
  }
SavetoDatabase = (imageresponse) => {
  let originalImageURL =imageresponse.secure_url;
      let  lowResUri = "http://res.cloudinary.com/demo/image/fetch/f_webp,q_auto:low,w_400/e_blur:90/" + originalImageURL;
      let params={
     "id":global.userid,
	"profileImageUrl":imageresponse.secure_url

        }
      axios.put(`/user/updateprofileimagename`, qs.stringify(params))
      .then( async response => {
        console.warn(response);
        global.profile.ProfilePicture=imageresponse.secure_url;
        await RefreshProfile();
        this.setState({
                      // avatarSource: { uri: imageresponse.secure_url },
                      // avatarSource:  lowResUri ,
                      loader_visible: false,
                      // isImagefromdevice: false,
                      showsaveBtn: false,
                  });
                                
                  let resp=response.data;
                 if(resp.StatusCode === '00') {
                let message =resp.Message
                Toast.show(message, Toast.SHORT, Toast.BOTTOM,ToastStyle);
               
            
              }
                        else {
                          console.warn('error');
                          let resp=response.data;
                          console.warn(resp);
                          let message =resp.Message
                          Toast.show(message, Toast.SHORT, Toast.BOTTOM,ToastStyle);
   
   
                        }            
      })
      .catch((error) => {
        console.warn(`Dp upload ${error}`);
      });
        }

  _uploadImage() {
    this.setState({
      loader_visible: true
    });
    let file2upload=this.state.imagetoUpload;
    // console.warn(file2upload)
    let source = { uri: file2upload.uri };
    this.setState({
        uploadingImg: true
    });
    uploadFile(file2upload)
        .then(response => response.json())
        .then(result => {
          // console.warn(result)
          this.SavetoDatabase(result)
            // this.setState({
            //     avatarSource: { uri: result.secure_url },
            //     loader_visible: false
            // });
        })

  }

  _selectPhotoTapped() {
    // const options = {
    //   quality: 1.0,
    //   maxWidth: 500,
    //   maxHeight: 500,
    //   storageOptions: {
    //     skipBackup: true
    //   },
    //   mediaType:'photo',
    //   // mediaType:'video',
    // };

    const options = {
      title: 'Select',
      noData:true,
      mediaType:'mixed',
      storageOptions: {
          skipBackup: true,
          path: 'construction-cloud'
      }
  };
 

    // ImagePicker.showImagePicker(options, (response) => {
      ImagePicker.launchImageLibrary(options, (response)  => {
      console.warn('Response = ', response);

      if (response.didCancel) {
        console.warn('User cancelled photo picker');
      }
      else if (response.error) {
        console.warn('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.warn('User tapped custom button: ', response.customButton);
      }
      else {
        let source = { uri: response.uri };

        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          avatarSource: source,
          isImagefromdevice:true,
          showsaveBtn:true,
          imagetoUpload:response,
        });
      }
    
    });
  }

  render() {
     const {isImagefromdevice,avatarSource,loader_visible,showsaveBtn} = this.state;
     let renderImage;
     let dd=null
    if(isImagefromdevice===true){
      renderImage=  <Image 
      style={styles.userDp} 
      source = {avatarSource}
      // source = {{
      //   uri:'https://res.cloudinary.com/custocrypt/image/upload/v1560706865/HHPNG%20image/Hidden_History_Location_Pin_NEW.png',
      // }}
      /> 
    }
    // else if (global.profile.ProfilePicture!==null){
 
    //   renderImage=   <Animatable.Image
    //   animation="bounceInDown"
    //   duration={500}
    //   style={styles.userDp}
      
    //   source={{uri:  global.profile.ProfilePicture}}
      
      
    // />
    // }
    else if (dd===null){
 
      renderImage=   <Animatable.Image
      animation="bounceInDown"
      duration={500}
      style={styles.userDp}
      
      // source={ 

      //   CustomAsset.displayPicture
           
      // } 
      source = {{
        uri:'https://res.cloudinary.com/custocrypt/image/upload/v1563653073/HHPNG%20image/category/old_man_with_pipe.jpg',
      }}
    />
    
    }

    return (
    
      // <View style={styles.pagewrap}>
      <Container>

     {/* {myspiner(loader_visible)}  */}
          <CustomizeHeader
   leftside='menu'
   ShowNotification={true}
    title='My Profile'
    />
        {/* <Content style={{ flex: 1, backgroundColor: '#000' }}> */}
          {/* <ScrollView style={{ marginTop:5}}> */}
            <View style={styles.innerpage}>
              <View style={styles.profile_image_wrap}>
                {/* <View style={{width:30,backgroundColor:'#f8ca40',alignItems:'flex-end'}}> */}
                {/* <View style={{width:45,borderRadius:45,height:45,backgroundColor:'#f8ca28',}}>
  <View style={{alignItems:'center',justifyContent:'center',paddingTop:7}}>
  <MaterialIcons name="call"   style={styles.iconsizeNcolour}/>
    </View>
    </View>  */}

                <View style={styles.imgwrap}>
                  
                  {renderImage}
               
                </View>
                {/* <View style={{width:40,borderRadius:40,height:40,backgroundColor:'#fff',marginRight:15,position:'absolute',bottom:1}}>
  <TouchableOpacity onPress={this._selectPhotoTapped}>
  <View style={{alignItems:'center',justifyContent:'center',paddingTop:7}}>
  <EvilIcons name="pencil"   style={styles.edit_iconsizeNcolour}/>
    </View>
    </TouchableOpacity>
    </View> */}
   

                {/* <View style={{width:45,borderRadius:45,height:45,backgroundColor:'blue',marginLeft:15,}}>
  <View style={{alignItems:'center',justifyContent:'center',paddingTop:7}}>
  <SimpleLineIcons name="envelope"   style={styles.iconsizeNcolour}/>
    </View>
    </View> */}
              </View>
             

              <Animatable.View
                animation="zoomInDown"
                duration={600}
                delay={100}
                style={styles.downsection}
              >
                <View style={styles.displayName}>
                   <Text style={styles.profile_name}>
                   Historian
                   </Text>
                </View>
                <View style={{marginTop:5,}}>
           <Button iconLeft rounded  style={{backgroundColor:'#636363',height:30}}>
            <Entypo name='plus' style={styles.iconsizeNcolour}/>
            <Text>Add Bio</Text>
          </Button>
           </View>
              </Animatable.View>
            </View>
          {/* </ScrollView> */}

    

        {/* </Content> */}
        <View style={styles.container}>
        <TabView
       style={[styles.container, this.props.style]}
        navigationState={this.state}
        renderScene={this._renderScene}
        renderTabBar={this._renderTabBar}
        onIndexChange={this._handleIndexChange}
/>
     
        </View>

        {/* </View> */}
        </Container>
       
    );
  }
}
export default Myprofile;

const inputStyles = {
  size: 100,
  color: '#FFFFFF',
};

const styles = StyleSheet.create({

  pagewrap: {
    flex: 1,
    backgroundColor: '#000',
    flexDirection:'column'
  },
  container: {
    flex: 1,
    marginTop:-20,
  },


  tabbar: {
    // backgroundColor: '#3f51b5',
    // backgroundColor: '#3e516f',
    backgroundColor: '#000',
  },
  tab: {
    width: 120,
  },
  indicator: {
    backgroundColor: '#727272',
    // backgroundColor: '#000',
  },
  label: {
    color: '#fff',
    // color: '#000',
    fontWeight: '400',
},



  innerpage: {
    flex: 1,
    backgroundColor: '#000',

  },
  profile_image_wrap: {
    // backgroundColor: 'rgba(0,0,0,0.4)',
    flex: 1,
    alignItems: 'center',
    // flexDirection:'row',
    // marginTop: 20,
  },
  iconsizeNcolour: {
    fontSize: 15,
    color: '#fff',
    paddingLeft:12,
  },
  footer_icon_style: {
    fontSize: 20,
    color: '#000',
  },
  sideicon_check: {
    fontSize: 40,
    color: 'green',
  },
  imgwrap: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    // width:200,
    // borderRadius:150,
    // height: 150,
    // backgroundColor:'red',
    flex: 1,
  },
  userDp: {
    height: 100,
    borderRadius: 50,
    width: 100,
    borderColor:'#fff',
    borderWidth:1
    // paddingTop: 10,
  },
  downsection: {
    // backgroundColor: 'rgba(0,0,0,0.4)',
    // paddingTop: '5%',
    // marginLeft: 'auto',
    // marginRight: 'auto',
    // width: '80%',
    // alignItems:'center',
    flex:1,
    // marginTop:10,
    alignItems:'center',
    // backgroundColor:'red'
  },
  displayName: {
    // flex:1,
    // paddingTop: 8,
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor:'red',
  },
  designation: {
    // flex:1,
    paddingTop: 8,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomColor: 'green',
    borderBottomWidth: 1,
  },
  profile_name: {
    color: '#fff',
    fontSize: 30,
    // fontWeight: 'bold',
    textAlign: 'center',
  },
  profile_designation: {
    color: '#000',
    fontSize: 20,
    // fontWeight:'bold',
    textAlign: 'center',
    paddingBottom: 12,
  },
  biography: {
    // flex:1,
    paddingTop: 8,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomColor: 'green',
    borderBottomWidth: 1,
  },
  profile_biography: {
    color: '#000',
    fontSize: 15,
    // fontWeight:'bold',
    textAlign: 'justify',
    paddingBottom: 12,
  },
  address: {
    // flex:1,
    paddingTop: 8,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomColor: 'green',
    borderBottomWidth: 1,
  },
  Phone_website: {
    // flex:1,
    paddingTop: 8,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomColor: 'green',
    borderBottomWidth: 1,
    flexDirection: 'row',
  },
  profile_address: {
    color: '#000',
    fontSize: 15,
    textAlign: 'justify',
    paddingBottom: 12,
  },
  profile_website: {
    color: '#000',
    fontSize: 15,
    textAlign: 'justify',
    paddingBottom: 12,
  },
  profile_website_wrap: {
    flex: 1,
  },
  profile_phone: {
    color: '#000',
    fontSize: 15,
    textAlign: 'justify',
    paddingBottom: 12,
  },
  profile_phone_wrap: {
    flex: 1,
  },
  demakation: {
    width: 20,
  },
  demakation_text: {
    borderStyle: 'solid',
    borderLeftWidth: 2,
    borderLeftColor: '#000',
    // backgroundColor:'#FFF',
    height: 20,
  },
  footerWrap: {
    // marginVertical: 5,
    // borderTopRadius: 15,
    borderStyle: 'solid',
    borderTopWidth: 1,
    borderTopColor: '#ccc',
    backgroundColor: '#FFF',
    height: 50,
  },
  footertab: {
    backgroundColor: '#FFF',
  },
  edit_iconsizeNcolour: {
    fontSize:30,
    color:'#F75A4A',
  
    },
  // 3720
});

function uploadFile(file) {
  // console.warn(file)
  let fileDir=file.uri;
  
  const cleanFilePath = Platform.OS === 'ios' ? fileDir.replace('file://', '') :fileDir;
 
  
  return RNFetchBlob.fetch('POST', 'https://api.cloudinary.com/v1_1/' + YOUR_CLOUDINARY_NAME() + '/image/upload?upload_preset=' + YOUR_CLOUDINARY_PRESET(), {
      'Content-Type': 'multipart/form-data'
  }, [
          { name: 'file', filename: file.fileName, data: RNFetchBlob.wrap(cleanFilePath) }
      ])
}
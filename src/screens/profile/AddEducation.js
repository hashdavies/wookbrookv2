import React, { Component } from 'react';
import {
  Text,
  Image,
  StyleSheet,
  ImageBackground,
  Keyboard,
  TouchableOpacity
} from 'react-native';
import { KeyboardAvoidingView } from 'react-native';
import CustomizeHeader from '../frags/CustomizeHeader';

import Ionicons from 'react-native-vector-icons/Ionicons';
import {
  Container,
  Header,
  Content,
  Textarea,
  Button,
  View,
  Thumbnail,
  Form,
  Item,
  Input,
  Label,
  Badge,
  Icon,
  Footer,
  Picker,
  Left,
  Right,
  Body,
  Title,
  Center,
} from 'native-base';
import {
  Kaede,
  Hoshi,
  Jiro,
  Isao,
  Madoka,
  Akira,
  Hideo,
  Kohana,
  Makiko,
  Sae,
  Fumi,
} from 'react-native-textinput-effects';

import { myspiner } from '../../dependency/UtilityFunctions';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Entypo from 'react-native-vector-icons/Entypo';
import User from "../../assets/workbrookAssets/user4.png";

import accountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import { ScrollView } from 'react-native-gesture-handler';
import DatePicker from 'react-native-datepicker'


@observer
class AddEducation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      errorloading: false,
      date: "2016-05-15",

    }
    this._LoginHandler = this._LoginHandler.bind(this);
  }
  async  _LoginHandler() {
    const { email, password } = this.state;
    let data = {
      email,
      password
    }
    // let    result = await accountStore._LoginUserNowtest(data);
    let result = await accountStore._LoginUserNow(data);
    let re = accountStore.redirectmode;

    // console.warn(re)
    if (re === true) {
      Toast.show('Login Successfully', Toast.SHORT, Toast.BOTTOM, ToastStyle);

      this.props.navigation.navigate("SignedIn");
    }
    else {
      // console.warn('fuckkkkkk')
    }

  }
  render() {
    return (
      <Container>
        <ScrollView>
          <CustomizeHeader
            leftside='cancel'
            title='Add Education'
            rightside='save'
            statedColor='#1E93E2'
            icon='md-close'
          />
          <View style={styles.profileWrap}>
            <View style={{ width: "100%", padding: 15 }}>
              <Item style={[styles.profileinput,]}>
                <Input style={styles.profileinputText} placeholderTextColor={'#2F2F2F'} placeholder="School/University" />
              </Item>
              <Item style={[styles.profileinput,]}>
                <Input style={styles.profileinputText} placeholderTextColor={'#2F2F2F'} placeholder="Degree" />
              </Item>
              <Item style={[styles.profileinput,]}>
                <Input style={styles.profileinputText} placeholderTextColor={'#2F2F2F'} placeholder="Field of study" />
              </Item>
              <View style={[styles.cardContent1, {marginBottom: 30, marginTop: 30, paddingLeft:0, paddingRight: 0}]}>
                <Left>
                  <DatePicker
                    style={{ width: 150, display: 'flex',flexDirection: 'column', justifyContent:'flex-start', alignItems: 'center', }}
                    date={this.state.date}
                    mode="date"
                    placeholder="select date"
                    format="YYYY-MM-DD"
                    minDate="2016-05-01"
                    maxDate="2016-06-01"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    showIcon={false}
                    customStyles={{
                      dateInput: {
                        marginLeft: 5,
                        borderTopWidth: 0,
                        borderLeftWidth: 0,
                        borderRightWidth: 0,
                        borderBottomColor: '#E5E5E5',
                        display: 'flex',flexDirection: 'row', justifyContent:'flex-start', alignItems: 'center' ,
                        height: 70,

                      },
                      dateText: {
                        fontSize: 13, fontWeight: '500', textAlign: 'left', color: '#2F2F2F',
                      },
                      // ... You can check the source to find the other keys.
                    }}
                    onDateChange={(date) => { this.setState({ date: date }) }}
                  />
                </Left>
                <Right style={{paddingLeft: 0}}>
                <DatePicker
                    style={{ width: 150, textAlign: 'left',}}
                    date={this.state.date}
                    mode="date"
                    placeholder="select date"
                    format="YYYY-MM-DD"
                    minDate="2016-05-01"
                    maxDate="2016-06-01"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    showIcon={false}
                    customStyles={{
                      dateInput: {
                        marginLeft: 0,
                        borderTopWidth: 0,
                        borderLeftWidth: 0,
                        borderRightWidth: 0,
                        borderBottomColor: '#E5E5E5',
                        display: 'flex',flexDirection: 'row', justifyContent:'flex-start', alignItems: 'center' ,
                        height: 70,
                      },
                      dateText: {
                        fontSize: 13, fontWeight: '500', textAlign: 'left', color: '#2F2F2F'
                      },
                      // ... You can check the source to find the other keys.
                    }}
                    onDateChange={(date) => { this.setState({ date: date }) }}
                  />
                </Right>
              </View>
              <View style={styles.cardContent1}>
                <Left>
                  <Text style={styles.minBold2} >Description (100 words)</Text>
                </Left>
                <Right>
                  <View style={styles.textIcon}>
                    <View style={[styles.iconesty, { borderColor: '#FF5964', }]}>
                      <Text note style={[styles.iconestyText]}>0</Text>
                    </View>
                    <View style={[styles.iconesty, { borderColor: '#FC9F13', }]}>
                      <Text note style={[styles.iconestyText, { color: '#FC9F13' }]}>30</Text>
                    </View>
                    <View style={[styles.iconesty, { borderColor: '#5ABC7A', }]}>
                      <Text note style={[styles.iconestyText, { color: '#5ABC7A' }]}>100</Text>
                    </View>
                  </View>
                </Right>
              </View>

              <Textarea rowSpan={5} style={styles.bordedTextArea} placeholder="" />
              {/* <Text style={styles.inputError}>Please enter your summary</Text> */}
            </View>

          </View>
        </ScrollView>
      </Container>
    );
  }
}
export default AddEducation;

const inputStyles = {
  size: 100,
  color: '#FFFFFF',
};

const height = 40;

const styles = StyleSheet.create({
  profileWrap: {
    flex: 1,
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: "#fff"
  },
  profileWrapinner: {
    flex: 1,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 25
  },
  profileinput: {
    marginLeft: 0,
    borderBottomColor: '#E5E5E5',
    height: 70,
    // marginBottom: 15
  },
  profileinputText: {
    height: "100%",
    color: "#2F2F2F",
    fontSize: 13,
    fontWeight: '500'
  },
  editpenWap: {
    backgroundColor: '#fff',
    height: 40,
    width: 40,
    borderRadius: 20,
    display: 'flex',
    justifyContent: "center",
    alignItems: "center",
    zIndex: 99,
    marginTop: -30,
    marginLeft: 50,
    marginRight: -50
  },
  inputError: {
    color: '#FF5964',
    fontWeight: '300',
    fontSize: 11
  },
  minBold2: {
    fontSize: 13,
    fontWeight: "500",
    color: '#2F2F2F'
  },
  cardContent1: {
    display: "flex",
    flexDirection: "row",
    width: '100%',
    paddingLeft: 5,
    paddingRight: 5,
  },
  textIcon: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center'
  },
  iconesty: {
    height: 23,
    width: 23,
    borderRadius: 23 / 2,
    borderWidth: 2,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 0,
    marginLeft: 3
  },
  iconestyText: {
    color: "#FF5964",
    fontSize: 9,
    fontWeight: '500'
  },
  bordedTextArea: {
    borderBottomColor: "#E5E5E5",
    borderBottomWidth: 1
  },


});

import React, { Component } from 'react';
import {
  Text,
  Image,
  StyleSheet,
  ImageBackground,
  Keyboard,
  TouchableOpacity
} from 'react-native';
import { KeyboardAvoidingView } from 'react-native';
import CustomizeHeader from '../frags/CustomizeHeader';

import Ionicons from 'react-native-vector-icons/Ionicons';
import {
  Container,
  Header,
  Content,
  Textarea,
  Button,
  View,
  Thumbnail,
  Form,
  Item,
  Input,
  Label,
  Badge,
  Icon,
  Footer,
  Picker,
  Left,
  Right,
  Body,
  Title,
  Center,
  CardItem
} from 'native-base';
import {
  Kaede,
  Hoshi,
  Jiro,
  Isao,
  Madoka,
  Akira,
  Hideo,
  Kohana,
  Makiko,
  Sae,
  Fumi,
} from 'react-native-textinput-effects';

import { myspiner } from '../../dependency/UtilityFunctions';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import User from "../../assets/workbrookAssets/user4.png";

import accountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import { ScrollView } from 'react-native-gesture-handler';
import DatePicker from 'react-native-datepicker'
// import Autocomplete from 'native-base-autocomplete'
import Autocomplete from 'react-native-autocomplete-input';
import DashboardStore from '../../stores/Dashboard';
import EmptyListComponent from '../../StatelessScreens/EmptyListComponent';

const API = 'https://swapi.co/api';
@observer
class EditSkills extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      errorloading: false,
      date: "2016-05-15",
      // films: ['Apple', 'Mango', 'Guava', 'Muskmelon', 'Watermelon', 'Orange', 'Sapota'],
      films: [],
      query: '',
    }
    this._LoginHandler = this._LoginHandler.bind(this);
  }
  async  _LoginHandler() {
    const { email, password } = this.state;
    let data = {
      email,
      password
    }
    // let    result = await accountStore._LoginUserNowtest(data);
    let result = await accountStore._LoginUserNow(data);
    let re = accountStore.redirectmode;

    // console.warn(re)
    if (re === true) {
      Toast.show('Login Successfully', Toast.SHORT, Toast.BOTTOM, ToastStyle);

      this.props.navigation.navigate("SignedIn");
    }
    else {
      // console.warn('fuckkkkkk')
    }

  }
 async componentDidMount() {
    DashboardStore._FetchAllIntrest(1,30);
  await  DashboardStore.__FetchAllMyKeywords();

    let staticData=[
      {title: 'val1', details: { id: '1', name:'Paris', country:'FR', continent:'Europe'}},
      {title: 'val2', details: { id: '2', name: 'Pattanduru', country:'PA', continent:'South America'}},
      {title: 'val3', details: { id: '3', name: 'Para', country:'PA', continent: 'South America'}},
      {title: 'val4', details: { id: '4', name:'London', country:'UK', continent: 'Europe'}},
      {title: 'val5', details: { id: '5', name:'New York', country: 'US', continent: 'North America'}},
      {title: 'val6', details: { id: '6', name:'Berlin', country: 'DE', continent: 'Europe'}},
     ]
   this.setState({
     films:staticData
   })
    //First method to be called after components mount
    //fetch the data from the server for the suggestion
    // fetch(`${API}/films/`)
    //   .then(res => res.json())
    //   .then(json => {
    //     const { results: films } = json;
    //     this.setState({ films });
    //     //setting the data in the films state
    //   });
  }
  findFilm(query) {
    //method called everytime when we change the value of the input
    if (query === '') {
      //if the query is null then return blank
      return [];
    }
    DashboardStore.IntrestSettings.AllSystemIntrest
    const { films } = this.state;
    //making a case insensitive regular expression to get similar value from the film json
    const regex = new RegExp(`${query.trim()}`, 'i');
    //return the filtered film array according the query from the input
    return films.filter(film => film.title.search(regex) >= 0);
  }

  
  render() {
  const { query } = this.state;
    const films = this.findFilm(query);
    const comp = (a, b) => a.toLowerCase().trim() === b.toLowerCase().trim();
    let   Keywords_ErrorComponentDisplay = <EmptyListComponent
    message={DashboardStore.ListMyKeywords.ErrorMessage}
    onclickhandler={()=>DashboardStore.__FetchAllMyKeywords()}
  />
    let MyKeywords= DashboardStore.ListMyKeywords.Keywords .map((item,index) => {

           
      return ( 
        <React.Fragment key={index}>
 <Button style={styles.btnChecky}>
        <Text style={styles.btnCheckyText}>{item}</Text>
        <AntDesign 
        name='close' 
        style={styles.iconsizeNcolour}
        
        />
      </Button>
        </React.Fragment>
       
)


});
    return (
      <Container>
        <ScrollView>
          <CustomizeHeader
            leftside='cancel'
            title='Edit Skills'
            rightside='save'
            statedColor='#1E93E2'
            icon='md-close'
          />
          <View style={{padding:15}}>
          <Autocomplete
          autoCapitalize="none"
          autoCorrect={false}
          containerStyle={styles.autocompleteContainer}
          data={films.length === 1 && comp(query, films[0].title) ? [] : films}
          defaultValue={query}
          onChangeText={text => this.setState({ query: text })}
          style={[styles.profileinput2,{borderColor: '#E5E5E5'}]}
          placeholderTextColor={'#B4B4B4'}
          placeholder="Skills (ex: Interior Design)"
          renderItem={({ item }) => (
            <TouchableOpacity onPress={() => this.setState({ query: item.title })}>
              <Text style={styles.itemText}>
                {item.title} ({item.release_date})
              </Text>
            </TouchableOpacity>
          )}
        />
        </View>
        {/* <View style={styles.descriptionContainer}>
          {films.length > 0 ? (
            <Text style={styles.infoText}>{this.state.query}</Text>
          ) : (
            <Text style={styles.infoText}>Enter The Film Title</Text>
          )}
        </View> */}
          <View style={styles.profileWrap}>
            <View style={styles.devtrop}>
              <CardItem style={{ width: "100%", backgroundColor: "transparent", paddingTop: 0, }}>
                <Left>
                  <Text style={styles.minBold2} >You can add upto 20 skills</Text>
                </Left>
              </CardItem>
              <CardItem style={{ width: "100%", backgroundColor: "transparent", paddingTop: 0, maxWidth: '100%' }}>
                <Left style={{ alignItems: 'flex-start', borderBottomWidth: 1, borderColor: '#E5E5E5', paddingBottom: 15, flexWrap: 'wrap', width: '100%' }}>
                {/* {MyKeywords} */}
                {DashboardStore.ListMyKeywords.Keywords.length > 0 ?
MyKeywords
:
Keywords_ErrorComponentDisplay
                        }
                  <Button style={styles.btnChecky}>
                    <Text style={styles.btnCheckyText}>Design</Text>
                    <AntDesign name='close' style={styles.iconsizeNcolour} />
                  </Button>
                  <Button style={styles.btnChecky}>
                    <Text style={styles.btnCheckyText}>Team Leadership</Text>
                    <AntDesign name='close' style={styles.iconsizeNcolour} />
                  </Button>
                  <Button style={styles.btnChecky}>
                    <Text style={styles.btnCheckyText}>UIUX</Text>
                    <AntDesign name='close' style={styles.iconsizeNcolour} />
                  </Button>
                  <Button style={styles.btnChecky}>
                    <Text style={styles.btnCheckyText}>Public Speaking</Text>
                    <AntDesign name='close' style={styles.iconsizeNcolour} />
                  </Button>
                  <Button style={styles.btnChecky}>
                    <Text style={styles.btnCheckyText}>Marketing</Text>
                    <AntDesign name='close' style={styles.iconsizeNcolour} />
                  </Button>
                </Left>
              </CardItem>
            </View>

            <View style={styles.devtrop}>
              <CardItem style={{ width: "100%", backgroundColor: "transparent", paddingTop: 0, }}>
                <Left>
                  <Text style={styles.minBold2} >Suggested skills based off your profile:</Text>
                </Left>
              </CardItem>
              <CardItem style={{ width: "100%", backgroundColor: "transparent", paddingTop: 0, maxWidth: '100%' }}>
                <Left style={{ alignItems: 'flex-start', paddingBottom: 15, flexWrap: 'wrap', width: '100%' }}>
                  <Button style={styles.SurgestKills}>
                    <Text style={styles.SurgestKillsText}>Design</Text>
                    <AntDesign name='plus' style={styles.SurgestKillsIcon} />
                  </Button>
                  <Button style={styles.SurgestKills}>
                    <Text style={styles.SurgestKillsText}>Team Leadership</Text>
                    <AntDesign name='plus' style={styles.SurgestKillsIcon} />
                  </Button>
                  <Button style={styles.SurgestKills}>
                    <Text style={styles.SurgestKillsText}>UIUX</Text>
                    <AntDesign name='plus' style={styles.SurgestKillsIcon} />
                  </Button>
                  <Button style={styles.SurgestKills}>
                    <Text style={styles.SurgestKillsText}>Public Speaking</Text>
                    <AntDesign name='plus' style={styles.SurgestKillsIcon} />
                  </Button>
                  <Button style={styles.SurgestKills}>
                    <Text style={styles.SurgestKillsText}>Marketing</Text>
                    <AntDesign name='plus' style={styles.SurgestKillsIcon} />
                  </Button>
                </Left>
              </CardItem>
            </View>

          </View>
        </ScrollView>
      </Container>
    );
  }
}
export default EditSkills;

const inputStyles = {
  size: 100,
  color: '#FFFFFF',
};

const height = 40;

const styles = StyleSheet.create({
  profileWrap: {
    flex: 1,
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: "#fff"
  },
  autocompleteContainer: {
    flex: 1,
    left: 0,
    position: 'absolute',
    right: 0,
    top: 0,
    zIndex: 1
  },
  profileWrapinner: {
    flex: 1,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 25
  },
  profileinput: {
    marginLeft: 0,
    borderWidth: 1,
    borderColor: '#E5E5E5',
    height: 55,
    // marginBottom: 15
  },
  profileinput2: {
    marginLeft: 0,
    borderWidth: 1,
    borderColor: '#E5E5E5',
    height: 55,
    color: "#2F2F2F",
    fontSize: 14,
    fontWeight: '500'
    // marginBottom: 15
  },
  profileinputText: {
    height: "100%",
    color: "#2F2F2F",
    fontSize: 14,
    fontWeight: '500'
  },
  btnChecky: {
    height: 28,
    borderWidth: 1,
    borderColor: '#1E93E2',
    borderRadius: 5,
    backgroundColor: "transparent",
    elevation: 0,
    paddingLeft: 18,
    paddingRight: 18,
    marginRight: 5,
    marginBottom: 5,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  SurgestKills: {
    height: 28,
    borderWidth: 1,
    borderColor: '#B4B4B4',
    borderRadius: 5,
    backgroundColor: "#B4B4B4",
    elevation: 0,
    paddingLeft: 18,
    paddingRight: 18,
    marginRight: 5,
    marginBottom: 5,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  SurgestKillsText: {
    fontSize: 11,
    fontWeight: 'bold',
    color: '#fff'

  },
  btnCheckyText: {
    fontSize: 11,
    fontWeight: 'bold',
    color: '#1E93E2'

  },
  SurgestKillsIcon: {
    fontSize: 11,
    fontWeight: 'bold',
    color: '#fff',
    marginLeft: 8

  },
  iconsizeNcolour: {
    fontSize: 11,
    fontWeight: 'bold',
    color: '#1E93E2',
    marginLeft: 8

  },
  SkillsminBold2: {
    fontSize: 11,
    color: '#2F2F2F'
  },
  minBold2: {
    fontSize: 11,
    color: '#2F2F2F'
  },
  devtrop: {
    display: "flex",
    justifyContent: "flex-start",
  },
// auto complete
autocompleteContainer: {
  backgroundColor: '#ffffff',
  borderWidth: 0,
},
descriptionContainer: {
  flex: 1,
  justifyContent: 'center',
},
itemText: {
  fontSize: 15,
  paddingTop: 5,
  paddingBottom: 5,
  margin: 2,
},
infoText: {
  textAlign: 'center',
  fontSize: 16,
},
// auto complete

});

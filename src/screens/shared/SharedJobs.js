import React, { Component } from 'react';
import {
  Text,
  Image,
  StyleSheet,
  ImageBackground,
  Keyboard,
  TouchableOpacity,
  FlatList,
  Alert
} from 'react-native';
import { KeyboardAvoidingView } from 'react-native';
import CustomizeHeader from '../frags/CustomizeHeader';

import Ionicons from 'react-native-vector-icons/Ionicons';
import {
  Container,
  Header,
  Content,
  Textarea,
  Button,
  View,
  Thumbnail,
  Form,
  Item,
  Input,
  Label,
  Badge,
  Icon,
  Footer,
  Picker,
  Left,
  Right,
  Body,
  Title,
  Center,
  CardItem,
  CheckBox
} from 'native-base';
import {
  Kaede,
  Hoshi,
  Jiro,
  Isao,
  Madoka,
  Akira,
  Hideo,
  Kohana,
  Makiko,
  Sae,
  Fumi,
} from 'react-native-textinput-effects';
import Modal from 'react-native-modal';
import { myspiner, AppInlineLoader } from '../../dependency/UtilityFunctions';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import User from "../../assets/workbrookAssets/user4.png";
import { withNavigation } from 'react-navigation';
import accountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import { ScrollView } from 'react-native-gesture-handler';
import imageUser from '../../assets/workbrookAssets/user2.png';
import DatePicker from 'react-native-datepicker'
// import Autocomplete from 'native-base-autocomplete'
import Autocomplete from 'react-native-autocomplete-input';
import GridView from 'react-native-gridview';
import { SvgUri } from 'react-native-svg';
import DashboardStore from '../../stores/Dashboard';
import EmptyListComponent from '../../StatelessScreens/EmptyListComponent';
const API = 'https://swapi.co/api';

@observer
class SharedJobs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      isModalVisible: false,
      isModalWithdrawAppVisible: false,
    }
    this.ReloadPageData = this.ReloadPageData.bind(this);
    this.GetApplicantInMyList = this.GetApplicantInMyList.bind(this);
    // this.FetchRecruiters = this.FetchRecruiters.bind(this);
    this.HireARecruiter = this.HireARecruiter.bind(this);
    this.ViewShortlistedApplicant = this.ViewShortlistedApplicant.bind(this)
    this.EditJobOffer = this.EditJobOffer.bind(this)
    this.CloseJobActionModal = this.CloseJobActionModal.bind(this)
    this.CloseJobOffer = this.CloseJobOffer.bind(this)

  }
  toggleModal = (item) => {
    console.log(item)
    let current_id = item.id;
    let Jobtitle = item.job_level["id"]
    // alert(Jobtitle)
    // let OldData=DashboardStore.JobPosting;
    let companyName = item.campany_name;
    let industry = item.industry;
    let aboutCompany = item.about_company;
    let location = item.location;
    let numberOfStaff = item.no_of_staff;
    let roleTitle = item.role_title;
    let jobLevel = parseInt(Jobtitle);
    let wordForApplicant = item.applicant_benefit;
    let intrest = item.skills_required;
    let additional_files = item.additional_files;
    // current_id: '';
    let isCompanyRepresentative = item.isCompanyRepresentative;
    let isToRefTimeLineOrToHireRecruter = item.isToRefTimeLineOrToHireRecruter;
    // HeaderText: 'New Post';
    // let  SelectedRecuiter=item.industry;

    let additional = {
      HeaderText: 'Edit Job',
      current_id: current_id,

      companyName,
      industry,
      aboutCompany,
      location,
      numberOfStaff,
      roleTitle,
      jobLevel,
      wordForApplicant,
      intrest,
      additional_files,
      isCompanyRepresentative,
      isToRefTimeLineOrToHireRecruter,
      //  SelectedRecuiter,
    }
    let NewJobPostData = { ...{}, ...additional }
    DashboardStore.JobPosting = NewJobPostData
    console.log(DashboardStore.JobPosting)

    this.setState({ isModalVisible: !this.state.isModalVisible });
  };
  CloseJobActionModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible });
  };
  toggleModalWithdrawApp = () => {
    this.setState({ isModalVisible: false });
    this.setState({ isModalWithdrawAppVisible: !this.state.isModalWithdrawAppVisible });
  };
  EditJobOffer = () => {
    // console.log(item)
    // DashboardStore.JobPosting
    // this.setState({ isModalVisible: false });
    // this.setState({ isModalWithdrawAppVisible: !this.state.isModalWithdrawAppVisible });

    this.props.navigation.navigate(
      'Newpost', {
      data: "EditJob",
      otherParam: 'anything you want here',
    }
    );
    this.setState({ isModalVisible: !this.state.isModalVisible });

  };
  ReloadPageData = () => {
    DashboardStore.__GetRecieversJobs(1, 30);
  };
  GetApplicantInMyList = (item) => {
    DashboardStore._updateParameterV2(item, "SingleSharedJob", "MySharedJobs");
    this.props.navigation.navigate("Shared");
  };
  // FetchRecruiters = (item) => {
  //   DashboardStore._updateParameterV2(item, "singleJob", "JobOffers");
  //   this.props.navigation.navigate("InsightRecruiterAvailable")
  // };
  //   FetchRecruiters = (item) => {
  //   DashboardStore._updateParameterV2(item, "singleJob", "JobOffers");
  //   this.props.navigation.navigate("InsightRecruiterAvailable");
  //   onPress={() => { this.props.navigation.navigate("RecruiterAvailable") }}

  // };
  CloseJobOffer = () => {
    // DashboardStore._updateParameterV2(item, "SingleRecruiter", "Recruiters");
    // this.props.navigation.navigate("RecruiterProfile");
    Alert.alert(
      'Close Job Offer',
      `Are you sure ?`,
      [
        {
          text: 'No', onPress: () => { },//Do nothing
          style: 'cancel'
        },
        {
          text: 'Yes', onPress: () => {
            DashboardStore.__CloseJobOffer();
            this.setState({ isModalVisible: !this.state.isModalVisible });

            // DashboardStore._updateParameterV2(item, "singleJob", "JobOffers");
            // this.props.navigation.navigate(
            // 'RecruiterAvailable',{
            // data: "HireNewRecruiter",
            // otherParam: 'anything you want here',
            // }
            // );
          }
        },
      ],
      { cancelable: true }
    );


  };
  HireARecruiter = (item) => {
    // DashboardStore._updateParameterV2(item, "SingleRecruiter", "Recruiters");
    // this.props.navigation.navigate("RecruiterProfile");
    Alert.alert(
      'Hire a recruiter',
      `Are you sure ?`,
      [
        {
          text: 'No', onPress: () => { },//Do nothing
          style: 'cancel'
        },
        {
          text: 'Yes', onPress: () => {
            // DashboardStore.Recruiters.CurrentJobId=0;
            let currentjobid = item.id;
            DashboardStore._updateParameterV2(currentjobid, "CurrentJobId", "Recruiters");
            DashboardStore._updateParameterV2(item, "singleJob", "JobOffers");
            this.props.navigation.navigate(
              'RecruiterAvailable', {
              data: "HireNewRecruiter",
              otherParam: 'anything you want here',
            }
            );
          }
        },
      ],
      { cancelable: true }
    );


  };
  componentDidMount() {
    // DashboardStore.__GetJobOffers(1, 30);
    DashboardStore.__GetRecieversJobs(1, 50);
  }
  ViewShortlistedApplicant = (item) => {
    let t = { job_id: item.id }
    let newItem = { ...item, ...t }
    console.log(newItem)
    console.log('newItem')
    DashboardStore._updateParameterV2(newItem, "SingleAcceptedJob", "RecruiterInsight");
    // this.props.navigation.navigate("ShortListedApplicants");
    this.props.navigation.navigate(
      'ShortListedApplicants',
      {
        data: "ViewOnly",
        otherParam: 'anything you want here',
      }

    );

  };
  render() {
    const { MySharedJobs } = DashboardStore;

    let ErrorComponentDisplay = null;
    // console.log(DashboardStore.JobPosting);

    if (DashboardStore.isProcessing === true) {
      ErrorComponentDisplay = AppInlineLoader(DashboardStore.isProcessing)

    }
    else {
      ErrorComponentDisplay = <EmptyListComponent
        message={MySharedJobs.ErrorMessage}
        onclickhandler={this.ReloadPageData}
      />
    }

    let ShaeredJobListing =
      <FlatList
        // Data
        data={MySharedJobs.MyJobList}
        // Render Items
        renderItem={({ item }) => {
          let Applicants = item.applicants
          // let isToRefTimeLineOrToHireRecruter = item.isToRefTimeLineOrToHireRecruter;

          const Applicants_images =null;
            // const Applicants_images =
            //   Applicants.slice(0, 7).map(function (item, index) {

            //     return <View style={styles.userFlexImageWrap} key={index}>
            //       <Image source={{ uri: item.image_url }} style={styles.userFlexImage} />
            //     </View>
            //   })
            return (
              <View style={styles.successCard2}>
                <View style={styles.cardinnerSty}>
                  {/* <Text style={styles.Text3}>{`${item.job_applicants} applicant(s)`} </Text> */}
                  <Text style={styles.Text3}>{`${item.campany_name}`} </Text>
                </View>
                <View style={styles.xLayer}>
                  <Left style={{ borderBottomWidth: 0, marginLeft: 0, flex: 1 }}>
                    <Text style={[styles.Text4, { marginLeft: 0 }]}>{item.role_title}</Text>
                    <Text style={styles.Text5} numberOfLines={1} >
                      {item.location}
                    </Text>
                  </Left>
                  <Right style={[styles.morestyIdc, { borderBottomWidth: 0, width: 20, flex: 0 }]}>
                    <TouchableOpacity style={styles.moreBtnSty}
                      onPress={() => this.toggleModal(item)}
                    >
                      <SvgUri
                        width='14.79px'
                        height='4px'
                        uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1589464409/Mobile_Assets/more_xcza8v.svg"
                      />
                    </TouchableOpacity>
                  </Right>
                </View>
                <Text style={[styles.Text1, { fontWeight: 'normal', textAlign: 'left', paddingLeft: 19, paddingRight: 19, marginTop: 14, marginBottom: 12 }]}>Top applicants</Text>
                <View style={styles.userFlex}>
                  {Applicants_images}

                </View>
                <View style={styles.successCard2Bottom}>
                  <Button transparent
                    onPress={() => this.GetApplicantInMyList(item)}
                  >
                    <Text style={[styles.Text3, { color: '#1E93E2' }]}>View  Applicants</Text>
                  </Button>
                  {/* <Button style={styles.card2Bottom}
                    onPress={() => this.HireARecruiter(item)}

                  >

                    <Text style={[styles.Text3, { color: '#fff' }]}>Hire a recruiter</Text>
                  </Button> */}
                </View>
              </View>
            )
        
        
        }}
        // Item Key
        keyExtractor={(item, index) => String(index)}
      //   Header (Title)
      //   ListHeaderComponent={this.renderHeader}
      //   Footer (Activity Indicator)
      //   ListFooterComponent={this.renderFooter()}
      //   On End Reached (Takes a function)
      //   onEndReached={this.retrieveMore}
      // How Close To The End Of List Until Next Data Request Is Made
      // onEndReachedThreshold={0}
      // Refreshing (Set To True When End Reached)
      // refreshing={this.state.refreshing}
      />













    return (
      <View style={{ flex: 1, backgroundColor: '#fff' }}>
        <CustomizeHeader
                        leftside='WhiteArr'
                        title='Shortlisted Applicants'
                        rightside='empty'
                        statedColor='#1E93E2'
                        icon='md-close'
                        // savehandler={this.proceednow}
                    />
        <ScrollView
          contentContainerStyle={{
            paddingLeft: 30,
            paddingRight: 30,
            paddingBottom: 30,
            paddingTop: 30
          }}
        >


{ 
          
            MySharedJobs.MyJobList.length > 0 ?
              <React.Fragment>
                <View style={styles.MContainer}>
                  <Text style={styles.Text2}>
                    {/* 33 */}
                  </Text>
                </View>
                {ShaeredJobListing}
                {/* <View style={styles.successCard2}>
                <View style={styles.cardinnerSty}>
                  <Text style={styles.Text3}>13 Applicants </Text>
                </View>
                <View style={styles.xLayer}>
                  <Left style={{ borderBottomWidth: 0, marginLeft: 0, flex: 1 }}>
                    <Text style={[styles.Text4, { marginLeft: 0 }]}>Supervisor</Text>
                    <Text style={styles.Text5} numberOfLines={1} >
                      Lagos
                    </Text>
                  </Left>
                  <Right style={[styles.morestyIdc, { borderBottomWidth: 0, width: 20, flex: 0 }]}>
                    <TouchableOpacity style={styles.moreBtnSty}
                      // onPress={() => this.toggleModal(item)}
                    >
                      <SvgUri
                        width='14.79px'
                        height='4px'
                        uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1589464409/Mobile_Assets/more_xcza8v.svg"
                      />
                    </TouchableOpacity>
                  </Right>
                </View>
                <Text style={[styles.Text1, { fontWeight: 'normal', textAlign: 'left', paddingLeft: 19, paddingRight: 19, marginTop: 14, marginBottom: 12 }]}>Top applicants</Text>
                <View style={styles.userFlex}>
                <View style={styles.userFlexImageWrap}>
                  <Image source={{ uri: "https://res.cloudinary.com/workbrook-hash/image/upload/v1589587886/Mobile_Assets/person2_sysv9h.png" }} style={styles.userFlexImage} />
                </View>

                </View>
                <View style={styles.successCard2Bottom}>
                  <Button transparent
                   style={{height: 28}}
                    onPress={() => {this.props.navigation.navigate("Shared")}}
                  >
                    <Text style={[styles.Text3, { color: '#1E93E2' }]}>View Shortlisted Applicants</Text>
                  </Button>
                  <Button style={styles.card2Bottom}
                    // onPress={() => this.HireARecruiter(item)}

                  >

                    <Text style={[styles.Text3, { color: '#fff' }]}>Hire a recruiter</Text>
                  </Button>
                </View>
              </View>
           */}
              </React.Fragment>
              :
              ErrorComponentDisplay

          } 
        



        </ScrollView>
        {/* modal views */}
        <Modal style={styles.MContainer} isVisible={this.state.isModalVisible}>
          <View style={styles.Mwrapper}>
            <View style={styles.firstView} >
              <Button full style={styles.actionBtn}

                onPress={this.EditJobOffer}
              >
                <Text style={styles.actionBtnText}>
                  {/* Edit the offer */}
                  Edit

              </Text>
              </Button>
              {/* <Button full style={[styles.actionBtn, { borderTopWidth: 1, borderTopColor: "#E5E5E5", borderBottomWidth: 1, borderBottomColor: "#E5E5E5" }]}>
                <Text style={styles.actionBtnText}>
                  Edit your profile
              </Text>
              </Button> */}
              <Button full style={[styles.actionBtn, { borderTopWidth: 1, borderTopColor: "#E5E5E5", }]}
                onPress={this.CloseJobOffer}

              >
                <Text style={[styles.actionBtnText, { color: "#FF5964" }]}>
                  {/* Close the offer */}
                  Close
              </Text>
              </Button>
            </View>
            <Button full style={[styles.btnCancel, { marginTop: 15 }]} title="Hide modal"
              onPress={this.CloseJobActionModal}
            >
              <Text style={[styles.btnCancelText,]}>Cancel</Text>
            </Button>
          </View>
        </Modal>
        {/* Withdraw App  modal*/}
        <Modal style={styles.MContainer} isVisible={this.state.isModalWithdrawAppVisible}>
          <View style={styles.Mwrapper}>
            <View style={styles.firstView} >
              <Button full style={styles.actionBtn2}>
                <Text style={[styles.actionBtnText, { color: "#FF5964" }]}>
                  Withdraw application
              </Text>
              </Button>
              <View style={styles.checkBoxWrap}>
                <Text style={[styles.Text4_2, { width: '100%' }]}>Why have you chosen to close this role?</Text>
                <View style={styles.CheckBoxBody}>
                  <CheckBox checked={true} color="#E5E5E5" style={{ borderRadius: 3, height: 13, width: 13, }} />
                  <Text style={styles.checkBoxText}>The process was taking too long</Text>
                </View>
                <View style={styles.CheckBoxBody}>
                  <CheckBox checked={true} color="#E5E5E5" style={{ borderRadius: 3, height: 13, width: 13, }} />
                  <Text style={styles.checkBoxText}>I was not contacted by the employer</Text>
                </View>
                <View style={styles.CheckBoxBody}>
                  <CheckBox checked={true} color="#E5E5E5" style={{ borderRadius: 3, height: 13, width: 13, }} />
                  <Text style={styles.checkBoxText}>I don’t like the job anymore</Text>
                </View>
                <View style={styles.CheckBoxBody}>
                  <CheckBox checked={true} color="#E5E5E5" style={{ borderRadius: 3, height: 13, width: 13, }} />
                  <Text style={styles.checkBoxText}>Other</Text>
                </View>
              </View>
            </View>
            <Button full style={[styles.btnCancel, { marginTop: 15 }]} title="Hide modal" onPress={this.toggleModalWithdrawApp}>
              <Text style={[styles.btnCancelText,]}>Cancel</Text>
            </Button>
          </View>
        </Modal>
        {/* Withdraw App modal*/}

        {/* modal views */}
      </View>
    );
  }
}
export default SharedJobs;

const inputStyles = {
  size: 100,
  color: '#FFFFFF',
};

const height = 40;

const styles = StyleSheet.create({
  Text1: {
    fontSize: 10,
    fontWeight: '500',
    color: "#2F2F2F",
    textAlign: 'center'
  },
  Text1_2: {
    fontSize: 10,
    fontWeight: 'bold',
    color: "#2F2F2F",
    textAlign: 'center'
  },
  Text3: {
    fontSize: 12,
    fontWeight: '500',
    color: "#FAFAFA",
    textAlign: 'center'
  },
  Text7: {
    fontSize: 12,
    fontWeight: '500',
    color: "#2F2F2F",
    textAlign: 'center'
  },
  Text4: {
    fontSize: 13,
    fontWeight: '500',
    color: "#2F2F2F",
  },
  Text4_2: {
    fontSize: 12,
    fontWeight: 'normal',
    color: "#676767",
  },
  Text5: {
    fontSize: 11,
    fontWeight: '300',
    color: "#2F2F2F",
  },
  Text2: {
    fontSize: 18,
    fontWeight: '500',
    color: "#1E93E2",
  },
  likeTabsty: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  viewPerTabSty: {
    display: 'flex',
    alignItems: 'center',
    maxWidth: 79,
    textAlign: 'center',
  },
  successCard: {
    display: 'flex',
    alignItems: 'center',
    textAlign: 'center',
    justifyContent: "center",
    backgroundColor: '#F3F3F3',
    height: 100,
    // elevation: 1,
    marginTop: 30,
  },
  successCard2: {
    display: 'flex',
    // justifyContent: 'space-between',
    textAlign: 'center',
    backgroundColor: '#F3F3F3',
    // height: 100,
    marginTop: 17,
  },
  cardinnerSty: {
    width: '100%',
    display: 'flex',
    alignItems: 'flex-end',
    textAlign: 'right',
    justifyContent: "center",
    backgroundColor: '#1E93E2',
    height: 24,
    paddingRight: 12,
    paddingLeft: 12,
  },
  morestyIdc: {
    display: 'flex',
    justifyContent: 'flex-end',
    alignItems: 'center',
    width: 20
  },
  moreBtnSty: {
    height: 20,
    width: 20,
    borderRadius: 5,
    borderWidth: 0,
    borderColor: '#1E93E2',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  xLayer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: 14,
    paddingLeft: 19,
    paddingRight: 19,
    paddingTop: 14,
  },
  // ======================= modal style ==============================
  MContainer: {
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
  },
  Mwrapper: {
    // height: 360,
    width: '98%',
    // backgroundColor: '#fff',
    padding: 15,
    display: 'flex',
    justifyContent: 'space-around',
    // alignItems: 'center'
  },
  firstView: {
    backgroundColor: '#FAFAFA',
    borderRadius: 5
  },
  actionBtn: {
    backgroundColor: '#FAFAFA',
    borderRadius: 5,
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    height: 43,
  },
  actionBtn2: {
    backgroundColor: '#FAFAFA',
    borderRadius: 5,
    elevation: 0,
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    height: 43,
  },
  checkBoxWrap: {
    borderTopWidth: 1,
    borderTopColor: '#E5E5E5',
    paddingTop: 14,
    paddingLeft: 20,
    paddingRight: 20,
    paddingBottom: 14,
    // display: 'flex',
    // justifyContent: 'center',
    // flexDirection: 'row',
    // alignItems: 'center',
    // height: 43,
  },
  actionBtnText: {
    fontSize: 12,
    color: '#1E93E2',
  },
  btnCancel: {
    backgroundColor: '#FF5964',
    borderRadius: 5,
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    height: 43,
    elevation: 0,
  },
  btnCancelText: {
    fontSize: 12,
    color: '#FAFAFA',
    fontWeight: 'bold',
  },
  checkBoxText: {
    fontSize: 11,
    color: '#2F2F2F',
    marginLeft: 20
  },
  CheckBoxBody: {
    display: 'flex',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10,
  },

  // ======================= modal style ==============================
  userFlex: {
    display: 'flex',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    alignItems: 'center',
    flexWrap: 'wrap',
    paddingLeft: 10,
    paddingRight: 10,
    // marginTop: 10,
  },
  successCard2Bottom: {
    display: 'flex',
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 19,
    paddingRight: 19,
    marginTop: 29,
    marginBottom: 17,
  },
  card2Bottom: {
    height: 28,
    backgroundColor: '#5ABC7A',
    borderRadius: 5,
    elevation: 0,
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 14,
    paddingRight: 14
  },
  userFlexImageWrap: {
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 5,
    width: '16.6%'
  },
  userFlexImage: {
    width: 32,
    height: 34,
    borderRadius: 34 / 2,
  },
});

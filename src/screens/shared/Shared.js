import React, { Component, Fragment } from 'react';
import { Text, StyleSheet, ImageBackground, Keyboard, TouchableOpacity, Alert, FlatList, ActivityIndicator } from 'react-native';
import { KeyboardAvoidingView, Image, ScrollView } from 'react-native';
import Modal from 'react-native-modal';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Checker from '../../assets/workbrookAssets/checker.png';
import {
    Container,
    Header,
    Content,
    Textarea,
    Button,
    View,
    Thumbnail,
    Form,
    Item,
    Input,
    Label,
    Badge,
    Icon,
    Footer,
    Picker,
    Item as FormItem,
    Left,
    Body,
    CardItem,
    Card,
    Right,
    ListItem,
    List,
    CheckBox,
} from 'native-base';
import {
    Kaede,
    Hoshi,
    Jiro,
    Isao,
    Madoka,
    Akira,
    Hideo,
    Kohana,
    Makiko,
    Sae,
    Fumi,
} from 'react-native-textinput-effects';
import CustomizeHeader from '../frags/CustomizeHeader';
import { SvgUri } from 'react-native-svg';


import { myspiner, temp, ShowNotification, AppInlineLoader, StageIdName } from '../../dependency/UtilityFunctions';
import Logo from '../../assets/workbrookAssets/logo.png';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import RBSheet from "react-native-raw-bottom-sheet";
import Interests from '../interests/Interests';
import Noti from "../../assets/workbrookAssets/noti.png";
import UserBl from "../../assets/workbrookAssets/userBl.png";
import ArrForward from "../../assets/workbrookAssets/arrfr.png";
import Shield from "../../assets/workbrookAssets/shield.png";
import Quest from "../../assets/workbrookAssets/quest.png";
import Info from "../../assets/workbrookAssets/info.png";
import Plus from "../../assets/workbrookAssets/plus.png";
import Remark from "../../assets/workbrookAssets/remark.png";
import Like from "../../assets/workbrookAssets/like.png";
import MoreIcon from "../../assets/workbrookAssets/more2.png";
import Profile from "../../assets/images/profile/dp2.png";
import cardPix from "../../assets/images/background/bg3_old.jpg";
import RNPickerSelect from 'react-native-picker-select';
import TagInput from 'react-native-tags-input';

import RNFetchBlob from 'react-native-fetch-blob'
import ImagePicker from 'react-native-image-picker';
import CloseModal from "../../assets/workbrookAssets/whitecloss.png";
import SuccessIcon from "../../assets/workbrookAssets/successPost.png";
import AccountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import { text } from 'react-native-communications';
import DashboardStore from '../../stores/Dashboard';
import { hasSetIntrest } from '../../auth';
import Autocomplete from 'react-native-autocomplete-input';
import MultiSelect from 'react-native-multiple-select';
import { YOUR_CLOUDINARY_PRESET, YOUR_CLOUDINARY_NAME, requestCameraPermission, YOUR_CLOUDINARY_PRESET_JobBanner } from '../../dependency/UtilityFunctions';
import EmptyListComponent from '../../StatelessScreens/EmptyListComponent';
import { borderRadius } from 'react-select/src/theme';
import style from 'react-native-datepicker/style';


items = [{
    id: '92iijs7yta',
    name: 'Ondo',
}, {
    id: 'a0s0a8ssbsd',
    name: 'Ogun',
}, {
    id: '16hbajsabsd',
    name: 'Calabar',
}, {
    id: 'sdhyaysdj',
    name: 'Kaduna',
}, {
    id: 'suudydjsjd',
    name: 'Abuja',
}];

@observer
class Shared extends Component {
    constructor(props) {
        super(props);
        this.mapRef = null;
        this.state = {
            email: '',
            password: '',
            ApplicationStagestage: '1',
            ActiveView: "Consider",
            ActiveStage: "Consider",
            selectShortListModal: false,
            sendShortListModal: false,
            confirmShortListModal: false,
            SuccessReplyModal: false,
            inviteUserModal: false,
            editShortlist: false,
            SingleApplicant:{},
            Stagecode2move:null

        }
        this.ReloadPageData = this.ReloadPageData.bind(this);
        this.ManageApplicant = this.ManageApplicant.bind(this);
        this.ChangeFetchParams = this.ChangeFetchParams.bind(this);
        this.ToggleShortlistModal = this.ToggleShortlistModal.bind(this);
        this.ToggleSendShortlistModal = this.ToggleSendShortlistModal.bind(this);
        this.ToggleconfirmShortlistModal = this.ToggleconfirmShortlistModal.bind(this);
        this.ToggleSuccessReplyModal = this.ToggleSuccessReplyModal.bind(this);
        this.ToggleinviteUserModal = this.ToggleinviteUserModal.bind(this);
        this.ToggleEditShortlist = this.ToggleEditShortlist.bind(this);
        this.ToggleMoveShortlist = this.ToggleMoveShortlist.bind(this);

    }
    ToggleShortlistModal = () => {

        this.setState({
             selectShortListModal: !this.state.selectShortListModal,
            //  SingleApplicant:item
            });
    };
    ShowMoveShortlistModal = (item) => {
console.log(item)
        this.setState({
             selectShortListModal: true,
             SingleApplicant:item
            });
    };
    ToggleEditShortlist = () => {
        
        this.setState({ editShortlist: !this.state.editShortlist });
    };
    ToggleSendShortlistModal = () => {
        this.setState({
            selectShortListModal: false,
            sendShortListModal: !this.state.sendShortListModal,

        });
    };
    ToggleconfirmShortlistModal = () => {
        this.setState({
            selectShortListModal: false,
            sendShortListModal: false,
            inviteUserModal: false,
            confirmShortListModal: !this.state.confirmShortListModal,

        });
    };
    ToggleSuccessReplyModal = () => {
        this.setState({
            selectShortListModal: false,
            sendShortListModal: false,
            confirmShortListModal: false,
            SuccessReplyModal: !this.state.SuccessReplyModal,

        });
         DashboardStore._updateParameterV2(false, "showSuccessReplyModal", "SharedApplicant");

    };
    SuggestStageToMoveApplicant = () => {
        // this.setState({
        //     selectShortListModal: false,
        //     sendShortListModal: false,
        //     confirmShortListModal: false,
        //     SuccessReplyModal: !this.state.SuccessReplyModal,

        // });
        const{SingleSharedJob}=DashboardStore.MySharedJobs;
        const{Stagecode2move,SingleApplicant}=this.state;
        let applicant_id=SingleApplicant.applicant_id;
        let jobId=SingleSharedJob.id;
        let payload={
            "job_id":parseInt(jobId),
            "stage_code":parseInt(Stagecode2move),
            // "job_stage":parseInt(Stagecode2move),
            "applicant_id":parseInt(applicant_id)
        }
        DashboardStore._updateParameterV2(payload, "SendPayload", "SharedApplicant");
DashboardStore.__SuggestStageToMoveApplicant();
    };
    ToggleinviteUserModal = () => {
        this.setState({
            selectShortListModal: false,
            inviteUserModal: !this.state.inviteUserModal,
        });
    };

    async componentDidMount() {
        const { ApplicationStagestage } = this.state;
        DashboardStore.__GetSharedApplicantList(ApplicationStagestage, 1, 50);
    }
    ReloadPageData = () => {
        const { ApplicationStagestage } = this.state;

        DashboardStore.__GetJobApplicant(ApplicationStagestage, 1, 10);

    };
    ManageApplicant = (item) => {

        DashboardStore._updateParameterV2(item, "singleApplicant", "JobApplicants");

        this.props.navigation.navigate("ViewApplicant")
    };
    ChangeFetchParams = (params, params2) => {
        this.setState({
            ApplicationStagestage: params,
            ActiveView: params2,
        })
        // DashboardStore.__GetJobApplicant(params, 1, 10);
        DashboardStore.__GetSharedApplicantList(params, 1, 50);

    };
    ToggleMoveShortlist = (params,stageCode) => {
        this.setState({
            ActiveStage: params,
            Stagecode2move:stageCode,
        })
        // DashboardStore.__GetJobApplicant(params, 1, 10);

    };
    render() {
        
        const { SharedApplicant } = DashboardStore;
        const { SingleApplicant } = this.state;
        let AplicantName = SingleApplicant.first_name == null || SingleApplicant.last_name == null ? SingleApplicant.username : `${SingleApplicant.first_name}  ${SingleApplicant.last_name} `

        let ErrorComponentDisplay = null;

        if (DashboardStore.isProcessing === true) {
            ErrorComponentDisplay = AppInlineLoader(DashboardStore.isProcessing)

        }
        else {
            ErrorComponentDisplay = <EmptyListComponent
                message={DashboardStore.SharedApplicant.ErrorMessage}
                onclickhandler={this.ReloadPageData}
            />
        }

        let JobOffersListing =
            <FlatList
                data={SharedApplicant.SharedApplicantList}
                // Render Items
                renderItem={({ item, index }) => {
                    //   let Applicants=item.applicants
                    // let posterName=  `${poster.first_name}  ${poster.last_name} `
                    // let poster = item.created_by_profile
                    let posterName = item.first_name == null || item.last_name == null ? item.username : `${item.first_name}  ${item.last_name} `

                    return (

               
<List style={{ borderBottomWidth: 1, borderBottomColor: '#E5E5E5', paddingBottom: 15 }}>
<ListItem Button avatar style={{ display: 'flex', justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center', }}
    // onPress={() => { this.props.navigation.navigate("ViewApplicant") }}
>
    <Left style={styles.leftAlignSty}>
        <Text style={styles.numberCount}>3</Text>
        <Thumbnail style={styles.applicantImg} source={{ uri: item.image_url }} />
    </Left>
    <Body style={[styles.leftAlignSty, { borderBottomWidth: 0, marginBottom: -3 }]}>
        <Text style={styles.cardTextJob4}>{posterName}</Text>
        {/* {item.isViewed === "0" ?
        <View style={styles.IndicatorSty}>
            <Text style={[styles.cardText2, { fontWeight: "500" }]}>New</Text>
        </View>
        :
        null
                            } */}
 
        <View style={styles.IndicatorSty}>
                        <Text style={[styles.cardText2, { fontWeight: "500" }]}>{StageIdName(item.move_to)}</Text>
        </View>
     
    </Body>

    <Right>
        {
            // this.state.editShortlist === true ?
                <TouchableOpacity onPress={()=>this.ShowMoveShortlistModal(item)}>
                    <Text style={styles.cardTextJob_12}>Move</Text>
                </TouchableOpacity>
                // :
                // <Fragment></Fragment>
        }

    </Right>


</ListItem>
</List>


                    )
                }}
                // Item Key
                keyExtractor={(item, index) => String(index)}
                // Header (Title)
                // ListHeaderComponent={this.renderHeader}
                // Footer (Activity Indicator)
                // ListFooterComponent={this.renderFooter()}
                // On End Reached (Takes a function)
                onEndReached={this.retrieveMore}
            // How Close To The End Of List Until Next Data Request Is Made
            // onEndReachedThreshold={0}
            // Refreshing (Set To True When End Reached)
            // refreshing={this.state.refreshing}
            />

        return (

            <Container style={styles.container}>
                <CustomizeHeader
                    leftside='WhiteArr'
                    title='Showing All Shared Applicants'
                    rightside='empty'
                    statedColor='#1E93E2'
                    icon='md-close'
                // savehandler={this.proceednow}
                />
                <View style={styles.likeTabSty}>


                    <TouchableOpacity
                        onPress={() => { this.ChangeFetchParams('1', 'Consider') }}

                    >
                        <Text style={[this.state.ActiveView == "Consider" ? styles.activeView : styles.cardText2, { marginRight: 3 }]}>Consider</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => { this.ChangeFetchParams('2', 'Test') }}
                    >
                        <Text style={[this.state.ActiveView == "Test" ? styles.activeView : styles.cardText2, { marginRight: 3 }]}>Test</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => { this.ChangeFetchParams('3', 'Interview') }}

                    >
                        <Text style={[this.state.ActiveView == "Interview" ? styles.activeView : styles.cardText2, { marginRight: 3 }]}>Interview</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => { this.ChangeFetchParams('5', 'Rejected') }}

                    >
                        <Text style={[this.state.ActiveView == "Rejected" ? styles.activeView : styles.cardText2, { marginRight: 3 }]}>Rejected</Text>
                    </TouchableOpacity>
                </View>

                <ScrollView
                    contentContainerStyle={{
                        padding: 35,
                    }}
                >
                    <View>
                        <Text style={styles.cardTextJob3}>
                            {this.state.ActiveView} Applicant(s)
                            </Text>

                    </View>
                    {
                        SharedApplicant.SharedApplicantList.length > 0 ?
                            JobOffersListing
                            :
                            ErrorComponentDisplay

                    }



                    {/* <List style={{ borderBottomWidth: 1, borderBottomColor: '#E5E5E5', paddingBottom: 15 }}>
                        <ListItem Button avatar style={{ display: 'flex', justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center', }}
                            onPress={() => { this.props.navigation.navigate("ViewApplicant") }}
                        >
                            <Left style={styles.leftAlignSty}>
                                <Text style={styles.numberCount}>1</Text>
                                <Thumbnail style={styles.applicantImg} source={Profile} />
                            </Left>
                            <Body style={[styles.leftAlignSty, { borderBottomWidth: 0, marginBottom: -3 }]}>
                                <Text style={styles.cardTextJob4}>Kumar Pratik</Text>
                                <View style={styles.IndicatorSty}>
                                    <Text style={[styles.cardText2, { fontWeight: "500" }]}>New</Text>
                                </View>
                            </Body>

                        </ListItem>
                    </List>
                    <List style={{ borderBottomWidth: 1, borderBottomColor: '#E5E5E5', paddingBottom: 15 }}>
                        <ListItem Button avatar style={{ display: 'flex', justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center', }}
                            onPress={() => { this.props.navigation.navigate("ViewApplicant") }}
                        >
                            <Left style={styles.leftAlignSty}>
                                <Text style={styles.numberCount}>2</Text>
                                <Thumbnail style={styles.applicantImg} source={Profile} />
                            </Left>
                            <Body style={[styles.leftAlignSty, { borderBottomWidth: 0, marginBottom: -3 }]}>
                                <Text style={styles.cardTextJob4}>Kumar Pratik</Text>
                                <View style={styles.IndicatorSty}>
                                    <Text style={[styles.cardText2, { fontWeight: "500" }]}>New</Text>
                                </View>
                            </Body>
                            <Right>
                                {
                                    this.state.editShortlist == true ?
                                        <TouchableOpacity onPress={this.ToggleShortlistModal}>
                                            <Text style={styles.cardTextJob_12}>Move</Text>
                                        </TouchableOpacity>
                                        :
                                        <Fragment></Fragment>
                                }

                            </Right>


                        </ListItem>
                    </List>
                    <List style={{ borderBottomWidth: 1, borderBottomColor: '#E5E5E5', paddingBottom: 15 }}>
                        <ListItem Button avatar style={{ display: 'flex', justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center', }}
                            onPress={() => { this.props.navigation.navigate("ViewApplicant") }}
                        >
                            <Left style={styles.leftAlignSty}>
                                <Text style={styles.numberCount}>3</Text>
                                <Thumbnail style={styles.applicantImg} source={Profile} />
                            </Left>
                            <Body style={[styles.leftAlignSty, { borderBottomWidth: 0, marginBottom: -3 }]}>
                                <Text style={styles.cardTextJob4}>Kumar Pratik</Text>
                                <View style={styles.IndicatorSty}>
                                    <Text style={[styles.cardText2, { fontWeight: "500" }]}>New</Text>
                                </View>
                            </Body>

                            <Right>
                                {
                                    this.state.editShortlist == true ?
                                        <TouchableOpacity onPress={this.ToggleShortlistModal}>
                                            <Text style={styles.cardTextJob_12}>Move</Text>
                                        </TouchableOpacity>
                                        :
                                        <Fragment></Fragment>
                                }

                            </Right>


                        </ListItem>
                    </List> */}

                </ScrollView>
                <View style={styles.sendShortlst}>
                    <Button style={[styles.btnShortList, { backgroundColor: this.state.editShortlist == true ? "#1E93E2" : "#5ABC7A" }]}
                        onPress={this.ToggleEditShortlist}
                    >
                        {
                            this.state.editShortlist == true ?
                                <Text style={styles.shortListTxt}> Done</Text>
                                :
                                <Text style={styles.shortListTxt}> Edit Shortlist</Text>
                        }
                    </Button>
                </View>
                <Modal style={styles.MContainer} isVisible={this.state.selectShortListModal}>
                    <Button onPress={this.ToggleShortlistModal} style={{ backgroundColor: "transparent", position: 'absolute', right: 10, top: 40, padding: 20, elevation: 0 }}>
                        <SvgUri
                            width="19px"
                            height="19px"
                            uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593711047/Mobile_Assets/closeWht_oohxti.svg" />
                    </Button>
                    <View style={[styles.Mwrapper, { height: 380 }]}>
                        <View style={{ paddingLeft: 21, paddingRight: 21, display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={[styles.stageTitle, {marginBottom: 28}]}>
                                Which Stage do you want  to
                    move {AplicantName}?
                            </Text>
                            <Button full style={[styles.btnMoveShortList, { backgroundColor: this.state.ActiveStage == "Consider" ? "#FFF" : "#FC9F13", borderColor: "#FC9F13" }]}
                                onPress={() => { this.ToggleMoveShortlist('Consider',1) }}
                            >
                                <Text style={[styles.shortListTxt, { color: this.state.ActiveStage == "Consider" ? "#FC9F13" : "#FFF" }]}> Consider</Text>
                            </Button>
                            <Button full style={[styles.btnMoveShortList, { backgroundColor: this.state.ActiveStage == "Test" ? "#FFF" : "#5ABC7A", borderColor: "#5ABC7A" }]}
                                onPress={() => { this.ToggleMoveShortlist('Test',2) }}
                            >
                                <Text style={[styles.shortListTxt, { color: this.state.ActiveStage == "Test" ? "#5ABC7A" : "#FFF" }]}> Test</Text>
                            </Button>

                            <Button full style={[styles.btnMoveShortList, { backgroundColor: this.state.ActiveStage == "Interview" ? "#FFF" : "#3C58BF", borderColor: "#3C58BF" }]}
                                onPress={() => { this.ToggleMoveShortlist('Interview',3) }}
                            >
                                <Text style={[styles.shortListTxt, { color: this.state.ActiveStage == "Interview" ? "#3C58BF" : "#FFF" }]}> Interview</Text>
                            </Button>
                            
                            <Button full style={[styles.btnMoveShortList, { backgroundColor: this.state.ActiveStage == "Rejected" ? "#FFF" : "#FF5964", borderColor: "#FF5964" }]}
                                onPress={() => { this.ToggleMoveShortlist('Rejected',5) }}
                            >
                                <Text style={[styles.shortListTxt, { color: this.state.ActiveStage == "Rejected" ? "#FF5964" : "#FFF" }]}> Rejected</Text>
                            </Button>
                            
                            {/* <View style={styles.sendShortlst}> */}
                            <TouchableOpacity style={styles.btnNext}
                                onPress={this.ToggleconfirmShortlistModal}
                            >
                                <Text style={[styles.cardTextJob18_2]}>Next</Text>
                            </TouchableOpacity>
                            {/* </View> */}


                        </View>

                    </View>
                </Modal>

                <Modal style={styles.MContainer} isVisible={this.state.sendShortListModal}>
                    <Button onPress={this.ToggleSendShortlistModal} style={{ backgroundColor: "transparent", position: 'absolute', right: 10, top: 40, padding: 20, elevation: 0 }}>
                        <SvgUri
                            width="19px"
                            height="19px"
                            uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593711047/Mobile_Assets/closeWht_oohxti.svg" />
                    </Button>
                    <View style={[styles.Mwrapper, { height: 380 }]}>
                        <View style={{ paddingLeft: 18, paddingRight: 18, display: 'flex', justifyContent: 'center', alignItems: 'center', width: "100%" }}>
                            {/* <Label style={styles.cardTextJob2}>Experience</Label> */}
                            <View style={{ width: "100%", marginTop: 10 }}>
                                <Text style={styles.cardTextJob18}>
                                    Send Shortlist to Jane Dokaszhuk?
                                </Text>
                                <View style={styles.tagTitle}>
                                    <Text style={styles.cardText2_2}>Consider</Text>
                                </View>
                            </View>
                            <ScrollView style={{ width: "100%", height: "50%" }}>
                                <List style={{ borderBottomWidth: 1, borderBottomColor: '#E5E5E5', paddingBottom: 15, width: "100%" }}>
                                    <ListItem Button avatar style={{ display: 'flex', justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center', }}
                                    // onPress={() => { this.props.navigation.navigate("ViewApplicant") }}
                                    >
                                        <Left style={styles.leftAlignSty}>

                                            <Thumbnail style={styles.applicantImg} source={Profile} />
                                        </Left>
                                        <Body style={[styles.leftAlignSty, { borderBottomWidth: 0, marginBottom: -3 }]}>
                                            <Text style={styles.cardTextJob4}>Kumar Pratik</Text>

                                        </Body>

                                    </ListItem>
                                </List>
                            </ScrollView>
                            <View style={styles.ShortlistGroupBtn}>
                                <TouchableOpacity style={[styles.ShortlistBtn,]}
                                    onPress={this.ToggleconfirmShortlistModal}
                                >
                                    <Text style={[styles.cardTextJob4_1,]}>Send</Text>
                                </TouchableOpacity>

                                <TouchableOpacity style={[styles.ShortlistBtn, { backgroundColor: "#FF5964" }]}
                                    onPress={this.ToggleSendShortlistModal}
                                >
                                    <Text style={[styles.cardTextJob4_1,]}>Cancel</Text>
                                </TouchableOpacity>
                            </View>


                        </View>

                    </View>
                </Modal>

                <Modal style={styles.MContainer} isVisible={this.state.confirmShortListModal}>
                    <Button onPress={this.ToggleconfirmShortlistModal} style={{ backgroundColor: "transparent", position: 'absolute', right: 10, top: 140, padding: 20, elevation: 0 }}>
                        <SvgUri
                            width="19px"
                            height="19px"
                            uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593711047/Mobile_Assets/closeWht_oohxti.svg" />
                    </Button>
                    <View style={[styles.Mwrapper, { height: 215 }]}>
                        <View style={{ paddingLeft: 18, paddingRight: 18, display: 'flex', justifyContent: 'center', alignItems: 'center', width: "100%" }}>
                            {/* <Label style={styles.cardTextJob2}>Experience</Label> */}
                            <View style={{ width: "100%", marginTop: 10, display: "flex", alignItems:'center' }}>
                                <Text style={[styles.cardTextJob18,]}>
                                    Are You sure
                                </Text>
                                <Text style={[styles.cardTextJob18,]}>
                                You want to move {AplicantName} to
                                </Text>
                               

                            </View>
                            <Button full style={[styles.btnMoveShortList, {display: this.state.ActiveStage == "Consider" ? "flex" : "none", backgroundColor: "#FC9F13", borderColor: "#FC9F13" }]}
                            >
                                <Text style={[styles.shortListTxt, { color : "#FFF" }]}>
                                     Consider
                                </Text>
                            </Button>

                            <Button full style={[styles.btnMoveShortList, { display: this.state.ActiveStage == "Test" ? "flex" : "none", backgroundColor: "#5ABC7A", borderColor: "#5ABC7A" }]}
                            >
                                <Text style={[styles.shortListTxt, { color:"#FFF" }]}> Test</Text>
                            </Button>

                            <Button full style={[styles.btnMoveShortList, { display: this.state.ActiveStage == "Interview" ? "flex" : "none", backgroundColor: "#3C58BF", borderColor: "#3C58BF" }]}
                            >
                                <Text style={[styles.shortListTxt, { color: "#FFF" }]}>
                                     Interview
                                </Text>
                            </Button>
                            
                            <Button full style={[styles.btnMoveShortList, { display: this.state.ActiveStage == "Rejected" ?  "flex" : "none", backgroundColor: "#FF5964", borderColor: "#FF5964" }]}
                            >
                                <Text style={[styles.shortListTxt, { color: "#FFF" }]}>
                                     Rejected
                                </Text>
                            </Button>
                            

                            <View style={styles.ShortlistGroupBtn}>
                                <TouchableOpacity style={[styles.ShortlistBtn,]}
                                    onPress={this.SuggestStageToMoveApplicant}
                                >
                                    <Text style={[styles.cardTextJob4_1,]}>Send</Text>
                                </TouchableOpacity>

                                <TouchableOpacity style={[styles.ShortlistBtn, { backgroundColor: "#FF5964" }]}
                                    onPress={this.ToggleconfirmShortlistModal}
                                >
                                    <Text style={[styles.cardTextJob4_1,]}>Cancel</Text>
                                </TouchableOpacity>
                            </View>


                        </View>

                    </View>
                </Modal>

                <Modal style={styles.MContainer} isVisible={DashboardStore.SharedApplicant.showSuccessReplyModal}>
                    <Button onPress={this.ToggleSuccessReplyModal} style={{ backgroundColor: "transparent", position: 'absolute', right: 10, top: 140, padding: 20, elevation: 0 }}>
                        <SvgUri
                            width="19px"
                            height="19px"
                            uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593711047/Mobile_Assets/closeWht_oohxti.svg" />
                    </Button>
                    <View style={[styles.Mwrapper, { height: 215 }]}>
                        <View style={{ paddingLeft: 18, paddingRight: 18, display: 'flex', justifyContent: 'center', alignItems: 'center', width: "100%" }}>
                            <SvgUri
                                style={{ marginBottom: 17 }}
                                width="79px"
                                height="79px"
                                uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593736755/Mobile_Assets/success_y53teq.svg"
                            />

                            <View style={{ width: "100%", marginTop: 10 }}>
                                <Text style={styles.cardTextJob18}>
                                You have moved {AplicantName} to
                                </Text>

                            </View>
                            <Button full style={[styles.btnMoveShortList, {display: this.state.ActiveStage == "Consider" ? "flex" : "none", backgroundColor: "#FC9F13", borderColor: "#FC9F13" }]}
                            >
                                <Text style={[styles.shortListTxt, { color : "#FFF" }]}>
                                     Consider
                                </Text>
                            </Button>

                            <Button full style={[styles.btnMoveShortList, { display: this.state.ActiveStage == "Test" ? "flex" : "none", backgroundColor: "#5ABC7A", borderColor: "#5ABC7A" }]}
                            >
                                <Text style={[styles.shortListTxt, { color:"#FFF" }]}> Test</Text>
                            </Button>

                            <Button full style={[styles.btnMoveShortList, { display: this.state.ActiveStage == "Interview" ? "flex" : "none", backgroundColor: "#3C58BF", borderColor: "#3C58BF" }]}
                            >
                                <Text style={[styles.shortListTxt, { color: "#FFF" }]}>
                                     Interview
                                </Text>
                            </Button>
                            
                            <Button full style={[styles.btnMoveShortList, { display: this.state.ActiveStage == "Rejected" ?  "flex" : "none", backgroundColor: "#FF5964", borderColor: "#FF5964" }]}
                            >
                                <Text style={[styles.shortListTxt, { color: "#FFF" }]}>
                                     Rejected
                                </Text>
                            </Button>
                            


                        </View>

                    </View>
                </Modal>
                
                <Modal style={styles.MContainer} isVisible={this.state.inviteUserModal}>
                    <Button onPress={this.ToggleinviteUserModal} style={{ backgroundColor: "transparent", position: 'absolute', right: 10, top: 100, padding: 20, elevation: 0 }}>
                        <SvgUri
                            width="19px"
                            height="19px"
                            uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593711047/Mobile_Assets/closeWht_oohxti.svg" />
                    </Button>
                    <View style={[styles.Mwrapper, { height: 270 }]}>
                        <View style={{ paddingLeft: 18, paddingRight: 18, display: 'flex', justifyContent: 'center', alignItems: 'center', width: "100%" }}>
                            <View style={{ width: "100%", marginTop: 10 }}>
                                <Text style={styles.cardTextJob18}>
                                    Invite New User
                                </Text>
                            </View>
                            <Item regular style={styles.inputStyleWrap2}>
                                <Input style={styles.inputStyling}
                                    placeholder="Full Name" />
                            </Item>
                            <Item regular style={styles.inputStyleWrap2}>
                                <Input style={styles.inputStyling}
                                    placeholder="Email"
                                />
                            </Item>

                            <View style={[styles.ShortlistGroupBtn, { justifyContent: "center" }]}>
                                <TouchableOpacity style={[styles.ShortlistBtn,]}
                                    onPress={this.ToggleconfirmShortlistModal}
                                >
                                    <Text style={[styles.cardTextJob4_1,]}>Send</Text>
                                </TouchableOpacity>

                            </View>


                        </View>

                    </View>
                </Modal>



            </Container>


        );
    }
}
export default Shared;

const styles = StyleSheet.create({
    container: {
        backgroundColor: "white",
        display: "flex",
        flex: 1,
        justifyContent: "flex-start",
    },

    rmPadding: {
        paddingTop: 0,
        paddingRight: 0,
        paddingBottom: 0,
        paddingLeft: 0,
    },
    rmPaddingLR: {
        paddingLeft: 0,
        paddingRight: 0,
    },

    cardTextJob1: {
        color: "#2F2F2F",
        fontSize: 36,
        fontWeight: '600',

    },
    cardTextJob2: {
        color: "#BCBCBC",
        fontSize: 14,
        fontWeight: '600',
        marginBottom: 5,
    },
    cardTextJob2_2: {
        color: "#fff",
        fontSize: 14,
        fontWeight: '600',
    },
    cardTextJob3: {
        color: "#2F2F2F",
        fontSize: 13,
        fontWeight: '500',
    },
    cardTextJob4: {
        fontSize: 12,
        color: '#2F2F2F',
        fontWeight: '600'
    },
    cardTextJob_12: {
        fontSize: 12,
        color: '#1E93E2',
        fontWeight: '600'
    },
    cardTextJob18: {
        fontSize: 18,
        color: '#000000',
        fontWeight: '600',
        width: "100%",
        textAlign: "center",
        marginBottom: 10,
    },
    cardTextJob18_2: {
        fontSize: 18,
        color: '#A7A7A7',
        fontWeight: '600',
        
    },

    cardTextJob5: {
        color: "#2F2F2F",
        fontSize: 10,
        fontWeight: '500',
    },
    cardTextJob7: {
        color: "#1E93E2",
        fontSize: 11,
        fontWeight: '500',
    },
    cardTextJob4_1: {
        color: "#FAFAFA",
        fontSize: 12,
        fontWeight: 'bold',
    },

    cardTextJob6: {
        color: "#1E93E2",
        fontSize: 11,
        fontWeight: '500',
    },
    cardText2: {
        color: "#FAFAFA",
        fontSize: 11,
    },
    cardText2_2: {
        color: "#FAFAFA",
        fontSize: 11,
        fontWeight: 'bold',
    },


    cardText1: {
        color: "#1E93E2",
        fontSize: 7,
    },
    bargeCount: {
        height: 20,
        width: 20,
        borderRadius: 20 / 2,
        backgroundColor: "#fff",
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    likeTabSty: {
        backgroundColor: "#1E93E2",
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        paddingTop: 30,
        paddingBottom: 10,
    },
    ShortlistGroupBtn: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingTop: 10,
        paddingBottom: 10,
        width: "100%"
    },
    ShortlistBtn: {
        backgroundColor: "#5ABC7A",
        display: 'flex',
        width: 100,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 12,
        paddingBottom: 12,
        elevation: 0,
        borderRadius: 5,
    },
    sendShortlst: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 12,
        paddingRight: 12,
    },
    btnShortList: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 17,
        paddingBottom: 17,
        paddingLeft: 20,
        paddingRight: 20,
        backgroundColor: "#5ABC7A",
        borderRadius: 10,
        elevation: 0,
    },
    btnMoveShortList: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        borderWidth: 1,
        paddingTop: 15,
        paddingBottom: 15,
        paddingLeft: 20,
        paddingRight: 20,
        backgroundColor: "#5ABC7A",
        borderRadius: 3,
        elevation: 0,
        marginBottom: 13,
    },
    stageTitle: {
        fontSize: 18,
        color: "#000000",
        fontWeight: "600",
        textAlign: "center",
    },
    tagTitle: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        paddingTop: 9,
        paddingBottom: 9,
        paddingLeft: 20,
        paddingRight: 20,
        backgroundColor: "#FC9F13",
        borderRadius: 3,
        elevation: 0,
    },
    shortListTxt: {
        fontSize: 13,
        color: '#fff',
        fontWeight: "bold"
    },
    inputStyleWrap: {
        height: 38,
        backgroundColor: '#E5E5E5',
        borderRadius: 5,
    },
    inputStyleWrap2: {
        height: 48,
        backgroundColor: '#E7E7E7',
        borderRadius: 5,
        marginBottom: 15,
    },
    inputStyling: {
        fontSize: 14,
        color: '#424051',
    },
    btInviteNew: {
        display: "flex",
        width: "100%",
        flexDirection: "row",
        justifyContent: "flex-start",
        alignItems: "center"
    },
    btnNext: {
        display: "flex",
        width: "100%",
        flexDirection: "row",
        justifyContent: "flex-end",
        alignItems: "center"
    },
    headerWrap: {
        marginBottom: 13,
        marginTop: 20,
        paddingLeft: 25,
        paddingRight: 25,
    },
    serachbtnSty: {
        height: 54,
        borderRadius: 4,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#5ABC7A',
        marginTop: 24
    },
    ViewBtnSty: {
        height: 23,
        width: 50,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#1E93E2',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    btnStyIdc: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    applicantImg: {
        height: 31,
        width: 32,
        borderRadius: 32 / 2,
    },
    numberCount: {
        fontWeight: '600',
        fontSize: 12,
        color: '#1E93E2',
        marginRight: 11
    },
    leftAlignSty: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
    },
    IndicatorSty: {
        // width: 7,
        // height: 7,
        borderRadius: 3,
        backgroundColor: "#FF5964",
        // marginTop: -15,
        marginRight: -5,
        paddingBottom: 3,
        paddingTop: 3,
        paddingLeft: 7,
        paddingRight: 7,
        marginLeft: 3,
    },
    activeView: {
        color: "#FAFAFA",
        fontSize: 11,
        fontWeight: 'bold',
    },

    // Modal style =============================================================
    MContainer: {
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
    },
    Mwrapper: {
        height: 510,
        width: '90%',
        backgroundColor: '#fff',
        // padding: 15,
        display: 'flex',
        justifyContent: 'space-around',
        borderRadius: 10
        // alignItems: 'center'
    },
    homeModalwrap: {
        // height: 519,
        width: '90%',
        backgroundColor: '#fff',
        // padding: 15,
        // display: 'flex',
        justifyContent: 'space-between',
        // alignItems: 'center'
    },
    homeModalHeader: {
        height: 89,
        width: '100%',
        backgroundColor: '#1E93E2',
        padding: 15,
        display: 'flex',
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    homeModalContentWrap: {
        width: '100%',
        backgroundColor: '#fff',
        padding: 35,
        paddingBottom: 20,
        display: 'flex',
    },
    homeModalwrapText1: {
        fontSize: 60,
        fontSize: 16,
        color: '#FAFAFA',
    },
    btnSave: {
        height: 54,
        backgroundColor: '#1E93E2',
        elevation: 0,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    // Modal Style ends=========================================================



});

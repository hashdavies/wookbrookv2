import React, { Component, Fragment } from 'react';
import { Text, StyleSheet, ImageBackground, Keyboard, TouchableOpacity, FlatList } from 'react-native';
import { KeyboardAvoidingView, Image, ScrollView } from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons';
import Checker from '../../assets/workbrookAssets/checker.png';
import {
    Container,
    Header,
    Content,
    Textarea,
    Button,
    View,
    Thumbnail,
    Form,
    Item,
    Input,
    Label,
    Badge,
    Icon,
    Footer,
    Picker,
    Item as FormItem,
    Left,
    Body,
    CardItem,
    Card,
    Right,
    ListItem,
    List,
} from 'native-base';
import {
    Kaede,
    Hoshi,
    Jiro,
    Isao,
    Madoka,
    Akira,
    Hideo,
    Kohana,
    Makiko,
    Sae,
    Fumi,
} from 'react-native-textinput-effects';
import CustomizeHeader from '../frags/CustomizeHeader';

import { myspiner, AppInlineLoader } from '../../dependency/UtilityFunctions';
import Logo from '../../assets/workbrookAssets/logo.png';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import RBSheet from "react-native-raw-bottom-sheet";
import Interests from '../interests/Interests';
import Plus from "../../assets/workbrookAssets/plus.png";
import Remark from "../../assets/workbrookAssets/remark.png";
import Like from "../../assets/workbrookAssets/like.png";
import MoreIcon from "../../assets/workbrookAssets/more2.png";
import Profile from "../../assets/images/profile/dp2.png";
import cardPix from "../../assets/images/background/bg3_old.jpg";

import AccountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import { text } from 'react-native-communications';
import DashboardStore from '../../stores/Dashboard';
import { hasSetIntrest } from '../../auth';
import EmptyListComponent from '../../StatelessScreens/EmptyListComponent';


@observer
class Notification extends Component {
    constructor(props) {
        super(props);
        this.mapRef = null;
        this.state = {
            email: '',
            password: '',
            errorloading: false,
        }
        // this.SetCheckers = this.SetCheckers.bind(this)
    }
    async componentDidMount() {
        let status = await hasSetIntrest();
        // alert(status)
        DashboardStore._UpdateUserSetIntrest(status);
        DashboardStore.__GetNotification(1, 30);

    }

    render() {
        const { SystemNotification } = DashboardStore;
        let ErrorComponentDisplay = null;

        if (DashboardStore.isProcessing === true) {
            ErrorComponentDisplay = AppInlineLoader(DashboardStore.isProcessing)

        }
        else {
            ErrorComponentDisplay = <EmptyListComponent
                message={DashboardStore.JobOppurtunity.ErrorMessage}
                onclickhandler={this.ReloadPageData}
            />
        }

        let MyNotification =
            <FlatList
                // Data
                data={SystemNotification.MyNotification}
                // Render Items
                renderItem={({ item }) => {
                    return (
                        <List style={{ borderBottomWidth: 1, borderColor: "#E5E5E5" }} >
                            <ListItem thumbnail>
                                <Left>
                                    <Image small source={{ uri: item.image_url }} style={{ width: 40, height: 40, borderRadius: 40 / 2 }} />
                                </Left>
                                <Body style={{ borderBottomWidth: 0, }}>
                                    <Text style={styles.cardTextJob7}>

                                        {item.message}                                </Text>
                                </Body>
                                {/* <Right style={{borderBottomWidth: 0,}}>
                                <Button transparent style={{marginLeft: 10}}>
                                    <Image source={MoreIcon} style={{ width: 14, height: 4 }} />
                                </Button>
                            </Right> */}
                            </ListItem>
                        </List>


                    )
                }}
                // Item Key
                keyExtractor={(item, index) => String(index)}
                // Header (Title)
                // ListHeaderComponent={this.renderHeader}
                // Footer (Activity Indicator)
                // ListFooterComponent={this.renderFooter()}
                // On End Reached (Takes a function)
                onEndReached={this.retrieveMore}
            // How Close To The End Of List Until Next Data Request Is Made
            // onEndReachedThreshold={0}
            // Refreshing (Set To True When End Reached)
            // refreshing={this.state.refreshing}
            />


        return (

            <Container style={styles.container}>
                 <CustomizeHeader
                        leftside='WhiteArr'
                        title='Notifications'
                        rightside='RhsDrawerWhite'
                        statedColor='#1E93E2'
                        icon='md-close'
                        // savehandler={this.proceednow}
                    />
                {/* <View style={styles.topBlue}>
                    <TouchableOpacity onPress={() => { this.props.navigation.toggleDrawer() }}>
                        <FontAwesome name="bars" style={{ color: "#fff", fontSize: 30 }} />
                    </TouchableOpacity>
                   
                </View> */}

                <ScrollView>
                    <CardItem style={{ display: 'flex', flexDirection: 'column', alignItems: 'flex-start', backgroundColor: '#ffffff', borderBottomWidth: 1, borderColor: "#E5E5E5" }}>
                        <Text style={styles.cardTextJob4_1}>Recent</Text>

                    </CardItem>
                    <View style={[{ padding: 0, backgroundColor: '#ffffff', }]}>
                        {
                            SystemNotification.MyNotification.length > 0 ?

                                MyNotification
                                :
                                ErrorComponentDisplay
                        }

                        {/* <List style={{borderBottomWidth: 1, borderColor: "#E5E5E5"}}>
                                <ListItem thumbnail>
                                    <Left>
                                        <Image small source={Profile} style={{ width: 40, height: 40, borderRadius: 40 / 2 }} />
                                    </Left>
                                    <Body  style={{borderBottomWidth: 0,}}>
                                        <Text style={styles.cardTextJob7}>
                                            Musa Batoille just refered you to a new job in Abuja - Website Developer at CBT Hub.
                                        </Text>
                                    </Body>
                                    <Right style={{borderBottomWidth: 0,}}>
                                        <Button transparent style={{marginLeft: 10}}>
                                            <Image source={MoreIcon} style={{ width: 14, height: 4 }} />
                                        </Button>
                                    </Right>
                                </ListItem>
                            </List>
                           */}



                    </View>
                </ScrollView>
            </Container>


        );
    }
}
export default Notification;

const styles = StyleSheet.create({
    container: {
        backgroundColor: "#F3F3F3",
        display: "flex",
        flex: 1,
        justifyContent: "flex-start",
    },
    topBlue: {
        padding: 15,
        height: 80,
        backgroundColor: "#1E93E2",
        display: 'flex',
        justifyContent: 'center',
        alignItems: "flex-end"
    },
    // topBlue: {
    //     // padding: 15,
    //     height: 106,
    //     backgroundColor: "#1E93E2",
    //     display: 'flex',
    //     justifyContent: 'center',
    // },
    rmPadding: {
        paddingTop: 0,
        paddingRight: 0,
        paddingBottom: 0,
        paddingLeft: 0,
    },
    rmPaddingLR: {
        paddingLeft: 0,
        paddingRight: 0,
    },

    cardTextJob1: {
        color: "#2F2F2F",
        fontSize: 11,
    },
    cardTextJob2: {
        color: "#676767",
        fontSize: 9,
        fontWeight: '300',
    },
    cardTextJob3: {
        color: "#2F2F2F",
        fontSize: 10,
        fontWeight: '500',
    },
    cardTextJob4: {
        color: "#1E93E2",
        fontSize: 11,
        fontWeight: '500',
    },
    cardTextJob4_1: {
        color: "#2F2F2F",
        fontSize: 12,
        fontWeight: '500',
    },
    cardTextJob5: {
        color: "#1E93E2",
        fontSize: 13,
        fontWeight: '600',
    },
    cardTextJob7: {
        fontSize: 12,
        color: '#2F2F2F',
    },


});

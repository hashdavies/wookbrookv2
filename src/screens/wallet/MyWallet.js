import React, { Component, Fragment } from 'react';
import { Text, StyleSheet, ImageBackground, Keyboard, TouchableOpacity, FlatList } from 'react-native';
import { KeyboardAvoidingView, Image, ScrollView } from 'react-native';
import ToggleSwitch from 'toggle-switch-react-native'
import Ionicons from 'react-native-vector-icons/Ionicons';
import Checker from '../../assets/workbrookAssets/checker.png';
import {
    Container,
    Header,
    Content,
    Textarea,
    Button,
    View,
    Thumbnail,
    Form,
    Item,
    Input,
    Label,
    Badge,
    Icon,
    Footer,
    Picker,
    Item as FormItem,
    Left,
    Body,
    CardItem,
    Card,
    Right,
    ListItem,
    List,
} from 'native-base';
import {
    Kaede,
    Hoshi,
    Jiro,
    Isao,
    Madoka,
    Akira,
    Hideo,
    Kohana,
    Makiko,
    Sae,
    Fumi,
} from 'react-native-textinput-effects';
import CustomizeHeader from '../frags/CustomizeHeader';

import { myspiner, AppInlineLoader } from '../../dependency/UtilityFunctions';
import Logo from '../../assets/workbrookAssets/logo.png';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import RBSheet from "react-native-raw-bottom-sheet";
import Interests from '../interests/Interests';
import Noti from "../../assets/workbrookAssets/noti.png";
import UserBl from "../../assets/workbrookAssets/userBl.png";
import ArrForward from "../../assets/workbrookAssets/arrfr.png";
import Shield from "../../assets/workbrookAssets/shield.png";
import Quest from "../../assets/workbrookAssets/quest.png";
import Info from "../../assets/workbrookAssets/info.png";
import Plus from "../../assets/workbrookAssets/plus.png";
import Remark from "../../assets/workbrookAssets/remark.png";
import Like from "../../assets/workbrookAssets/like.png";
import MoreIcon from "../../assets/workbrookAssets/more2.png";
import Profile from "../../assets/images/profile/dp2.png";
import cardPix from "../../assets/images/background/bg3_old.jpg";
import CloseModal from "../../assets/workbrookAssets/whitecloss.png";
import Modal from 'react-native-modal';
import AccountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import { text } from 'react-native-communications';
import DashboardStore from '../../stores/Dashboard';
import { hasSetIntrest } from '../../auth';
import { SvgUri } from 'react-native-svg';
import Balance from './Balance';
import EmptyListComponent from '../../StatelessScreens/EmptyListComponent';


@observer
class MyWallet extends Component {
    constructor(props) {
        super(props);
        this.mapRef = null;
        this.state = {
            email: '',
            password: '',
            errorloading: false,
            modalVisible: false,
            isOn: true,
        }
        // this.SetCheckers = this.SetCheckers.bind(this)
        this.toggleMe = this.toggleMe.bind(this);
        this.toggleModal = this.toggleModal.bind(this)
    }
    toggleMe = () => {
        this.setState({ isOn: !this.state.isOn });
    };
    toggleModal = () => {
        this.setState({ modalVisible: !this.state.modalVisible });
    };
    async componentDidMount() {
        let status = await hasSetIntrest();
        // alert(status)
        DashboardStore._UpdateUserSetIntrest(status);
DashboardStore.__GetTransactionHistory(1,50);
    }

    render() {
        const{TransactionInHistory}=DashboardStore;
let ErrorComponentDisplay = null;

if (DashboardStore.isProcessing === true) {
    ErrorComponentDisplay = AppInlineLoader(DashboardStore.isProcessing)

}
else {
    ErrorComponentDisplay = <EmptyListComponent
        message={DashboardStore.TransactionInHistory.ErrorMessage}
        onclickhandler={this.ReloadPageData}
    />
}
let MyTransactionHistory =
    <FlatList
        data={TransactionInHistory.TransactionHistory}
        // Render Items
        renderItem={({ item, index }) => {
            //   let Applicants=item.applicants
            // let posterName=  `${poster.first_name}  ${poster.last_name} `
            // let poster = item.created_by_profile
 
            return (
         item.transactionType==="credit" ?
         

         
                <View style={styles.tranListContent}>
                <View style={styles.trasDetails}>
                    <View style={styles.trasDetailsSymbol}>
                        <Text style={styles.Text8}>+</Text>
                    </View>
                    <Text style={[styles.Text7, { color: '#2F2F2F', flex: 1 }]}>
                     {item.description}
                        </Text>
                </View>
                <View style={styles.amountDis}>
                    <Text style={[styles.Text7, { color: '#5ABC7A' }]}>
                        
                        {item.amount}   {item.currency}
                        </Text>
                </View>

            </View>
         :
         <View style={styles.tranListContent}>
         <View style={styles.trasDetails}>
             <View style={styles.trasDetailsSymbol}>
                 <Text style={styles.Text8}>-</Text>
             </View>
             <Text style={[styles.Text7, { color: '#2F2F2F', flex: 1 }]}>
             {item.description}
                 </Text>
         </View>
         <View style={styles.amountDis}>
             <Text style={[styles.Text7, { color: '#FF5964' }]}>
             {item.amount}   {item.currency}
             </Text>
         </View>
     </View>

         
            )
        }}
        // Item Key
        keyExtractor={(item, index) => String(index)}
        // Header (Title)
        // ListHeaderComponent={this.renderHeader}
        // Footer (Activity Indicator)
        // ListFooterComponent={this.renderFooter()}
        // On End Reached (Takes a function)
        onEndReached={this.retrieveMore}
    // How Close To The End Of List Until Next Data Request Is Made
    // onEndReachedThreshold={0}
    // Refreshing (Set To True When End Reached)
    // refreshing={this.state.refreshing}
    />
        return (

            <Container style={styles.container}>
                <CustomizeHeader
                    leftside='WhiteArr'
                    title='My Wallet'
                    rightside='empty'
                    statedColor='#1E93E2'
                    icon='md-close'
                />
                <ScrollView>
                <View style={styles.cupHolder}>
                    <Balance/>
                    <View style={styles.buttonList}>
                        <Button style={styles.btnAddSty}
                            onPress={() => { this.props.navigation.navigate("PayWith") }}
                        >
                            <Text style={styles.Text7}>ADD FUNDS</Text>
                        </Button>
                        <Button style={styles.btnAddSty}
                            onPress={() => { this.props.navigation.navigate("WalletWithdraw") }}
                        >
                            <Text style={styles.Text7}>WITHDRAW</Text>
                        </Button>
                    </View>
                </View>
                <View style={styles.titleView}>
                    <Text style={styles.Text4}>
                        Transaction History
                    </Text>
                </View>

                <ScrollView
                    style={{ padding: 21, paddingTop: 0, }}
                >
                    <View style={styles.transactionListWrap}>
                     {
                    TransactionInHistory.TransactionHistory.length > 0 ?
                   <React.Fragment>
     
                     {MyTransactionHistory}
     
                   </React.Fragment>
                   :
                   ErrorComponentDisplay
               }
                     
                     
                
                        {/* <View style={styles.tranListContent}>
                            <View style={styles.trasDetails}>
                                <View style={styles.trasDetailsSymbol}>
                                    <Text style={styles.Text8}>+</Text>
                                </View>
                                <Text style={[styles.Text7, { color: '#2F2F2F', flex: 1 }]}>
                                    Monthly earnings via WB
                                    League
                                    </Text>
                            </View>
                            <View style={styles.amountDis}>
                                <Text style={[styles.Text7, { color: '#5ABC7A' }]}>
                                    150 NGN
                                </Text>
                            </View>
                        </View>
                        <View style={styles.tranListContent}>
                            <View style={styles.trasDetails}>
                                <View style={styles.trasDetailsSymbol}>
                                    <Text style={styles.Text8}>-</Text>
                                </View>
                                <Text style={[styles.Text7, { color: '#2F2F2F', flex: 1 }]}>
                                    One-off post to timeline
                                    </Text>
                            </View>
                            <View style={styles.amountDis}>
                                <Text style={[styles.Text7, { color: '#FF5964' }]}>
                                    1,250 NGN
                                </Text>
                            </View>
                        </View>
                    */}
                    </View>

                </ScrollView>
                </ScrollView>
       
            </Container>


        );
    }
}
export default MyWallet;

const styles = StyleSheet.create({
    container: {
        backgroundColor: "#FFF",
        display: "flex",
        flex: 1,
        justifyContent: "flex-start",
    },
    likeFooter: {
        display: "flex",
        flexDirection: 'row',
        justifyContent: "flex-end",
        alignItems: 'center',
        // width: '100%'
        marginTop: 40,
        marginBottom: 30,
    },
    cupHolder: {
        display: "flex",
        justifyContent: "center",
        alignItems: 'center',
        paddingTop: 14,
        paddingBottom: 14,
        paddingLeft: 30,
        paddingRight: 30,
        // marginBottom: 40,
        backgroundColor: '#1E93E2',
    },
    contentList: {
        display: "flex",
        alignItems: 'center',
        marginBottom: 32,
    },
    buttonList: {
        display: "flex",
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%'
    },
    likebtnLs: {
        display: "flex",
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#1E93E2',
        height: 47,
        justifyContent: 'space-between',
        width: '100%',
        marginBottom: 11,
        borderWidth: 1,
        borderColor: '#1E93E2',
        borderRadius: 15,
        paddingLeft: 20,
        paddingRight: 20,
    },
    ModallikebtnLs: {
        display: "flex",
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#1E93E2',
        height: 37,
        justifyContent: 'space-between',
        width: '100%',
        marginBottom: 11,
        borderWidth: 1,
        borderColor: '#1E93E2',
        borderRadius: 9,
        paddingLeft: 20,
        paddingRight: 20,
    },
    CompleteBtn: {
        display: "flex",
        alignItems: 'center',
        height: 47,
        backgroundColor: '#5ABC7A',
        justifyContent: 'space-between',
        width: '100%',
        marginBottom: 11,
        borderWidth: 1,
        borderColor: '#5ABC7A',
        borderRadius: 15,
        elevation: 0,
        paddingLeft: 20,
        paddingRight: 20,
    },
    Text1: {
        fontSize: 13,
        fontWeight: '600',
        color: '#1E93E2',
    },
    Text1_Modal: {
        fontSize: 10,
        fontWeight: '500',
        color: '#FAFAFA',
    },
    Text1_Modal2: {
        fontSize: 10,
        fontWeight: '500',
        color: '#FAFAFA',
        marginLeft: 20,
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'center',
        flexDirection: 'row',
    },
    Text2: {
        fontSize: 16,
        fontWeight: '600',
        color: '#1E93E2',
    },
    Text3: {
        fontSize: 12,
        fontWeight: '500',
        color: '#1E93E2',
    },
    Text4: {
        fontSize: 14,
        fontWeight: '600',
        color: '#1E93E2',
    },
    Text5: {
        fontSize: 10,
        fontWeight: '300',
        color: '#2F2F2F',
    },
    Text6: {
        fontSize: 48,
        fontWeight: '500',
        color: '#FFFFFF',
    },
    Text7: {
        fontSize: 13,
        fontWeight: '500',
        color: '#FFFFFF',
    },
    Text8: {
        fontSize: 25,
        // fontWeight: '500',
        color: '#2F2F2F',
        flex: 1
    },
    titlesty: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 31
    },
    titlestyText: {
        fontWeight: '300',
        fontSize: 11,
        color: '#2F2F2F',
    },
    toppersty: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    transactionListWrap: {
        display: 'flex',
        width: '100%'
        // justifyContent: 'center',
        // alignItems: 'center',
    },
    tranListContent: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingTop: 15,
        paddingBottom: 15,
        borderBottomWidth: 1,
        borderBottomColor: '#E5E5E5',

    },
    btnAddSty: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: 138,
        height: 45,
        borderWidth: 1,
        borderColor: '#FFFFFF',
        borderRadius: 3,
        backgroundColor: 'transparent',
        elevation: 0,
    },
    titleView: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: "100%",
        height: 55,
        elevation: 1,
        borderBottomWidth: 1,
        borderBottomColor: '#FFFFFF',
        backgroundColor: '#F7F7F7',
        elevation: 0,
    },
    amountDis: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        width: 80,
    },
    trasDetails: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        flex: 1,
    },
    trasDetailsSymbol: {
        display: 'flex',
        // flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        width: 34,
        height: 34,
        borderRadius: 34 / 2,
        backgroundColor: '#F2F2F2',
        marginRight: 19,
    },
    // Modal style =============================================================
    MContainer: {
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
    },
    Mwrapper: {
        height: 510,
        width: '90%',
        backgroundColor: '#fff',
        // padding: 15,
        display: 'flex',
        justifyContent: 'space-around',
        // alignItems: 'center'
    },
    homeModalwrap: {
        // height: '80%',
        width: '100%',
        backgroundColor: '#fff',
        // padding: 15,
        // display: 'flex',
        justifyContent: 'space-between',
        overflow: 'hidden',
        // alignItems: 'center'
    },

    homeModalHeader: {
        width: '100%',
        backgroundColor: '#fff',
        padding: 25,
        display: 'flex',
        alignItems: 'center'
    },
    homeModalContentWrap: {
        width: '100%',
        padding: 18,
        paddingTop: 0,
        display: 'flex',
        alignItems: 'center'
    },
    homeModalwrapText1: {
        fontSize: 60,
        fontSize: 16,
        color: '#FAFAFA',
    },
    btnSave: {
        height: 54,
        backgroundColor: '#1E93E2',
        elevation: 0,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    noteItemsWrap: {
        display: 'flex',
        marginBottom: 40,
        paddingLeft: 18,
        paddingRight: 18,
        width: '100%',
    },
    noteItems: {
        flexDirection: 'row',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        // marginBottom: 13,
        marginTop: 8,
        width: '100%',
    },
    // Modal Style ends=========================================================





});

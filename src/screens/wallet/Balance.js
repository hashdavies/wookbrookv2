import React, { Component, Fragment } from 'react';
import { Text, StyleSheet,} from 'react-native';
import {
    Button,
    View,
} from 'native-base';
import { observer } from 'mobx-react/native'
import DashboardStore from '../../stores/Dashboard';

// import DashboardStore from '../../stores/Dashboard';
// import { SvgUri } from 'react-native-svg';


@observer
class Balance extends Component {
    constructor(props) {
        super(props);
        // this.mapRef = null;
        this.state = {
           
        }
    }
   
    
    componentDidMount() {
        DashboardStore.__GetAccountBalance();

    }
    render() {
        const{WalletBalance}=DashboardStore
        return (
                
                <Fragment>
                    <View style={styles.toppersty}>
                        <Text style={{ fontSize: 20, color: '#fff' }}>$</Text>
        <Text style={styles.Text6}>{WalletBalance.Balance}</Text>
                    </View>

                    <Text style={[styles.Text7, { marginBottom: 30, marginTop: 6 }]}>
                        Available Balance
                        </Text>
                </Fragment>
        );
    }
}
export default Balance;

const styles = StyleSheet.create({
    cupHolder: {
        display: "flex",
        justifyContent: "center",
        alignItems: 'center',
        paddingTop: 14,
        paddingBottom: 14,
        paddingLeft: 30,
        paddingRight: 30,
        // marginBottom: 40,
        backgroundColor: '#1E93E2',
    },
    toppersty: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    Text6: {
        fontSize: 48,
        fontWeight: '500',
        color: '#FFFFFF',
    },
    Text7: {
        fontSize: 13,
        fontWeight: '500',
        color: '#FFFFFF',
    },
    container: {
        backgroundColor: "#FFF",
        display: "flex",
        flex: 1,
        justifyContent: "flex-start",
    },

    likeFooter: {
        display: "flex",
        flexDirection: 'row',
        justifyContent: "flex-end",
        alignItems: 'center',
        // width: '100%'
        marginTop: 40,
        marginBottom: 30,
    },

    contentList: {
        display: "flex",
        alignItems: 'center',
        marginBottom: 32,
    },
    buttonList: {
        display: "flex",
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%'
    },
    likebtnLs: {
        display: "flex",
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#1E93E2',
        height: 47,
        justifyContent: 'space-between',
        width: '100%',
        marginBottom: 11,
        borderWidth: 1,
        borderColor: '#1E93E2',
        borderRadius: 15,
        paddingLeft: 20,
        paddingRight: 20,
    },
    ModallikebtnLs: {
        display: "flex",
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#1E93E2',
        height: 37,
        justifyContent: 'space-between',
        width: '100%',
        marginBottom: 11,
        borderWidth: 1,
        borderColor: '#1E93E2',
        borderRadius: 9,
        paddingLeft: 20,
        paddingRight: 20,
    },
    CompleteBtn: {
        display: "flex",
        alignItems: 'center',
        height: 47,
        backgroundColor: '#5ABC7A',
        justifyContent: 'space-between',
        width: '100%',
        marginBottom: 11,
        borderWidth: 1,
        borderColor: '#5ABC7A',
        borderRadius: 15,
        elevation: 0,
        paddingLeft: 20,
        paddingRight: 20,
    },
    Text1: {
        fontSize: 13,
        fontWeight: '600',
        color: '#1E93E2',
    },
    Text1_Modal: {
        fontSize: 10,
        fontWeight: '500',
        color: '#FAFAFA',
    },
    Text1_Modal2: {
        fontSize: 10,
        fontWeight: '500',
        color: '#FAFAFA',
        marginLeft: 20,
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'center',
        flexDirection: 'row',
    },
    Text2: {
        fontSize: 16,
        fontWeight: '600',
        color: '#1E93E2',
    },
    Text3: {
        fontSize: 12,
        fontWeight: '500',
        color: '#1E93E2',
    },
    Text4: {
        fontSize: 14,
        fontWeight: '600',
        color: '#1E93E2',
    },
    Text5: {
        fontSize: 10,
        fontWeight: '300',
        color: '#2F2F2F',
    },


    Text8: {
        fontSize: 15,
        // fontWeight: '500',
        color: '#2F2F2F',
    },
    titlesty: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 31
    },
    titlestyText: {
        fontWeight: '300',
        fontSize: 11,
        color: '#2F2F2F',
    },

    transactionListWrap: {
        display: 'flex',
        width: '100%'
        // justifyContent: 'center',
        // alignItems: 'center',
    },
    tranListContent: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingTop: 15,
        paddingBottom: 15,
        borderBottomWidth: 1,
        borderBottomColor: '#E5E5E5',

    },
    btnAddSty: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: 138,
        height: 45,
        borderWidth: 1,
        borderColor: '#FFFFFF',
        borderRadius: 3,
        backgroundColor: 'transparent',
        elevation: 0,
    },
    titleView: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: "100%",
        height: 55,
        elevation: 1,
        borderBottomWidth: 1,
        borderBottomColor: '#FFFFFF',
        backgroundColor: '#F7F7F7',
        elevation: 0,
    },
    amountDis: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        width: 80,
    },
    trasDetails: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        flex: 1,
    },
    trasDetailsSymbol: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: 34,
        height: 34,
        borderRadius: 34 / 2,
        backgroundColor: '#F2F2F2',
        marginRight: 19,
    },
    addfundBtn: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: '#E5E5E5',
        borderRadius: 3,
        backgroundColor: '#FFF',
        height: 45
    },
    btnADDNewCard: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        height: 15,
        backgroundColor: 'transparent',
        elevation: 0,
        marginTop: 10,
    },
    btnPay: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        height: 41,
        backgroundColor: '#1E93E2',
        borderRadius: 3,
        elevation: 0,
        marginTop: 10,
    },
    // Modal style =============================================================
    MContainer: {
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
    },
    Mwrapper: {
        height: 510,
        width: '90%',
        backgroundColor: '#fff',
        // padding: 15,
        display: 'flex',
        justifyContent: 'space-around',
        // alignItems: 'center'
    },
    homeModalwrap: {
        // height: '80%',
        width: '100%',
        backgroundColor: '#fff',
        // padding: 15,
        // display: 'flex',
        justifyContent: 'space-between',
        overflow: 'hidden',
        // alignItems: 'center'
    },

    homeModalHeader: {
        width: '100%',
        backgroundColor: '#fff',
        padding: 25,
        display: 'flex',
        alignItems: 'center'
    },
    homeModalContentWrap: {
        width: '100%',
        padding: 18,
        paddingTop: 0,
        display: 'flex',
        alignItems: 'center'
    },
    homeModalwrapText1: {
        fontSize: 60,
        fontSize: 16,
        color: '#FAFAFA',
    },
    btnSave: {
        height: 54,
        backgroundColor: '#1E93E2',
        elevation: 0,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    noteItemsWrap: {
        display: 'flex',
        marginBottom: 40,
        paddingLeft: 18,
        paddingRight: 18,
        width: '100%',
    },
    noteItems: {
        flexDirection: 'row',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        // marginBottom: 13,
        marginTop: 8,
        width: '100%',
    },
    // Modal Style ends=========================================================





});

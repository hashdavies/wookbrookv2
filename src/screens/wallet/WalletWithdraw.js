import React, { Component, Fragment } from 'react';
import { Text, StyleSheet, ImageBackground, Keyboard, TouchableOpacity } from 'react-native';
import { KeyboardAvoidingView, Image, ScrollView } from 'react-native';
import ToggleSwitch from 'toggle-switch-react-native'
import Ionicons from 'react-native-vector-icons/Ionicons';
import Checker from '../../assets/workbrookAssets/checker.png';
import {
    Container,
    Header,
    Content,
    Textarea,
    Button,
    View,
    Thumbnail,
    Form,
    Item,
    Input,
    Label,
    Badge,
    Icon,
    Footer,
    Picker,
    Item as FormItem,
    Left,
    Body,
    CardItem,
    Card,
    Right,
    ListItem,
    List,
    CheckBox
} from 'native-base';
import {
    Kaede,
    Hoshi,
    Jiro,
    Isao,
    Madoka,
    Akira,
    Hideo,
    Kohana,
    Makiko,
    Sae,
    Fumi,
} from 'react-native-textinput-effects';
import CustomizeHeader from '../frags/CustomizeHeader';

import { myspiner } from '../../dependency/UtilityFunctions';
import Logo from '../../assets/workbrookAssets/logo.png';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import RBSheet from "react-native-raw-bottom-sheet";
import Interests from '../interests/Interests';
import Noti from "../../assets/workbrookAssets/noti.png";
import UserBl from "../../assets/workbrookAssets/userBl.png";
import ArrForward from "../../assets/workbrookAssets/arrfr.png";
import Shield from "../../assets/workbrookAssets/shield.png";
import Quest from "../../assets/workbrookAssets/quest.png";
import Info from "../../assets/workbrookAssets/info.png";
import Plus from "../../assets/workbrookAssets/plus.png";
import Remark from "../../assets/workbrookAssets/remark.png";
import Like from "../../assets/workbrookAssets/like.png";
import MoreIcon from "../../assets/workbrookAssets/more2.png";
import Profile from "../../assets/images/profile/dp2.png";
import cardPix from "../../assets/images/background/bg3_old.jpg";
import CloseModal from "../../assets/workbrookAssets/whitecloss.png";
import Modal from 'react-native-modal';
import AccountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import { text } from 'react-native-communications';
import DashboardStore from '../../stores/Dashboard';
import { hasSetIntrest } from '../../auth';
import { SvgUri } from 'react-native-svg';
import MultiSelect from 'react-native-multiple-select';
import { CreditCardInput, LiteCreditCardInput } from "react-native-credit-card-input";
import Balance from './Balance';

items = [{
    id: '92iijs7yta',
    name: 'Ondo',
}, {
    id: 'a0s0a8ssbsd',
    name: 'Ogun',
}, {
    id: '16hbajsabsd',
    name: 'Calabar',
}, {
    id: 'suudydjsjd',
    name: 'Abuja',
}];

@observer
class WalletWithdraw extends Component {
    constructor(props) {
        super(props);
        this.mapRef = null;
        this.state = {
            email: '',
            password: '',
            errorloading: false,
            modalVisible: false,
            modalVisible2: false,
            isOn: true,
            // selectedItems = [],
        }
        // this.SetCheckers = this.SetCheckers.bind(this)
        this.toggleMe = this.toggleMe.bind(this);
        this.toggleModal = this.toggleModal.bind(this)
        this.toggleModal2 = this.toggleModal2.bind(this)
    }

    onSelectedItemsChange = selectedItems => {
        this.setState({ selectedItems });
    };
    toggleMe = () => {
        this.setState({ isOn: !this.state.isOn });
    };
    toggleModal = () => {
        this.setState({ modalVisible: !this.state.modalVisible });
    };
    toggleModal2 = () => {
        this.setState({ modalVisible2: !this.state.modalVisible2 });
    };
    toggleModalBoth = () => {
        this.setState({ modalVisible: !this.state.modalVisible });
        this.setState({ modalVisible2: !this.state.modalVisible2 });
    };
    async componentDidMount() {
        let status = await hasSetIntrest();
        // alert(status)
        DashboardStore._UpdateUserSetIntrest(status);

    }
    _onChange = (form) => {
        console.log(form)
    };


    render() {
        const { selectedItems } = this.state;
        return (

            <Container style={styles.container}>
                <CustomizeHeader
                    leftside='WhiteArr'
                    title='My Wallet'
                    rightside='empty'
                    statedColor='#1E93E2'
                    icon='md-close'
                />
                <ScrollView>
                <View style={styles.cupHolder}>
                   <Balance/>
                    <View style={styles.buttonList}>
                        <Button style={[styles.btnAddSty]}
                            onPress={() => { this.props.navigation.navigate("AddFund") }}
                        >
                            <Text style={[styles.Text7]}>ADD FUNDS</Text>
                        </Button>
                        <Button style={[styles.btnAddSty, { backgroundColor: '#FFF' }]}
                            onPress={() => { this.props.navigation.navigate("WalletWithdraw") }}
                        >
                            <Text style={[styles.Text7, { color: '#1E93E2' }]}>WITHDRAW</Text>
                        </Button>
                    </View>
                </View>
                <View style={styles.titleView}>
                    <Text style={styles.Text4}>
                        Add money to your wallet
                    </Text>
                </View>

                <ScrollView
                    style={{ padding: 17, paddingTop: 0, }}
                >
                    <Button full style={[styles.btnAddFirstCard, {}]}
                     onPress={() => { this.props.navigation.navigate("AddWithdrawerAccount") }}
                    // onPress={this.toggleModal}
                    >
                        <Text style={[styles.Text7, { color: '#1E93E2', }]}>+ ADD ACCOUNT</Text>
                    </Button>

                    <Card style={{ padding: 5, paddingBottom: 17 }}>
                        <CardItem>
                            <Left>
                                {/* <Image source={{ uri: 'https://res.cloudinary.com/workbrook-hash/image/upload/v1590847025/Mobile_Assets/gtb_noxr6v.png' }} style={{ width: 33, height: 33, }} /> */}
                                <SvgUri
                                    width="33px"
                                    height="33px"
                                    uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1590891946/Mobile_Assets/gtb_ogqfoz.svg"
                                >
                                </SvgUri>

                            </Left>
                            <Right>
                                <SvgUri
                                    width="21px"
                                    height="21px"
                                    uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1590846662/Mobile_Assets/checkBlue_ajznt3.svg"
                                >
                                </SvgUri>
                            </Right>
                        </CardItem>

                        <View style={{ display: 'flex', marginTop: 10, marginLeft: 15, marginRight: 15 }}>

                            <Text style={[styles.Text4, { fontWeight: '500', color: "#2F2F2F" }]}>
                                **** 8047123
                            </Text>
                            <Text style={[styles.Text4, { fontWeight: '500', color: "#2F2F2F" }]}>
                                Adekunle Ciroma Chukwu
                            </Text>

                        </View>
                        <Button full style={[styles.btnPay, { marginLeft: 15, marginRight: 15 }]}
                            onPress={this.toggleModal}
                        >
                            <Text style={[styles.Text7]}>Withdraw</Text>
                        </Button>


                    </Card>
                    <Button style={styles.btnADDNewCard}
                        onPress={() => { this.props.navigation.navigate("AddWithdrawerAccount") }}
                    >
                        <Text style={[styles.Text7, { color: '#1E93E2', fontSize: 13, marginTop: 5, marginBottom: 5 }]}>+ Add Account</Text>
                    </Button>
                </ScrollView>

                <Modal style={styles.MContainer} isVisible={this.state.modalVisible} >
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                        showsHorizontalScrollIndicator={false}
                    >
                        <View style={styles.homeModalwrap}>
                            <Button onPress={this.toggleModal} style={{ backgroundColor: "transparent", position: 'absolute', right: 0, top: -10, padding: 10, elevation: 0, zIndex: 9999, }}>
                                <Image source={CloseModal} style={{ width: 19, height: 19, }} />
                            </Button>
                            <View>

                                <View style={styles.homeModalHeader}>
                                    <Text style={styles.Text4}>Confirm your account details</Text>
                                </View>
                                <View style={styles.noteItemsWrap}>
                                    <View style={styles.itemListSty}>
                                        <Text style={[styles.Text9]}>
                                            Bank Name:
                                        </Text>
                                        <Text style={[styles.Text9, { fontWeight: 'normal', marginLeft: 3 }]}>Guaranty Trust Bank</Text>
                                    </View>
                                    <View style={styles.itemListSty}>
                                        <Text style={[styles.Text9]}>
                                            Account Name:
                                        </Text>
                                        <Text style={[styles.Text9, { fontWeight: 'normal', marginLeft: 3 }]}>Adekunle Ciroma</Text>
                                    </View>
                                    <View style={styles.itemListSty}>
                                        <Text style={[styles.Text9]}>
                                            Account Number:
                                        </Text>
                                        <Text style={[styles.Text9, { fontWeight: 'normal', marginLeft: 3 }]}>10002845596</Text>
                                    </View>

                                </View>

                            </View>
                            <View style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', width: '100%', height: 54, borderTopWidth: 1, borderTopColor: "#E5E5E5" }}>
                                <Button full style={[styles.btnSave, { backgroundColor: "transparent", flex: 1 }]}
                                    onPress={this.toggleModalBoth}
                                >
                                    <Text style={[styles.btnCheckyText, { color: '#1E93E2' }]}>{DashboardStore.isProcessing === true ? 'Processing' : 'Confirm'}</Text>
                                </Button>
                            </View>
                        </View>
                    </ScrollView>
                </Modal>
                <Modal style={styles.MContainer} isVisible={this.state.modalVisible2} >
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                        showsHorizontalScrollIndicator={false}
                    >
                        <View style={styles.homeModalwrap}>
                            <Button onPress={this.toggleModal2} style={{ backgroundColor: "transparent", position: 'absolute', right: 0, top: -10, padding: 10, elevation: 0, zIndex: 9999, }}>
                                <Image source={CloseModal} style={{ width: 19, height: 19, }} />
                            </Button>
                            <View>

                                <View style={styles.homeModalHeader}>
                                    <Text style={styles.Text4}>Enter your password</Text>
                                </View>
                                <View style={styles.noteItemsWrap2}>
                                    <View style={styles.itemListSty}>
                                        <Item regular style={{ borderRadius: 3 }}>
                                            <Input placeholder="Password" placeholderTextColor="#BCBCBC" />
                                        </Item>
                                    </View>
                                </View>

                            </View>
                            <View style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', width: '100%', height: 54, borderTopWidth: 1, borderTopColor: "#E5E5E5" }}>
                                <Button full style={[styles.btnSave, { backgroundColor: "transparent", flex: 1 }]}
                                    onPress={this.toggleModal2}
                                >
                                    <Text style={[styles.btnCheckyText, { color: '#1E93E2' }]}>{DashboardStore.isProcessing === true ? 'Processing' : 'Withdraw'}</Text>
                                </Button>
                            </View>
                        </View>
                    </ScrollView>
                </Modal>
                </ScrollView>

            </Container>


        );
    }
}
export default WalletWithdraw;

const styles = StyleSheet.create({
    container: {
        backgroundColor: "#FFF",
        display: "flex",
        flex: 1,
        justifyContent: "flex-start",
    },
    likeFooter: {
        display: "flex",
        flexDirection: 'row',
        justifyContent: "flex-end",
        alignItems: 'center',
        // width: '100%'
        marginTop: 40,
        marginBottom: 30,
    },
    cupHolder: {
        display: "flex",
        justifyContent: "center",
        alignItems: 'center',
        paddingTop: 14,
        paddingBottom: 14,
        paddingLeft: 30,
        paddingRight: 30,
        // marginBottom: 40,
        backgroundColor: '#1E93E2',
    },
    contentList: {
        display: "flex",
        alignItems: 'center',
        marginBottom: 32,
    },
    buttonList: {
        display: "flex",
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%'
    },
    likebtnLs: {
        display: "flex",
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#1E93E2',
        height: 47,
        justifyContent: 'space-between',
        width: '100%',
        marginBottom: 11,
        borderWidth: 1,
        borderColor: '#1E93E2',
        borderRadius: 15,
        paddingLeft: 20,
        paddingRight: 20,
    },
    ModallikebtnLs: {
        display: "flex",
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#1E93E2',
        height: 37,
        justifyContent: 'space-between',
        width: '100%',
        marginBottom: 11,
        borderWidth: 1,
        borderColor: '#1E93E2',
        borderRadius: 9,
        paddingLeft: 20,
        paddingRight: 20,
    },
    CompleteBtn: {
        display: "flex",
        alignItems: 'center',
        height: 47,
        backgroundColor: '#5ABC7A',
        justifyContent: 'space-between',
        width: '100%',
        marginBottom: 11,
        borderWidth: 1,
        borderColor: '#5ABC7A',
        borderRadius: 15,
        elevation: 0,
        paddingLeft: 20,
        paddingRight: 20,
    },
    Text1: {
        fontSize: 13,
        fontWeight: '600',
        color: '#1E93E2',
    },
    Text1_Modal: {
        fontSize: 10,
        fontWeight: '500',
        color: '#FAFAFA',
    },
    Text1_Modal2: {
        fontSize: 10,
        fontWeight: '500',
        color: '#FAFAFA',
        marginLeft: 20,
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'center',
        flexDirection: 'row',
    },
    Text2: {
        fontSize: 16,
        fontWeight: '600',
        color: '#1E93E2',
    },
    Text3: {
        fontSize: 12,
        fontWeight: '500',
        color: '#1E93E2',
    },
    Text4: {
        fontSize: 14,
        fontWeight: '600',
        color: '#1E93E2',
    },
    btnCheckyText: {
        fontSize: 14,
        fontWeight: '500',
        color: '#1E93E2',
    },
    Text5: {
        fontSize: 10,
        fontWeight: '300',
        color: '#2F2F2F',
    },
    Text6: {
        fontSize: 48,
        fontWeight: '500',
        color: '#FFFFFF',
    },
    Text7: {
        fontSize: 13,
        fontWeight: '500',
        color: '#FFFFFF',
    },
    Text8: {
        fontSize: 15,
        // fontWeight: '500',
        color: '#2F2F2F',
    },
    Text9: {
        fontSize: 11,
        fontWeight: '600',
        color: '#2F2F2F',
    },
    titlesty: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 31
    },
    flexItem: {
        display: 'flex',
    },
    titlestyText: {
        fontWeight: '300',
        fontSize: 11,
        color: '#2F2F2F',
    },
    toppersty: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    transactionListWrap: {
        display: 'flex',
        width: '100%'
        // justifyContent: 'center',
        // alignItems: 'center',
    },
    tranListContent: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingTop: 15,
        paddingBottom: 15,
        borderBottomWidth: 1,
        borderBottomColor: '#E5E5E5',

    },
    btnAddSty: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: 138,
        height: 45,
        borderWidth: 1,
        borderColor: '#FFFFFF',
        borderRadius: 3,
        backgroundColor: 'transparent',
        elevation: 0,
    },
    titleView: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: "100%",
        height: 55,
        elevation: 1,
        borderBottomWidth: 1,
        borderBottomColor: '#FFFFFF',
        backgroundColor: '#F7F7F7',
        elevation: 0,
    },
    amountDis: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        width: 80,
    },
    trasDetails: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        flex: 1,
    },
    trasDetailsSymbol: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: 34,
        height: 34,
        borderRadius: 34 / 2,
        backgroundColor: '#F2F2F2',
        marginRight: 19,
    },
    addfundBtn: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: '#E5E5E5',
        borderRadius: 3,
        backgroundColor: '#FFF',
        height: 45
    },
    btnADDNewCard: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        height: 15,
        backgroundColor: 'transparent',
        elevation: 0,
        marginTop: 10,
        textAlign: 'right'
    },
    btnPay: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        height: 41,
        backgroundColor: '#1E93E2',
        borderRadius: 3,
        elevation: 0,
        marginTop: 10,
    },
    btnAddFirstCard: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: '#1E93E2',
        borderWidth: 1,
        height: 45,
        backgroundColor: 'transparent',
        borderRadius: 3,
        elevation: 0,
        marginTop: 10,
    },
    // Modal style =============================================================
    MContainer: {
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
    },
    Mwrapper: {
        height: 510,
        width: '90%',
        backgroundColor: '#fff',
        // padding: 15,
        display: 'flex',
        justifyContent: 'space-around',
        // alignItems: 'center'
    },
    homeModalwrap: {
        // height: '80%',
        width: '100%',
        backgroundColor: '#fff',
        padding: 37,
        paddingBottom: 0,
        // display: 'flex',
        justifyContent: 'space-between',
        overflow: 'hidden',
        // alignItems: 'center'
        borderRadius: 12
    },

    homeModalHeader: {
        width: '100%',
        backgroundColor: '#fff',
        // padding: 25,
        display: 'flex',
        alignItems: 'center'
    },
    homeModalContentWrap: {
        width: '100%',
        padding: 18,
        paddingTop: 0,
        display: 'flex',
        alignItems: 'center'
    },
    homeModalwrapText1: {
        fontSize: 60,
        fontSize: 16,
        color: '#FAFAFA',
    },
    btnSave: {
        height: 54,
        backgroundColor: '#1E93E2',
        elevation: 0,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    noteItemsWrap: {
        display: 'flex',
        marginTop: 14,
        marginBottom: 21,
        paddingLeft: 35,
        paddingRight: 35,
        width: '100%',
    },
    noteItemsWrap2: {
        display: 'flex',
        marginTop: 14,
        marginBottom: 21,

        width: '100%',
    },
    noteItems: {
        flexDirection: 'row',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        // marginBottom: 13,
        marginTop: 8,
        width: '100%',
    },
    // Modal Style ends=========================================================
    itemListSty: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        height: 45
    },




});

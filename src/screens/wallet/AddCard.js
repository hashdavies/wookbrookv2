import React, { Component, Fragment } from 'react';
import { Text, StyleSheet, ImageBackground, Keyboard, TouchableOpacity } from 'react-native';
import { KeyboardAvoidingView, Image, ScrollView } from 'react-native';
import ToggleSwitch from 'toggle-switch-react-native'
import Ionicons from 'react-native-vector-icons/Ionicons';
import Checker from '../../assets/workbrookAssets/checker.png';
import {
    Container,
    Header,
    Content,
    Textarea,
    Button,
    View,
    Thumbnail,
    Form,
    Item,
    Input,
    Label,
    Badge,
    Icon,
    Footer,
    Picker,
    Item as FormItem,
    Left,
    Body,
    CardItem,
    Card,
    Right,
    ListItem,
    List,
    CheckBox
} from 'native-base';
import {
    Kaede,
    Hoshi,
    Jiro,
    Isao,
    Madoka,
    Akira,
    Hideo,
    Kohana,
    Makiko,
    Sae,
    Fumi,
} from 'react-native-textinput-effects';
import CustomizeHeader from '../frags/CustomizeHeader';

import { myspiner, ShowNotification } from '../../dependency/UtilityFunctions';
import Logo from '../../assets/workbrookAssets/logo.png';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import RBSheet from "react-native-raw-bottom-sheet";
import Interests from '../interests/Interests';
import Noti from "../../assets/workbrookAssets/noti.png";
import UserBl from "../../assets/workbrookAssets/userBl.png";
import ArrForward from "../../assets/workbrookAssets/arrfr.png";
import Shield from "../../assets/workbrookAssets/shield.png";
import Quest from "../../assets/workbrookAssets/quest.png";
import Info from "../../assets/workbrookAssets/info.png";
import Plus from "../../assets/workbrookAssets/plus.png";
import Remark from "../../assets/workbrookAssets/remark.png";
import Like from "../../assets/workbrookAssets/like.png";
import MoreIcon from "../../assets/workbrookAssets/more2.png";
import Profile from "../../assets/images/profile/dp2.png";
import cardPix from "../../assets/images/background/bg3_old.jpg";
import CloseModal from "../../assets/workbrookAssets/whitecloss.png";
import Modal from 'react-native-modal';
import AccountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import { text } from 'react-native-communications';
import DashboardStore from '../../stores/Dashboard';
import { hasSetIntrest } from '../../auth';
import { SvgUri } from 'react-native-svg';
import MultiSelect from 'react-native-multiple-select';
import RNPickerSelect from 'react-native-picker-select';
import { CreditCardInput, LiteCreditCardInput } from "react-native-credit-card-input";
import { Col, Row, Grid } from "react-native-easy-grid";
import Balance from './Balance';
import Stripe from 'react-native-stripe-api';
import { STRIPE_KEY } from '../../dependency/Config';


items = [{
    id: '92iijs7yta',
    name: 'Ondo',
}, {
    id: 'a0s0a8ssbsd',
    name: 'Ogun',
}, {
    id: '16hbajsabsd',
    name: 'Calabar',
}, {
    id: 'suudydjsjd',
    name: 'Abuja',
}];

@observer
class AddCard extends Component {
    constructor(props) {
        super(props);
        this.mapRef = null;
        this.state = {
            email: '',
            password: '',
            errorloading: false,
            modalVisible: false,
            checkedStatus: false,
            isOn: true,
            cardnumber: '4242424242424242' ,
            exp_month: '09', 
            exp_year: '21', 
            cvc: '111',
            card_name: '',
         }
        // this.SetCheckers = this.SetCheckers.bind(this)
        this.toggleMe = this.toggleMe.bind(this);
        this.toggleModal = this.toggleModal.bind(this)
        this.AddCardToSystem = this.AddCardToSystem.bind(this)
        this.toggleChecked = this.toggleChecked.bind(this)
    }

    onSelectedItemsChange = selectedItems => {
        this.setState({ selectedItems });
    };
    toggleMe = () => {
        this.setState({ isOn: !this.state.isOn });
    };
    toggleChecked = () => {
        this.setState({ checkedStatus: !this.state.checkedStatus });
    };
    toggleModal = () => {
        this.setState({ modalVisible: !this.state.modalVisible });
    };
    async componentDidMount() {
        let status = await hasSetIntrest();
        // alert(status)
        DashboardStore._UpdateUserSetIntrest(status);
        DashboardStore.__GetMycardList(1,30);

    }
    _onChange = (form) => {
        console.log(form)
    };
     AddCardToSystem= async()=> {
         const{cardnumber,exp_month,exp_year,cvc}=this.state;
         if(cardnumber===''){
            ShowNotification("Card Number is required", true);
            return;
         }
         else if(exp_month===''){
            ShowNotification("Card Expiring Month is required", true);
            return;
         }
         else if(exp_year===''){
            ShowNotification("Card Expiring Year is required", true);
            return;
         }
         else if(cvc===''){
            ShowNotification("Card CVV   is required", true);
            return;
         }

        try{
            
            DashboardStore._ToggleProcessing(true)
          const apiKey = STRIPE_KEY;
          const client = new Stripe(apiKey);
          const token = await client.createToken({
            number: cardnumber ,
            exp_month: exp_month, 
            exp_year: exp_year, 
            cvc: cvc,
            address_zip: '12345'
          });
          console.log("Stripe Token")
          console.log(token)
          let StripeToken=token.id;
          DashboardStore._ToggleProcessing(false)
        //   DashboardStore.__AddCardToSystem(StripeToken);
          DashboardStore.__AddCardToSystem("tok_visa");
          console.log("Stripe Token")
        } catch (error) {
        //   Alert.alert('Opps', error);
          DashboardStore._ToggleProcessing(false)
          ShowNotification("An Error Occured. Please try again later ", true);


      }

    }

    render() {
        const { selectedItems } = this.state;
        return (

            <Container style={styles.container}>
                <CustomizeHeader
                    leftside='WhiteArr'
                    title='My Wallet'
                    rightside='empty'
                    statedColor='#1E93E2'
                    icon='md-close'
                />
                <ScrollView>
                <View style={styles.cupHolder}>
                    {/* <View style={styles.toppersty}>
                        <Text style={{ fontSize: 20, color: '#fff' }}>N</Text>
                        <Text style={styles.Text6}>5,4570.00</Text>
                    </View> */}

                    <Balance/>
                    <View style={styles.buttonList}>
                        <Button style={[styles.btnAddSty, { backgroundColor: '#FFF' }]}>
                            <Text style={[styles.Text7, { color: '#1E93E2' }]}>ADD FUNDS</Text>
                        </Button>
                        <Button style={styles.btnAddSty}
                            onPress={() => { this.props.navigation.navigate("WalletWithdraw") }}
                        >
                            <Text style={styles.Text7}>WITHDRAW</Text>
                        </Button>
                    </View>
                </View>
                <View style={styles.titleView}>
                    <Text style={styles.Text4}>
                        Add New Card
                    </Text>
                </View>

                <ScrollView
                    style={{ padding: 10, paddingTop: 0, }}
                >


                    <Card style={{ padding: 5, paddingBottom: 17, paddingTop: 17 }}>
                        {/* <LiteCreditCardInput onChange={this._onChange} style={{}}  /> */}

                        <Item regular style={styles.addfundBtn}>
                            <Input 
                            placeholder="CARD NUMBER" 
                            placeholderTextColor="#BCBCBC" 
                            style={[styles.Text7, { color: '#2F2F2F' }]}
                            keyboardType={"number-pad"}
                            defaultValue={this.state.cardnumber}
                            onChangeText={(cardnumber) => {this.setState({
                                cardnumber
                            })}}

                            />
                        </Item>
                        {/* <Item regular style={styles.addfundBtn}>
                            <Input 
                            placeholder="CARDHOLDER’S NAME" 
                            placeholderTextColor="#BCBCBC" 
                            style={[styles.Text7, { color: '#2F2F2F' }]} 
                            keyboardType={"default"}
                            defaultValue={this.state.card_name}
                            onChangeText={(card_name) => {this.setState({
                                card_name
                            })}}
                            />
                        </Item> */}
                        <Grid style={{marginTop: 36, paddingLeft: 10, paddingRight: 10}}>

                            <Col>
                                <View style={[styles.addfundBtn2, { flex: 1, marginRight: 1 }]}>
                                    <RNPickerSelect
                                        placeholder={{
                                            label: 'MM',
                                            color: '#A7A7A7',
                                            fontSize: 11,
                                        }}
                                        onValueChange={(value) => {this.setState({
                                            exp_month:value
                                        })}}
                                        value={this.state.exp_month}
                                        items={[
                                            { label: 'January', value:"01" },
                                            { label: 'February', value: "02" },
                                            { label: 'March', value: "03" },
                                            { label: 'April', value: "04" },
                                            { label: 'May', value: "05" },
                                            { label: 'June', value: "06" },
                                            { label: 'July', value: "07" },
                                            { label: 'August', value: "08" },
                                            { label: 'September', value: "09" },
                                            { label: 'October', value: "10" },
                                            { label: 'November', value: "11" },
                                            { label: 'December', value: "12" }
                                        ]}
                                        style={{

                                            iconContainer: {
                                                top: 20,
                                                right: 10,
                                            },
                                            placeholder: {
                                                fontSize: 11,
                                                color: '#2F2F2F',
                                            },
                                        }}
                                    />
                                </View>
                            </Col>
                            <Col>
                                <View style={[styles.addfundBtn2, { flex: 1 }]}>
                                    <RNPickerSelect
                                        placeholder={{
                                            label: 'YYYY',
                                            color: '#A7A7A7',
                                            fontSize: 11,
                                        }}
                                        onValueChange={(value) => {this.setState({
                                            exp_year:value
                                        })}}
                                        value={this.state.exp_year}

                                        items={[
                                            {label: '2020', value:"20"},
                                            {label: '2021', value: "21"},
                                            {label: '2022', value: "22"},
                                            {label: '2023', value: "23"},
                                            {label: '2024', value: "24"},
                                            {label: '2025', value: "25"},
                                            {label: '2026', value: "26"},
                                            {label: '2027', value: "27"},
                                            {label: '2028', value: "28"},
                                            {label: '2029', value: "29"},
                                            {label: '2030', value: "30"},
                                            {label: '2031', value: "31"}
                                        ]}
                                        style={{

                                            iconContainer: {
                                                top: 20,
                                                right: 10,
                                            },
                                            placeholder: {
                                                fontSize: 11,
                                                color: '#2F2F2F',
                                            },
                                        }}
                                    />
                                </View>
                            </Col>
                            <Col>
                                <Item regular style={[styles.addfundBtn, { flex: 1 }]}>
                                    <Input 
                                    placeholder="CVV"
                                     placeholderTextColor="#BCBCBC" 
                                     style={[styles.Text7, { color: '#2F2F2F' }]} 
                                     keyboardType={"number-pad"}
                                     defaultValue={this.state.cvc}  
                                     onChangeText={(card_cvv) => {this.setState({
                                        cvc:card_cvv
                                     })}}
                                     />
                                </Item>
                            </Col>


                        </Grid>

                        <Button full style={styles.btnADDNewCard}>
                            <Text style={[styles.Text7, { color: '#1E93E2', fontSize: 8, marginTop: 5, marginBottom: 5, marginRight: 15, }]}>What is CVV?</Text>
                        </Button>
                        {/* <View style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', marginTop: 10, paddingLeft: 0, paddingRight: 10 }}>
                            <CheckBox checked={this.state.checkedStatus} onPress={this.toggleChecked} style={{borderRadius: 3, fontSize: 11 }} />
                            <Text style={[styles.Text7, { marginLeft: 15, color: "#2F2F2F" }]}>Save this card</Text>

                        </View> */}

                        <Button full style={[styles.btnPay, { marginLeft: 10, marginRight: 10 }]}
                        onPress={this.AddCardToSystem}
                        >

                                    <Text style={[styles.Text7]}>{DashboardStore.isProcessing===true?'Processing':'Add Card'}</Text>
                        </Button>
                    </Card>
                </ScrollView>
         </ScrollView>
            </Container>


        );
    }
}
export default AddCard;

const styles = StyleSheet.create({
    container: {
        backgroundColor: "#FFF",
        display: "flex",
        flex: 1,
        justifyContent: "flex-start",
    },
    likeFooter: {
        display: "flex",
        flexDirection: 'row',
        justifyContent: "flex-end",
        alignItems: 'center',
        // width: '100%'
        marginTop: 40,
        marginBottom: 30,
    },
    cupHolder: {
        display: "flex",
        justifyContent: "center",
        alignItems: 'center',
        paddingTop: 14,
        paddingBottom: 14,
        paddingLeft: 30,
        paddingRight: 30,
        // marginBottom: 40,
        backgroundColor: '#1E93E2',
    },
    contentList: {
        display: "flex",
        alignItems: 'center',
        marginBottom: 32,
    },
    buttonList: {
        display: "flex",
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%'
    },
    likebtnLs: {
        display: "flex",
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#1E93E2',
        height: 47,
        justifyContent: 'space-between',
        width: '100%',
        marginBottom: 11,
        borderWidth: 1,
        borderColor: '#1E93E2',
        borderRadius: 15,
        paddingLeft: 20,
        paddingRight: 20,
    },
    ModallikebtnLs: {
        display: "flex",
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#1E93E2',
        height: 37,
        justifyContent: 'space-between',
        width: '100%',
        marginBottom: 11,
        borderWidth: 1,
        borderColor: '#1E93E2',
        borderRadius: 9,
        paddingLeft: 20,
        paddingRight: 20,
    },
    CompleteBtn: {
        display: "flex",
        alignItems: 'center',
        height: 47,
        backgroundColor: '#5ABC7A',
        justifyContent: 'space-between',
        width: '100%',
        marginBottom: 11,
        borderWidth: 1,
        borderColor: '#5ABC7A',
        borderRadius: 15,
        elevation: 0,
        paddingLeft: 20,
        paddingRight: 20,
    },
    Text1: {
        fontSize: 13,
        fontWeight: '600',
        color: '#1E93E2',
    },
    Text1_Modal: {
        fontSize: 10,
        fontWeight: '500',
        color: '#FAFAFA',
    },
    Text1_Modal2: {
        fontSize: 10,
        fontWeight: '500',
        color: '#FAFAFA',
        marginLeft: 20,
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'center',
        flexDirection: 'row',
    },
    Text2: {
        fontSize: 16,
        fontWeight: '600',
        color: '#1E93E2',
    },
    Text3: {
        fontSize: 12,
        fontWeight: '500',
        color: '#1E93E2',
    },
    Text4: {
        fontSize: 14,
        fontWeight: '600',
        color: '#1E93E2',
    },
    Text5: {
        fontSize: 10,
        fontWeight: '300',
        color: '#2F2F2F',
    },
    Text6: {
        fontSize: 48,
        fontWeight: '500',
        color: '#FFFFFF',
    },
    Text7: {
        fontSize: 13,
        fontWeight: '500',
        color: '#FFFFFF',
    },
    Text8: {
        fontSize: 15,
        // fontWeight: '500',
        color: '#2F2F2F',
    },
    titlesty: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 31
    },
    flexItem: {
        display: 'flex',
        // flexDirection: 'row',
        // justifyContent: 'center',
        // alignItems: 'center',
        // marginBottom: 31
    },
    titlestyText: {
        fontWeight: '300',
        fontSize: 11,
        color: '#2F2F2F',
    },
    toppersty: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    transactionListWrap: {
        display: 'flex',
        width: '100%'
        // justifyContent: 'center',
        // alignItems: 'center',
    },
    tranListContent: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingTop: 15,
        paddingBottom: 15,
        borderBottomWidth: 1,
        borderBottomColor: '#E5E5E5',

    },
    btnAddSty: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: 138,
        height: 45,
        borderWidth: 1,
        borderColor: '#FFFFFF',
        borderRadius: 3,
        backgroundColor: 'transparent',
        elevation: 0,
    },
    titleView: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: "100%",
        height: 55,
        elevation: 1,
        borderBottomWidth: 1,
        borderBottomColor: '#FFFFFF',
        backgroundColor: '#F7F7F7',
        elevation: 0,
    },
    amountDis: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        width: 80,
    },
    trasDetails: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        flex: 1,
    },
    trasDetailsSymbol: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: 34,
        height: 34,
        borderRadius: 34 / 2,
        backgroundColor: '#F2F2F2',
        marginRight: 19,
    },
    addfundBtn: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: '#E5E5E5',
        borderRadius: 3,
        backgroundColor: '#FFF',
        // paddingBottom: 15,
        // paddingTop: 15,
        height: 44,
        // marginBottom: 12
    },
    addfundBtn2: {
        // display: 'flex',
        // flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: '#E5E5E5',
        borderRadius: 3,
        backgroundColor: '#FFF',
        height: 44
    },
    btnADDNewCard: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        height: 15,
        backgroundColor: 'transparent',
        elevation: 0,
        marginTop: 0,
        textAlign: 'right'
    },
    btnPay: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        height: 41,
        backgroundColor: '#1E93E2',
        borderRadius: 3,
        elevation: 0,
        marginTop: 10,
    },
    // Modal style =============================================================
    MContainer: {
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
    },
    Mwrapper: {
        height: 510,
        width: '90%',
        backgroundColor: '#fff',
        // padding: 15,
        display: 'flex',
        justifyContent: 'space-around',
        // alignItems: 'center'
    },
    homeModalwrap: {
        // height: '80%',
        width: '100%',
        backgroundColor: '#fff',
        // padding: 15,
        // display: 'flex',
        justifyContent: 'space-between',
        overflow: 'hidden',
        // alignItems: 'center'
    },

    homeModalHeader: {
        width: '100%',
        backgroundColor: '#fff',
        padding: 25,
        display: 'flex',
        alignItems: 'center'
    },
    homeModalContentWrap: {
        width: '100%',
        padding: 18,
        paddingTop: 0,
        display: 'flex',
        alignItems: 'center'
    },
    homeModalwrapText1: {
        fontSize: 60,
        fontSize: 16,
        color: '#FAFAFA',
    },
    btnSave: {
        height: 54,
        backgroundColor: '#1E93E2',
        elevation: 0,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    noteItemsWrap: {
        display: 'flex',
        marginBottom: 40,
        paddingLeft: 18,
        paddingRight: 18,
        width: '100%',
    },
    noteItems: {
        flexDirection: 'row',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        // marginBottom: 13,
        marginTop: 8,
        width: '100%',
    },
    // Modal Style ends=========================================================





});

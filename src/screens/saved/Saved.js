import React, { Component, Fragment } from 'react';
import { Text, StyleSheet, ImageBackground, Keyboard, TouchableOpacity, Alert } from 'react-native';
import {
    KeyboardAvoidingView, Image, ScrollView, FlatList
} from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons';
import Checker from '../../assets/workbrookAssets/checker.png';
import {
    Container,
    Header,
    Content,
    Textarea,
    Button,
    View,
    Thumbnail,
    Form,
    Item,
    Input,
    Label,
    Badge,
    Icon,
    Footer,
    Picker,
    Item as FormItem,
    Left,
    Body,
    CardItem,
    Card,
    Right,
    ListItem,
    List,
    CheckBox,
} from 'native-base';
import {
    Kaede,
    Hoshi,
    Jiro,
    Isao,
    Madoka,
    Akira,
    Hideo,
    Kohana,
    Makiko,
    Sae,
    Fumi,
} from 'react-native-textinput-effects';
import CustomizeHeader from '../frags/CustomizeHeader';

import { myspiner, temp, ShowNotification, AppInlineLoader, getLodgedInDetailsProfile, _DocumentPicker, cloudinaryUpload } from '../../dependency/UtilityFunctions';
import Logo from '../../assets/workbrookAssets/logo.png';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import RBSheet from "react-native-raw-bottom-sheet";
import Interests from '../interests/Interests';
import Noti from "../../assets/workbrookAssets/noti.png";
import UserBl from "../../assets/workbrookAssets/userBl.png";
import ArrForward from "../../assets/workbrookAssets/arrfr.png";
import Shield from "../../assets/workbrookAssets/shield.png";
import Quest from "../../assets/workbrookAssets/quest.png";
import Info from "../../assets/workbrookAssets/info.png";
import Plus from "../../assets/workbrookAssets/plus.png";
import Remark from "../../assets/workbrookAssets/remark.png";
import Like from "../../assets/workbrookAssets/like.png";
import MoreIcon2 from "../../assets/workbrookAssets/more2.png";
import Profile from "../../assets/images/profile/dp2.png";
import cardPix from "../../assets/images/background/bg3_old.jpg";
import RNPickerSelect from 'react-native-picker-select';
import TagInput from 'react-native-tags-input';
import MoreIcon from "../../assets/workbrookAssets/more.png";
import Circle from '../../assets/workbrookAssets/circle.png';

import RNFetchBlob from 'react-native-fetch-blob'
import ImagePicker from 'react-native-image-picker';
import CloseModal from "../../assets/workbrookAssets/whitecloss.png";
import SuccessIcon from "../../assets/workbrookAssets/successPost.png";
import Drop from "../../assets/workbrookAssets/drop.png";
import Modal from 'react-native-modal';
import AccountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import { text } from 'react-native-communications';
import DashboardStore from '../../stores/Dashboard';
import { hasSetIntrest } from '../../auth';
import Autocomplete from 'react-native-autocomplete-input';
import MultiSelect from 'react-native-multiple-select';
import { YOUR_CLOUDINARY_PRESET, YOUR_CLOUDINARY_NAME, requestCameraPermission, YOUR_CLOUDINARY_PRESET_JobBanner } from '../../dependency/UtilityFunctions';
import EmptyListComponent from '../../StatelessScreens/EmptyListComponent';
import { SvgUri } from 'react-native-svg';


@observer
class Saved extends Component {
    constructor(props) {
        super(props);
        this.mapRef = null;
        this.state = {
            profile: {}



        }
        this.ApplyFOrJobOutSide = this.ApplyFOrJobOutSide.bind(this)
        this.toggleModal = this.toggleModal.bind(this)
        this.toggleModalClose = this.toggleModalClose.bind(this)
        this.ApplyForJob = this.ApplyForJob.bind(this)
        this.ChangeResume = this.ChangeResume.bind(this)
        this.toggleApplyModalShow = this.toggleApplyModalShow.bind(this)
        this.ReloadPageData = this.ReloadPageData.bind(this);
        this.gotoPreviewPostPage = this.gotoPreviewPostPage.bind(this);

    }

    async componentDidMount() {

        DashboardStore.__GetSavedJobs(1, 30)

        let InnerProfile = await getLodgedInDetailsProfile();
        let isAvailable = InnerProfile.isAvailable;
        if (isAvailable === true) {

            let this_userprofile = InnerProfile.storeData;
            console.log(this_userprofile)
            console.log("InnerProfile")
            this.setState({
                profile: this_userprofile
            })

            DashboardStore._UpdateLoggedInUserProfile(this_userprofile);
        }
    }
    toggleModal = (item) => {
        console.log(item);


        DashboardStore._updateParameterV2(item, "SingleJobOffer", "JobOppurtunity");

        DashboardStore._ToggleHomeModal(true)
    };
    toggleModalClose = () => {
        DashboardStore._ToggleHomeModal(false)
    };
    // retrieveMore = () => {
    retrieveMore = async () => {
        // alert("Loading More data...")
        ShowNotification("Loading More data...", true);

    };
    toggleRefModalShow = () => {
        DashboardStore._ToggleHomeModal(false)
        DashboardStore._ToggleReferModal(true)
    };
    toggleRefModalClose = () => {
        DashboardStore._ToggleReferModal(false)
    };
    ApplyFOrJobOutSide = (item) => {
        DashboardStore._updateParameterV2(item, "SingleJobOffer", "JobOppurtunity");

        DashboardStore._ToggleHomeModal(false)
        DashboardStore._ToggleApplyModal(true)
    };
    LikeAndUnlikeJob = (item) => {
        let job_id = item.id;
        DashboardStore.__LikeUnLike(job_id);
    };
    SaveJobForLater = () => {
        let item = DashboardStore.JobOppurtunity.SingleJobOffer

        let job_id = item.id;
        DashboardStore.__SaveJobForLater(job_id);
    };
    toggleApplyModalShow = () => {
        // DashboardStore._updateParameterV2(item, "SingleJobOffer", "JobOppurtunity");

        DashboardStore._ToggleHomeModal(false)
        DashboardStore._ToggleApplyModal(true)
    };
    toggleApplyModalClose = () => {
        DashboardStore._ToggleApplyModal(false)
    };

    toggleSuccessModalShow = () => {
        DashboardStore._ToggleApplyModal(false)
        DashboardStore._ToggleSucessModal(true)
    };
    ApplyForJob = () => {
        // DashboardStore._ToggleApplyModal(false)
        // DashboardStore._ToggleSucessModal(true)
        // alert("heyman")
        DashboardStore.__ApplyForJob();

    };

    ChangeResume = async () => {
        // alert("kkkkk")
        let resp = await _DocumentPicker();
        console.log(resp);
        console.log("tuface");
        if (resp !== false || resp !== "cancel") {

            const source = {
                uri: resp.uri,
                type: resp.type,
                name: resp.name,
            }
            this.setState({
                filename: resp.name,
            })
            console.log("Bacl from picker")
            let FileUrl = await cloudinaryUpload(source);
            if (FileUrl !== false) {

                console.log("FileUrl");
                console.log(FileUrl);
                console.log("FileUrl_done");
                DashboardStore._updateParameterV2(FileUrl, "resume", "profileupdatedata");
                //   ShowNotification('Done Uploading ....', true);
                DashboardStore.__UpdateProfile_external();
                ShowNotification('Done Saving your cv', true);

            }


        }


    };

    toggleSuccessModalClose = () => {
        // DashboardStore._ToggleSucessModal(false)
        DashboardStore._ToggleApplyModal(false)
        DashboardStore.isJobApplySuccessful = false;

    };
    ReloadPageData = () => {
        DashboardStore.__GetSavedJobs(1, 30)

    };
    // ReloadPageData = () => {
    //     DashboardStore.__GetReruitersRequest(1, 10);

    //   };
    // AcceptNow = () => {
    //     DashboardStore.__GetReruitersRequest(1, 10);

    //   };
    gotoPreviewPostPage = (item, page) => {
        // console.log(item);


        DashboardStore._updateParameterV2(item, "SingleJobOffer", "JobOppurtunity");
        this.props.navigation.navigate(page)
    };

    render() {
        const { MySavedJobs } = DashboardStore;
        const { profile } = this.state;
        const { LoggedInUserProfile } = DashboardStore;
        let Fullname = '';
        if (LoggedInUserProfile.first_name == null || LoggedInUserProfile.last_name == null) {
            Fullname = `${LoggedInUserProfile.username} `;
        }
        else {
            Fullname = `${LoggedInUserProfile.first_name} ${LoggedInUserProfile.last_name} `

        }
        let ErrorComponentDisplay = null;
        console.log(DashboardStore.ShortlistedApplicant);

        if (DashboardStore.isProcessing === true) {
            ErrorComponentDisplay = AppInlineLoader(DashboardStore.isProcessing)

        }
        else {
            ErrorComponentDisplay = <EmptyListComponent
                message={DashboardStore.ShortlistedApplicant.ErrorMessage}
            //  onclickhandler={this.LoadAvailableProffesions}
            />
        }
        const { SingleJobOffer } = DashboardStore.JobOppurtunity;
        let SkillRequired = null;
        if (Object.keys(SingleJobOffer).length > 0 && SingleJobOffer.constructor === Object) {
            SkillRequired = SingleJobOffer.skills_required.map((item, index) => {


                return (
                    <ListItem avatar style={[styles.ListItemWrapCircle, { marginLeft: 0, paddingLeft: 15, height: 25 }]} key={index}>
                        <Left style={styles.ListLeftSty}>
                            <View style={{ width: 15, }}>
                                <Thumbnail square source={Circle} style={styles.circlesty} />
                            </View>
                            <Text style={[styles.cupText1Sz2, { marginLeft: 10 }]}>
                                {/* Earn points by completing tasks */}
                                {item}

                            </Text>
                        </Left>
                    </ListItem>
                )


            });

        }





        let MySavedJobsView =
            <FlatList
                // Data
                data={MySavedJobs.MySavedjoblist}
                // Render Items
                renderItem={({ item }) => {
                    let poster = item.created_by_profile
                    let posterName = poster.first_name == null || poster.last_name == null ? poster.username : `${poster.first_name}  ${poster.last_name} `
                    let posterImage = poster.image_url;
                    let jobLinks = item.additional_files;
                    let jobBanner = jobLinks[0];
                    return (

                        <Card style={{ elevation: 1, paddingTop: 15, paddingBottom: 8, borderRadius: 5, backgroundColor: '#F3F3F3', borderWidth: 0, marginTop: 17, paddingLeft: 15, paddingRight: 15, }}>
                            <CardItem style={[styles.rmPadding, { paddingBottom: 5, backgroundColor: 'transparent', borderBottomWidth: 1, borderBottomColor: '#E5E5E5' }]}>
                                <Left style={[styles.rmPadding, { flex: 1, }]}>
                                    <Body style={[styles.rmPadding], { paddingLeft: 0, marginLeft: 0 }}>
                                        <Text style={[styles.cardTextJob5_1, { color: '#2F2F2F' }]}>
                                            {/* {posterName} */}
                                            {item.role_title}

                                        </Text>
                                        <Text style={styles.cardTextJob3}>
                                            {`${item.campany_name} , ${item.location}`}

                                        </Text>
                                    </Body>
                                </Left>

                            </CardItem>
                            <CardItem style={{ display: 'flex', flexDirection: 'row', alignItems: 'flex-start', backgroundColor: 'transparent', paddingLeft: 0, paddingRight: 0, }}>

                                <Text style={[styles.cardTextJob3, { fontSize: 11, fontWeight: '300' }]}>
                                    {item.applicant_benefit}
                                </Text>

                            </CardItem>

                            <CardItem style={[styles.rmPaddingLR, { borderBottomWidth: 0, display: 'flex', flexDirection: 'row', backgroundColor: 'transparent', }]}>

                                <Button full style={[styles.btnActionWrap]}
                                    // onPress={() => this.ApplyFOrJobOutSide(item)}
                                    onPress={() => this.gotoPreviewPostPage(item, 'PreviewPost')}

                                >
                                    <Text style={styles.cardTextJob6}>See more</Text>
                                </Button>

                            </CardItem>
                        </Card>

                    )
                }}
                // Item Key
                keyExtractor={(item, index) => String(index)}
            // Header (Title)
            // ListHeaderComponent={this.renderHeader}
            // Footer (Activity Indicator)
            // ListFooterComponent={this.renderFooter()}
            // On End Reached (Takes a function)
            // onEndReached={this.retrieveMore}
            // How Close To The End Of List Until Next Data Request Is Made
            // onEndReachedThreshold={0}
            // Refreshing (Set To True When End Reached)
            // refreshing={this.state.refreshing}
            />

        return (

            <Container style={styles.container}>
                <CustomizeHeader
                    leftside='WhiteArr'
                    title='Saved'
                    rightside='empty'
                    statedColor='#1E93E2'
                    icon='md-close'
                // savehandler={this.proceednow}
                />

                <ScrollView
                    contentContainerStyle={{ paddingLeft: 30, paddingRight: 30, paddingBottom: 30, paddingTop: 23 }}
                >


                    {
                        MySavedJobs.MySavedjoblist.length > 0 ?
                            MySavedJobsView
                            :
                            ErrorComponentDisplay

                    }


                </ScrollView>

                <Modal style={styles.MContainer} isVisible={DashboardStore.isApplyModalVisible2} >
                    <Button onPress={this.toggleApplyModalClose} style={{ backgroundColor: "transparent", position: 'absolute', right: 10, top: 40, padding: 20, elevation: 0 }}>
                    <SvgUri
                                    width="19px"
                                    height="19px"
                                    uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593711047/Mobile_Assets/closeWht_oohxti.svg" />
                        </Button>
                    <View style={[styles.Mwrapper, { height: 400 }]}>
                        <View style={{ paddingTop: 42, paddingLeft: 42, paddingRight: 42, paddingBottom: 10 }}>
                            <Text style={styles.cardText1, { fontSize: 18, color: '#1E93E2', textAlign: 'center', fontWeight: 'bold' }}>
                                {SingleJobOffer.role_title}
                            </Text>
                            <Text style={styles.cardText1, { fontSize: 11, color: '#1E93E2', textAlign: 'center', fontWeight: 'normal' }}>
                                {/* Guaranty Trust Bank, Lagos */}
                                {`  ${SingleJobOffer.campany_name}  ${SingleJobOffer.location}`}
                            </Text>

                            <Item style={[styles.profileinput, { marginTop: 47 }]}>
                                <Input
                                    style={styles.profileinputText}
                                    placeholderTextColor={'#A7A7A7'}
                                    placeholder="Your full name"
                                    defaultValue={Fullname}
                                />
                            </Item>

                            <Item style={[styles.profileinput,]}>
                                <Input
                                    style={styles.profileinputText}
                                    placeholderTextColor={'#A7A7A7'}
                                    placeholder="Your email address"
                                    defaultValue={LoggedInUserProfile.email}

                                />
                            </Item>

                            {/* <Item style={[styles.profileinput,]}>
                                            <Input
                                                style={styles.profileinputText}
                                                placeholderTextColor={'#A7A7A7'}
                                                placeholder="Your phone number"
                                                defaultValue={Fullname}

                                            />
                                        </Item> */}

                            {LoggedInUserProfile.resume != null ?

                                <React.Fragment>
                                    <CardItem style={{ width: "100%", backgroundColor: "transparent", paddingTop: 0 }}>
                                        <Left style={{ alignItems: 'flex-start', borderBottomWidth: 1, borderColor: '#E5E5E5', paddingBottom: 5 }}>
                                            <Button style={[styles.btnChecky, { borderWidth: 0, paddingLeft: 0, paddingRight: 0 }]}
                                                onPress={() => Communicator('web', LoggedInUserProfile.resume)}

                                            >
                                                <Text style={styles.btnCheckyText}>{` Your Current Resume`}</Text>
                                            </Button>
                                        </Left>
                                    </CardItem>
                                    <View>
                                        <Text>
                                            OR
    </Text>
                                    </View>

                                    <React.Fragment>
                                        <Button full transparent style={{ elevation: 0, height: 20, marginTop: 20 }}
                                            onPress={this.ChangeResume}
                                        >
                                            <Text style={[styles.cardTextJob4, { color: '#1E93E2', fontSize: 16 }]}>
                                                Upload your CV (max 2MB)
        </Text>

                                        </Button>
                                        <View>
                                            <Text style={[styles.cardTextJob4, { color: '#1E93E2', fontSize: 12 }]}>
                                                {this.state.filename}
                                            </Text>
                                        </View>
                                    </React.Fragment>
                                </React.Fragment>

                                :

                                <React.Fragment>
                                    <Button full transparent style={{ elevation: 0, height: 20, marginTop: 20 }}
                                        onPress={this.ChangeResume}
                                    >
                                        <Text style={[styles.cardTextJob4, { color: '#1E93E2', fontSize: 16 }]}>
                                            Upload your CV (max 2MB)
        </Text>

                                    </Button>
                                    <View>
                                        <Text style={[styles.cardTextJob4, { color: '#1E93E2', fontSize: 12 }]}>
                                            {this.state.filename}
                                        </Text>
                                    </View>
                                </React.Fragment>

                            }

                        </View>
                        {LoggedInUserProfile.resume != null ?
                            <Button style={[styles.btnSave, { marginTop: 15, borderRadius: 0 }]}
                                onPress={this.ApplyForJob}
                            >
                                <Text style={[styles.btnCheckyText, { color: '#fff' }]}>{DashboardStore.isProcessing === true ? 'Processing...' : 'Apply'}</Text>
                            </Button>
                            :
                            <Button style={[styles.btnSave, { marginTop: 15, borderRadius: 0 }]}
                                //  onPress={this.ApplyForJob}
                                disabled={true}
                            >
                                <Text style={[styles.btnCheckyText, { color: '#fff' }]}>{DashboardStore.isProcessing === true ? 'Processing' : 'Upload your cv before procceding'}</Text>
                            </Button>

                        }
                    </View>
                </Modal>
                <Modal style={styles.MContainer} isVisible={DashboardStore.isJobApplySuccessful} >
                    <Button onPress={this.toggleSuccessModalClose} style={{ backgroundColor: "transparent", position: 'absolute', right: 10, top: 40, padding: 20, elevation: 0 }}>
                        <SvgUri
                                    width="19px"
                                    height="19px"
                                    uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593711047/Mobile_Assets/closeWht_oohxti.svg" />
                        </Button>
                    <View style={[styles.Mwrapper, { height: 380 }]}>
                        <View style={{ paddingLeft: 29, paddingRight: 29, display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                            <SvgUri
                            style={{marginBottom: 17 }}
                            width="129px"
                            height="129px"
                             uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1590846662/Mobile_Assets/checkBlue_ajznt3.svg"
                            />
                            <Text style={styles.cardText1, { fontSize: 16, color: '#1E93E2', textAlign: 'center', fontWeight: '600' }}>
                                Congratulations!
                                        </Text>
                            <Text style={styles.cardText1, { fontSize: 16, color: '#1E93E2', textAlign: 'center', fontWeight: '600' }}>
                                You have successfully applied.
                                        </Text>


                        </View>

                    </View>
                </Modal>

            </Container>


        );
    }
}
export default Saved;

const styles = StyleSheet.create({
    container: {
        backgroundColor: "white",
        display: "flex",
        flex: 1,
        justifyContent: "flex-start",
    },
    imageHolder: {
        display: "flex",
        alignItems: "center",
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 25,
        paddingBottom: 25,
        backgroundColor: '#000000',

    },
    recruiterInfoWrap: {
        display: "flex",
        alignItems: "flex-start",
        flexDirection: 'row',
        paddingLeft: 15,
        paddingRight: 15,
        marginTop: 10
    },
    recruiterInfoText1Wrap: {
        display: "flex",
        alignItems: "flex-start",
        flexDirection: 'row',
        color: '#A7A7A7',
        fontSize: 10,
        fontWeight: '300',
        width: 100,
        paddingLeft: 5,
    },
    recruiterInfoText2Wrap: {
        flex: 1,
        display: "flex",
        alignItems: "flex-start",
        flexDirection: 'row',
        paddingLeft: 5,
        // paddingRight: 15,
        color: '#3C3C3C',
        fontSize: 12,
        fontWeight: 'normal',
    },
    btnNewPost: {
        height: 38,
        elevation: 0,
        minWidth: '90%',
        marginLeft: 15,
        marginRight: 15,
    },
    IconSetsty1: {
        maxWidth: 16,
        maxHeight: 17,
    },
    IconSetsty: {
        maxWidth: 16,
        maxHeight: 16,
    },
    iconWrapSet: {
        width: 20,
        marginRight: 10
    },
    IconSetsty2: {
        width: 8,
        height: 15,

    },
    bottomIndBorderNon: {
        marginLeft: 15,
        marginRight: 15,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        // borderBottomWidth: 1,
        // borderColor: '#E5E5E5'
    },
    bottomIndBorder: {
        marginLeft: 15,
        marginRight: 15,
        borderBottomWidth: 1,
        borderColor: '#E5E5E5',
    },
    rmPadding: {
        paddingTop: 0,
        paddingRight: 0,
        paddingBottom: 0,
        paddingLeft: 0,
    },
    rmPaddingLR: {
        paddingLeft: 0,
        paddingRight: 0,
    },

    cardTextJob1: {
        color: "#2F2F2F",
        fontSize: 11,
    },
    cardTextJob2: {
        color: "#676767",
        fontSize: 9,
        fontWeight: '300',
    },
    cardTextJob2_2: {
        color: "#2F2F2F",
        fontSize: 9,
        fontWeight: '500',
    },
    cardTextJob3: {
        color: "#2F2F2F",
        fontSize: 11,
        fontWeight: '300',
    },
    cardTextJob6: {
        color: "#FAFAFA",
        fontSize: 14,
        fontWeight: '600',
    },
    cardTextJob4: {
        color: "#1E93E2",
        fontSize: 11,
        fontWeight: '500',
    },
    cardTextJob4_1: {
        color: "#fff",
        fontSize: 12,
        fontWeight: '500',
    },
    cardTextJob5: {
        color: "#2F2F2F",
        fontSize: 10,
        fontWeight: 'bold',
    },
    cardTextJob5_1: {
        color: "#000000",
        fontSize: 13,
        fontWeight: '500',
    },

    cardTextJob7: {
        fontSize: 12,
        color: '#2F2F2F',
    },
    cardTextJob7: {
        fontSize: 16,
        fontWeight: '600',
        color: '#FAFAFA',
    },
    Mwrapper: {
        height: 510,
        width: '90%',
        backgroundColor: '#fff',
        // padding: 15,
        display: 'flex',
        justifyContent: 'space-around',
        // alignItems: 'center'
    },
    cardText1: {
        color: "#2F2F2F",
        fontWeight: '500',
        fontSize: 12,
    },
    MContainer: {
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
    },
    termConditionWrap: {
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
    },
    likenoti: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        paddingBottom: 16,
        paddingTop: 16,
    },
    // =============================================== drop address ==================
    textIcon: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center'
    },
    nameSty2: {
        fontSize: 12,
        color: '#FAFAFA'
    },
    // =============================================== drop address ==================
    // =============================================== card style ========================
    rmPadding: {
        paddingTop: 0,
        paddingRight: 0,
        paddingBottom: 0,
        paddingLeft: 0,
    },
    btnActionWrap: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#5ABC7A',
        height: 54,
        elevation: 0,
        flex: 1,
    },

    // =============================================== Card style =====================

    // Modal style =============================================================
    MContainer: {
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
    },
    Mwrapper: {
        height: 510,
        width: '90%',
        backgroundColor: '#fff',
        // padding: 15,
        display: 'flex',
        justifyContent: 'space-around',
        // alignItems: 'center'
    },
    homeModalwrap: {
        // height: 519,
        width: '90%',
        backgroundColor: '#fff',
        // padding: 15,
        // display: 'flex',
        justifyContent: 'space-between',
        // alignItems: 'center'
    },
    homeModalHeader: {
        height: 89,
        width: '100%',
        backgroundColor: '#1E93E2',
        padding: 15,
        display: 'flex',
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    homeModalContentWrap: {
        width: '100%',
        backgroundColor: '#fff',
        padding: 35,
        paddingBottom: 20,
        display: 'flex',
    },
    homeModalwrapText1: {
        fontSize: 60,
        fontSize: 16,
        color: '#FAFAFA',
    },
    btnSave: {
        height: 54,
        backgroundColor: '#1E93E2',
        elevation: 0,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    // Modal Style ends=========================================================
    btnCheckyText: {
        fontSize: 11,
        fontWeight: 'bold',
        color: '#1E93E2'
    },
});

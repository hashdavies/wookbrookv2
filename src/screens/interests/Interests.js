import React, { Component, Fragment } from 'react';
import { Text, StyleSheet, ImageBackground, Keyboard, TouchableOpacity ,  FlatList,} from 'react-native';
import { KeyboardAvoidingView, Image, ScrollView } from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons';
import Checker from '../../assets/workbrookAssets/checker.png';
import {
    Container,
    Header,
    Content,
    Textarea,
    Button,
    View,
    Thumbnail,
    Form,
    Item,
    Input,
    Label,
    Badge,
    Icon,
    Footer,
    Picker,
    Item as FormItem,
} from 'native-base';
import {
    Kaede,
    Hoshi,
    Jiro,
    Isao,
    Madoka,
    Akira,
    Hideo,
    Kohana,
    Makiko,
    Sae,
    Fumi,
} from 'react-native-textinput-effects';

import { myspiner, temp } from '../../dependency/UtilityFunctions';
import Logo from '../../assets/workbrookAssets/logo.png';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import RBSheet from "react-native-raw-bottom-sheet";

import AccountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import DashboardStore from '../../stores/Dashboard';
import EmptyListComponent from '../../StatelessScreens/EmptyListComponent';


@observer
class Landing extends Component {
    constructor(props) {
        super(props);
        this.mapRef = null;
        this.state = {
            email: '',
            password: '',
            errorloading: false,
            checker : false
        }
        this.SetCheckers = this.SetCheckers.bind(this)
        this.__SaveIntrest = this.__SaveIntrest.bind(this)
        this.__SkipSetIntrestNow = this.__SkipSetIntrestNow.bind(this)
    }
   async  componentDidMount() {
        let UserloginToken=await temp();
    // let loggedinUserdetails=await loggedinUserdetails();
    console.log(UserloginToken);
    DashboardStore._getAxiosInstance(UserloginToken);
    console.log("UserloginToken");
    DashboardStore._FetchAllIntrest(1,30);
    }
    SetCheckers() {
        this.setState({checker: !this.state.checker})
    }
    __ReloadPage = () => {
        DashboardStore._FetchAllIntrest(1,30);
    }
    SelectInterest = (params) => { 
        console.log(params)
         DashboardStore.__ToggleIntrestArray(params.title);
        
      };
    __SaveIntrest = () => { 
        // console.log(params)
         DashboardStore.__SaveMyIntrest();
        
      };
    __SkipSetIntrestNow = () => { 
        DashboardStore.ProceedWithIntrestRecord();
      };
    render() {
        const {IntrestSettings}=DashboardStore;
        let ErrorComponentDisplay=null;

        if (DashboardStore.isProcessing === true) {
          ErrorComponentDisplay = myspiner(DashboardStore.isProcessing)
         
        }
        else {
          ErrorComponentDisplay = <EmptyListComponent
          message={IntrestSettings.ErrorMessage}
          onclickhandler={this.__ReloadPage}
        />
        }
    //     const SuggestedIntrestListkkk=  
    //     <FlatList
    //     data={IntrestSettings.AllSystemIntrest.slice()}
    //     renderItem={({ item ,index}) =>{
    //         let This_myIntrest=IntrestSettings.SelectedSystemIntrest.slice();

    //         return(
    //         // DashboardStore.IntrestSettings.SelectedSystemIntrest
    //         <React.Fragment>

           
    //       <View style={[styles.flexbox,  This_myIntrest.includes(item.title)===true ? styles.BtnInterestActivate :styles.BtnInterest ]} key={index}  >
    //             <Button full style={[styles.BtnInterest]}
    //             onPress={()=> this.SelectInterest(item)}
    //             >
    //                   {
    //                         This_myIntrest.includes(item.title) === false ? <Fragment><Text>heyy</Text></Fragment> : <Image source = {{uri: "https://res.cloudinary.com/tomzyadexcloud/image/upload/v1586005439/checker.png"}} style={{height: 20, width: 20}} alt=""/>  
    //                 }
    //                 <Text style={[styles.buttonText], { fontSize: 12, color: '#FAFAFA' }}>
    //                     {item.title}
    //                 </Text>
    //             </Button>
    //         </View> 
    //         </React.Fragment>
          
    //             )}}
     
    
    //     keyExtractor={(item,index) => index.toString()} 
    // />
let allsettingIntrest=IntrestSettings.AllSystemIntrest.slice();
        let SuggestedIntrestList= allsettingIntrest.map((item,index) => {

            let singledata=item;
            let This_myIntrest=IntrestSettings.SelectedSystemIntrest.slice();

            return ( 
                <View style={[styles.flexbox]} key={index}  >
                <Button full style={[This_myIntrest.includes(item.title)===true ? styles.BtnInterestActivate :styles.BtnInterest ]}
                onPress={()=> this.SelectInterest(item)}
                >
                      {
                            This_myIntrest.includes(item.title) === false ? <Fragment></Fragment> : <Image source = {{uri: "https://res.cloudinary.com/tomzyadexcloud/image/upload/v1586005439/checker.png"}} style={{height: 20, width: 20}} alt=""/>  
                    }
                    <Text style={[styles.buttonText], { fontSize: 12, color: '#FAFAFA', textAlign: 'center' }}>
                        {item.title}
                    </Text>
                </Button>
            </View> 
  )
      
     
});

        return (
            <React.Fragment>
              {  myspiner(DashboardStore.isProcessing)}
                <Container style={styles.container}>
                    <View style={[styles.contentWrap,]}>
                        <Text style={styles.yourInetrest}>
                            Select your interests.
                        </Text>
                        <View style={{ marginTop: 8, width: '100%' }}>
                            <Text style={[styles.contentText, { marginBottom: 0 }]}>
                                Let us know what opportunities you will
                                {/* {JSON.stringify(DashboardStore.IntrestSettings.AllSystemIntrest)} */}
                            </Text>
                            <View style={{ marginTop: 0, display: 'flex', flexDirection: 'row', width: '100%' }}>
                                <Text style={styles.contentText}>
                                    be interested in.
                            </Text>
                                <Text style={[styles.contentText, { color: '#1E93E2', marginLeft: 5 }]}>
                                    You can choose more than one
                            </Text>
                            </View>

                        </View>
                    </View>
                    <ScrollView 
                    showsVerticalScrollIndicator={false}
                    >
                   
                    <View style={[styles.contentWrap2, { paddingTop: 10 }]}>
                    {IntrestSettings.AllSystemIntrest.length > 0 ?
                    
                    <React.Fragment>

                    {SuggestedIntrestList}

                    {/* <View style={[styles.flexbox,]}>
                        <Button full style={[styles.BtnInterest]}>
                            <Text style={[styles.buttonText], { fontSize: 12, color: '#FAFAFA', textAlign:'center' }}>
                                Finance
                            </Text>
                        </Button>
                    </View>
                    <View style={[styles.flexbox,]}>
                        <Button full style={[this.state.checker == false ? styles.BtnInterest : styles.BtnInterestActivate]}
                        onPress={() => {
                            this.SetCheckers();
                          }}
                        >
                            {
                             this.state.checker == false ? <Fragment></Fragment> : <Image source = {{uri: "https://res.cloudinary.com/tomzyadexcloud/image/upload/v1586005439/checker.png"}} style={{height: 20, width: 20}} alt=""/>  
                            }

                            
                            <Text style={[styles.buttonText], { fontSize: 12, color: '#FAFAFA', textAlign:'center' }}>
                            Marketing
                            </Text>
                        </Button>
                    </View>
                    */}
                    
           
                    </React.Fragment>
  :
  ErrorComponentDisplay
   
  
  }
                       
                    </View>
                    </ScrollView>
          <View style={[styles.contentWrap, { justifyContent: 'space-between', alignItems: 'flex-end', height: 40, flexDirection:'row' , width: '100%' }]}>
                       {
                           IntrestSettings.SelectedSystemIntrest.length >0 ?

                           <Button full style={[styles.loginBtn1, { backgroundColor: '#fff', height: 30, marginBottom: 0, textAlign: 'right', display:'flex', flexDirection:'row', justifyContent: 'flex-end', alignSelf: 'center' }]}
                           onPress={this.__SaveIntrest}
                           >
                            
                           <Text style={[styles.buttonText, { color: '#1E93E2', fontSize: 14, marginLeft: 2, width:102,}]}>
                          Done
                           </Text>
                       </Button>
                           :
                           null
                       }
                    
                        <Button full style={[styles.loginBtn1, { backgroundColor: '#fff', height: 30, marginBottom: 0, textAlign: 'right', display:'flex', flexDirection:'row', justifyContent: 'flex-end', alignSelf: 'center' }]}
                        onPress={  this.__SkipSetIntrestNow   }
                        >
                       
                            <Text style={[styles.buttonText, { color: '#1E93E2', fontSize: 14, marginLeft: 2, width:102,}]}>
                            I’ll do this later
                            </Text>
                        </Button>
                    </View>
                </Container>

            </React.Fragment>
        );
    }
}
export default Landing;

const styles = StyleSheet.create({
    container: {
        backgroundColor: "#fff",
        display: "flex",
        justifyContent: "space-between",
        paddingTop: 50,
        paddingRight: 30,
        paddingBottom: 10,
        paddingLeft: 30,
    },
    contentWrap: {
        // backgroundColor: "red",
        display: "flex",
    },
    contentWrap2: {
        // backgroundColor: "red",
        display: "flex",
        flexDirection: 'row',
        flexWrap: "wrap",
        justifyContent: 'flex-start', 
    },
    contentText: {
        color: "#2F2F2F",
        fontSize: 12,
        textAlign: 'justify',
    },
    loginBtn1: {
        height: 56,
        borderRadius: 5,
        marginBottom: 15,
        elevation: 0,
    },
    loginBtn2: {
        height: 30,
        width: 30,
        borderRadius: 30 / 2,
        elevation: 1,
        backgroundColor: 'transparent',
        // backgroundColor: '#1E93E2',
    },
    buttonText: {
        color: "#FAFAFA",
        fontSize: 14,
        fontWeight: "600",
    },
    styleBtmSty: {
        width: '100%',
        padding: 15,
        flex: 1,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    styleBtmSty2: {
        width: '90%',
        height: '70%',
        padding: 10,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        borderRadius: 6,
    },
    inputSty: {
        fontSize: 14,
        fontWeight: "300",
    },

    //   ===============================================
    yourInetrest: {
        fontSize: 36,
        fontWeight: "600",
        color: "#1E93E2",
    },
    flexbox: {
        display: "flex",
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        padding: 3,
        minWidth: 95,
        maxWidth: '33.3%',
        height: 100,
    },
    BtnInterest: {
        height: 92,
        width: 92,
        minWidth: 92,
        borderRadius: 92 / 2,
        backgroundColor: '#1E93E2',
        elevation: 0,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection : 'column',
        textAlign: 'center',
        
    },
    BtnInterestActivate: {
        height: 92,
        width: 92,
        minWidth: 92,
        borderRadius: 92 / 2,
        backgroundColor: '#0F4971',
        elevation: 0,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection : 'column',
        
    },

});

import React, { Component } from 'react';
import {
    Text,
    Image,
    StyleSheet,
    ImageBackground,
    Keyboard,
    TouchableOpacity
} from 'react-native';
import { KeyboardAvoidingView } from 'react-native';
import CustomizeHeader from '../frags/CustomizeHeader';

import Ionicons from 'react-native-vector-icons/Ionicons';
import {
    Container,
    Header,
    Content,
    Textarea,
    Button,
    View,
    Thumbnail,
    Form,
    Item,
    Input,
    Label,
    Badge,
    Icon,
    Footer,
    Picker,
    Left,
    Right,
    Body,
    Title,
    Center,
    CardItem,
    CheckBox,
    ListItem,
    Card,
} from 'native-base';
import {
    Kaede,
    Hoshi,
    Jiro,
    Isao,
    Madoka,
    Akira,
    Hideo,
    Kohana,
    Makiko,
    Sae,
    Fumi,
} from 'react-native-textinput-effects';
import Modal from 'react-native-modal';
import { myspiner } from '../../dependency/UtilityFunctions';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import User from "../../assets/workbrookAssets/user4.png";
import { withNavigation } from 'react-navigation';
import accountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import { ScrollView } from 'react-native-gesture-handler';
import imageUser from '../../assets/workbrookAssets/user2.png';
import DatePicker from 'react-native-datepicker'
// import Autocomplete from 'native-base-autocomplete'
import Autocomplete from 'react-native-autocomplete-input';
import GridView from 'react-native-gridview';
import { SvgUri } from 'react-native-svg';
const API = 'https://swapi.co/api';

@observer
class MyCards extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            isModalVisible: false,
            isModalViewApplicants: false,
        }

    }
    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible });
    };
    toggleModalViewApplicants = () => {
        this.setState({ isModalViewApplicants: !this.state.isModalViewApplicants });
    };

    componentDidMount() {
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: '#fff' }}>
                {/* <CustomizeHeader
                    leftside='WhiteArr'
                    title='Recruiter Insights'
                    rightside='empty'
                    statedColor='#1E93E2'
                    icon='md-close'
                /> */}
                <ScrollView
                    contentContainerStyle={{
                        paddingLeft: 26,
                        paddingRight: 26,
                        paddingBottom: 26,
                        paddingTop: 26
                    }}
                >
                    <Card style={styles.CardSty2}>
                        <View style={{ width: "100%", display: 'flex', flexDirection: 'row', justifyContent: "space-between" , }}>
                            <Left style={{ flex: 2 ,}}>

                                <SvgUri

                                    width="42.58"
                                    height="26.61"
                                    uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1592684286/Mobile_Assets/masterCard_unpe0g.svg"
                                >

                                </SvgUri>

                            </Left>
                            <Right style={{ flex: 1, }}>
                                <ListItem style={{ borderBottomColor: "transparent", width: '100%' }}>
                                    <CheckBox style={styles.checkBoxSty} checked={true} />
                                </ListItem>
                            </Right>
                        </View>
                        <View style={{marginBottom:21, marginTop: 21, paddingRight: 21,}}>
                            <Text style={[styles.Text6, { color: "#2F2F2F" , marginBottom:5}]}>
                                **** **** **** 3446
                            </Text>
                            <Text style={[styles.Text9,]}>
                                5334
                            </Text>
                        </View>
                        <View style={{ width: "100%", display: 'flex', flexDirection: 'row', justifyContent: "space-between" }}>
                            <Left style={{ flex: 2 }}>
                                <Text style={styles.Text4}>
                                    Adekunle Ciroma Chukwu
                                </Text>
                            </Left>
                            <Right style={[styles.changeView, { flex: 1 }]}>
                                <View style={{display: 'flex'}}>
                                    <Text style={[styles.Text9, {fontSize: 5}]}>
                                        VALID
                                    </Text>
                                    <Text style={[styles.Text9, {fontSize: 5}]}>
                                        THRU
                                    </Text>
                                </View>
                                <Text style={[styles.Text9, {fontSize: 9}]}>09/20</Text>
                            </Right>
                        </View>

                    </Card>

                    <Card style={styles.CardSty2}>
                        <View style={{ width: "100%", display: 'flex', flexDirection: 'row', justifyContent: "space-between" , }}>
                            <Left style={{ flex: 2 ,}}>

                                <SvgUri

                                    width="69.85"
                                    height="21.18"
                                    uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1592684286/Mobile_Assets/visacard_cigin6.svg"
                                >

                                </SvgUri>

                            </Left>
                            <Right style={{ flex: 1, }}>
                                <ListItem style={{ borderBottomColor: "transparent", width: '100%' }}>
                                    <CheckBox style={styles.checkBoxSty} checked={true} />
                                </ListItem>
                            </Right>
                        </View>
                        <View style={{marginBottom:21, marginTop: 21, paddingRight: 21,}}>
                            <Text style={[styles.Text6, { color: "#2F2F2F" , marginBottom:5}]}>
                                **** **** **** 3446
                            </Text>
                            <Text style={[styles.Text9,]}>
                                5334
                            </Text>
                        </View>
                        <View style={{ width: "100%", display: 'flex', flexDirection: 'row', justifyContent: "space-between" }}>
                            <Left style={{ flex: 2 }}>
                                <Text style={styles.Text4}>
                                    Adekunle Ciroma Chukwu
                                </Text>
                            </Left>
                            <Right style={[styles.changeView, { flex: 1 }]}>
                                <View style={{display: 'flex'}}>
                                    <Text style={[styles.Text9, {fontSize: 5}]}>
                                        VALID
                                    </Text>
                                    <Text style={[styles.Text9, {fontSize: 5}]}>
                                        THRU
                                    </Text>
                                </View>
                                <Text style={[styles.Text9, {fontSize: 9}]}>09/20</Text>
                            </Right>
                        </View>

                    </Card>


                    <View style={styles.successCard2}>

                        <Button style={[styles.copyVisitBtn, { backgroundColor: '#1E93E2' }]}
                            onPress={() => { this.props.navigation.navigate("PaymentMethod") }}
                        >
                            <AntDesign name="pluscircleo" style={{ color: "#fff", marginRight: 13, }} />
                            <Text style={[styles.Text3, { color: '#fff', }]}>
                                Add Payment Method
                             </Text>
                        </Button>
                    </View>
                    <View style={styles.typeofCard}>
                        <Text style={styles.Text4}>Credit or Debit Card</Text>
                        <SvgUri
                            style={{ marginLeft: 5 }}
                            width='29px'
                            height='23px'
                            uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1592666483/Mobile_Assets/cardVisa_fx7udr.svg"
                        >

                        </SvgUri>
                        <SvgUri
                            style={{ marginLeft: 5 }}
                            width='29px'
                            height='23px'
                            uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1592666483/Mobile_Assets/cardmaster_jhtiqg.svg"
                        >
                        </SvgUri>

                    </View>



                </ScrollView>
                {/* modal views */}
                <Modal style={styles.MContainer} isVisible={this.state.isModalVisible}>
                    <View style={styles.Mwrapper}>
                        <View style={styles.firstView} >
                            <Button full style={styles.actionBtn}>
                                <Text style={styles.actionBtnText}>
                                    View the job poster’s profile
              </Text>
                            </Button>
                            <Button full style={[styles.actionBtn, { borderTopWidth: 1, borderTopColor: "#E5E5E5", borderBottomWidth: 1, borderBottomColor: "#E5E5E5" }]}>
                                <Text style={styles.actionBtnText}>
                                    Edit your profile
              </Text>
                            </Button>
                            <Button full style={styles.actionBtn}
                                onPress={this.toggleModalWithdrawApp}
                            >
                                <Text style={[styles.actionBtnText, { color: "#FF5964" }]}>
                                    Withdraw application
              </Text>
                            </Button>
                        </View>
                        <Button style={[styles.btnCancel, { marginTop: 15 }]} title="Hide modal" onPress={this.toggleModal}>
                            <Text style={[styles.btnCancelText,]}>Cancel</Text>
                        </Button>
                    </View>
                </Modal>
                {/* view applicants  modal*/}
                <Modal style={styles.MContainer} isVisible={this.state.isModalViewApplicants}>
                    <View style={styles.Mwrapper}>
                        <View style={styles.firstView} >
                            <Button full style={styles.actionBtn2}>
                                <Text style={[styles.actionBtnText, { fontWeight: '600', height: 20 }]}>
                                    Unique Application Link
                                </Text>
                            </Button>
                            <View style={styles.codeWrap}>
                                <Text style={[styles.Text4_2, {
                                    width: '100%', textAlign: 'center',
                                    marginBottom: 17
                                }]}>
                                    This link will be used by the recruiter to invite prospective applicants to the app as well as to apply for the respective job offer.
                                </Text>
                                <View style={[styles.codeDisplaySty, { marginBottom: 15 }]}>
                                    <Text style={[styles.Text4, { color: '#bcbcbc' }]}>
                                        uyvcucuaYUYQ_ADKBIUI7NOAIDOENBUBIUYibujvwd9374
                                    </Text>

                                </View>

                            </View>
                            <View style={styles.checkBoxWrap}>
                                <Button transparent style={styles.copylinkSty}
                                    onPress={this.toggleModalViewApplicants}>
                                    <Text style={styles.Text6}>
                                        Copy Link
                                    </Text>

                                </Button>

                            </View>
                        </View>
                    </View>
                </Modal>
                {/* Withdraw App modal*/}

                {/* modal views */}
            </View>
        );
    }
}
export default withNavigation(MyCards);

const inputStyles = {
    size: 100,
    color: '#FFFFFF',
};

const height = 40;

const styles = StyleSheet.create({
    Text1: {
        fontSize: 10,
        fontWeight: '500',
        color: "#2F2F2F",
        textAlign: 'center'
    },
    Text1_3: {
        fontSize: 10,
        fontWeight: '500',
        color: "#1E93E2",
        textAlign: 'center'
    },
    Text1_4: {
        fontSize: 10,
        fontWeight: '500',
        color: "#FF5964",
        textAlign: 'center'
    },
    Text1_2: {
        fontSize: 10,
        fontWeight: 'bold',
        color: "#2F2F2F",
        textAlign: 'center'
    },
    Text3: {
        fontSize: 12,
        fontWeight: '500',
        color: "#FAFAFA",
        textAlign: 'center'
    },
    Text7: {
        fontSize: 12,
        fontWeight: '500',
        color: "#2F2F2F",
        textAlign: 'center'
    },
    Text4: {
        fontSize: 13,
        fontWeight: '500',
        color: "#2F2F2F",
    },
    Text4_2: {
        fontSize: 12,
        fontWeight: 'normal',
        color: "#676767",
    },
    Text5: {
        fontSize: 11,
        fontWeight: '300',
        color: "#2F2F2F",
    },
    Text6: {
        fontSize: 14,
        fontWeight: '500',
        color: "#1E93E2",
    },
    Text8: {
        fontSize: 12,
        fontWeight: '600',
        color: "#1E93E2",
    },
    Text9: {
        fontSize: 6,
        fontWeight: '500',
        color: "#2F2F2F",
    },
    Text2: {
        fontSize: 18,
        fontWeight: '500',
        color: "#1E93E2",
    },
    likeTabsty: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    viewPerTabSty: {
        display: 'flex',
        alignItems: 'center',
        maxWidth: 79,
        textAlign: 'center',
    },
    successCard: {
        display: 'flex',
        alignItems: 'center',
        textAlign: 'center',
        justifyContent: "center",
        backgroundColor: '#F3F3F3',
        height: 100,
        // elevation: 1,
        marginTop: 30,
    },
    successCard2: {
        display: 'flex',
        // justifyContent: 'space-between',
        textAlign: 'center',
        // backgroundColor: '#F3F3F3',
        // height: 100,
        // marginTop: 17,
    },
    CardSty2: {
        display: 'flex',
        textAlign: 'center',
        paddingLeft:20,
        paddingBottom:20,
        paddingTop:20,
        // backgroundColor: '#F3F3F3',
        // height: 100,
        marginBottom: 20,
    },
    typeofCard: {
        display: 'flex',
        flexDirection: "row",
        justifyContent: 'flex-end',
        textAlign: 'center',
        // backgroundColor: '#F3F3F3',
    },
    cardinnerSty: {
        width: '100%',
        display: 'flex',
        alignItems: 'flex-end',
        textAlign: 'right',
        justifyContent: "center",
        backgroundColor: '#2f2f2f',
        height: 24,
        paddingRight: 12,
        paddingLeft: 12,
    },
    morestyIdc: {
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'center',
        width: 20
    },
    moreBtnSty: {
        height: 20,
        width: 20,
        borderRadius: 5,
        borderWidth: 0,
        borderColor: '#1E93E2',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    copylinkSty: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: 18,
    },
    xLayer: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingBottom: 14,
        paddingLeft: 19,
        paddingRight: 19,
        paddingTop: 14,
    },
    // ======================= modal style ==============================
    MContainer: {
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
    },
    Mwrapper: {
        // height: 360,
        width: '98%',
        // backgroundColor: '#fff',
        padding: 15,
        display: 'flex',
        justifyContent: 'space-around',
        // alignItems: 'center'
    },
    firstView: {
        backgroundColor: '#FAFAFA',
        borderRadius: 5,
        padding: 25,
    },
    actionBtn: {
        backgroundColor: '#FAFAFA',
        borderRadius: 5,
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
        height: 43,
    },
    actionBtn2: {
        backgroundColor: '#FAFAFA',
        borderRadius: 5,
        elevation: 0,
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
        height: 43,
    },
    codeWrap: {
        paddingTop: 14,
        paddingBottom: 14,
        // display: 'flex',
        // justifyContent: 'center',
        // flexDirection: 'row',
        // alignItems: 'center',
        // height: 43,
    },
    checkBoxWrap: {
        borderTopWidth: 1,
        borderTopColor: '#E5E5E5',
        paddingTop: 14,
        paddingLeft: 20,
        paddingRight: 20,
        // paddingBottom: 14,
        // display: 'flex',
        // justifyContent: 'center',
        // flexDirection: 'row',
        // alignItems: 'center',
        // height: 43,
    },
    actionBtnText: {
        fontSize: 12,
        color: '#1E93E2',
    },
    btnCancel: {
        backgroundColor: '#FF5964',
        borderRadius: 5,
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
        height: 43,
        elevation: 0,
    },
    btnCancelText: {
        fontSize: 12,
        color: '#FAFAFA',
        fontWeight: 'bold',
    },
    checkBoxText: {
        fontSize: 11,
        color: '#2F2F2F',
        marginLeft: 20
    },
    CheckBoxBody: {
        display: 'flex',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 10,
    },

    // ======================= modal style ==============================
    userFlex: {
        display: 'flex',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        alignItems: 'center',
        flexWrap: 'wrap',
        paddingLeft: 10,
        paddingRight: 10,
        // marginTop: 10,
    },
    successCard2Bottom: {
        display: 'flex',
        justifyContent: 'space-between',
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 19,
        paddingRight: 19,
        marginTop: 29,
        marginBottom: 17,
    },

    copyViewBtnWrap: {
        display: 'flex',
        alignItems: 'center',
        paddingLeft: 19,
        paddingRight: 19,
        marginTop: 20,
        marginBottom: 17,
    },
    card2Bottom: {
        height: 28,
        backgroundColor: '#5ABC7A',
        borderRadius: 5,
        elevation: 0,
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 14,
        paddingRight: 14
    },
    userFlexImageWrap: {
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 5,
        width: '16.6%'
    },
    userFlexImage: {
        width: 32,
        height: 34,
        borderRadius: 34 / 2,
    },
    ViewBtnSty: {
        height: 23,
        width: 50,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#1E93E2',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    ViewBtnRmSty: {
        height: 23,
        width: 50,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#FF5964',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 2,
    },
    copyVisitBtn: {
        height: 41,
        width: '100%',
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#1E93E2',
        display: 'flex',
        justifyContent: 'center',
        marginBottom: 15,
        alignItems: 'center',
        elevation: 0
    },
    codeDisplaySty: {
        borderWidth: 1,
        borderColor: "#E5E5E5",
        borderRadius: 3,
        padding: 15,
    },
    groupBtnWrap: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: "center"
    },
    changeView: {
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
    },
    checkBoxSty: {
       width: 27,
       height: 27,
       borderRadius: 27 / 2, 
       display: 'flex',
       justifyContent: "center",
       alignItems: "center",
    //    flexDirection: "row",
    },
});

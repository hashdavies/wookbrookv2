import React, { Component } from 'react';
import {
  Text,
  Image,
  StyleSheet,
  ImageBackground,
  Keyboard,
  TouchableOpacity
} from 'react-native';
import { KeyboardAvoidingView } from 'react-native';
import CustomizeHeader from '../frags/CustomizeHeader';

import Ionicons from 'react-native-vector-icons/Ionicons';
import {
  Container,
  Header,
  Content,
  Textarea,
  Button,
  View,
  Thumbnail,
  Form,
  Item,
  Input,
  Label,
  Badge,
  Icon,
  Footer,
  Picker,
  Left,
  Right,
  Body,
  Title,
  Center,
  CardItem
} from 'native-base';
import {
  Kaede,
  Hoshi,
  Jiro,
  Isao,
  Madoka,
  Akira,
  Hideo,
  Kohana,
  Makiko,
  Sae,
  Fumi,
} from 'react-native-textinput-effects';
import {
  TabView,
  TabBar,
  SceneMap,
  type NavigationState,
} from 'react-native-tab-view';
import { myspiner, getLodgedInDetailsProfile } from '../../dependency/UtilityFunctions';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import User from "../../assets/workbrookAssets/user4.png";

import accountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import { ScrollView } from 'react-native-gesture-handler';
import imageUser from '../../assets/workbrookAssets/user2.png';
import DatePicker from 'react-native-datepicker'
// import Autocomplete from 'native-base-autocomplete'
import Autocomplete from 'react-native-autocomplete-input';
import BankDetails from './BankDetails';
import Cards from './Cards';
import DashboardStore from '../../stores/Dashboard';

const API = 'https://swapi.co/api';
const Dimensions = require('Dimensions');
let deviceWidth = Dimensions.get('window').width;
let deviceHeight = Dimensions.get('window').height;
const ScreenSIze = deviceWidth / 2;
let w1 = deviceWidth / 3;
let w = w1 - 12;
let t = deviceHeight / 3;
const boxsize = (t + t) / 3;
let h = deviceHeight / 3;

type State = NavigationState<{
  key: string,
  title: string,
}>;
const initialLayout = {
  height: 0,
  width: Dimensions.get('window').width,
};

@observer
class RecruiterDashboard extends Component <*, State> {
  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      routes: [
        { key: 'Cards', title: 'My Cards' },
        { key: 'BankDetails', title: 'Insights ' },
      ],
      profile:{}
    }
  }

  async componentDidMount() {
    DashboardStore.UpdateNavigationState(this.props.navigation);
let InnerProfile= await getLodgedInDetailsProfile();
let isAvailable =InnerProfile.isAvailable;
if(isAvailable===true){

let this_userprofile=InnerProfile.storeData;
console.log(this_userprofile)
console.log("InnerProfile")
this.setState({
  profile:this_userprofile
})
  }
}
  // Tab function Start here  
  _handleIndexChange = index => this.setState({ index });

  _renderTabBar = props => (
    <TabBar
      {...props}
      scrollEnabled
      indicatorStyle={styles.indicator}
      style={styles.tabbar}
      tabStyle={styles.tab}
      contentContainerStyle={styles.styleinner}
      labelStyle={styles.label}
    />
  );

  _renderScene = SceneMap({
    Cards: Cards,
    BankDetails: BankDetails,
  });
  // Tab function ends here  


  render() {
    const{LoggedInUserProfile}=DashboardStore;
    let Fullname='';
    if(LoggedInUserProfile.first_name==null || LoggedInUserProfile.last_name==null){
          Fullname=   `${LoggedInUserProfile.username} `;
    }
    else{
          Fullname=  `${LoggedInUserProfile.first_name} ${LoggedInUserProfile.last_name} `

    }
    return (
      <Container>
         <CustomizeHeader
            leftside='WhiteArr'
            title='Payments'
            rightside='empty'
            statedColor='#1E93E2'
            icon='md-close'
          />
          {/* <View style={[styles.profileWrapinner, { backgroundColor: "#1E93E2" }]}>
            <Image 
          //  source={imageUser} 
            source={{uri:LoggedInUserProfile.image_url}}
            style={{ height: 48, width: 48, borderRadius: 48 / 2 }} />
            <Text style={styles.Text1}>{Fullname}</Text>
          </View> */}
          <View style={styles.topperContentSide2}>
            <TabView
              //  style={[styles.container2, this.props.style]}
              navigationState={this.state}
              renderScene={this._renderScene}
              renderTabBar={this._renderTabBar}
              onIndexChange={this._handleIndexChange}
              // initialLayout={initialLayout}
            />
          </View>
      </Container>
    );
  }
}
export default RecruiterDashboard;

const inputStyles = {
  size: 100,
  color: '#FFFFFF',
};

const height = 40;

const styles = StyleSheet.create({
  Text1: {
    fontSize: 13,
    fontWeight: '600',
    color: "#FAFAFA",
  },
  profileWrap: {
    flex: 1,
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: "#fff"
  },
  autocompleteContainer: {
    flex: 1,
    left: 0,
    position: 'absolute',
    right: 0,
    top: 0,
    zIndex: 1
  },

  profileWrapinner: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 0,
    paddingBottom:20
  },
  profileinput: {
    marginLeft: 0,
    borderWidth: 1,
    borderColor: '#E5E5E5',
    height: 55,
    // marginBottom: 15
  },
  profileinput2: {
    marginLeft: 0,
    // borderWidth: 1,
    // borderColor: '#E5E5E5',
    height: 55,
    color: "#2F2F2F",
    fontSize: 14,
    fontWeight: '500'
    // marginBottom: 15
  },
  profileinputText: {
    height: "100%",
    color: "#2F2F2F",
    fontSize: 14,
    fontWeight: '500'
  },
  devtrop: {
    display: "flex",
    justifyContent: "flex-start",
  },
  topperContentSide2: {
    flex: 1,
    // marginTop:-20,
    // height: 300,
    width: '100%',
    paddingBottom: 15,
    display: 'flex',
    flexDirection: "row",
    justifyContent: 'center',
    // backgroundColor : 'red'
  },
  minBold2: {
    fontSize: 11,
    color: '#2F2F2F'
  },
  tabbar: {
    backgroundColor: '#1E93E2',
    height: 40,
    width: '100%',
    // marginBottom:20,
    display: 'flex',
    flexDirection: "row",
    // justifyContent: 'center',
    alignItems: 'center',
    
  },
  tab: {
    width: ScreenSIze,
    display:"flex",
        flexDirection: "row",
        justifyContent:"center",
  },
  label: {
    color: '#FAFAFA',
    fontWeight: '500',
    fontSize: 12,
  },
  indicator: {
    backgroundColor: '#FAFAFA',
  },
  styleinner: {
    display: 'flex',
    flexDirection: "row",
    justifyContent: 'center',
  },
});

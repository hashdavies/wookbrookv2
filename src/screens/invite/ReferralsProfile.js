import React, { Component, Fragment } from 'react';
import { Text, StyleSheet, ImageBackground, Keyboard, TouchableOpacity, Alert, FlatList, ActivityIndicator } from 'react-native';
import { KeyboardAvoidingView, Image, ScrollView } from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons';
import Checker from '../../assets/workbrookAssets/checker.png';
import {
    Container,
    Header,
    Content,
    Textarea,
    Button,
    View,
    Thumbnail,
    Form,
    Item,
    Input,
    Label,
    Badge,
    Icon,
    Footer,
    Picker,
    Item as FormItem,
    Left,
    Body,
    CardItem,
    Card,
    Right,
    ListItem,
    List,
    CheckBox,
} from 'native-base';
import {
    Kaede,
    Hoshi,
    Jiro,
    Isao,
    Madoka,
    Akira,
    Hideo,
    Kohana,
    Makiko,
    Sae,
    Fumi,
} from 'react-native-textinput-effects';
import CustomizeHeader from '../frags/CustomizeHeader';

import { myspiner, temp, ShowNotification, AppInlineLoader } from '../../dependency/UtilityFunctions';
import Logo from '../../assets/workbrookAssets/logo.png';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import RBSheet from "react-native-raw-bottom-sheet";
import Interests from '../interests/Interests';
import Noti from "../../assets/workbrookAssets/noti.png";
import UserBl from "../../assets/workbrookAssets/userBl.png";
import ArrForward from "../../assets/workbrookAssets/arrfr.png";
import Shield from "../../assets/workbrookAssets/shield.png";
import Quest from "../../assets/workbrookAssets/quest.png";
import Info from "../../assets/workbrookAssets/info.png";
import Plus from "../../assets/workbrookAssets/plus.png";
import Remark from "../../assets/workbrookAssets/remark.png";
import Like from "../../assets/workbrookAssets/like.png";
import MoreIcon from "../../assets/workbrookAssets/more2.png";
import Profile from "../../assets/images/profile/dp2.png";
import cardPix from "../../assets/images/background/bg3_old.jpg";
import RNPickerSelect from 'react-native-picker-select';
import TagInput from 'react-native-tags-input';
import imageUser from '../../assets/workbrookAssets/user2.png';
import { SvgUri } from 'react-native-svg';

import RNFetchBlob from 'react-native-fetch-blob'
import ImagePicker from 'react-native-image-picker';
import CloseModal from "../../assets/workbrookAssets/whitecloss.png";
import SuccessIcon from "../../assets/workbrookAssets/successPost.png";
import Modal from 'react-native-modal';
import AccountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import { text } from 'react-native-communications';
import DashboardStore from '../../stores/Dashboard';
import { hasSetIntrest } from '../../auth';
import Autocomplete from 'react-native-autocomplete-input';
import MultiSelect from 'react-native-multiple-select';
import { YOUR_CLOUDINARY_PRESET, YOUR_CLOUDINARY_NAME, requestCameraPermission, YOUR_CLOUDINARY_PRESET_JobBanner } from '../../dependency/UtilityFunctions';
import EmptyListComponent from '../../StatelessScreens/EmptyListComponent';


items = [{
    id: '92iijs7yta',
    name: 'Ondo',
}, {
    id: 'a0s0a8ssbsd',
    name: 'Ogun',
}, {
    id: '16hbajsabsd',
    name: 'Calabar',
}, {
    id: 'sdhyaysdj',
    name: 'Kaduna',
}, {
    id: 'suudydjsjd',
    name: 'Abuja',
}];

@observer
class ReferralsProfile extends Component {
    constructor(props) {
        super(props);
        this.mapRef = null;
        this.state = {
            email: '',
            password: '',

        }
        selectedItems = [];
        this.CheckRecruiterFullDetails = this.CheckRecruiterFullDetails.bind(this)
    }
    onSelectedItemsChange = selectedItems => {
        this.setState({ selectedItems });
    };
    CheckRecruiterFullDetails = (item, page) => {

        DashboardStore._updateParameterV2(item, "SingleRecruiter", "Recruiters");
        this.props.navigation.navigate(page);

    };

    async componentDidMount() {
        let UserloginToken = await temp();
        // let loggedinUserdetails=await loggedinUserdetails();
        console.log(UserloginToken);
        DashboardStore._getAxiosInstance(UserloginToken);
        console.log("UserloginToken");
        DashboardStore.__GetRecruiters(1, 10);

    }
    render() {
        const { selectedItems } = this.state;
       
       
        return (

            <Container style={styles.container}>
                {/* <CustomizeHeader
                    leftside='WhiteArr'
                    title='Referrals Profile'
                    rightside='empty'
                    statedColor='#1E93E2'
                    icon='md-close'
                // savehandler={this.proceednow}
                /> */}
                <View style={[styles.profileWrapinner, { backgroundColor: "#1E93E2" }]}>
                <Button transparent onPress={() => { this.props.navigation.goBack() }} style={{position: 'absolute', top : 33, left: 22}}>
                        <SvgUri
                          // style={{ marginTop: 20, marginLeft: -17 }}
                          width='11px'
                          height='22px'
                          uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1589394608/Mobile_Assets/arrbk_ipiscg.svg"
                        />
                        {/* <Image
                          source={WhiteArr}
                          style={{ height: 22, width: 11, }}
                        /> */}
                      </Button>
                    <Image source={imageUser} style={{ height: 48, width: 48, borderRadius: 48 / 2, marginBottom: 12 }} />
                    <Text style={styles.Text2}>Jane Dokaszhuk</Text>
                </View>

                <ScrollView
                    contentContainerStyle={{
                        padding: 15

                    }}
                >

                    <View style={{ marginBottom: 30, marginTop:10 }}>
                        <Text style={styles.cardText1}>
                            Activity Log
                        </Text>
                    </View>

                    <List style={{}}>
                        <ListItem avatar style={{ display: 'flex', justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center', borderBottomWidth: 1, borderBottomColor: '#E5E5E5', marginLeft: 0}}>
                            
                        <Body style={[styles.flexdisplay, { borderBottomWidth: 0, marginLeft: 0,}]}>
                                <Text style={styles.cardTextJob8}>Jane has just joined workbrook</Text>
                            </Body>
                            <Right style={[styles.btnStyIdc, { borderBottomWidth: 0 , paddingRight: 0,}]}>
                                    <Text style={[styles.cardTextJob8, { fontSize: 11 }]}>2s</Text>
                            </Right>
                        </ListItem>
                        <ListItem avatar style={{ display: 'flex', justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center', borderBottomWidth: 1, borderBottomColor: '#E5E5E5', marginLeft: 0}}>
                            
                        <Body style={[styles.flexdisplay, { borderBottomWidth: 0, marginLeft: 0,}]}>
                                <Text style={styles.cardTextJob8}>Completed their profile</Text>
                            </Body>
                            <Right style={[styles.btnStyIdc, { borderBottomWidth: 0 , paddingRight: 0,}]}>
                                    <Text style={[styles.cardTextJob8, { fontSize: 11 }]}>Now</Text>
                            </Right>
                        </ListItem>
                        <ListItem avatar style={{ display: 'flex', justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center', borderBottomWidth: 1, borderBottomColor: '#E5E5E5', marginLeft: 0}}>
                            
                        <Body style={[styles.flexdisplay, { borderBottomWidth: 0, marginLeft: 0,}]}>
                                <Text style={styles.cardTextJob8}>Applied for a job</Text>
                            </Body>
                            <Right style={[styles.btnStyIdc, { borderBottomWidth: 0 , paddingRight: 0,}]}>
                                    <Text style={[styles.cardTextJob8, { fontSize: 11 }]}>Now</Text>
                            </Right>
                        </ListItem>
                        <ListItem avatar style={{ display: 'flex', justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center', borderBottomWidth: 1, borderBottomColor: '#E5E5E5', marginLeft: 0}}>
                            
                        <Body style={[styles.flexdisplay, { borderBottomWidth: 0, marginLeft: 0,}]}>
                                <Text style={styles.cardTextJob8}>Referred/Invited a friend</Text>
                            </Body>
                            <Right style={[styles.btnStyIdc, { borderBottomWidth: 0 , paddingRight: 0,}]}>
                                    <Text style={[styles.cardTextJob8, { fontSize: 11 }]}>1h</Text>
                            </Right>
                        </ListItem>
                        
                        <ListItem avatar style={{ display: 'flex', justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center', borderBottomWidth: 1, borderBottomColor: '#E5E5E5', marginLeft: 0}}>
                            
                        <Body style={[styles.flexdisplay, { borderBottomWidth: 0, marginLeft: 0,}]}>
                                <Text style={styles.cardTextJob8}>Applied for a job</Text>
                            </Body>
                            <Right style={[styles.btnStyIdc, { borderBottomWidth: 0 , paddingRight: 0,}]}>
                                    <Text style={[styles.cardTextJob8, { fontSize: 11 }]}>1h</Text>
                            </Right>
                        </ListItem>
                        
                        <ListItem avatar style={{ display: 'flex', justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center', borderBottomWidth: 1, borderBottomColor: '#E5E5E5', marginLeft: 0}}>
                            
                            <Body style={[styles.flexdisplay, { borderBottomWidth: 0, marginLeft: 0,}]}>
                                <Text style={styles.cardTextJob8}>Jane is now a recruiter</Text>
                                <TouchableOpacity>
                                <Text style={[styles.cardTextJob8, {color:"#1E93E2", fontWeight: "bold", marginLeft: 2} ]}>Join Her</Text>
                                </TouchableOpacity>
                            </Body>
                            <Right style={[styles.btnStyIdc, { borderBottomWidth: 0 , paddingRight: 0,}]}>
                                    <Text style={[styles.cardTextJob8, { fontSize: 11 }]}>1h</Text>
                            </Right>
                        </ListItem>
                        
                    </List>

                </ScrollView>



            </Container>


        );
    }
}
export default ReferralsProfile;

const styles = StyleSheet.create({
    container: {
        backgroundColor: "#EDEDED",
        display: "flex",
        flex: 1,
        justifyContent: "flex-start",
    },
    profileWrapinner: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 20,
        paddingBottom: 35
    },
    rmPadding: {
        paddingTop: 0,
        paddingRight: 0,
        paddingBottom: 0,
        paddingLeft: 0,
    },
    rmPaddingLR: {
        paddingLeft: 0,
        paddingRight: 0,
    },

    cardTextJob1: {
        color: "#2F2F2F",
        fontSize: 36,
        fontWeight: '600',

    },
    cardTextJob2: {
        color: "#BCBCBC",
        fontSize: 14,
        fontWeight: '600',
        marginBottom: 5,
    },
    cardTextJob2_2: {
        color: "#fff",
        fontSize: 14,
        fontWeight: '600',
    },
    cardTextJob2_3: {
        color: "#BCBCBC",
        fontSize: 14,
        fontWeight: '500',
    },
    cardTextJob3: {
        color: "#2F2F2F",
        fontSize: 13,
        fontWeight: '500',
    },
    cardTextJob4: {
        fontSize: 12,
        color: '#2F2F2F',
        fontWeight: '600'
    },

    cardTextJob5: {
        color: "#2F2F2F",
        fontSize: 10,
        fontWeight: '500',
    },
    cardTextJob7: {
        color: "#1E93E2",
        fontSize: 11,
        fontWeight: '500',
    },
    cardTextJob4_1: {
        color: "#fff",
        fontSize: 12,
        fontWeight: '500',
    },

    cardTextJob6: {
        color: "#1E93E2",
        fontSize: 11,
        fontWeight: '500',
    },
    cardTextJob8: {
        color: "#2F2F2F",
        fontSize: 12,
        fontWeight: 'normal',
    },

    Text1: {
        fontSize: 13,
        fontWeight: '600',
        color: "#FAFAFA",
    },
    Text2: {
        fontSize: 16,
        fontWeight: '600',
        color: "#FAFAFA",
    },
    cardText1: {
        color: "#2F2F2F",
        fontWeight: '600',
        fontSize: 24,
    },

    inputStyleWrap: {
        height: 38,
        backgroundColor: '#E5E5E5',
        borderRadius: 5,
    },
    headerWrap: {
        marginBottom: 13,
        marginTop: 20,
        paddingLeft: 25,
        paddingRight: 25,
    },
    serachbtnSty: {
        height: 54,
        borderRadius: 4,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#5ABC7A',
        marginTop: 24
    },
    ViewBtnSty: {
        height: 23,
        width: 50,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#1E93E2',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    btnStyIdc: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    flexdisplay: {
        display: 'flex',
        flexDirection:"row",
        alignItems: 'center',
    },

});

import React, { Component, Fragment } from 'react';
import { Text, StyleSheet, ImageBackground, Keyboard, TouchableOpacity, Alert, FlatList, ActivityIndicator } from 'react-native';
import { Platform,KeyboardAvoidingView, Image, ScrollView } from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons';
import Checker from '../../assets/workbrookAssets/checker.png';
import {
    Container,
    Header,
    Content,
    Textarea,
    Button,
    View,
    Thumbnail,
    Form,
    Item,
    Input,
    Label,
    Badge,
    Icon,
    Footer,
    Picker,
    Item as FormItem,
    Left,
    Body,
    CardItem,
    Card,
    Right,
    ListItem,
    List,
    CheckBox,
} from 'native-base';
import {
    Kaede,
    Hoshi,
    Jiro,
    Isao,
    Madoka,
    Akira,
    Hideo,
    Kohana,
    Makiko,
    Sae,
    Fumi,
} from 'react-native-textinput-effects';
import CustomizeHeader from '../frags/CustomizeHeader';

import { myspiner, temp, ShowNotification, AppInlineLoader, GenerateInviteLink, ShareOptions, generateMyUniqueInvitationId, getLodgedInDetailsProfile } from '../../dependency/UtilityFunctions';
import Logo from '../../assets/workbrookAssets/logo.png';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import RBSheet from "react-native-raw-bottom-sheet";
import Interests from '../interests/Interests';
import Noti from "../../assets/workbrookAssets/noti.png";
import UserBl from "../../assets/workbrookAssets/userBl.png";
import ArrForward from "../../assets/workbrookAssets/arrfr.png";
import Shield from "../../assets/workbrookAssets/shield.png";
import Quest from "../../assets/workbrookAssets/quest.png";
import Info from "../../assets/workbrookAssets/info.png";
import Plus from "../../assets/workbrookAssets/plus.png";
import Remark from "../../assets/workbrookAssets/remark.png";
import Like from "../../assets/workbrookAssets/like.png";
import MoreIcon from "../../assets/workbrookAssets/more2.png";
import Profile from "../../assets/images/profile/dp2.png";
import cardPix from "../../assets/images/background/bg3_old.jpg";
import RNPickerSelect from 'react-native-picker-select';
import TagInput from 'react-native-tags-input';

import RNFetchBlob from 'react-native-fetch-blob'
import ImagePicker from 'react-native-image-picker';
import CloseModal from "../../assets/workbrookAssets/whitecloss.png";
import SuccessIcon from "../../assets/workbrookAssets/successPost.png";
import Modal from 'react-native-modal';
import AccountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import { text } from 'react-native-communications';
import DashboardStore from '../../stores/Dashboard';
import { hasSetIntrest } from '../../auth';
import Autocomplete from 'react-native-autocomplete-input';
import MultiSelect from 'react-native-multiple-select';
import { YOUR_CLOUDINARY_PRESET, YOUR_CLOUDINARY_NAME, requestCameraPermission, YOUR_CLOUDINARY_PRESET_JobBanner } from '../../dependency/UtilityFunctions';
import EmptyListComponent from '../../StatelessScreens/EmptyListComponent';
import { Clipboard } from 'react-native';
import Share from 'react-native-share';


items = [{
    id: '92iijs7yta',
    name: 'Ondo',
}, {
    id: 'a0s0a8ssbsd',
    name: 'Ogun',
}, {
    id: '16hbajsabsd',
    name: 'Calabar',
}, {
    id: 'sdhyaysdj',
    name: 'Kaduna',
}, {
    id: 'suudydjsjd',
    name: 'Abuja',
}];


@observer
class UniqueInviteLink extends Component {
    constructor(props) {
        super(props);
        this.mapRef = null;
        this.state = {
            InviteLink: '',
            isCopied:false,
            profile:{}
 
        }
        this.writeToClipboard=this.writeToClipboard.bind(this)
        this.ShareInvitationLink=this.ShareInvitationLink.bind(this)
    }

    async componentDidMount() {
        let InnerProfile = await getLodgedInDetailsProfile();
        let isAvailable = InnerProfile.isAvailable;
        if (isAvailable === true) {
        
            let this_userprofile = InnerProfile.storeData;
            console.log(this_userprofile)
            console.log("InnerProfile")
            this.setState({
                profile: this_userprofile
            })
let InvitationCode=profile.invitation_code;
      let myInviteLInk=  await generateMyUniqueInvitationId(InvitationCode);
      this.setState({
        InviteLink:myInviteLInk
      })
    }
}
    writeToClipboard = async () => {
        await Clipboard.setString(this.state.text);
        this.setState({
            isCopied:true 
        })
        // alert('Copied to Clipboard!');
      };
    ShareInvitationLink = async () => {
        // alert("hey")
        // const url = GenerateInviteLink();
        let profile=this.state.profile;
        const url = this.state.InviteLink;
        let fullname = profile.first_name == null || profile.last_name == null ? profile.username : `${profile.first_name}  ${profile.last_name} `

        const title = 'Join Workbrook';
        const message = `${fullname} will like you to join workbrook. Please follow the link to download workbrook… (on iOS and Android)`;
let options=ShareOptions(url,title,message);

        Share.open(options)
        .then((res) => {
            console.log(res)
            console.log("res")

        })
        .catch((err) => {
            err && console.log(err);
            console.log("Error from share")
        });
      };
    render() {
        const {InviteLink,isCopied}=this.state;
        return (

            <Container style={styles.container}>
                <CustomizeHeader
                    leftside='WhiteArr'
                    title='Unique Invite Link'
                    rightside='empty'
                    statedColor='#1E93E2'
                    icon='md-close'
                // savehandler={this.proceednow}
                />

                <ScrollView
                    contentContainerStyle={{
                        padding: 16,
                    }}
                >
                    {isCopied===true ?

<Card style={{ marginBottom: 16, backgroundColor: '#FFF', elevation: 1, borderRadius: 5, borderColor: 'transparent' }}>
<View style={styles.cardBodySty}>
    <View style={{ borderRadius: 5, borderWidth: 1, borderColor: "#ECEBEB", padding: 10, }} >
        <Text style={{ fontSize: 11, fontWeight: '300', color: '#676767' }}>
        {InviteLink}
            </Text>
    </View>
</View>

<View style={styles.cardFooterSty}>
    <Button 
    style={[styles.applicantStateActive,{backgroundColor: 'transparent', borderWidth:1, borderColor: "#1E93E2" }]}
    >
        <Text style={[styles.stateactiveText, {color: "#1E93E2", fontWeight: "bold"} ]}>Link Copied</Text>
    </Button>

    <Button style={[styles.applicantStateActive,{backgroundColor: '#5ABC7A'}]}
       onPress={
        this.ShareInvitationLink
    }
    >
        <Text style={styles.stateactiveText}>Share</Text>
    </Button>
    
</View>
</Card>

                    :
                    <Card style={{ marginBottom: 16, backgroundColor: '#FFF', elevation: 1, borderRadius: 5, borderColor: 'transparent' }}>
                    <View style={styles.cardBodySty}>
                        <View style={{ borderRadius: 5, borderWidth: 1, borderColor: "#ECEBEB", padding: 10, }} >
                            <Text style={{ fontSize: 11, fontWeight: '300', color: '#676767' }}>
{InviteLink}
                         </Text>
                        </View>
                    </View>

                    <View style={styles.cardFooterSty}>
                        <Button style={[styles.applicantStateActive]}
                            onPress={this.writeToClipboard}
                        >
                            <Text style={styles.stateactiveText}>Copy Link</Text>
                        </Button>
                        <Button style={[styles.applicantShareStateActive]}
                     
                        >
                            <Text style={[styles.stateactiveText, {color: "#A5A5A5"} ]}>Share</Text>
                        </Button>
                    </View>
                </Card>
              
       

                    }
            
                </ScrollView>



            </Container>


        );
    }
}
export default UniqueInviteLink;

const styles = StyleSheet.create({
    container: {
        backgroundColor: "#FAFAFA",
        display: "flex",
        flex: 1,
        justifyContent: "flex-start",
    },

    rmPadding: {
        paddingTop: 0,
        paddingRight: 0,
        paddingBottom: 0,
        paddingLeft: 0,
    },
    rmPaddingLR: {
        paddingLeft: 0,
        paddingRight: 0,
    },

    cardTextJob1: {
        color: "#2F2F2F",
        fontSize: 36,
        fontWeight: '600',

    },
    cardTextJob2: {
        color: "#BCBCBC",
        fontSize: 14,
        fontWeight: '600',
        marginBottom: 5,
    },
    cardTextJob2_2: {
        color: "#fff",
        fontSize: 14,
        fontWeight: '600',
    },
    cardTextJob3: {
        color: "#2F2F2F",
        fontSize: 13,
        fontWeight: '500',
    },
    cardTextJob4: {
        fontSize: 12,
        color: '#2F2F2F',
        fontWeight: '600'
    },

    cardTextJob5: {
        color: "#2F2F2F",
        fontSize: 10,
        fontWeight: '500',
    },
    cardTextJob5_1: {
        color: "#1E93E2",
        fontSize: 10,
        fontWeight: '600',
    },
    cardTextJob7: {
        color: "#1E93E2",
        fontSize: 11,
        fontWeight: '500',
    },
    cardTextJob4_1: {
        color: "#fff",
        fontSize: 12,
        fontWeight: '500',
    },

    cardTextJob6: {
        color: "#1E93E2",
        fontSize: 11,
        fontWeight: '500',
    },


    cardText1: {
        color: "#2F2F2F",
        fontWeight: '500',
        fontSize: 12,
    },
    Text1: {
        color: "#FAFAFA",
        fontWeight: '500',
        fontSize: 11,
    },
    Text2: {
        color: "#676767",
        fontWeight: '500',
        fontSize: 11,
    },
    cardBodySty: {
        marginTop: 30,
        marginBottom: 11,
        fontWeight: '500',
        fontSize: 11,
        paddingLeft: 20,
        paddingRight: 20,
    },
    stateactiveText: {
        fontSize: 11,
        fontWeight: 'bold',
        color: '#FAFAFA'
    },
    stateNotactiveText: {
        fontSize: 11,
        fontWeight: 'bold',
        color: '#B4B4B4'
    },
    applicantState: {
        width: "48%",
        height: 28,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#B4B4B4',
        fontSize: 11,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent',
        elevation: 0,
        marginBottom: 8,
    },
    applicantStateActive: {
        width: "100%",
        height: 28,
        borderRadius: 5,
        backgroundColor: '#1E93E2',
        fontSize: 11,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        elevation: 0,
        marginBottom: 8,

    },
    applicantShareStateActive: {
        width: "100%",
        height: 28,
        borderRadius: 5,
        backgroundColor: '#D7D7D7',
        fontSize: 11,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        elevation: 0,
        marginBottom: 8,

    },
    cardFooterSty: {
        display: 'flex',
        // flexDirection: 'row',
        justifyContent: 'space-between',
        flexWrap: 'wrap',
        paddingLeft: 20,
        paddingRight: 20,
        marginBottom: 20,
        textAlign: 'center',

    },
    inputStyleWrap: {
        height: 38,
        backgroundColor: '#E5E5E5',
        borderRadius: 5,
    },
    headerWrap: {
        marginBottom: 13,
        marginTop: 20,
        paddingLeft: 25,
        paddingRight: 25,
    },
    serachbtnSty: {
        height: 54,
        borderRadius: 4,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#5ABC7A',
        marginTop: 24
    },
    ViewBtnSty: {
        height: 23,
        width: 50,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#FF5964',
        backgroundColor: "#FF5964",
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    btnStyIdc: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    applicantImg: {
        height: 31,
        width: 32,
        borderRadius: 32 / 2,
    },
    numberCount: {
        fontWeight: '600',
        fontSize: 12,
        color: '#1E93E2',
        marginRight: 11
    },
    leftAlignSty: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
    },
    Text8: {
        fontSize: 12,
        fontWeight: '600',
        color: "#1E93E2",
    },


});

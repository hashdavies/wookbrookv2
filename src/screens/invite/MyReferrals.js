import React, { Component, Fragment } from 'react';
import { Text, StyleSheet, ImageBackground, Keyboard, TouchableOpacity, Alert, FlatList, ActivityIndicator } from 'react-native';
import { KeyboardAvoidingView, Image, ScrollView } from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons';
import Checker from '../../assets/workbrookAssets/checker.png';
import {
    Container,
    Header,
    Content,
    Textarea,
    Button,
    View,
    Thumbnail,
    Form,
    Item,
    Input,
    Label,
    Badge,
    Icon,
    Footer,
    Picker,
    Item as FormItem,
    Left,
    Body,
    CardItem,
    Card,
    Right,
    ListItem,
    List,
    CheckBox,
} from 'native-base';
import {
    Kaede,
    Hoshi,
    Jiro,
    Isao,
    Madoka,
    Akira,
    Hideo,
    Kohana,
    Makiko,
    Sae,
    Fumi,
} from 'react-native-textinput-effects';
import CustomizeHeader from '../frags/CustomizeHeader';

import { myspiner, temp, ShowNotification, AppInlineLoader } from '../../dependency/UtilityFunctions';
import Logo from '../../assets/workbrookAssets/logo.png';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import RBSheet from "react-native-raw-bottom-sheet";
import Interests from '../interests/Interests';
import Noti from "../../assets/workbrookAssets/noti.png";
import UserBl from "../../assets/workbrookAssets/userBl.png";
import ArrForward from "../../assets/workbrookAssets/arrfr.png";
import Shield from "../../assets/workbrookAssets/shield.png";
import Quest from "../../assets/workbrookAssets/quest.png";
import Info from "../../assets/workbrookAssets/info.png";
import Plus from "../../assets/workbrookAssets/plus.png";
import Remark from "../../assets/workbrookAssets/remark.png";
import Like from "../../assets/workbrookAssets/like.png";
import MoreIcon from "../../assets/workbrookAssets/more2.png";
import Profile from "../../assets/images/profile/dp2.png";
import cardPix from "../../assets/images/background/bg3_old.jpg";
import RNPickerSelect from 'react-native-picker-select';
import TagInput from 'react-native-tags-input';

import RNFetchBlob from 'react-native-fetch-blob'
import ImagePicker from 'react-native-image-picker';
import CloseModal from "../../assets/workbrookAssets/whitecloss.png";
import SuccessIcon from "../../assets/workbrookAssets/successPost.png";
import Modal from 'react-native-modal';
import AccountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import { text } from 'react-native-communications';
import DashboardStore from '../../stores/Dashboard';
import { hasSetIntrest } from '../../auth';
import Autocomplete from 'react-native-autocomplete-input';
import MultiSelect from 'react-native-multiple-select';
import DelayInput from "react-native-debounce-input";

import { YOUR_CLOUDINARY_PRESET, YOUR_CLOUDINARY_NAME, requestCameraPermission, YOUR_CLOUDINARY_PRESET_JobBanner } from '../../dependency/UtilityFunctions';
import EmptyListComponent from '../../StatelessScreens/EmptyListComponent';


items = [{
    id: '92iijs7yta',
    name: 'Ondo',
}, {
    id: 'a0s0a8ssbsd',
    name: 'Ogun',
}, {
    id: '16hbajsabsd',
    name: 'Calabar',
}, {
    id: 'sdhyaysdj',
    name: 'Kaduna',
}, {
    id: 'suudydjsjd',
    name: 'Abuja',
}];

@observer
class MyReferrals extends Component {
    constructor(props) {
        super(props);
        this.mapRef = null;
        this.state = {
            email: '',
            password: '',

        }
        selectedItems = [];
        this.CheckRecruiterFullDetails = this.CheckRecruiterFullDetails.bind(this)
        this.ReloadPageData=this.ReloadPageData.bind(this);
        this.SearchInviteList=this.SearchInviteList.bind(this)


    }
    onSelectedItemsChange = selectedItems => {
        this.setState({ selectedItems });
    };
    CheckRecruiterFullDetails = (item, page) => {

        DashboardStore._updateParameterV2(item, "SingleRecruiter", "Recruiters");
        this.props.navigation.navigate(page);

    };
    SearchInviteList = (query) => {
        // alert("hey");
        let stringlenght=query.length;
        if(stringlenght<4){
    return;
        }
     DashboardStore.__SearchMyInviteList(query)
    }
    ReloadPageData = () => {
        DashboardStore.__GetMyInviteList(1, 10);
      };

    async componentDidMount() {
        let UserloginToken = await temp();
        // let loggedinUserdetails=await loggedinUserdetails();
        console.log(UserloginToken);
        DashboardStore._getAxiosInstance(UserloginToken);
        console.log("UserloginToken");
        DashboardStore.__GetMyInviteList(1, 10);

    }
    render() {
        const { selectedItems } = this.state;
        const {InviteList}=DashboardStore;
        let ErrorComponentDisplay = null;

        if (DashboardStore.isProcessing === true) {
            ErrorComponentDisplay = AppInlineLoader(DashboardStore.isProcessing)

        }
        else {
            ErrorComponentDisplay = <EmptyListComponent
                message={InviteList.ErrorMessage}
                onclickhandler={this.ReloadPageData}
                />
        }

        let MyInvitesList =
            <FlatList
                // Data
                data={InviteList.Myinvites}
                // Render Items
                renderItem={({ item }) => {
                    let Part_name = item.first_name == null || item.last_name == null ? "item.username" : `${item.first_name}  ${item.last_name} `

                    return (

                        // <List style={{ borderBottomWidth: 1, borderBottomColor: '#E5E5E5', paddingBottom: 15 }}>
                        //     <ListItem avatar style={{ display: 'flex', justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center', }}>
                        //         <Left>
                        //             <Thumbnail source={Profile} />
                        //         </Left>
                        //         <Body style={{ borderBottomWidth: 0, }}>
                        //             <Text style={styles.cardTextJob4}>{item.fullname}</Text>
                        //             <Text style={styles.cardTextJob5} numberOfLines={3} >{item.summary}</Text>
                        //         </Body>
                        //         <Right style={[styles.btnStyIdc, { borderBottomWidth: 0 }]}>
                        //             <Button transparent style={styles.ViewBtnSty}
                        //                  onPress={() => { this.CheckRecruiterFullDetails(item, "RecruiterProfile") }}
                        //             >
                        //                 <Text style={[styles.cardTextJob5, { color: '#1E93E2' }]}>VIEW</Text>
                        //             </Button>
                        //         </Right>
                        //     </ListItem>
                        // </List>

                        <List style={{ borderBottomWidth: 1, borderBottomColor: '#E5E5E5', paddingBottom: 15 }}>
                        <ListItem avatar style={{ display: 'flex', justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center', }}>
                            <Left>
                                <Thumbnail source={{uri:item.image_url}} style={{width: 41, height:42, borderRadius: 42/2}} />
                            </Left>
                            <Body style={{ borderBottomWidth: 0, }}>
                                <Text style={styles.cardTextJob4}>{Part_name}</Text>
                                <Text style={styles.cardTextJob5} numberOfLines={1} >Doing what you like will always keep you happy . . .</Text>
                            </Body>
                            <Right style={[styles.btnStyIdc, { borderBottomWidth: 0 }]}>
                                <Button transparent style={styles.ViewBtnSty}
                                onPress={() => { this.props.navigation.navigate("ReferralsProfile") }}
                                >
                                    <Text style={[styles.cardTextJob5, { color: '#1E93E2' }]}>VIEW</Text>
                                </Button>
                            </Right>
                        </ListItem>
                    </List>

 

                    )
                }}
                // Item Key
                keyExtractor={(item, index) => String(index)}
            // Header (Title)
            // ListHeaderComponent={this.renderHeader}
            // Footer (Activity Indicator)
            // ListFooterComponent={this.renderFooter()}
            // On End Reached (Takes a function)
            // onEndReached={this.retrieveMore}
            // How Close To The End Of List Until Next Data Request Is Made
            // onEndReachedThreshold={0}
            // Refreshing (Set To True When End Reached)
            // refreshing={this.state.refreshing}
            />
        return (

            <Container style={styles.container}>
                <CustomizeHeader
                    leftside='WhiteArr'
                    title='Invitees'
                    rightside='empty'
                    statedColor='#1E93E2'
                    icon='md-close'
                // savehandler={this.proceednow}
                />

                <ScrollView
                    contentContainerStyle={{
                        marginTop: 15

                    }}
                >
                    
                    <View style={{ paddingLeft: 15, paddingRight: 15, marginBottom: 10 }}>
                        {/* <Label style={styles.cardTextJob2}>Experience</Label> */}
                        <Item regular style={styles.inputStyleWrap}>
                            <AntDesign name="search1" style={[styles.cardTextJob2_3, {marginLeft:13, marginRight: 6, fontSize: 20} ]} />
                            {/* <Input
                                placeholder="Search"
                                placeholderTextColor="#BCBCBC"
                                style={[styles.cardTextJob2_3, {color: "#2F2F2F"}]}
                            /> */}
                                 <DelayInput
        // value={value}
        minLength={3}
        // inputRef={inputRef}
        // onChangeText={setValue}
        onChangeText={(query) => this.SearchInviteList(query)}
         delayTimeout={500}
        placeholder="Search"
        placeholderTextColor="#BCBCBC"
        style={[styles.cardTextJob2_3, {color: "#2F2F2F",flex:1,width:300,}]}
       // style={{ color: "#fff" ,flex:1,width:300}} 
       />
                        </Item>
                    </View>
                    {
                           InviteList.Myinvites.length > 0 ?

                           MyInvitesList
                                :
                                ErrorComponentDisplay
                        }
           

                    {/* <List style={{ borderBottomWidth: 1, borderBottomColor: '#E5E5E5', paddingBottom: 15 }}>
                        <ListItem avatar style={{ display: 'flex', justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center', }}>
                            <Left>
                                <Thumbnail source={Profile} style={{width: 41, height:42, borderRadius: 42/2}} />
                            </Left>
                            <Body style={{ borderBottomWidth: 0, }}>
                                <Text style={styles.cardTextJob4}>Kumar Pratik</Text>
                                <Text style={styles.cardTextJob5}>Doing what you like will always keep you happy . . .</Text>
                            </Body>
                            <Right style={[styles.btnStyIdc, { borderBottomWidth: 0 }]}>
                                <Button transparent style={styles.ViewBtnSty}
                                onPress={() => { this.props.navigation.navigate("ReferralsProfile") }}
                                >
                                    <Text style={[styles.cardTextJob5, { color: '#1E93E2' }]}>VIEW</Text>
                                </Button>
                            </Right>
                        </ListItem>
                    </List> */}
                 
                </ScrollView>



            </Container>


        );
    }
}
export default MyReferrals;

const styles = StyleSheet.create({
    container: {
        backgroundColor: "#FAFAFA",
        display: "flex",
        flex: 1,
        justifyContent: "flex-start",
    },

    rmPadding: {
        paddingTop: 0,
        paddingRight: 0,
        paddingBottom: 0,
        paddingLeft: 0,
    },
    rmPaddingLR: {
        paddingLeft: 0,
        paddingRight: 0,
    },

    cardTextJob1: {
        color: "#2F2F2F",
        fontSize: 36,
        fontWeight: '600',

    },
    cardTextJob2: {
        color: "#BCBCBC",
        fontSize: 14,
        fontWeight: '600',
        marginBottom: 5,
    },
    cardTextJob2_2: {
        color: "#fff",
        fontSize: 14,
        fontWeight: '600',
    },
    cardTextJob2_3: {
        color: "#BCBCBC",
        fontSize: 14,
        fontWeight: '500',
    },
    cardTextJob3: {
        color: "#2F2F2F",
        fontSize: 13,
        fontWeight: '500',
    },
    cardTextJob4: {
        fontSize: 12,
        color: '#2F2F2F',
        fontWeight: '600'
    },

    cardTextJob5: {
        color: "#2F2F2F",
        fontSize: 10,
        fontWeight: '500',
    },
    cardTextJob7: {
        color: "#1E93E2",
        fontSize: 11,
        fontWeight: '500',
    },
    cardTextJob4_1: {
        color: "#fff",
        fontSize: 12,
        fontWeight: '500',
    },

    cardTextJob6: {
        color: "#1E93E2",
        fontSize: 11,
        fontWeight: '500',
    },


    cardText1: {
        color: "#2F2F2F",
        fontWeight: '500',
        fontSize: 12,
    },
    inputStyleWrap: {
        height: 38,
        backgroundColor: '#E5E5E5',
        borderRadius: 5,
    },
    headerWrap: {
        marginBottom: 13,
        marginTop: 20,
        paddingLeft: 25,
        paddingRight: 25,
    },
    serachbtnSty: {
        height: 54,
        borderRadius: 4,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#5ABC7A',
        marginTop: 24
    },
    ViewBtnSty: {
        height: 23,
        width: 50,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#1E93E2',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    btnStyIdc: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },

});

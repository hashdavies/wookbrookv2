const Dimensions = require('Dimensions');
import React, { Component } from 'react';
import { View, ScrollView, StyleSheet, ImageBackground, Image, Switch } from 'react-native';
const window = Dimensions.get('window');
import { Container, Header, Content, Button, Text, Thumbnail } from 'native-base';
import MapView, { Marker, ProviderPropType, AnimatedRegion, animateToRegion, PROVIDER_GOOGLE } from 'react-native-maps';
import { Col, Row, Grid } from "react-native-easy-grid";
import openMap, { createOpenLink } from 'react-native-open-maps';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import * as Animatable from 'react-native-animatable';
import RBSheet from "react-native-raw-bottom-sheet";
import { CustomAsset } from '../../../utils/assets';
// import { Switch } from 'react-native-switch';
import style from 'react-native-datepicker/style';
const IMAGE_HEIGHT = window.width / 2;


var mapStyle = [
  {
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#212121"
      }
    ]
  },
  {
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#212121"
      }
    ]
  },
  {
    "featureType": "administrative",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "administrative.country",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "administrative.locality",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#bdbdbd"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#181818"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#1b1b1b"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "color": "#2c2c2c"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#8a8a8a"
      }
    ]
  },
  {
    "featureType": "road.arterial",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#373737"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#3c3c3c"
      }
    ]
  },
  {
    "featureType": "road.highway.controlled_access",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#4e4e4e"
      }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "featureType": "transit",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#000000"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#3d3d3d"
      }
    ]
  }
]

class LanguageSelect2 extends Component {
  constructor(props) {
    super(props)
    this.mapRef = null;
    this.state = {
      switchValue: false
    };
    this.toggleSwitch = this.toggleSwitch.bind(this)
  }

  toggleSwitch = () => {
    this.setState({ switchValue: !this.state.switchValue })
  }

  render() {
    const uri = "https://res.cloudinary.com/merlotek/image/upload/v1582644432/Bankto/profile-pic-l-10_arw25k.jpg";

    return (
      <Container>

        <View style={{ backgroundColor: '#fff', flex: 1, position: 'relative' }} >
          <MapView
            style={{ flex: 1 }}

            provider={PROVIDER_GOOGLE}
            showsUserLocation
            initialRegion={{
              latitude: 37.78825,
              longitude: -122.4324,
              latitudeDelta: 0.0922,
              longitudeDelta: 0.0421
            }}
          />
          <RBSheet
            ref={ref => {
              this.RBSheet = ref;
            }}
            height={300}
            duration={250}
            customStyles={{
              container: {
                justifyContent: "center",
                alignItems: "center",
                opacity: 0.9,
                borderTopLeftRadius: 20,
                borderTopRightRadius: 20,
                // height: 222
              }
            }}
          >
            <View style={styles.styleBtmSty}>
              <View style={[styles.styleNameImage, {justifyContent: 'space-between', padding: 15, backgroundColor: "#f8f8f8" }]}>
                <View style={[styles.styleNameImage, {alignItems: 'center'}]}>
                  <Thumbnail square style={{borderRadius: 8}} source={{ uri: uri }} />
                  <View style={{ marginLeft: 10 }}>
                    <Text style={styles.StyText1}>Jeremiah Curtis</Text>
                    <View style={[styles.styleNameImage,{marginTop:6}]}>
                      <Button rounded style={styles.btnsmal}>
                        <Text style={styles.btnsmalTxt}>Cash</Text>
                      </Button>
                      <Button rounded style={styles.btnsmal}>
                        <Text style={styles.btnsmalTxt}>Promo</Text>
                      </Button>
                    </View>
                  </View>
                </View>
                <View style={[styles.contentSideWrap, { alignItems: 'flex-end' }]} >
                  <Text style={[styles.StyText2], {fontSize: 15}}>2.2 km</Text>
                </View>
              </View>
              <View style={[styles.contentSideWrap2, { padding: 15, paddingTop: 0 }]}>
                <View style={[styles.makeBdBtm]}>
                  <Text style={[styles.StyText2, { marginBottom: 10 }]}>Address</Text>
                  <Text style={styles.StyTex6}>7958 Swift Village, Lekki, Lagos</Text>

                </View>
                <View style={[styles.makeBdBtm]}>
                  <Text style={[styles.StyText2, { marginBottom: 10 }]}>Ailment</Text>
                  <Text style={styles.StyTex6}>I have a headache and I can’t breathe well, Help!</Text>

                </View>
                <View style={[styles.unMakedBdBtm]}>
                  <View style={[styles.unMakedIcon]}>
                  <MaterialCommunityIcons style={styles.unMakedIconSty} name="alarm-light-outline"/>

                  </View>
                  <Button transparent>
                    <Text style={[styles.StyText2_1, { fontWeight: 'bold', color:'#bec2ce' }]}> Ignore </Text>
                  </Button>
                  <Button style={[styles.StyTextAceeptWrap]}>
                    <Text style={[styles.StyText2_1, { fontWeight: 'bold', color:'#fff' }]}> ACCEPT </Text>
                  </Button>

                </View>
              </View>

            </View>
          </RBSheet>
          <View style={styles.topscreen}

          >
            <View style={styles.wrappersidebar}>

              <View style={[styles.notifySty, { backgroundColor: '#000', }]}>
                <Text style={[styles.dayWrap, { color: '#fff', }]}>Online</Text>
              </View>
              <View style={styles.menuBarSty}

              >
                <Button transparent
                  onPress={() => {
                    this.RBSheet.open();
                  }}
                >
                  <Image style={styles.menuBarSty2} source={CustomAsset.bar} />
                </Button>
              </View>
            </View>

          </View>

          {/* <Content style={styles.bottomView}>

            </Content> */}


        </View>
      </Container>
    );
  }
}
export default LanguageSelect2;

const styles = StyleSheet.create({

  contentSideWrap: {
    flex: 1,
    justifyContent: 'flex-end',
    marginTop: 40,
  },
  contentSideWrap2: {
    flex: 1,
    // justifyContent: 'flex-end',
  },
  styleBtmSty: {
    width: '100%',
    // padding: 15,
    flex: 1,
    display: 'flex',
    // justifyContent: 'space-between',
    // paddingTop: 35,
  },
  styleName: {
    display: 'flex',
  },
  styleNameImage: {
    display: 'flex',
    flexDirection: 'row'
  },
  StyText1: {
    fontSize: 16,
    color: '#262628'
  },
  StyText2: {
    fontSize: 12,
    color: '#bec2ce'
  },
  StyText2_1: {
    fontSize: 14,
    color: '#bec2ce'
  },
  StyText3: {
    fontSize: 11,
    color: '#959393'
  },
  StyText4: {
    fontSize: 16,
    color: '#fff',
    lineHeight: 25
  },
  StyText5: {
    fontSize: 12,
    color: '#fff',
    lineHeight: 20
  },
  StyTex6: {
    fontSize: 14,
    color: '#242e42',
    lineHeight: 16
  },
  fontsty1: {
    fontSize: 26,
    color: '#fff',
    lineHeight: 25
  },
  bottomView: {
    width: '100%',
    height: 222,
    position: 'absolute',
    backgroundColor: '#fff',
    opacity: 0.9,
    bottom: 0,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  blackShade: {
    width: '100%',
    minHeight: 118,
    backgroundColor: '#0a0b0d',
  },
  colSty: {
    display: 'flex',
    justifyContent: 'space-around',
    alignItems: 'center',
    marginTop: 15,
    marginBottom: 10,
  },
  topscreen: {
    position: 'absolute',
    top: 20,
    width: '100%',
    left: 0,
    right: 0
  },
  wrappersidebar: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: 15
  },
  notifySty: {
    width: 111,
    height: 30,
    borderWidth: 1,
    borderColor: '#0a0b0d',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    backgroundColor: "#fff",
    alignItems: 'center',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 3,
  },
  wrappernoti: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    height: 64,
    backgroundColor: '#0a0b0d',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingLeft: 20,
    paddingRight: 20,
  },
  dayWrapIcon1: {
    backgroundColor: '#0a0b0d',
    width: 38,
    height: 38,
    borderRadius: 19,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: "center",
    alignItems: 'center',
    borderWidth: 1,
    borderStyle: 'dashed',
    borderColor: '#fff',
  },
  dayWrapIcon: {
    backgroundColor: '#fff',
    width: 30,
    height: 30,
    borderRadius: 15,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: "center",
    alignItems: 'center'
  },
  dayWrap: {
    fontSize: 16,
    color: '#0a0b0d'
  },
  menuBarSty: {
    height: 26,
    width: 26,
    backgroundColor: '#fff',
    borderRadius: 13,
    marginRight: 15,
    marginBottom: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    position: 'absolute',
    right: 0,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  menuBarSty2: {
    // height: 15,
    width: 15,
  },
  btnsmal: {
    width: 65,
    height: 16,
    backgroundColor: "#0a0b0d",
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 5
  },
  btnsmalTxt: {
    fontSize: 9,
    color: '#fff',
  },
  makeBdBtm: {
    borderBottomWidth: 1,
    borderBottomColor: '#bec2ce',
    paddingTop: 10,
    paddingBottom: 10,
  },
  unMakedBdBtm: {
    paddingTop: 10,
    paddingBottom: 10,
    flexDirection: 'row',
    justifyContent: "space-between",
    alignItems: 'center'
  },
  unMakedIcon: {
    width: 50,
    height: 50,
    borderWidth: 1,
    borderColor: '#707070',
    borderRadius: 25,
    flexDirection: 'row',
    justifyContent: "center",
    alignItems: 'center',
    backgroundColor: '#fff'
  },
  unMakedIconSty: {
   fontSize: 20,
    color: '#ff5858'
  },

  StyTextAceeptWrap: {
    width: 137,
    height: 39,
    flexDirection: 'row',
    justifyContent: "center",
    alignItems: 'center',
    backgroundColor: '#0a0b0d'
  }


});

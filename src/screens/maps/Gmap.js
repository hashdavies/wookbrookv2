const Dimensions = require('Dimensions');
import React, { Component } from 'react';
import {
  Platform,
   View,
    ScrollView, 
    StyleSheet, 
    ImageBackground,
     Image,
      Switch,
      TouchableOpacity
     } from 'react-native';
const window = Dimensions.get('window');
const { width, height } = Dimensions.get('window');

import { Container, Header, Content, Button, Text, Thumbnail } from 'native-base';
import MapView, { Marker, ProviderPropType, AnimatedRegion, animateToRegion, PROVIDER_GOOGLE } from 'react-native-maps';
import { Col, Row, Grid } from "react-native-easy-grid";
import openMap, { createOpenLink } from 'react-native-open-maps';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import * as Animatable from 'react-native-animatable';
import RBSheet from "react-native-raw-bottom-sheet";
import AccountStore from '../../stores/Account';
// import OtpPage from './OtpPage';
 import { observer } from 'mobx-react/native';
// import PasswordTextBox from '../../component/PasswordTextBox';


import { CustomAsset } from '../../../utils/assets';
// import { Switch } from 'react-native-switch';
import style from 'react-native-datepicker/style';
import { temp, request_device_location_runtime_permission, loggedinUserdetails } from '../../dependency/UtilityFunctions';
import DashboardStore from '../../stores/Dashboard';
const IMAGE_HEIGHT = window.width / 2;


var mapStyle = [
  {
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#212121"
      }
    ]
  },
  {
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#212121"
      }
    ]
  },
  {
    "featureType": "administrative",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "administrative.country",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "administrative.locality",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#bdbdbd"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#181818"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#1b1b1b"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "color": "#2c2c2c"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#8a8a8a"
      }
    ]
  },
  {
    "featureType": "road.arterial",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#373737"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#3c3c3c"
      }
    ]
  },
  {
    "featureType": "road.highway.controlled_access",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#4e4e4e"
      }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "featureType": "transit",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#000000"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#3d3d3d"
      }
    ]
  }
]

const ASPECT_RATIO = width / height;
const General_LATITUDE_DELTA = 0.1922;
// const General_LATITUDE_DELTA = 0.0922;
// const LATITUDE = 51.5074;
// const LONGITUDE = 0.1278;
// const LATITUDE = 51.5123;
// const LONGITUDE = 0.0910;
const LATITUDE = 51.499726;
const LONGITUDE = -0.088447;
 const LATITUDE_DELTA = 0.1922;
//  const LATITUDE_DELTA = 0.0922;
//const LATITUDE_DELTA = 2;
// const LATITUDE_DELTA = 40 //Very high zoom level
// const LATITUDE_DELTA = 10;

const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;


 @observer
class LanguageSelect2 extends Component {
  constructor(props) {
    super(props)
    this.mapRef = null;
    this.state = {
      switchValue: false,
      initialRegion: {
        // london
      //   latitude: parseFloat('51.499726'),
      // longitude: parseFloat('-0.088447'),
        latitude: parseFloat('6.5244'),
      longitude: parseFloat('3.3792'),
        // ibadan
        // latitude: parseFloat('7.3872'),
        // longitude: parseFloat('3.8355'),
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      },
      current_latitude: "",
      current_longitude:"",
    };
    this.toggleSwitch = this.toggleSwitch.bind(this)
    this.goToInitialLocation = this.goToInitialLocation.bind(this);
    // this._AnimateToRegion = this._AnimateToRegion.bind(this);
  }
  async componentDidMount(){
    let UserloginToken=await temp();
    let loggedinUserdetails=await loggedinUserdetails();
    console.log(UserloginToken);
    console.log("hey");
    console.log(loggedinUserdetails);
    DashboardStore._UpdateLoginToken(UserloginToken);
    // DashboardStore.FetchPracticinalProfile(hey);
    if (Platform.OS === 'android') {

      await request_device_location_runtime_permission().then(
        (state) => {
console.log(state)
console.log('state')
      if(state===true){
this.getCurrentLocation();
      }
      else{
        console.log("Permission denyyy")
      }
        },
        (error) => {
          console.log(error)
          console.log("errorkkkkk")
           
        },
          );

    }
  }
  componentWillUnmount() {
 
    navigator.geolocation.clearWatch(this.getLongLat);
 
  }
  getCurrentLocation() {
    this.getLongLat = navigator.geolocation.watchPosition(
      // this.getLongLat = navigator.geolocation.getCurrentPosition(
        (position) => {
          console.log(position)
          console.log('position')
          console.log('position')
          // this.setState({
          //   latitude: position.coords.latitude,
          //   longitude: position.coords.longitude,
          //   error: null,
          // });
          let c_latitude=parseFloat(position.coords.latitude);
         let c_longitude=parseFloat(position.coords.longitude);
          let region = {
            latitude: c_latitude,
            longitude: c_longitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
          };
          this.setState({
            current_latitude: c_latitude,
              current_longitude:c_longitude,
            initialRegion: region
          });
          this.goToInitialLocation
          // this.KeeptrackOfLocation(position||this.state.initialRegion);
          
          //  this.mapRef.animateToRegion(this.state.region, 100);
  
        },
        (error) => {
          console.log(error)
          console.log("error watchingggg")
          this.setState({ error: error.message })
          
        },
        { enableHighAccuracy: true, timeout: 30000, maximumAge: 100, distanceFilter: 10 },
      );

  }

  goToInitialLocation() {
    console.log("Initial rendring method")
    let initialRegion = Object.assign({}, this.state.initialRegion);
    // initialRegion["latitudeDelta"] = 0.005;
    // initialRegion["longitudeDelta"] = 0.005;


    initialRegion["latitudeDelta"] = LATITUDE_DELTA;
    initialRegion["longitudeDelta"] = LONGITUDE_DELTA;
    this.mapRef.animateToRegion(initialRegion, 2000);
  }
 
  toggleSwitch = (e) => {
    console.log(e)
    this.setState({ switchValue: !this.state.switchValue })
    DashboardStore.ChangePractitionalPresence(e);
   //DashboardStore.UserOnlineStatus=e;
  }

  render() {
    const uri = "https://res.cloudinary.com/merlotek/image/upload/v1582644432/Bankto/profile-pic-l-10_arw25k.jpg";
const {LoggedInUserProfile,UserOnlineStatus}=DashboardStore;
    return (
      <Container>

        <View style={{ backgroundColor: '#fff', flex: 1, position: 'relative' }} >
          <MapView
            style={{ flex: 1 }}
            
            provider={PROVIDER_GOOGLE}
            showsUserLocation={true}
            followUserLocation={true}
            showsMyLocationButton={true}
            loadingEnabled={true}
            loadingBackgroundColor='#000000'
            onUserLocationChange={event => {
              console.log(event.nativeEvent.coordinate)
              let longitude=event.nativeEvent.coordinate.longitude
              let latitude=event.nativeEvent.coordinate.latitude
              console.log(latitude)
            //  DashboardStore.UpdatePractitionalLocation(longitude,latitude)
            }
              
            }
            // initialRegion={this.state.region}
            // region={this.state.region}
            onMapReady={this.goToInitialLocation}
            // initialRegion={{
            //   latitude: 37.78825,
            //   longitude: -122.4324,
            //   latitudeDelta: 0.0922,
            //   longitudeDelta: 0.0421
            // }}
            initialRegion={this.state.initialRegion}
            ref={(ref) => { this.mapRef = ref }}

          />
          <RBSheet
            ref={ref => {
              this.RBSheet = ref;
            }}
            height={220}
            duration={250}
            customStyles={{
              container: {
                justifyContent: "center",
                alignItems: "center",
                opacity: 0.9,
                borderTopLeftRadius: 20,
                borderTopRightRadius: 20,
                height: 222
              }
            }}
          >
            <View style={styles.styleBtmSty}>
              <View style={[styles.styleNameImage, { justifyContent: 'space-between' }]}>
                <View style={styles.styleNameImage}>
                  <Thumbnail small source={{ uri: uri }} />
                  <View style={{ marginLeft: 10 }}>
                    <Text style={styles.StyText1}>
{LoggedInUserProfile.firstname +" "+ LoggedInUserProfile.lastname}

                    
                    {/* {JSON.stringify(LoggedInUserProfile)} */}
                    </Text>
          <Text style={styles.StyText2}>{LoggedInUserProfile.displayname}</Text>
                  </View>
                </View>
                <View>
                  <Text style={styles.StyText1}>N5,325.00</Text>
                  <Text style={styles.StyText2}>Earned</Text>
                </View>
              </View>
              <View style={styles.blackShade}>
                <Grid style={{ height: '100%' }}>
                  <Col style={styles.colSty} >
                    <Ionicons style={styles.fontsty1} name="md-time" />
                    <Text style={styles.StyText4}>10.2</Text>
                    <Text style={styles.StyText3}>HOURS ONLINE</Text>
                  </Col>
                  <Col style={styles.colSty}>
                    <AntDesign style={styles.fontsty1} name="dashboard" />
                    <Text style={styles.StyText4}>30 KM</Text>
                    <Text style={styles.StyText3}>TOTAL DISTANCE</Text>
                  </Col>
                  <Col style={styles.colSty}>
                    <MaterialCommunityIcons style={styles.fontsty1} name="dashboard" />
                    <Text style={styles.StyText4}>20</Text>
                    <Text style={styles.StyText3}>TOTAL JOBS</Text>
                  </Col>
                </Grid>
              </View>

            </View>
          </RBSheet>
          <View style={styles.topscreen}
          
          >
            <View style={styles.wrappersidebar}>

              <View style={styles.notifySty}>
              <TouchableOpacity 
              onPress={
                () => {this.props.navigation.navigate('RequstPractitional')}
                }>
              <Text style={styles.dayWrap}>{UserOnlineStatus===true?"Online":"Offline"}</Text>
          
                </TouchableOpacity>
             </View>
              <View style={styles.menuBarSty}
                 
              >
                <Button transparent
                onPress={() => {
                  this.RBSheet.open();
                }}
                >
                <Image style={styles.menuBarSty2} source={CustomAsset.bar} />
                </Button>
              </View>
            </View>
            {UserOnlineStatus===true?
           null
            
            :
            
            <View style={styles.wrappernoti}>
            <View style={styles.dayWrapIcon1}>
              <View style={styles.dayWrapIcon}>
                <Ionicons style={styles.dayWrap} name="ios-moon" />
              </View>
            </View>
            <View style={{}}>

              <Text style={[styles.StyText4, { lineHeight: 20 }]}>You are offline !</Text>
              <Text style={styles.StyText5}>Go online to start accepting jobs.</Text>
            </View>
            <View style={{ color: '#fff', borderWidth: 1, borderColor: '#fff', borderRadius: 20, }}>
              <Switch
                style={{ color: '#fff', borderWidth: 1, padding: 0 }}
                onValueChange={(e)=>this.toggleSwitch(e)}
                value={DashboardStore.UserOnlineStatus} />
            </View>
          </View>
       
            
            }
       
          </View>

          {/* <Content style={styles.bottomView}>

            </Content> */}


        </View>
      </Container>
    );
  }
}
export default LanguageSelect2;

const styles = StyleSheet.create({

  contentSideWrap: {
    flex: 1,
    justifyContent: 'flex-end',
    marginTop: 40,
  },
  styleBtmSty: {
    width: '100%',
    padding: 15,
    flex: 1,
    display: 'flex',
    justifyContent: 'space-between',
    paddingTop: 35,
  },
  styleName: {
    display: 'flex',
  },
  styleNameImage: {
    display: 'flex',
    flexDirection: 'row'
  },
  StyText1: {
    fontSize: 16,
    color: '#262628'
  },
  StyText2: {
    fontSize: 12,
    color: '#bec2ce'
  },
  StyText3: {
    fontSize: 11,
    color: '#959393'
  },
  StyText4: {
    fontSize: 16,
    color: '#fff',
    lineHeight: 25
  },
  StyText5: {
    fontSize: 12,
    color: '#fff',
    lineHeight: 20
  },
  fontsty1: {
    fontSize: 26,
    color: '#fff',
    lineHeight: 25
  },
  bottomView: {
    width: '100%',
    height: 222,
    position: 'absolute',
    backgroundColor: '#fff',
    opacity: 0.9,
    bottom: 0,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  blackShade: {
    width: '100%',
    minHeight: 118,
    backgroundColor: '#0a0b0d',
  },
  colSty: {
    display: 'flex',
    justifyContent: 'space-around',
    alignItems: 'center',
    marginTop: 15,
    marginBottom: 10,
  },
  topscreen: {
    position: 'absolute',
    top: 20,
    width: '100%',
    left: 0,
    right: 0
  },
  wrappersidebar: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: 15
  },
  notifySty: {
    width: 111,
    height: 30,
    borderWidth: 1,
    borderColor: '#0a0b0d',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    backgroundColor: "#fff",
    alignItems: 'center',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 3,
  },
  wrappernoti: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    height: 64,
    backgroundColor: '#0a0b0d',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingLeft: 20,
    paddingRight: 20,
  },
  dayWrapIcon1: {
    backgroundColor: '#0a0b0d',
    width: 38,
    height: 38,
    borderRadius: 19,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: "center",
    alignItems: 'center',
    borderWidth: 1,
    borderStyle: 'dashed',
    borderColor: '#fff',
  },
  dayWrapIcon: {
    backgroundColor: '#fff',
    width: 30,
    height: 30,
    borderRadius: 15,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: "center",
    alignItems: 'center'
  },
  dayWrap: {
    fontSize: 16,
    color: '#0a0b0d'
  },
  menuBarSty: {
    height: 26,
    width: 26,
    backgroundColor: '#fff',
    borderRadius: 13,
    marginRight: 15,
    marginBottom: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    position: 'absolute',
    right: 0,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  menuBarSty2:{
    // height: 15,
    width: 20,
  }


});

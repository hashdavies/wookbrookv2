import React from 'react';
import { Image, Text } from 'react-native';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import { MAP_APIKEY } from '../../dependency/Config';

const homePlace = { description: 'Home', geometry: { location: { lat: 48.8152937, lng: 2.4597668 } } };
const workPlace = { description: 'Work', geometry: { location: { lat: 48.8496818, lng: 2.2940881 } } };

const GooglePlacesInput = (props) => {
  return (
    <GooglePlacesAutocomplete
      placeholder='location'
      minLength={2} // minimum length of text to search
      autoFocus={false}
      returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
      // keyboardAppearance={'light'} // Can be left out for default keyboardAppearance https://facebook.github.io/react-native/docs/textinput.html#keyboardappearance
      listViewDisplayed='true'    // true/false/undefined
      fetchDetails={true}
      // renderDescription={row => row.description} // custom description render
      renderDescription={row => row.description || row.formatted_address || row.name} // custom description render

      // onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true
      //   console.log(data, details);
      // }}
      onPress={props.OnpressAction}

      // getDefaultValue={() => ''}
      getDefaultValue={() => {
        return props.MydefaultLocation || ''
        // console.warn("heyman")
      }}
      query={{
        // available options: https://developers.google.com/places/web-service/autocomplete
        key: MAP_APIKEY,
        language: 'en', // language of the results
        // types: '(address)' // default: 'geocode'
      }}

      styles={{
        // textInputContainer: {
        //   width: '100%'
        // },
        // description: {
        //   fontWeight: 'bold'
        // },
        // predefinedPlacesDescription: {
        //   color: '#1faadb'
        // }
        textInputContainer: {
          // width: '100%'
          // marginVertical: 5, 
          borderTopColor: '#fff',
          borderBottomColor: '#fff',
          borderRadius: 4,
          borderStyle: 'solid',
          borderWidth: 0,
          // borderColor:'#357d83',
          borderColor: 'transaparent',
          // borderColor:'#4ab1be',
          // backgroundColor: '#fff',
          backgroundColor: 'transparent',
          alignItems: 'center',
          height: 50,
          // flex: 1,
          // height: 'auto',
          // height: height,
          // overflow: 'hidden'
        },
        description: {
          fontWeight: 'bold'
        },
        predefinedPlacesDescription: {
          color: 'red'
        },

        textInput: {
          // backgroundColor:'#f1c274',
          backgroundColor: 'transparent',
          marginLeft: 0,
          marginRight: 0,
          // height: 50,
          // color: '#fff',
          // fontSize: 16,
          paddingTop: 0,
          color: "#2F2F2F",
          fontSize: 11,
          fontWeight: '300',
          marginBottom: 0,
        },
        listView: {
          backgroundColor: '#000',
          color: '#000',
          position: 'relative',
          backgroundColor: '#fff',
          // marginLeft: 0,
          // marginRight: 0,
          height: 150,
          // color: 'green',
          // fontSize: 16,
          // paddingTop:0,
          zIndex: 99,
        },
      }}

      // currentLocation={true} // Will add a 'Current location' button at the top of the predefined places list
      currentLocationLabel="Current location"
      nearbyPlacesAPI='GoogleReverseGeocoding' // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
      GoogleReverseGeocodingQuery={{
        // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
      }}
      GooglePlacesSearchQuery={{
        // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
        // rankby: 'distance',
        // type: 'cafe'
      }}

      // GooglePlacesDetailsQuery={{
      //   // available options for GooglePlacesDetails API : https://developers.google.com/places/web-service/details
      //   fields: 'formatted_address,geometry',
      // }}

      //filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities
      enablePoweredByContainer={false}
      // predefinedPlaces={[homePlace, workPlace]}
      enablePoweredByContainer={false}
      debounce={0} // debounce the requests in ms. Set to 0 to remove debounce. By default 0ms.
    // renderLeftButton={()  => <Image source={require('path/custom/left-icon')} />}
    // renderRightButton={() => <Text>Custom text after the input</Text>}
    />
  );
}
export default GooglePlacesInput;

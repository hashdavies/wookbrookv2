import React, { Component } from 'react';
import { Platform, StyleSheet, TouchableOpacity } from 'react-native';
import * as Animatable from 'react-native-animatable';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import { withNavigation } from 'react-navigation';

import {
  Container,
  Text,
  Header,
  Content,
  Textarea,
  Button,
  View,
  Thumbnail,
  Form,
  Item,
  Input,
  Label,
  Badge,
  Icon,
  Footer,
  FooterTab,
  Picker,
  Item as FormItem,
} from 'native-base';

import { CustomAsset } from '../../../utils/assets';
import { IconStyles, AppColor } from '../../../utils/AppStyles';
import {NavigationActions,navigation} from 'react-navigation';



// const FooterwithPicture = ({ data }) => {
  class FooterwithPicture extends Component {
    render() {
      return (
    <Footer style={styles.footerWrap}>
     {
      //  global.usertype==='chiefJudge' || global.usertype==='judicialStaff' ? 
       global.usertype==='judicialStaff' ? 
       
       <TouchableOpacity onPress={()=>this.props.navigation.navigate("Newcase")}>
       <View style={{ flexDirection: 'row', flex: 1, alignItems: 'center' }}>
       <View style={{ width: 80 }}>
           <View style={[styles.addnewcase_wrap]}>
             <Entypo name="plus" style={[IconStyles.MidiumIcon, AppColor.WhiteColour]} />
           </View>
         </View>
         <View style={{ width: 150, alignItems: 'flex-start' }}>
           <Text>Open New Case</Text>
         </View>
         </View>
       </TouchableOpacity>
        :
       null 
       }  
   
       
     
      <View style={[{ flexDirection: 'row', flex: 1, }, styles.footertab_left]}>
       {/* <View style={{ flexDirection: 'row', flex: 1, alignItems: 'center' }}> */}
        <Animatable.Image
          animation="bounceInDown"
          duration={500}
          style={styles.userDp}
          source={ global.profile.ProfilePicture===null  ? 

            CustomAsset.displayPicture
           
            :
            
            {uri:  global.profile.ProfilePicture}
          
          } 
        />
      </View>

    </Footer>
  );
      }
};

// export default FooterwithPicture;
export default withNavigation(FooterwithPicture);
const styles = StyleSheet.create({
  footerWrap: {
    borderStyle: 'solid',
    borderTopWidth: 1,
    borderTopColor: '#ccc',
    backgroundColor: '#FFF',
    height: 70,
  },
  footertab: {
    backgroundColor: '#FFF',
  },
  footertab_left: {
    justifyContent: 'flex-end',
    alignItems: 'center',
    marginRight: 16,
  },
  userDp: {
    height: 50,
    borderRadius: 25,
    width: 50,
    //  paddingTop:10
  },

  addnewcase_wrap: {
    // borderStyle:'solid',
    // borderColor:'#ccc',
    // borderWidth:2,
    borderRadius: 30,
    backgroundColor: '#62be6c',
    height: 50,
    width: 50,
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 12,
  },
  // footer_icon_style: {
  //   fontSize:20,
  //   color:'#000',

  //   },
});

import React, { Component, Fragment } from "react";
import {
  View,
  Text,
  StyleSheet, Platform, Image
} from "react-native";
import { StatusBar } from 'react-native';
import { withNavigation } from 'react-navigation';
import { Left, Right, Icon, Title } from 'native-base';
// import {loggedinUserdetails} from '../dependency/UtilityFunctions'; 
import { Container, Header, Content, Button, Thumbnail, Form, Item, Input, Label, Body, CheckBox, Badge } from 'native-base';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import EvilIcons from 'react-native-vector-icons/MaterialIcons';
import MiniUser from '../../assets/workbrookAssets/miniUser.png';
import Dp from '../../assets/workbrookAssets/dp2.png';
import Bar from '../../assets/workbrookAssets/bar.png';
import DarkCross from '../../assets/workbrookAssets/darkCross.png';
import BlackArr from '../../assets/workbrookAssets/backBl.png';
import WhiteArr from '../../assets/workbrookAssets/backWht.png';
import BlueClose from '../../assets/workbrookAssets/blueClose.png';
import DashboardStore from "../../stores/Dashboard";
import { getLodgedInDetailsProfile } from "../../dependency/UtilityFunctions";
import { SvgUri } from 'react-native-svg';
// import axios from 'axios';
// import qs from 'qs';
class CustomizeHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loggedin_userid: '',
      fullname: '',
      Myid: '',
      NotificationCount: 0,
      profile: {}


    }
  }
  async componentDidMount() {

    // this.Getdetails();
    console.log(DashboardStore.LoggedInUserProfile)
    console.log("DashboardStore.LoggedInUserProfile")
    let InnerProfile = await getLodgedInDetailsProfile();
    let isAvailable = InnerProfile.isAvailable;
    if (isAvailable === true) {

      let this_userprofile = InnerProfile.storeData;
      console.log(this_userprofile)
      console.log("InnerProfile")
      this.setState({
        profile: this_userprofile
      })

      DashboardStore._UpdateLoggedInUserProfile(this_userprofile);
    }
  }
  render() {
    const { NotificationCount, profile } = this.state;
    // 
    const { LoggedInUserProfile } = DashboardStore;
    const { icon2render } = this.props
    let title = `Happy Reading...`;
    let statedColor = this.props.statedColor || '#000';
    let colorChanger = this.props.textColor || '#fff';
    return (
      // <Header style={{backgroundColor:'#000',borderBottomColor:'#000',borderBottomWidth:2,fontSize:14,zIndex:10,position:'absolute',}}>
      <Header style={{ backgroundColor: statedColor, borderBottomColor: statedColor, borderBottomWidth: 2, fontSize: 11, display: 'flex', flexDirection: "row", justifyContent: 'space-between', height: 85, zIndex: 9999 }}>
        <StatusBar
          backgroundColor="#000000"
          barStyle="light-content"
          animated={true}
          StatusBarAnimation='slide'
        />
        <Left style={{ maxWidth: 50, }}>
          {
            this.props.leftside == 'profile' ?
              <Button transparent 
              onPress={() => { this.props.navigation.navigate("profile") }}
              
              >
                <Image
                  source={{ uri: LoggedInUserProfile.image_url }}
                  style={{ height: 30, width: 30, borderRadius: 30 / 2 }}
                />

              </Button>
              :
              this.props.leftside == 'menu' ?
                <Button transparent onPress={() => { this.props.navigation.toggleDrawer() }}>
                  <MaterialCommunityIcons name="menu" size={20} color='#fff' />
                </Button>
                :
                this.props.leftside == 'cancel' ?
                  <Button transparent onPress={() => { this.props.navigation.goBack() }}>
                    <AntDesign name='close' style={styles.iconsizeNcolour} />
                  </Button>
                  :
                  this.props.leftside == 'empty' ?
                    <Fragment>
                    </Fragment>
                    :
                    this.props.leftside == 'WhiteArr' ?
                      <Button transparent onPress={() => { this.props.navigation.goBack() }}>
                        <SvgUri
                          // style={{ marginTop: 20, marginLeft: -17 }}
                          width='11px'
                          height='22px'
                          uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1589394608/Mobile_Assets/arrbk_ipiscg.svg"
                        />
                        {/* <Image
                          source={WhiteArr}
                          style={{ height: 22, width: 11, }}
                        /> */}
                      </Button>
                      :
                      <Button transparent onPress={() => { this.props.navigation.goBack() }}>
                         <SvgUri
                          // style={{ marginTop: 20, marginLeft: -17 }}
                          width='11px'
                          height='22px'
                          uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1597714885/Mobile_Assets/arrBackBlk_ralvt8.svg"
                        />
                      </Button>

          }
        </Left>
        {
          this.props.title == 'Workbrook' ?
            <Body style={{ flex: 1, display: 'flex', justifyContent: 'center', flexDirection: 'row', }}>
              <SvgUri
                width='166px'
                height='28px'
                uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1590509137/Mobile_Assets/workbrook_ep9zfi.svg"
              >


              </SvgUri>
            </Body>
            :
            <Body style={{ flex: 1, display: 'flex', justifyContent: 'center', flexDirection: 'row' }}>
              <Title style={{ color: colorChanger, fontWeight: '600', fontSize: 18, }}> {this.props.title || title}</Title>
            </Body>
        }

        <Right style={{ maxWidth: 60, }}>

          {

            this.props.rightside == 'cancel' ?


              <Button transparent onPress={() => { this.props.navigation.goBack() }}>
                <AntDesign name='close' style={styles.iconsizeNcolour} />
              </Button>

              :
              this.props.rightside == 'save' ?
                <Button transparent style={[styles.savesty, { padding: 0, }]}
                  onPress={this.props.savehandler || null}

                >
                  <Text style={{ color: "#fff", fontSize: 14, fontWeight: '600', }}> Save </Text>
                </Button>
                :
                this.props.rightside == 'RhsDrawer' ?
                  <Button transparent onPress={() => { this.props.navigation.toggleDrawer() }}>
                    {/* <Image source={Bar} /> */}
                    <SvgUri
                      width='22px'
                      height='25px'
                      uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1590509287/Mobile_Assets/bar_v8ptvz.svg"
                    ></SvgUri>
                  </Button>
                  :
                this.props.rightside == 'RhsDrawerWhite' ?
                  <Button transparent onPress={() => { this.props.navigation.toggleDrawer() }}>
                    {/* <Image source={Bar} /> */}
                    <SvgUri
                      width='22px'
                      height='25px'
                      uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1598315444/App/bars-solid_1_mtxi20.svg"
                    ></SvgUri>
                  </Button>
                  :
                  this.props.rightside == 'darkCancel' ?
                    <Button transparent onPress={() => { this.props.navigation.goBack() }}>
                      <Image source={DarkCross} style={{ width: 18, height: 18 }} />
                    </Button>
                    :
                    this.props.rightside == 'buleCancel' ?
                      <Button transparent onPress={() => { this.props.navigation.goBack() }}>
                        <Image source={BlueClose} style={{ width: 18, height: 18 }} />
                      </Button>
                      :
                    this.props.rightside == 'search' ?
                      <Button transparent onPress={() => { this.props.navigation.toggleDrawer() }}>
                       
                       <SvgUri
                      width='19px'
                      height='20px'
                      uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1592541889/Mobile_Assets/search_amufvh.svg"
                    ></SvgUri>
                       
                      </Button>
                      :
                      this.props.rightside == 'empty' ?
                        <Fragment>
                        </Fragment>
                        :
                        <Button transparent onPress={() => { this.props.navigation.toggleDrawer() }}>
                          <MaterialCommunityIcons name="menu" size={30} color='#fff' />
                        </Button>
          }
        </Right>
      </Header>
    );
  }
}

export default withNavigation(CustomizeHeader);
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  iconsizeNcolour: {
    fontSize: 20,
    color: '#fff',
  },
  savesty: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: "center",
    // paddingBottom: 0,
    padding: 0
  },
  headerBody: {
    ...Platform.select({
      ios: {
        marginLeft: -150,
      },
      android: {
        marginLeft: -8,
      }
    }),

  },
  headerBody_2: {
    ...Platform.select({
      ios: {
        marginLeft: -80,
      },
      android: {
        marginLeft: -80,
      }
    }),

  }

});
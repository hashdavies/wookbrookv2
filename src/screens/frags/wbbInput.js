import React, { Component } from "react";
import { 
  View,
  Text,
  StyleSheet
  } from "react-native";
  import {StatusBar} from 'react-native';
  import { withNavigation } from 'react-navigation';
import { Left, Right, Icon, Title } from 'native-base';
 import { Container, Header, Content, Button,Thumbnail,Form, Item, Input, Label,Body,CheckBox,Badge } from 'native-base';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import SpinnerButton from 'react-native-spinner-button';

class WbbButton extends Component{
  constructor(props) {
    super(props);
    this.state = {
       defaultLoading:false
    };
   }
   componentDidMount(){
    }
 


  render(){
    return (
      <Input
      style={[this.props.Inputstyle || styles.defaultStyle, {backgroundColor:this.props.backgroundColorSty}]}
      placeholder={this.props.inputPlaceHolder}
      onPress={this.props.onPressAction}
    />
   );
  }
}
 
export default withNavigation(WbbButton);
const styles = StyleSheet.create({
  defaultStyle:{
    backgroundColor: '#FFF',
     borderRadius: 8, 
     elevation: 0,
      // padding: 22,
      height: 52,
      marginBottom: 17,
    },
    buttonText:{ 
      // textTransform:'capitalize',
      fontSize: 18,
      color:'#fff'
    },


});
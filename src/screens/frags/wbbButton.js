import React, { Component } from "react";
import { 
  View,
  Text,
  StyleSheet
  } from "react-native";
  import {StatusBar} from 'react-native';
  import { withNavigation } from 'react-navigation';
import { Left, Right, Icon, Title } from 'native-base';
 import { Container, Header, Content, Button,Thumbnail,Form, Item, Input, Label,Body,CheckBox,Badge } from 'native-base';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import SpinnerButton from 'react-native-spinner-button';

class WbbButton extends Component{
  constructor(props) {
    super(props);
    this.state = {
       defaultLoading:false
    };
   }
   componentDidMount(){
    }
 


  render(){
    return (
      <Button
      full={this.props.BtnWidth || true}
      style={[this.props.Btnstyle || styles.buttonStyle, {backgroundColor:this.props.backgroundColorSty}]}
      onPress={this.props.onPressAction}
    >
      <Text style={this.props.buttonTextStyle||styles.buttonText}>{this.props.buttonText}</Text>
    </Button>
   );
  }
}
 
export default withNavigation(WbbButton);
const styles = StyleSheet.create({
  buttonStyle:{
    backgroundColor: '#49CB70',
     borderRadius: 10, 
     elevation: 0,
      padding: 18,
      height: 56,
      marginBottom: 20,

    },
    buttonText:{ 
      // textTransform:'capitalize',
      fontSize: 18,
      color:'#fff'
    },


});
import React, { Component } from 'react';
import { Platform, StyleSheet, TouchableOpacity } from 'react-native';
import * as Animatable from 'react-native-animatable';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Feather from 'react-native-vector-icons/Feather';
import {
  Container,
  Text,
  Header,
  Content,
  Textarea,
  Button,
  View,
  Thumbnail,
  Form,
  Item,
  Input,
  Label,
  Badge,
  Icon,
  Footer,
  FooterTab,
  Picker,
  Item as FormItem,
} from 'native-base';

import { CustomAsset } from '../../../utils/assets';
import { IconStyles, AppColor } from '../../../utils/AppStyles';

const FooterNoteDetails = ({ data }) => {
  return (
    <Footer style={styles.footerWrap}>
      <View style={{ flexDirection: 'row', flex: 1, alignItems: 'center' }}>
        <View style={{ width: 80 }}>
          <View style={[styles.addnewcase_wrap]}>
            <Ionicons name="ios-arrow-back" style={[IconStyles.MidiumIcon, AppColor.GreenColor]} />
          </View>
        </View>
        <View style={{ width: 150, alignItems: 'flex-start' }}>
          <Text>Note</Text>
        </View>
      </View>
      <View style={[{ width: 100, flexDirection: 'row' }, styles.footertab_left]}>
        <View style={[styles.Icon_wrap]}>
          <Entypo name="new-message" style={[IconStyles.MidiumIcon, AppColor.BlackColour]} />
        </View>
        <View style={[styles.Icon_wrap]}>
          <Feather name="arrow-up-right" style={[IconStyles.MidiumIcon, AppColor.BlackColour]} />
        </View>
        <View style={[styles.Icon_wrap]}>
          <MaterialIcons name="delete" style={[IconStyles.MidiumIcon, AppColor.BlackColour]} />
        </View>
      </View>
    </Footer>
  );
};

export default FooterNoteDetails;

const styles = StyleSheet.create({
  footerWrap: {
    borderStyle: 'solid',
    borderTopWidth: 1,
    borderTopColor: 'green',
    backgroundColor: '#FFF',
    height: 70,
  },
  footertab: {
    backgroundColor: '#FFF',
  },
  footertab_left: {
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 16,
  },
  userDp: {
    height: 50,
    borderRadius: 50,
    width: 50,
    //  paddingTop:10
  },

  addnewcase_wrap: {
    // borderStyle:'solid',
    // borderColor:'#ccc',
    // borderWidth:2,
    borderRadius: 30,
    backgroundColor: '#fff',
    height: 50,
    width: 50,
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 12,
  },
  Icon_wrap: {
    // borderRadius:30,
    backgroundColor: '#FFF',
    height: 50,
    width: 25,
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 12,
  },
  // footer_icon_style: {
  //   fontSize:20,
  //   color:'#000',

  //   },
});

import React, { Component, Fragment } from 'react';
import { Text, StyleSheet, ImageBackground, Keyboard, TouchableOpacity, Alert,FlatList,ActivityIndicator } from 'react-native';
import { KeyboardAvoidingView, Image, ScrollView } from 'react-native';
import { StackActions } from 'react-navigation';

import Ionicons from 'react-native-vector-icons/Ionicons';
import Checker from '../../assets/workbrookAssets/checker.png';
import {
    Container,
    Header,
    Content,
    Textarea,
    Button,
    View,
    Thumbnail,
    Form,
    Item,
    Input,
    Label,
    Badge,
    Icon,
    Footer,
    Picker,
    Item as FormItem,
    Left,
    Body,
    CardItem,
    Card,
    Right,
    ListItem,
    List,
    CheckBox,
} from 'native-base';
import {
    Kaede,
    Hoshi,
    Jiro,
    Isao,
    Madoka,
    Akira,
    Hideo,
    Kohana,
    Makiko,
    Sae,
    Fumi,
} from 'react-native-textinput-effects';
import CustomizeHeader from '../frags/CustomizeHeader';

import { myspiner, temp, ShowNotification, AppInlineLoader } from '../../dependency/UtilityFunctions';
import Logo from '../../assets/workbrookAssets/logo.png';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import RBSheet from "react-native-raw-bottom-sheet";
import Interests from '../interests/Interests';
import Noti from "../../assets/workbrookAssets/noti.png";
import UserBl from "../../assets/workbrookAssets/userBl.png";
import ArrForward from "../../assets/workbrookAssets/arrfr.png";
import Shield from "../../assets/workbrookAssets/shield.png";
import Quest from "../../assets/workbrookAssets/quest.png";
import Info from "../../assets/workbrookAssets/info.png";
import Plus from "../../assets/workbrookAssets/plus.png";
import Remark from "../../assets/workbrookAssets/remark.png";
import Like from "../../assets/workbrookAssets/like.png";
import MoreIcon from "../../assets/workbrookAssets/more2.png";
import Profile from "../../assets/images/profile/dp2.png";
import cardPix from "../../assets/images/background/bg3_old.jpg";
import RNPickerSelect from 'react-native-picker-select';
import TagInput from 'react-native-tags-input';

import RNFetchBlob from 'react-native-fetch-blob'
import ImagePicker from 'react-native-image-picker';
import CloseModal from "../../assets/workbrookAssets/whitecloss.png";
import SuccessIcon from "../../assets/workbrookAssets/successPost.png";
import Modal from 'react-native-modal';
import AccountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import { text } from 'react-native-communications';
import DashboardStore from '../../stores/Dashboard';
import { hasSetIntrest } from '../../auth';
import Autocomplete from 'react-native-autocomplete-input';
import MultiSelect from 'react-native-multiple-select';
import { YOUR_CLOUDINARY_PRESET, YOUR_CLOUDINARY_NAME, requestCameraPermission, YOUR_CLOUDINARY_PRESET_JobBanner } from '../../dependency/UtilityFunctions';
import EmptyListComponent from '../../StatelessScreens/EmptyListComponent';

import RecruiterLanding from '../recruiter/RecruiterLanding';
items = [{
    id: '92iijs7yta',
    name: 'Ondo',
}, {
    id: 'a0s0a8ssbsd',
    name: 'Ogun',
}, {
    id: '16hbajsabsd',
    name: 'Calabar',
}, {
    id: 'sdhyaysdj',
    name: 'Kaduna',
}, {
    id: 'suudydjsjd',
    name: 'Abuja',
}];

@observer
class InsightRecruiterAvailable extends Component {
    constructor(props) {
        super(props);
        this.mapRef = null;
        this.state = {
            email: '',
            password: '',
            // isacceptedtermandCondition=false,

        }
        selectedItems = [];
this.CheckRecruiterFullDetails=this.CheckRecruiterFullDetails.bind(this)
     }
    onSelectedItemsChange = selectedItems => {
        this.setState({ selectedItems });
    };
    CheckRecruiterFullDetails = (item) => {
        DashboardStore._updateParameterV2(item, "SingleRecruiter", "Recruiters");
        this.props.navigation.navigate("RecruiterProfile");
        this.props.navigation.navigate(
            'RecruiterProfile',
            {
              data: "ViewOnly",
              otherParam: 'anything you want here',
            }
      
          );
    };
 
    async componentDidMount(){
         let UserloginToken=await temp();
        // let loggedinUserdetails=await loggedinUserdetails();
        console.log(UserloginToken);
        DashboardStore._getAxiosInstance(UserloginToken);
         DashboardStore.__GetJobRecruiters(1,30);

    }
  
    render() {
        const { selectedItems } = this.state;
        const {isGoBack,JobRecruiters,RedirectMode } = DashboardStore;
        
        // if(isGoBack===true){
        //     // this.props.navigation.goBack();
        //     const popAction = StackActions.pop({
        //         n: 1,
        //       });
              
        //       this.props.navigation.dispatch(popAction);
        //     DashboardStore._UpdateDefaultValue();
        // }

        let ErrorComponentDisplay=null;
 
        if (DashboardStore.isProcessing === true) {
            ErrorComponentDisplay = AppInlineLoader(DashboardStore.isProcessing)
        
       }
         else {
           ErrorComponentDisplay = <EmptyListComponent
           message={DashboardStore.Recruiters.ErrorMessage}
             onclickhandler={this.LoadAvailableProffesions}
           />
         }
  
        let AllRecruiters=
        <FlatList
            // Data
            data={JobRecruiters.RecruiterList}
            // Render Items
            renderItem={({ item }) => {
                console.log(item)
                // console.log("funmilayo")
                if(item==false){

                }
                else{
                    return (
            
                        <List style={{ borderBottomWidth: 1, borderBottomColor: '#E5E5E5', paddingBottom: 15 }}>
                        <ListItem avatar style={{ display: 'flex', justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center', }}>
                            <Left>
                                <Thumbnail style={styles.profileImgCltr} source={{uri:item.profilePicture}} />
                            </Left>
                            <Body style={{ borderBottomWidth: 0, }}>
                                <Text style={styles.cardTextJob4}>{item.fullname}</Text>
                    <Text style={styles.cardTextJob5} numberOfLines={1}>{item.location}</Text>
                            </Body>
                            <Right style={[styles.btnStyIdc, { borderBottomWidth: 0 }]}>
                                <Button transparent style={styles.ViewBtnSty}
                                // onPress={() => { this.props.navigation.navigate("RecruiterProfile") }}
                                onPress={() => {  this.CheckRecruiterFullDetails(item)}}
                                >
                                    <Text style={[styles.cardTextJob5, { color: '#1E93E2' }]}>VIEW</Text>
                                </Button>
                            </Right>
                        </ListItem>
                    </List>
                   
                         )
                }
         
            
            }}
            // Item Key
            keyExtractor={(item, index) => String(index)}
            // Header (Title)
            // ListHeaderComponent={this.renderHeader}
            // Footer (Activity Indicator)
            // ListFooterComponent={this.renderFooter()}
            // On End Reached (Takes a function)
            // onEndReached={this.retrieveMore}
            // How Close To The End Of List Until Next Data Request Is Made
            // onEndReachedThreshold={0}
            // Refreshing (Set To True When End Reached)
            // refreshing={this.state.refreshing}
          />
        return (
        
  
            <Container style={styles.container}>
                <CustomizeHeader
                    leftside='WhiteArr'
                    title={`Recruiters ( ${JobRecruiters.TotalCounter} )`}
                    rightside='empty'
                    statedColor='#000'
                    icon='md-close'
                    // savehandler={this.proceednow}
                />

                <ScrollView
                    contentContainerStyle={{

                    }}
                >
                    <View style={styles.headerWrap}>
                        <Text style={styles.cardTextJob1}>Recruiters
                         
                        </Text>
                    </View>
                    <View style={{paddingLeft: 15, paddingRight: 15}}>
                         {/* <Label style={styles.cardTextJob2}>Experience</Label> */}
                        {/* <Item regular style={styles.inputStyleWrap}> */}
                        {/* <Icon type="FontAwesome" active name='search' style={{color: "#C4C4C4"}} /> */}
                            {/* <Input
                            placeholder="Search"
                            
                            /> */}
                        {/* </Item> */}
                     </View>
                 
                     {
                        JobRecruiters.RecruiterList.length > 0 ?
                        AllRecruiters
                            :
                            ErrorComponentDisplay

                    }
                    {/* <List style={{ borderBottomWidth: 1, borderBottomColor: '#E5E5E5', paddingBottom: 15 }}>
                        <ListItem avatar style={{ display: 'flex', justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center', }}>
                            <Left>
                                <Thumbnail style={styles.profileImgCltr} source={Profile} />
                            </Left>
                            <Body style={{ borderBottomWidth: 0, }}>
                                <Text style={styles.cardTextJob4}>John Siimith>>>>>>>>>>>>>>>>>>></Text>
                                <Text style={styles.cardTextJob5} numberOfLines={1} >London, United Kingdom</Text>
                            </Body>
                            <Right style={[styles.btnStyIdc, { borderBottomWidth: 0 }]}>
                                <Button transparent style={styles.ViewBtnSty}
                                onPress={() => { this.props.navigation.navigate("RecruiterProfile") }}
                                >
                                    <Text style={[styles.cardTextJob5, { color: '#1E93E2' }]}>VIEW</Text>
                                </Button>
                            </Right>
                        </ListItem>
                    </List> */}
                   
         
                </ScrollView>



             
            </Container>
                

        );
    }
}
export default InsightRecruiterAvailable;

const styles = StyleSheet.create({
    MContainer: {
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
    },
    container: {
        backgroundColor: "white",
        display: "flex",
        flex: 1,
        justifyContent: "flex-start",
    },

    rmPadding: {
        paddingTop: 0,
        paddingRight: 0,
        paddingBottom: 0,
        paddingLeft: 0,
    },
    rmPaddingLR: {
        paddingLeft: 0,
        paddingRight: 0,
    },

    cardTextJob1: {
        color: "#2F2F2F",
        fontSize: 36,
        fontWeight: '600',

    },
    cardTextJob2: {
        color: "#BCBCBC",
        fontSize: 14,
        fontWeight: '600',
        marginBottom: 5,
    },
    cardTextJob2_2: {
        color: "#fff",
        fontSize: 14,
        fontWeight: '600',
    },
    cardTextJob3: {
        color: "#2F2F2F",
        fontSize: 13,
        fontWeight: '500',
    },
    cardTextJob4: {
        fontSize: 12,
        color: '#2F2F2F',
        fontWeight: '600'
    },

    cardTextJob5: {
        color: "#2F2F2F",
        fontSize: 10,
        fontWeight: '500',
    },
    cardTextJob7: {
        color: "#1E93E2",
        fontSize: 11,
        fontWeight: '500',
    },
    cardTextJob4_1: {
        color: "#fff",
        fontSize: 12,
        fontWeight: '500',
    },

    cardTextJob6: {
        color: "#1E93E2",
        fontSize: 11,
        fontWeight: '500',
    },


    cardText1: {
        color: "#2F2F2F",
        fontWeight: '500',
        fontSize: 12,
    },
    inputStyleWrap: {
        height: 38,
        backgroundColor: '#E5E5E5',
        borderRadius: 5,
    },
    headerWrap: {
        marginBottom: 13,
        marginTop: 20,
        paddingLeft: 25,
        paddingRight: 25,
    },
    serachbtnSty: {
        height: 54,
        borderRadius: 4,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#5ABC7A',
        marginTop: 24
    },
    ViewBtnSty: {
        height: 23,
        width: 50,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#1E93E2',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    btnStyIdc: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    profileImgCltr: {
       height: 42,
       width: 41,
       borderRadius: 42 / 2,
    },

});

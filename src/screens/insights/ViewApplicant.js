import React, { Component, Fragment } from 'react';
import { Text, StyleSheet, ImageBackground, Keyboard, TouchableOpacity, Alert, FlatList, ActivityIndicator } from 'react-native';
import { KeyboardAvoidingView, Image, ScrollView } from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons';
import Checker from '../../assets/workbrookAssets/checker.png';
import {
    Container,
    Header,
    Content,
    Textarea,
    Button,
    View,
    Thumbnail,
    Form,
    Item,
    Input,
    Label,
    Badge,
    Icon,
    Footer,
    Picker,
    Item as FormItem,
    Left,
    Body,
    CardItem,
    Card,
    Right,
    ListItem,
    List,
    CheckBox,
} from 'native-base';
import {
    Kaede,
    Hoshi,
    Jiro,
    Isao,
    Madoka,
    Akira,
    Hideo,
    Kohana,
    Makiko,
    Sae,
    Fumi,
} from 'react-native-textinput-effects';
import CustomizeHeader from '../frags/CustomizeHeader';

import { myspiner, temp, ShowNotification, AppInlineLoader } from '../../dependency/UtilityFunctions';
import Logo from '../../assets/workbrookAssets/logo.png';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import RBSheet from "react-native-raw-bottom-sheet";
import Interests from '../interests/Interests';
import Noti from "../../assets/workbrookAssets/noti.png";
import UserBl from "../../assets/workbrookAssets/userBl.png";
import ArrForward from "../../assets/workbrookAssets/arrfr.png";
import Shield from "../../assets/workbrookAssets/shield.png";
import Quest from "../../assets/workbrookAssets/quest.png";
import Info from "../../assets/workbrookAssets/info.png";
import Plus from "../../assets/workbrookAssets/plus.png";
import Remark from "../../assets/workbrookAssets/remark.png";
import Like from "../../assets/workbrookAssets/like.png";
import MoreIcon from "../../assets/workbrookAssets/more2.png";
import Profile from "../../assets/images/profile/dp2.png";
import cardPix from "../../assets/images/background/bg3_old.jpg";
import RNPickerSelect from 'react-native-picker-select';
import TagInput from 'react-native-tags-input';

import RNFetchBlob from 'react-native-fetch-blob'
import ImagePicker from 'react-native-image-picker';
import CloseModal from "../../assets/workbrookAssets/whitecloss.png";
import SuccessIcon from "../../assets/workbrookAssets/successPost.png";
import Modal from 'react-native-modal';
import AccountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import { text } from 'react-native-communications';
import DashboardStore from '../../stores/Dashboard';
import { hasSetIntrest } from '../../auth';
import Autocomplete from 'react-native-autocomplete-input';
import MultiSelect from 'react-native-multiple-select';
import { YOUR_CLOUDINARY_PRESET, YOUR_CLOUDINARY_NAME, requestCameraPermission, YOUR_CLOUDINARY_PRESET_JobBanner } from '../../dependency/UtilityFunctions';
import EmptyListComponent from '../../StatelessScreens/EmptyListComponent';


items = [{
    id: '92iijs7yta',
    name: 'Ondo',
}, {
    id: 'a0s0a8ssbsd',
    name: 'Ogun',
}, {
    id: '16hbajsabsd',
    name: 'Calabar',
}, {
    id: 'sdhyaysdj',
    name: 'Kaduna',
}, {
    id: 'suudydjsjd',
    name: 'Abuja',
}];

@observer
class ViewApplicant extends Component {
    constructor(props) {
        super(props);
        this.mapRef = null;
        this.state = {
            email: '',
            password: '',
            currentStage: 0,

        }
        this.ChangeApplicationStage = this.ChangeApplicationStage.bind(this);
        this.RejectApplication = this.RejectApplication.bind(this);

    }

    async componentDidMount() {
        const { JobApplicants } = DashboardStore
        let applicantdata = JobApplicants.singleApplicant;
        let applicationStage = applicantdata.applicationStage;
        let thisUserId = applicantdata.id;
        // alert(thisUserId +".....")
        this.setState({
            currentStage: applicationStage
        })
        // OtherProfile.User_id;

        DashboardStore._updateParameterV2(thisUserId, "User_id", "OtherProfile");

        const { singleJob } = DashboardStore.JobOffers;
        let job_id = singleJob.id;
        let Applicant_id = applicantdata.applicant_id;

        DashboardStore.__ChangeApplicatView(job_id,Applicant_id);

    }
    ChangeApplicationStage = (stage_code) => {
        this.setState({
            currentStage: stage_code
        })
        const { JobApplicants } = DashboardStore;
        const { singleJob } = DashboardStore.JobOffers;
        let job_id = singleJob.id;

        let applicantdata = JobApplicants.singleApplicant;
        let Applicant_id = applicantdata.applicant_id;
        let buttonText = '';
        let defaultMessage = '';
        if (stage_code === 1) {
            buttonText = "Send consideration";
            defaultMessage = 'Success You are being considered for the role';
        }
        else if (stage_code === 2) {

            buttonText = "Send screening details";
            defaultMessage = 'You have been sent details for the screening';
        }
        else if (stage_code === 3) {

            buttonText = "Send interview details";
            defaultMessage = 'You have been invited for interviews ';
        }
        else if (stage_code === 4) {

            buttonText = "Send employment letter";
            defaultMessage = 'You got the job!';
        }


        DashboardStore._updateParameterV2(stage_code, "stage_code", "ProcessJobApplication");
        DashboardStore._updateParameterV2(buttonText, "buttonText", "ProcessJobApplication");
        DashboardStore._updateParameterV2(defaultMessage, "defaultMessage", "ProcessJobApplication");
        DashboardStore._updateParameterV2(job_id, "job_id", "ProcessJobApplication");
        DashboardStore._updateParameterV2(Applicant_id, "Applicant_id", "ProcessJobApplication");

        this.props.navigation.navigate("ViewApp")
    };
    Proceed2Reject = () => {
    let stage_code=this.state.currentStage;
        const { JobApplicants } = DashboardStore;
        const { singleJob } = DashboardStore.JobOffers;
        let job_id = singleJob.id;

        let applicantdata = JobApplicants.singleApplicant;
        let Applicant_id = applicantdata.applicant_id;
        // let buttonText = '';
        let defaultMessage = '';
        // if (stage_code === 1) {
        //     buttonText = "Send consideration";
        //     defaultMessage = 'Success You are being considered for the role';
        // }
        // else if (stage_code === 2) {

        //     buttonText = "Send screening details";
        //     defaultMessage = 'You have been sent details for the screening';
        // }
        // else if (stage_code === 3) {

        //     buttonText = "Send interview details";
        //     defaultMessage = 'You have been invited for interviews ';
        // }
        // else if (stage_code === 4) {

        //     buttonText = "Send employment letter";
        //     defaultMessage = 'You got the job!';
        // }


        DashboardStore._updateParameterV2(stage_code, "stage_code", "ProcessJobApplication");
        // DashboardStore._updateParameterV2(buttonText, "buttonText", "ProcessJobApplication");
        DashboardStore._updateParameterV2(defaultMessage, "defaultMessage", "ProcessJobApplication");
        DashboardStore._updateParameterV2(job_id, "job_id", "ProcessJobApplication");
        DashboardStore._updateParameterV2(Applicant_id, "Applicant_id", "ProcessJobApplication");

        DashboardStore.__RejectApplication();
    };
    RejectApplication = () => {
        Alert.alert(
            'Regret Application',
            `Are you sure ? `,
            [
                {
                    text: 'No', onPress: () => { },//Do nothing
                    style: 'cancel'
                },
                {
                    text: 'Yes', onPress: () => {
                       this.Proceed2Reject();
                    }
                },
            ],
            { cancelable: true }
        );
    };
    render() {
        const { JobApplicants } = DashboardStore
        const { currentStage } = this.state
        let applicantdata = JobApplicants.singleApplicant;
        let ApplicantName = applicantdata.first_name == null || applicantdata.last_name == null ? applicantdata.username : `${applicantdata.first_name}  ${applicantdata.last_name} `
        // const { isGoBack } = DashboardStore;
        // if (isGoBack === true) {
        //     this.props.navigation.goBack();
        //     DashboardStore._UpdateDefaultValue();
        // }
        const { isGoBack ,RedirectMode} = DashboardStore;
        // if (isGoBack === true) {
        //     this.props.navigation.goBack();
        //     DashboardStore._UpdateDefaultValue();
        // }
        if (RedirectMode.rejectApplicant === true) {
            let _CurrentRedirect=RedirectMode.CurrentRedirect;
                 DashboardStore._updateParameterV2(false, "rejectApplicant", "RedirectMode");
        DashboardStore._updateParameterV2("David_Nill", "rejectApplicant", "RedirectMode");
        this.props.navigation.goBack();

        }
        return (

            <Container style={styles.container}>
                <CustomizeHeader
                    leftside='WhiteArr'
                    title='View applicant'
                    rightside='empty'
                    statedColor='#1E93E2'
                    icon='md-close'
                // savehandler={this.proceednow}
                />

                <ScrollView
                    contentContainerStyle={{
                        padding: 16,
                    }}
                >
                    <Card style={{ marginBottom: 16, backgroundColor: '#FFF', elevation: 0, borderRadius: 5, borderColor: 'transparent' }}>
                        <CardItem style={{ backgroundColor: 'transparent' }}>
                            <Left>
                                <Thumbnail style={{ width: 46, height: 48, borderRadius: 48 / 2 }} source={{ uri: applicantdata.image_url }} />

                                <Body>
                                <TouchableOpacity onPress={() => { this.props.navigation.navigate("otherProfile") }}>
                                        <Text style={[styles.Text8, { color: '#2F2F2F', fontSize: 13 }]}>
                                            {/* Jane Dokaszhuk */}
                                            {ApplicantName}
                                        </Text>
                                        <Text style={[styles.Text8, {}]}>
                                            {/* View Profile {this.state.currentStage} */}
                                            View Profile 
                                        </Text>
                                    </TouchableOpacity>
                                </Body>
                            </Left>
                            <Right>
                                <Button transparent style={styles.ViewBtnSty}
                                    onPress={this.RejectApplication}

                                >
                                    <Text style={styles.Text1}>{DashboardStore.isProcessing===true ? 'Processing...':'Regret'}</Text>
                                </Button>
                            </Right>
                        </CardItem>
                        <View style={styles.cardBodySty}>
                            <Text style={styles.Text2}>
                                {/* {JSON.stringify(JobApplicants.singleApplicant)} */}
                                {/* Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation. */}
                                {applicantdata.about}
                            </Text>
                        </View>
                        <View style={styles.cardFooterSty}>
                            <Button style={[currentStage == 1 ? styles.applicantStateActive : styles.applicantState]}
                                // onPress={() => { this.props.navigation.navigate("ViewApp") }}
                                onPress={() => { this.ChangeApplicationStage(1) }}

                            >
                                <Text style={currentStage == 1 ? styles.stateactiveText : styles.stateNotactiveText}> Consider</Text>

                            </Button>
                            <Button style={[currentStage == 2 ? styles.applicantStateActive : styles.applicantState]}
                                onPress={() => { this.ChangeApplicationStage(2) }}

                            >
                                <Text style={currentStage == 2 ? styles.stateactiveText : styles.stateNotactiveText}> Test/Screen</Text>
                            </Button>
                            <Button style={[currentStage == 3 ? styles.applicantStateActive : styles.applicantState]}
                                onPress={() => { this.ChangeApplicationStage(3) }}
                            >
                                <Text style={currentStage == 3 ? styles.stateactiveText : styles.stateNotactiveText}> Interview</Text>
                            </Button>
                            <Button style={[currentStage == 4 ? styles.applicantStateActive : styles.applicantState]}
                                onPress={() => { this.ChangeApplicationStage(4) }}
                            >
                                <Text style={currentStage == 4 ? styles.stateactiveText : styles.stateNotactiveText}> Employ</Text>
                            </Button>
                        </View>
                    </Card>

                </ScrollView>

                {/* 655956 */}

            </Container>


        );
    }
}
export default ViewApplicant;

const styles = StyleSheet.create({
    container: {
        backgroundColor: "#F8F7F7",
        display: "flex",
        flex: 1,
        justifyContent: "flex-start",
    },

    rmPadding: {
        paddingTop: 0,
        paddingRight: 0,
        paddingBottom: 0,
        paddingLeft: 0,
    },
    rmPaddingLR: {
        paddingLeft: 0,
        paddingRight: 0,
    },

    cardTextJob1: {
        color: "#2F2F2F",
        fontSize: 36,
        fontWeight: '600',

    },
    cardTextJob2: {
        color: "#BCBCBC",
        fontSize: 14,
        fontWeight: '600',
        marginBottom: 5,
    },
    cardTextJob2_2: {
        color: "#fff",
        fontSize: 14,
        fontWeight: '600',
    },
    cardTextJob3: {
        color: "#2F2F2F",
        fontSize: 13,
        fontWeight: '500',
    },
    cardTextJob4: {
        fontSize: 12,
        color: '#2F2F2F',
        fontWeight: '600'
    },

    cardTextJob5: {
        color: "#2F2F2F",
        fontSize: 10,
        fontWeight: '500',
    },
    cardTextJob7: {
        color: "#1E93E2",
        fontSize: 11,
        fontWeight: '500',
    },
    cardTextJob4_1: {
        color: "#fff",
        fontSize: 12,
        fontWeight: '500',
    },

    cardTextJob6: {
        color: "#1E93E2",
        fontSize: 11,
        fontWeight: '500',
    },


    cardText1: {
        color: "#2F2F2F",
        fontWeight: '500',
        fontSize: 12,
    },
    Text1: {
        color: "#FAFAFA",
        fontWeight: '500',
        fontSize: 11,
    },
    Text2: {
        color: "#676767",
        fontWeight: '500',
        fontSize: 11,
    },
    cardBodySty: {
        marginTop: 30,
        marginBottom: 25,
        fontWeight: '500',
        fontSize: 11,
        paddingLeft: 20,
        paddingRight: 20,
    },
    stateactiveText: {
        fontSize: 11,
        fontWeight: 'bold',
        color: '#FAFAFA'
    },
    stateNotactiveText: {
        fontSize: 11,
        fontWeight: 'bold',
        color: '#B4B4B4'
    },
    applicantState: {
        width: "48%",
        height: 28,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#B4B4B4',
        fontSize: 11,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent',
        elevation: 0,
        marginBottom: 8,
    },
    applicantStateActive: {
        width: "48%",
        height: 28,
        borderRadius: 5,
        backgroundColor: '#1E93E2',
        fontSize: 11,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        elevation: 0,
        marginBottom: 8,

    },
    cardFooterSty: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        flexWrap: 'wrap',
        paddingLeft: 20,
        paddingRight: 20,
        marginBottom: 20,

    },
    inputStyleWrap: {
        height: 38,
        backgroundColor: '#E5E5E5',
        borderRadius: 5,
    },
    headerWrap: {
        marginBottom: 13,
        marginTop: 20,
        paddingLeft: 25,
        paddingRight: 25,
    },
    serachbtnSty: {
        height: 54,
        borderRadius: 4,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#5ABC7A',
        marginTop: 24
    },
    ViewBtnSty: {
        height: 23,
        width: 50,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#FF5964',
        backgroundColor: "#FF5964",
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    btnStyIdc: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    applicantImg: {
        height: 31,
        width: 32,
        borderRadius: 32 / 2,
    },
    numberCount: {
        fontWeight: '600',
        fontSize: 12,
        color: '#1E93E2',
        marginRight: 11
    },
    leftAlignSty: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
    },
    Text8: {
        fontSize: 12,
        fontWeight: '600',
        color: "#1E93E2",
    },


});

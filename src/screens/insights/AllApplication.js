import React, { Component, Fragment } from 'react';
import { Text, StyleSheet, ImageBackground, Keyboard, TouchableOpacity, Alert, FlatList, ActivityIndicator } from 'react-native';
import { KeyboardAvoidingView, Image, ScrollView } from 'react-native';
import Modal from 'react-native-modal';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Checker from '../../assets/workbrookAssets/checker.png';
import {
    Container,
    Header,
    Content,
    Textarea,
    Button,
    View,
    Thumbnail,
    Form,
    Item,
    Input,
    Label,
    Badge,
    Icon,
    Footer,
    Picker,
    Item as FormItem,
    Left,
    Body,
    CardItem,
    Card,
    Right,
    ListItem,
    List,
    CheckBox,
} from 'native-base';
import {
    Kaede,
    Hoshi,
    Jiro,
    Isao,
    Madoka,
    Akira,
    Hideo,
    Kohana,
    Makiko,
    Sae,
    Fumi,
} from 'react-native-textinput-effects';
import CustomizeHeader from '../frags/CustomizeHeader';
import { SvgUri } from 'react-native-svg';


import { myspiner, temp, ShowNotification, AppInlineLoader, validateEmail } from '../../dependency/UtilityFunctions';
import Logo from '../../assets/workbrookAssets/logo.png';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import RBSheet from "react-native-raw-bottom-sheet";
import Interests from '../interests/Interests';
import Noti from "../../assets/workbrookAssets/noti.png";
import UserBl from "../../assets/workbrookAssets/userBl.png";
import ArrForward from "../../assets/workbrookAssets/arrfr.png";
import Shield from "../../assets/workbrookAssets/shield.png";
import Quest from "../../assets/workbrookAssets/quest.png";
import Info from "../../assets/workbrookAssets/info.png";
import Plus from "../../assets/workbrookAssets/plus.png";
import Remark from "../../assets/workbrookAssets/remark.png";
import Like from "../../assets/workbrookAssets/like.png";
import MoreIcon from "../../assets/workbrookAssets/more2.png";
import Profile from "../../assets/images/profile/dp2.png";
import cardPix from "../../assets/images/background/bg3_old.jpg";
import RNPickerSelect from 'react-native-picker-select';
import TagInput from 'react-native-tags-input';

import RNFetchBlob from 'react-native-fetch-blob'
import ImagePicker from 'react-native-image-picker';
import CloseModal from "../../assets/workbrookAssets/whitecloss.png";
import SuccessIcon from "../../assets/workbrookAssets/successPost.png";
import AccountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import { text } from 'react-native-communications';
import DashboardStore from '../../stores/Dashboard';
import { hasSetIntrest } from '../../auth';
import Autocomplete from 'react-native-autocomplete-input';
import MultiSelect from 'react-native-multiple-select';
import { YOUR_CLOUDINARY_PRESET, YOUR_CLOUDINARY_NAME, requestCameraPermission, YOUR_CLOUDINARY_PRESET_JobBanner } from '../../dependency/UtilityFunctions';
import EmptyListComponent from '../../StatelessScreens/EmptyListComponent';
import { borderRadius } from 'react-select/src/theme';
import style from 'react-native-datepicker/style';


items = [{
    id: '92iijs7yta',
    name: 'Ondo',
}, {
    id: 'a0s0a8ssbsd',
    name: 'Ogun',
}, {
    id: '16hbajsabsd',
    name: 'Calabar',
}, {
    id: 'sdhyaysdj',
    name: 'Kaduna',
}, {
    id: 'suudydjsjd',
    name: 'Abuja',
}];

@observer
class AllApplication extends Component {
    constructor(props) {
        super(props);
        this.mapRef = null;
        this.state = {
            email: '',
            password: '',
            ApplicationStagestage: '*',
            ActiveView: "All",
            selectShortListModal: false,
            sendShortListModal: false,
            confirmShortListModal: false,
            SuccessReplyModal: false,
            inviteUserModal: false,
            SelectedUser: {},
            invitedUserfullname: '',
            invitedUserEmail: '',

        }
        this.ReloadPageData = this.ReloadPageData.bind(this);
        this.ManageApplicant = this.ManageApplicant.bind(this);
        this.ChangeFetchParams = this.ChangeFetchParams.bind(this);
        this.ToggleShortlistModal = this.ToggleShortlistModal.bind(this);
        this.ToggleSendShortlistModal = this.ToggleSendShortlistModal.bind(this);
        this.ToggleconfirmShortlistModal = this.ToggleconfirmShortlistModal.bind(this);
        this.ToggleSuccessReplyModal = this.ToggleSuccessReplyModal.bind(this);
        this.ToggleinviteUserModal = this.ToggleinviteUserModal.bind(this);

    }
    ToggleShortlistModal = () => {
        this.setState({ selectShortListModal: !this.state.selectShortListModal });
    };
    ToggleSendShortlistModal = (selectedUser) => {
        this.setState({
            selectShortListModal: false,
            sendShortListModal: !this.state.sendShortListModal,
            SelectedUser: selectedUser,

        });
    };
    ToggleconfirmShortlistModal = () => {
        this.setState({
            selectShortListModal: false,
            sendShortListModal: false,
            inviteUserModal: false,
            confirmShortListModal: !this.state.confirmShortListModal,

        });
    };
    FormatValue = () => {
        const { SelectedUser, invitedUserfullname, invitedUserEmail } = this.state;

        // let this={email:}
        let preparedata = {
            email: invitedUserEmail,
            username: invitedUserfullname,
            first_name: null,
        }
        console.log(preparedata)
        this.setState({
            SelectedUser: preparedata

        })
    }

    ToggleSuccessReplyModal = () => {
        const { ApplicationStagestage, SelectedUser } = this.state;
        let User = SelectedUser.first_name == null || SelectedUser.last_name == null ? SelectedUser.username : `${SelectedUser.first_name}  ${SelectedUser.last_name} `
        const { singleJob } = DashboardStore.JobOffers;
        console.log(singleJob)
        console.log("singleJob");
        let jobId = singleJob.id;
        let receiverMail = SelectedUser.email;
        if (User === '') {
            ShowNotification("Receiver Name Can not be empty", true);
            return;
        }
        if (receiverMail === '') {
            ShowNotification("Receiver email Can not be empty", true);
            return;
        }
        if (validateEmail(receiverMail) === false) {
            ShowNotification("Enter a valid Email", true);
            return;
        }
        this.setState({
            selectShortListModal: false,
            sendShortListModal: false,
            confirmShortListModal: false,
            SuccessReplyModal: !this.state.SuccessReplyModal,

        });


        let Data = {
            "job_stage": parseInt(ApplicationStagestage),
            "recievers_user_id": parseInt(SelectedUser.id),
            "job_id": parseInt(jobId),
            "recievers_email": receiverMail,
            "recievers_fullName": User,
            "notification_url": "https://wookbrookapp.page.link/naxz",

        }
        console.log(Data);
        DashboardStore._updateParameterV2(Data, "Shortlistdata", "AppUsersList");
        DashboardStore.__SendShortListToUser();
    };
    CloseSentSuccessReplyModal = () => {
        DashboardStore._updateParameterV2(false, "showSuccessReplyModal", "AppUsersList");

    };
    ToggleinviteUserModal = () => {
        this.setState({
            selectShortListModal: false,
            inviteUserModal: !this.state.inviteUserModal,

        });
    };
    SearchAppUsers = (query) => {
        // alert("hey");
        let stringlenght = query.length;
        if (stringlenght < 4) {
            DashboardStore._updateParameterV2(0, "queryString", "AppUsersList");
            DashboardStore.__SearchAllAppUsers(false);
            return;
        }
        DashboardStore._updateParameterV2(query, "queryString", "AppUsersList");
        DashboardStore.__SearchAllAppUsers(false);
    }
    async componentDidMount() {
        const { ApplicationStagestage, SelectedUser } = this.state;
        DashboardStore.__GetJobApplicant(ApplicationStagestage, 1, 10);
        DashboardStore.__SearchAllAppUsers();
    }
    ReloadPageData = () => {
        const { ApplicationStagestage } = this.state;

        DashboardStore.__GetJobApplicant(ApplicationStagestage, 1, 10);

    };
    ManageApplicant = (item) => {

        DashboardStore._updateParameterV2(item, "singleApplicant", "JobApplicants");

        this.props.navigation.navigate("ViewApplicant")
    };
    ChangeFetchParams = (params, params2) => {
        this.setState({
            ApplicationStagestage: params,
            ActiveView: params2,
        })
        DashboardStore.__GetJobApplicant(params, 1, 10);

    };
    render() {
        const { JobApplicants, AppUsersList } = DashboardStore;
        const { ApplicationStagestage, SelectedUser } = this.state;

        let ErrorComponentDisplay = null;

        if (DashboardStore.isProcessing === true) {
            ErrorComponentDisplay = AppInlineLoader(DashboardStore.isProcessing)

        }
        else {
            ErrorComponentDisplay = <EmptyListComponent
                message={DashboardStore.JobApplicants.ErrorMessage}
                onclickhandler={this.ReloadPageData}
            />
        }


        let JobOffersListing =
            <FlatList
                data={JobApplicants.ApplicantList}
                // Render Items
                renderItem={({ item, index }) => {
                    //   let Applicants=item.applicants
                    // let posterName=  `${poster.first_name}  ${poster.last_name} `
                    // let poster = item.created_by_profile
                    let posterName = item.first_name == null || item.last_name == null ? item.username : `${item.first_name}  ${item.last_name} `

                    return (

                        <List style={{ borderBottomWidth: 1, borderBottomColor: '#E5E5E5', paddingBottom: 15 }}>
                            <ListItem Button avatar style={{ display: 'flex', justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center', }}
                                onPress={() => { this.ManageApplicant(item) }}
                            >
                                <Left style={styles.leftAlignSty}>
                                    <Text style={styles.numberCount}>{index + 1}</Text>
                                    <Thumbnail style={styles.applicantImg} source={{ uri: item.image_url }} />
                                </Left>
                                <Body style={[styles.leftAlignSty, { borderBottomWidth: 0, marginBottom: -3 }]}>
                                    <Text style={styles.cardTextJob4}>{posterName}</Text>
                                    {item.isViewed === "0" ?
                                        <View style={styles.IndicatorSty}>
                                            <Text style={[styles.cardText2, { fontWeight: "500" }]}>New</Text>
                                        </View>
                                        :
                                        null

                                    }
                                </Body>

                            </ListItem>
                        </List>
                    )
                }}
                // Item Key
                keyExtractor={(item, index) => String(index)}
                // Header (Title)
                // ListHeaderComponent={this.renderHeader}
                // Footer (Activity Indicator)
                // ListFooterComponent={this.renderFooter()}
                // On End Reached (Takes a function)
                onEndReached={this.retrieveMore}
            // How Close To The End Of List Until Next Data Request Is Made
            // onEndReachedThreshold={0}
            // Refreshing (Set To True When End Reached)
            // refreshing={this.state.refreshing}
            />

        let SearchAppUserResult =
            <FlatList
                data={AppUsersList.Userlist}
                // Render Items
                renderItem={({ item, index }) => {
                    //   let Applicants=item.applicants
                    // let posterName=  `${poster.first_name}  ${poster.last_name} `
                    // let poster = item.created_by_profile
                    let posterName = item.first_name == null || item.last_name == null ? item.username : `${item.first_name}  ${item.last_name} `

                    return (

                        <ListItem Button avatar style={{ display: 'flex', justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center', }}
                            onPress={() => this.ToggleSendShortlistModal(item)}
                        >
                            <Left style={styles.leftAlignSty}>

                                <Thumbnail style={styles.applicantImg} source={{ uri: item.image_url }} />
                            </Left>
                            <Body style={[styles.leftAlignSty, { borderBottomWidth: 0, marginBottom: -3 }]}>
                                <Text style={styles.cardTextJob4}>{posterName}</Text>

                            </Body>

                        </ListItem>
                    )
                }}
                // Item Key
                keyExtractor={(item, index) => String(index)}
                // Header (Title)
                // ListHeaderComponent={this.renderHeader}
                // Footer (Activity Indicator)
                // ListFooterComponent={this.renderFooter()}
                // On End Reached (Takes a function)
                onEndReached={this.retrieveMore}
            // How Close To The End Of List Until Next Data Request Is Made
            // onEndReachedThreshold={0}
            // Refreshing (Set To True When End Reached)
            // refreshing={this.state.refreshing}
            />

        return (

            <Container style={styles.container}>
                <CustomizeHeader
                    leftside='WhiteArr'
                    title='Showing All Applicants'
                    rightside='empty'
                    statedColor='#1E93E2'
                    icon='md-close'
                // savehandler={this.proceednow}
                />
                <View style={styles.likeTabSty}>
                    <TouchableOpacity style={[{ display: "flex", flexDirection: 'row', justifyContent: "center", alignItems: 'center' }]}
                        onPress={() => { this.ChangeFetchParams('*', 'All') }}

                    >
                        <Text style={[this.state.ActiveView == "All" ? styles.activeView : styles.cardText2, { marginRight: 3 }]}>All</Text>
                        <View style={styles.bargeCount}>
                            <Text style={styles.cardText1}>
                                {JobApplicants.TotalCounter || 0}
                            </Text>
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity
                        onPress={() => { this.ChangeFetchParams('1', 'Consider') }}

                    >
                        <Text style={[this.state.ActiveView == "Consider" ? styles.activeView : styles.cardText2, { marginRight: 3 }]}>Consider</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => { this.ChangeFetchParams('2', 'Test') }}
                    >
                        <Text style={[this.state.ActiveView == "Test" ? styles.activeView : styles.cardText2, { marginRight: 3 }]}>Test</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => { this.ChangeFetchParams('3', 'Interview') }}

                    >
                        <Text style={[this.state.ActiveView == "Interview" ? styles.activeView : styles.cardText2, { marginRight: 3 }]}>Interview</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => { this.ChangeFetchParams('5', 'Regret') }}

                    >
                        <Text style={[this.state.ActiveView == "Regret" ? styles.activeView : styles.cardText2, { marginRight: 3 }]}>Regret</Text>
                    </TouchableOpacity>
                </View>

                <ScrollView
                    contentContainerStyle={{
                        padding: 35,
                    }}
                >
                    <View>
                        <Text style={styles.cardTextJob3}>
                            {this.state.ActiveView} Applicant(s)
                            </Text>

                    </View>
                    {
                        JobApplicants.ApplicantList.length > 0 ?
                            JobOffersListing
                            :
                            ErrorComponentDisplay

                    }



                    {/* <List style={{ borderBottomWidth: 1, borderBottomColor: '#E5E5E5', paddingBottom: 15 }}>
                        <ListItem Button avatar style={{ display: 'flex', justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center', }}
                            onPress={() => { this.props.navigation.navigate("ViewApplicant") }}
                        >
                            <Left style={styles.leftAlignSty}>
                                <Text style={styles.numberCount}>2</Text>
                                <Thumbnail style={styles.applicantImg} source={Profile} />
                            </Left>
                            <Body style={[styles.leftAlignSty, { borderBottomWidth: 0, marginBottom: -3 }]}>
                                <Text style={styles.cardTextJob4}>Kumar Pratik</Text>
                                <View style={styles.IndicatorSty}>
                                    <Text style={[styles.cardText2, {fontWeight: "500"}]}>New</Text>
                                </View>
                            </Body>

                        </ListItem>
                    </List> */}

                </ScrollView>
                {ApplicationStagestage === "*" || JobApplicants.ApplicantList.length <= 0 ?
                    null
                    :
                    <View style={styles.sendShortlst}>
                        <Button style={styles.btnShortList}
                            onPress={this.ToggleShortlistModal}
                        >
                            <Text style={styles.shortListTxt}>Send Shortlist</Text>
                        </Button>
                    </View>

                }


                <Modal style={styles.MContainer} isVisible={this.state.selectShortListModal}>
                    <Button onPress={this.ToggleShortlistModal} style={{ backgroundColor: "transparent", position: 'absolute', right: 10, top: 40, padding: 20, elevation: 0 }}>
                        <SvgUri
                            width="19px"
                            height="19px"
                            uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593711047/Mobile_Assets/closeWht_oohxti.svg" />
                    </Button>
                    <View style={[styles.Mwrapper, { height: 380 }]}>
                        <View style={{ paddingLeft: 29, paddingRight: 29, display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                            {/* <Label style={styles.cardTextJob2}>Experience</Label> */}
                            <Item regular style={styles.inputStyleWrap}>
                                <Input
                                    placeholder="Search"
                                    onChangeText={(query) => this.SearchAppUsers(query)}
                                    returnKeyType={'search'}
                                    keyboardType={'default'}
                                // onSubmitEditing={
                                //   this.proceednow
                                // }
                                />
                                <Icon type="FontAwesome" active name='search' style={{ color: "#C4C4C4" }} />
                            </Item>
                            <ScrollView style={{ width: "100%", height: "70%" }}>
                                <List style={{ borderBottomWidth: 1, borderBottomColor: '#E5E5E5', paddingBottom: 15, width: "100%" }}>
                                    {
                                        AppUsersList.Userlist.length > 0 ?
                                            SearchAppUserResult
                                            :

                                            <ListItem Button avatar style={{ display: 'flex', justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center', }}>

                                                <Body style={[styles.leftAlignSty, { borderBottomWidth: 0, marginBottom: -3 }]}>
                                                    <Text style={styles.cardTextJob4}>No Result Found</Text>
                                                </Body>

                                            </ListItem>

                                    }
                                    {/* <ListItem Button avatar style={{ display: 'flex', justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center', }}
                                        onPress={this.ToggleSendShortlistModal} 
                                    >
                                        <Left style={styles.leftAlignSty}>
                                            
                                            <Thumbnail style={styles.applicantImg} source={Profile} />
                                        </Left>
                                        <Body style={[styles.leftAlignSty, { borderBottomWidth: 0, marginBottom: -3 }]}>
                                            <Text style={styles.cardTextJob4}>Kumar Pratik</Text>
                                            
                                        </Body>

                                    </ListItem> */}
                                </List>
                            </ScrollView>
                            {
                                AppUsersList.Userlist.length > 0 ?
                                    null
                                    :
                                    <TouchableOpacity style={styles.btInviteNew}
                                        onPress={this.ToggleinviteUserModal}
                                    >
                                        <SvgUri
                                            style={{ marginRight: 20 }}
                                            width="19px"
                                            height="19px"
                                            uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593886348/Mobile_Assets/plusBig_kxa6tk.svg"
                                        />
                                        <Text style={[styles.cardTextJob4, { color: "#1E93E2" }]}>Invite New</Text>
                                    </TouchableOpacity>


                            }




                        </View>

                    </View>
                </Modal>

                <Modal style={styles.MContainer} isVisible={this.state.sendShortListModal}>
                    <Button onPress={this.ToggleSendShortlistModal} style={{ backgroundColor: "transparent", position: 'absolute', right: 10, top: 40, padding: 20, elevation: 0 }}>
                        <SvgUri
                            width="19px"
                            height="19px"
                            uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593711047/Mobile_Assets/closeWht_oohxti.svg" />
                    </Button>
                    <View style={[styles.Mwrapper, { height: 380 }]}>
                        <View style={{ paddingLeft: 18, paddingRight: 18, display: 'flex', justifyContent: 'center', alignItems: 'center', width: "100%" }}>
                            {/* <Label style={styles.cardTextJob2}>Experience</Label> */}
                            <View style={{ width: "100%", marginTop: 10 }}>
                                <Text style={styles.cardTextJob18}>
                                    Send Shortlist to {SelectedUser.first_name == null || SelectedUser.last_name == null ? SelectedUser.username : `${SelectedUser.first_name}  ${SelectedUser.last_name} `}?
                                </Text>
                                <View style={styles.tagTitle}>
                                    <Text style={styles.cardText2_2}>{this.state.ActiveView}</Text>
                                </View>
                            </View>
                            <ScrollView style={{ width: "100%", height: "50%" }}>
                                <List style={{ borderBottomWidth: 1, borderBottomColor: '#E5E5E5', paddingBottom: 15, width: "100%" }}>
                                    {/* <ListItem Button avatar style={{ display: 'flex', justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center', }}
                                     >
                                        <Left style={styles.leftAlignSty}>
                                            
                                            <Thumbnail style={styles.applicantImg} source={Profile} />
                                        </Left>
                                        <Body style={[styles.leftAlignSty, { borderBottomWidth: 0, marginBottom: -3 }]}>
                                            <Text style={styles.cardTextJob4}>Kumar Pratik</Text>
                                            
                                        </Body>

                                    </ListItem> */}
                                    {
                                        JobApplicants.ApplicantList.length > 0 ?
                                            JobOffersListing
                                            :
                                            ErrorComponentDisplay

                                    }
                                </List>
                            </ScrollView>
                            <View style={styles.ShortlistGroupBtn}>
                                <TouchableOpacity style={[styles.ShortlistBtn,]}
                                    onPress={this.ToggleconfirmShortlistModal}
                                >
                                    <Text style={[styles.cardTextJob4_1,]}>Send</Text>
                                </TouchableOpacity>

                                <TouchableOpacity style={[styles.ShortlistBtn, { backgroundColor: "#FF5964" }]}
                                    onPress={this.ToggleSendShortlistModal}
                                >
                                    <Text style={[styles.cardTextJob4_1,]}>Cancel</Text>
                                </TouchableOpacity>
                            </View>


                        </View>

                    </View>
                </Modal>

                <Modal style={styles.MContainer} isVisible={this.state.confirmShortListModal}>
                    <Button onPress={this.ToggleconfirmShortlistModal} style={{ backgroundColor: "transparent", position: 'absolute', right: 10, top: 140, padding: 20, elevation: 0 }}>
                        <SvgUri
                            width="19px"
                            height="19px"
                            uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593711047/Mobile_Assets/closeWht_oohxti.svg" />
                    </Button>
                    <View style={[styles.Mwrapper, { height: 140 }]}>
                        <View style={{ paddingLeft: 18, paddingRight: 18, display: 'flex', justifyContent: 'center', alignItems: 'center', width: "100%" }}>
                            {/* <Label style={styles.cardTextJob2}>Experience</Label> */}
                            <View style={{ width: "100%", marginTop: 10 }}>
                                <Text style={styles.cardTextJob18}>
                                Are you sure?
                                </Text>

                            </View>

                            <View style={styles.ShortlistGroupBtn}>
                                <TouchableOpacity style={[styles.ShortlistBtn,]}
                                    onPress={this.ToggleSuccessReplyModal}
                                >
                                    <Text style={[styles.cardTextJob4_1,]}>Send</Text>
                                </TouchableOpacity>

                                <TouchableOpacity style={[styles.ShortlistBtn, { backgroundColor: "#FF5964" }]}
                                    onPress={this.ToggleconfirmShortlistModal}
                                >
                                    <Text style={[styles.cardTextJob4_1,]}>Cancel</Text>
                                </TouchableOpacity>
                            </View>


                        </View>

                    </View>
                </Modal>

                <Modal style={styles.MContainer} isVisible={AppUsersList.showSuccessReplyModal}>
                    <Button onPress={this.CloseSentSuccessReplyModal} style={{ backgroundColor: "transparent", position: 'absolute', right: 10, top: 140, padding: 20, elevation: 0 }}>
                        <SvgUri
                            width="19px"
                            height="19px"
                            uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593711047/Mobile_Assets/closeWht_oohxti.svg" />
                    </Button>
                    <View style={[styles.Mwrapper, { height: 178 }]}>
                        <View style={{ paddingLeft: 18, paddingRight: 18, display: 'flex', justifyContent: 'center', alignItems: 'center', width: "100%" }}>
                            <SvgUri
                                style={{ marginBottom: 17 }}
                                width="79px"
                                height="79px"
                                uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593736755/Mobile_Assets/success_y53teq.svg"
                            />

                            <View style={{ width: "100%", marginTop: 10 }}>
                                <Text style={styles.cardTextJob18}>
                                    Shortlist sent to {SelectedUser.first_name == null || SelectedUser.last_name == null ? SelectedUser.username : `${SelectedUser.first_name}  ${SelectedUser.last_name} `}
                                </Text>

                            </View>


                        </View>

                    </View>
                </Modal>

                <Modal style={styles.MContainer} isVisible={this.state.inviteUserModal}>
                    <Button onPress={this.ToggleinviteUserModal} style={{ backgroundColor: "transparent", position: 'absolute', right: 10, top: 100, padding: 20, elevation: 0 }}>
                        <SvgUri
                            width="19px"
                            height="19px"
                            uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593711047/Mobile_Assets/closeWht_oohxti.svg" />
                    </Button>
                    <View style={[styles.Mwrapper, { height: 270 }]}>
                        <View style={{ paddingLeft: 18, paddingRight: 18, display: 'flex', justifyContent: 'center', alignItems: 'center', width: "100%" }}>
                            <View style={{ width: "100%", marginTop: 10 }}>
                                <Text style={styles.cardTextJob18}>
                                    Invite New User
                                </Text>
                            </View>
                            <Item regular style={styles.inputStyleWrap2}>
                                <Input style={styles.inputStyling}
                                    placeholder="Full Name"

                                    onChangeText={(invitedUserfullname) => {
                                        this.setState({
                                            invitedUserfullname
                                        })
                                    }} returnKeyType={'next'}
                                    keyboardType={'default'}
                                    onBlur={this.FormatValue}
                                />
                            </Item>
                            <Item regular style={styles.inputStyleWrap2}>
                                <Input style={styles.inputStyling}
                                    placeholder="Email"
                                    onChangeText={(invitedUserEmail) => {
                                        this.setState({
                                            invitedUserEmail
                                        })
                                    }}
                                    returnKeyType={'done'}
                                    keyboardType={'email-address'}
                                    onBlur={this.FormatValue}

                                />
                            </Item>

                            <View style={[styles.ShortlistGroupBtn, { justifyContent: "center" }]}>
                                <TouchableOpacity style={[styles.ShortlistBtn,]}
                                    onPress={this.ToggleconfirmShortlistModal}
                                >
                                    <Text style={[styles.cardTextJob4_1,]}>Send</Text>
                                </TouchableOpacity>

                            </View>


                        </View>

                    </View>
                </Modal>



            </Container>


        );
    }
}
export default AllApplication;

const styles = StyleSheet.create({
    container: {
        backgroundColor: "white",
        display: "flex",
        flex: 1,
        justifyContent: "flex-start",
    },

    rmPadding: {
        paddingTop: 0,
        paddingRight: 0,
        paddingBottom: 0,
        paddingLeft: 0,
    },
    rmPaddingLR: {
        paddingLeft: 0,
        paddingRight: 0,
    },

    cardTextJob1: {
        color: "#2F2F2F",
        fontSize: 36,
        fontWeight: '600',

    },
    cardTextJob2: {
        color: "#BCBCBC",
        fontSize: 14,
        fontWeight: '600',
        marginBottom: 5,
    },
    cardTextJob2_2: {
        color: "#fff",
        fontSize: 14,
        fontWeight: '600',
    },
    cardTextJob3: {
        color: "#2F2F2F",
        fontSize: 13,
        fontWeight: '500',
    },
    cardTextJob4: {
        fontSize: 12,
        color: '#2F2F2F',
        fontWeight: '600'
    },
    cardTextJob18: {
        fontSize: 18,
        color: '#000000',
        fontWeight: '600',
        width: "100%",
        textAlign: "center",
        marginBottom: 10,
    },

    cardTextJob5: {
        color: "#2F2F2F",
        fontSize: 10,
        fontWeight: '500',
    },
    cardTextJob7: {
        color: "#1E93E2",
        fontSize: 11,
        fontWeight: '500',
    },
    cardTextJob4_1: {
        color: "#FAFAFA",
        fontSize: 12,
        fontWeight: 'bold',
    },

    cardTextJob6: {
        color: "#1E93E2",
        fontSize: 11,
        fontWeight: '500',
    },
    cardText2: {
        color: "#FAFAFA",
        fontSize: 11,
    },
    cardText2_2: {
        color: "#FAFAFA",
        fontSize: 11,
        fontWeight: 'bold',
    },


    cardText1: {
        color: "#1E93E2",
        fontSize: 7,
    },
    bargeCount: {
        height: 20,
        width: 20,
        borderRadius: 20 / 2,
        backgroundColor: "#fff",
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    likeTabSty: {
        backgroundColor: "#1E93E2",
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        paddingTop: 30,
        paddingBottom: 10,
    },
    ShortlistGroupBtn: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingTop: 10,
        paddingBottom: 10,
        width: "100%"
    },
    ShortlistBtn: {
        backgroundColor: "#5ABC7A",
        display: 'flex',
        width: 100,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 12,
        paddingBottom: 12,
        elevation: 0,
        borderRadius: 5,
    },
    sendShortlst: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 12,
        paddingRight: 12,
    },
    btnShortList: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 17,
        paddingBottom: 17,
        paddingLeft: 20,
        paddingRight: 20,
        backgroundColor: "#5ABC7A",
        borderRadius: 10,
        elevation: 0,
    },
    tagTitle: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        paddingTop: 9,
        paddingBottom: 9,
        paddingLeft: 20,
        paddingRight: 20,
        backgroundColor: "#FC9F13",
        borderRadius: 3,
        elevation: 0,
    },
    shortListTxt: {
        fontSize: 13,
        color: '#fff',
        fontWeight: "bold"
    },
    inputStyleWrap: {
        height: 38,
        backgroundColor: '#E5E5E5',
        borderRadius: 5,
    },
    inputStyleWrap2: {
        height: 48,
        backgroundColor: '#E7E7E7',
        borderRadius: 5,
        marginBottom: 15,
    },
    inputStyling: {
        fontSize: 14,
        color: '#424051',
    },
    btInviteNew: {
        display: "flex",
        width: "100%",
        flexDirection: "row",
        justifyContent: "flex-start",
        alignItems: "center"
    },
    headerWrap: {
        marginBottom: 13,
        marginTop: 20,
        paddingLeft: 25,
        paddingRight: 25,
    },
    serachbtnSty: {
        height: 54,
        borderRadius: 4,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#5ABC7A',
        marginTop: 24
    },
    ViewBtnSty: {
        height: 23,
        width: 50,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#1E93E2',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    btnStyIdc: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    applicantImg: {
        height: 31,
        width: 32,
        borderRadius: 32 / 2,
    },
    numberCount: {
        fontWeight: '600',
        fontSize: 12,
        color: '#1E93E2',
        marginRight: 11
    },
    leftAlignSty: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
    },
    IndicatorSty: {
        // width: 7,
        // height: 7,
        borderRadius: 3,
        backgroundColor: "#FF5964",
        // marginTop: -15,
        marginRight: -5,
        paddingBottom: 3,
        paddingTop: 3,
        paddingLeft: 7,
        paddingRight: 7,
        marginLeft: 3,
    },
    activeView: {
        color: "#FAFAFA",
        fontSize: 11,
        fontWeight: 'bold',
    },

    // Modal style =============================================================
    MContainer: {
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
    },
    Mwrapper: {
        height: 510,
        width: '90%',
        backgroundColor: '#fff',
        // padding: 15,
        display: 'flex',
        justifyContent: 'space-around',
        borderRadius: 10
        // alignItems: 'center'
    },
    homeModalwrap: {
        // height: 519,
        width: '90%',
        backgroundColor: '#fff',
        // padding: 15,
        // display: 'flex',
        justifyContent: 'space-between',
        // alignItems: 'center'
    },
    homeModalHeader: {
        height: 89,
        width: '100%',
        backgroundColor: '#1E93E2',
        padding: 15,
        display: 'flex',
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    homeModalContentWrap: {
        width: '100%',
        backgroundColor: '#fff',
        padding: 35,
        paddingBottom: 20,
        display: 'flex',
    },
    homeModalwrapText1: {
        fontSize: 60,
        fontSize: 16,
        color: '#FAFAFA',
    },
    btnSave: {
        height: 54,
        backgroundColor: '#1E93E2',
        elevation: 0,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    // Modal Style ends=========================================================



});

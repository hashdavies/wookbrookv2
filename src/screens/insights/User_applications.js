import React, { Component } from 'react';
import {
  Text,
  Image,
  StyleSheet,
  ImageBackground,
  Keyboard,
  TouchableOpacity, FlatList
} from 'react-native';
import { KeyboardAvoidingView } from 'react-native';
import CustomizeHeader from '../frags/CustomizeHeader';
import { withNavigation } from 'react-navigation';

import Ionicons from 'react-native-vector-icons/Ionicons';
import {
  Container,
  Header,
  Content,
  Textarea,
  Button,
  View,
  Thumbnail,
  Form,
  Item,
  Input,
  Label,
  Badge,
  Icon,
  Footer,
  Picker,
  Left,
  Right,
  Body,
  Title,
  Center,
  CardItem,
  CheckBox
} from 'native-base';
import {
  Kaede,
  Hoshi,
  Jiro,
  Isao,
  Madoka,
  Akira,
  Hideo,
  Kohana,
  Makiko,
  Sae,
  Fumi,
} from 'react-native-textinput-effects';
import Modal from 'react-native-modal';
import { myspiner, AppInlineLoader } from '../../dependency/UtilityFunctions';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import User from "../../assets/workbrookAssets/user4.png";

import accountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import { ScrollView } from 'react-native-gesture-handler';
import imageUser from '../../assets/workbrookAssets/user2.png';
import DatePicker from 'react-native-datepicker'
// import Autocomplete from 'native-base-autocomplete'
import Autocomplete from 'react-native-autocomplete-input';
import { SvgUri } from 'react-native-svg';
import DashboardStore from '../../stores/Dashboard';
import EmptyListComponent from '../../StatelessScreens/EmptyListComponent';
import RadioGroup from 'react-native-radio-button-group';
const API = 'https://swapi.co/api';

var radiogroup_options = [
  {
    id: 0,
    label: 'The process was taking too long'
    // labelView: (
    //   <Text style={{flex: 1}}>
    //    The process was taking too long
    //   </Text>
    // )
  },
  { id: 1, label: 'I was not contacted by the employer' },
  { id: 2, label: 'I don’t like the job anymore' },
  { id: 3, label: 'Others' },
];
@observer
class Applications extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      isModalVisible: false,
      isModalOfferVisible: false,
      isModalWithdrawAppVisible: false,
      withdrawReason: "",

    }
    this.ReloadPageData = this.ReloadPageData.bind(this);
    this.toggleModal = this.toggleModal.bind(this);
    this.toggleModalWtDrawal = this.toggleModalWtDrawal.bind(this);
    this.GotoMyProfile = this.GotoMyProfile.bind(this);
    this.SubmitWithdrawal = this.SubmitWithdrawal.bind(this);

  }
  toggleModal = (item) => {

    DashboardStore._updateParameterV2(item, "singleApplication", "MyApplications");

    this.setState({ isModalVisible: !this.state.isModalVisible });
  };
  toggleModalOffer = () => {
    this.setState({ isModalOfferVisible: !this.state.isModalOfferVisible });
  };
  toggleModalWtDrawal = () => {
    this.setState({ isModalWithdrawAppVisible: !this.state.isModalWithdrawAppVisible });
  };
  toggleModalWithdrawApp = () => {
    // const { withdrawReason } = this.state;
    // const { singleApplication } = DashboardStore.MyApplications
    // let job_id = singleApplication.id
    this.setState({ isModalVisible: false });
    // this.setState(
    //   {
    //     isModalWithdrawAppVisible: !this.state.isModalWithdrawAppVisible
    //   }
    // );
    let thisstatus = DashboardStore.RedirectMode.withdrawApplication;
    // console.log(withdrawReason)
    // RedirectMode.withdrawApplication
    DashboardStore._updateParameterV2(!thisstatus, "withdrawApplication", "RedirectMode");
    // DashboardStore._updateParameterV2(job_id, "job_id", "WithdrawApplication");
    // DashboardStore.__WithDrawMyApplication();
  };
  SubmitWithdrawal = () => {
    const { withdrawReason } = this.state;
    const { singleApplication } = DashboardStore.MyApplications
    const { RedirectMode } = DashboardStore;
    let job_id = singleApplication.id
    // this.setState({ isModalVisible: false });
    // this.setState(
    //   {
    //     isModalWithdrawAppVisible: !this.state.isModalWithdrawAppVisible
    //   }
    // );
    // console.log(withdrawReason)
    DashboardStore._updateParameterV2(withdrawReason, "message", "WithdrawApplication");
    DashboardStore._updateParameterV2(job_id, "job_id", "WithdrawApplication");
    DashboardStore.__WithDrawMyApplication();
  };

  componentDidMount() {
    DashboardStore.__GetApplicationStatistics();
    DashboardStore.__GetMyApplicationList(1, 10);

  }
  ReloadPageData = () => {
    DashboardStore.__GetMyApplicationList(1, 10);

  };
  GotoMyProfile = () => {

    this.setState({
      isModalVisible: false,
      isModalWithdrawAppVisible: false
    });
    this.props.navigation.navigate("profile")

  };
  render() {

    const { isGoBack, RedirectMode } = DashboardStore;
    if (isGoBack === true) {
      this.props.navigation.goBack();
      DashboardStore._UpdateDefaultValue();
    }

    const { ApplicationsStatistics, MyApplications } = DashboardStore;
    let ErrorComponentDisplay = null;
    // console.log(DashboardStore.MyApplications);

    if (DashboardStore.isProcessing === true) {
      ErrorComponentDisplay = AppInlineLoader(DashboardStore.isProcessing)

    }
    else {
      ErrorComponentDisplay = <EmptyListComponent
        message={MyApplications.ErrorMessage}
        onclickhandler={this.ReloadPageData}
      />
    }
    let MyApplicationList =
      <FlatList
        // Data
        data={MyApplications.ApplicationsList}
        // Render Items
        renderItem={({ item }) => {
          //  let Applicants=item.applicants
         let applicationStage= item.applicationStage;
let ApplicationView=null;
if(applicationStage==0){
  ApplicationView= <View style={[styles.cardinnerSty,{backgroundColor:'#0070c0'} ]}>
                <Text style={styles.Text3}>Applied</Text>
              </View>
}
else if(applicationStage==1){
  ApplicationView= <View style={[styles.cardinnerSty,{backgroundColor:'#00b050'} ]}>
                <Text style={styles.Text3}>Under Consideration</Text>
              </View>
}
else if(applicationStage==2){
  ApplicationView= <View style={[styles.cardinnerSty,{backgroundColor:'#ffff00'} ]}>
                <Text style={styles.Text3}>Screening</Text>
              </View>
}
else if(applicationStage==3){
  ApplicationView=  <View style={[styles.cardinnerSty,{backgroundColor:'#ffff00'} ]}>
                <Text style={[styles.Text3,{color:'#000'}]}>Interviewing</Text>
              </View>
}
else if(applicationStage==4){
  ApplicationView=<View style={[styles.cardinnerSty,{backgroundColor:'#7030a0'} ]}>
                <Text style={styles.Text3}>Hired!</Text>
              </View>
}
else if(applicationStage==5){
  ApplicationView=<View style={[styles.cardinnerSty,{backgroundColor:'#ff0000'} ]}>
                <Text style={styles.Text3}>Unsuccessful!</Text>
              </View>
}
// else if(applicationStage==5){
//   <View style={[styles.cardinnerSty,{backgroundColor:'#ff0000'} ]}>
//                 <Text style={styles.Text3}>Unsuccessful!</Text>
//               </View>
// }
else{

}


          return (
            <View style={styles.successCard2}>
              {ApplicationView}
            
              <View style={styles.xLayer}>
                <Left style={{ borderBottomWidth: 0, marginLeft: 0, flex: 1 }}>
                  <Text style={[styles.Text4, { marginLeft: 0 }]}>{item.campany_name}</Text>
                  <Text style={[styles.Text4, { marginLeft: 0,fontWeight:'400' }]}>{item.role_title}</Text>
                  <Text style={styles.Text5} numberOfLines={1} >
                    {/* P&G, Victoria Island */}
                    {item.location}
                  </Text>
                  {item. skills_match > 70 ?
                  <Text style={styles.Text5_5} numberOfLines={1} >
{/* Your profile is a 70% match for this job opportunity */}
Your profile is a <Text style={{color:"#1E93E2", fontSize: 11, fontWeight: 'bold'}}> {item. skills_match} % </Text> match for this job opportunity
                </Text>
:
item.skills_match  > 49 ?
<Text style={styles.Text5_5} numberOfLines={1} >

Your profile is a good match for this opportunity 
</Text>
                :
null
                 
                 }
                </Left>
                <Right style={[styles.morestyIdc, { borderBottomWidth: 0, width: 20, flex: 0 }]}>
                  <TouchableOpacity style={styles.moreBtnSty}
                    onPress={() => this.toggleModal(item)}
                  >
                    <SvgUri
                      width='14.79px'
                      height='4px'
                      uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1589464409/Mobile_Assets/more_xcza8v.svg"
                    />
                  </TouchableOpacity>
                </Right>
              </View>

            </View>

          )
        }}
        // Item Key
        keyExtractor={(item, index) => String(index)}
        // Header (Title)
        // ListHeaderComponent={this.renderHeader}
        // Footer (Activity Indicator)
        // ListFooterComponent={this.renderFooter()}
        // On End Reached (Takes a function)
        onEndReached={this.retrieveMore}
      // How Close To The End Of List Until Next Data Request Is Made
      // onEndReachedThreshold={0}
      // Refreshing (Set To True When End Reached)
      // refreshing={this.state.refreshing}
      />

    return (
      <View style={{ flex: 1, backgroundColor: '#fff' }}>
        <ScrollView
          contentContainerStyle={{
            paddingLeft: 30,
            paddingRight: 30,
            paddingBottom: 30,
            paddingTop: 30
          }}
        >
          <View style={[styles.likeTabsty, { width: '100%' }]}>
            <View style={styles.viewPerTabSty}>
              <SvgUri
                // style={{ marginTop: 20, marginLeft: -17 }}
                width='26px'
                height='30px'
                uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1589394608/Mobile_Assets/profile_xlmerx.svg"
              />
              <Text style={[styles.Text2, { marginTop: 12, marginBottom: 12 }]}>{ApplicationsStatistics.profileVisit}</Text>
              <Text style={[styles.Text1,]}>Profile visits</Text>
            </View>
            <View style={styles.viewPerTabSty}>
              <SvgUri
                // style={{ marginTop: 20, marginLeft: -17 }}
                width='29px'
                height='26px'
                uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1589394608/Mobile_Assets/ref_ace292.svg"
              />
              <Text style={[styles.Text2, { marginTop: 12, marginBottom: 12 }]}>{ApplicationsStatistics.successfulReferral}</Text>
              <Text style={[styles.Text1,]}>Successful referrals</Text>
            </View>
            <View style={styles.viewPerTabSty}>
              <SvgUri
                // style={{ marginTop: 20, marginLeft: -17 }}
                width='23px'
                height='30px'
                uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1589394608/Mobile_Assets/cv_c3iv1f.svg"
              />
              <Text style={[styles.Text2, { marginTop: 12, marginBottom: 12 }]}>{ApplicationsStatistics.cvViews}</Text>
              <Text style={[styles.Text1,]}>CV views</Text>
            </View>
          </View>
          {/* <View style={styles.successCard}>
            <SvgUri
              // style={{ marginTop: 20, marginLeft: -17 }}
              width='45px'
              height='45px'
              uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1589394608/Mobile_Assets/thankyou_l8tqh1.svg"
            />
            <Text>Thank you</Text>
          </View> */}
          {
            MyApplications.ApplicationsList.length > 0 ?
              <React.Fragment>

                {MyApplicationList}

              </React.Fragment>
              :
              ErrorComponentDisplay

          }

          {/* <View style={styles.successCard2}>
            <View style={[styles.cardinnerSty, { backgroundColor: "#FAC01E" }]}>
              <Text style={styles.Text3}>Interview Stage</Text>
            </View>
            <View style={styles.xLayer}>
              <Left style={{ borderBottomWidth: 0, marginLeft: 0, flex: 1 }}>
                <Text style={[styles.Text4, { marginLeft: 0 }]}>Office Assistant</Text>
                <Text style={styles.Text5} numberOfLines={1} >
                  P&G, Victoria Island
                </Text>
                <Text style={styles.Text5_5} numberOfLines={1} >
                  You are within the top <Text style={{color:"#1E93E2", fontSize: 11, fontWeight: 'bold'}}> 30% </Text> of all 92 applicants
                </Text>
              </Left>
              <Right style={[styles.morestyIdc, { borderBottomWidth: 0, width: 20, flex: 0 }]}>
                <TouchableOpacity style={styles.moreBtnSty}
                  onPress={this.toggleModalOffer}
                >
                  <SvgUri
                    width='14.79px'
                    height='4px'
                    uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1589464409/Mobile_Assets/more_xcza8v.svg"
                  />
                </TouchableOpacity>
              </Right>
            </View>

          </View>

          <View style={styles.successCard2}>
            <View style={styles.cardinnerSty}>
              <Text style={styles.Text3}>You are being considered for the role</Text>
            </View>
            <View style={styles.xLayer}>
              <Left style={{ borderBottomWidth: 0, marginLeft: 0, flex: 1 }}>
                <Text style={[styles.Text4, { marginLeft: 0 }]}>General manager</Text>
                <Text style={styles.Text5} numberOfLines={1} >
                  P&G, Victoria Island
                </Text>
              </Left>
              <Right style={[styles.morestyIdc, { borderBottomWidth: 0, width: 20, flex: 0 }]}>
                <TouchableOpacity style={styles.moreBtnSty}
                  onPress={this.toggleModal}
                >
                  <SvgUri
                    width='14.79px'
                    height='4px'
                    uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1589464409/Mobile_Assets/more_xcza8v.svg"
                  />
                </TouchableOpacity>
              </Right>
            </View>

          </View>
         
         */}

        </ScrollView>
        {/* modal views */}
        <Modal style={styles.MContainer} isVisible={this.state.isModalVisible}>
          
          <View style={styles.Mwrapper}>
          <Button onPress={() => this.toggleModal()} style={{ backgroundColor: "transparent", position: 'absolute', right: 10, top: -30, padding: 20, elevation: 0 }}>
            <SvgUri
              width="19px"
              height="19px"
              uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593711047/Mobile_Assets/closeWht_oohxti.svg" />
          </Button>
            <View style={styles.firstView} >
              {/* <Button full style={styles.actionBtn}>
                <Text style={styles.actionBtnText}>
                  View the job poster’s profile
              </Text>
              </Button> */}
              <Button
                full
                style={[styles.actionBtn, { borderTopWidth: 1, borderTopColor: "#E5E5E5", borderBottomWidth: 1, borderBottomColor: "#E5E5E5" }]}
                onPress={this.GotoMyProfile}
              >
                <Text style={styles.actionBtnText}>
                  Edit your profile
              </Text>
              </Button>
              <Button full style={styles.actionBtn}
                onPress={this.toggleModalWithdrawApp}
              >
                <Text style={[styles.actionBtnText, { color: "#FF5964" }]}>
                  Withdraw application
              </Text>
              </Button>
            </View>
            <Button full style={[styles.btnCancel, { marginTop: 15 }]} title="Hide modal" onPress={() => this.toggleModal()}>
              <Text style={[styles.btnCancelText,]}>Cancel</Text>
            </Button>
          </View>
        </Modal>

        {/* edit offer*/}

        <Modal style={styles.MContainer} isVisible={this.state.isModalOfferVisible}>
        
          <View style={styles.Mwrapper}>
          <Button onPress={this.toggleModalOffer} style={{ backgroundColor: "transparent", position: 'absolute', right: 10, top: -30, padding: 20, elevation: 0 }}>
            <SvgUri
              width="19px"
              height="19px"
              uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593711047/Mobile_Assets/closeWht_oohxti.svg" />
          </Button>
            <View style={styles.firstView} >
              <Button full style={styles.actionBtn}>
                <Text style={styles.actionBtnText}>
                  Edit the offer
              </Text>
              </Button>
              <Button full style={styles.actionBtn}>
                <Text style={[styles.actionBtnText, { color: "#FF5964" }]}>
                  Close the offer
              </Text>
              </Button>
            </View>
            <Button full style={[styles.btnCancel, { marginTop: 15 }]} title="Hide modal" onPress={this.toggleModalOffer}>
              <Text style={[styles.btnCancelText,]}>Cancel</Text>
            </Button>
          </View>
        </Modal>

        {/* edit offer*/}

        {/* Withdraw App  modal*/}
        <Modal style={styles.MContainer} isVisible={RedirectMode.withdrawApplication}>
          <View style={styles.Mwrapper}>
          <Button onPress={this.toggleModalWithdrawApp} style={{ backgroundColor: "transparent", position: 'absolute', right: 10, top: -30, padding: 20, elevation: 0 }}>
            <SvgUri
              width="19px"
              height="19px"
              uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593711047/Mobile_Assets/closeWht_oohxti.svg" />
          </Button>
            <View style={styles.firstView} >
              <Button full style={styles.actionBtn2}>
                <Text style={[styles.actionBtnText, { color: "#FF5964" }]}>
                  Withdraw application
              </Text>
              </Button>
              <View style={styles.checkBoxWrap}>
                <Text style={[styles.Text4_2, { width: '100%' }]}>Why have you chosen to close this role?</Text>
                {/* <View style={styles.CheckBoxBody}>
                  <CheckBox checked={true} color="#000" style={{ borderRadius: 3, }} />
                  <Text style={styles.checkBoxText}>The process was taking too long</Text>
                </View> */}
                {/* <View style={styles.CheckBoxBody}>
                  <CheckBox checked={true} color="#E5E5E5" style={{ borderRadius: 3, height: 13, width: 13, }} />
                  <Text style={styles.checkBoxText}>I was not contacted by the employer</Text>
                </View> */}
                {/* <View style={styles.CheckBoxBody}>
                  <CheckBox checked={true} color="#E5E5E5" style={{ borderRadius: 3, height: 13, width: 13, }} />
                  <Text style={styles.checkBoxText}>I don’t like the job anymore</Text>
                </View> */}
                {/* <View style={styles.CheckBoxBody}>
                  <CheckBox checked={true} color="#E5E5E5" style={{ borderRadius: 3, height: 13, width: 13, }} />
                  <Text style={styles.checkBoxText}>Other</Text>
                </View> */}
                <RadioGroup
                  horizontal
                  options={radiogroup_options}
                  onChange={(option) => {
                    this.setState({
                      withdrawReason: option.label
                    })
                  }}
                  activeButtonId={0}
                />
              </View>
            </View>
            <Button
            full
              style={[styles.btnCancel, { marginTop: 15, color: 'green' }]} title="Hide modal"
              onPress={this.SubmitWithdrawal}>
              <Text style={[styles.btnCancelText,]}>{DashboardStore.isProcessing === true ? 'Processing...' : 'Submit'}</Text>
            </Button>
          </View>
        </Modal>
        {/* Withdraw App modal*/}

        {/* modal views */}
      </View>
    );
  }
}
export default withNavigation(Applications);

const inputStyles = {
  size: 100,
  color: '#FFFFFF',
};

const height = 40;

const styles = StyleSheet.create({
  Text1: {
    fontSize: 10,
    fontWeight: '500',
    color: "#2F2F2F",
    textAlign: 'center'
  },
  Text1_2: {
    fontSize: 10,
    fontWeight: 'bold',
    color: "#2F2F2F",
    textAlign: 'center'
  },
  Text3: {
    fontSize: 12,
    fontWeight: '500',
    color: "#FAFAFA",
    textAlign: 'center'
  },
  Text7: {
    fontSize: 12,
    fontWeight: '500',
    color: "#2F2F2F",
    textAlign: 'center'
  },
  Text4: {
    fontSize: 13,
    fontWeight: '500',
    color: "#2F2F2F",
  },
  Text4_2: {
    fontSize: 12,
    fontWeight: 'normal',
    color: "#676767",
  },
  Text5: {
    fontSize: 11,
    fontWeight: '300',
    color: "#2F2F2F",
  },
  Text5_5: {
    fontSize: 10,
    color: "#2F2F2F",
  },
  Text2: {
    fontSize: 18,
    fontWeight: '500',
    color: "#1E93E2",
  },
  likeTabsty: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  viewPerTabSty: {
    display: 'flex',
    alignItems: 'center',
    maxWidth: 79,
    textAlign: 'center',
  },
  successCard: {
    display: 'flex',
    alignItems: 'center',
    textAlign: 'center',
    justifyContent: "center",
    backgroundColor: '#F3F3F3',
    height: 100,
    // elevation: 1,
    marginTop: 30,
  },
  successCard2: {
    display: 'flex',
    // justifyContent: 'space-between',
    textAlign: 'center',
    backgroundColor: '#F3F3F3',
    height: 100,
    marginTop: 17,
  },
  cardinnerSty: {
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    textAlign: 'center',
    justifyContent: "center",
    backgroundColor: '#1E93E2',
    height: 24,
  },
  morestyIdc: {
    display: 'flex',
    justifyContent: 'flex-end',
    alignItems: 'center',
    width: 20
  },
  moreBtnSty: {
    height: 20,
    width: 20,
    borderRadius: 5,
    borderWidth: 0,
    borderColor: '#1E93E2',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  xLayer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: 14,
    paddingLeft: 19,
    paddingRight: 19,
    paddingTop: 14,
  },
  // ======================= modal style ==============================
  MContainer: {
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
  },
  Mwrapper: {
    // height: 360,
    width: '98%',
    // backgroundColor: '#fff',
    padding: 15,
    display: 'flex',
    justifyContent: 'space-around',
    // alignItems: 'center'
  },
  firstView: {
    backgroundColor: '#FAFAFA',
    borderRadius: 5
  },
  actionBtn: {
    backgroundColor: '#FAFAFA',
    borderRadius: 5,
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    height: 43,
  },
  actionBtn2: {
    backgroundColor: '#FAFAFA',
    borderRadius: 5,
    elevation: 0,
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    height: 43,
  },
  checkBoxWrap: {
    borderTopWidth: 1,
    borderTopColor: '#E5E5E5',
    paddingTop: 14,
    paddingLeft: 20,
    paddingRight: 20,
    paddingBottom: 14,
    width: '90%'
    // display: 'flex',
    // justifyContent: 'center',
    // flexDirection: 'row',
    // alignItems: 'center',
    // height: 43,
  },
  actionBtnText: {
    fontSize: 12,
    color: '#1E93E2',
  },
  btnCancel: {
    backgroundColor: '#FF5964',
    borderRadius: 5,
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    height: 43,
    elevation: 0,
  },
  btnCancelText: {
    fontSize: 12,
    color: '#FAFAFA',
    fontWeight: 'bold',
  },
  checkBoxText: {
    fontSize: 11,
    color: '#2F2F2F',
    marginLeft: 20
  },
  CheckBoxBody: {
    display: 'flex',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10,
  },
  // ======================= modal style ==============================

});

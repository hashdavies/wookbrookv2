import React, { Component, Fragment } from 'react';
import { Text, StyleSheet, ImageBackground, Keyboard, TouchableOpacity, Alert, FlatList, ActivityIndicator } from 'react-native';
import { KeyboardAvoidingView, Image, ScrollView } from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons';
import Checker from '../../assets/workbrookAssets/checker.png';
import {
    Container,
    Header,
    Content,
    Textarea,
    Button,
    View,
    Thumbnail,
    Form,
    Item,
    Input,
    Label,
    Badge,
    Icon,
    Footer,
    Picker,
    Item as FormItem,
    Left,
    Body,
    CardItem,
    Card,
    Right,
    ListItem,
    List,
    CheckBox,
} from 'native-base';
import {
    Kaede,
    Hoshi,
    Jiro,
    Isao,
    Madoka,
    Akira,
    Hideo,
    Kohana,
    Makiko,
    Sae,
    Fumi,
} from 'react-native-textinput-effects';
import CustomizeHeader from '../frags/CustomizeHeader';

import { myspiner, temp, ShowNotification, AppInlineLoader, cloudinaryUpload, _DocumentPicker, YOUR_CLOUDINARY_PRESET_JobApplicationFiles } from '../../dependency/UtilityFunctions';
import Logo from '../../assets/workbrookAssets/logo.png';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import RBSheet from "react-native-raw-bottom-sheet";
import Interests from '../interests/Interests';
import Noti from "../../assets/workbrookAssets/noti.png";
import UserBl from "../../assets/workbrookAssets/userBl.png";
import ArrForward from "../../assets/workbrookAssets/arrfr.png";
import Shield from "../../assets/workbrookAssets/shield.png";
import Quest from "../../assets/workbrookAssets/quest.png";
import Info from "../../assets/workbrookAssets/info.png";
import Plus from "../../assets/workbrookAssets/plus.png";
import Remark from "../../assets/workbrookAssets/remark.png";
import Like from "../../assets/workbrookAssets/like.png";
import MoreIcon from "../../assets/workbrookAssets/more2.png";
import Profile from "../../assets/images/profile/dp2.png";
import cardPix from "../../assets/images/background/bg3_old.jpg";
import RNPickerSelect from 'react-native-picker-select';
import TagInput from 'react-native-tags-input';

import RNFetchBlob from 'react-native-fetch-blob'
import ImagePicker from 'react-native-image-picker';
import CloseModal from "../../assets/workbrookAssets/whitecloss.png";
import SuccessIcon from "../../assets/workbrookAssets/successPost.png";
import Modal from 'react-native-modal';
import AccountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import { text } from 'react-native-communications';
import DashboardStore from '../../stores/Dashboard';
import { hasSetIntrest } from '../../auth';
import Autocomplete from 'react-native-autocomplete-input';
import MultiSelect from 'react-native-multiple-select';
import { YOUR_CLOUDINARY_PRESET, YOUR_CLOUDINARY_NAME, requestCameraPermission, YOUR_CLOUDINARY_PRESET_JobBanner } from '../../dependency/UtilityFunctions';
import EmptyListComponent from '../../StatelessScreens/EmptyListComponent';


items = [{
    id: '92iijs7yta',
    name: 'Ondo',
}, {
    id: 'a0s0a8ssbsd',
    name: 'Ogun',
}, {
    id: '16hbajsabsd',
    name: 'Calabar',
}, {
    id: 'sdhyaysdj',
    name: 'Kaduna',
}, {
    id: 'suudydjsjd',
    name: 'Abuja',
}];

@observer
class ViewApp extends Component {
    constructor(props) {
        super(props);
        this.mapRef = null;
        this.state = {
            email: '',
            password: '',
            filename: '',
            isuploading:false

        }
        this.UpdateApplicationLevel = this.UpdateApplicationLevel.bind(this);
        this.UploadAdditionalFile = this.UploadAdditionalFile.bind(this)

    }

    async componentDidMount() {
    }
    UpdateApplicationLevel = () => {
        DashboardStore._updateParameterV2("UpdateApplicantstage", "CurrentRedirect", "RedirectMode");
        DashboardStore.__UpdateApplicationLevel();
    };
    UploadAdditionalFile = async () => {
        // alert("kkkkk")
        this.setState({
            isuploading:true
        })
        let resp = await _DocumentPicker();
        console.log(resp);
        console.log("tuface");
        if (resp !== false || resp !== "cancel") {

            const source = {
                uri: resp.uri,
                type: resp.type,
                name: resp.name,
            }
            this.setState({
                filename: resp.name,
            })
            console.log("Bacl from picker")
            ShowNotification('Attaching your file', false);

            let FileUrl = await cloudinaryUpload(source,YOUR_CLOUDINARY_PRESET_JobApplicationFiles());
            if (FileUrl !== false) {

                console.log("FileUrl");
                console.log(FileUrl);
                console.log("FileUrl_done");
                DashboardStore._updateParameterV2(FileUrl, "additional_files", "ProcessJobApplication");
                //   ShowNotification('Done Uploading ....', true);
              //  DashboardStore.__UpdateProfile_external();
                ShowNotification('Done Attaching your File', true);
                this.setState({
                    isuploading:false
                })
            }


        }
        else{
            this.setState({
                isuploading:false
            })
        }
        


    };
    
    render() {
        const { isuploading } = this.state;
        const { JobApplicants } = DashboardStore;
        let applicantdata = JobApplicants.singleApplicant;
        let ApplicantName = applicantdata.first_name == null || applicantdata.last_name == null ? applicantdata.username : `${applicantdata.first_name}  ${applicantdata.last_name} `
        const { isGoBack ,RedirectMode} = DashboardStore;
        // if (isGoBack === true) {
        //     this.props.navigation.goBack();
        //     DashboardStore._UpdateDefaultValue();
        // }
        if (RedirectMode.UpdateApplicantstage === true) {
            let _CurrentRedirect=RedirectMode.CurrentRedirect;
                 DashboardStore._updateParameterV2(false, _CurrentRedirect, "RedirectMode");
        DashboardStore._updateParameterV2("David_Nill", "CurrentRedirect", "RedirectMode");
        this.props.navigation.goBack();

        }
        return (

            <Container style={styles.container}>
                <CustomizeHeader
                    leftside='WhiteArr'
                    title='View applicant'
                    rightside='empty'
                    statedColor='#1E93E2'
                    icon='md-close'
                // savehandler={this.proceednow}
                />

                <ScrollView
                    contentContainerStyle={{
                        padding: 16,
                    }}
                >
                    <Card style={{ marginBottom: 16, backgroundColor: '#FFF', elevation: 0, borderRadius: 5, borderColor: 'transparent' }}>
                        <CardItem style={{ backgroundColor: 'transparent' }}>
                            <Left>
                                <Thumbnail style={{ width: 46, height: 48, borderRadius: 48 / 2 }} source={{ uri: applicantdata.image_url }} />

                                <Body>
                                <TouchableOpacity onPress={() => { this.props.navigation.navigate("otherProfile") }}>
                                        <Text style={[styles.Text8, { color: '#2F2F2F', fontSize: 13 }]}>
                                            {ApplicantName}
                                        </Text>
                                        <Text style={[styles.Text8, {}]}>
                                            View Profile
                                        </Text>
                                    </TouchableOpacity>
                                </Body>
                            </Left>
                            {/* <Right>
                                <Button transparent style={styles.ViewBtnSty}>
                                    <Text style={styles.Text1}>Reject</Text>
                                </Button>
                            </Right> */}
                        </CardItem>
                        <View style={styles.cardBodySty}>
                            <Textarea
                                style={{ borderRadius: 5, fontSize: 11, fontWeight: '300' }}
                                rowSpan={3}
                                bordered
                                placeholder="Send custom message"
                                defaultValue={DashboardStore.ProcessJobApplication.defaultMessage}
                                onChangeText={(defaultMessage) => DashboardStore.onChangeText('defaultMessage', defaultMessage, 'ProcessJobApplication')}

                            />
                            <Button transparent style={{ height: 12, marginTop: 5 }}
                            onPress={this.UploadAdditionalFile}

                            >
                                <Text style={styles.cardTextJob5_1}>Upload additional files (max 2MB)</Text>
                            </Button>
                            <Text style={[styles.cardTextJob5_1,{color:'green'}]}>{this.state.filename}</Text>

                        </View>

                        <View style={styles.cardFooterSty}>
                            <Button style={[styles.applicantStateActive]}
                                onPress={this.UpdateApplicationLevel}
                                disabled={isuploading===true ?true :false}
                            >
                                <Text style={styles.stateactiveText}>{DashboardStore.isProcessing === true ? 'processing ...' : DashboardStore.ProcessJobApplication.buttonText}</Text>

                            </Button>
                        </View>
                    </Card>

                </ScrollView>



            </Container>


        );
    }
}
export default ViewApp;

const styles = StyleSheet.create({
    container: {
        backgroundColor: "#F8F7F7",
        display: "flex",
        flex: 1,
        justifyContent: "flex-start",
    },

    rmPadding: {
        paddingTop: 0,
        paddingRight: 0,
        paddingBottom: 0,
        paddingLeft: 0,
    },
    rmPaddingLR: {
        paddingLeft: 0,
        paddingRight: 0,
    },

    cardTextJob1: {
        color: "#2F2F2F",
        fontSize: 36,
        fontWeight: '600',

    },
    cardTextJob2: {
        color: "#BCBCBC",
        fontSize: 14,
        fontWeight: '600',
        marginBottom: 5,
    },
    cardTextJob2_2: {
        color: "#fff",
        fontSize: 14,
        fontWeight: '600',
    },
    cardTextJob3: {
        color: "#2F2F2F",
        fontSize: 13,
        fontWeight: '500',
    },
    cardTextJob4: {
        fontSize: 12,
        color: '#2F2F2F',
        fontWeight: '600'
    },

    cardTextJob5: {
        color: "#2F2F2F",
        fontSize: 10,
        fontWeight: '500',
    },
    cardTextJob5_1: {
        color: "#1E93E2",
        fontSize: 10,
        fontWeight: '600',
    },
    cardTextJob7: {
        color: "#1E93E2",
        fontSize: 11,
        fontWeight: '500',
    },
    cardTextJob4_1: {
        color: "#fff",
        fontSize: 12,
        fontWeight: '500',
    },

    cardTextJob6: {
        color: "#1E93E2",
        fontSize: 11,
        fontWeight: '500',
    },


    cardText1: {
        color: "#2F2F2F",
        fontWeight: '500',
        fontSize: 12,
    },
    Text1: {
        color: "#FAFAFA",
        fontWeight: '500',
        fontSize: 11,
    },
    Text2: {
        color: "#676767",
        fontWeight: '500',
        fontSize: 11,
    },
    cardBodySty: {
        marginTop: 30,
        marginBottom: 25,
        fontWeight: '500',
        fontSize: 11,
        paddingLeft: 20,
        paddingRight: 20,
    },
    stateactiveText: {
        fontSize: 11,
        fontWeight: 'bold',
        color: '#FAFAFA'
    },
    stateNotactiveText: {
        fontSize: 11,
        fontWeight: 'bold',
        color: '#B4B4B4'
    },
    applicantState: {
        width: "48%",
        height: 28,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#B4B4B4',
        fontSize: 11,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent',
        elevation: 0,
        marginBottom: 8,
    },
    applicantStateActive: {
        width: "100%",
        height: 28,
        borderRadius: 5,
        backgroundColor: '#1E93E2',
        fontSize: 11,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        elevation: 0,
        marginBottom: 8,

    },
    cardFooterSty: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        flexWrap: 'wrap',
        paddingLeft: 20,
        paddingRight: 20,
        marginBottom: 20,

    },
    inputStyleWrap: {
        height: 38,
        backgroundColor: '#E5E5E5',
        borderRadius: 5,
    },
    headerWrap: {
        marginBottom: 13,
        marginTop: 20,
        paddingLeft: 25,
        paddingRight: 25,
    },
    serachbtnSty: {
        height: 54,
        borderRadius: 4,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#5ABC7A',
        marginTop: 24
    },
    ViewBtnSty: {
        height: 23,
        width: 50,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#FF5964',
        backgroundColor: "#FF5964",
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    btnStyIdc: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    applicantImg: {
        height: 31,
        width: 32,
        borderRadius: 32 / 2,
    },
    numberCount: {
        fontWeight: '600',
        fontSize: 12,
        color: '#1E93E2',
        marginRight: 11
    },
    leftAlignSty: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
    },
    Text8: {
        fontSize: 12,
        fontWeight: '600',
        color: "#1E93E2",
    },


});

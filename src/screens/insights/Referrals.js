import React, { Component } from 'react';
import {
  Text,
  Image,
  StyleSheet,
  ImageBackground,
  Keyboard,
  TouchableOpacity, FlatList
} from 'react-native';
import { KeyboardAvoidingView } from 'react-native';
import CustomizeHeader from '../frags/CustomizeHeader';
import ProgressCircle from 'react-native-progress-circle'
import Ionicons from 'react-native-vector-icons/Ionicons';
import { withNavigation } from 'react-navigation';

import {
  Container,
  Header,
  Content,
  Textarea,
  Button,
  View,
  Thumbnail,
  Form,
  Item,
  Input,
  Label,
  Badge,
  Icon,
  Footer,
  Picker,
  Left,
  Right,
  Body,
  Title,
  Center,
  CardItem,
  CheckBox
} from 'native-base';
import {
  Kaede,
  Hoshi,
  Jiro,
  Isao,
  Madoka,
  Akira,
  Hideo,
  Kohana,
  Makiko,
  Sae,
  Fumi,
} from 'react-native-textinput-effects';
import Modal from 'react-native-modal';
import { myspiner, AppInlineLoader } from '../../dependency/UtilityFunctions';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import User from "../../assets/workbrookAssets/user4.png";
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import accountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import { ScrollView } from 'react-native-gesture-handler';
import imageUser from '../../assets/workbrookAssets/user2.png';
import DatePicker from 'react-native-datepicker'
// import Autocomplete from 'native-base-autocomplete'
import Autocomplete from 'react-native-autocomplete-input';
import { SvgUri } from 'react-native-svg';
import DashboardStore from '../../stores/Dashboard';
import EmptyListComponent from '../../StatelessScreens/EmptyListComponent';
const API = 'https://swapi.co/api';
@observer
class Referrals extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      isModalVisible: false,
      isModalWithdrawAppVisible: false,
    }
    this.ReloadPageData = this.ReloadPageData.bind(this);
    this.GotoUserProfile = this.GotoUserProfile.bind(this);

  }
  toggleModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible });
  };
  toggleModalWithdrawApp = () => {
    this.setState({ isModalVisible: false });
    this.setState({ isModalWithdrawAppVisible: !this.state.isModalWithdrawAppVisible });
  };

  componentDidMount() {
    DashboardStore.__GetReferalStatistics();
    DashboardStore.__GetMyrefarals(1, 10);
  }
  ReloadPageData = () => {
    DashboardStore.__GetMyrefarals(1, 10);

  };
  GotoUserProfile = (item) => {
    let thisUserId = item.user_id;
    DashboardStore._updateParameterV2(thisUserId, "User_id", "OtherProfile");
    this.props.navigation.navigate("otherProfile")
  };
  render() {
    const { ReferalStatistics, MyRefarals } = DashboardStore;

    let ErrorComponentDisplay = null;
    console.log(DashboardStore.MyRefarals);

    if (DashboardStore.isProcessing === true) {
      ErrorComponentDisplay = AppInlineLoader(DashboardStore.isProcessing)

    }
    else {
      ErrorComponentDisplay = <EmptyListComponent
        message={MyRefarals.ErrorMessage}
        onclickhandler={this.ReloadPageData}
      />
    }
    let MyRefaralsList =
      <FlatList
        // Data
        data={MyRefarals.referalsList}
        // Render Items
        renderItem={({ item }) => {
          //  let Applicants=item.applicants
          let ApplicantName = item.first_name == null || item.last_name == null ? item.username : `${item.first_name}  ${item.last_name} `

          return (
            <CardItem>
              <Left>
                <Thumbnail style={{ width: 29, height: 30, borderRadius: 30 / 2 }} source={{ uri: 'https://res.cloudinary.com/workbrook-hash/image/upload/v1589587873/Mobile_Assets/ddperson4_xg97eh.png' }} />

                <Body>
                  <TouchableOpacity
                    onPress={() => this.GotoUserProfile(item)}
                  >
                    <Text style={[styles.Text8, { color: '#2F2F2F' }]}>{ApplicantName}</Text>
                    <Text style={[styles.Text8, { fontSize: 10 }]}>View Profile</Text>
                  </TouchableOpacity>
                </Body>
              </Left>
              <Right>
                <Text style={styles.Text8}>{item.points} pts</Text>
              </Right>
            </CardItem>
          )
        }}
        // Item Key
        keyExtractor={(item, index) => String(index)}
        // Header (Title)
        // ListHeaderComponent={this.renderHeader}
        // Footer (Activity Indicator)
        // ListFooterComponent={this.renderFooter()}
        // On End Reached (Takes a function)
        onEndReached={this.retrieveMore}
      // How Close To The End Of List Until Next Data Request Is Made
      // onEndReachedThreshold={0}
      // Refreshing (Set To True When End Reached)
      // refreshing={this.state.refreshing}
      />






    return (
      <View style={{ flex: 1, backgroundColor: '#fff' }}>
        <ScrollView
          contentContainerStyle={{
            paddingLeft: 30,
            paddingRight: 30,
            paddingBottom: 30,
            paddingTop: 30
          }}
        >
          <View style={[styles.likeTabsty, { width: '100%' }]}>

            <View style={styles.viewPerTabSty}>
              <SvgUri
                // style={{ marginTop: 20, marginLeft: -17 }}
                width='30px'
                height='30px'
                uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593446132/Mobile_Assets/thumbsUp_z9acfx.svg"
              />
              <Text style={[styles.Text2, { marginTop: 12, marginBottom: 12 }]}>{ReferalStatistics.clicksByReferral}</Text>
              <Text style={[styles.Text1,]}>Interested Referees</Text>
            </View>

            <View style={styles.viewPerTabSty}>
              <SvgUri

                width='26px'
                height='30px'
                uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593446132/Mobile_Assets/realUser_ghww2n.svg"
              />
              <Text style={[styles.Text2, { marginTop: 12, marginBottom: 12 }]}>{ReferalStatistics.appliedForJob}</Text>
              <Text style={[styles.Text1,]}>Referees Applied</Text>
            </View>

            <View style={styles.viewPerTabSty}>
              <SvgUri
                // style={{ marginTop: 20, marginLeft: -17 }}
                width='30px'
                height='30px'
                uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1590846662/Mobile_Assets/checkBlue_ajznt3.svg"
              />
              <Text style={[styles.Text2, { marginTop: 12, marginBottom: 12 }]}>{ReferalStatistics.gotEmployed}</Text>
              <Text style={[styles.Text1,]}>Successful Referrals</Text>
            </View>
          </View>
          <View style={styles.ReferralsList}>

            <Text style={styles.Text4}>My Referrals</Text>

            {
              MyRefarals.referalsList.length > 0 ?
                <React.Fragment>

                  {MyRefaralsList}

                </React.Fragment>
                :
                ErrorComponentDisplay

            }
            {/* <CardItem>
              <Left>
                <Thumbnail style={{ width: 29, height: 30, borderRadius: 30 / 2 }} source={{ uri: 'https://res.cloudinary.com/workbrook-hash/image/upload/v1589587873/Mobile_Assets/ddperson4_xg97eh.png' }} />

                <Body>
                  <TouchableOpacity>
                    <Text style={[styles.Text8, { color: '#2F2F2F' }]}>Jane Dokaszhuk</Text>
                    <Text style={[styles.Text8, { fontSize: 10 }]}>View Profile</Text>
                  </TouchableOpacity>
                </Body>
              </Left>
              <Right>
                <TouchableOpacity style={styles.ViewSty} onPress={this.toggleModal}>
                  <Text style={styles.Text8}>
                    View
                  </Text>
                </TouchableOpacity>

              </Right>
            </CardItem>
            <CardItem>
              <Left>
                <Thumbnail style={{ width: 29, height: 30, borderRadius: 30 / 2 }} source={{ uri: 'https://res.cloudinary.com/workbrook-hash/image/upload/v1589587873/Mobile_Assets/ddperson4_xg97eh.png' }} />

                <Body>
                  <TouchableOpacity>
                    <Text style={[styles.Text8, { color: '#2F2F2F' }]}>Jane Dokaszhuk</Text>
                    <Text style={[styles.Text8, { fontSize: 10 }]}>View Profile</Text>
                  </TouchableOpacity>
                </Body>
              </Left>
              <Right>
                <TouchableOpacity style={styles.ViewSty} onPress={this.toggleModal}>
                  <Text style={styles.Text8}>View</Text>
                </TouchableOpacity>
              </Right>
            </CardItem> */}


          </View>

        </ScrollView>
        <Modal style={styles.MContainer} isVisible={this.state.isModalVisible}>
          <View style={styles.Mwrapper}>
            <View style={styles.firstView} >
              <View style={styles.contentWrap}>
                <AnimatedCircularProgress
                  size={37}
                  width={2}
                  fill={100}
                  tintColor="#5ABC7A"
                  backgroundColor="#F5F5F5">
                  {
                    (fill) => (
                      <Image source={{ uri: "https://res.cloudinary.com/workbrook-hash/image/upload/v1587164401/sample.jpg" }} style={{ width: 29, height: 30, borderRadius: 30 / 2,}} />
                    )
                  }
                </AnimatedCircularProgress>
                <Text style={[styles.Text8, { color: '#2F2F2F', marginLeft: 9 }]}>Jane Dokaszhuk</Text>
              </View>

              <CardItem style={[{ borderBottomWidth: 1, borderBottomColor: "#E5E5E5", paddingLeft: 0, paddingRight: 0, backgroundColor:"transparent" }]}>
                <Left style={{ flex: 2 }}>
                  <Body>
                    <TouchableOpacity>
                      <Text style={[styles.Text8, { color: '#2F2F2F' }]}>Jane Dokaszhuk</Text>
                      <Text style={[styles.Text8, { fontSize: 10, color: '#2F2F2F' }]}>P&G, Victoria Island</Text>
                    </TouchableOpacity>
                  </Body>
                </Left>
                <Right style={styles.flexDisplay}>
                  <SvgUri
                    style={{ marginRight: 10 }}
                    width='16px'
                    height='16px'
                    uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593446132/Mobile_Assets/thumbsUp_z9acfx.svg"
                  />


                </Right>
              </CardItem>

              <CardItem style={[{ borderBottomWidth: 1, borderBottomColor: "#E5E5E5", paddingLeft: 0, paddingRight: 0, backgroundColor:"transparent" }]}>
                <Left style={{ flex: 2 }}>
                  <Body>
                    <TouchableOpacity>
                      <Text style={[styles.Text8, { color: '#2F2F2F' }]}>Jane Dokaszhuk</Text>
                      <Text style={[styles.Text8, { fontSize: 10, color: '#2F2F2F' }]}>P&G, Victoria Island</Text>
                    </TouchableOpacity>
                  </Body>
                </Left>
                <Right style={styles.flexDisplay}>
                  <SvgUri
                    style={{ marginRight: 10 }}
                    width='16px'
                    height='16px'
                    uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593446132/Mobile_Assets/thumbsUp_z9acfx.svg"
                  />
                  <SvgUri
                    style={{ marginRight: 10 }}
                    width='13px'
                    height='16px'
                    uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593446132/Mobile_Assets/realUser_ghww2n.svg"
                  />

                </Right>
              </CardItem>

              <CardItem style={[{ borderBottomWidth: 1, borderBottomColor: "#E5E5E5", paddingLeft: 0, paddingRight: 0, backgroundColor:"transparent", }]}>
                <Left style={{ flex: 2 }}>
                  <Body>
                    <TouchableOpacity>
                      <Text style={[styles.Text8, { color: '#2F2F2F' }]}>Jane Dokaszhuk</Text>
                      <Text style={[styles.Text8, { fontSize: 10, color: '#2F2F2F' }]}>P&G, Victoria Island</Text>
                    </TouchableOpacity>
                  </Body>
                </Left>
                <Right style={styles.flexDisplay}>
                  <SvgUri
                    style={{ marginRight: 10 }}
                    width='16px'
                    height='16px'
                    uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593446132/Mobile_Assets/thumbsUp_z9acfx.svg"
                  />
                  <SvgUri
                    style={{ marginRight: 10 }}
                    width='13px'
                    height='16px'
                    uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593446132/Mobile_Assets/realUser_ghww2n.svg"
                  />
                  <SvgUri
                    style={{ marginRight: 10 }}
                    width='16px'
                    height='16px'
                    uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1590846662/Mobile_Assets/checkBlue_ajznt3.svg"
                  />

                </Right>
              </CardItem>

            </View>
            <Button onPress={this.toggleModal} style={[styles.btnCancel, { marginTop: 15 }]} title="Hide modal" onPress={this.toggleModal}>
              <Text style={[styles.btnCancelText,]}>Close</Text>
            </Button>
          </View>
        </Modal>


      </View>
    );
  }
}
// export default Referrals;
export default withNavigation(Referrals);
const inputStyles = {
  size: 100,
  color: '#FFFFFF',
};

const height = 40;

const styles = StyleSheet.create({
  Text1: {
    fontSize: 10,
    fontWeight: '500',
    color: "#2F2F2F",
    textAlign: 'center'
  },
  Text1_2: {
    fontSize: 10,
    fontWeight: 'bold',
    color: "#2F2F2F",
    textAlign: 'center'
  },
  Text3: {
    fontSize: 12,
    fontWeight: '500',
    color: "#FAFAFA",
    textAlign: 'center'
  },
  Text7: {
    fontSize: 12,
    fontWeight: '500',
    color: "#2F2F2F",
    textAlign: 'center'
  },
  Text4: {
    fontSize: 13,
    fontWeight: '500',
    color: "#2F2F2F",
  },
  Text4_2: {
    fontSize: 12,
    fontWeight: 'normal',
    color: "#676767",
  },
  Text5: {
    fontSize: 11,
    fontWeight: '300',
    color: "#2F2F2F",
  },
  Text8: {
    fontSize: 12,
    fontWeight: '600',
    color: "#1E93E2",
  },
  Text2: {
    fontSize: 18,
    fontWeight: '500',
    color: "#1E93E2",
  },
  likeTabsty: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  viewPerTabSty: {
    display: 'flex',
    alignItems: 'center',
    maxWidth: 79,
    textAlign: 'center',
  },
  successCard: {
    display: 'flex',
    alignItems: 'center',
    textAlign: 'center',
    justifyContent: "center",
    backgroundColor: '#F3F3F3',
    height: 100,
    // elevation: 1,
    marginTop: 30,
  },
  ReferralsList: {
    borderTopWidth: 1,
    borderTopColor: '#E5E5E5',
    // alignItems: 'center',
    // textAlign: 'center',
    // justifyContent: "center",
    // display: "flex",
    marginTop: 30,
    paddingTop: 30,
  },
  successCard2: {
    display: 'flex',
    // justifyContent: 'space-between',
    textAlign: 'center',
    backgroundColor: '#F3F3F3',
    height: 100,
    marginTop: 17,
  },
  cardinnerSty: {
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    textAlign: 'center',
    justifyContent: "center",
    backgroundColor: '#1E93E2',
    height: 24,
  },
  morestyIdc: {
    display: 'flex',
    justifyContent: 'flex-end',
    alignItems: 'center',
    width: 20
  },
  ViewSty: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 6,
    paddingTop: 6,
    paddingLeft: 12,
    paddingRight: 12,
    borderWidth: 1,
    borderColor: "#1E93E2",
  },
  moreBtnSty: {
    height: 20,
    width: 20,
    borderRadius: 5,
    borderWidth: 0,
    borderColor: '#1E93E2',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  xLayer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: 14,
    paddingLeft: 19,
    paddingRight: 19,
    paddingTop: 14,
  },
  // ======================= modal style ==============================
  MContainer: {
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
  },
  Mwrapper: {
    // height: 360,
    width: '98%',
    // backgroundColor: '#fff',
    padding: 15,
    display: 'flex',
    justifyContent: 'space-around',
    // alignItems: 'center'
  },
  firstView: {
    backgroundColor: '#FAFAFA',
    borderRadius: 5,
    paddingBottom: 25,
    paddingRight: 20,
    paddingLeft: 20,
  },
  actionBtn: {
    backgroundColor: '#FAFAFA',
    borderRadius: 5,
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    height: 43,
  },
  actionBtn2: {
    backgroundColor: '#FAFAFA',
    borderRadius: 5,
    elevation: 0,
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    height: 43,
  },
  checkBoxWrap: {
    borderTopWidth: 1,
    borderTopColor: '#E5E5E5',
    paddingTop: 14,
    paddingLeft: 20,
    paddingRight: 20,
    paddingBottom: 14,
    // display: 'flex',
    // justifyContent: 'center',
    // flexDirection: 'row',
    // alignItems: 'center',
    // height: 43,
  },
  actionBtnText: {
    fontSize: 12,
    color: '#1E93E2',
  },
  btnCancel: {
    backgroundColor: '#FF5964',
    borderRadius: 5,
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    height: 43,
    elevation: 0,
  },
  btnCancelText: {
    fontSize: 12,
    color: '#FAFAFA',
    fontWeight: 'bold',
  },
  checkBoxText: {
    fontSize: 11,
    color: '#2F2F2F',
    marginLeft: 20
  },
  CheckBoxBody: {
    display: 'flex',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10,
  },
  flexDisplay: {
    display: 'flex',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    alignItems: 'center',
  },
  contentWrap: {
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 25,
    marginTop: 23,
  },
  // ======================= modal style ==============================

});

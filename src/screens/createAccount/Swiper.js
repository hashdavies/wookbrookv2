/**
 * Swiper
 * Renders a swipable set of screens passed as children,
 * pagination indicators and a button to swipe through screens
 * or to get out of the flow when the last screen is reached
 */

import React, { Component } from 'react';
import {
  Dimensions, // Detects screen dimensions
  Platform, // Detects platform running the app
  ScrollView, // Handles navigation between screens
  StyleSheet, // CSS-like styles
  View,
  TouchableOpacity, // Container component
} from 'react-native';
// import Button from './Button';
// import { Button } from 'react-native-elements';
import {
  Container,
  Header,
  Content,
  Button,
  Text,
  Textarea,
  Thumbnail,
  Form,
  Item,
  Input,
  Label,
  Badge,
  Icon,
  Footer,
  Picker,
  Item as FormItem,
} from 'native-base';
import { withNavigation } from 'react-navigation';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
// Detect screen width and height

const { width, height } = Dimensions.get('window');
import accountStore from '../../stores/Account';
import { observer } from 'mobx-react/native';

@observer
  class Swiper extends Component {
  // Props for ScrollView component
  static defaultProps = {
    // Arrange screens horizontally
    horizontal: true,
    // Scroll exactly to the next screen, instead of continous scrolling
    pagingEnabled: true,
    // Hide all scroll indicators
    showsHorizontalScrollIndicator: false,
    showsVerticalScrollIndicator: false,
    // Do not bounce when the end is reached
    bounces: false,
    // Do not scroll to top when the status bar is tapped
    scrollsToTop: false,
    // Remove offscreen child views
    removeClippedSubviews: true,
    // Do not adjust content behind nav-, tab- or toolbars automatically
    automaticallyAdjustContentInsets: false,
    // Fisrt is screen is active
    index: 0,
  };

  state = this.initState(this.props);

  /**
   * Initialize the state
   */
  initState(props) {
    // Get the total number of slides passed as children
    const total = props.children ? props.children.length || 1 : 0,
      // Current index
      index = total > 1 ? Math.min(props.index, total - 1) : 0,
      // Current offset
      offset = width * index;

    const state = {
      total,
      index,
      offset,
      width,
      height,
    };

    // Component internals as a class property,
    // and not state to avoid component re-renders when updated
    this.internals = {
      isScrolling: false,
      offset,
    };

    return state;
  }

  /**
   * Scroll begin handler
   * @param {object} e native event
   */
  onScrollBegin = e => {
    console.warn(this.state.index);
    console.warn("this.state.begin");
    // Update internal isScrolling state
    this.internals.isScrolling = true;
  };

  /**
   * Scroll end handler
   * @param {object} e native event
   */
  onScrollEnd = e => {
    // Update internal isScrolling state
    this.internals.isScrolling = false;

    // Update index
    this.updateIndex(
      e.nativeEvent.contentOffset
        ? e.nativeEvent.contentOffset.x
        : // When scrolled with .scrollTo() on Android there is no contentOffset
          e.nativeEvent.position * this.state.width,
    );
  };

  /*
     * Drag end handler
     * @param {object} e native event
     */
  onScrollEndDrag = e => {
    const {
        contentOffset: { x: newOffset },
      } = e.nativeEvent,
      { children } = this.props,
      { index } = this.state,
      { offset } = this.internals;
      console.warn(this.state.index);
      console.warn("this.state.begin");
    // Update internal isScrolling state
    // if swiped right on the last slide
    // or left on the first one
    if (offset === newOffset && (index === 0 || index === children.length - 1)) {
      this.internals.isScrolling = false;
    }
  };

  /**
   * Update index after scroll
   * @param {object} offset content offset
   */
  updateIndex = offset => {
    const state = this.state,
      diff = offset - this.internals.offset,
      step = state.width;
    let index = state.index;

    // Do nothing if offset didn't change
    if (!diff) {
      return;
    }

    // Make sure index is always an integer
    index = parseInt(index + Math.round(diff / step), 10);

    // Update internal offset
    this.internals.offset = offset;
    // Update index in the state
    this.setState({
      index,
    });
  };

  /**
   * Swipe one slide forward
   */
  swipe = () => {
    // alert(this.state.total)
    // alert(this.state.index +1)
    // Ignore if already scrolling or if there is less than 2 slides
   

    if (
      this.internals.isScrolling ||
      this.state.total < 2 ||
      this.state.index + 1 >= this.state.total
    ) {
      return;
    }

    const state = this.state,
      diff = this.state.index + 1,
      x = diff * state.width,
      y = 0;
console.warn(this.state.index);
console.warn("this.state.index");
    // Call scrollTo on scrollView component to perform the swipe
    this.scrollView && this.scrollView.scrollTo({ x, y, animated: true });

    // Update internal scroll state
    this.internals.isScrolling = true;

    // Trigger onScrollEnd manually on android
    if (Platform.OS === 'android') {
      setImmediate(() => {
        this.onScrollEnd({
          nativeEvent: {
            position: diff,
          },
        });
      });
    }
    // alert(diff);
  };
  VerifyContent = (index) => {
    const {Reg_Role,Reg_firstname,Reg_lastname,Reg_gendar,Reg_email,Reg_password,Reg_username,Reg_city,
      Reg_state,Reg_country,Reg_jad_division,Reg_about_self
  }=accountStore.regInfo;
if(index===0){
 
if (Reg_Role===''){
  Toast.show('Select Your Role', Toast.SHORT, Toast.BOTTOM,ToastStyle);
}
else{
  this.swipe()
}

}
 if(index===1){
  if (Reg_firstname===''){
    Toast.show('Enter First Name', Toast.SHORT, Toast.BOTTOM,ToastStyle);
  }
  else if(Reg_lastname===''){
    Toast.show('Enter Last Name', Toast.SHORT, Toast.BOTTOM,ToastStyle);
  }
  else if(Reg_email===''){
    Toast.show('Enter Email Address', Toast.SHORT, Toast.BOTTOM,ToastStyle);
  }
  else if(Reg_password===''){
    Toast.show('Choose a Password', Toast.SHORT, Toast.BOTTOM,ToastStyle);
  }
  else if(Reg_username===''){
    Toast.show('Choose a Username', Toast.SHORT, Toast.BOTTOM,ToastStyle);
  }
  else{
    this.swipe()
  }
}
 if(index===2){
  this.swipe()
}
 
// else{
// console.warn("Not Neccessary");
// }
  }

  swipeLeft = () => {
    // alert("ji")
    // Ignore if already scrolling or if there is less than 2 slides
    if (this.internals.isScrolling || this.state.total < 2 ) {
      return;
    }
    if (this.state.index - 1 < 0) {
      this.props.navigation.navigate("Landingpage");
    }

    const state = this.state,
      diff = this.state.index - 1,
      x = diff * state.width,
      y = 0;

    // Call scrollTo on scrollView component to perform the swipe
    this.scrollView && this.scrollView.scrollTo({ x, y, animated: true });

    // Update internal scroll state
    this.internals.isScrolling = true;

    // Trigger onScrollEnd manually on android
    if (Platform.OS === 'android') {
      setImmediate(() => {
        this.onScrollEnd({
          nativeEvent: {
            position: diff,
          },
        });
      });
    }
    // alert(diff);
  };

  /**
   * Render ScrollView component
   * @param {array} slides to swipe through
   */
  renderScrollView = pages => {
    return (
      <ScrollView
        ref={component => {
          this.scrollView = component;
        }}
        {...this.props}
        contentContainerStyle={[styles.wrapper, this.props.style]}
        onScrollBeginDrag={this.onScrollBegin}
        onMomentumScrollEnd={this.onScrollEnd}
        onScrollEndDrag={this.onScrollEndDrag}
      >
        {pages.map((page, i) => (
          // Render each slide inside a View
          <View style={[styles.fullScreen, styles.slide]} key={i}>
            {page}
          </View>
        ))}
      </ScrollView>
    );
  };

  /**
   * Render pagination indicators
   */
  renderPagination = () => {
    if (this.state.total <= 1) {
      return null;
    }

    const ActiveDot = <View style={[styles.dot, styles.activeDot]} />,
      Dot = <View style={styles.dot} />;

    let dots = [];

    for (let key = 0; key < this.state.total; key++) {
      dots.push(
        key === this.state.index
          ? // Active dot
            React.cloneElement(ActiveDot, { key })
          : // Other dots
            React.cloneElement(Dot, { key }),
      );
    }

    return (
      <View
        pointerEvents="none"
        // style={[styles.pagination, styles.fullScreen]}
        style={[styles.pagination]}
      >
        {dots}
      </View>
    );
  };

  /**
   * Render Continue or Done button
   */
  renderButton = () => {
    const lastScreen = this.state.index === this.state.total - 1;
    return (
      <Footer style={styles.footerWrap}>
        <View style={{ flex: 1, flexDirection: 'row', paddingTop: 20 }}>
          {/* <View pointerEvents="box-none" style={[styles.buttonWrapper, styles.fullScreen]}>
       
        </View> */}

          <TouchableOpacity style={styles.submitButton} onPress={() => this.swipeLeft()}>
            <View
              style={{
                flex: 1,
                alignItems: 'flex-start',
                backgroundColor: 'transparent',
                marginLeft: 20,
              }}
            >
              <Text style={{ color: '#000', flex: 1 }}>Back</Text>
            </View>
          </TouchableOpacity>
          <View style={{ flex: 1, backgroundColor: 'transparent' }}>{this.renderPagination()}</View>
          {lastScreen ? (
            // Show this button on the last screen
            // TODO: Add a handler that would send a user to your app after onboarding is complete
            <TouchableOpacity
              style={styles.submitButton}
            //  onPress={() => console.warn('Submit Registration Now')}
             onPress={ async () => {
             await  accountStore._RegisterUserNow()
              let re=accountStore.redirectmode;
         
              // console.warn(re)
              if(re===true){
                // console.warn('vvvvvvvvv')
                this.props.navigation.navigate("SignedIn");
              }
              else{
                console.warn('fuckkkkkk')
              }
              }
            }
             //onPress={() => {this.props.navigation.navigate('Landingpage')}}
            >
              <View
                style={{
                  flex: 1,
                  alignItems: 'flex-end',
                  backgroundColor: 'transparent',
                  marginRight: 20,
                }}
              >
                <Text style={{ color: '#000', flex: 1 }}>Submit</Text>
              </View>
            </TouchableOpacity>
          ) : (
            // Or this one otherwise
            <TouchableOpacity style={styles.submitButton} onPress={() =>{
            //  this.swipe()
            const{index}=this.state
             console.warn(index)
             this.VerifyContent(index);
            }
             }>
              <View
                style={{
                  flex: 1,
                  alignItems: 'flex-end',
                  backgroundColor: 'transparent',
                  marginRight: 20,
                }}
              >
                <Text style={{ color: '#000', flex: 1 }}>Next</Text>
              </View>
            </TouchableOpacity>
          )}
        </View>
      </Footer>
    );
  };

  /**
   * Render the component
   */
  render = ({ children } = this.props) => {
    return (
      <View style={[styles.container, styles.fullScreen]}>
        {/* Render screens */}
        {this.renderScrollView(children)}
        {/* Render pagination */}
        {/* {this.renderPagination()} */}
        {/* Render Continue or Done button */}
        {this.renderButton()}
      </View>
    );
  };
}

export default withNavigation(Swiper);



const styles = StyleSheet.create({
  footerWrap: {
    // marginVertical: 5,
    // borderTopRadius: 15,
    borderStyle: 'solid',
    borderTopWidth: 1,
    borderTopColor: '#ccc',
    backgroundColor: '#FFF',
    height: 80,
  },
  // Set width and height to the screen size
  fullScreen: {
    width: width,
    height: height,
  },
  // Main container
  container: {
    backgroundColor: 'transparent',
    position: 'relative',
  },
  // Slide
  slide: {
    backgroundColor: 'transparent',
  },
  // Pagination indicators
  pagination: {
    // position: 'absolute',
    // bottom: 110,
    left: 0,
    right: 0,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    // alignItems: 'flex-end',
    // alignItems: 'center',
    // backgroundColor: 'purple',
    // paddingTop:12
  },
  // Pagination dot
  dot: {
    // backgroundColor: 'rgba(0,0,0,.25)',
    backgroundColor: '#ccc',
    width: 8,
    height: 8,
    borderRadius: 4,
    marginLeft: 3,
    marginRight: 3,
    // marginTop: 3,
    // marginBottom: 3
  },
  // Active dot
  activeDot: {
    backgroundColor: '#000000',
  },
  // Button wrapper
  buttonWrapper: {
    backgroundColor: 'transparent',
    flexDirection: 'column',
    position: 'absolute',
    bottom: 0,
    left: 0,
    flex: 1,
    paddingHorizontal: 10,
    paddingVertical: 40,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
});

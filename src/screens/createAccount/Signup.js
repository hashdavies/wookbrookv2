import React, { Component } from 'react';
import { Text, StyleSheet, ImageBackground, Keyboard, TouchableOpacity } from 'react-native';
import { KeyboardAvoidingView, Image, ScrollView } from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons';
import {
  Container,
  Header,
  Content,
  Textarea,
  Button,
  View,
  Thumbnail,
  Form,
  Item,
  Input,
  Label,
  Badge,
  Icon,
  Footer,
  Picker,
  Item as FormItem,
} from 'native-base';
import {
  Kaede,
  Hoshi,
  Jiro,
  Isao,
  Madoka,
  Akira,
  Hideo,
  Kohana,
  Makiko,
  Sae,
  Fumi,
} from 'react-native-textinput-effects';
import { Col, Row, Grid } from 'react-native-easy-grid';

import { myspiner } from '../../dependency/UtilityFunctions';
import Logo from '../../assets/workbrookAssets/logo.png';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';

import AccountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import PasswordTextBox from '../../component/PasswordTextBox';
import CustomizeHeader from '../frags/CustomizeHeader';


@observer
class Signup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      errorloading: false,

    }
    this.proceednow = this.proceednow.bind(this);

  }
  componentDidMount() {
    AccountStore.UpdateNavigationState(this.props.navigation);

  }
  // proceednow() {
    proceednow = () => {
    AccountStore.RegisterUserNow_NormalReg();
    //     const{value,valid,phoneNumber}=this.state;


    // if(phoneNumber===null){
    //  // alert("ji")
    //   ShowNotification("Enter your registered mobile number",true);
    // }

    // else if(!valid){
    //   ShowNotification("Enter a valid mobile number",true);
    // }
    // else if(AccountStore.regInfo.isPasswordError===true){
    //   ShowNotification("Enter a valid Password",true);
    // }
    // else{


    // console.log(value)
    // console.log(phoneNumber)

    // AccountStore._updateUserModile(phoneNumber,value);
    // AccountStore.VerifyPhoneNumber(phoneNumber);
    // }
    //      console.log(this.state)
  }

  render() {
    const { isProcessing } = AccountStore;
    return (
      <Container>
        <CustomizeHeader
          // leftside='cancel'
          title='Sign Up with Email'
          textColor='#000'
          rightside='empty'
          statedColor='#fff'
          DontShhowNotification={false}
        />
{/* 
        <CustomizeHeader
          leftside='cancel'
          title='Sign Up with Email'
          DontShhowNotification={false}
        /> */}
        <ScrollView>

          <Container style={styles.container}>


            <View style={styles.contentWrap}>

              <Item regular style={[styles.loginBtn1, { backgroundColor: '#E5E5E5', marginTop: 25, }]}>
                <Input
                  placeholderTextColor={'#BCBCBC'}
                  style={styles.inputSty}
                  placeholder='Email'
                  keyboardType={'email-address'}
                  onChangeText={(email) => AccountStore.onChangeText('email', email, 'regInfo_Normal')}
                />
              </Item>
              <Item regular style={[styles.loginBtn1, { backgroundColor: '#E5E5E5', marginBottom: 5, }]}>
                <Input
                  placeholderTextColor={'#BCBCBC'}
                  style={styles.inputSty}
                  placeholder='Choose a Username'
                  onChangeText={(username) => AccountStore.onChangeText('username', username, 'regInfo_Normal')}
                />
              </Item>
              <Text style={[styles.contentText, { color: '#BCBCBC', marginBottom: 10, }]}>You will not be able to change it.
          </Text>
             {/* <Item regular style={[styles.loginBtn1, { backgroundColor: '#E5E5E5', marginBottom: 5, }]}>
              <Input
               secureEntryInput={true} 
               placeholderTextColor={'#BCBCBC'} 
               style={styles.inputSty}
                placeholder='Choose a Password'
                onChange={(password) => AccountStore.onChangeText('password', password,'regInfo_Normal')}
                />
            </Item>  */}
              <PasswordTextBox
                icon="lock"
                label="Password"
                placeholder="Password"
                //  onChange={(password) => this.setState({password})}
                onChange={(password) => AccountStore.onChangeText('password', password, 'regInfo_Normal')}
                returnKeyType={'done'}
                keyboardType={'default'}
                onSubmitEditing={
                  this.proceednow
                }
              />
              <React.Fragment>
                {AccountStore.regInfo_Normal.isPasswordError === false ?
                  <Text style={[styles.contentText, { color: '#BCBCBC', marginBottom: 10, }]}>
                    Must contain UPPER and lowercase and numbers.
  </Text>
                  :
                  <Text style={[styles.contentText, { color: 'red', marginBottom: 10, }]}>
                    Must contain UPPER and lowercase and numbers.
</Text>

                }

              </React.Fragment>

              <Button full style={[styles.loginBtn1, { backgroundColor: '#5ABC7A', marginTop: 10, marginBottom: 5 }]}
                onPress={this.proceednow}
              >
                <Text style={styles.buttonText}>

                  {isProcessing === true ? 'Processing...' : ' Sign Up'}
                </Text>
              </Button>
              <Button full transparent style={[styles.loginBtn1, { backgroundColor: 'transparent', height: 30 }]}


              >
                <Text style={[styles.buttonText, { fontSize: 11, color: '#2F2F2F' }]}>
                  By joining, you agree to Workbrook's
              </Text>
                <Text style={[styles.buttonText, { color: '#1E93E2', fontSize: 11, marginLeft: 2 }]}>
                  Terms of Service
              </Text>
              </Button>

              <View style={[styles.contentWrap, { justifyContent: 'space-between', alignItems: 'center', height: 40, flexDirection: 'row' }]}>
              <Button style={[styles.loginBtn2, { backgroundColor: 'tranparent', height: 30, marginBottom: 0 }]}
                onPress={() => this.props.navigation.navigate("Login")}
              >
                <Text style={[styles.buttonText, { color: '#1E93E2', fontSize: 16, marginLeft: 2, fontWeight: '600' }]}>
                  Sign In
          </Text>
              </Button>
              {/* <Button style={[styles.loginBtn2, { backgroundColor: 'tranparent', height: 30, marginBottom: 0, width: 150, display: 'flex', justifyContent:'flex-end',alignItems: 'center' }]}
                onPress={() => this.props.navigation.navigate("ForgetPassword")}
              >
                <Text style={[styles.buttonText, { color: '#1E93E2', fontSize: 16, marginLeft: 2, fontWeight: '600' }]}>
                  Forgot Password
          </Text>
              </Button> */}
            </View>

            </View>
          </Container>

        </ScrollView>

      </Container>

    );
  }
}
export default Signup;

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#fff",
    display: "flex",
    justifyContent: "space-between",
    paddingTop: 50,
    paddingRight: 30,
    // paddingBottom: 35,
    paddingLeft: 30,
  },
  contentWrap: {
    // backgroundColor: "red",
    display: "flex",
  },
  socialWrapper: {
    display: "flex",
    justifyContent: 'space-evenly',
    alignItems: 'center',
    flexDirection: 'row',
    marginTop: 20
  },
  contentText: {
    color: "#2F2F2F",
    fontSize: 12,
  },
  contentTextHeader: {
    color: "#2F2F2F",
    fontSize: 24,
    fontWeight: "600",

  },
  loginBtn1: {
    height: 56,
    borderRadius: 5,
    marginBottom: 15,
    elevation: 0,
  },
  loginBtn2: {
    height: 55,
    borderRadius: 5,
    width: 95,
    elevation: 0,
  },
  buttonText: {
    color: "#FAFAFA",
    fontSize: 14,
    fontWeight: "600",
  },
  inputSty: {
    // color: "#BCBCBC",
    fontSize: 14,
    fontWeight: "300",
  },

});

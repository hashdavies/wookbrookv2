import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  ScrollView,
  View,
  StatusBar,
  Image,
  StyleProvider,
  TouchableOpacity,
  ImageBackground,
  TouchableHighlight,
  FlatList,
} from "react-native";

import AntDesign from "react-native-vector-icons/AntDesign";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import Feather from "react-native-vector-icons/Feather";
import Entypo from "react-native-vector-icons/Entypo";

import ZocialIcon from "react-native-vector-icons/Zocial";
import MaterialCommunityIconsIcon from "react-native-vector-icons/MaterialCommunityIcons";
import SimpleLineIcons from "react-native-vector-icons/SimpleLineIcons";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";

import { CustomAsset } from "../../../utils/assets";
import { ReturnPartOfText, myspiner } from "../../dependency/UtilityFunctions";
import CustomizeHeader from "../frags/CustomizeHeader";
import EmptyListComponent from "../../StatelessScreens/EmptyListComponent";

import DashBordStore from "../../stores/Dashboard";
import { observer } from "mobx-react/native";
import Swiper from "./Swiper";
import {
  Container,
  Header,
  Content,
  Button,
  Thumbnail,
  Form,
  Item,
  Input,
  Label,
  Body,
  CheckBox,
} from "native-base";
import dashboardStore from "../../stores/Dashboard";

const Dimensions = require("Dimensions");
let deviceWidth = Dimensions.get("window").width;
let deviceHeight = Dimensions.get("window").height;

let phoneWidth = deviceWidth / 2.8;
let topImageWidth = deviceWidth - 10;

let w1 = deviceWidth / 3;
let w = w1 - 12;
let t = deviceHeight / 3;
const boxsize = (t + t) / 3;
let h = deviceHeight / 3;

@observer
export default class Articles extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      index: 0,
    };
  }
  componentDidMount() {
    DashBordStore.GetFeaturedArticle();
    DashBordStore.GetAllArticle();
  }

  render() {
    let ErrorComponentDisplay;

    if (DashBordStore.Pagelodders.FeaturedArticle === true) {
      ErrorComponentDisplay = myspiner(
        dashboardStore.Pagelodders.FeaturedArticle
      );
    } else {
      ErrorComponentDisplay = <EmptyListComponent message="No data found" />;
    }

    const AllArticles = (
      <FlatList
        data={DashBordStore.AllArticle}
        renderItem={({ item }) => (
          <React.Fragment>
            {/* { console.warn(item.properties.article['articlesid'])} */}
            <View style={styles.superWrap}>
              <View style={styles.leftwrap}>
                <View
                  style={{
                    flex: 1,
                    alignItems: "center",
                    padding: 2,
                    paddingTop: 20,
                  }}
                >
                  <Text style={{ fontSize: 25, color: "#fff" }}>
                    {item.properties.article["articletitle"]} ...
                  </Text>
                </View>

                <View style={{ flex: 1, justifyContent: "center" }}>
                  <View style={styles.rightimage_wrap}>
                    <ImageBackground
                      source={{ uri: item.properties.article["articleimage"] }}
                      style={styles.backgroundImage}
                      resizeMode="cover"
                      imageStyle={{ borderRadius: 15 }}
                    ></ImageBackground>
                  </View>
                </View>
              </View>
              <View style={styles.left_down_wrap}>
                <View style={{ flex: 1, alignItems: "flex-start" }}>
                  <View>
                    <Text style={{ fontSize: 12, color: "#686868" }}>
                      Jinad david
                    </Text>
                  </View>
                  <View>
                    <Text style={{ fontSize: 12, color: "#686868" }}>
                      23/4/2019 . 6 min read
                    </Text>
                  </View>
                </View>

                <View style={styles.down_right_wrap}>
                  <View style={{ width: 60 }}>
                    <Feather name="bookmark" size={30} color="#fff" />
                  </View>
                  <View style={{ width: 60 }}>
                    <FontAwesome name="ellipsis-v" size={30} color="#fff" />
                  </View>
                </View>
              </View>
            </View>
          </React.Fragment>
        )}
        keyExtractor={(item) =>
          item.properties.article["articlesid"].toString()
        }
      />
    );

    let itemkeys = -1;
    const FeaturedArticle_View = DashBordStore.FeaturedArticle.map((item) => {
      // console.warn(item);
      itemkeys += 1;
      return (
        <View style={styles.categoryWrapper}>
          <ImageBackground
            source={{ uri: item.articleimage }}
            style={styles.FeaturedBackGround}
            resizeMode="cover"
            imageStyle={{ borderRadius: 5 }}
          >
            <View
              style={{
                backgroundColor: "rgba(0,0,0,0.1)",
                flex: 1,
                flex: 1,
                justifyContent: "flex-end",
                marginBottom: 0,
                padding: 19,
              }}
            >
              <View style={styles.displayTitle}>
                <Text style={styles.textDisplayTite}>{item.articletitle}</Text>
              </View>
              <View style={styles.displayTitle}>
                <Text style={styles.textDisplaycontent}>
                  {ReturnPartOfText(item.article, 30)}
                </Text>
              </View>
            </View>
          </ImageBackground>
        </View>
      );
    });

    return (
      <Container>
        <CustomizeHeader
          leftside="menu"
          title="Articles"
          ShowNotification={true}
        />
        <View style={styles.container}>
          <ScrollView style={{ marginTop: 0 }}>
            {DashBordStore.FeaturedArticle.length > 0 ? (
              <Swiper>
                {FeaturedArticle_View}

                {/* <View style={styles.categoryWrapper}>

<ImageBackground
source={{ uri: 'http://i.imgur.com/XP2BE7q.jpg' }}
style= {styles.FeaturedBackGround} 
resizeMode='cover'
imageStyle={{ borderRadius: 5 }}
>
<View    style={{backgroundColor: 'rgba(0,0,0,0.1)', flex: 1,  flex: 1,
justifyContent: 'flex-end',
marginBottom:0,padding:19,}} >


<View style={styles.displayTitle}>
<Text style={styles.textDisplayTite}>
Premium Tours
</Text>

</View>
<View style={styles.displayTitle}>
<Text style={styles.textDisplaycontent}>
Visit Top Destinations this Weekend
</Text>

</View>
</View>

</ImageBackground>

</View>
*/}
              </Swiper>
            ) : (
              ErrorComponentDisplay
            )}

            {DashBordStore.AllArticle.length > 0 ? (
              AllArticles
            ) : (
              <View
                style={{
                  flex: 1,
                  alignItems: "center",
                  padding: 2,
                  paddingTop: 20,
                }}
              >
                <Text style={{ fontSize: 25, color: "#fff" }}>
                  Loading Please wait ...
                </Text>
              </View>
            )}

            {/* 
            <View style={styles.superWrap}>


              <View style={styles.leftwrap}>
                <View style={{ flex: 1, alignItems: 'center', padding: 2, paddingTop: 20, }}>
                  <Text style={{ fontSize: 25, color: '#fff', }}>
                    On Tuesday, The 20th of june, 2017 ,  Member of ...
</Text>
                </View>

                <View style={{ flex: 1, justifyContent: 'center', }}>
                  <View style={styles.rightimage_wrap}>

                    <ImageBackground
                      source={{ uri: 'http://i.imgur.com/XP2BE7q.jpg' }}
                      style={styles.backgroundImage}
                      resizeMode='cover'
                      imageStyle={{ borderRadius: 15 }}
                    >


                    </ImageBackground>


                  </View>
                </View>

              </View>
              <View style={styles.left_down_wrap}>
                <View style={{ flex: 1, alignItems: 'flex-start', }}>
                  <View>
                    <Text style={{ fontSize: 12, color: '#686868', }}>
                      Jinad david
</Text>
                  </View>
                  <View>
                    <Text style={{ fontSize: 12, color: '#686868', }}>
                      23/4/2019   . 6 min read</Text>
                  </View>
                </View>

                <View style={styles.down_right_wrap}>

                  <View style={{ width: 60, }}>
                    <Feather name="bookmark" size={30} color='#fff' />
                  </View>
                  <View style={{ width: 60, }}>
                    <FontAwesome name="ellipsis-v" size={30} color='#fff' />
                  </View>

                </View>

              </View>
            </View>

            <View style={styles.superWrap}>


              <View style={styles.leftwrap}>
                <View style={{ flex: 1, alignItems: 'center', padding: 2, paddingTop: 20, }}>
                  <Text style={{ fontSize: 25, color: '#fff', }}>
                    Nelson death at Trafalgar secure is position as ...
</Text>
                </View>

                <View style={{ flex: 1, justifyContent: 'center', }}>
                  <View style={styles.rightimage_wrap}>

                    <ImageBackground
                      source={{ uri: 'http://i.imgur.com/XP2BE7q.jpg' }}
                      style={styles.backgroundImage}
                      resizeMode='cover'
                      imageStyle={{ borderRadius: 15 }}
                    >


                    </ImageBackground>


                  </View>
                </View>

              </View>
              <View style={styles.left_down_wrap}>
                <View style={{ flex: 1, alignItems: 'flex-start', }}>
                  <View>
                    <Text style={{ fontSize: 12, color: '#686868', }}>
                      Jinad david
</Text>
                  </View>
                  <View>
                    <Text style={{ fontSize: 12, color: '#686868', }}>
                      23/4/2019   . 6 min read</Text>
                  </View>
                </View>

                <View style={styles.down_right_wrap}>

                  <View style={{ width: 60, }}>
                    <Feather name="bookmark" size={30} color='#fff' />
                  </View>
                  <View style={{ width: 60, }}>
                    <FontAwesome name="ellipsis-v" size={30} color='#fff' />
                  </View>

                </View>

              </View>
            </View> */}

            <View style={styles.pagewrap}>
              <View
                style={[styles.upperTextwrap, { alignItems: "flex-start" }]}
              >
                <View style={[styles.uppertext, { flexDirection: "row" }]}>
                  <Text style={{ color: "#fff", fontSize: 20 }}>
                    Featured Collections
                  </Text>
                  {/* <Entypo name="chevron-right" size={30} color='#6a6a6a' /> */}
                </View>
                {/* <View style={[styles.uppertext, { alignItems: 'flex-end' }]}>
                  <Text style={{ color: '#ff8025', fontSize: 20, }}>
                    Show on Map
    </Text>
                </View> */}
              </View>

              <ScrollView style={{ marginTop: 0 }} horizontal={true}>
                <View style={{ marginRight: 12 }}>
                  <ImageBackground
                    source={{ uri: "http://i.imgur.com/XP2BE7q.jpg" }}
                    style={styles.cat_BackgroundImage}
                    resizeMode="cover"
                    imageStyle={{ borderRadius: 5 }}
                  ></ImageBackground>
                </View>
                <View style={{ marginRight: 12 }}>
                  <ImageBackground
                    source={{ uri: "http://i.imgur.com/XP2BE7q.jpg" }}
                    style={styles.cat_BackgroundImage}
                    resizeMode="cover"
                    imageStyle={{ borderRadius: 5 }}
                  ></ImageBackground>
                </View>
                <View style={{ marginRight: 12 }}>
                  <ImageBackground
                    source={{ uri: "http://i.imgur.com/XP2BE7q.jpg" }}
                    style={styles.cat_BackgroundImage}
                    resizeMode="cover"
                    imageStyle={{ borderRadius: 5 }}
                  ></ImageBackground>
                </View>
                <View style={{ marginRight: 12 }}>
                  <ImageBackground
                    source={{ uri: "http://i.imgur.com/XP2BE7q.jpg" }}
                    style={styles.cat_BackgroundImage}
                    resizeMode="cover"
                    imageStyle={{ borderRadius: 5 }}
                  ></ImageBackground>
                </View>
              </ScrollView>
            </View>
          </ScrollView>
        </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  categoryWrapper: {
    borderRadius: 25,
    borderColor: "#000",
    borderWidth: 2,
    marginRight: 5,
  },
  superWrap: {
    flex: 1,
    borderBottomColor: "#626262",
    borderBottomWidth: 0.5,
  },
  leftwrap: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    padding: 12,
  },
  rightimage_wrap: {
    borderColor: "#000",
    borderWidth: 2,
    width: "80%",
    marginLeft: "auto",
    marginRight: "auto",
  },
  left_down_wrap: {
    flex: 1,
    flexDirection: "row",
    padding: 12,
  },

  down_right_wrap: {
    flex: 1,
    flexDirection: "row",
    marginLeft: 12,
    justifyContent: "flex-end",
    alignItems: "flex-end",
  },
  pagewrap: {
    flex: 1,
    // backgroundColor: '#000',
    flexDirection: "column",
    padding: 12,
  },
  backgroudContentWrap: {
    // backgroundColor: '#1b1b1b',
    flex: 1,
    paddingTop: 0,
    flex: 1,
    justifyContent: "flex-end",

    marginBottom: 12,
    padding: 0,
  },

  upperTextwrap: {
    flexDirection: "row",
    padding: 12,
  },
  uppertext: {
    flex: 1,
  },
  container: {
    flex: 1,
    backgroundColor: "#000",
  },
  displayTitle: {
    // flex:1,
    paddingTop: 8,
    justifyContent: "center",
    alignItems: "center",
  },

  textDisplayTite: {
    color: "#fff",
    fontSize: 30,
    textAlign: "center",
    // backgroundColor:'red'
  },
  cat_textDisplayTite: {
    color: "#fff",
    fontSize: 20,
  },
  textDisplaycontent: {
    color: "#fff",
    fontSize: 20,
  },
  cat_textDisplaycontent: {
    color: "#fff",
    fontSize: 12,
  },
  titleWrap: {
    borderBottomColor: "#ccc",
    borderBottomWidth: 1,
    //   backgroundColor: 'rgba(0,0,0,0.1)',
    //   flex: 1,
    //   paddingTop:0,
    //   flex: 1,
    // justifyContent: 'center',
    // marginBottom: 12,
    paddingBottom: 15,
  },

  noteStyle: {
    margin: 5,
    fontStyle: "italic",
    color: "#b2bec3",
    fontSize: 15,
  },
  featuredTitleStyle: {
    marginHorizontal: 5,
    textShadowColor: "#00000f",
    textShadowOffset: { width: 3, height: 3 },
    textShadowRadius: 3,
  },
  bottomView: {
    width: "100%",
    height: 50,
    backgroundColor: "#EE5407",
    justifyContent: "center",
    alignItems: "center",
    position: "absolute", //Here is the trick
    bottom: 0, //Here is the trick
  },
  // container: {
  //   width: '100%',
  //   flex: 1,
  //   // justifyContent: 'center',
  //   alignItems: 'center',
  //   backgroundColor: 'purple',
  // },

  iconsizeNcolour: {
    fontSize: 30,
    color: "#fff",
  },
  innerBoxShadow: {
    position: "relative",
    width: w,
    height: 90,
    backgroundColor: "#fff",
    borderRadius: 3,
    overflow: "hidden",
  },
  downlistboxesWrap: {
    marginRight: "auto",
    marginLeft: "auto",
    width: "80%",
    marginTop: 12,
    flexDirection: "row",
    justifyContent: "center",
  },
  boxes: {
    flex: 1,
    backgroundColor: "#2c3f50",
    height: 70,
    shadowOffset: { width: 10, height: 10 },
    shadowColor: "red",
    shadowOpacity: 1.0,
    marginRight: 8,
  },
  boxesContentCenter: {
    alignItems: "center",
    justifyContent: "center",
    flex: 1,
  },
  downthreebox: {
    height: 45,
    backgroundColor: "#72cae2",
    alignItems: "center",
    justifyContent: "center",
    width: w,
    marginRight: 12,
    padding: 12,
  },
  threetabs: {
    height: 60,
    backgroundColor: "#f8ca28",
    alignItems: "center",
    justifyContent: "center",
    flex: 1,
    // borderRadius:1,
    borderRightColor: "#baac34",
    borderRightWidth: 1,
  },
  threetabsText: {
    color: "#000",
    fontSize: 20,
    textAlign: "center",
  },
  textsize: {
    color: "#2c3f50",
    fontSize: 20,
    textAlign: "center",
  },
  img: {
    height: 80,
    // borderRadius: 50,
    width: "70%",
    marginRight: "auto",
    marginLeft: "auto",
  },
  caroImage: {
    flex: 1,
    width: "100%",
  },
  imgwrap: {
    alignItems: "center",
    justifyContent: "center",
    // width:150,
    // borderRadius:150,
    // height:150,
    backgroundColor: "#fff",
  },
  userDp: {
    height: 118,
    borderRadius: 118,
    width: 118,
    paddingTop: 10,
  },
  btnwrap: {
    flexDirection: "row",
    marginRight: "auto",
    marginLeft: "auto",
    width: "80%",
  },
  listdownsection: {
    flexDirection: "row",
    marginTop: 12,
    // justifyContent:'space-around'
    justifyContent: "center",
    alignItems: "center",
    width: "70%",
    marginRight: "auto",
    marginLeft: "auto",
  },
  listdowndownsection: {
    flexDirection: "row",
    // marginTop:12,
    // justifyContent:'space-around'
    justifyContent: "center",
    alignItems: "center",
    //   width: '70%',
    //   marginRight:'auto',
    // marginLeft:'auto',
  },

  backgroundImage: {
    height: 140,
    // flex: 1,
    width: null,
  },
  FeaturedBackGround: {
    height: 200,
    width: topImageWidth,
  },
  cat_BackgroundImage: {
    height: 160,
    width: phoneWidth,
    // height: null,
    // opacity:2,
    // borderRadius:12,
    // padding:2,
  },

  // centertext: {
  //   flex:1,
  // justifyContent: 'center',
  // alignItems: 'center'
  // },

  quickorder: {
    backgroundColor: "#3e516f",
  },
  requestBeautician: {
    backgroundColor: "#087071",
  },
  profile: {
    backgroundColor: "#f88d01",
  },
  about: {
    backgroundColor: "#f75a4a",
  },
  customerservice: {
    backgroundColor: "#935e42",
  },
  feedback: {
    backgroundColor: "#a83c87",
  },
});

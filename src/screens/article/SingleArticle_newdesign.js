
import React, { Component } from 'react';
import {
  Platform, StyleSheet, Text, ScrollView,
  View, StatusBar, Image, StyleProvider, TouchableOpacity, ImageBackground, TouchableHighlight, FlatList
} from 'react-native';

import AntDesign from 'react-native-vector-icons/AntDesign';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Feather from 'react-native-vector-icons/Feather';
import Entypo from 'react-native-vector-icons/Entypo';

import ZocialIcon from 'react-native-vector-icons/Zocial';
import MaterialCommunityIconsIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {
  TabView,
  TabBar,
  SceneMap,
  type NavigationState,
} from 'react-native-tab-view';
import { CustomAsset } from '../../../utils/assets';
import { ReturnPartOfText, myspiner, SkeletonLoader } from '../../dependency/UtilityFunctions'
import CustomizeHeader from '../frags/CustomizeHeader';
import EmptyListComponent from '../../StatelessScreens/EmptyListComponent';
import SkeletonPlaceholder from "react-native-skeleton-placeholder";
import DashBordStore from '../../stores/Dashboard'
import { observer } from 'mobx-react/native'
import Swiper from './Swiper';
import { Container, Header, Content, Button, Thumbnail, Form, Item, Input, Label, Body, CheckBox } from 'native-base';
import dashboardStore from '../../stores/Dashboard';
import SkeletonHoc from '../../containers/SkeletonHoc';

import Info from './Info';
import Review from './Review';
import More from './More';
const Dimensions = require('Dimensions');
let deviceWidth = Dimensions.get('window').width;
let deviceHeight = Dimensions.get('window').height;



let phoneWidth = deviceWidth / 2.8;
let topImageWidth = deviceWidth;




let w1 = deviceWidth / 3;
let w = w1 - 12;
let t = deviceHeight / 3;
const boxsize = (t + t) / 3;
let h = deviceHeight / 3;

type State = NavigationState<{
  key: string,
  title: string,
}>;


@observer
export default class Articles extends Component<*, State> {

  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      index: 1,
      routes: [
        { key: 'Info', title: 'Info' },
        { key: 'Review', title: 'Review' },
        { key: 'More', title: 'More' },
      ],

    }
    this._ReadArticle = this._ReadArticle.bind(this);
    this.__ReloadPage = this.__ReloadPage.bind(this);

  }
  componentDidMount() {
    // DashBordStore.GetFeaturedArticle();
    DashBordStore.GetArticles();
    // DashBordStore.GetAllArticle();


  }
  _ReadArticle = (Item) => {
    // const {MarkerViewDetails}=this.state;
    console.warn(Item);
    this.props.navigation.navigate(
      'SingleArticle',
      {
        data: Item,
        otherParam: 'anything you want here',
      }

    );
  }
  __ReloadPage = () => {
    DashBordStore.GetArticles();
  }

  // Tab function Start here  
  _handleIndexChange = index =>
    this.setState({
      index,
    });
  _renderTabBar = props => (
    <TabBar
      {...props}
      scrollEnabled
      indicatorStyle={styles.indicator}
      style={styles.tabbar}
      tabStyle={styles.tab}
      labelStyle={styles.label}
    />
  );

  _renderScene = SceneMap({
    Review: Review,
    Info: Info,
    More: More,
  });
  // Tab function ends here  
  render() {
    let ErrorComponentDisplay = null;
    let itemkeys = -1;
    if (DashBordStore.Pagelodders.loader_visible === true) {
      // ErrorComponentDisplay = myspiner(dashboardStore.Pagelodders.loader_visible)
      // ErrorComponentDisplay = SkeletonLoader();
      ErrorComponentDisplay = <SkeletonHoc>
        <View style={{ width: "100%", height: 200, marginBottom: 20 }} />


        {[0, 1, 2, 3, 4].map((_, index) => (
          <View key={index} style={{ marginBottom: 12 }}>
            <SkeletonPlaceholder>
              <View style={{ flexDirection: "row", margin: 13, backgroundColor: '#ccc' }}>

                <View
                  style={{
                    justifyContent: "space-evenly",
                    marginLeft: 12,
                    flex: 1
                  }}
                >
                  <View style={{ width: "60%", height: 20 }} />
                  <View style={{ width: "60%", height: 30 }} />
                  {/* <View style={{ width: "80%", height: 20 }} /> */}
                </View>
                <View style={{ width: 100, height: 100 }} />

              </View>
            </SkeletonPlaceholder>
          </View>
        ))}
        <View style={{
          backgroundColor: '#fff', flex: 1,
          justifyContent: 'flex-end',
          marginBottom: 0, padding: 19, height: 140
        }} >


          <View style={styles.displayTitle}>

          </View>
          <View style={styles.displayTitle}>


          </View>
          <View style={styles.displayTitle}>


          </View>
        </View>

      </SkeletonHoc>;


    }
    else {
      ErrorComponentDisplay = <EmptyListComponent
        message={DashBordStore.Articles.ErrorMessage}
        onclickhandler={this.__ReloadPage}
      />
    }



    itemkeys = -1;
    const FeaturedArticle_View = DashBordStore.Articles.FeaturedArticle.map((item) => {
      // console.log(item);
      itemkeys += 1;
      return (
        <View style={styles.categoryWrapper} key={itemkeys}>
          <TouchableHighlight underlayColor="#000"
          // onPress={() => this._ReadArticle(item)}
          >
            <ImageBackground
              source={{ uri: item.articleimage }}
              style={styles.FeaturedBackGround}
              resizeMode='cover'
            // imageStyle={{ borderRadius: 5 }}
            >
            </ImageBackground>
          </TouchableHighlight>
        </View>


      );

    });




    return (
      <Container>
        <StatusBar
          backgroundColor="#000000"
          barStyle="light-content"
          animated={true}
          StatusBarAnimation='slide'
        />
        <CustomizeHeader
          leftside='back'
          title='Tour'
          ShowNotification={true}



        />
        <View style={styles.container}>

          {DashBordStore.Articles.FeaturedArticle.length > 0 ?

            <ScrollView style={{ marginTop: 0, height: 'auto' }}>

              <React.Fragment>
                <Swiper>

                  {FeaturedArticle_View}

                </Swiper>

                <View style={[styles.upperTextwrap, { alignItems: 'flex-start', display: 'flex', }]}>
                  <View style={[styles.uppertext, { flexDirection: 'column', borderBottomWidth: 1, borderBottomColor: '#646464', paddingBottom: 5, }]}>
                    <Text style={{ color: '#fff', fontSize: 25, textAlign: 'center' }}>
                      Great Fire of London
</Text>
                    <Text style={{ color: '#fff', fontSize: 14, textAlign: 'center' }}>
                      Audio Tour
</Text>

                  </View>

                </View>

                <View style={styles.pagewrap}>
                  <ScrollView >
                    <Text style={{ color: '#fff', textAlign: 'justify', marginBottom: 8, lineHeight: 20 }}>
                      The fire started at 1am on Sunday morning on 2 September in Thomas
                      Farriner's bakery on Pudding Lane. It may have been caused by a
                      spark from his oven falling onto a pile of fuel nearby. The fire
                      spread easily because London was very dry after a long, hot summer
</Text>
                    <Text style={{ color: '#fff', textAlign: 'justify', marginBottom: 8, lineHeight: 20 }}>
                      After a co-ordinated effort to put the fire out it was finally
                      won by two factors. Strong east winds that had been fanning
                      the flames died down, and the Tower of London garrison used
                      gundpowder to create effective firebreaks which stop the
                      fire travelling any further east.
</Text>
                  </ScrollView>
                  <View style={styles.footerItemWrap}>
                    <View style={styles.footerInnerWrap}>

                      <Button transparent style={styles.footerBtn1} >
                        <MaterialCommunityIconsIcon name='format-letter-case' style={styles.footerIconSty1}/>
                      </Button>
                      <Button rounded iconLeft style={[styles.footerBtn2, {minWidth: 100}]}>
                        <AntDesign name='sound' style={[styles.footerIconSty3, {marginRight:10,}]} />
                        <Text style={styles.footerIconSty3}>Listen</Text>
                      </Button>

                    </View>
                    <View style={[styles.footerInnerWrap, {justifyContent:'flex-end'}]}>
                      <Button transparent style={styles.footerBtn1}>
                        <AntDesign name='hearto' style={styles.footerIconSty2} />
                      </Button>
                      <Button transparent style={styles.footerBtn1}>
                        <FontAwesome name='bookmark-o' style={styles.footerIconSty2} />
                      </Button>
                      <Button transparent style={styles.footerBtn1}>
                        <AntDesign name='sharealt' style={styles.footerIconSty2} />
                      </Button>
                    </View>
                  </View>
                  {/* <View>

                  </View> */}
                </View>
              </React.Fragment>








            </ScrollView>
            :
            ErrorComponentDisplay
            // null
          }
        </View>
      </Container>
    );



  }
}


const styles = StyleSheet.create({
  categoryWrapper: {
    borderColor: '#000',
    borderWidth: 2,
    marginRight: 5,
  },
  container2: {
    flex: 1,
    width: '100%',
  },
  footerItemWrap: {
    padding: 10,
    paddingRight: 0,
    backgroundColor: '#2E3436',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
  },
  footerInnerWrap: {
    flex: 1,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    // backgroundColor: 'red',
  },
  footerIconSty1: {
    fontSize: 25,
    fontWeight: 'bold',
    color: '#fff',
  },
  footerIconSty2: {
    fontSize: 20,
    color: '#fff',
    marginRight:10,
  },
  footerIconSty3: {
    fontSize: 17,
    color: '#fff',
  },
  footerBtn1: {
    marginRight:10,
  },
  footerBtn2: {
    borderWidth:1,
    borderColor: '#fff',
    fontSize: 25,
    color: '#fff',
    backgroundColor: 'transparent',
    paddingRight: 5,
    paddingLeft: 10,
    height: 30,
    marginTop: 7
  },
  superWrap: {
    flex: 1,
    borderBottomColor: '#242424',
    borderBottomWidth: 0.5
  },
  leftwrap: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    padding: 12,
  },
  rightimage_wrap: {
    borderColor: '#000',
    borderWidth: 2,
    width: '60%',
    marginLeft: 'auto',
    marginRight: 12,
    justifyContent: 'flex-end',

  },
  left_down_wrap: {
    flex: 1,
    flexDirection: 'row',
    padding: 12,
  },

  down_right_wrap: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  pagewrap: {
    padding: 12,
    minHeight: 300,
    height: 'auto',

  },
  backgroudContentWrap: {
    // backgroundColor: '#1b1b1b',
    flex: 1,
    paddingTop: 0,
    flex: 1,
    justifyContent: 'flex-end',

    marginBottom: 12,
    padding: 0,
  },


  tabbar: {
    // backgroundColor: '#3f51b5',
    // backgroundColor: '#3e516f',
    backgroundColor: '#000',
  },
  tab: {
    width: 120,
  },
  indicator: {
    backgroundColor: '#FF7101',
    // backgroundColor: '#000',
  },
  label: {
    color: '#fff',
    // color: '#000',
    fontWeight: '400',
  },

  upperTextwrap: {
    flexDirection: 'row',
    padding: 12,
  },
  uppertext: {
    flex: 1
  },
  container: {
    flex: 1,
    backgroundColor: '#000',
  },
  displayTitle: {
    // flex:1,
    paddingTop: 2,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    // backgroundColor: 'rgba(0,0,0,0.4)',
    // paddingLeft:10,
  },

  textDisplayTite: {
    color: '#fff',
    fontSize: 20,
    textAlign: 'left',
    // backgroundColor:'red'
  },
  cat_textDisplayTite: {
    color: '#fff',
    fontSize: 20,
  },
  textDisplaycontent: {
    color: '#fff',
    fontSize: 12,
  },
  cat_textDisplaycontent: {
    color: '#fff',
    fontSize: 12,
  },
  titleWrap: {
    borderBottomColor: '#ccc',
    borderBottomWidth: 1,
    //   backgroundColor: 'rgba(0,0,0,0.1)', 
    //   flex: 1,
    //   paddingTop:0,  
    //   flex: 1,
    // justifyContent: 'center',
    // marginBottom: 12,
    paddingBottom: 15,
  },

  noteStyle: {
    margin: 5,
    fontStyle: 'italic',
    color: '#b2bec3',
    fontSize: 15,
  },
  featuredTitleStyle: {
    marginHorizontal: 5,
    textShadowColor: '#00000f',
    textShadowOffset: { width: 3, height: 3 },
    textShadowRadius: 3
  },
  bottomView: {
    width: '100%',
    height: 50,
    backgroundColor: '#EE5407',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute', //Here is the trick
    bottom: 0, //Here is the trick
  },
  // container: {
  //   width: '100%',
  //   flex: 1,
  //   // justifyContent: 'center',
  //   alignItems: 'center',
  //   backgroundColor: 'purple',
  // },

  iconsizeNcolour: {
    fontSize: 30,
    color: '#fff',

  },
  innerBoxShadow: {
    position: "relative",
    width: w,
    height: 90,
    backgroundColor: "#fff",
    borderRadius: 3,
    overflow: "hidden"

  },
  downlistboxesWrap: {
    marginRight: 'auto',
    marginLeft: 'auto',
    width: '80%',
    marginTop: 12,
    flexDirection: 'row',
    justifyContent: 'center'
  },
  boxes: {
    flex: 1,
    backgroundColor: '#2c3f50',
    height: 70,
    shadowOffset: { width: 10, height: 10, },
    shadowColor: 'red',
    shadowOpacity: 1.0,
    marginRight: 8
  },
  boxesContentCenter: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1
  },
  downthreebox: {
    height: 45,
    backgroundColor: '#72cae2',
    alignItems: 'center',
    justifyContent: 'center',
    width: w,
    marginRight: 12,
    padding: 12
  },
  threetabs: {
    height: 60,
    backgroundColor: '#f8ca28',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    // borderRadius:1,
    borderRightColor: '#baac34',
    borderRightWidth: 1,
  },
  threetabsText: {
    color: '#000',
    fontSize: 20,
    textAlign: 'center'
  },
  textsize: {
    color: '#2c3f50',
    fontSize: 20,
    textAlign: 'center',
  },
  img: {
    height: 80,
    // borderRadius: 50,
    width: '70%',
    marginRight: 'auto',
    marginLeft: 'auto',
  },
  caroImage: {
    flex: 1,
    width: '100%'
  },
  imgwrap: {

    alignItems: 'center',
    justifyContent: 'center',
    // width:150,
    // borderRadius:150,
    // height:150,
    backgroundColor: '#fff'
  },
  userDp: {
    height: 118,
    borderRadius: 118,
    width: 118,
    paddingTop: 10
  },
  btnwrap: {
    flexDirection: 'row',
    marginRight: 'auto',
    marginLeft: 'auto',
    width: '80%',
  },
  listdownsection: {
    flexDirection: 'row',
    marginTop: 12,
    // justifyContent:'space-around'
    justifyContent: 'center',
    alignItems: 'center',
    width: '70%',
    marginRight: 'auto',
    marginLeft: 'auto',
  },
  listdowndownsection: {
    flexDirection: 'row',
    // marginTop:12,
    // justifyContent:'space-around'
    justifyContent: 'center',
    alignItems: 'center',
    //   width: '70%',
    //   marginRight:'auto',
    // marginLeft:'auto',
  },

  backgroundImage: {
    height: 80,
    // flex: 1,
    width: null,
    backgroundColor: '#A9A9A9',
  },
  FeaturedBackGround: {
    height: 320,
    width: topImageWidth,
  },
  cat_BackgroundImage: {
    height: 160,
    width: phoneWidth,
    backgroundColor: '#A9A9A9',
    // height: null,
    // opacity:2,
    // borderRadius:12,
    // padding:2,
  },

  // centertext: {
  //   flex:1,
  // justifyContent: 'center',
  // alignItems: 'center'
  // },


  quickorder: {
    backgroundColor: '#3e516f',
  },
  requestBeautician: {
    backgroundColor: '#087071',
  },
  profile: {
    backgroundColor: '#f88d01',
  },
  about: {
    backgroundColor: '#f75a4a',
  },
  customerservice: {
    backgroundColor: '#935e42',
  },
  feedback: {
    backgroundColor: '#a83c87',
  },

}); 
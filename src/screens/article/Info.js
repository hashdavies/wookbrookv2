
import React, { Component } from 'react';
import {
  Platform, StyleSheet, Text, ScrollView,
  View, StatusBar, Image, StyleProvider, TouchableOpacity, ImageBackground, TouchableHighlight
} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Feather from 'react-native-vector-icons/Feather';
import Entypo from 'react-native-vector-icons/Entypo';
import ZocialIcon from 'react-native-vector-icons/Zocial';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { CustomAsset } from '../../../utils/assets';
import CustomizeHeader from '../frags/CustomizeHeader';
import Dialog from "react-native-dialog";

const Dimensions = require('Dimensions');
let deviceWidth = Dimensions.get('window').width;
let deviceHeight = Dimensions.get('window').height;



let phoneWidth = deviceWidth / 2.8;
let w1 = deviceWidth / 3;
let w = w1 - 12;
let t = deviceHeight / 3;
const boxsize = (t + t) / 3;
let h = deviceHeight / 3;


import { Container, Header, Content, Button, Thumbnail, Icon,
  Form, Item, Input, Label, Body, CheckBox } from 'native-base';

export default class Tours extends Component {

  constructor(props) {
    super(props);
    this.state = {
      dialogVisible: false
    }
  }

  showDialog = () => {
    this.setState({ dialogVisible: true });
  };
 
  handleCancel = () => {
    this.setState({ dialogVisible: false });
  };
 
  handleDelete = () => {
    // The user has pressed the "Delete" button, so here you can do your own logic.
    // ...Your logic
    this.setState({ dialogVisible: false });
  };



  render() {

    return (
      <Container>
        <View style={styles.container}>
        <View style={{ display: 'flex',flexDirection: 'row', justifyContent: 'center', padding: 2, paddingTop: 20, }}>
                  <Text style={{ color: '#fff', textAlign : 'justify', lineHeight: 20 }}>
                  The Great Fire of London swept through the central parts of the English 
                  city from Sunday, 2 September to Thursday, 6 September 1666. 
                  The fire gutted the medieval City of London inside the old Roman city wall
                  </Text>
                </View>
                <View style={styles.BtnWrap}>
                <Button bordered style={styles.btnsty1}>
            <Text style={{color: '#FF7101'}}>Get Direction</Text>
          </Button>
         </View>
                <View style={styles.btmBtnWrap}>
                <Button iconLeft transparent primary style={styles.btmBtnSty1}>
            <Icon name='unlock' style={styles.btmIconSty} />
            <Text>Unlock</Text>
          </Button>
          <Text style={styles.btmseprator}>|</Text>
          <Button iconLeft transparent primary style={styles.btmBtnSty1} onPress={this.showDialog}>
            <Icon name='play' style={styles.btmIconSty} />
            <Text>Preview</Text>
          </Button>
         </View>
              </View>
              <React.Fragment>
              <Dialog.Container contentStyle={{backgroundColor: '#2D2D2D', color: '#fff'}} visible={this.state.dialogVisible}>
          <Dialog.Title style={[styles.addcolor,{fontSize: 25, paddingBottom: 8,}]}>The Great Fire of London</Dialog.Title>
          <Dialog.Description style={[styles.addcolor, {fontSize: 18, borderTopWidth: 1, borderTopColor: '#646464', paddingTop: 12, paddingBottom: 5}]}>
            Gain Access to:
          </Dialog.Description>
          <Dialog.Description style={[styles.addcolor, {fontSize: 14}]}>
            <MaterialCommunityIcons name='human-handsup' style={styles.AccessIcon} />
            Audio tour
          </Dialog.Description>
          <Dialog.Description style={[styles.addcolor, {fontSize: 14}]}>
            <MaterialIcons name='location-on' style={styles.AccessIcon} />
            More than 10 location
          </Dialog.Description>
          <Dialog.Description style={[styles.addcolor, {fontSize: 14}]}>
            <AntDesign name='download' style={styles.AccessIcon} />
            Access to map offline
          </Dialog.Description>
          <Dialog.Description style={[styles.addcolor, {fontSize: 14}]}>
            <FontAwesome name='plane' style={styles.AccessIcon} />
            No roaming fees
          </Dialog.Description>
          <Dialog.Button style={[styles.addcolor, {textTransform: 'uppercase'}]} label="Cancel" onPress={this.handleCancel} />
          <Dialog.Button style={{color: '#FF7101', textTransform: 'uppercase'}} label="Buy $100" onPress={this.handleDelete} />
        </Dialog.Container>
        </React.Fragment>
      </Container>
    );



  }
}


const styles = StyleSheet.create({


  container: {
    flex: 1,
    // justifyContent: 'center',
    backgroundColor: '#000',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',


  },
  BtnWrap: {
    // borderColor : '#FF7101',
    // minWidth: 200,
    // textAlign: 'center'
  },
  btnsty1: {
    borderColor : '#FF7101',
    minWidth: '90%',
    textAlign: 'center',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection : 'row',
    borderRadius: 5,
    marginTop: 10,
    marginBottom: 5,
  },
  btmBtnWrap: {
    borderColor : '#FF7101',
    backgroundColor: '#FF7101',
    minWidth: '90%',
    textAlign: 'center',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection : 'row',
    marginBottom: 5,
    paddingLeft: 10,
    paddingRight: 10,
  },
  btmBtnSty1: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  btmseprator: {
    fontSize: 25
  },
  btmIconSty: {
    color: '#000',
    marginRight: 10
  },
  AccessIcon: {
    color: '#FF7101',
    marginRight: 50,
    letterSpacing: 20,
    fontSize: 14,
  },
  addcolor: {
    color: '#fff',
  },
  

}); 
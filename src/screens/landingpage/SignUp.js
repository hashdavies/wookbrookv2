const Dimensions = require('Dimensions');
import React, { Component } from 'react';
import {  ScrollView, StyleSheet, ImageBackground, Image, TouchableOpacity } from 'react-native';
import {
  Container,
  Text,
  Header,
  Content,
  Button,
  View,
  Form,
  Item,
  Input,
  Label,
} from 'native-base';
import {
  Kaede,
  Hoshi,
  Jiro,
  Isao,
  Madoka,
  Akira,
  Hideo,
  Kohana,
  Makiko,
  Sae,
  Fumi,
} from 'react-native-textinput-effects';
const window = Dimensions.get('window');
// import { Container, Header, Content, Button, Text } from 'native-base';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import * as Animatable from 'react-native-animatable';
import PhoneInput from 'react-native-phone-input';
import firebase from '@react-native-firebase/app';import * as RNLocalize from "react-native-localize";

import SpinButton from '../../component/SpinButton'

const IMAGE_HEIGHT = window.width / 2;

import { CustomAsset } from '../../../utils/assets';
import { ShowNotification } from '../../dependency/UtilityFunctions';
import AccountStore from '../../stores/Account';
import OtpPage from './OtpPage';
 import { observer } from 'mobx-react/native';
import PasswordTextBox from '../../component/PasswordTextBox';

 @observer
class SignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activity_loader: true,
      defaultLoading:false,
      valid: false,
      type: "",
      phoneNumber: null,
      value: "",
      pickerData:[],
      defaultlocalize:'ng'
    };
    this.updateInfo = this.updateInfo.bind(this);
    this.proceednow = this.proceednow.bind(this);
    this._GotoPage = this._GotoPage.bind(this);

    // this.proceednowold = this.proceednowold.bind(this);
  }

  componentDidMount() {
    // this.signOut();
   // this.ClearAllLocalStorage();
      this.unsubscribe = firebase.auth().onAuthStateChanged((user) => {
        if (user) {
  
          console.log(user.metadata);
          console.log(user.phoneNumber);
          console.warn(user.metadata);
          console.warn(user.phoneNumber);
          let phoneNumber=user.phoneNumber;
          let new_formated_number = phoneNumber.replace('+234','0');
          console.warn(new_formated_number)
  
          // AccountStore._updateUserModile(new_formated_number,phoneNumber);
          // AccountStore.readyToLoadData = true;
          // AccountStore.LoadUserData(new_formated_number);
          this.setState({ user: user.toJSON() });
          this.signOut();
          ShowNotification("Auto verify, Code Accepted",true);

          this._GotoPage("RegistrationPage")
          //alert(JSON.stringify(user.toJSON()))
        } 
        else {
         // alert(JSON.stringify("User has been signed out,"))
          console.warn("User has been signed out, reset the state")
          // User has been signed out, reset the state
          this.setState({
            user: null,
            message: '',
            codeInput: '',
            phoneNumber: '+44',
            confirmResult: null,
  
          });
        }
      });
      AccountStore.UpdateNavigationState(this.props.navigation);
    ///  console.log()
     // console.log(RNLocalize.getLocales());
  //console.log(RNLocalize.getCurrencies());
  let userlocale=RNLocalize.getLocales();
  // console.log(userlocale[0].countryCode)
  let countrycode=userlocale[0].countryCode.toLowerCase() || 'ng'
  this.setState({
    defaultlocalize:countrycode,
  });
    }
  
    componentWillUnmount() {
       if (this.unsubscribe) this.unsubscribe();
    }
    signOut = () => {
      firebase.auth().signOut();
    }
  updateInfo(mobile) {
    if(mobile.length >2){
      this.setState({
      valid: this.phone.isValidNumber(),
      type: this.phone.getNumberType(),
      value: this.phone.getValue(),
      getISOCode: this.phone.getISOCode(),
      phoneNumber: mobile,
    });
    }
    
 
  }
  _GotoPage(page) {
 
 this.props.navigation.navigate(page);
}
  proceednow() {
    const{value,valid,phoneNumber}=this.state;


if(phoneNumber===null){
 // alert("ji")
  ShowNotification("Enter your registered mobile number",true);
}

else if(!valid){
  ShowNotification("Enter a valid mobile number",true);
}
else if(AccountStore.regInfo.isPasswordError===true){
  ShowNotification("Enter a valid Password",true);
}
else{
 
 
console.log(value)
console.log(phoneNumber)

AccountStore._updateUserModile(phoneNumber,value);
AccountStore.AuthenticateUser(phoneNumber);
}
     console.log(this.state)
  }

  renderPhoneNumberInput() {
    // const { phoneNumber } = this.state;
       
     return (
 
      <View    style={{backgroundColor: '#fff', flex: 1,paddingTop:12,}} >
      <ScrollView style={{ marginTop: 0 }}>
{/* <View style={{backgroundColor:'#000'}}> */}
    
           <View style={styles.contentSideWrap}>
        
<View style={{justifyContent: 'center', alignItems: 'flex-start'}}>
<Text style={[{color:'#4e4646',textAlign:'center'},styles.HeaderText]}>
Get Started
</Text>
<Text style={[{color:'#948f8f',textAlign:'center'},styles.HeaderSubText]}>
Enter your mobile number to login or register
</Text>

</View>

<View > 
<Form style={styles.form2}>

          
            
{/* <Item floatingLabel> */}
{/* <Label style={styles.text3}>Phone number{JSON.stringify(this.state)}</Label> */}
{/* <Label style={styles.text3}>Phone number</Label> */}
{/* <Input
autoFocus
keyboardType={'phone-pad'}
returnKeyType={"next"} 
style={styles.input1}
onChangeText={value => this.setState({ phoneNumber: value })}

/> */}
<PhoneInput
 ref={ref => {
  this.phone = ref;
}}
 returnKeyType={"next"} 
 style={styles.input1}
 textStyle={{color:'#4e4646',textAlign:'left'}}
//  flagStyle={{width:70,height:30,}}
 flagStyle= {{width: 40, height: 20, borderWidth:0,}}
 focus={true}
 keyboardType={'phone-pad'}
 allowZeroAfterCountryCode={false}
 onChangePhoneNumber={value => this.updateInfo(value)}
  initialCountry={this.state.defaultlocalize}
 offset={20}
//  onSubmitEditing={ this.proceednow}

 />
{/* </Item> */}
</Form> 

 

<PasswordTextBox
      icon="lock"
      label="Password" 
      placeholder='Set password'
       onChange={(password) => AccountStore.onChangeText('password', password,'regInfo')}

       />
<View style={{flex:1,marginTop:5}}>
  <Text style={styles.PasswordInstruction}>
  The password must contain both Alphabets and numbers and
Must the be atleast 8 characters long
  </Text>
</View>



<View style={{flex:1,flexDirection:'row',marginTop: 25 ,}}>
<View style={{  flex:1,justifyContent:'center'}}>
           <Text style={{color:'red',fontSize:12}}>
         
           {AccountStore.regInfo.isPasswordError===true ?

'  Enter A valid Password  '

:

''

           } 
           </Text>
            </View>
<View style={{ width:140}}>
              {/* <Button
                iconRight
                light
                block
                 style={[styles.Button_Style,styles.BlackBackground]}
                // onPress={() => {this.props.navigation.navigate('Signup')}}
              >
                <Text style={[styles.textstyle,{color:'#fff',}]}>NEXT</Text>
               </Button> */}


               <SpinButton
          style={[{ backgroundColor: '#0C5E9E', borderStyle: 'solid', borderRadius: 5, elevation: 0, height: 50 },styles.Button_Style,styles.BlackBackground]}
          loaderState={AccountStore.isProcessing}
          buttonText='NEXT'
          buttonTextStyle={{ fontWeight:'bold', padding: 20, fontSize: 14,color:'#fff' }}
          onPressAction={ 
            // ()=>AccountStore.AuthenticateUser('08062268055')
            this.proceednow
            // this.signIn
            // setTimeout(() => {
            //   this.setState({ defaultLoading: false });
            // }, 3000);
        }
         />


            </View>
          
</View>
<View  style={{flex:1,justifyContent:'flex-start',alignItems:'flex-start',flexDirection:'row',marginTop:15}}>
            <Text style={[{color:'#948f8f',textAlign:'center'},styles.HeaderSubText,{marginRight:12,color:'#000'}]}>
Already have account click 
</Text>
<TouchableOpacity
           onPress={() => {this.props.navigation.navigate('LoginPage')}}

>
<Text style={[{color:'#948f8f',textAlign:'center'},styles.HeaderSubText,{color:'#000'}]}>
here
</Text>
</TouchableOpacity>

            </View>
</View>          
        
  
     
     </View>
       </ScrollView>
    
   
     </View>  
    
     );
   }

  render() {
    return (
      <Container  style={styles.PageWrapper}>
      {!AccountStore.signInData.confirmResult && this.renderPhoneNumberInput()}
 { AccountStore.signInData.confirmResult && 
 <OtpPage
 NavigateToPage={()=>this._GotoPage("RegistrationPage")}
 />}
       
 
      </Container>
    );
  }
}
export default SignUp;

const styles = StyleSheet.create({
  Inputcontainer: {
    // marginVertical: 5,
    // borderRadius: 4,
    // borderStyle: 'solid',
    // borderWidth: 0.9,
    // borderColor:'grey',

    // borderColor:'#357d83',
    // borderColor: '#727272',
    // borderColor:'#4ab1be',
    // backgroundColor: 'green',
    flex: 0,
    height: 'auto',
    overflow: 'hidden'
  },

  PasswordInstruction:{
    fontSize: 12,
    fontWeight: "300",
    fontStyle: "normal",
    lineHeight: 13,
  letterSpacing: 0.1,
  textAlign: "left",
  color: "#948f8f"
  },
  labelStyle:{ 
    color: '#000000', 
    fontSize: 15,
   },
   inputStyle__:{ 
    color:'#948f8f',
    fontSize:14,
    fontWeight:'normal' 
   },
  HeaderText:{ 
  fontSize:20,
  fontWeight:"500",
},
  HeaderSubText:{ 
  fontSize:14,
  fontWeight:"normal",
},
form2: {
  marginLeft: 0,
},

text3: {
  color: '#4e4646',
  fontWeight: 'bold',
  padding: 0,
  margin: 0,
  marginLeft:12,

},
input1: {
  // paddingTop: 4,
  marginTop: 19,
  borderColor: '#CCCCCC',
// borderTopWidth: 1,
borderBottomWidth: 1,
// borderLeftColor:'red',
// borderLeftWidth:1,
height: 50,
fontSize: 25,
paddingLeft: 20,
paddingRight: 20,
color:'green'
},


PageWrapper: {
  // backgroundColor: 'rgba(0,0,0,0.4)',
  paddingTop: '1%',
  marginLeft: 'auto',
  marginRight: 'auto',
  width: '100%',
  backgroundColor:'#fff',
  paddingLeft:10,
},

textstyle: {
  textAlign: 'center',
  flex: 1,
  color: '#0a0b0d',
  // fontWeight: 'bold',
},
textstyle2: {
  textAlign: 'center',
  // flex: 1,
  color: '#fff',
  fontWeight: 'bold',
  marginLeft:-8,

},
BlackBackground:{ 
 
   backgroundColor:'#0a0b0d',
  },
Button_Style:{ 
justifyContent: 'center',
 alignItems: 'center',
 borderRadius:5,
 borderColor:'#707070' ,
 borderStyle:'solid'
 },
 contentSideWrap: {
  flex:1,
  // width: '100%',
   justifyContent: 'flex-end',
  //  flexDirection: 'column',
  // alignContent:'center',
  marginTop:40,
  marginRight:12
  // alignItems: 'center',

  // paddingTop: 150,
  // backgroundColor:'green',
},








  signupwithGoogle:{ 
  // justifyContent: 'center',
  // alignItems: 'center',
  borderRadius:5,
  backgroundColor:'#4589f1',
  // justifyContent:'center' 
paddingLeft:50,
// paddingRight:'auto',
// width:'80%',
},


  container: {
    flex: 1,
    alignItems: 'center',
  },
  iconstyle: {
    fontSize: 13,
    color: '#fff',
    // width: 40,
    paddingLeft:6,
    // alignItems:'flex-end',
    // alignSelf:'center',
    //  backgroundColor:'green',
    //  width:100,
    
  },
  iconstyle_login: {
    fontSize: 10,
    color: '#fff',
    width: 40,
    paddingLeft:8,
    paddingTop:3,
    // alignItems:'center',
     
  },

  textstyle3: {
    textAlign: 'center',
    // flex: 1,
    color: '#fff',
    fontWeight: 'bold',
    fontSize:14,
    // backgroundColor:'green'

  },
  backgroundImage: {
    // height:null,
    flex: 1,
    width: null,
    // height: null,
    opacity: 2,
  },


 
  img: {
    height: 100,
    // borderRadius: 100,
    width: 100,
  },
  imgdown: {
    // flex: 1,
    aspectRatio: 0.7,
    width: 170,
    // height:170,
    resizeMode: 'contain',
    //  marginBottom:60,
    marginTop: -40,
  },
});

const Dimensions = require('Dimensions');
import React, { Component } from 'react';
import { View, ScrollView, StyleSheet, ImageBackground, Image } from 'react-native';
const window = Dimensions.get('window');
import { Container, Header, Content, Button, Text } from 'native-base';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import * as Animatable from 'react-native-animatable';

const IMAGE_HEIGHT = window.width / 2;

import { CustomAsset } from '../../../utils/assets';

class LanguageSelect extends Component {
  constructor(props){
    super(props)
   }
 
  render() {
    return (
      <Container>
        {/* <Content style={{flex:1,}}> */}
        {/* <View style={{flex:1,}}>      */}
        {/* <ImageBackground source={CustomAsset.bg} style={styles.backgroundImage} resizeMode="cover">
         
        </ImageBackground> */}
       
           <View    style={{backgroundColor: '#fff', flex: 1,paddingTop:12,}} >
          <ScrollView style={{ marginTop: 0 }}>
  {/* <View style={{backgroundColor:'#000'}}> */}
        
               <View style={styles.contentSideWrap}>
            
 <View style={{justifyContent: 'center', alignItems: 'center'}}>
<Text style={[{color:'#0a0b0d',textAlign:'center'},styles.HeaderText]}>
Choose your Language
</Text>

</View>
 
              
              <View
                animation="zoomInDown"
                duration={600}
                delay={1001}
                style={styles.buttonwrap}
              >
                <View style={{ flexDirection: 'column', marginBottom: 15 }}>
                  <Button
                    iconRight
                    light
                    block
                    style={[{ justifyContent: 'center', alignItems: 'center',borderRadius:5, },styles.BlackBackground]}
                    // onPress={() => {this.props.navigation.navigate('Signup')}}
                  >
                    <Text style={[styles.textstyle,{color:'#fff'}]}>ENGLISH</Text>
                   </Button>
                </View>
                <View style={{ flexDirection: 'column', marginBottom: 15 }}>
                  <Button
                    iconRight
                    light
                    block
                    style={styles.Button_Style}
                    // onPress={() => {this.props.navigation.navigate('Signup')}}
                  >
                    <Text style={styles.textstyle}>PIDGIN</Text>
                   </Button>
                </View>
                <View style={{ flexDirection: 'column', marginBottom: 15 }}>
                  <Button
                    iconRight
                    light
                    block
                    style={styles.Button_Style}
                    // onPress={() => {this.props.navigation.navigate('Signup')}}
                  >
                    <Text style={styles.textstyle}>YORUBA</Text>
                   </Button>
                </View>
                <View style={{ flexDirection: 'column', marginBottom: 15 }}>
                  <Button
                    iconRight
                    light
                    block
                     style={styles.Button_Style}
                    // onPress={() => {this.props.navigation.navigate('Signup')}}
                  >
                    <Text style={styles.textstyle}>IGBO</Text>
                   </Button>
                </View>
                <View style={{ flexDirection: 'column', marginBottom: 15 }}>
                  <Button
                    iconRight
                    light
                    block
                     style={styles.Button_Style}
                    // onPress={() => {this.props.navigation.navigate('Signup')}}
                  >
                    <Text style={styles.textstyle}>HAUSA</Text>
                   </Button>
                </View>
                <View style={{ flexDirection: 'column', marginBottom: 15 }}>
                  <Button
                    iconRight
                    light
                    block
                     style={styles.Button_Style}
                    // onPress={() => {this.props.navigation.navigate('Signup')}}
                  >
                    <Text style={styles.textstyle}>FRENCH</Text>
                   </Button>
                </View>
                <View style={{ flexDirection: 'column', marginBottom: 15 }}>
                  <Button
                    iconRight
                    light
                    block
                     style={styles.Button_Style}
                    // onPress={() => {this.props.navigation.navigate('Signup')}}
                  >
                    <Text style={styles.textstyle}>PORTUGUESE</Text>
                   </Button>
                </View>
             
                <View style={{ flexDirection: 'column', marginBottom: 15,marginTop:30 }}>
                  
                  <Button
                    // iconLeft
                    light
                    block
                    style={{ justifyContent: 'center', alignItems: 'center',borderRadius:5,backgroundColor:'#0a0b0d',height:59, }}
               onPress={() => {this.props.navigation.navigate('SignUp')}}
                  >
                     <Text style={styles.textstyle2}>NEXT</Text>
                  
                  </Button>  
              </View>
             
         </View>
      
         
         </View>
           </ScrollView>
        
       
         </View>  
        {/* </Content> */}
      </Container>
    );
  }
}
export default LanguageSelect;

const styles = StyleSheet.create({

  HeaderText:{ 
  fontSize:20,
  fontWeight:"500",
},
innerpage: {
  // backgroundColor: 'rgba(0,0,0,0.4)',
  flex: 1,
  // padding:30,
  // justifyContent:'center',
  // alignItems:'center',
},

buttonwrap: {
  // backgroundColor: 'rgba(0,0,0,0.4)',
  paddingTop: '15%',
  marginLeft: 'auto',
  marginRight: 'auto',
  width: '60%',
  // backgroundColor:'#000'
},

textstyle: {
  textAlign: 'center',
  flex: 1,
  color: '#0a0b0d',
  // fontWeight: 'bold',
},
textstyle2: {
  textAlign: 'center',
  // flex: 1,
  color: '#fff',
  fontWeight: 'bold',
  marginLeft:-8,

},
BlackBackground:{ 
 
   backgroundColor:'#0a0b0d',
  },
Button_Style:{ 
justifyContent: 'center',
 alignItems: 'center',
 borderRadius:5,
 borderColor:'#707070' ,
 borderStyle:'solid'
 },
 contentSideWrap: {
  flex:1,
  // width: '100%',
   justifyContent: 'flex-end',
  //  flexDirection: 'column',
  // alignContent:'center',
  marginTop:40,
  // alignItems: 'center',

  // paddingTop: 150,
  // backgroundColor:'green',
},








  signupwithGoogle:{ 
  // justifyContent: 'center',
  // alignItems: 'center',
  borderRadius:5,
  backgroundColor:'#4589f1',
  // justifyContent:'center' 
paddingLeft:50,
// paddingRight:'auto',
// width:'80%',
},


  container: {
    flex: 1,
    alignItems: 'center',
  },
  iconstyle: {
    fontSize: 13,
    color: '#fff',
    // width: 40,
    paddingLeft:6,
    // alignItems:'flex-end',
    // alignSelf:'center',
    //  backgroundColor:'green',
    //  width:100,
    
  },
  iconstyle_login: {
    fontSize: 10,
    color: '#fff',
    width: 40,
    paddingLeft:8,
    paddingTop:3,
    // alignItems:'center',
     
  },

  textstyle3: {
    textAlign: 'center',
    // flex: 1,
    color: '#fff',
    fontWeight: 'bold',
    fontSize:14,
    // backgroundColor:'green'

  },
  backgroundImage: {
    // height:null,
    flex: 1,
    width: null,
    // height: null,
    opacity: 2,
  },


 
  img: {
    height: 100,
    // borderRadius: 100,
    width: 100,
  },
  imgdown: {
    // flex: 1,
    aspectRatio: 0.7,
    width: 170,
    // height:170,
    resizeMode: 'contain',
    //  marginBottom:60,
    marginTop: -40,
  },
});

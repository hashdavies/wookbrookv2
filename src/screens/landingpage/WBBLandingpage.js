import React, { Component } from 'react';
import { Text, StyleSheet, ImageBackground, Keyboard, TouchableOpacity } from 'react-native';
import { KeyboardAvoidingView, Image, ScrollView, Alert } from 'react-native';
import WBB_Button from "../frags/wbbButton"
import Ionicons from 'react-native-vector-icons/Ionicons';
import {
  Container,
  Header,
  Content,
  Textarea,
  Button,
  View,
  Thumbnail,
  Form,
  Item,
  Input,
  Label,
  Badge,
  Icon,
  Footer,
  Picker,
  Item as FormItem,
} from 'native-base';
import {
  Kaede,
  Hoshi,
  Jiro,
  Isao,
  Madoka,
  Akira,
  Hideo,
  Kohana,
  Makiko,
  Sae,
  Fumi,
} from 'react-native-textinput-effects';
import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes,
} from 'react-native-google-signin';
// import { GoogleSignin } from '@react-native-community/google-signin';

import { myspiner, _signIn_with_Google, _signOut_GoogleAccount } from '../../dependency/UtilityFunctions';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import RBSheet from "react-native-raw-bottom-sheet";


import AccountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import { SvgUri } from 'react-native-svg';


@observer
class Landing extends Component {
  constructor(props) {
    super(props);
    this.mapRef = null;
    this.state = {
      email: '',
      password: '',
      errorloading: false,
      userInfo: null,
      error: null,

    }
  }
  componentDidMount() {
  }
  // async componentDidMount() {
  // }

  render() {

    return (

      <ScrollView>

        <Container style={styles.container}>
          <View style={styles.logoWrap}>
            <SvgUri
            width="105px"
            height="80"
            uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1597142778/mobileWBB/wbbLogo_depzwh.svg"
            />
            <SvgUri
            width="139px"
            height="21px"
            uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1597142778/mobileWBB/wbbName_pzybtk.svg"
            />
          </View>

          <View style={styles.logoWrap}>
            <WBB_Button
            buttonText="Sign Up"
            height="80"
            
            />
            <WBB_Button
            backgroundColorSty="#fff"
            buttonTextStyle={styles.buttonText}
            buttonText="Sign In"
            height="80"
            />
            
          </View>

        </Container>
      
      </ScrollView>
    );
  }

}



export default Landing;

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#3A6ED4",
    display: "flex",
    justifyContent: "space-around",
    paddingTop: 50,
    paddingRight: 30,
    paddingBottom: 35,
    paddingLeft: 30,
  },
  contentWrap: {
    // backgroundColor: "red",
    display: "flex",
  },
  logoWrap: {
    display: "flex",
    alignItems: "center"
  },
  buttonText:{ 
    // textTransform:'capitalize',
    fontSize: 18,
    color:'#171721'
  },
  
 
});

const Dimensions = require('Dimensions');
import React, { Component } from 'react';
import {  ScrollView, StyleSheet, ImageBackground, Image ,Platform} from 'react-native';
import {
  Container,
  Text,
  Header,
  Content,
  Button,
  View,
  Form,
  Item,
  Input,
  Label,
  Body,
  Title
} from 'native-base';
const window = Dimensions.get('window');
// import { Container, Header, Content, Button, Text } from 'native-base';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import * as Animatable from 'react-native-animatable';
import PhoneInput from 'react-native-phone-input';
import CodeInput from 'react-native-confirmation-code-input';
import RBSheet from "react-native-raw-bottom-sheet";


const IMAGE_HEIGHT = window.width / 2;

import { CustomAsset } from '../../../utils/assets';
import AccountStore from '../../stores/Account';
import { ShowNotification } from '../../dependency/UtilityFunctions';

class OtpPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activity_loader: true,
      defaultLoading:false,
      valid: false,
      type: "",
      phoneNumber: null,
      value: "",
      pickerData:[],
      defaultlocalize:'ng',
      ErrorMessage:''
    };
    this._onFinishCheckingCode1 = this._onFinishCheckingCode1.bind(this);
    this._GotoPage = this._GotoPage.bind(this);

  }
  _GotoPage(page) {
 
    this.props.navigation.navigate(page);
   }
  _onFinishCheckingCode1(codeInput) {
    // TODO: call API to check code here
   let confirmResult=AccountStore.signInData.confirmResult;
   console.log(confirmResult)
    if (confirmResult && codeInput.length >5) {
      confirmResult.confirm(codeInput)
        .then((user) => {
          console.log(user)
          this.setState({ message: 'Code Confirmed!' });
          console.log("User confirmed")
          // AccountStore.LoadUserData('08062268055');
          // AccountStore.LoadUserData(AccountStore.currentUserMobile.currentMobileNumber_normal);
          // this._GotoPage("RegistrationPage")
          this.props.NavigateToPage
        })
        .catch(error => {

          ShowNotification('You have enter invalid code, please try again.', true);
          console.log(error)
          this.setState({ 
            message: `Code Confirm Error: ${error.message}`,
            ErrorMessage:'You have entered a wrong OTP'
          })
         

        });
    }
    else{
      ShowNotification('Please enter the code sent to your phone number.', true);
    }
      this.refs.codeInputRef1.clear();
 
}

  render() {
    const{ErrorMessage}=this.state;
    return (
      <Container  >
              <Header style={{backgroundColor:'#000',borderBottomColor:'#000',borderBottomWidth:2,fontSize:11}}>

              <Body style={ styles.headerBody}>
          <Title style={{ color:'#fff',fontWeight:'normal',fontSize:16,marginLeft:12}}> You will receive OTP shortly </Title>
          


        </Body>
</Header>
        {/* <Content style={{flex:1,}}> */}
        {/* <View style={{flex:1,}}>      */}
        {/* <ImageBackground source={CustomAsset.bg} style={styles.backgroundImage} resizeMode="cover">
         
        </ImageBackground> */}
       
           {/* <View    style={{backgroundColor: '#fff', flex: 1,paddingTop:12,}} > */}
           <View    style={styles.PageWrapper}>
          <ScrollView style={{ marginTop: 0 }}>
  {/* <View style={{backgroundColor:'#000'}}> */}
        
               <View style={styles.contentSideWrap}>
            
 <View style={{justifyContent: 'center', alignItems: 'flex-start'}}>
<Text style={[{color:'#4e4646',textAlign:'center',marginBottom:12},styles.HeaderText]}>
Hey there,
</Text>
<Text style={[{color:'#948f8f',textAlign:'center'},styles.HeaderSubText]}>
Enter the 6 digit code sent to your
mobile number

</Text>

</View>
 
    <View style={{flex:1,}} > 
<View style={{marginTop:20}}>
  <Text style={styles.PhoneNumberStyle}>
  {/* +234 71 878 6729 */}
  {AccountStore.currentUserMobile.currentMobileNumber_inter}
  </Text>
</View>
    <Form style={styles.form2}>

              
                
    <CodeInput 
    ref="codeInputRef1"
    className={'border-b'}
    // ref="codeInputRef2"
codeLength={6} 
inactiveColor='rgba(49, 180, 4, 1.3)'
autoFocus={true}
keyboardType={'phone-pad'}
space={15}
size={35}
inputPosition='center'
onFulfill={(token) => this._onFinishCheckingCode1(token)}
activeColor='#000'
inactiveColor='#e9e8e8'
containerStyle={{ marginTop: 25,marginBottom:25 }}
// codeInputStyle={{ borderColor:'#ccc',color:'#656565', borderRadius:4, backgroundColor: '#fff'}}
// codeInputStyle={{ color:'#e9e8e8', borderBottomColor:'#e9e8e8',borderBottomWidth:1}}
/>
</Form> 
<View style={{flex:1,flexDirection:'row',marginTop: 25 ,}}>
<View style={{  flex:1,justifyContent:'center',}}>
               <Text style={{color:'#ff5858',fontSize:13}}>
               {ErrorMessage}
               </Text>
                </View>
<View style={{ width:140}}>
                  <Button
                    iconRight
                    light
                    block
                     style={[styles.Button_Style,styles.BlackBackground]}
                    // onPress={() => {this.props.navigation.navigate('Signup')}}
                  >
                    <Text style={[styles.textstyle,{color:'#fff',}]}>NEXT</Text>
                   </Button>
                </View>
</View>
   
    </View>          


        <View style={{marginTop: 40 ,backgroundColor:'transparent',flex:1,marginBottom:15}}>
          <Text style={{color:'#4e4646',fontWeight:'500',marginBottom:12,}}>
          Resend Code via
          </Text>
              <View style={{flex:1,flexDirection:'row',}}>
<View style={{  flex:1,}}>
<View style={{ width:140}}>
                  <Button
                    // iconRight
                    light
                    block
                     style={[styles.Button_Style,styles.WhiteBackground]}
                    // onPress={() => {this.props.navigation.navigate('Signup')}}
                  >
                      <View style={{flex:1,flexDirection:'row'}}>
                      <MaterialIcons name="call" style={[styles.iconstyle,{color:'#000',textAlign:'right',marginTop:4}]} />

<Text style={[styles.textstyle,{color:'#000',backgroundColor:'transparent'}]}>CALL</Text>
                      </View>
                       
                   </Button>
                </View>
                </View>
<View style={{ width:140}}>
                  <Button
                    iconRight
                    light
                    block
                     style={[styles.Button_Style,styles.BlackBackground]}
                    // onPress={() => {this.props.navigation.navigate('Signup')}}
                    onPress={() => this.RBSheet.open()}
                  >
                      <View style={{flex:1,flexDirection:'row'}}>
                      <MaterialCommunityIcons name="message-processing" style={[styles.iconstyle,{color:'#fff',textAlign:'right',marginTop:4}]} />

<Text style={[styles.textstyle,{color:'#fff',backgroundColor:'transparent'}]}>SMS</Text>
                      </View>
                    </Button>
                </View>
</View>
          </View>    

         
         </View>
           </ScrollView>
        
           <View style={{ flex: 1, justifyContent: "center", alignItems: "center", }}>
        {/* <Button title="OPEN BOTTOM SHEET" onPress={() => this.RBSheet.open()} /> */}
        <RBSheet
          ref={ref => {
            this.RBSheet = ref;
          }}
          height={250}
          duration={150}
          closeOnDragDown={false}
          customStyles={{
            container: {
              justifyContent: "center",
              alignItems: "center",
              // borderColor:'red',
              // borderWidth:2,
              borderTopRightRadius:20,
              borderTopLeftRadius:20,
            }
          }}
        >
          {/* <YourOwnComponent /> */}

          <View style={{flex:1,flexDirection:'row',width:'90%',backgroundColor:'#fff',paddingTop:30,}}> 


<View style={{width:40}}>
<MaterialCommunityIcons name="message-processing" style={[styles.iconstyle,{color:'#0a0b0d',textAlign:'right',fontSize:30}]} />

</View>
<View style={{flex:1}}>
<View style={{flex:1,flexDirection:'column',marginLeft:20,}}>
<Text style={{fontSize:18,fontWeight:'600',color:'#0a0b0d',marginBottom:12}}>
Resend Code
</Text>
<Text style={{fontSize:16,color:'#4e4646',}}>
Are you sure you want to send
the code to following number?

</Text>
<Text style={{fontSize:16,color:'#4e4646',}}>
+234 71 878 6729

</Text>
<View style={{flex:1,flexDirection:'row',marginTop: 25 ,}}>
<View style={{  flex:1,backgroundColor:'transparent',alignItems:'flex-end',justifyContent:'center',marginRight:12,height:50}}>
               <Text style={{color:'#948f8f',fontSize:14}}>
               CHANGE
               </Text>
                </View>
<View style={{ width:100}}>
                  <Button
                    iconRight
                    light
                    block
                     style={[styles.Button_Style,styles.BlackBackground]}
                    // onPress={() => {this.props.navigation.navigate('Signup')}}
                  >
                    <Text style={[styles.textstyle,{color:'#fff',}]}>YES</Text>
                   </Button>
                </View>
</View>
   
</View>
</View>
          </View>
        </RBSheet>
      </View>
         </View>  
        {/* </Content> */}
      </Container>
    );
  }
}
export default OtpPage;

const styles = StyleSheet.create({

  HeaderText:{ 
  fontSize:20,
  fontWeight:"500",
},
  HeaderSubText:{ 
  fontSize:14,
  fontWeight:"normal",
},
form2: {
  marginLeft: 0,
},

text3: {
  color: '#4e4646',
  fontWeight: 'bold',
  padding: 0,
  margin: 0,
  marginLeft:12,

},
PhoneNumberStyle: {
  fontSize:18,
  fontWeight:'500',
  color:'#4e4646',
},

headerBody: {
  ...Platform.select({
      ios: {
        marginLeft:-150,
      },
      android: {
        marginLeft:-8,
      }
    }),
  
  },
input1: {
  // paddingTop: 4,
  marginTop: 19,
  borderColor: '#CCCCCC',
// borderTopWidth: 1,
borderBottomWidth: 1,
// borderLeftColor:'red',
// borderLeftWidth:1,
height: 50,
fontSize: 25,
paddingLeft: 20,
paddingRight: 20,
color:'green'
},


PageWrapper: {
  // backgroundColor: 'rgba(0,0,0,0.4)',
  paddingTop: '1%',
  marginLeft: 'auto',
  marginRight: 'auto',
  width: '100%',
  backgroundColor:'#fff',
  paddingLeft:10,
  flex:1,
},

textstyle: {
  textAlign: 'center',
  flex: 1,
  color: '#0a0b0d',
  // fontWeight: 'bold',
},
textstyle2: {
  textAlign: 'center',
  // flex: 1,
  color: '#fff',
  fontWeight: 'bold',
  marginLeft:-8,

},
BlackBackground:{ 
 
   backgroundColor:'#0a0b0d',
  },
WhiteBackground:{ 
 
   backgroundColor:'#ffffff',
   borderColor:'#707070',
   borderWidth:2,
   borderBottomColor:'red'
  //  height:20,
  },
Button_Style:{ 
justifyContent: 'center',
 alignItems: 'center',
 borderRadius:5,
 borderColor:'#707070' ,
 borderStyle:'solid'
 },
 contentSideWrap: {
  flex:1,
  // width: '100%',
   justifyContent: 'flex-end',
  //  flexDirection: 'column',
  // alignContent:'center',
  marginTop:40,
  marginRight:12
  // alignItems: 'center',

  // paddingTop: 150,
  // backgroundColor:'green',
},








  signupwithGoogle:{ 
  // justifyContent: 'center',
  // alignItems: 'center',
  borderRadius:5,
  backgroundColor:'#4589f1',
  // justifyContent:'center' 
paddingLeft:50,
// paddingRight:'auto',
// width:'80%',
},


  container: {
    flex: 1,
    alignItems: 'center',
  },
  iconstyle: {
    fontSize: 20,
    color: '#fff',
    // width: 40,
    // paddingLeft:50,
    alignItems:'flex-end',
    // alignSelf:'center',
    //  backgroundColor:'green',
     width:40,
    //  flex:1,
     justifyContent:'flex-end'
    
  },
  iconstyle_login: {
    fontSize: 10,
    color: '#fff',
    width: 40,
    paddingLeft:8,
    paddingTop:3,
    // alignItems:'center',
     
  },

  textstyle3: {
    textAlign: 'center',
    // flex: 1,
    color: '#fff',
    fontWeight: 'bold',
    fontSize:14,
    // backgroundColor:'green'

  },
  backgroundImage: {
    // height:null,
    flex: 1,
    width: null,
    // height: null,
    opacity: 2,
  },


 
  img: {
    height: 100,
    // borderRadius: 100,
    width: 100,
  },
  imgdown: {
    // flex: 1,
    aspectRatio: 0.7,
    width: 170,
    // height:170,
    resizeMode: 'contain',
    //  marginBottom:60,
    marginTop: -40,
  },
});

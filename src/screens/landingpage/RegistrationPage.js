const Dimensions = require('Dimensions');
import React, { Component } from 'react';
import {  ScrollView, StyleSheet, ImageBackground, Image ,Platform} from 'react-native';
import {    Keyboard, TouchableOpacity } from 'react-native';
import DatePicker from 'react-native-datepicker'

import {
  Container,
  Text,
  Header,
  Content,
  Button,
  View,
  Form,
  Item,
  Input,
  Label,
  Body,
  Title
} from 'native-base';
import {
  Kaede,
  Hoshi,
  Jiro,
  Isao,
  Madoka,
  Akira,
  Hideo,
  Kohana,
  Makiko,
  Sae,
  Fumi,
} from 'react-native-textinput-effects';
const window = Dimensions.get('window');
// import { Container, Header, Content, Button, Text } from 'native-base';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import * as Animatable from 'react-native-animatable';
import PhoneInput from 'react-native-phone-input';
import CodeInput from 'react-native-confirmation-code-input';
import RBSheet from "react-native-raw-bottom-sheet";
import MultiSelect from 'react-native-multiple-select';


const IMAGE_HEIGHT = window.width / 2;

import { CustomAsset } from '../../../utils/assets';
import AccountStore from '../../stores/Account';
import { observer } from 'mobx-react/native';
import SpinButton from '../../component/SpinButton';


const BloodGroup = [{
  id: 'A+',
  name: 'A+',
},
 {
  id: 'A-',
  name: 'A-',
},
 {
  id: 'B+',
  name: 'B+',
},
 {
  id: 'AB+',
  name: 'AB+',
},
 {
  id: 'AB-',
  name: 'AB-',
},
 {
  id: 'O-',
  name: 'O-',
},
 {
  id: 'O+',
  name: 'O+',
},
 
 
];
const Genotypes = [{
  id: 'AA',
  name: 'AA',
},
 {
  id: 'AS',
  name: 'AS',
},
 {
  id: 'SS',
  name: 'SS',
},
 {
  id: 'AC',
  name: 'AC',
},
 
 
 
];

 @observer
class RegistrationPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activity_loader: true,
      defaultLoading:false,
      valid: false,
      type: "",
      phoneNumber: null,
      value: "",
      pickerData:[],
      defaultlocalize:'ng',
      genderview:'Male',
      genotype : [],
      bloodgroup : [],
      ReminderDate: null,
    };
    this._onFinishCheckingCode1 = this._onFinishCheckingCode1.bind(this);
    this.changeGenderValue = this.changeGenderValue.bind(this);

  }

  componentDidMount(){
    this.ShowCurrentDate();
  }
  ShowCurrentDate = () => {
    let date = new Date().getDate();
   let month = new Date().getMonth() + 1;
   let year = new Date().getFullYear();
   if(date <10){
   date= `0${date}`;
   }
   if(month <10){
month= `0${month}`;
   }
let yearmax =year +1;
   // let currentdate = date + '-' + month + '-' + year;
   let minyear = year + '-' + month + '-' + date;
   let maxyear = yearmax + '-' + month + '-' + date;
   this.setState({minyear: minyear})
   this.setState({ReminderDate: minyear})
   this.setState({maxyear: maxyear})
   // Alert.alert(date + '-' + month + '-' + year);
   AccountStore.onChangeText('dob', minyear,'regInfo')
  }
 changeGenderValue=(value)=>{
this.setState({
  genderview:value
})
AccountStore.onChangeText('gender', value,'regInfo')
 }
  _onFinishCheckingCode1(codeInput) {
    // TODO: call API to check code here
  //  let confirmResult=AccountStore.signInData.confirmResult;
  //  console.log(confirmResult)
  //   if (confirmResult && codeInput.length >5) {
  //     confirmResult.confirm(codeInput)
  //       .then((user) => {
  //         console.log(user)
  //         this.setState({ message: 'Code Confirmed!' });
  //         console.log("User confirmed")
  //         // AccountStore.LoadUserData('08062268055');
  //         AccountStore.LoadUserData(AccountStore.currentUserMobile.currentMobileNumber_normal);

  //       })
  //       .catch(error => {
  //         ShowNotification('You have enter invalid code, please try again.', true);
  //         console.log(error)
  //         this.setState({ message: `Code Confirm Error: ${error.message}` })
         

  //       });
  //   }
  //   else{
  //     ShowNotification('Please enter the code sent to your phone number.', true);
  //   }
  console.log(codeInput);
      this.refs.codeInputRef2.clear();
 
}
onSelectedItemsChange = (name,selectedItems) => {
  // do something with selectedItems
  console.log('Selected Items: ', selectedItems);
  console.log('Selected Items: ', selectedItems[0]);
  // selectedItems=selectedItems[0]
  this.setState({
  //   state[]:selectedItems
    [name]:selectedItems,
  })
  let Singlevalue=selectedItems[0];
  AccountStore.onChangeText(name, Singlevalue,'regInfo')
  console.log(this.state);
};


  render() {

    if(AccountStore.redirectmode===true){
      //  Toast.show('Login Successfully', Toast.SHORT, Toast.BOTTOM,ToastStyle);
    
        this.props.navigation.navigate("SignedIn");
      }
      else{
       // console.warn('fuckkkkkk')
      }


    const{genderview}=this.state;
    return (
      <Container  >
 
           <View    style={styles.PageWrapper}>
          <ScrollView style={{ marginTop: 0 }}>
  {/* <View style={{backgroundColor:'#000'}}> */}
        
               <View style={styles.contentSideWrap}>
            
 <View style={{justifyContent: 'center', alignItems: 'flex-start'}}>
<Text style={[{color:'#4e4646',textAlign:'center',marginBottom:12},styles.HeaderText]}>
Enter your info
</Text>
<Text style={[{color:'#948f8f',textAlign:'left'},styles.HeaderSubText]}>
Will use this email address as the primary
email address for communication

</Text>

</View>
 {/* <View style={{flex:1,marginTop:12,}}>
   <View style={styles.ImageBorderWrap}>
   <View style={styles.Image_InnerBorderWrap}>
   <AntDesign name="plus" style={[styles.iconstyle,{color:'#000',textAlign:'center',marginTop:4}]} />
</View>
   </View>
  
 </View> */}
          
 <View padder style={[styles.wrapper]}>

<View style={styles.inputWrapper}>

  <Hoshi
    label={'First name'}
    // this is used as active border color
    borderColor={'#948f8f'}
    // active border height
    borderHeight={0}
    inputPadding={1}
    labelHeight={24}
    inputStyle={[styles.inputStyle__,]}
    labelStyle={styles.labelStyle}
    // inputStyle={[HideoStyle.input]}
    style={[styles.Input_container]}
    // this is used to set backgroundColor of label mask.
    // please pass the backgroundColor of your TextInput container.
   
    // value={AccountStore.loginInfo.email}
     onChangeText={(firstname) => AccountStore.onChangeText('firstname', firstname,'regInfo')}
    //  onChangeText={(firstname) => console.log(firstname)}

    returnKeyType={"next"}
    keyboardType={'default'}
  />
</View>
 
 
 <View style={styles.inputWrapper}>

  <Hoshi
    label={'Last name'}
    // this is used as active border color
    borderColor={'#948f8f'}
    // active border height
     borderHeight={0}
    inputPadding={1}
        inputStyle={[styles.inputStyle__,]}

    labelStyle={styles.labelStyle}
    // inputStyle={[HideoStyle.input]}
    style={[styles.Input_container]}
    // this is used to set backgroundColor of label mask.
    // please pass the backgroundColor of your TextInput container.
   
    // value={AccountStore.loginInfo.email}
    onChangeText={(lastname) => AccountStore.onChangeText('lastname', lastname,'regInfo')}
    
    returnKeyType={"next"}
    keyboardType={'default'}
  />
</View>



<View style={styles.inputWrapper}>

  <Hoshi
    label={'Email address'}
    // this is used as active border color
    borderColor={'#948f8f'}
    // active border height
     borderHeight={0}
    inputPadding={1}
        inputStyle={[styles.inputStyle__,]}

    labelStyle={styles.labelStyle}
    // inputStyle={[HideoStyle.input]}
    style={[styles.Input_container]}
    // this is used to set backgroundColor of label mask.
    // please pass the backgroundColor of your TextInput container.
   
    // value={AccountStore.loginInfo.email}
    onChangeText={(email) => AccountStore.onChangeText('email', email,'regInfo')}

    returnKeyType={"next"}
    keyboardType={'email-address'}
  />
</View>


<View style={[styles.inputWrapper,{flex:1,}]}>
<DatePicker 

           utcOffset={0}
           style={styles.datePickerContainer}
           date={this.state.ReminderDate}
           mode="date"    
           placeholder={'Date of birth'}
           format="YYYY-MM-DD"
          //  minDate={this.state.minyear}
           maxDate={this.state.ReminderDate}
           confirmBtnText="Confirm"
           cancelBtnText="Cancel"
           customStyles={{
             dateIcon: {
               position: 'absolute',
               left: 0,
               top: 4,
               marginLeft: 0
             },
             dateInput: {
               marginLeft: 36,
               borderTopWidth:0,
               borderLeftWidth:0,
               borderRightWidth:0,
             }
             // ... You can check the source to find the other keys.
           }}
           // date={this.state.endTime}
           onDateChange={(date) => {
             console.log(date);
             this.setState({ReminderDate: date
            
            })
            AccountStore.onChangeText('dob', date,'regInfo')
          }}
         />
  {/* <Hoshi
    label={'Date of birth'}
    // this is used as active border color
    borderColor={'#948f8f'}
    // active border height
     borderHeight={0}
    inputPadding={1}
        inputStyle={[styles.inputStyle__,]}

    labelStyle={{ color: '#000000', fontSize: 15, }}
    // inputStyle={[HideoStyle.input]}
    style={[styles.Input_container]}
    onChangeText={(dob) => AccountStore.onChangeText('dob', dob,'regInfo')}

    // this is used to set backgroundColor of label mask.
    // please pass the backgroundColor of your TextInput container.
   
    // value={AccountStore.loginInfo.email}
     
    returnKeyType={"next"}
    keyboardType={'default'}
  /> */}
</View>


<View style={styles.inputWrapper}>

  <Hoshi
    label={'Height'}
    // this is used as active border color
    borderColor={'#948f8f'}
    // active border height
     borderHeight={0}
    inputPadding={1}
        inputStyle={[styles.inputStyle__,]}

    labelStyle={styles.labelStyle}
    // inputStyle={[HideoStyle.input]}
    style={[styles.Input_container]}
    // this is used to set backgroundColor of label mask.
    // please pass the backgroundColor of your TextInput container.
   
    // value={AccountStore.loginInfo.email}
    // onChangeText=    onChangeText={(dob) => AccountStore.onChangeText('dob', dob,'regInfo')}
    onChangeText={(height) => AccountStore.onChangeText('height', height,'regInfo')}

    returnKeyType={"next"}
    keyboardType={'default'}
  />
</View>
<View style={styles.inputWrapper}>

  <Hoshi
    label={'Weight'}
    // this is used as active border color
    borderColor={'#948f8f'}
    // active border height
     borderHeight={0}
    inputPadding={1}
        inputStyle={[styles.inputStyle__,]}

    labelStyle={styles.labelStyle}
    // inputStyle={[HideoStyle.input]}
    style={[styles.Input_container]}
    // this is used to set backgroundColor of label mask.
    // please pass the backgroundColor of your TextInput container.
   
    // value={AccountStore.loginInfo.email}
    onChangeText={(weight) => AccountStore.onChangeText('weight', weight,'regInfo')}

    returnKeyType={"next"}
    keyboardType={'default'}
  />
</View>





<View style={[styles.inputWrapper,{flex:1,flexDirection:'row',marginTop:12,marginLeft:1,}]}>
<View style={{width:70,}}>
<Text style={styles.textstyle}>
Gender
</Text>
</View>
<TouchableOpacity
           onPress={() => {this.changeGenderValue('Male') }}

>
<View style={[styles.genderPill,genderview==='Male' ? styles.genderPill_selected:'']}>
<Text style={genderview==='Male' ? styles.textstyle_select:styles.textstyle}>
Male
</Text>
</View>
  
</TouchableOpacity>
<TouchableOpacity
          onPress={() => {this.changeGenderValue('Female') }}
 >
<View style={[styles.genderPill,genderview==='Female' ? styles.genderPill_selected:'']}>
<Text style={genderview==='Female' ? styles.textstyle_select:styles.textstyle}>
Female
</Text>
</View>

</TouchableOpacity>

 
</View>

<View style={[styles.inputWrapper,{marginTop:12,paddingLeft:1,}]}>

 


<MultiSelect
                  single
                  items={Genotypes}
                  uniqueKey="id"
                //   onSelectedItemsChange={onSelectedItemsChange}
                  onSelectedItemsChange={(value)=>this.onSelectedItemsChange('genotype',value)}

                  selectedItems={this.state.genotype}
                  selectText="Choose Genotype"
                  searchInputPlaceholderText="Search Items..."
                  tagRemoveIconColor="#CCC"
                  tagBorderColor="#CCC"
                  tagTextColor="#CCC"
                  selectedItemTextColor="#CCC"
                  selectedItemIconColor="#CCC"
                  itemTextColor="#000"
                  styleTextDropdownSelected={{fontSize:12,color:'#000'}}
                  styleRowList={{backgroundColor:'redk',}}
                  styleSelectorContainer={{heigh:120,backgroundColor:'orange',color:'green'}}
                  // styleDropdownMenuSubsection={{ borderBottomWidth: 0, borderColor: '#f1f1f1',backgroundColor:'red' }}
                  // styleMainWrapper={{ shadowColor: "#000",shadowOffset: { width: 0, height: 1,}, shadowOpacity: 0.1, shadowRadius: 1.00, elevation: 1, borderRadius: 8, paddingTop: 7, paddingLeft: 5, paddingRight: 5}}
                  searchInputStyle={{ color: '#000', }}
                  submitButtonColor="#CCC"
                  submitButtonText="Submit"
                />
</View>
<View style={[styles.inputWrapper,{marginTop:12,paddingLeft:1,}]}>

 


<MultiSelect
                  single
                  items={BloodGroup}
                  uniqueKey="id"
                //   onSelectedItemsChange={onSelectedItemsChange}
                  onSelectedItemsChange={(value)=>this.onSelectedItemsChange('bloodgroup',value)}

                  selectedItems={this.state.bloodgroup}
                  selectText="Choose  BloodGroup"
                  searchInputPlaceholderText="Search Items..."
                  tagRemoveIconColor="#CCC"
                  tagBorderColor="#CCC"
                  tagTextColor="#CCC"
                  selectedItemTextColor="#CCC"
                  selectedItemIconColor="#CCC"
                  itemTextColor="#000"
                  styleTextDropdownSelected={{fontSize:12,color:'#000'}}
                  styleRowList={{backgroundColor:'redk',}}
                  styleSelectorContainer={{heigh:120,backgroundColor:'orange',color:'green'}}
                  // styleDropdownMenuSubsection={{ borderBottomWidth: 0, borderColor: '#f1f1f1',backgroundColor:'red' }}
                  // styleMainWrapper={{ shadowColor: "#000",shadowOffset: { width: 0, height: 1,}, shadowOpacity: 0.1, shadowRadius: 1.00, elevation: 1, borderRadius: 8, paddingTop: 7, paddingLeft: 5, paddingRight: 5}}
                  searchInputStyle={{ color: '#000', }}
                  submitButtonColor="#CCC"
                  submitButtonText="Submit"
                />
</View>

 
 

 
<View style={{ marginTop: 20, }}>
  <View style={{ flexDirection: 'column', marginBottom: 15 }}>
    {/* <Button
      iconRight
      light
      block
      style={{ height:60,justifyContent: 'center', alignItems: 'center', borderRadius: 6, backgroundColor: '#0a0b0d' }}
    onPress={AccountStore.RegisterUserNow}
    >
      <Text style={{color:'#ffffff'}}>SUBMIT</Text>
    </Button> */}

    <SpinButton
      style={{ height:60,justifyContent: 'center', alignItems: 'center', borderRadius: 6, backgroundColor: '#0a0b0d' }}
      loaderState={AccountStore.isProcessing}
          buttonText='NEXT'
          buttonTextStyle={{ fontWeight:'bold', padding: 20, fontSize: 20,color:'#fff' }}
          onPressAction={ 
            AccountStore.RegisterUserNow
        }
         />

  </View>
</View>
 


</View>


    
         
         </View>
           </ScrollView>
        
        
       
         </View>  
        {/* </Content> */}
      </Container>
    );
  }
}
export default RegistrationPage;
const height = 40;

HideoStyle = {
  container: {
    // marginVertical: 5,
    // borderRadius: 4,
    // borderStyle: 'solid',
    // borderWidth: 0.9,
    // borderColor:'grey',

    // borderColor:'#357d83',
    // borderColor: '#727272',
    // borderColor:'#4ab1be',
    // backgroundColor: 'green',
    flex: 0,
    height: 'auto',
    overflow: 'hidden'
  },
  input: {
    height: height,
    backgroundColor: '#fff',
    fontWeight: '600',
    fontSize: 11,
    paddingBottom:0,
  },
  size: {
    input: height,
    icon: 20
  },
  inputstyle:{ 
    color: '#fff',
     fontSize: 20, 

    }
}
const styles = StyleSheet.create({
  Input_container: {
    // marginVertical: 5,
    // borderRadius: 4,
    // borderStyle: 'solid',
    // borderWidth: 0.9,
    // borderColor:'grey',

    // borderColor:'#357d83',
    // borderColor: '#727272',
    // borderColor:'#4ab1be',
    // backgroundColor: 'green',
    flex: 0,
    height: 'auto',
    overflow: 'hidden',
    borderTopWidth:0,
    borderLeftWidth:0,
  },
  datePickerContainer: {
    flex:1,
    width:'100%',
    marginTop:12,
    // borderTopWidth:0,
    // borderLeftWidth:0,
    // backgroundColor:'red'
  },
  inputStyle__:{ 
  color:'#948f8f',
  fontSize:14,
  fontWeight:'normal' ,
  paddingHorizontal: 0,
  paddingVertical: 0 ,
  textAlignVertical: 'bottom',
  // padding:-40,
 },
  genderPill:{ 
 borderColor:'#707070',
 borderRadius:25,
 borderWidth:1,
 alignItems:'center',
 width:100,
 marginRight:12,
 padding:4
 },
labelStyle:{ 
 color: '#000000', 
 fontSize: 15,
},
  genderPill_selected:{ 
backgroundColor:'#000',
shadowColor: "#000",
shadowOffset: {
	width: 0,
	height: 2,
},
shadowOpacity: 0.25,
shadowRadius: 3.84,

elevation: 5,

},
  HeaderText:{ 
  fontSize:20,
  fontWeight:"500",
},
  inputWrapper:{ 
marginTop:3,
},
  HeaderSubText:{ 
  fontSize:14,
  fontWeight:"normal",
},
ImageBorderWrap:{
width:120,
height:120,
borderRadius:120,
backgroundColor:'#0a0b0d',
justifyContent:'center',
alignItems:'center',
},
Image_InnerBorderWrap:{
width:60,
height:60,
borderRadius:60,
backgroundColor:'#ffffff',
justifyContent:'center',
alignItems:'center',
},
textstyle: {
  textAlign: 'center',
  // flex: 1,
  color: '#000000',
  textAlign:'left'
  // fontWeight: 'bold',
},
textstyle_select: {
  color:'#fff',
  // fontWeight:'bold',
},







form2: {
  marginLeft: 0,
},

text3: {
  color: '#4e4646',
  fontWeight: 'bold',
  padding: 0,
  margin: 0,
  marginLeft:12,

},
PhoneNumberStyle: {
  fontSize:18,
  fontWeight:'500',
  color:'#4e4646',
},

headerBody: {
  ...Platform.select({
      ios: {
        marginLeft:-150,
      },
      android: {
        marginLeft:-8,
      }
    }),
  
  },
input1: {
  // paddingTop: 4,
  marginTop: 19,
  borderColor: '#CCCCCC',
// borderTopWidth: 1,
borderBottomWidth: 1,
// borderLeftColor:'red',
// borderLeftWidth:1,
height: 50,
fontSize: 25,
// paddingLeft: 20,
paddingRight: 20,
color:'green'
},


PageWrapper: {
  // backgroundColor: 'rgba(0,0,0,0.4)',
  paddingTop: '1%',
  marginLeft: 'auto',
  marginRight: 'auto',
  width: '100%',
  backgroundColor:'#fff',
  paddingLeft:10,
  flex:1,
},


textstyle2: {
  textAlign: 'center',
  // flex: 1,
  color: '#fff',
  fontWeight: 'bold',
  marginLeft:-8,

},
BlackBackground:{ 
 
   backgroundColor:'#0a0b0d',
  },
WhiteBackground:{ 
 
   backgroundColor:'#ffffff',
   borderColor:'#707070',
   borderWidth:2,
   borderBottomColor:'red'
  //  height:20,
  },
Button_Style:{ 
justifyContent: 'center',
 alignItems: 'center',
 borderRadius:5,
 borderColor:'#707070' ,
 borderStyle:'solid'
 },
 contentSideWrap: {
  flex:1,
  // width: '100%',
   justifyContent: 'flex-end',
  //  flexDirection: 'column',
  // alignContent:'center',
  marginTop:40,
  marginRight:12
  // alignItems: 'center',

  // paddingTop: 150,
  // backgroundColor:'green',
},








  signupwithGoogle:{ 
  // justifyContent: 'center',
  // alignItems: 'center',
  borderRadius:5,
  backgroundColor:'#4589f1',
  // justifyContent:'center' 
paddingLeft:50,
// paddingRight:'auto',
// width:'80%',
},


  container: {
    flex: 1,
    alignItems: 'center',
  },
  iconstyle: {
    fontSize: 20,
    color: '#fff',
    // width: 40,
    // paddingLeft:50,
    alignItems:'flex-end',
    // alignSelf:'center',
    //  backgroundColor:'green',
     width:40,
    //  flex:1,
     justifyContent:'flex-end'
    
  },
  iconstyle_login: {
    fontSize: 10,
    color: '#fff',
    width: 40,
    paddingLeft:8,
    paddingTop:3,
    // alignItems:'center',
     
  },

  textstyle3: {
    textAlign: 'center',
    // flex: 1,
    color: '#fff',
    fontWeight: 'bold',
    fontSize:14,
    // backgroundColor:'green'

  },
  backgroundImage: {
    // height:null,
    flex: 1,
    width: null,
    // height: null,
    opacity: 2,
  },


 
  img: {
    height: 100,
    // borderRadius: 100,
    width: 100,
  },
  imgdown: {
    // flex: 1,
    aspectRatio: 0.7,
    width: 170,
    // height:170,
    resizeMode: 'contain',
    //  marginBottom:60,
    marginTop: -40,
  },
});

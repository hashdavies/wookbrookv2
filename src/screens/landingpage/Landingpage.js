import React, { Component } from 'react';
import { Text, StyleSheet, ImageBackground, Keyboard, TouchableOpacity } from 'react-native';
import { KeyboardAvoidingView, Image, ScrollView, Alert } from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons';
import {
  Container,
  Header,
  Content,
  Textarea,
  Button,
  View,
  Thumbnail,
  Form,
  Item,
  Input,
  Label,
  Badge,
  Icon,
  Footer,
  Picker,
  Item as FormItem,
} from 'native-base';
import {
  Kaede,
  Hoshi,
  Jiro,
  Isao,
  Madoka,
  Akira,
  Hideo,
  Kohana,
  Makiko,
  Sae,
  Fumi,
} from 'react-native-textinput-effects';
import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes,
} from 'react-native-google-signin';
// import { GoogleSignin } from '@react-native-community/google-signin';

import { myspiner, _signIn_with_Google, _signOut_GoogleAccount } from '../../dependency/UtilityFunctions';
import Logo from '../../assets/workbrookAssets/logo.png';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import RBSheet from "react-native-raw-bottom-sheet";


import AccountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import { SvgUri } from 'react-native-svg';


@observer
class Landing extends Component {
  constructor(props) {
    super(props);
    this.mapRef = null;
    this.state = {
      email: '',
      password: '',
      errorloading: false,
      userInfo: null,
      error: null,

    }
  }
  // componentDidMount() {
  // }
  async componentDidMount() {
    this._configureGoogleSignIn();
    // _signOut_GoogleAccount();
    // this._signOut();
    // await this._getCurrentUser();
    AccountStore.UpdateNavigationState(this.props.navigation);

  }
  _configureGoogleSignIn() {
    GoogleSignin.configure();
  }
  // {
  //   webClientId: config.webClientId,
  //   offlineAccess: false,
  // }
  async _getCurrentUser() {
    try {
      const userInfo = await GoogleSignin.signInSilently();
      console.log(userInfo);
      let email = userInfo["user"];
      console.log(email)
      this.setState({ userInfo, error: null });
    } catch (error) {
      const errorMessage =
        error.code === statusCodes.SIGN_IN_REQUIRED ? 'Please sign in :)' : error.message;
      this.setState({
        error: new Error(errorMessage),
      });
    }
  }
  render() {
    const { userInfo } = this.state;
    const { isProcessing, errorDisplay, fcmToken } = AccountStore;

    // const body = userInfo ? this.renderUserInfo(userInfo) : this.renderSignInButton();

    return (

      <ScrollView>


        {/* <View style={[styles.container, styles.pageContainer]}>

    {this.renderIsSignedIn()}

    {this.renderGetCurrentUser()}

    {this.renderGetTokens()}

    {body}
  </View> */}













        <Container style={styles.container}>
          {/* {myspiner(isProcessing)} */}

          <View style={styles.contentWrap}>

            {/* <Image source={{ uri: 'https://res.cloudinary.com/custocrypt/image/upload/v1585645693/workbrook/logo.png' }} style={{ height: 28, width: 166, }} /> */}
            <SvgUri
            width="166px"
            height="28"
              uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1590509137/Mobile_Assets/workbrook_ep9zfi.svg"
            />
            <Text style={styles.contentText}>
              Create an account and discover a world of
              opportunities, talents and verified professionals
              that will take your business to the next level.
          </Text>
            <Text style={{ color: 'red' }}>
              {errorDisplay.message}
            </Text>
            {/* <Text style={{color:'red'}}>
 {fcmToken.token}
</Text> */}
          </View>
          <View style={[styles.contentWrap, { paddingTop: 10 }]}>
            {/* <Button full style={[styles.loginBtn1, { backgroundColor: '#415793' }]}>
              <Text style={styles.buttonText}>
                Connect with Facebook
          </Text>
            </Button> */}
            <Button
              full
              style={[styles.loginBtn1, { backgroundColor: '#346BE6' }]}
              onPress={() => _signIn_with_Google()}
            >
              <Text style={styles.buttonText}>
                Connect with Google
          </Text>
            </Button>
            <Button full style={[styles.loginBtn1, { backgroundColor: '#0078B5' }]}
              onPress={() => {
                this.RBSheet.open();
              }}
            >
              <Text style={styles.buttonText}>
                Connect with LinkedIn
          </Text>
            </Button>
            <Button full style={[styles.loginBtn1, { backgroundColor: '#5ABC7A' }]}
              onPress={() => this.props.navigation.navigate('Signup')}
            >
              <Text style={styles.buttonText}>
                Sign Up with Email
          </Text>
            </Button>
            <Button full transparent style={[styles.loginBtn1, { backgroundColor: 'transparent', height: 30 }]}>
              <Text style={[styles.buttonText, { fontSize: 11, color: '#2F2F2F' }]}>
                By joining, you agree to Workbrook's
          </Text>
              <Text style={[styles.buttonText, { color: '#1E93E2', fontSize: 11, marginLeft: 2 }]}>
                Terms of Service
          </Text>
            </Button>
          </View>
          <View style={[styles.contentWrap, { justifyContent: 'center', alignItems: 'center', height: 40 }]}>
            <Button full transparent style={[styles.loginBtn1, { backgroundColor: 'transparent', height: 30, marginBottom: 0 }]}
              onPress={() => this.props.navigation.navigate("Login")}
            >
              <Text style={[styles.buttonText, { fontSize: 14, color: '#2F2F2F' }]}>
                Registered?
          </Text>
              <Text style={[styles.buttonText, { color: '#1E93E2', fontSize: 14, marginLeft: 2 }]}>
                Sign In
          </Text>
            </Button>
          </View>
        </Container>
        <RBSheet
          ref={ref => {
            this.RBSheet = ref;
          }}
          closeOnSwipeDown={true}
          // closeOnPressMask={true}
          duration={250}
          customStyles={{
            container: {
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: 'transparent',
              borderTopLeftRadius: 20,
              borderTopRightRadius: 20,
              height: '100%'
            }
          }}
        >
          <View style={styles.styleBtmSty}>

            <View style={[styles.styleBtmSty2, { position: 'relative' }]}>

              <View style={{ fontWeight: 'bold', fontSize: 14, }}>
                <Text style={{ fontWeight: 'bold', fontSize: 14, }}>LinkedIn Account</Text>
              </View >
              <View style={{ height: 250, width: '100%', overFlow: 'hidden' }} >
                <ScrollView style={{ width: '100%', height: '100%' }}>
                  <Item regular style={[styles.loginBtn1, { backgroundColor: '#E5E5E5', marginTop: 25 }]}>
                    <Input placeholderTextColor={'#BCBCBC'} style={styles.inputSty} placeholder='Email or Username' />
                  </Item>
                  <Item regular style={[styles.loginBtn1, { backgroundColor: '#E5E5E5' }]}>
                    <Input secureEntryInput={true} placeholderTextColor={'#BCBCBC'} style={styles.inputSty} placeholder='Password' />
                  </Item>
                  <Button full style={[styles.loginBtn1, { backgroundColor: '#5ABC7A', marginTop: 10 }]}
                    onPress={() => {
                      this.RBSheet.close();
                    }}
                  >
                    <Text style={styles.buttonText}>
                      Continue
          </Text>
                  </Button>
                </ScrollView>
              </View>
              <Button full style={[styles.loginBtn2, { position: 'absolute', top: -10, right: -10, backgroundColor: "#fff" }]}
                onPress={() => {
                  this.RBSheet.close();
                }}
              >
                <FontAwesome name="times-circle" style={{ fontSize: 30, color: '#1E93E2' }} />

              </Button>



            </View>
          </View>
        </RBSheet>

      </ScrollView>
    );
  }
  renderIsSignedIn() {
    return (
      <Button
        onPress={async () => {
          const isSignedIn = await GoogleSignin.isSignedIn();
          Alert.alert(String(isSignedIn));
        }}
        title="is user signed in?"
      />
    );
  }

  renderGetCurrentUser() {
    return (
      <Button
        onPress={async () => {
          const userInfo = await GoogleSignin.getCurrentUser();
          Alert.alert('current user', userInfo ? JSON.stringify(userInfo.user) : 'null');
        }}
        title="get current user"
      />
    );
  }

  renderGetTokens() {
    return (
      <Button
        onPress={async () => {
          const isSignedIn = await GoogleSignin.getTokens();
          Alert.alert('tokens', JSON.stringify(isSignedIn));
        }}
        title="get tokens"
      />
    );
  }

  renderUserInfo(userInfo) {
    return (
      <View style={styles.container}>
        <Text style={styles.userInfo}>Welcome {userInfo.user.name}</Text>
        <Text>Your user info: {JSON.stringify(userInfo.user)}</Text>
        {/* <TokenClearingView userInfo={userInfo} /> */}

        <Button onPress={this._signOut} title="Log out" />
        {this.renderError()}
      </View>
    );
  }

  renderSignInButton() {
    return (
      <View style={styles.container}>

        <GoogleSigninButton
          size={GoogleSigninButton.Size.Standard}
          color={GoogleSigninButton.Color.Auto}
          onPress={this._signIn}
        />
        {this.renderError()}
      </View>
    );
  }

  renderError() {
    const { error } = this.state;
    if (!error) {
      return null;
    }
    const text = `${error.toString()} ${error.code ? error.code : ''}`;
    return <Text>{text}</Text>;
  }

  //   _signIn = async () => {
  //     try {
  //       await GoogleSignin.hasPlayServices();
  // //       const isSignedIn = await GoogleSignin.isSignedIn();
  // //       if(isSignedIn){
  // // console.log("yes ")
  // //       }
  // //       else{
  // //         console.log("nooo ")
  // //       }
  //       const userInfo = await GoogleSignin.signIn();
  //       console.log(userInfo);
  //       let profile=userInfo["user"];
  //       let email=profile.email
  //       let familyName=profile.familyName
  //       let givenName=profile.givenName
  //       let UserName=profile.id
  //       let platform_id=profile.id
  //       let platform="Google"

  //       // value,Feild,parent
  //       // "username":"",
  //       // "email":"",
  //       // "platform_uid":"",
  //       // "platform":""

  //       AccountStore._updateParameter(platform_id,"platform_uid","payload_regInfo");
  //       AccountStore._updateParameter(platform,"platform","payload_regInfo");
  //       AccountStore._updateParameter(email,"email","payload_regInfo");
  //       AccountStore._updateParameter(platform_id,"username","payload_regInfo");
  //       AccountStore._updateParameter(UserName,"username","payload_regInfo");
  //       AccountStore._updateParameter(givenName,"first_name","payload_regInfo");
  //       AccountStore._updateParameter(familyName,"last_name","payload_regInfo");


  //       AccountStore._RegisterUserNow_SocialMedia();
  //       // console.log(email)
  //       // console.log(email)
  //       // console.log("userInfo");

  //       // photo: "https://lh3.googleusercontent.com/a-/AOh14GgP-fgNmr7R9LDL-DwVqHhu-pTTZw4_vIlWfW4FPQ"
  //       // email: "iyamalaika@gmail.com"
  //       // familyName: "David"
  //       // givenName: "Jinad"
  //       // name: "Jinad David"
  //       // id: "101496660815957053665"

  //       this.setState({ userInfo, error: null });
  //     }
  //      catch (error) {
  //        console.log(error)
  //       switch (error.code) {
  //         case statusCodes.SIGN_IN_CANCELLED:
  //           // sign in was cancelled
  //           Alert.alert('cancelled');
  //           break;
  //         case statusCodes.IN_PROGRESS:
  //           // operation (eg. sign in) already in progress
  //           Alert.alert('in progress');
  //           break;
  //         case statusCodes.PLAY_SERVICES_NOT_AVAILABLE:
  //           // android only
  //           Alert.alert('play services not available or outdated');
  //           break;
  //         default:
  //           Alert.alert('Something went wrong', error.toString());
  //           this.setState({
  //             error,
  //           });
  //       }
  //     }
  //   };

  //   _signOut = async () => {
  //     try {
  //       await GoogleSignin.revokeAccess();
  //       await GoogleSignin.signOut();

  //       this.setState({ userInfo: null, error: null });
  //     } catch (error) {
  //       this.setState({
  //         error,
  //       });
  //     }
  //   };


}



export default Landing;

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#fff",
    display: "flex",
    justifyContent: "space-between",
    paddingTop: 50,
    paddingRight: 30,
    paddingBottom: 35,
    paddingLeft: 30,
  },
  contentWrap: {
    // backgroundColor: "red",
    display: "flex",
  },
  contentText: {
    color: "#2F2F2F",
    fontSize: 14,
    marginTop: 20,
    textAlign: 'justify',
  },
  loginBtn1: {
    height: 56,
    borderRadius: 5,
    marginBottom: 15,
    elevation: 0,
  },
  loginBtn2: {
    height: 30,
    width: 30,
    borderRadius: 30 / 2,
    elevation: 1,
    backgroundColor: 'transparent',
    // backgroundColor: '#1E93E2',
  },
  buttonText: {
    color: "#FAFAFA",
    fontSize: 14,
    fontWeight: "600",
  },
  styleBtmSty: {
    width: '100%',
    padding: 15,
    flex: 1,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  styleBtmSty2: {
    width: '90%',
    // height: '70%',
    padding: 10,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    borderRadius: 6,
  },
  inputSty: {
    fontSize: 14,
    fontWeight: "300",
  },

});

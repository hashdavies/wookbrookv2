import React, { Component, Fragment } from 'react';
import { Text, StyleSheet, ImageBackground, Keyboard, TouchableOpacity } from 'react-native';
import { KeyboardAvoidingView, Image, ScrollView } from 'react-native';
import ToggleSwitch from 'toggle-switch-react-native'
import Ionicons from 'react-native-vector-icons/Ionicons';
import Checker from '../../assets/workbrookAssets/checker.png';
import DatePicker from 'react-native-datepicker'
import { Col, Row, Grid } from "react-native-easy-grid";
import {
    Container,
    Header,
    Content,
    Textarea,
    Button,
    View,
    Thumbnail,
    Form,
    Item,
    Input,
    Label,
    Badge,
    Icon,
    Footer,
    Picker,
    Item as FormItem,
    Left,
    Body,
    CardItem,
    Card,
    Right,
    ListItem,
    List,

} from 'native-base';
import {
    Kaede,
    Hoshi,
    Jiro,
    Isao,
    Madoka,
    Akira,
    Hideo,
    Kohana,
    Makiko,
    Sae,
    Fumi,
} from 'react-native-textinput-effects';
import CustomizeHeader from '../frags/CustomizeHeader';

import { myspiner } from '../../dependency/UtilityFunctions';
import Logo from '../../assets/workbrookAssets/logo.png';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import RBSheet from "react-native-raw-bottom-sheet";
import Interests from '../interests/Interests';
import Noti from "../../assets/workbrookAssets/noti.png";
import UserBl from "../../assets/workbrookAssets/userBl.png";
import ArrForward from "../../assets/workbrookAssets/arrfr.png";
import Shield from "../../assets/workbrookAssets/shield.png";
import Quest from "../../assets/workbrookAssets/quest.png";
import Info from "../../assets/workbrookAssets/info.png";
import Plus from "../../assets/workbrookAssets/plus.png";
import Remark from "../../assets/workbrookAssets/remark.png";
import Like from "../../assets/workbrookAssets/like.png";
import MoreIcon from "../../assets/workbrookAssets/more2.png";
import Profile from "../../assets/images/profile/dp2.png";
import cardPix from "../../assets/images/background/bg3_old.jpg";
import RNPickerSelect from 'react-native-picker-select';

import AccountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import { text } from 'react-native-communications';
import DashboardStore from '../../stores/Dashboard';
import { hasSetIntrest } from '../../auth';


@observer
class AccountSettings extends Component {
    constructor(props) {
        super(props);
        this.mapRef = null;
        this.state = { chosenDate: new Date() };
        this.setDate = this.setDate.bind(this);
        this.state = {
            email: '',
            password: '',
            errorloading: false,
            isOn: true,
            minyear: "",
            maxyear: "",
            ReminderDate: null,
        }
        // this.SetCheckers = this.SetCheckers.bind(this)
        this.toggleMe = this.toggleMe.bind(this)
        this.proceednow = this.proceednow.bind(this)
    }
    toggleMe = () => {
        this.setState({ isOn: !this.state.isOn });
    };
    async componentDidMount() {
        // let status = await hasSetIntrest();
        // // alert(status)
        // DashboardStore._UpdateUserSetIntrest(status);
        const { isShowOnProfile } = DashboardStore.profileupdatedata;
        let showstatus = isShowOnProfile == 1 ? true : false;
        this.setState({
            isOn: showstatus
        })
        DashboardStore.UpdateNavigationState(this.props.navigation);
        this.ShowCurrentDate();

    }
    ShowCurrentDate = () => {
        let date = new Date().getDate();
        let month = new Date().getMonth() + 1;
        let year = new Date().getFullYear();
        if (date < 10) {
            date = `0${date}`;
        }
        if (month < 10) {
            month = `0${month}`;
        }
        let yearmax = year + 1;
        // let currentdate = date + '-' + month + '-' + year;
        let minyear = year + '-' + month + '-' + date;
        let maxyear = year + '-' + month + '-' + date;
        //   this.setState({minyear: minyear})
        this.setState({ ReminderDate: minyear })
        this.setState({ maxyear: maxyear })
        // Alert.alert(date + '-' + month + '-' + year);

    }

    setDate(newDate) {
        console.log(newDate)
        this.setState({ chosenDate: newDate });
    }

    proceednow = () => {

        if (DashboardStore.profileupdatedata.gender === '') {
            ShowNotification('Gender is missing', true);
        }

        else {
            const { isOn } = this.state;
            let on = parseInt(1);
            let off = parseInt(0);
            let showprofile = isOn === true ? on : off;
            DashboardStore._updateParameterV2(showprofile, "isShowOnProfile", "profileupdatedata");
            DashboardStore._updateParameterV2("AccountSettings", "CurrentRedirect", "RedirectMode");
            // DashboardStore._updateParameterV2(false, "withdrawApplication", "RedirectMode");

            DashboardStore.__UpdateProfile_external();
        }
        

    }
    render() {
        const { isGoBack, RedirectMode } = DashboardStore;
        if (RedirectMode.AccountSettings === true) {
            let _CurrentRedirect=RedirectMode.CurrentRedirect;
                 DashboardStore._updateParameterV2(false, _CurrentRedirect, "RedirectMode");
                 DashboardStore._ResetRedirectMode2Default();
        // DashboardStore._updateParameterV2("David_Nill", "CurrentRedirect", "RedirectMode");
        this.props.navigation.goBack();

        }



        return (
            <Container style={styles.container}>
                {myspiner(DashboardStore.isProcessing)}
                <CustomizeHeader
                    leftside='WhiteArr'
                    title='Account'
                    rightside='save'
                    statedColor='#1E93E2'
                    icon='md-close'
                    savehandler={this.proceednow}
                />

                <ScrollView>

                    <CardItem style={[{ backgroundColor: 'transparent' }]}>
                        <Left style={{ alignItems: 'center' }}>
                            <View>
                                <Text style={styles.cardTextJob8}>
                                    Personal Information
                                    </Text>
                                <Text style={styles.cardTextJob9}>
                                    Kindly provide your personal information. Note that this
                                    information will not be visible publicly unless
                                    you choose to make it public.
                                    </Text>
                            </View>

                        </Left>
                    </CardItem>
                    <View style={[{ padding: 0, backgroundColor: '#FFFFFF' }]}>
                        <CardItem style={[{ backgroundColor: 'transparent', width: '100%' }]}>
                            <Item stackedLabel style={[{ backgroundColor: 'transparent', width: '100%' }]}>
                                <Label style={styles.labelSty}>Email</Label>
                                <Input
                                    style={styles.cardTextJob3_1}
                                    defaultValue={DashboardStore.profileupdatedata.email}
                                //  onChangeText={(first_name) => DashboardStore.onChangeText('first_name', first_name,'profileupdatedata')}

                                />
                            </Item>
                        </CardItem>
                        <CardItem style={[{ backgroundColor: 'transparent' }]}>

                            <Item stackedLabel style={[{ backgroundColor: 'transparent', width: '100%' }]}>
                                <Label style={styles.labelSty}>Phone Number</Label>
                                <Input
                                    style={styles.cardTextJob3_1}
                                    style={styles.inputSty1}
                                defaultValue={DashboardStore.profileupdatedata.phone}
                                 onChangeText={(phone) => DashboardStore.onChangeText('phone', phone,'profileupdatedata')}

                                />
                            </Item>
                        </CardItem>
                        <CardItem style={[{ backgroundColor: 'transparent' }]}>

                            <View stackedLabel style={[{ borderBottomWidth: 1, marginTop: 10, width: '100%', borderBottomColor: '#E5E5E5' }]}>
                                <Label style={styles.labelSty}>Gender</Label>
                                <RNPickerSelect
                                    onValueChange={(value) => {

                                        console.log(value)
                                        DashboardStore._updateParameterV2(value, "gender", "profileupdatedata");

                                    }}
                                    value={DashboardStore.profileupdatedata.gender}
                                    items={[
                                        { label: 'Male', value: "1" },
                                        { label: 'Female', value: "2" },
                                    ]}
                                />

                            </View>
                        </CardItem>
                        <CardItem style={[{ backgroundColor: 'transparent' }]}>

                            {/* <View stackedLabel style={[{ borderBottomWidth: 1, marginTop: 10, width: '100%', borderBottomColor: '#E5E5E5' }]}> */}
                                {/* <Label style={styles.labelSty}>Birthday</Label> */}
                                {/* <Grid>
                                    <Col>
                                    <RNPickerSelect
                                     placeholder={{
                                        label: 'DD',
                                        color: '#A7A7A7',
                                        fontSize: 11,
                                      }}
                                      onValueChange={(value) => console.log(value)}
                                        items={[
                                            { label: '17', value: 1 },
                                            { label: '18', value: 2 },
                                        ]}
                                        style={{
                                           
                                            iconContainer: {
                                              top: 20,
                                              right: 10,
                                            },
                                            placeholder: {
                                              fontSize: 11,
                                              color: '#2F2F2F',
                                            },
                                          }}
                                          />
                                    
                                    </Col>
                                    <Col>
                                    <RNPickerSelect
                                    placeholder={{
                                        label: 'MM',
                                        color: '#A7A7A7',
                                        fontSize: 11,
                                      }}
                                      onValueChange={(value) => console.log(value)}
                                        items={[
                                            { label: '10', value: 1 },
                                            { label: '11', value: 2 },
                                            { label: '12', value: 3 },
                                        ]}
                                        style={{
                                           
                                            iconContainer: {
                                              top: 20,
                                              right: 10,
                                            },
                                            placeholder: {
                                              fontSize: 11,
                                              color: '#2F2F2F',
                                            },
                                          }}
                                />
                                    </Col>
                                    <Col>
                                    <RNPickerSelect
                                   placeholder={{
                                    label: 'YYYY',
                                    color: '#A7A7A7',
                                    fontSize: 11,
                                  }}
                                  onValueChange={(value) => console.log(value)}
                                    items={[
                                        { label: '2020', value: 1 },
                                        { label: '1993', value: 2 },
                                    ]}
                                    style={{
                                       
                                        iconContainer: {
                                          top: 20,
                                          right: 10,
                                        },
                                        placeholder: {
                                          fontSize: 11,
                                          color: '#2F2F2F',
                                        },
                                      }}
                                />
                                    </Col>
                                   
                                </Grid> */}
                                
                                {/* <DatePicker
                                    utcOffset={0}
                                    style={{ width: 200, flex: 1, }}
                                    date={this.state.ReminderDate}
                                    mode="date"
                                    placeholder={this.state.ReminderDate}
                                    format="YYYY-MM-DD"
                                    showIcon={false}
                                    //    minDate={this.state.minyear}
                                    maxDate={this.state.maxyear}

                                    confirmBtnText="Confirm"
                                    cancelBtnText="Cancel"
                                    customStyles={{
                                        dateIcon: {
                                            position: 'absolute',
                                            left: 0,
                                            top: 4,
                                            marginLeft: 0
                                        },
                                        dateInput: {
                                            marginLeft: 36
                                        }
                                        // ... You can check the source to find the other keys.
                                    }}
                                    // date={this.state.endTime}
                                    onDateChange={(date) => {
                                        console.log(date);
                                        this.setState({
                                            ReminderDate: date

                                        })
                                        DashboardStore._updateParameterV2(date, "dob", "profileupdatedata");

                                    }}
                                /> */}
                            {/* </View> */}
                        </CardItem>
                     
                        <CardItem style={[{ backgroundColor: 'transparent' }]}>
                            <Left style={{ alignItems: 'center' }}>
                                <Text style={styles.cardTextJob4_1}>
                                    Show on profile
                                    </Text>
                            </Left>
                            <Right>
                                <ToggleSwitch
                                    isOn={this.state.isOn}
                                    onColor="#5ABC7A"
                                    offColor="#BCBCBC"
                                    // label="Example label"
                                    // labelStyle={{ color: "black", fontWeight: "900" }}
                                    size="medium"
                                    onToggle={this.toggleMe}
                                />
                            </Right>
                        </CardItem>
                    </View>
                </ScrollView>
            </Container>


        );
    }
}
export default AccountSettings;

const styles = StyleSheet.create({
    container: {
        backgroundColor: "#FFF",
        display: "flex",
        flex: 1,
        justifyContent: "flex-start",
    },
    IconSetsty1: {
        maxWidth: 16,
        maxHeight: 17,
    },
    IconSetsty: {
        maxWidth: 16,
        maxHeight: 16,
    },
    iconWrapSet: {
        width: 20,
        marginRight: 10
    },
    IconSetsty2: {
        width: 8,
        height: 15,

    },
    topBlue: {
        // padding: 15,
        height: 106,
        backgroundColor: "#1E93E2",
        display: 'flex',
        justifyContent: 'center',
    },
    rmPadding: {
        paddingTop: 0,
        paddingRight: 0,
        paddingBottom: 0,
        paddingLeft: 0,
    },
    rmPaddingLR: {
        paddingLeft: 0,
        paddingRight: 0,
    },

    cardTextJob1: {
        color: "#2F2F2F",
        fontSize: 11,
    },
    cardTextJob2: {
        color: "#676767",
        fontSize: 9,
        fontWeight: '300',
    },
    cardTextJob3: {
        color: "#2F2F2F",
        fontSize: 10,
        fontWeight: '500',
    },
    cardTextJob3_1: {
        color: "#2F2F2F",
        fontSize: 11,
        fontWeight: '300',
        marginBottom: 0,
    },
    cardTextJob4: {
        color: "#1E93E2",
        fontSize: 11,
        fontWeight: '500',
    },
    cardTextJob4_1: {
        color: "#2F2F2F",
        fontSize: 14,
        fontWeight: 'normal',
    },
    profileinput: {
        marginLeft: 0,
        borderBottomColor: '#E5E5E5',
        height: 70,
        // marginBottom: 15
    },
    cardTextJob5: {
        color: "#1E93E2",
        fontSize: 13,
        fontWeight: '600',
    },
    cardTextJob7: {
        fontSize: 12,
        color: '#2F2F2F',
    },
    cardTextJob8: {
        fontSize: 14,
        color: '#2F2F2F',
        fontWeight: '500',
    },
    cardTextJob9: {
        fontSize: 11,
        color: '#A7A7A7',
        fontWeight: '300',
    },
    inputSty1: {
        height: 40,
    },
    labelSty: {
        color: "#2F2F2F",
        fontSize: 11,
        fontWeight: '500'
    },



});

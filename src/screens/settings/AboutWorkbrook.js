import React, { Component, Fragment } from 'react';
import { Text, StyleSheet, ImageBackground, Keyboard, TouchableOpacity } from 'react-native';
import { KeyboardAvoidingView, Image, ScrollView } from 'react-native';
import ToggleSwitch from 'toggle-switch-react-native'
import Ionicons from 'react-native-vector-icons/Ionicons';
import Checker from '../../assets/workbrookAssets/checker.png';
import {
    Container,
    Header,
    Content,
    Textarea,
    Button,
    View,
    Thumbnail,
    Form,
    Item,
    Input,
    Label,
    Badge,
    Icon,
    Footer,
    Picker,
    Item as FormItem,
    Left,
    Body,
    CardItem,
    Card,
    Right,
    ListItem,
    List,
} from 'native-base';
import {
    Kaede,
    Hoshi,
    Jiro,
    Isao,
    Madoka,
    Akira,
    Hideo,
    Kohana,
    Makiko,
    Sae,
    Fumi,
} from 'react-native-textinput-effects';
import CustomizeHeader from '../frags/CustomizeHeader';

import { myspiner } from '../../dependency/UtilityFunctions';
import Logo from '../../assets/workbrookAssets/logo.png';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import RBSheet from "react-native-raw-bottom-sheet";
import Interests from '../interests/Interests';
import Noti from "../../assets/workbrookAssets/noti.png";
import UserBl from "../../assets/workbrookAssets/userBl.png";
import ArrForward from "../../assets/workbrookAssets/arrfr.png";
import Shield from "../../assets/workbrookAssets/shield.png";
import Quest from "../../assets/workbrookAssets/quest.png";
import Info from "../../assets/workbrookAssets/info.png";
import Plus from "../../assets/workbrookAssets/plus.png";
import Remark from "../../assets/workbrookAssets/remark.png";
import Like from "../../assets/workbrookAssets/like.png";
import MoreIcon from "../../assets/workbrookAssets/more2.png";
import Profile from "../../assets/images/profile/dp2.png";
import cardPix from "../../assets/images/background/bg3_old.jpg";

import AccountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import { text } from 'react-native-communications';
import DashboardStore from '../../stores/Dashboard';
import { hasSetIntrest } from '../../auth';


@observer
class AboutWorkbrook extends Component {
    constructor(props) {
        super(props);
        this.mapRef = null;
        this.state = {
            email: '',
            password: '',
            errorloading: false,
            isOn: true,
        }
        // this.SetCheckers = this.SetCheckers.bind(this)
        this.toggleMe = this.toggleMe.bind(this)
    }
    toggleMe = () => {
        this.setState({ isOn: !this.state.isOn });
    };
    async componentDidMount() {
        let status = await hasSetIntrest();
        // alert(status)
        DashboardStore._UpdateUserSetIntrest(status);

    }

    render() {
        return (
            <Container style={styles.container}>
                <CustomizeHeader
                    leftside='WhiteArr'
                    title='About Workbrook'
                    rightside='empty'
                    statedColor='#1E93E2'
                    icon='md-close'
                    savehandler={this.proceednow}
                />

                <ScrollView>
                    <CardItem style={[{ backgroundColor: 'transparent' }]}>
                        <Left style={{ alignItems: 'center' }}>
                            <View>
                                <Text style={styles.cardTextJob8}>
                                About workbrook
                                </Text>
                                <Text style={styles.cardTextJob9}>
                                No work is left undone anywhere in the world. workbrook gives you an opportunity to share your work needs with people across the world, share opportunities with your contacts on the leading social networking platforms and open a door of opportunities to your next big move!  workbrook matches extraordinary people with great opportunities! 

                                  
                                </Text>
                            </View>

                        </Left>
                    </CardItem>
                    <View style={[{ padding: 0, backgroundColor: '#FFFFFF' }]}>
                        <TouchableOpacity style={[{ backgroundColor: 'transparent' }]}
                        // onPress={() => { this.props.navigation.navigate("SecuritySettigs") }}
                        >
                            <CardItem style={[{ backgroundColor: 'transparent' }]}>
                                <Left style={{ alignItems: 'center' }}>
                                    <Text style={styles.cardTextJob8}>
                                        Data Policy
                                    </Text>
                                </Left>
                                <Right>
                                    <Image source={ArrForward} style={styles.IconSetsty2} />
                                </Right>
                            </CardItem>
                        </TouchableOpacity>
                        <TouchableOpacity style={[{ backgroundColor: 'transparent' }]}
                        // onPress={() => { this.props.navigation.navigate("SecuritySettigs") }}
                        >
                            <CardItem style={[{ backgroundColor: 'transparent' }]}>
                                <Left style={{ alignItems: 'center' }}>
                                    <Text style={styles.cardTextJob8}>
                                        Terms of Use
                                    </Text>
                                </Left>
                                <Right>
                                    <Image source={ArrForward} style={styles.IconSetsty2} />
                                </Right>
                            </CardItem>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </Container>


        );
    }
}
export default AboutWorkbrook;

const styles = StyleSheet.create({
    container: {
        backgroundColor: "#FFF",
        display: "flex",
        flex: 1,
        justifyContent: "flex-start",
    },
    IconSetsty1: {
        maxWidth: 16,
        maxHeight: 17,
    },
    IconSetsty: {
        maxWidth: 16,
        maxHeight: 16,
    },
    iconWrapSet: {
        width: 20,
        marginRight: 10
    },
    IconSetsty2: {
        width: 8,
        height: 15,

    },
    topBlue: {
        // padding: 15,
        height: 106,
        backgroundColor: "#1E93E2",
        display: 'flex',
        justifyContent: 'center',
    },
    rmPadding: {
        paddingTop: 0,
        paddingRight: 0,
        paddingBottom: 0,
        paddingLeft: 0,
    },
    rmPaddingLR: {
        paddingLeft: 0,
        paddingRight: 0,
    },

    cardTextJob1: {
        color: "#2F2F2F",
        fontSize: 11,
    },
    cardTextJob2: {
        color: "#676767",
        fontSize: 9,
        fontWeight: '300',
    },
    cardTextJob3: {
        color: "#2F2F2F",
        fontSize: 10,
        fontWeight: '500',
    },
    cardTextJob4: {
        color: "#1E93E2",
        fontSize: 11,
        fontWeight: '500',
    },
    cardTextJob4_1: {
        color: "#2F2F2F",
        fontSize: 14,
        fontWeight: 'normal',
    },
    cardTextJob5: {
        color: "#1E93E2",
        fontSize: 13,
        fontWeight: '600',
    },
    cardTextJob7: {
        fontSize: 12,
        color: '#2F2F2F',
    },
    cardTextJob8: {
        fontSize: 14,
        color: '#2F2F2F',
        fontWeight: '500',
    },
    cardTextJob9: {
        fontSize: 11,
        color: '#A7A7A7',
        fontWeight: '300',
    },
    inputSty1: {
        height: 40,
    },



});

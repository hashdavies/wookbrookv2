import React, { Component, Fragment } from 'react';
import { Text, StyleSheet, ImageBackground, Keyboard, TouchableOpacity } from 'react-native';
import { KeyboardAvoidingView, Image, ScrollView } from 'react-native';
import ToggleSwitch from 'toggle-switch-react-native'
import Ionicons from 'react-native-vector-icons/Ionicons';
import Checker from '../../assets/workbrookAssets/checker.png';
import {
    Container,
    Header,
    Content,
    Textarea,
    Button,
    View,
    Thumbnail,
    Form,
    Item,
    Input,
    Label,
    Badge,
    Icon,
    Footer,
    Picker,
    Item as FormItem,
    Left,
    Body,
    CardItem,
    Card,
    Right,
    ListItem,
    List,
} from 'native-base';
import {
    Kaede,
    Hoshi,
    Jiro,
    Isao,
    Madoka,
    Akira,
    Hideo,
    Kohana,
    Makiko,
    Sae,
    Fumi,
} from 'react-native-textinput-effects';
import CustomizeHeader from '../frags/CustomizeHeader';

import { myspiner } from '../../dependency/UtilityFunctions';
import Logo from '../../assets/workbrookAssets/logo.png';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import RBSheet from "react-native-raw-bottom-sheet";
import Interests from '../interests/Interests';
import Noti from "../../assets/workbrookAssets/noti.png";
import UserBl from "../../assets/workbrookAssets/userBl.png";
import ArrForward from "../../assets/workbrookAssets/arrfr.png";
import Shield from "../../assets/workbrookAssets/shield.png";
import Quest from "../../assets/workbrookAssets/quest.png";
import Info from "../../assets/workbrookAssets/info.png";
import Plus from "../../assets/workbrookAssets/plus.png";
import Remark from "../../assets/workbrookAssets/remark.png";
import Like from "../../assets/workbrookAssets/like.png";
import MoreIcon from "../../assets/workbrookAssets/more2.png";
import Profile from "../../assets/images/profile/dp2.png";
import cardPix from "../../assets/images/background/bg3_old.jpg";
import { SvgUri } from 'react-native-svg';
import AccountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import { text } from 'react-native-communications';
import DashboardStore from '../../stores/Dashboard';
import { hasSetIntrest } from '../../auth';
import Modal from 'react-native-modal';


@observer
class SecuritySettigs extends Component {
    constructor(props) {
        super(props);
        this.mapRef = null;
        this.state = {
            email: '',
            password: '',
            errorloading: false,
            isOn: true,
            SuccesChange: false,
        }
        // this.SetCheckers = this.SetCheckers.bind(this)
        this.toggleMe = this.toggleMe.bind(this)
        this.changepassword = this.changepassword.bind(this)
        this.toggleSuccessChange = this.toggleSuccessChange.bind(this)
    }
    toggleMe = () => {
        this.setState({ isOn: !this.state.isOn });
    };
    toggleSuccessChange = () => {
        this.setState({ SuccesChange: !this.state.SuccesChange });
    };
    changepassword = () => {
        // this.setState({ isOn: !this.state.isOn });
        DashboardStore.__ChangePassword();
    };
    async componentDidMount() {
        let status = await hasSetIntrest();
        // alert(status)
        DashboardStore._UpdateUserSetIntrest(status);

    }

    render() {
        const { isGoBack } = DashboardStore;
        if (isGoBack === true) {
            this.props.navigation.goBack();
            DashboardStore._UpdateDefaultValue();
        }
        return (

            <Container style={styles.container}>
                <CustomizeHeader
                    leftside='WhiteArr'
                    title='Account'
                    rightside='empty'
                    statedColor='#1E93E2'
                    icon='md-close'
                    savehandler={this.proceednow}
                />

                <ScrollView style={{ paddingBottom: 20, }}>
                    <CardItem style={[{ paddingBottom: 0 }]}>
                        <Left >
                            <View>
                                <Text style={styles.cardTextJob8}>
                                    Login Security
                                    </Text>
                                <Text style={[styles.notices, { marginTop: 37, }]}>Required fields are marked *
                                    </Text>
                            </View>
                        </Left>
                    </CardItem>
                    <View style={[{ padding: 0, backgroundColor: '#FFFFFF', paddingBottom: 30 }]}>
                        <Item stackedLabel style={[styles.FailedBottomIndBorder, { height: 55 }]}>
                            <Label style={styles.failedlabelSty}>Current password<Text style={styles.asteriskSty}>*</Text></Label>
                            <Input
                                placeholder="Current password"
                                placeholderLabel
                                placeholderTextColor="#A7A7A7"
                                style={[styles.cardTextJob3_1,]}
                                defaultValue={DashboardStore.ChangePassword.currentpassword}
                                onChangeText={(currentpassword) => DashboardStore.onChangeText('currentpassword', currentpassword, 'ChangePassword')}

                            />

                        </Item>
                        <Item stackedLabel style={[styles.bottomIndBorder, { height: 55 }]}>
                            <Label style={styles.labelSty}>Enter New Password<Text style={styles.asteriskSty}>*</Text></Label>
                            <Input
                                placeholder="New password"
                                placeholderLabel
                                placeholderTextColor="#A7A7A7"
                                style={[styles.cardTextJob3_1,]}
                                defaultValue={DashboardStore.ChangePassword.newpassword}
                                onChangeText={(newpassword) => DashboardStore.onChangeText('newpassword', newpassword, 'ChangePassword')}

                            />

                        </Item>
                        <Item stackedLabel style={[styles.bottomIndBorder, { height: 55 }]}>
                            <Label style={styles.labelSty}>Confirm New Password<Text style={styles.asteriskSty}>*</Text></Label>
                            <Input
                                placeholder="Confirm New Password"
                                placeholderLabel
                                placeholderTextColor="#A7A7A7"
                                style={[styles.cardTextJob3_1,]}
                                defaultValue={DashboardStore.ChangePassword.confirmpasssword}
                                onChangeText={(confirmpasssword) => DashboardStore.onChangeText('confirmpasssword', confirmpasssword, 'ChangePassword')}

                            />

                        </Item>
                        <View style={{ padding: 15 }}>
                            <Button style={[styles.copyVisitBtn, { backgroundColor: '#5ABC7A' }]}
                                // onPress={this.toggleSuccessChange}
                            onPress={this.changepassword}
                            >
                                <Text style={[styles.Text3, { color: '#fff' }]}>
                                    {DashboardStore.isProcessing ? "Processing..." : ' Change Login'}
                                </Text>
                            </Button>
                        </View>
                    </View>
                </ScrollView>


                <Modal style={styles.MContainer} isVisible={this.state.SuccesChange} >
                    <Button onPress={this.toggleSuccessChange} style={{ backgroundColor: "transparent", position: 'absolute', right: 10, top: 40, padding: 20, elevation: 0 }}>
                        <SvgUri
                            width="19px"
                            height="19px"
                            uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593711047/Mobile_Assets/closeWht_oohxti.svg" />

                    </Button>
                    <View style={[styles.Mwrapper, { height: 380 }]}>
                        <View style={{ paddingLeft: 29, paddingRight: 29, display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                            <SvgUri
                                width="129px"
                                height="129px"
                                uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593736755/Mobile_Assets/success_y53teq.svg" />

                            <Text style={styles.cardText1, { fontSize: 16, color: '#5ABC7A', textAlign: 'center', fontWeight: '600', marginTop: 17 }}>
                                Congratulations!
                                        </Text>
                            <Text style={styles.cardText1, { fontSize: 16, color: '#5ABC7A', textAlign: 'center', fontWeight: '600' }}>
                                You have successfully changed
                                your password.
                            </Text>
                        </View>

                    </View>
                </Modal>
            </Container>


        );
    }
}
export default SecuritySettigs;

const styles = StyleSheet.create({
    container: {
        backgroundColor: "#FFFFFF",
        display: "flex",
        // flex: 1,
        justifyContent: "flex-start",
    },
    IconSetsty1: {
        maxWidth: 16,
        maxHeight: 17,
    },
    IconSetsty: {
        maxWidth: 16,
        maxHeight: 16,
    },
    iconWrapSet: {
        width: 20,
        marginRight: 10
    },
    IconSetsty2: {
        width: 8,
        height: 15,

    },
    topBlue: {
        // padding: 15,
        height: 106,
        backgroundColor: "#1E93E2",
        display: 'flex',
        justifyContent: 'center',
    },
    rmPadding: {
        paddingTop: 0,
        paddingRight: 0,
        paddingBottom: 0,
        paddingLeft: 0,
    },
    rmPaddingLR: {
        paddingLeft: 0,
        paddingRight: 0,
    },

    cardTextJob1: {
        color: "#2F2F2F",
        fontSize: 11,
    },
    cardTextJob2: {
        color: "#676767",
        fontSize: 9,
        fontWeight: '300',
    },
    cardTextJob3: {
        color: "#2F2F2F",
        fontSize: 10,
        fontWeight: '500',
    },
    cardTextJob3_1: {
        color: "#2F2F2F",
        fontSize: 11,
        fontWeight: '300',
        marginBottom: 0,
    },
    cardTextJob4: {
        color: "#1E93E2",
        fontSize: 11,
        fontWeight: '500',
    },
    cardTextJob4_1: {
        color: "#2F2F2F",
        fontSize: 14,
        fontWeight: 'normal',
    },
    cardTextJob5: {
        color: "#1E93E2",
        fontSize: 13,
        fontWeight: '600',
    },
    cardTextJob7: {
        fontSize: 12,
        color: '#2F2F2F',
    },
    cardTextJob8: {
        fontSize: 14,
        color: '#2F2F2F',
        fontWeight: '500',
    },
    cardTextJob9: {
        fontSize: 11,
        color: '#A7A7A7',
        fontWeight: '300',
    },
    inputSty1: {
        height: 40,
    },
    bottomIndBorder: {
        marginLeft: 15,
        marginRight: 15,
        borderBottomWidth: 1,
        borderColor: '#E5E5E5',
        marginTop: 15,
    },
    FailedBottomIndBorder: {
        marginLeft: 15,
        marginRight: 15,
        borderBottomWidth: 1,
        borderColor: '#FF5964',
    },
    copyVisitBtn: {
        height: 44,
        width: '100%',
        borderRadius: 4,
        borderWidth: 1,
        borderColor: '#5ABC7A',
        display: 'flex',
        justifyContent: 'center',
        marginBottom: 6,
        alignItems: 'center',
        elevation: 0
    },
    Text3: {
        fontSize: 12,
        fontWeight: '500',
        color: "#FAFAFA",
        textAlign: 'center'
    },
    notices: {
        color: "#FF5964",
        fontSize: 10,
        fontWeight: '300',
    },
    labelSty: {
        color: "#2F2F2F",
        fontSize: 11,
        fontWeight: '500'
    },
    failedlabelSty: {
        color: "#FF5964",
        fontSize: 11,
        fontWeight: '500'
    },
    asteriskSty: {
        color: "#FF5964",
        fontSize: 10,
    },
    // ===================================== modal style ================================
    MContainer: {
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
    },
    Mwrapper: {
        height: 510,
        width: '90%',
        backgroundColor: '#fff',
        // padding: 15,
        display: 'flex',
        justifyContent: 'space-around',
        // alignItems: 'center'
    },
    cardText1: {
        color: "#2F2F2F",
        fontWeight: '500',
        fontSize: 12,
    },
    // ===================================== modal style ================================



});

import React, { Component, Fragment } from 'react';
import { Text, StyleSheet, ImageBackground, Keyboard, TouchableOpacity } from 'react-native';
import { KeyboardAvoidingView, Image, ScrollView } from 'react-native';
import ToggleSwitch from 'toggle-switch-react-native'
import Ionicons from 'react-native-vector-icons/Ionicons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Checker from '../../assets/workbrookAssets/checker.png';
import {
    Container,
    Header,
    Content,
    Textarea,
    Button,
    View,
    Thumbnail,
    Form,
    Item,
    Input,
    Label,
    Badge,
    Icon,
    Footer,
    Picker,
    Item as FormItem,
    Left,
    Body,
    CardItem,
    Card,
    Right,
    ListItem,
    List,
} from 'native-base';
import {
    Kaede,
    Hoshi,
    Jiro,
    Isao,
    Madoka,
    Akira,
    Hideo,
    Kohana,
    Makiko,
    Sae,
    Fumi,
} from 'react-native-textinput-effects';
import CustomizeHeader from '../frags/CustomizeHeader';

import { myspiner, AppInlineLoader, ConvertToCurrency } from '../../dependency/UtilityFunctions';
import Logo from '../../assets/workbrookAssets/logo.png';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import RBSheet from "react-native-raw-bottom-sheet";
import Interests from '../interests/Interests';
import Noti from "../../assets/workbrookAssets/noti.png";
import UserBl from "../../assets/workbrookAssets/userBl.png";
import ArrForward from "../../assets/workbrookAssets/arrfr.png";
import Shield from "../../assets/workbrookAssets/shield.png";
import Quest from "../../assets/workbrookAssets/quest.png";
import Info from "../../assets/workbrookAssets/info.png";
import Plus from "../../assets/workbrookAssets/plus.png";
import Remark from "../../assets/workbrookAssets/remark.png";
import Like from "../../assets/workbrookAssets/like.png";
import MoreIcon from "../../assets/workbrookAssets/more2.png";
import Profile from "../../assets/images/profile/dp2.png";
import cardPix from "../../assets/images/background/bg3_old.jpg";
import CloseModal from "../../assets/workbrookAssets/whitecloss.png";
import Modal from 'react-native-modal';
import AccountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import { text } from 'react-native-communications';
import DashboardStore from '../../stores/Dashboard';
import { hasSetIntrest } from '../../auth';
import { SvgUri } from 'react-native-svg';
import EmptyListComponent from '../../StatelessScreens/EmptyListComponent';


@observer
class WBPoints extends Component {
    constructor(props) {
        super(props);
        this.mapRef = null;
        this.state = {
            email: '',
            password: '',
            errorloading: false,
            modalVisible: false,
            isOn: true,
        }
        // this.SetCheckers = this.SetCheckers.bind(this)
        this.toggleMe = this.toggleMe.bind(this);
        this.toggleModal = this.toggleModal.bind(this)
    }
    toggleMe = () => {
        this.setState({ isOn: !this.state.isOn });
    };
    toggleModal = () => {
        this.setState({ modalVisible: !this.state.modalVisible });
    };
    async componentDidMount() {
        DashboardStore.__GetWbLeaguemyPosition();
        DashboardStore.__GetWbLeaguePointAggregation();

    }

    render() {
        const { WbLeague, WbLeaguePointAggregation } = DashboardStore;

        let ErrorComponentDisplay = null;
        // console.log(DashboardStore.MyApplications);

        if (DashboardStore.isProcessing === true) {
            ErrorComponentDisplay = AppInlineLoader(DashboardStore.isProcessing)

        }
        // else {
        //     ErrorComponentDisplay = <EmptyListComponent
        //         message={MyApplications.ErrorMessage}
        //         onclickhandler={this.ReloadPageData}
        //     />
        // }

        let Fullname = '';
        if (WbLeague.data.first_name == null || WbLeague.data == null) {
            Fullname = `${WbLeague.data.username} `;
        }
        else {
            Fullname = `${WbLeague.data.first_name} ${WbLeague.data.last_name} `

        }

        return (

            <Container style={styles.container}>
                <CustomizeHeader
                    leftside='empty'
                    title='My WB points'
                    rightside='cancel'
                    statedColor='#1E93E2'
                    icon='md-close'
                />

                <ScrollView
                    style={{ padding: 21 }}
                >
                    <React.Fragment>
                        {
                            // DashboardStore.JobOppurtunity.JobsOffer.length > 0 ?
                            DashboardStore.isProcessing !== true ?
                                <React.Fragment>
                                    <View style={styles.cupHolder}>
                                        <Image
                                            style={{ width: 58, height: 60, borderRadius: 58 / 2 }}
                                            source={{ uri: WbLeague.data.image_url }}
                                        />
                                        <Text style={[styles.Text2, { marginBottom: 10, marginTop: 9 }]}>{Fullname}</Text>
                                        {
                                            Object.keys(WbLeague.myPosition).length === 0 && WbLeague.myPosition.constructor === Object ?
                                                <View style={styles.titlesty}>

                                                    <Text style={[styles.titlestyText]}>Please complete your profile</Text>

                                                </View>
                                                :
                                                <View style={styles.titlesty}>

                                                    <Text style={[styles.titlestyText]}>You are currently </Text>
                                                    <Text style={[styles.titlestyText, { fontWeight: 'bold', marginRight: 3, marginLeft: 3 }]}>{WbLeague.myPosition.rank}</Text>
                                                    <TouchableOpacity
                                                        onPress={() => { this.props.navigation.navigate("LeagueParticipants") }}

                                                    >
                                                        <Text style={[styles.titlestyText, { fontWeight: 'bold', color: '#1E93E2', }]}>
                                                            View Ranking
                                                        </Text>
                                                    </TouchableOpacity>
                                                    {/* <Text style={[styles.titlestyText]}>{WbLeague.myPosition.location}</Text> */}
                                                </View>


                                        }
                                        {/* DashboardStore.WbLeaguePointAggregation.execRef = execRef;           
            DashboardStore.WbLeaguePointAggregation.graduateRef = graduateRef;           
            DashboardStore.WbLeaguePointAggregation.graduateRef = graduateRef;           
            DashboardStore.WbLeaguePointAggregation.invitedAFriend = invitedAFriend;           
            DashboardStore.WbLeaguePointAggregation.mGRef = mGRef;           
            DashboardStore.WbLeaguePointAggregation.nMRef = nMRef;           
            DashboardStore.WbLeaguePointAggregation.profilePoint = profilePoint;           
            DashboardStore.WbLeaguePointAggregation.sMRef = sMRef;           
            DashboardStore.WbLeaguePointAggregation.signUP = signUP; */}

                                    </View>
                                    <View style={styles.contentList}>
                                        <View style={styles.likebtnL_2}>
                                            <Text style={[styles.Text1, {}]}>
                                                Sign Up
                                </Text>
                                            <Text style={[styles.Text1, { color: "#5ABC7A" }]}>
                                                {`${WbLeaguePointAggregation.signUP.signUpPoint == null ? 0 : WbLeaguePointAggregation.signUP.signUpPoint} pts`}
                                            </Text>
                                        </View>
                                        <View style={styles.likebtnL_2}>
                                            <Text style={[styles.Text1, {}]}>

                                                Complete Your Profile
                                            </Text>
                                            <Text style={[styles.Text1, { color: '#5ABC7A' }]}>
                                                {`${WbLeaguePointAggregation.profilePoint.profileCompletedPoint == null ? 0 : WbLeaguePointAggregation.profilePoint.profileCompletedPoint} pts`}
                                            </Text>
                                        </View>
                                        <View style={styles.likebtnL_2}>
                                            <Text style={[styles.Text1, {}]}>
                                                Successful Executive Referral
                                </Text>
                                            <Text style={[styles.Text1, { color: '#5ABC7A' }]}>
                                                {`${WbLeaguePointAggregation.execRef.execRefPoint == null ? 0 : WbLeaguePointAggregation.execRef.execRefPoint} pts`}
                                            </Text>
                                        </View>
                                        <View style={styles.likebtnL_2}>
                                            <Text style={[styles.Text1, {}]}>
                                                Successful Senior Manager Referral
                                </Text>
                                            <Text style={[styles.Text1, { color: '#5ABC7A' }]}>
                                                {`${WbLeaguePointAggregation.sMRef.sMRefPoint == null ? 0 : WbLeaguePointAggregation.sMRef.sMRefPoint} pts`}
                                            </Text>
                                        </View>
                                        <View style={styles.likebtnL_2}>
                                            <Text style={[styles.Text1, {}]}>
                                                Successful Manager Referral
                                </Text>
                                            <Text style={[styles.Text1, { color: '#5ABC7A' }]}>
                                                {`${WbLeaguePointAggregation.mGRef.mGRefPoint == null ? 0 : WbLeaguePointAggregation.mGRef.mGRefPoint} pts`}
                                            </Text>
                                        </View>
                                        <View style={styles.likebtnL_2}>
                                            <Text style={[styles.Text1, {}]}>
                                                Successful Non-Manager Referral
                                </Text>
                                            <Text style={[styles.Text1, { color: '#5ABC7A' }]}>
                                                {`${WbLeaguePointAggregation.nMRef.nMGRefPoint == null ? 0 : WbLeaguePointAggregation.nMRef.nMGRefPoint} pts`}
                                            </Text>
                                        </View>
                                        <View style={styles.likebtnL_2}>
                                            <Text style={[styles.Text1, {}]}>
                                                Successful Graduate Referral
                                </Text>
                                            <Text style={[styles.Text1, { color: '#5ABC7A' }]}>
                                                {`${WbLeaguePointAggregation.graduateRef.graduateRefPoint == null ? 0 : WbLeaguePointAggregation.graduateRef.graduateRefPoint} pts`}
                                            </Text>
                                        </View>
                                        <View style={styles.likebtnL_2}>
                                            <Text style={[styles.Text1, {}]}>
                                                Invite a friend
                                </Text>
                                            <Text style={[styles.Text1, { color: '#5ABC7A' }]}>
                                                {`${WbLeaguePointAggregation.invitedAFriend.invitedAFriendPoint == null ? 0 : WbLeaguePointAggregation.invitedAFriend.invitedAFriendPoint} pts`}
                                            </Text>
                                        </View>

                                    </View>
                                    <View style={styles.contentLis2}>
                                        <View style={styles.likebtnL_2}>
                                            <Text style={[styles.Text1, {}]}>
                                                Total Points Earned
                                </Text>
                                            <Button style={styles.CompleteBt2}>
                                                <Text style={[styles.Text1, { color: '#FAFAFA' }]}>
                                                    {WbLeaguePointAggregation.grandTotal}  pts
                                </Text>
                                            </Button>
                                        </View>

                                    </View>
                                    {WbLeaguePointAggregation.grandTotal < 50 ?
                                        <React.Fragment>
                                            <Button style={[styles.CompleteBt3, { borderColor: '#BCBCBC', }]}
                                            // onPress={() => { this.props.navigation.navigate("LeagueParticipants") }}
                                            >
                                                <Text style={[styles.Text1, { color: '#BCBCBC' }]}>
                                                    WITHDRAW $  {ConvertToCurrency(WbLeaguePointAggregation.grandTotal)}
                                                </Text>

                                            </Button>
                                            <View style={styles.inforSty}>
                                                <AntDesign name="infocirlce" style={{ fontSize: 12, marginRight: 2 }} />
                                                <Text style={{ fontSize: 11 }}>You need a minimum of 50pts</Text>
                                            </View>

                                        </React.Fragment>

                                        :
                                        <Button style={styles.CompleteBt3}
                                        // onPress={() => { this.props.navigation.navigate("LeagueParticipants") }}
                                        >
                                            <Text style={[styles.Text1, { color: '#1E93E2' }]}>
                                                WITHDRAW $ {ConvertToCurrency(WbLeaguePointAggregation.grandTotal)}
                                            </Text>

                                        </Button>


                                    }


                                </React.Fragment>
                                :
                                ErrorComponentDisplay
                        }
                    </React.Fragment>



                    <TouchableOpacity onPress={() => this.toggleModal()}>
                        <View style={styles.likeFooter}>
                            <Text style={styles.Text3}>What’s this about</Text>

                            <SvgUri
                                style={{ marginLeft: 15 }}
                                width='24px'
                                height='24px'
                                uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1590634092/Mobile_Assets/info_uegybm.svg"
                            ></SvgUri>
                        </View>
                    </TouchableOpacity>

                </ScrollView>
                <Modal style={styles.MContainer} isVisible={this.state.modalVisible} >
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                        showsHorizontalScrollIndicator={false}
                    >
                        <View style={styles.homeModalwrap}>
                            <Button onPress={this.toggleModal} style={{ backgroundColor: "transparent", position: 'absolute', right: 0, top: -10, padding: 10, elevation: 0, zIndex: 9999, }}>
                                <Image source={CloseModal} style={{ width: 19, height: 19, }} />
                            </Button>
                            <View>
                                <View style={styles.homeModalHeader}>
                                    <Text style={styles.Text4}>How points are awarded.</Text>
                                </View>
                                <View style={styles.homeModalContentWrap}>
                                    <View style={styles.ModallikebtnLs}>
                                        <Text style={[styles.Text1_Modal, { color: '#1E93E2', flex: 1 }]}>
                                            Sign Up
                                    </Text>
                                        <View style={styles.flexSideRight}>
                                            <View style={[styles.view1_Moda1_]}>
                                                <Text style={[styles.Text1_Modal2], { color: "#5ABC7A", marginLeft: 10 }}>
                                                    5pts
                                            </Text>
                                            </View>
                                            <View style={[styles.view1_Moda2_]}>
                                                <Text style={[styles.Text1_Modal2], { color: "#5ABC7A" }}>
                                                    $5
                                            </Text>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={styles.ModallikebtnLs}>
                                        <Text style={[styles.Text1_Modal, { color: '#1E93E2', flex: 1 }]}>
                                            Complete Your Profile
                                    </Text>
                                        <View style={styles.flexSideRight}>
                                            <View style={[styles.view1_Moda1_]}>
                                                <Text style={[styles.Text1_Modal2], { color: "#5ABC7A", marginLeft: 10 }}>
                                                    5pts
                                            </Text>
                                            </View>
                                            <View style={[styles.view1_Moda2_]}>
                                                <Text style={[styles.Text1_Modal2], { color: "#5ABC7A" }}>
                                                    $5
                                            </Text>
                                            </View>
                                        </View>

                                    </View>
                                    <View style={styles.ModallikebtnLs}>
                                        <Text style={[styles.Text1_Modal, { color: '#1E93E2', flex: 1 }]}>
                                            Successful Executive Referral
                                    </Text>
                                        <View style={styles.flexSideRight}>
                                            <View style={[styles.view1_Moda1_]}>
                                                <Text style={[styles.Text1_Modal2], { color: "#5ABC7A", marginLeft: 10 }}>
                                                    30pts
                                            </Text>
                                            </View>
                                            <View style={[styles.view1_Moda2_]}>
                                                <Text style={[styles.Text1_Modal2], { color: "#5ABC7A" }}>
                                                    $30
                                            </Text>
                                            </View>
                                        </View>

                                    </View>
                                    <View style={styles.ModallikebtnLs}>
                                        <Text style={[styles.Text1_Modal, { color: '#1E93E2', flex: 1 }]}>
                                            Successful Senior Manager Referral
                                    </Text>
                                        <View style={styles.flexSideRight}>
                                            <View style={[styles.view1_Moda1_]}>
                                                <Text style={[styles.Text1_Modal2], { color: "#5ABC7A", marginLeft: 10 }}>
                                                    20pts
                                            </Text>
                                            </View>
                                            <View style={[styles.view1_Moda2_]}>
                                                <Text style={[styles.Text1_Modal2], { color: "#5ABC7A" }}>
                                                    $20
                                            </Text>
                                            </View>
                                        </View>

                                    </View>
                                    <View style={styles.ModallikebtnLs}>
                                        <Text style={[styles.Text1_Modal, { color: '#1E93E2', flex: 1 }]}>
                                            Successful Manager Referral
                                    </Text>
                                        <View style={styles.flexSideRight}>
                                            <View style={[styles.view1_Moda1_]}>
                                                <Text style={[styles.Text1_Modal2], { color: "#5ABC7A", marginLeft: 10 }}>
                                                    15pts
                                            </Text>
                                            </View>
                                            <View style={[styles.view1_Moda2_]}>
                                                <Text style={[styles.Text1_Modal2], { color: "#5ABC7A" }}>
                                                    $15
                                            </Text>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={styles.ModallikebtnLs}>
                                        <Text style={[styles.Text1_Modal, { color: '#1E93E2', flex: 1 }]}>
                                            Successful Non-Manager Referral
                                    </Text>
                                        <View style={styles.flexSideRight}>
                                            <View style={[styles.view1_Moda1_]}>
                                                <Text style={[styles.Text1_Modal2], { color: "#5ABC7A", marginLeft: 10 }}>
                                                    10pts
                                            </Text>
                                            </View>
                                            <View style={[styles.view1_Moda2_]}>
                                                <Text style={[styles.Text1_Modal2], { color: "#5ABC7A" }}>
                                                    $10
                                            </Text>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={styles.ModallikebtnLs}>
                                        <Text style={[styles.Text1_Modal, { color: '#1E93E2', flex: 1 }]}>
                                            Successful Graduate Referral
                                    </Text>
                                        <View style={styles.flexSideRight}>
                                            <View style={[styles.view1_Moda1_]}>
                                                <Text style={[styles.Text1_Modal2], { color: "#5ABC7A", marginLeft: 10 }}>
                                                    10pts
                                                </Text>
                                            </View>
                                            <View style={[styles.view1_Moda2_]}>
                                                <Text style={[styles.Text1_Modal2], { color: "#5ABC7A" }}>
                                                    $10
                                                    </Text>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={styles.ModallikebtnLs}>
                                        <Text style={[styles.Text1_Modal, { color: '#1E93E2', flex: 1 }]}>
                                            Invite a friend
                                            </Text>
                                        <View style={styles.flexSideRight}>
                                            <View style={[styles.view1_Moda1_]}>
                                                <Text style={[styles.Text1_Modal2], { color: "#5ABC7A", marginLeft: 10 }}>
                                                    5pts
                                                    </Text>
                                            </View>
                                            <View style={[styles.view1_Moda2_]}>
                                                <Text style={[styles.Text1_Modal2], { color: "#5ABC7A" }}>
                                                    $5
                                                    </Text>
                                            </View>
                                        </View>
                                    </View>


                                </View>
                                <View style={styles.noteItemsWrap}>
                                    <View style={styles.noteItems}>
                                        <SvgUri
                                            style={{ marginRight: 13 }}
                                            width="7px"
                                            height="15px"
                                            uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1590639365/Mobile_Assets/arrforblue_wau5kk.svg"
                                        >
                                        </SvgUri>
                                        <Text style={[styles.Text5, { flex: 1 }]}>
                                            You have to complete your profile before cash out is possible.
                                </Text>
                                    </View>

                                    <View style={styles.noteItems}>
                                        <SvgUri
                                            style={{ marginRight: 13 }}
                                            width="7px"
                                            height="15px"
                                            uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1590639365/Mobile_Assets/arrforblue_wau5kk.svg"
                                        >
                                        </SvgUri>
                                        <Text style={[styles.Text5, { flex: 1 }]}>
                                            At least 50points before you can cash out.

                                </Text>
                                    </View>
                                    <View style={styles.noteItems}>
                                        <SvgUri
                                            style={{ marginRight: 13 }}
                                            width="7px"
                                            height="15px"
                                            uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1590639365/Mobile_Assets/arrforblue_wau5kk.svg"
                                        >
                                        </SvgUri>
                                        <Text style={[styles.Text5, { flex: 1 }]}>
                                            You cant earn points on "invite a friend" until the friend posts a job or applies for a job and is successful or makes a successful referral.
                                </Text>
                                    </View>
                                </View>

                            </View>
                            <View style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', width: '100%', backgroundColor: 'red', height: 54 }}>
                                <Button full style={[styles.btnSave, { backgroundColor: "#FF5964", flex: 1 }]}
                                    onPress={this.toggleModal}
                                >
                                    <Text style={[styles.btnCheckyText, { color: '#fff' }]}>{DashboardStore.isProcessing === true ? 'Processing' : 'Close'}</Text>
                                </Button>
                            </View>
                        </View>
                    </ScrollView>
                </Modal>

            </Container>


        );
    }
}
export default WBPoints;

const styles = StyleSheet.create({
    container: {
        backgroundColor: "#FFF",
        display: "flex",
        flex: 1,
        justifyContent: "flex-start",
    },
    likeFooter: {
        display: "flex",
        flexDirection: 'row',
        justifyContent: "flex-end",
        alignItems: 'center',
        // width: '100%'
        marginTop: 40,
        marginBottom: 30,
    },
    cupHolder: {
        display: "flex",
        justifyContent: "center",
        alignItems: 'center',
        paddingTop: 6,
        marginBottom: 40,
    },
    contentList: {
        display: "flex",
        alignItems: 'center',
        marginBottom: 32,
        borderTopWidth: 1,
        borderBottomWidth: 1,
        borderColor: "#E5E5E5",
        paddingTop: 20,
    },
    contentLis2: {
        display: "flex",
        alignItems: 'center',
        // marginBottom: 32,
        // paddingTop: 20,
    },
    likebtnL_2: {
        display: "flex",
        flexDirection: 'row',
        alignItems: 'center',
        // backgroundColor: '#1E93E2',
        // height: 47,
        justifyContent: 'space-between',
        width: '100%',
        // marginBottom: 11,
        // borderWidth: 1,
        // borderColor: '#1E93E2',
        // borderRadius: 15,
        paddingLeft: 20,
        paddingRight: 20,
        paddingBottom: 20,
    },
    likebtnLs: {
        display: "flex",
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#1E93E2',
        height: 47,
        justifyContent: 'space-between',
        width: '100%',
        marginBottom: 11,
        borderWidth: 1,
        borderColor: '#1E93E2',
        borderRadius: 15,
        paddingLeft: 20,
        paddingRight: 20,
    },
    ModallikebtnLs: {
        display: "flex",
        flexDirection: 'row',
        alignItems: 'center',
        // backgroundColor: '#1E93E2',
        height: 37,
        justifyContent: 'space-between',
        width: '100%',
        marginBottom: 11,
        // borderWidth: 1,
        // borderColor: '#1E93E2',
        // borderRadius: 9,
        paddingLeft: 20,
        paddingRight: 20,
    },
    inforSty: {
        display: "flex",
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
        justifyContent: 'center'
    },
    flexSideRight: {
        display: "flex",
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end'
    },
    CompleteBtn: {
        display: "flex",
        alignItems: 'center',
        height: 47,
        backgroundColor: '#5ABC7A',
        justifyContent: 'space-between',
        width: '100%',
        marginBottom: 11,
        borderWidth: 1,
        borderColor: '#5ABC7A',
        borderRadius: 15,
        elevation: 0,
        paddingLeft: 20,
        paddingRight: 20,
    },
    CompleteBt2: {
        display: "flex",
        alignItems: 'center',
        height: 37,
        backgroundColor: '#5ABC7A',
        justifyContent: 'space-between',
        width: 81,
        marginBottom: 11,
        borderRadius: 15,
        elevation: 0,
        paddingLeft: 20,
        paddingRight: 20,
    },
    CompleteBt3: {
        display: "flex",
        alignItems: 'center',
        height: 45,
        backgroundColor: 'transparent',
        justifyContent: 'center',
        width: '100%',
        borderWidth: 1,
        borderColor: '#1E93E2',
        marginBottom: 11,
        borderRadius: 3,
        elevation: 0,
        paddingLeft: 20,
        paddingRight: 20,
    },
    Text1: {
        fontSize: 13,
        fontWeight: '600',
        color: '#1E93E2',
    },
    Text1_Modal: {
        fontSize: 10,
        fontWeight: '500',
        color: '#FAFAFA',
    },
    Text1_Modal2: {
        fontSize: 10,
        fontWeight: '500',
        color: '#FAFAFA',
        // marginLeft: 20,
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'center',
        flexDirection: 'row',
    },
    view1_Moda2_: {
        display: 'flex',
        width: 40,
        justifyContent: 'flex-end',
        alignItems: 'center',
        flexDirection: 'row',
        // backgroundColor: 'green'
    },
    view1_Moda1_: {
        marginRight: 20,
        display: 'flex',
        width: 50,
        justifyContent: 'flex-end',
        alignItems: 'center',
        flexDirection: 'row',
    },
    Text2: {
        fontSize: 16,
        fontWeight: '600',
        color: '#1E93E2',
    },
    Text3: {
        fontSize: 12,
        fontWeight: '500',
        color: '#1E93E2',
    },
    Text4: {
        fontSize: 14,
        fontWeight: '600',
        color: '#1E93E2',
    },
    Text5: {
        fontSize: 10,
        fontWeight: '300',
        color: '#2F2F2F',
    },
    titlesty: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 31
    },
    titlestyText: {
        fontWeight: '300',
        fontSize: 11,
        color: '#2F2F2F',
    },
    // Modal style =============================================================
    MContainer: {
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
    },
    Mwrapper: {
        height: 510,
        width: '90%',
        backgroundColor: '#fff',
        // padding: 15,
        display: 'flex',
        justifyContent: 'space-around',
        // alignItems: 'center'
    },
    homeModalwrap: {
        // height: '80%',
        width: '100%',
        backgroundColor: '#fff',
        // padding: 15,
        // display: 'flex',
        justifyContent: 'space-between',
        overflow: 'hidden',
        // alignItems: 'center'
    },

    homeModalHeader: {
        width: '100%',
        backgroundColor: '#fff',
        padding: 25,
        display: 'flex',
        alignItems: 'center'
    },
    homeModalContentWrap: {
        width: '100%',
        padding: 18,
        paddingTop: 0,
        display: 'flex',
        alignItems: 'center'
    },
    homeModalwrapText1: {
        fontSize: 60,
        fontSize: 16,
        color: '#FAFAFA',
    },
    btnSave: {
        height: 54,
        backgroundColor: '#1E93E2',
        elevation: 0,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    noteItemsWrap: {
        display: 'flex',
        marginBottom: 40,
        paddingLeft: 18,
        paddingRight: 18,
        width: '100%',
    },
    noteItems: {
        flexDirection: 'row',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        // marginBottom: 13,
        marginTop: 8,
        width: '100%',
    },
    // Modal Style ends=========================================================





});

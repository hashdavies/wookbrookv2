import React, { Component, Fragment } from 'react';
import { Text, StyleSheet, ImageBackground, Keyboard, TouchableOpacity } from 'react-native';
import { KeyboardAvoidingView, Image, ScrollView } from 'react-native';
import ToggleSwitch from 'toggle-switch-react-native'
import Ionicons from 'react-native-vector-icons/Ionicons';
import Checker from '../../assets/workbrookAssets/checker.png';
import {
    Container,
    Header,
    Content,
    Textarea,
    Button,
    View,
    Thumbnail,
    Form,
    Item,
    Input,
    Label,
    Badge,
    Icon,
    Footer,
    Picker,
    Item as FormItem,
    Left,
    Body,
    CardItem,
    Card,
    Right,
    ListItem,
    List,
} from 'native-base';
import {
    Kaede,
    Hoshi,
    Jiro,
    Isao,
    Madoka,
    Akira,
    Hideo,
    Kohana,
    Makiko,
    Sae,
    Fumi,
} from 'react-native-textinput-effects';
import CustomizeHeader from '../frags/CustomizeHeader';

import { myspiner } from '../../dependency/UtilityFunctions';
import Logo from '../../assets/workbrookAssets/logo.png';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import RBSheet from "react-native-raw-bottom-sheet";
import Interests from '../interests/Interests';
import Noti from "../../assets/workbrookAssets/noti.png";
import UserBl from "../../assets/workbrookAssets/userBl.png";
import ArrForward from "../../assets/workbrookAssets/arrfr.png";
import Shield from "../../assets/workbrookAssets/shield.png";
import Quest from "../../assets/workbrookAssets/quest.png";
import Info from "../../assets/workbrookAssets/info.png";
import Plus from "../../assets/workbrookAssets/plus.png";
import Remark from "../../assets/workbrookAssets/remark.png";
import Like from "../../assets/workbrookAssets/like.png";
import MoreIcon from "../../assets/workbrookAssets/more2.png";
import Profile from "../../assets/images/profile/dp2.png";
import cardPix from "../../assets/images/background/bg3_old.jpg";

import AccountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import { text } from 'react-native-communications';
import DashboardStore from '../../stores/Dashboard';
import { hasSetIntrest } from '../../auth';
import { SvgUri } from 'react-native-svg';


@observer
class League extends Component {
    constructor(props) {
        super(props);
        this.mapRef = null;
        this.state = {
            email: '',
            password: '',
            errorloading: false,
            isOn: true,
        }
        // this.SetCheckers = this.SetCheckers.bind(this)
        this.toggleMe = this.toggleMe.bind(this)
    }
    toggleMe = () => {
        this.setState({ isOn: !this.state.isOn });
    };
    async componentDidMount() {
        let status = await hasSetIntrest();
        // alert(status)
        DashboardStore._UpdateUserSetIntrest(status);

    }

    render() {
        return (

            <Container style={styles.container}>
                <CustomizeHeader
                    leftside='WhiteArr'
                    title='Workbrook League'
                    rightside='empty'
                    statedColor='#1E93E2'
                    icon='md-close'
                />

                <ScrollView
                    style={{ padding: 26, }}
                >
                    <View style={styles.cupHolder}>
                        <SvgUri
                            width='44px'
                            height='40px'
                            uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1590509060/Mobile_Assets/cup_litl8j.svg"
                        ></SvgUri>
                    </View>
                    <View style={styles.contentList}>
                        <Text style={styles.Text2}>
                            How you earn points
                            in the WB league
                        </Text>
                        <Text style={styles.Text1}>
                            Earn points by completing tasks on the app
                            </Text>
                        <Text style={styles.Text1}>
                            Complete your profile to 100%
                            </Text>
                        <Text style={styles.Text1}>
                            Make a successful referral.
                            </Text>
                        <Text style={styles.Text1}>
                            Invite a friend to use workbrook.
                            </Text>
                        <Text style={styles.Text1}>
                            Convert your earned points to real money.
                            </Text>
                            <TouchableOpacity
                            onPress={() => { this.props.navigation.navigate("profile") }}
                            >
                        <Text style={styles.Text1_2}>
                            Complete Your Profile (10pts)
                            </Text>
                            </TouchableOpacity>
                        <Button style={styles.CompleteBtn}
                            onPress={() => { this.props.navigation.navigate("WBPoints") }}
                        >
                            <Text style={[styles.Text1_3,]}>
                            Take me to my WB Points
                            </Text>
                        </Button>

                    </View>

                </ScrollView>
            </Container>


        );
    }
}
export default League;

const styles = StyleSheet.create({
    container: {
        backgroundColor: "#FAFAFA",
        display: "flex",
        flex: 1,
        justifyContent: "flex-start",
    },
    cupHolder: {
        display: "flex",
        justifyContent: "center",
        // alignItems: 'center',
        paddingTop: 6,
        // marginBottom: 32,
    },
    contentList: {
        display: "flex",
        alignItems: 'flex-start',
    },
    likebtnLs: {
        display: "flex",
        // alignItems: 'center',
        height: 47,
        justifyContent: 'center',
        width: '100%',
        marginBottom: 11,
        borderWidth: 1,
        borderColor: '#1E93E2',
        borderRadius: 15,
        paddingLeft: 20,
        paddingRight: 20,
    },
    CompleteBtn: {
        display: "flex",
        alignItems: 'center',
        height: 47,
        width: 190,
        backgroundColor: '#5ABC7A',
        justifyContent: 'center',
        marginBottom: 49,
        borderWidth: 1,
        borderColor: '#5ABC7A',
        borderRadius: 10,
        elevation: 0,
    },
    Text1: {
        fontSize: 14,
        fontWeight: '500',
        color: '#1E93E2',
        marginBottom: 13,
    },
    Text1_2: {
        fontSize: 14,
        fontWeight: 'bold',
        color: '#5ABC7A',
        marginBottom: 43,
        marginTop: 40,
    },
    Text1_3: {
        fontSize: 14,
        fontWeight: 'bold',
        color: '#FAFAFA',
    },
    Text2: {
        fontSize: 18,
        fontWeight: 'bold',
        color: '#1E93E2',
        marginBottom: 27,
        marginTop: 27
    },




});

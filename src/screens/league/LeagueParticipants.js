import React, { Component, Fragment } from 'react';
import { Text, StyleSheet, ImageBackground, Keyboard, TouchableOpacity, Alert, FlatList, ActivityIndicator } from 'react-native';
import { KeyboardAvoidingView, Image, ScrollView } from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons';
import Checker from '../../assets/workbrookAssets/checker.png';
import {
    Container,
    Header,
    Content,
    Textarea,
    Button,
    View,
    Thumbnail,
    Form,
    Item,
    Input,
    Label,
    Badge,
    Icon,
    Footer,
    Picker,
    Item as FormItem,
    Left,
    Body,
    CardItem,
    Card,
    Right,
    ListItem,
    List,
    CheckBox,
} from 'native-base';
import {
    Kaede,
    Hoshi,
    Jiro,
    Isao,
    Madoka,
    Akira,
    Hideo,
    Kohana,
    Makiko,
    Sae,
    Fumi,
} from 'react-native-textinput-effects';
import CustomizeHeader from '../frags/CustomizeHeader';

import { myspiner, temp, ShowNotification, AppInlineLoader } from '../../dependency/UtilityFunctions';
import Logo from '../../assets/workbrookAssets/logo.png';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import RBSheet from "react-native-raw-bottom-sheet";
import Interests from '../interests/Interests';
import Noti from "../../assets/workbrookAssets/noti.png";
import UserBl from "../../assets/workbrookAssets/userBl.png";
import ArrForward from "../../assets/workbrookAssets/arrfr.png";
import Shield from "../../assets/workbrookAssets/shield.png";
import Quest from "../../assets/workbrookAssets/quest.png";
import Info from "../../assets/workbrookAssets/info.png";
import Plus from "../../assets/workbrookAssets/plus.png";
import Remark from "../../assets/workbrookAssets/remark.png";
import Like from "../../assets/workbrookAssets/like.png";
import MoreIcon from "../../assets/workbrookAssets/more2.png";
import Profile from "../../assets/images/profile/dp2.png";
import cardPix from "../../assets/images/background/bg3_old.jpg";
import RNPickerSelect from 'react-native-picker-select';
import TagInput from 'react-native-tags-input';

import RNFetchBlob from 'react-native-fetch-blob'
import ImagePicker from 'react-native-image-picker';
import CloseModal from "../../assets/workbrookAssets/whitecloss.png";
import SuccessIcon from "../../assets/workbrookAssets/successPost.png";
import Modal from 'react-native-modal';
import AccountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import { text } from 'react-native-communications';
import DashboardStore from '../../stores/Dashboard';
import { hasSetIntrest } from '../../auth';
import Autocomplete from 'react-native-autocomplete-input';
import MultiSelect from 'react-native-multiple-select';
import { YOUR_CLOUDINARY_PRESET, YOUR_CLOUDINARY_NAME, requestCameraPermission, YOUR_CLOUDINARY_PRESET_JobBanner } from '../../dependency/UtilityFunctions';
import EmptyListComponent from '../../StatelessScreens/EmptyListComponent';


items = [{
    id: '92iijs7yta',
    name: 'Ondo',
}, {
    id: 'a0s0a8ssbsd',
    name: 'Ogun',
}, {
    id: '16hbajsabsd',
    name: 'Calabar',
}, {
    id: 'sdhyaysdj',
    name: 'Kaduna',
}, {
    id: 'suudydjsjd',
    name: 'Abuja',
}];

@observer
class LeagueParticipants extends Component {
    constructor(props) {
        super(props);
        this.mapRef = null;
        this.state = {
            email: '',
            password: '',

        }
        this.ReloadPageData=this.ReloadPageData.bind(this);

    }

    async componentDidMount() {
        DashboardStore.__GetWbLeague_List();
    }
    ReloadPageData = () => {
        DashboardStore.__GetWbLeague_List();
      };
    render() {
        let ErrorComponentDisplay = null;
        const {WbLeagueList}=DashboardStore;
        
 
if (DashboardStore.isProcessing === true) {
    ErrorComponentDisplay = AppInlineLoader(DashboardStore.isProcessing)

}
else {
    ErrorComponentDisplay = <EmptyListComponent
        // message={WbLeagueList.ErrorMessage}
        message={"No league or profile not completed"}
        onclickhandler={this.ReloadPageData}
    />
}


let LeagueParticipants__=
<FlatList
     data={WbLeagueList.GeneralList}
    // Render Items
    renderItem={({ item,index }) => {
    //   let Applicants=item.applicants
    // let posterName=  `${poster.first_name}  ${poster.last_name} `
    // let poster = item.created_by_profile
    let Part_name = item.first_name == null || item.last_name == null ? item.username : `${item.first_name}  ${item.last_name} `
  
return (

    <List style={{ borderBottomWidth: 1, borderBottomColor: '#E5E5E5', paddingBottom: 15 }}>
    <ListItem Button avatar style={{ display: 'flex', justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center', }}
        onPress={() => { this.props.navigation.navigate("MyWallet") }}
    >
        <Left style={styles.leftAlignSty}>
            <Text style={styles.numberCount}>{index + 1}.</Text>
            <Thumbnail style={styles.applicantImg} source={{uri:item.image_url}} />
        </Left>
        <Body style={[styles.leftAlignSty, { borderBottomWidth: 0, marginBottom: -5 }]}>
<Text style={styles.cardTextJob4}>{Part_name}</Text>
        </Body>
        <Right style={[styles.leftAlignSty, { borderBottomWidth: 0, marginBottom: -5 }]}>
            <Text style={styles.titlestyText2}>
{item.totalPoint} pts
            </Text>
        </Right>

    </ListItem>
</List>

  )}}
    // Item Key
    keyExtractor={(item, index) => String(index)}
    // Header (Title)
    // ListHeaderComponent={this.renderHeader}
    // Footer (Activity Indicator)
    // ListFooterComponent={this.renderFooter()}
    // On End Reached (Takes a function)
    onEndReached={this.retrieveMore}
    // How Close To The End Of List Until Next Data Request Is Made
    // onEndReachedThreshold={0}
    // Refreshing (Set To True When End Reached)
    // refreshing={this.state.refreshing}
  />

        return (

            <Container style={styles.container}>
                <CustomizeHeader
                    leftside='WhiteArr'
                    title='Workbrook League'
                    rightside='empty'
                    statedColor='#1E93E2'
                    icon='md-close'
                // savehandler={this.proceednow}
                />

                <ScrollView
                    contentContainerStyle={{
                        padding: 35,
                    }}
                >
                    <View style={styles.titlesty}>
                        <Text style={[styles.titlestyText]}>Showing the top ten</Text>
                        <Text style={[styles.titlestyText, { fontWeight: 'bold', color: '#1E93E2', marginRight: 3, marginLeft: 3,marginBottom:3 }]}>Brookers</Text>
                        <Text style={[styles.titlestyText]}>in the country.</Text>
                    </View>

                    {/* <List style={{ borderBottomWidth: 1, borderBottomColor: '#E5E5E5', paddingBottom: 15 }}>
                        <ListItem Button avatar style={{ display: 'flex', justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center', }}
                            onPress={() => { this.props.navigation.navigate("MyWallet") }}
                        >
                            <Left style={styles.leftAlignSty}>
                                <Text style={styles.numberCount}>1.</Text>
                                <Thumbnail style={styles.applicantImg} source={Profile} />
                            </Left>
                            <Body style={[styles.leftAlignSty, { borderBottomWidth: 0, marginBottom: -5 }]}>
                                <Text style={styles.cardTextJob4}>Jane Dokaszhuk</Text>
                            </Body>
                            <Right style={[styles.leftAlignSty, { borderBottomWidth: 0, marginBottom: -5 }]}>
                                <Text style={styles.titlestyText2}>
                                    543pts
                                </Text>
                            </Right>

                        </ListItem>
                    </List>

                    <List style={{ borderBottomWidth: 1, borderBottomColor: '#E5E5E5', paddingBottom: 15 }}>
                        <ListItem Button avatar style={{ display: 'flex', justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center', }}
                            onPress={() => { this.props.navigation.navigate("ViewApplicant") }}
                        >
                            <Left style={styles.leftAlignSty}>
                                <Text style={styles.numberCount}>2.</Text>
                                <Thumbnail style={styles.applicantImg} source={Profile} />
                            </Left>
                            <Body style={[styles.leftAlignSty, { borderBottomWidth: 0, marginBottom: -5 }]}>
                                <Text style={styles.cardTextJob4}>Tomosz Kuba</Text>
                            </Body>
                            <Right style={[styles.leftAlignSty, { borderBottomWidth: 0, marginBottom: -5 }]}>
                                <Text style={styles.titlestyText2}>
                                    453pts
                                </Text>
                            </Right>

                        </ListItem>
                    </List>
                    <List style={{ borderBottomWidth: 1, borderBottomColor: '#E5E5E5', paddingBottom: 15 }}>
                        <ListItem Button avatar style={{ display: 'flex', justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center', }}
                            onPress={() => { this.props.navigation.navigate("ViewApplicant") }}
                        >
                            <Left style={styles.leftAlignSty}>
                                <Text style={styles.numberCount}>3.</Text>
                                <Thumbnail style={styles.applicantImg} source={Profile} />
                            </Left>
                            <Body style={[styles.leftAlignSty, { borderBottomWidth: 0, marginBottom: -5 }]}>
                                <Text style={styles.cardTextJob4}>Aeeski Lohun</Text>
                            </Body>
                            <Right style={[styles.leftAlignSty, { borderBottomWidth: 0, marginBottom: -5 }]}>
                                <Text style={styles.titlestyText2}>
                                    213pts
                                </Text>
                            </Right>

                        </ListItem>
                    </List> */}

                    {
                            WbLeagueList.GeneralList.length > 0 ?

                            LeagueParticipants__
                                :
                                ErrorComponentDisplay
                        }


                </ScrollView>



            </Container>


        );
    }
}
export default LeagueParticipants;

const styles = StyleSheet.create({
    container: {
        backgroundColor: "white",
        display: "flex",
        flex: 1,
        justifyContent: "flex-start",
    },

    rmPadding: {
        paddingTop: 0,
        paddingRight: 0,
        paddingBottom: 0,
        paddingLeft: 0,
    },
    rmPaddingLR: {
        paddingLeft: 0,
        paddingRight: 0,
    },

    cardTextJob1: {
        color: "#2F2F2F",
        fontSize: 36,
        fontWeight: '600',

    },
    cardTextJob2: {
        color: "#BCBCBC",
        fontSize: 14,
        fontWeight: '600',
        marginBottom: 5,
    },
    cardTextJob2_2: {
        color: "#fff",
        fontSize: 14,
        fontWeight: '600',
    },
    cardTextJob3: {
        color: "#2F2F2F",
        fontSize: 13,
        fontWeight: '500',
    },
    cardTextJob4: {
        fontSize: 12,
        color: '#2F2F2F',
        fontWeight: '600'
    },

    cardTextJob5: {
        color: "#2F2F2F",
        fontSize: 10,
        fontWeight: '500',
    },
    cardTextJob7: {
        color: "#1E93E2",
        fontSize: 11,
        fontWeight: '500',
    },
    cardTextJob4_1: {
        color: "#fff",
        fontSize: 12,
        fontWeight: '500',
    },

    cardTextJob6: {
        color: "#1E93E2",
        fontSize: 11,
        fontWeight: '500',
    },


    cardText1: {
        color: "#2F2F2F",
        fontWeight: '500',
        fontSize: 12,
    },
    inputStyleWrap: {
        height: 38,
        backgroundColor: '#E5E5E5',
        borderRadius: 5,
    },
    headerWrap: {
        marginBottom: 13,
        marginTop: 20,
        paddingLeft: 25,
        paddingRight: 25,
    },
    serachbtnSty: {
        height: 54,
        borderRadius: 4,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#5ABC7A',
        marginTop: 24
    },
    ViewBtnSty: {
        height: 23,
        width: 50,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#1E93E2',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    btnStyIdc: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    titlesty: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 31
    },
    applicantImg: {
        height: 31,
        width: 32,
        borderRadius: 32 / 2,
    },
    numberCount: {
        fontWeight: '600',
        fontSize: 12,
        color: '#1E93E2',
        marginRight: 11
    },

    titlestyText: {
        fontWeight: '300',
        fontSize: 11,
        color: '#2F2F2F',
    },
    leftAlignSty: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
    },
    titlestyText2: {
        fontWeight: '600',
        fontSize: 12,
        color: '#1E93E2',
    },


});

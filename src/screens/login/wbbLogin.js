import React, { Component } from 'react';
import { Text, StyleSheet, ImageBackground, Keyboard, TouchableOpacity } from 'react-native';
import { KeyboardAvoidingView, Image, ScrollView } from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons';
import {
  Container,
  Header,
  Content,
  Textarea,
  Button,
  View,
  Thumbnail,
  Form,
  Item,
  Input,
  Label,
  Badge,
  Icon,
  Footer,
  Picker,
  Item as FormItem,
} from 'native-base';
import {
  Kaede,
  Hoshi,
  Jiro,
  Isao,
  Madoka,
  Akira,
  Hideo,
  Kohana,
  Makiko,
  Sae,
  Fumi,
} from 'react-native-textinput-effects';
import { Col, Row, Grid } from 'react-native-easy-grid';
import WBB_Input from "../frags/wbbInput"

import { myspiner } from '../../dependency/UtilityFunctions';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';

import AccountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import PasswordTextBox from '../../component/PasswordTextBox';
import CustomizeHeader from '../frags/CustomizeHeader';
import style from 'react-native-datepicker/style';


@observer
class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      errorloading: false,

    }
  }

  render() {
    const { isProcessing } = AccountStore;

    return (
      <View style={styles.containerWrap}>
        <ScrollView style={{height: "100%"}}>
            <WBB_Input inputPlaceHolder="Choose a username *"  Inputstyle={style.inputSty} backgroundColorSty="#fff"/>
        </ScrollView>
        <View style={styles.BottomIndicator}> 
             <Text style={styles.Text1}>
             Already have an acount? Log In
             </Text>
        </View>
      </View>
    );
  }
}
export default Login;

const styles = StyleSheet.create({
  containerWrap: {
    backgroundColor: "#3A6ED4",
    display: "flex",
    justifyContent: "space-between",
    paddingTop: 50,
    paddingRight: 30,
    paddingBottom: 35,
    paddingLeft: 30,
  },
  container: {
    backgroundColor: "transparent",
    display: "flex"
  },
  contentWrap: {
    // backgroundColor: "red",
    display: "flex",
  },
  BottomIndicator: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    borderTopWidth: 1,
    borderColor: "rgba(131, 170, 244, 0.56)",
    paddingTop: 10
  },
  Text1: {
    fontFamily: "Roboto",
    fontSize: 12,
    color: '#D9E6FF',
  },
  inputSty: {
    backgroundColor: '#FFF',
    borderRadius: 8, 
    elevation: 0,
     padding: 22,
     // height: 52,
     marginBottom: 17,
  },

});

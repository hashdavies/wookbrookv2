import React, { Component } from 'react';
import { Text, StyleSheet, ImageBackground, Keyboard, TouchableOpacity } from 'react-native';
import { KeyboardAvoidingView, Image, ScrollView } from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons';
import {
  Container,
  Header,
  Content,
  Textarea,
  Button,
  View,
  Thumbnail,
  Form,
  Item,
  Input,
  Label,
  Badge,
  Icon,
  Footer,
  Picker,
  Item as FormItem,
} from 'native-base';
import {
  Kaede,
  Hoshi,
  Jiro,
  Isao,
  Madoka,
  Akira,
  Hideo,
  Kohana,
  Makiko,
  Sae,
  Fumi,
} from 'react-native-textinput-effects';
import { Col, Row, Grid } from 'react-native-easy-grid';

import { myspiner, _signIn_with_Google } from '../../dependency/UtilityFunctions';
import Logo from '../../assets/workbrookAssets/logo.png';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';

import AccountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import PasswordTextBox from '../../component/PasswordTextBox';
import CustomizeHeader from '../frags/CustomizeHeader';


@observer
class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      errorloading: false,

    }
    this._LoginHandler = this._LoginHandler.bind(this);
  }
  componentDidMount() {
    AccountStore.UpdateNavigationState(this.props.navigation);
  }
  // async  _LoginHandler_ff() {
  //   const { email, password } = this.state;
  //   let data = {
  //     email,
  //     password
  //   }
  //   // let    result = await AccountStore._LoginUserNowtest(data);
  //   let result = await AccountStore._LoginUserNow(data);
  //   let re = AccountStore.redirectmode;

  //   // console.warn(re)
  //   if (re === true) {
  //     Toast.show('Login Successfully', Toast.SHORT, Toast.BOTTOM, ToastStyle);

  //     this.props.navigation.navigate("SignedIn");
  //   }
  //   else {
  //     // console.warn('fuckkkkkk')
  //   }

  // }
  _LoginHandler() {

    AccountStore._LoginNow();


  }
  render() {
    const { isProcessing } = AccountStore;

    return (
      <React.Fragment>
        <CustomizeHeader
          leftside='empty'
          title='Sign In'
          textColor='#000'
          rightside='darkCancel'
          statedColor='#fff'
          DontShhowNotification={false}
        />
        <ScrollView>

          <Container style={styles.container}>
            <View style={styles.contentWrap}>
              <Text style={styles.contentTextHeader}>Welcome</Text>
              <Text style={[styles.contentText]}>
                Please enter your registration email and password.
          </Text>
              <Item regular style={[styles.loginBtn1, { backgroundColor: '#E5E5E5', marginTop: 25 }]}>
                <Input
                  placeholderTextColor={'#BCBCBC'}
                  style={styles.inputSty}
                  placeholder='Email or Username'
                  onChangeText={(email) => AccountStore.onChangeText('email', email, 'payload_loginInfo')}
                  keyboardType={'email-address'}
                />
              </Item>
              <PasswordTextBox
                icon="lock"
                label="Password"
                placeholder="Password"
                //  onChange={(password) => this.setState({password})}
                onChange={(password) => AccountStore.onChangeText('password', password, 'payload_loginInfo')}
                returnKeyType={'done'}
                keyboardType={'default'}
                onSubmitEditing={
                  this._LoginHandler
                }
              />
              {/* <Item regular style={[styles.loginBtn1, { backgroundColor: '#E5E5E5' }]}>
            <Input secureEntryInput={true} placeholderTextColor={'#BCBCBC'} style={styles.inputSty} placeholder='Password' />
          </Item> */}

              <Button full style={[styles.loginBtn1, { backgroundColor: '#5ABC7A', marginTop: 10 }]}
                onPress={this._LoginHandler}
              >
                <Text style={styles.buttonText}>

                  {isProcessing === true ? 'Processing...' : ' Continue'}
                </Text>
              </Button>
              <Text style={[styles.buttonText, { fontSize: 12, color: '#2F2F2F', marginBottom: 10, marginTop: 15, textAlign: 'center' }]}>
                or via social networks
          </Text>
              <View style={styles.socialWrapper}>
                {/* <Button full style={[styles.loginBtn2, { backgroundColor: '#415793' }]}>
                  <Text style={styles.buttonText}>
                    Facebook
          </Text>
                </Button> */}
                <Button full style={[styles.loginBtn2, { backgroundColor: '#346BE6' }]}
                   onPress={() => _signIn_with_Google()}
                >
                  <Text style={styles.buttonText}>
                    Google
          </Text>
                </Button>
                <Button full style={[styles.loginBtn2, { backgroundColor: '#0078B5' }]}>
                  <Text style={styles.buttonText}>
                    LinkedIn
          </Text>
                </Button>

              </View>


            </View>

            <View style={[styles.contentWrap, { justifyContent: 'space-between', alignItems: 'center', height: 40, flexDirection: 'row' }]}>
              <Button style={[styles.loginBtn2, { backgroundColor: 'tranparent', height: 30, marginBottom: 0 }]}
                onPress={() => this.props.navigation.navigate("Signup")}
              >
                <Text style={[styles.buttonText, { color: '#1E93E2', fontSize: 16, marginLeft: 2, fontWeight: '600' }]}>
                  Sign Up
          </Text>
              </Button>
              <Button style={[styles.loginBtn2, { backgroundColor: 'tranparent', height: 30, marginBottom: 0, width: 150, display: 'flex', justifyContent:'flex-end',alignItems: 'center' }]}
                onPress={() => this.props.navigation.navigate("ForgetPassword")}
              >
                <Text style={[styles.buttonText, { color: '#1E93E2', fontSize: 16, marginLeft: 2, fontWeight: '600' }]}>
                  Forgot Password
          </Text>
              </Button>
            </View>



          </Container>

        </ScrollView>
      </React.Fragment>
    );
  }
}
export default Login;

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#fff",
    display: "flex",
    justifyContent: "space-between",
    paddingTop: 50,
    paddingRight: 30,
    paddingBottom: 35,
    paddingLeft: 30,
  },
  contentWrap: {
    // backgroundColor: "red",
    display: "flex",
  },
  socialWrapper: {
    display: "flex",
    justifyContent: 'space-evenly',
    alignItems: 'center',
    flexDirection: 'row',
    marginTop: 20
  },
  contentText: {
    color: "#2F2F2F",
    fontSize: 12,
  },
  contentTextHeader: {
    color: "#2F2F2F",
    fontSize: 24,
    fontWeight: "600",

  },
  loginBtn1: {
    height: 56,
    borderRadius: 5,
    marginBottom: 15,
    elevation: 0,
  },
  loginBtn2: {
    height: 55,
    borderRadius: 5,
    width: 95,
    elevation: 0,
  },
  buttonText: {
    color: "#FAFAFA",
    fontSize: 14,
    fontWeight: "600",
  },
  inputSty: {
    // color: "#BCBCBC",
    fontSize: 14,
    fontWeight: "300",
  },

});

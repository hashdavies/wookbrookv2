import React, { Component, Fragment } from 'react';
import { Text, StyleSheet, ImageBackground, Keyboard, TouchableOpacity, Alert, FlatList, ActivityIndicator } from 'react-native';
import { KeyboardAvoidingView, Image, ScrollView } from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons';
import Checker from '../../assets/workbrookAssets/checker.png';
import {
    Container,
    Header,
    Content,
    Textarea,
    Button,
    View,
    Thumbnail,
    Form,
    Item,
    Input,
    Label,
    Badge,
    Icon,
    Footer,
    Picker,
    Item as FormItem,
    Left,
    Body,
    CardItem,
    Card,
    Right,
    ListItem,
    List,
    CheckBox,
} from 'native-base';
import {
    Kaede,
    Hoshi,
    Jiro,
    Isao,
    Madoka,
    Akira,
    Hideo,
    Kohana,
    Makiko,
    Sae,
    Fumi,
} from 'react-native-textinput-effects';
import CustomizeHeader from '../frags/CustomizeHeader';

import { myspiner, temp, ShowNotification, AppInlineLoader } from '../../dependency/UtilityFunctions';
import Logo from '../../assets/workbrookAssets/logo.png';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import RBSheet from "react-native-raw-bottom-sheet";
import Interests from '../interests/Interests';
import Noti from "../../assets/workbrookAssets/noti.png";
import UserBl from "../../assets/workbrookAssets/userBl.png";
import ArrForward from "../../assets/workbrookAssets/arrfr.png";
import Shield from "../../assets/workbrookAssets/shield.png";
import Quest from "../../assets/workbrookAssets/quest.png";
import Info from "../../assets/workbrookAssets/info.png";
import Plus from "../../assets/workbrookAssets/plus.png";
import Remark from "../../assets/workbrookAssets/remark.png";
import Like from "../../assets/workbrookAssets/like.png";
import MoreIcon from "../../assets/workbrookAssets/more2.png";
import Profile from "../../assets/images/profile/dp2.png";
import cardPix from "../../assets/images/background/bg3_old.jpg";
import RNPickerSelect from 'react-native-picker-select';
import TagInput from 'react-native-tags-input';

import RNFetchBlob from 'react-native-fetch-blob'
import ImagePicker from 'react-native-image-picker';
import CloseModal from "../../assets/workbrookAssets/whitecloss.png";
import SuccessIcon from "../../assets/workbrookAssets/successPost.png";
import Modal from 'react-native-modal';
import AccountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import { text } from 'react-native-communications';
import DashboardStore from '../../stores/Dashboard';
import { hasSetIntrest } from '../../auth';
import Autocomplete from 'react-native-autocomplete-input';
import MultiSelect from 'react-native-multiple-select';
import { YOUR_CLOUDINARY_PRESET, YOUR_CLOUDINARY_NAME, requestCameraPermission, YOUR_CLOUDINARY_PRESET_JobBanner } from '../../dependency/UtilityFunctions';
import EmptyListComponent from '../../StatelessScreens/EmptyListComponent';


items = [{
    id: '92iijs7yta',
    name: 'Ondo',
}, {
    id: 'a0s0a8ssbsd',
    name: 'Ogun',
}, {
    id: '16hbajsabsd',
    name: 'Calabar',
}, {
    id: 'sdhyaysdj',
    name: 'Kaduna',
}, {
    id: 'suudydjsjd',
    name: 'Abuja',
}];

@observer
class Likes extends Component {
    constructor(props) {
        super(props);
        this.mapRef = null;
        this.state = {
        }
        this.ReloadPageData = this.ReloadPageData.bind(this);
         this.GotoUserProfile=this.GotoUserProfile.bind(this);

    }

    async componentDidMount() {
         DashboardStore.__GetJobLikes(1, 40);
    }
    ReloadPageData = () => {
 
        DashboardStore.__GetJobLikes(1, 40);

    };
 
    GotoUserProfile = (item) => {
        let thisUserId =item.user_id;
        DashboardStore._updateParameterV2(thisUserId, "User_id", "OtherProfile");
        this.props.navigation.navigate("otherProfile")
      };
  
    render() {

        const { JoblikeObject } = DashboardStore;
         let ErrorComponentDisplay = null;

        if (DashboardStore.isProcessing === true) {
            ErrorComponentDisplay = AppInlineLoader(DashboardStore.isProcessing)

        }
        else {
            ErrorComponentDisplay = <EmptyListComponent
                message={DashboardStore.JoblikeObject.ErrorMessage}
                onclickhandler={this.ReloadPageData}
            />
        }


        let PostLIkes =
            <FlatList
                data={JoblikeObject.mylikes}
                // Render Items
                renderItem={({ item, index }) => {
                    //   let Applicants=item.applicants
                    // let posterName=  `${poster.first_name}  ${poster.last_name} `
                    // let poster = item.created_by_profile
                    let LikesBy = item.first_name == null || item.last_name == null ? item.username : `${item.first_name}  ${item.last_name} `

                    return (
                        <List style={{ borderBottomWidth: 1, borderBottomColor: '#E5E5E5', paddingBottom: 15 }}>
                        <ListItem Button avatar style={{ display: 'flex', justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center', }}
                            onPress={()=>this.GotoUserProfile(item)}
                        >
                            <Left style={styles.leftAlignSty}>
                                <Thumbnail style={styles.applicantImg} source={{ uri: item.image_url }} />
                            </Left>
                            <Body style={[styles.leftAlignSty, { borderBottomWidth: 0, marginBottom: -3 }]}>
                                <Text style={styles.cardTextJob4}>{LikesBy}</Text>
                         
                            </Body>

                        </ListItem>
                    </List>
                    )
                }}
                // Item Key
                keyExtractor={(item, index) => String(index)}
                // Header (Title)
                // ListHeaderComponent={this.renderHeader}
                // Footer (Activity Indicator)
                // ListFooterComponent={this.renderFooter()}
                // On End Reached (Takes a function)
                onEndReached={this.retrieveMore}
            // How Close To The End Of List Until Next Data Request Is Made
            // onEndReachedThreshold={0}
            // Refreshing (Set To True When End Reached)
            // refreshing={this.state.refreshing}
            />

        return (

            <Container style={styles.container}>
                <CustomizeHeader
                    leftside='WhiteArr'
                    title='Likes'
                    rightside='empty'
                    statedColor='#1E93E2'
                    icon='md-close'
                // savehandler={this.proceednow}
                />
                
                <ScrollView
                    contentContainerStyle={{
                        padding: 35,
                    }}
                >
                   
          {
           JoblikeObject.mylikes.length > 0 ?
              <React.Fragment>

                {PostLIkes}

              </React.Fragment>
              :
              ErrorComponentDisplay

          }
                
                    {/* <List style={{ borderBottomWidth: 1, borderBottomColor: '#E5E5E5', paddingBottom: 15 }}>
                        <ListItem Button avatar style={{ display: 'flex', justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center', }}
                            onPress={() => { this.props.navigation.navigate("ViewApplicant") }}
                        >
                            <Left style={styles.leftAlignSty}>
                                <Thumbnail style={styles.applicantImg} source={Profile} />
                            </Left>
                            <Body style={[styles.leftAlignSty, { borderBottomWidth: 0, marginBottom: -3 }]}>
                                <Text style={styles.cardTextJob4}>Jane Dokaszhuk</Text>
                             
                            </Body>

                        </ListItem>
                    </List> */}
                  

                </ScrollView>



            </Container>


        );
    }
}
export default Likes;

const styles = StyleSheet.create({
    container: {
        backgroundColor: "white",
        display: "flex",
        flex: 1,
        justifyContent: "flex-start",
    },

    rmPadding: {
        paddingTop: 0,
        paddingRight: 0,
        paddingBottom: 0,
        paddingLeft: 0,
    },
    rmPaddingLR: {
        paddingLeft: 0,
        paddingRight: 0,
    },

    cardTextJob1: {
        color: "#2F2F2F",
        fontSize: 36,
        fontWeight: '600',

    },
    cardTextJob2: {
        color: "#BCBCBC",
        fontSize: 14,
        fontWeight: '600',
        marginBottom: 5,
    },
    cardTextJob2_2: {
        color: "#fff",
        fontSize: 14,
        fontWeight: '600',
    },
    cardTextJob3: {
        color: "#2F2F2F",
        fontSize: 13,
        fontWeight: '500',
    },
    cardTextJob4: {
        fontSize: 12,
        color: '#2F2F2F',
        fontWeight: '600'
    },

    cardTextJob5: {
        color: "#2F2F2F",
        fontSize: 10,
        fontWeight: '500',
    },
    cardTextJob7: {
        color: "#1E93E2",
        fontSize: 11,
        fontWeight: '500',
    },
    cardTextJob4_1: {
        color: "#fff",
        fontSize: 12,
        fontWeight: '500',
    },

    cardTextJob6: {
        color: "#1E93E2",
        fontSize: 11,
        fontWeight: '500',
    },
    cardText2: {
        color: "#FAFAFA",
        fontSize: 11,
    },
    cardText2_2: {
        color: "#FAFAFA",
        fontSize: 11,
        fontWeight: 'bold',
    },


    cardText1: {
        color: "#1E93E2",
        fontSize: 7,
    },
    bargeCount: {
        height: 20,
        width: 20,
        borderRadius: 20 / 2,
        backgroundColor: "#fff",
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    likeTabSty: {

        backgroundColor: "#1E93E2",
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        paddingTop: 30,
        paddingBottom: 10,
    },
    inputStyleWrap: {
        height: 38,
        backgroundColor: '#E5E5E5',
        borderRadius: 5,
    },
    headerWrap: {
        marginBottom: 13,
        marginTop: 20,
        paddingLeft: 25,
        paddingRight: 25,
    },
    serachbtnSty: {
        height: 54,
        borderRadius: 4,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#5ABC7A',
        marginTop: 24
    },
    ViewBtnSty: {
        height: 23,
        width: 50,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#1E93E2',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    btnStyIdc: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    applicantImg: {
        height: 31,
        width: 32,
        borderRadius: 32 / 2,
    },
    numberCount: {
        fontWeight: '600',
        fontSize: 12,
        color: '#1E93E2',
        marginRight: 11
    },
    leftAlignSty: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
    },
    IndicatorSty: {
        // width: 7,
        // height: 7,
        borderRadius: 3,
        backgroundColor: "#FF5964",
        // marginTop: -15,
        marginRight: -5,
        paddingBottom: 3,
        paddingTop: 3,
        paddingLeft: 7,
        paddingRight: 7,
        marginLeft: 3,
    },
    activeView: {
        color: "#FAFAFA",
        fontSize: 11,
        fontWeight: 'bold',
    },
    


});

import React, { Component, Fragment } from "react";
import {
  Text,
  StyleSheet,
  ImageBackground,
  Keyboard,
  TouchableOpacity,
} from "react-native";
import {
  Platform,
  KeyboardAvoidingView,
  Image,
  ScrollView,
  ActivityIndicator,
  Dimensions,
  FlatList,
  SafeAreaView,
} from "react-native";

import Ionicons from "react-native-vector-icons/Ionicons";
import Checker from "../../assets/workbrookAssets/checker.png";
import {
  Container,
  Header,
  Content,
  Textarea,
  Button,
  View,
  Thumbnail,
  Form,
  Item,
  Input,
  Label,
  Badge,
  Icon,
  Footer,
  Picker,
  Item as FormItem,
  Left,
  Body,
  CardItem,
  Card,
  Right,
  List,
  ListItem,
} from "native-base";
import {
  Kaede,
  Hoshi,
  Jiro,
  Isao,
  Madoka,
  Akira,
  Hideo,
  Kohana,
  Makiko,
  Sae,
  Fumi,
} from "react-native-textinput-effects";
import CustomizeHeader from "../frags/CustomizeHeader";

import {
  myspiner,
  temp,
  getLodgedInDetailsProfile,
  Communicator,
  _DocumentPicker,
  cloudinaryUpload,
  ShowNotification,
  AppInlineLoader,
  generateMyUniqueRefrence,
  ShareOptions,
} from "../../dependency/UtilityFunctions";
import Logo from "../../assets/workbrookAssets/logo.png";
import Toast from "react-native-toast-native";
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import Entypo from "react-native-vector-icons/Entypo";
import RBSheet from "react-native-raw-bottom-sheet";
import Interests from "../interests/Interests";
import Plus from "../../assets/workbrookAssets/plus.png";
import Remark from "../../assets/workbrookAssets/remark.png";
import Like from "../../assets/workbrookAssets/like.png";
import MoreIcon from "../../assets/workbrookAssets/more.png";
import SuccessIcon from "../../assets/workbrookAssets/success.png";
import Profile from "../../assets/images/profile/dp2.png";
import cardPix from "../../assets/images/background/bg3_old.jpg";
import CloseModal from "../../assets/workbrookAssets/whitecloss.png";
import Circle from "../../assets/workbrookAssets/circle.png";
import AccountStore from "../../stores/Account";
import { observer } from "mobx-react/native";
import { CustomAsset } from "../../../utils/assets";
import { text } from "react-native-communications";
import DashboardStore from "../../stores/Dashboard";
import { hasSetIntrest } from "../../auth";
import Modal from "react-native-modal";
import Share from "react-native-share";
import EmptyListComponent from "../../StatelessScreens/EmptyListComponent";
import { SvgUri } from "react-native-svg";

const url = "https://workbrook.contents.com/jobid/referercode";
const title = "Refer People you know. ";
const message =
  "Please check out this Job Oppurtunity. \n link not working yet. work inprogress. ";
const icon = "data:<data_type>/<file_extension>;base64,<base64_data>";
const options = Platform.select({
  ios: {
    activityItemSources: [
      {
        // For sharing url with custom title.
        placeholderItem: { type: "url", content: url },
        item: {
          default: { type: "url", content: url },
        },
        subject: {
          default: title,
        },
        linkMetadata: { originalUrl: url, url, title },
      },
      {
        // For sharing text.
        placeholderItem: { type: "text", content: message },
        item: {
          default: { type: "text", content: message },
          message: null, // Specify no text to share via Messages app.
        },
        linkMetadata: {
          // For showing app icon on share preview.
          title: message,
        },
      },
      {
        // For using custom icon instead of default text icon at share preview when sharing with message.
        placeholderItem: {
          type: "url",
          content: icon,
        },
        item: {
          default: {
            type: "text",
            content: `${message} ${url}`,
          },
        },
        linkMetadata: {
          title: message,
          icon: icon,
        },
      },
    ],
  },
  default: {
    title,
    subject: title,
    message: `${message} ${url}`,
  },
});
@observer
class PreviewPost extends Component {
  constructor(props) {
    super(props);
    this.mapRef = null;
    this.state = {
      email: "",
      filename: "",
      password: "",
      errorloading: false,
      isModalVisible: false,
      profile: {},
    };
    this.toggleModal = this.toggleModal.bind(this);
    this.toggleModalClose = this.toggleModalClose.bind(this);
    this.ApplyForJob = this.ApplyForJob.bind(this);
    this.ChangeResume = this.ChangeResume.bind(this);
    this.ApplyFOrJobOutSide = this.ApplyFOrJobOutSide.bind(this);
    this.toggleApplyModalShow = this.toggleApplyModalShow.bind(this);
    this.ReferJobs2People = this.ReferJobs2People.bind(this);
    this.ReloadPageData = this.ReloadPageData.bind(this);

    // this.retrieveMore = this.retrieveMore.bind(this)
  }
  async componentDidMount() {
    DashboardStore.UpdateNavigationState(this.props.navigation);

    let UserloginToken = await temp();
    // let loggedinUserdetails=await loggedinUserdetails();
    console.log(UserloginToken);
    DashboardStore._getAxiosInstance(UserloginToken);
    DashboardStore.__GetJobsOppurtunites(1, 30);

    let InnerProfile = await getLodgedInDetailsProfile();
    let isAvailable = InnerProfile.isAvailable;
    if (isAvailable === true) {
      let this_userprofile = InnerProfile.storeData;
      console.log(this_userprofile);
      console.log("InnerProfile");
      this.setState({
        profile: this_userprofile,
      });

      DashboardStore._UpdateLoggedInUserProfile(this_userprofile);
    }
  }
  toggleModal = (item) => {
    console.log(item);

    DashboardStore._updateParameterV2(item, "SingleJobOffer", "JobOppurtunity");

    DashboardStore._ToggleHomeModal(true);
  };
  toggleModalClose = () => {
    DashboardStore._ToggleHomeModal(false);
  };
  // retrieveMore = () => {
  retrieveMore = async () => {
    // alert("Loading More data...")
    ShowNotification("Loading More data...", true);
  };
  toggleRefModalShow = () => {
    DashboardStore._ToggleHomeModal(false);
    DashboardStore._ToggleReferModal(true);
  };
  toggleRefModalClose = () => {
    DashboardStore._ToggleReferModal(false);
  };
  ApplyFOrJobOutSide = (item) => {
    DashboardStore._updateParameterV2(item, "SingleJobOffer", "JobOppurtunity");

    DashboardStore._ToggleHomeModal(false);
    DashboardStore._ToggleApplyModal(true);
  };
  LikeAndUnlikeJob = (item) => {
    let job_id = item.id;
    DashboardStore.__LikeUnLike(job_id);
  };
  SaveJobForLater = () => {
    let item = DashboardStore.JobOppurtunity.SingleJobOffer;

    let job_id = item.id;
    DashboardStore.__SaveJobForLater(job_id);
  };
  toggleApplyModalShow = () => {
    // DashboardStore._updateParameterV2(item, "SingleJobOffer", "JobOppurtunity");

    DashboardStore._ToggleHomeModal(false);
    DashboardStore._ToggleApplyModal(true);
  };
  toggleApplyModalClose = () => {
    DashboardStore._ToggleApplyModal(false);
  };

  toggleSuccessModalShow = () => {
    DashboardStore._ToggleApplyModal(false);
    DashboardStore._ToggleSucessModal(true);
  };
  ApplyForJob = () => {
    // DashboardStore._ToggleApplyModal(false)
    // DashboardStore._ToggleSucessModal(true)
    // alert("heyman")
    DashboardStore.__ApplyForJob();
  };
  ReferJobs2People = async () => {
    let item = DashboardStore.JobOppurtunity.SingleJobOffer;
    DashboardStore._ToggleHomeModal(false);
    // DashboardStore._ToggleApplyModal(true)
    // Share.open(options);

    const { profile } = this.state;
    let invitationcode = profile.invitation_code;
    let job_id = item.id;
    console.log(item);
    console.log("job detailsddd");
    DashboardStore._ToggleHomeModal(false);
    // DashboardStore._ToggleApplyModal(true)
    // Share.open(options);
    // let path=`refers?referralCode=${invitationcode}&optionbar=${job_id}`;
    let GeneratedURL = await generateMyUniqueRefrence(invitationcode, job_id);
    // dynamicEventLink(path);

    const url = GeneratedURL;
    const title = "New jobs on workbrook";
    const message = "We think you may be interested in these opportunities ";
    let options_ = ShareOptions(url, title, message);

    Share.open(options_)
      .then((res) => {
        console.log(res);
        console.log("res");
      })
      .catch((err) => {
        err && console.log(err);
        console.log("Error from share");
      });
  };
  ChangeResume = async () => {
    // alert("kkkkk")
    let resp = await _DocumentPicker();
    console.log(resp);
    console.log("tuface");
    if (resp !== false || resp !== "cancel") {
      const source = {
        uri: resp.uri,
        type: resp.type,
        name: resp.name,
      };
      this.setState({
        filename: resp.name,
      });
      console.log("Bacl from picker");
      let FileUrl = await cloudinaryUpload(source);
      if (FileUrl !== false) {
        console.log("FileUrl");
        console.log(FileUrl);
        console.log("FileUrl_done");
        DashboardStore._updateParameterV2(
          FileUrl,
          "resume",
          "profileupdatedata"
        );
        //   ShowNotification('Done Uploading ....', true);
        DashboardStore.__UpdateProfile_external();
        ShowNotification("Done Saving your cv", true);
      }
    }
  };

  toggleSuccessModalClose = () => {
    // DashboardStore._ToggleSucessModal(false)
    DashboardStore._ToggleApplyModal(false);
    DashboardStore.isJobApplySuccessful = false;
  };
  ReloadPageData = () => {
    DashboardStore.__GetJobsOppurtunites(1, 10);
  };
  renderFooter = () => {
    try {
      // Check If Loading
      if (DashboardStore.isProcessing) {
        return <ActivityIndicator />;
      } else {
        return null;
      }
    } catch (error) {
      console.log(error);
    }
  };
  render() {
    const { profile } = this.state;
    const { LoggedInUserProfile } = DashboardStore;
    let Fullname = "";
    if (
      LoggedInUserProfile.first_name == null ||
      LoggedInUserProfile.last_name == null
    ) {
      Fullname = `${LoggedInUserProfile.username} `;
    } else {
      Fullname = `${LoggedInUserProfile.first_name} ${LoggedInUserProfile.last_name} `;
    }
    const { SingleJobOffer } = DashboardStore.JobOppurtunity;
    let SkillRequired = null;
    if (
      Object.keys(SingleJobOffer).length > 0 &&
      SingleJobOffer.constructor === Object
    ) {
      SkillRequired = SingleJobOffer.skills_required.map((item, index) => {
        return (
          <ListItem
            avatar
            style={[
              styles.ListItemWrapCircle,
              {
                marginLeft: 0,
                paddingLeft: 15,
                paddingTop: 0,
                paddingBottom: 0,
              },
            ]}
            key={index}
          >
            <Left style={styles.ListLeftSty}>
              <View style={{ width: 15 }}>
                <SvgUri
                  width="11px"
                  height="11px"
                  uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593602698/Mobile_Assets/dot_n0m4cb.svg"
                />
                {/* <Thumbnail square source={Circle} style={styles.circlesty} /> */}
              </View>
              <Text style={[styles.cupText1Sz2, { marginLeft: 10 }]}>
                {/* Earn points by completing tasks */}
                {item}
              </Text>
            </Left>
          </ListItem>
        );
      });
    }

    let ErrorComponentDisplay = null;

    if (DashboardStore.isProcessing === true) {
      ErrorComponentDisplay = AppInlineLoader(DashboardStore.isProcessing);
    } else {
      ErrorComponentDisplay = (
        <EmptyListComponent
          message={DashboardStore.JobOppurtunity.ErrorMessage}
          onclickhandler={this.ReloadPageData}
        />
      );
    }

    return (
      <Container style={styles.container}>
        <Button
          full
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "flex-end",
            paddingLeft: 15,
            paddingRight: 15,
            backgroundColor: "#1E93E2",
          }}
          onPress={() => {
            this.props.navigation.goBack();
          }}
        >
          <SvgUri
            width="18px"
            height="18px"
            uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1594146750/Mobile_Assets/close_white_iocsmo.svg"
          />
        </Button>
        <ScrollView>
          <View style={[{ padding: 15, backgroundColor: "#F0F0F0" }]}>
            <View style={styles.homeModalwrap}>
              {/* <Button onPress={this.toggleModalClose} style={{ backgroundColor: "transparent", position: 'absolute', right: 0, top: 0, padding: 10, elevation: 0, zIndex: 9999, }}>
                                    <Image source={CloseModal} style={{ width: 19, height: 19, }} />
                                </Button> */}
              <View>
                <View style={styles.homeModalHeader}>
                  <Text
                    style={[styles.homeModalwrapText1, { marginBottom: 2 }]}
                  >
                    {/* {JSON.stringify(SingleJobOffer)} */}
                    {/* Finance Manager */}
                    {SingleJobOffer.role_title}
                  </Text>
                  <Text
                    style={[
                      styles.homeModalwrapText1,
                      { fontSize: 11, textAlign: "center" },
                    ]}
                  >
                    {/* Lorem ipsum dolor sit amet, consectetuer adipiscing elit, */}
                  </Text>
                </View>
                <View style={styles.homeModalContentWrap}>
                  <View style={{ flex: 1 }}>
                    <Text
                      style={[
                        styles.cardTextJob1,
                        { fontSize: 12, fontWeight: "500" },
                      ]}
                    >
                      Company Name
                    </Text>
                    <Text style={[styles.cardTextJob1, { fontWeight: "300" }]}>
                      {SingleJobOffer.campany_name}
                    </Text>
                  </View>
                  {/* <Text style={[styles.cardTextJob1, { fontSize: 12 }]}>
                                          
                                            </Text> */}
                  <Text
                    style={[
                      styles.cardTextJob1,
                      { fontSize: 12, marginTop: 20, fontWeight: "500" },
                    ]}
                  >
                    About the company
                  </Text>
                  <Text style={[styles.cardTextJob1, { fontWeight: "300" }]}>
                    {/* Explore hundreds of HR training for all levels of Human resource professionals. Join the next Human Resources training class and network with other HR professionals. */}
                    {SingleJobOffer.about_company}
                  </Text>
                  <View style={{ flex: 1 }}>
                    <Text
                      style={[
                        styles.cardTextJob1,
                        { fontSize: 12, marginTop: 20, fontWeight: "500" },
                      ]}
                    >
                      Company Industry
                    </Text>
                    <Text style={[styles.cardTextJob1, { fontWeight: "300" }]}>
                      {SingleJobOffer.industry}
                    </Text>
                  </View>
                  <View style={{ flex: 1 }}>
                    <Text
                      style={[
                        styles.cardTextJob1,
                        { fontSize: 12, marginTop: 20, fontWeight: "500" },
                      ]}
                    >
                      Skill Required
                    </Text>

                    <List style={[styles.ListWrap2]}>{SkillRequired}</List>
                  </View>
                  <View style={{ flex: 1 }}>
                    <Text
                      style={[
                        styles.cardTextJob1,
                        { fontSize: 12, marginTop: 20, fontWeight: "500" },
                      ]}
                    >
                      Skill Match
                    </Text>
                    <Text style={[styles.cardTextJob1, { fontWeight: "300" }]}>
                      {SingleJobOffer.skills_match != null
                        ? `${SingleJobOffer.skills_match.toFixed(2)} % match`
                        : "nill"}
                    </Text>
                  </View>
                  <Text
                    style={[
                      styles.cardTextJob1,
                      { fontSize: 12, marginTop: 20, fontWeight: "500" },
                    ]}
                  >
                    What is in it for you
                  </Text>
                  <Text style={[styles.cardTextJob1, { fontWeight: "300" }]}>
                    {/* Explore hundreds of HR training for all levels of Human resource professionals. Join the next Human Resources training class and network with other HR professionals. */}
                    {SingleJobOffer.applicant_benefit}
                  </Text>
                  {/* transparent */}
                  <View style={{ flex: 1 }}>
                    <Text
                      style={[
                        styles.cardTextJob1,
                        { fontSize: 12, marginTop: 20, fontWeight: "500" },
                      ]}
                    >
                      Type of Employment
                    </Text>
                    <Text style={[styles.cardTextJob1, { fontWeight: "300" }]}>
                      {SingleJobOffer.job_type == null
                        ? "Nill"
                        : SingleJobOffer.job_type}
                    </Text>
                  </View>
                  <View style={{ flex: 1 }}>
                    <Text
                      style={[
                        styles.cardTextJob1,
                        { fontSize: 12, marginTop: 20, fontWeight: "500" },
                      ]}
                    >
                      Job Location
                    </Text>
                    <Text style={[styles.cardTextJob1, { fontWeight: "300" }]}>
                      {SingleJobOffer.location == null
                        ? "Nill"
                        : SingleJobOffer.location}
                    </Text>
                  </View>
                </View>

                {SingleJobOffer.isSaved == 0 ? (
                  <Button
                    full
                    transparent
                    style={{
                      elevation: 0,
                      height: Platform.OS === "ios" ? 40 : 20,
                      marginBottom: 12,
                    }}
                    onPress={this.SaveJobForLater}
                  >
                    <Text style={[styles.cardTextJob4, { color: "#2F2F2F" }]}>
                      save for later
                    </Text>
                  </Button>
                ) : (
                  <Button
                    full
                    transparent
                    style={{
                      elevation: 0,
                      height: Platform.OS === "ios" ? 40 : 20,
                      marginBottom: 12,
                    }}
                  >
                    <Text style={[styles.cardTextJob4, { color: "#1E93E2" }]}>
                      saved
                    </Text>
                  </Button>
                )}
              </View>
              <View
                style={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                  width: "100%",
                  backgroundColor: "red",
                  height: 54,
                }}
              >
                <Button
                  full
                  style={[styles.btnSave, { flex: 1 }]}
                  // onPress={this.ApplyForJob}
                  onPress={() => this.ApplyFOrJobOutSide(SingleJobOffer)}
                >
                  <Text style={[styles.btnCheckyText, { color: "#fff" }]}>
                    {DashboardStore.isProcessing === true
                      ? "Processing"
                      : "Apply"}
                  </Text>
                </Button>
                <Button
                  full
                  style={[
                    styles.btnSave,
                    { backgroundColor: "#5ABC7A", flex: 1 },
                  ]}
                  onPress={this.ReferJobs2People}
                >
                  <Text style={[styles.btnCheckyText, { color: "#fff" }]}>
                    {DashboardStore.isProcessing === true
                      ? "Processing"
                      : "Refer"}
                  </Text>
                </Button>
              </View>
            </View>

            <Modal
              style={styles.MContainer}
              isVisible={DashboardStore.isHomeModalVisible}
            ></Modal>
            <Modal
              style={styles.MContainer}
              isVisible={DashboardStore.isReferModalVisible}
            >
              <Button
                onPress={this.toggleRefModalClose}
                style={{
                  backgroundColor: "transparent",
                  position: "absolute",
                  right: 10,
                  top: -10,
                  padding: 20,
                  elevation: 0,
                }}
              >
                <SvgUri
                  width="19px"
                  height="19px"
                  uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593711047/Mobile_Assets/closeWht_oohxti.svg"
                />
              </Button>
              <View style={styles.Mwrapper}>
                {/* <Button onPress={this.toggleRefModalClose} style={{ backgroundColor: "transparent", position: 'absolute', right: 0, top: -40, padding: 10, elevation: 0 }}><Image source={CloseModal} style={{ width: 11, height: 10, }} /></Button> */}
                <View
                  style={{
                    paddingTop: 42,
                    paddingLeft: 42,
                    paddingRight: 42,
                    paddingBottom: 10,
                  }}
                >
                  <Text
                    style={(styles.cardText1, { fontSize: 14, color: "#000" })}
                  >
                    Your Information
                  </Text>

                  <Item style={[styles.profileinput]}>
                    <Input
                      style={styles.profileinputText}
                      placeholderTextColor={"#A7A7A7"}
                      placeholder="Your full name"
                    />
                  </Item>

                  <Item style={[styles.profileinput]}>
                    <Input
                      style={styles.profileinputText}
                      placeholderTextColor={"#A7A7A7"}
                      placeholder="Your email address"
                    />
                  </Item>
                  <Text
                    style={
                      (styles.cardText1,
                      {
                        fontSize: 14,
                        marginBottom: 15,
                        marginTop: 15,
                        color: "#000",
                      })
                    }
                  >
                    Tell us about your friend
                  </Text>
                  <Item style={[styles.profileinput]}>
                    <Input
                      style={styles.profileinputText}
                      placeholderTextColor={"#A7A7A7"}
                      placeholder="Full Name"
                    />
                  </Item>
                  <Item style={[styles.profileinput]}>
                    <Input
                      style={styles.profileinputText}
                      placeholderTextColor={"#A7A7A7"}
                      placeholder="Email"
                    />
                  </Item>
                  <Item style={[styles.profileinput2]}>
                    <Textarea
                      rowSpan={4}
                      placeholderTextColor={"#A7A7A7"}
                      placeholder="Additional comments"
                    />
                  </Item>
                </View>
                <Button
                  style={[
                    styles.btnSave,
                    { marginTop: 15, backgroundColor: "#5ABC7A" },
                  ]}
                  onPress={this.toggleRefModalClose}
                >
                  <Text style={[styles.btnCheckyText, { color: "#fff" }]}>
                    {DashboardStore.isProcessing === true
                      ? "Processing"
                      : "Email your friend"}
                  </Text>
                </Button>
              </View>
            </Modal>
            <Modal
              style={styles.MContainer}
              isVisible={DashboardStore.isApplyModalVisibleqqqqq}
            >
              <Button
                onPress={this.toggleApplyModalClose}
                style={{
                  backgroundColor: "transparent",
                  position: "absolute",
                  right: 10,
                  top: 40,
                  padding: 20,
                  elevation: 0,
                }}
              >
                <SvgUri
                  width="19px"
                  height="19px"
                  uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593711047/Mobile_Assets/closeWht_oohxti.svg"
                />
              </Button>
              <View style={[styles.MwrapperNew, { height: 400 }]}>
                <View
                  style={{
                    paddingTop: 42,
                    paddingLeft: 42,
                    paddingRight: 42,
                    paddingBottom: 10,
                  }}
                >
                  <Text
                    style={
                      (styles.cardText1,
                      {
                        fontSize: 18,
                        color: "#1E93E2",
                        textAlign: "center",
                        fontWeight: "bold",
                      })
                    }
                  >
                    {SingleJobOffer.role_title}
                  </Text>
                  <Text
                    style={
                      (styles.cardText1,
                      {
                        fontSize: 11,
                        color: "#1E93E2",
                        textAlign: "center",
                        fontWeight: "normal",
                      })
                    }
                  >
                    {/* Guaranty Trust Bank, Lagos */}
                    {`  ${SingleJobOffer.campany_name}  ${SingleJobOffer.location}`}
                  </Text>

                  <Item style={[styles.profileinput, { marginTop: 47 }]}>
                    <Input
                      style={styles.profileinputText}
                      placeholderTextColor={"#A7A7A7"}
                      placeholder="Your full name"
                      defaultValue={Fullname}
                    />
                  </Item>

                  <Item style={[styles.profileinput]}>
                    <Input
                      style={styles.profileinputText}
                      placeholderTextColor={"#A7A7A7"}
                      placeholder="Your email address"
                      defaultValue={LoggedInUserProfile.email}
                    />
                  </Item>

                  {/* <Item style={[styles.profileinput,]}>
                                            <Input
                                                style={styles.profileinputText}
                                                placeholderTextColor={'#A7A7A7'}
                                                placeholder="Your phone number"
                                                defaultValue={Fullname}

                                            />
                                        </Item> */}

                  {LoggedInUserProfile.resume != null ? (
                    <React.Fragment>
                      <CardItem
                        style={{
                          width: "100%",
                          backgroundColor: "transparent",
                          paddingTop: 0,
                        }}
                      >
                        <Left
                          style={{
                            alignItems: "flex-start",
                            borderBottomWidth: 1,
                            borderColor: "#E5E5E5",
                            paddingBottom: 5,
                          }}
                        >
                          <Button
                            style={[
                              styles.btnChecky,
                              {
                                borderWidth: 0,
                                paddingLeft: 0,
                                paddingRight: 0,
                              },
                            ]}
                            onPress={() =>
                              Communicator("web", LoggedInUserProfile.resume)
                            }
                          >
                            <Text
                              style={styles.btnCheckyText}
                            >{` Your Current Resume`}</Text>
                          </Button>
                        </Left>
                      </CardItem>
                      <View>
                        <Text>OR</Text>
                      </View>

                      <React.Fragment>
                        <Button
                          full
                          transparent
                          style={{ elevation: 0, height: 20, marginTop: 20 }}
                          onPress={this.ChangeResume}
                        >
                          <Text
                            style={[
                              styles.cardTextJob4,
                              { color: "#1E93E2", fontSize: 16 },
                            ]}
                          >
                            Upload your resume (max 2MB)
                          </Text>
                        </Button>
                        <View>
                          <Text
                            style={[
                              styles.cardTextJob4,
                              { color: "#1E93E2", fontSize: 12 },
                            ]}
                          >
                            {this.state.filename}
                          </Text>
                        </View>
                      </React.Fragment>
                    </React.Fragment>
                  ) : (
                    <React.Fragment>
                      <Button
                        full
                        transparent
                        style={{ elevation: 0, height: 20, marginTop: 20 }}
                        onPress={this.ChangeResume}
                      >
                        <Text
                          style={[
                            styles.cardTextJob4,
                            { color: "#1E93E2", fontSize: 16 },
                          ]}
                        >
                          Upload your resume (max 2MB)
                        </Text>
                      </Button>
                      <View>
                        <Text
                          style={[
                            styles.cardTextJob4,
                            { color: "#1E93E2", fontSize: 12 },
                          ]}
                        >
                          {this.state.filename}
                        </Text>
                      </View>
                    </React.Fragment>
                  )}
                </View>
                {LoggedInUserProfile.resume != null ? (
                  <Button
                    full
                    style={[styles.btnSave, { marginTop: 15, borderRadius: 0 }]}
                    onPress={this.ApplyForJob}
                  >
                    <Text style={[styles.btnCheckyText, { color: "#fff" }]}>
                      {DashboardStore.isProcessing === true
                        ? "Processing..."
                        : "Apply"}
                    </Text>
                  </Button>
                ) : (
                  <Button
                    full
                    style={[styles.btnSave, { marginTop: 15, borderRadius: 0 }]}
                    //  onPress={this.ApplyForJob}
                    disabled={true}
                  >
                    <Text style={[styles.btnCheckyText, { color: "#fff" }]}>
                      {DashboardStore.isProcessing === true
                        ? "Processing"
                        : "Upload your cv before procceding"}
                    </Text>
                  </Button>
                )}
              </View>
            </Modal>
            <Modal
              style={styles.MContainer}
              isVisible={DashboardStore.isJobApplySuccessful}
            >
              <Button
                onPress={this.toggleSuccessModalClose}
                style={{
                  backgroundColor: "transparent",
                  position: "absolute",
                  right: 10,
                  top: 40,
                  padding: 20,
                  elevation: 0,
                }}
              >
                <SvgUri
                  width="19px"
                  height="19px"
                  uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1593711047/Mobile_Assets/closeWht_oohxti.svg"
                />
              </Button>
              <View style={[styles.Mwrapper, { height: 380 }]}>
                <View
                  style={{
                    paddingLeft: 29,
                    paddingRight: 29,
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <SvgUri
                    style={{ marginBottom: 17 }}
                    width="129px"
                    height="129px"
                    uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1590846662/Mobile_Assets/checkBlue_ajznt3.svg"
                  />
                  <Text
                    style={
                      (styles.cardText1,
                      {
                        fontSize: 16,
                        color: "#1E93E2",
                        textAlign: "center",
                        fontWeight: "600",
                      })
                    }
                  >
                    Congratulations!
                  </Text>
                  <Text
                    style={
                      (styles.cardText1,
                      {
                        fontSize: 16,
                        color: "#1E93E2",
                        textAlign: "center",
                        fontWeight: "600",
                      })
                    }
                  >
                    You have successfully applied.
                  </Text>
                </View>
              </View>
            </Modal>
          </View>
        </ScrollView>
      </Container>
    );
  }
}
export default PreviewPost;

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#F3F3F3",
    display: "flex",
    flex: 1,
    justifyContent: "flex-start",
  },
  contentWrap: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    // marginTop: 10,
    marginBottom: 15,
    padding: 15,
    height: 55,
    backgroundColor: "#fff",
  },
  contentWrap2: {
    display: "flex",
    flexDirection: "row",
    flexWrap: "wrap",
    justifyContent: "flex-start",
  },
  likeCaroCard: {
    display: "flex",
    justifyContent: "space-between",
    padding: 15,
    backgroundColor: "#FFFFFF",
    width: 150,
    height: 155,
    marginRight: 10,
  },
  rmPadding: {
    paddingTop: 0,
    paddingRight: 0,
    paddingBottom: 0,
    paddingLeft: 0,
  },
  rmPaddingLR: {
    paddingLeft: 0,
    paddingRight: 0,
  },
  innercard: {
    display: "flex",
    justifyContent: "space-between",
    // padding: 10,
    paddingLeft: 10,
    paddingRight: 10,
    // paddingBottom: 0,
    backgroundColor: "#ffffff",
    width: "100%",
    flex: 1,
    // height: 155,
    // marginRight: 10
  },
  likeCaroCard2: {
    display: "flex",
    // justifyContent: 'space-between',
    backgroundColor: "#FFFFFF",
    width: 150,
    height: 155,
    marginRight: 10,
  },
  cardTopper: {
    padding: 5,
    // backgroundColor: "#FAFAFA",
    // borderBottomColor: "#E5E5E5",
    // borderBottomWidth: 0.5,
  },
  cardTopper1: {
    backgroundColor: "#FAFAFA",
    borderTopColor: "#E5E5E5",
    borderTopWidth: 0.5,
  },
  cardText1: {
    color: "#2F2F2F",
    fontWeight: "500",
    fontSize: 12,
  },
  cardText1_2: {
    color: "#2F2F2F",
    fontSize: 12,
  },
  cardTextJob1: {
    color: "#2F2F2F",
    fontSize: 11,
  },
  cardTextJob2: {
    color: "#676767",
    fontSize: 9,
    fontWeight: "300",
  },
  cardTextJob3: {
    color: "#2F2F2F",
    fontSize: 10,
    fontWeight: "500",
  },
  cardTextJob4: {
    color: "#1E93E2",
    fontSize: 11,
    fontWeight: "500",
  },
  cardTextJob4_1: {
    color: "#1E93E2",
    fontSize: 12,
    fontWeight: "500",
  },
  cardTextJob5: {
    color: "#1E93E2",
    fontSize: 13,
    fontWeight: "600",
  },
  cardText2: {
    color: "#2F2F2F",
    fontSize: 11,
    lineHeight: 13,
    maxHeight: 40,
  },
  cardText3: {
    color: "#676767",
    fontSize: 9,
    fontWeight: "300",
  },
  thumbnailSty: {
    height: 24,
    width: 24,
    // lineHeight: 13,
  },
  thumbnailtextSty1: {
    fontSize: 11,
    color: "#2F2F2F",
  },
  thumbnailtextSty2: {
    fontSize: 9,
    fontWeight: "300",
    color: "#676767",
  },
  cardHeader: {
    backgroundColor: "#1E93E2",
    height: 24,
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    paddingLeft: 20,
  },
  dotstyle: {
    width: 10,
    height: 10,
    borderRadius: 5,
    borderWidth: 3,
    borderColor: "rgba(237, 237, 237, 0.7)",
    marginRight: 8,
  },
  likebottomCard: {
    height: 170,
    width: "100%",
    backgroundColor: "#1E93E2",
    marginTop: 10,
    padding: 20,
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  likebottomCardSide1: {
    width: "72%",
    display: "flex",
  },
  recruiter: {
    width: 198,
    height: 32,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 10,
    elevation: 0,
  },
  profileinput: {
    marginLeft: 0,
    borderWidth: 1,
    borderColor: "#E5E5E5",
    height: 50,
    // marginBottom: 15
  },
  profileinput2: {
    marginLeft: 0,
    borderWidth: 1,
    borderColor: "#E5E5E5",
    // height: 50,
    marginTop: 15,
  },
  profileinputText: {
    height: "100%",
    color: "#2F2F2F",
    fontSize: 14,
    fontWeight: "300",
    marginBottom: -15,
  },
  // Modal style =============================================================
  MContainer: {
    display: "flex",
    justifyContent: "center",
    flexDirection: "row",
    alignItems: "center",
  },
  Mwrapper: {
    height: 510,
    width: "90%",
    backgroundColor: "#fff",
    // padding: 15,
    display: "flex",
    justifyContent: "space-around",
    // alignItems: 'center'
  },
  MwrapperNew: {
    height: 510,
    width: "90%",
    backgroundColor: "#fff",
    // padding: 15,
    display: "flex",
    justifyContent: "space-between",
    // alignItems: 'center'
  },
  homeModalwrap: {
    // height: 519,
    width: "100%",
    backgroundColor: "#fff",
    justifyContent: "space-between",
    // alignItems: 'center'
  },
  homeModalHeader: {
    height: 89,
    width: "100%",
    backgroundColor: "#1E93E2",
    padding: 15,
    display: "flex",
    justifyContent: "space-around",
    alignItems: "center",
  },
  homeModalContentWrap: {
    width: "100%",
    backgroundColor: "#fff",
    padding: 35,
    // paddingBottom: 20,
    display: "flex",
  },
  homeModalwrapText1: {
    fontSize: 60,
    fontSize: 16,
    color: "#FAFAFA",
  },
  btnSave: {
    height: 54,
    backgroundColor: "#1E93E2",
    elevation: 0,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  // Modal Style ends=========================================================
  ListWrap2: {
    width: "100%",
    justifyContent: "center",
    paddingRight: "0%",
    paddingLeft: 0,
    marginLeft: 0,
  },
  ListItemWrapCircle: {
    width: "100%",
    paddingBottom: 10,
    // paddingTop: 10,
    // backgroundColor:"yellow"
  },
  ListLeftSty: {
    alignItems: "center",
    paddingTop: 5,
    paddingBottom: 5,
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    // backgroundColor:"yellow"
  },
  cupText1Sz2: {
    fontSize: 11,
    // fontWeight: '300',
    color: "#2F2F2F",
  },
  circlesty: {
    height: 11,
    width: 11,
  },
  btnChecky: {
    height: 28,
    borderWidth: 1,
    borderColor: "#1E93E2",
    borderRadius: 5,
    backgroundColor: "transparent",
    elevation: 0,
    paddingLeft: 28,
    paddingRight: 28,
    marginRight: 5,
    marginBottom: 5,
  },
  btnCheckyText: {
    fontSize: 11,
    fontWeight: "bold",
    color: "#1E93E2",
  },
});

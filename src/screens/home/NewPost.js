import React, { Component, Fragment } from 'react';
import { Text, StyleSheet, ImageBackground, Keyboard, TouchableOpacity, Alert } from 'react-native';
import { KeyboardAvoidingView, Image, ScrollView } from 'react-native';
import { StackActions } from 'react-navigation';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Checker from '../../assets/workbrookAssets/checker.png';
import {
    Container,
    Header,
    Content,
    Textarea,
    Button,
    View,
    Thumbnail,
    Form,
    Item,
    Input,
    Label,
    Badge,
    Icon,
    Footer,
    Picker,
    Item as FormItem,
    Left,
    Body,
    CardItem,
    Card,
    Right,
    ListItem,
    List,
    CheckBox,
} from 'native-base';
import {
    Kaede,
    Hoshi,
    Jiro,
    Isao,
    Madoka,
    Akira,
    Hideo,
    Kohana,
    Makiko,
    Sae,
    Fumi,
} from 'react-native-textinput-effects';
import CustomizeHeader from '../frags/CustomizeHeader';

import { myspiner, temp, ShowNotification } from '../../dependency/UtilityFunctions';
import Logo from '../../assets/workbrookAssets/logo.png';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import RBSheet from "react-native-raw-bottom-sheet";
import Interests from '../interests/Interests';
import Noti from "../../assets/workbrookAssets/noti.png";
import UserBl from "../../assets/workbrookAssets/userBl.png";
import ArrForward from "../../assets/workbrookAssets/arrfr.png";
import Shield from "../../assets/workbrookAssets/shield.png";
import Quest from "../../assets/workbrookAssets/quest.png";
import Info from "../../assets/workbrookAssets/info.png";
import Plus from "../../assets/workbrookAssets/plus.png";
import Remark from "../../assets/workbrookAssets/remark.png";
import Like from "../../assets/workbrookAssets/like.png";
import MoreIcon from "../../assets/workbrookAssets/more2.png";
import Profile from "../../assets/images/profile/dp2.png";
import cardPix from "../../assets/images/background/bg3_old.jpg";
import RNPickerSelect from 'react-native-picker-select';
import TagInput from 'react-native-tags-input';

import RNFetchBlob from 'react-native-fetch-blob'
import ImagePicker from 'react-native-image-picker';
import CloseModal from "../../assets/workbrookAssets/whitecloss.png";
import SuccessIcon from "../../assets/workbrookAssets/successPost.png";
import Modal from 'react-native-modal';
import AccountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import { text } from 'react-native-communications';
import DashboardStore from '../../stores/Dashboard';
import { hasSetIntrest } from '../../auth';
import Autocomplete from 'react-native-autocomplete-input';
import MultiSelect from 'react-native-multiple-select';
import { YOUR_CLOUDINARY_PRESET, YOUR_CLOUDINARY_NAME, requestCameraPermission, YOUR_CLOUDINARY_PRESET_JobBanner } from '../../dependency/UtilityFunctions';
import { SvgUri } from 'react-native-svg';
import GooglePlacesInput from '../frags/GooglePlacesInput';
import * as Animatable from 'react-native-animatable';



items = [{
    id: 'Ondo',
    name: 'Ondo',
}, {
    id: 'Ogun',
    name: 'Ogun',
}, {
    id: 'Calabar',
    name: 'Calabar',
}, {
    id: 'Kaduna',
    name: 'Kaduna',
}, {
    id: 'Abuja',
    name: 'Abuja',
}];

let NumberOfStaff = [
    {
        id: 'Sole Employee',
        name: 'Sole Employee',
    },
    {
        id: '2-20',
        name: '2-20',
    },
    {
        id: '21-50',
        name: '21-50',
    }, {
        id: '51-200',
        name: '51-200',
    }, {
        id: '501- 1000',
        name: '501- 1000',
    },
    {
        id: '1001- 10,000',
        name: '1001- 10,000',
    },
    {
        id: '10001- 50,000',
        name: '10001- 50,000',
    },
    {
        id: '50,001+',
        name: '50,001+',
    }
];
let defaultindustries = [{
    id: 'Tech',
    name: 'Tech',
}, {
    id: 'business',
    name: 'business',
}, {
    id: '16hbajsabsd',
    name: 'Calabar',
}, {
    id: 'sdhyaysdj',
    name: 'Kaduna',
}, {
    id: 'suudydjsjd',
    name: 'Abuja',
}];
@observer
class Settings extends Component {
    constructor(props) {
        super(props);
        this.mapRef = null;
        this.state = {
            email: '',
            password: '',
            errorloading: false,
            AmCompanyRep: false,
            selectedItems: [],
            industry: [],
            numberOfStaff: [],
            jobLevel: [],
            jobType: [],
            JobIntrests: [],
            tags: {
                tag: '',
                tagsArray: []
            },

            avatarSource: null,
            cover_pix: null,
            isImagefromdevice: false,
            loader_visible: false,
            showsaveBtn: false,
            uploading: false,
            file2upload: null,
            ViewMode: '',
            isToRefTimeLineOrToHireRecruter: '',


        }
        selectedItems = [];

        this.Post2Referrals = this.Post2Referrals.bind(this)
        this.changeAmCompanyRep = this.changeAmCompanyRep.bind(this)
        this._selectPhotoTapped = this._selectPhotoTapped.bind(this)
        this._uploadImage = this._uploadImage.bind(this)
        this.toggleSuccessPostModalShow = this.toggleSuccessPostModalShow.bind(this)
        this.Post2RecruiterDashboard = this.Post2RecruiterDashboard.bind(this)
        this.Update_Post2RecruiterDashboard = this.Update_Post2RecruiterDashboard.bind(this)
        this.toggleSuccessPostModalClose = this.toggleSuccessPostModalClose.bind(this)
    }
    onSelectedItemsChange = selectedItems => {
        this.setState({ selectedItems });
    };
    updateTagState = (state) => {
        this.setState({
            tags: state
        })
    };
    async Post2Referrals() {

        const { JobIntrests, AmCompanyRep, showsaveBtn } = this.state;
        const { JobPosting } = DashboardStore
        // if(showsaveBtn===false){
        //        let message="Please choose job banner";
        //             ShowNotification(message, true);
        // }
        // else{
        //   let rr= await  this.cloudinaryUpload();
        //    console.log("proceed")
        // }

        // const{JobPosting}=DashboardStore
        //     let companyName=JobPosting.companyName;
        //    let  industry=JobPosting.industry;
        //     let aboutCompany=JobPosting.aboutCompany;
        //    let  location=JobPosting.location;
        //     let numberOfStaff=JobPosting.numberOfStaff;
        //    let  roleTitle=JobPosting.roleTitle;
        //    let  jobLevel=JobPosting.jobLevel;
        //   let   wordForApplicant=JobPosting.wordForApplicant;


        //    "additional_files":["Link1","Link1","Link1"],
        //   "isCompanyRepresentative":1,
        //   "isToRefTimeLineOrToHireRecruter":2

        // if(JobIntrests.length<1){
        //     let message="Select 1 or More Skills required (Intrest) for the job";
        //     ShowNotification(message, true);
        //  }
        // else if(companyName===''){
        //     let message="Company name is required";
        //     ShowNotification(message, true);
        //  }
        // else if(industry===''){
        //     let message="Company  industry is required";
        //     ShowNotification(message, true);
        //  }
        // else if(aboutCompany===''){
        //     let message="Tell us briefly about the company";
        //     ShowNotification(message, true);
        //  }
        // else if(location===''){
        //     let message="Job location is required";
        //     ShowNotification(message, true);
        //  }
        // else if(numberOfStaff===''){
        //     let message="Number of staff is required";
        //     ShowNotification(message, true);
        //  }
        // else if(roleTitle===''){
        //     let message="Number of staff is required";
        //     ShowNotification(message, true);
        //  }
        // else if(jobLevel===''){
        //     let message="Job Entry level is required";
        //     ShowNotification(message, true);
        //  }
        // else if(wordForApplicant===''){
        //     let message="Tell us about the application";
        //     ShowNotification(message, true);
        //  }
        // else{


        let isCompanyRepresentative = AmCompanyRep === true ? 1 : 0;
        // DashboardStore._updateParameterV2(JobIntrests, "intrest", "JobPosting");
        DashboardStore._updateParameterV2(isCompanyRepresentative, "isCompanyRepresentative", "JobPosting");




        console.log(JobPosting);
        DashboardStore.__ProccessJobPosting();
        // }

    };
    Post2RecruiterDashboard = () => {
        this.props.navigation.navigate("RecruiterAvailable")
        return;
        // const { JobIntrests, AmCompanyRep, showsaveBtn } = this.state;
        const { JobPosting } = DashboardStore
        // let current_id = JobPosting.current_id;
        let companyName = JobPosting.companyName;
        let industry = JobPosting.industry;
        let aboutCompany = JobPosting.aboutCompany;
        let location = JobPosting.location;
        let numberOfStaff = JobPosting.numberOfStaff;
        let roleTitle = JobPosting.roleTitle;
        let jobLevel = JobPosting.jobLevel;
        let wordForApplicant = JobPosting.wordForApplicant;
        let JobIntrests = JobPosting.intrest;
        let isCompanyRepresentative = JobPosting.isCompanyRepresentative;
        let isToRefTimeLineOrToHireRecruter = JobPosting.isToRefTimeLineOrToHireRecruter;
        let additional_files = JobPosting.additional_files;



        if (additional_files.length < 1) {
            let message = "Job Banner missing";
            ShowNotification(message, true);
        }
        else if (companyName === '') {
            let message = "Company name is required";
            ShowNotification(message, true);
        }
        else if (industry === '') {
            let message = "Company  industry is required";
            ShowNotification(message, true);
        }
        else if (JobIntrests.length < 1) {
            let message = "Select 1 or More Skills required (Intrest) for the job";
            ShowNotification(message, true);
        }
        else if (aboutCompany === '') {
            let message = "Tell us briefly about the company";
            ShowNotification(message, true);
        }
        else if (location === '') {
            let message = "Job location is required";
            ShowNotification(message, true);
        }
        else if (numberOfStaff === '') {
            let message = "Number of staff is required";
            ShowNotification(message, true);
        }
        else if (roleTitle === '') {
            let message = "Number of staff is required";
            ShowNotification(message, true);
        }
        else if (jobLevel === '') {
            let message = "Job Entry level is required";
            ShowNotification(message, true);
        }
        else if (wordForApplicant === '') {
            let message = "Tell us about the application";
            ShowNotification(message, true);
        }
        else {
            this.props.navigation.navigate("RecruiterAvailable")

        }
    };
    Update_Post2RecruiterDashboard = () => {

        // const { JobIntrests, AmCompanyRep, showsaveBtn } = this.state;
        const { JobPosting } = DashboardStore
        // let current_id = JobPosting.current_id;
        let companyName = JobPosting.companyName;
        let industry = JobPosting.industry;
        let aboutCompany = JobPosting.aboutCompany;
        let location = JobPosting.location;
        let numberOfStaff = JobPosting.numberOfStaff;
        let roleTitle = JobPosting.roleTitle;
        let jobLevel = JobPosting.jobLevel;
        let wordForApplicant = JobPosting.wordForApplicant;
        let JobIntrests = JobPosting.intrest;
        let isCompanyRepresentative = JobPosting.isCompanyRepresentative;
        let isToRefTimeLineOrToHireRecruter = JobPosting.isToRefTimeLineOrToHireRecruter;
        let additional_files = JobPosting.additional_files;



        if (additional_files.length < 1) {
            let message = "Job Banner missing";
            ShowNotification(message, true);
        }
        else if (companyName === '') {
            let message = "Company name is required";
            ShowNotification(message, true);
        }
        else if (industry === '') {
            let message = "Company  industry is required";
            ShowNotification(message, true);
        }
        else if (JobIntrests.length < 1) {
            let message = "Select 1 or More Skills required (Intrest) for the job";
            ShowNotification(message, true);
        }
        else if (aboutCompany === '') {
            let message = "Tell us briefly about the company";
            ShowNotification(message, true);
        }
        else if (location === '') {
            let message = "Job location is required";
            ShowNotification(message, true);
        }
        else if (numberOfStaff === '') {
            let message = "Number of staff is required";
            ShowNotification(message, true);
        }
        else if (roleTitle === '') {
            let message = "Number of staff is required";
            ShowNotification(message, true);
        }
        else if (jobLevel === '') {
            let message = "Job Entry level is required";
            ShowNotification(message, true);
        }
        else if (wordForApplicant === '') {
            let message = "Tell us about the application";
            ShowNotification(message, true);
        }
        else {

            DashboardStore.__ProccessJobPosting2Recruiters_update();
        }
    };
    async componentDidMount() {
        // DashboardStore._ToggleModal___("newreferalJobSuccess",false);
        DashboardStore.UpdateNavigationState(this.props.navigation);
        let This__data = this.props.navigation.getParam('data', ''); //EditJob

        this.setState({
            ViewMode: This__data
        })
        let UserloginToken = await temp();
        // let loggedinUserdetails=await loggedinUserdetails();
        console.log(UserloginToken);
        DashboardStore._getAxiosInstance(UserloginToken);
        //  DashboardStore.__GetIndustries(1,10);
        DashboardStore.__ALLSkillWithOutFilter(1, 500);
        DashboardStore.__JobsLevels();
        DashboardStore.__GetUserIndustries();

        if (This__data === "EditJob") {
            this.PrepareDataForEditing();
        }
        else {
            DashboardStore.__ResetJobpostingData();
        }

    }

    onSelectedItemsChange = selectedItems => {
        this.setState({ selectedItems });

    };
    PrepareDataForEditing = () => {
        let indu = [];
        let numberOfStaff_array = [];
        let jobLevel_array = [];
        // let jobLevel_array=[];
        let selectedIndunstry = DashboardStore.JobPosting.industry;
        let numberOfStaff = DashboardStore.JobPosting.numberOfStaff;
        let jobLevel = DashboardStore.JobPosting.jobLevel;
        let skills_required = DashboardStore.JobPosting.intrest;
        let iscompanyrep = DashboardStore.JobPosting.isCompanyRepresentative;
        let isToRefTimeLineOrToHireRecruter = DashboardStore.JobPosting.isToRefTimeLineOrToHireRecruter;
        indu.push(selectedIndunstry);
        numberOfStaff_array.push(numberOfStaff);
        numberOfStaff_array.push(numberOfStaff);
        jobLevel_array.push(jobLevel);
        // alert(iscompanyrep)
        // indu=Array.from(selectedIndunstry);
        iscompanyrep = iscompanyrep == 1 ? true : false;
        //  alert(iscompanyrep)

        console.log("indu>>>>><<<<<<")
        console.log(jobLevel_array)
        console.log(skills_required)
        console.log(numberOfStaff_array)
        console.log("indu>>>>><<<<<<")

        this.setState({
            industry: indu,
            numberOfStaff: numberOfStaff_array,
            jobLevel: jobLevel_array,
            JobIntrests: skills_required,
            AmCompanyRep: iscompanyrep,
            isToRefTimeLineOrToHireRecruter
        })

    };
    changeAmCompanyRep = () => {
        const { AmCompanyRep } = this.state;
        console.log(AmCompanyRep)
        this.setState({
            AmCompanyRep: !AmCompanyRep
        })
        // this.setState({ selectedItems });

    };
    OnIntrestJobChange = JobIntrests => {
        this.setState({ JobIntrests });
        DashboardStore._updateParameterV2(JobIntrests, "intrest", "JobPosting");

    };
    _selectPhotoTapped() {
        // const options = {
        //   quality: 1.0,
        //   maxWidth: 500,
        //   maxHeight: 500,
        //   storageOptions: {
        //     skipBackup: true
        //   },
        //   mediaType:'photo',
        //   // mediaType:'video',
        // };

        const options = {
            title: 'Select',
            noData: true,
            mediaType: 'mixed',
            storageOptions: {
                skipBackup: true,
                path: 'construction-cloud'
            }
        };


        ImagePicker.showImagePicker(options, (response) => {
            // ImagePicker.launchImageLibrary(options, (response)  => {
            console.warn('Response = ', response);

            if (response.didCancel) {
                console.warn('User cancelled photo picker');
            }
            else if (response.error) {
                console.warn('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.warn('User tapped custom button: ', response.customButton);
            }
            else {
                console.log(response)
                // let source = { uri: response.uri };

                // You can also display the image using data:
                // let source = { uri: 'data:image/jpeg;base64,' + response.data };
                const source = {
                    uri: response.uri,
                    type: response.type,
                    name: response.fileName,
                }
                console.log(source)
                this.setState({
                    avatarSource: source,
                    isImagefromdevice: true,
                    showsaveBtn: true,
                    file2upload: source,
                    uploading: true,
                });

                this.cloudinaryUpload();
            }

        });
    }
    _uploadImage() {
        // this.setState({
        //   loader_visible: true
        // });
        ShowNotification('Uploading your file...', true);
        let file2upload = this.state.file2upload;
        // console.warn(file2upload)
        let source = { uri: file2upload.uri };
        this.setState({
            uploadingImg: true
        });
        uploadFile(file2upload)
            .then(response => response.json())
            .then(result => {
                console.log(result)
                let imageurl = result.secure_url;
                DashboardStore._updateParameterV2(imageurl, "image_url", "profileupdatedata");
                ShowNotification('Saving to database....', true);
                DashboardStore.__UpdateProfile();
                // this.SavetoDatabase(result)
                // this.setState({
                //     avatarSource: { uri: result.secure_url },
                //     loader_visible: false
                // });
            })

    }

    async cloudinaryUpload() {
        const { file2upload } = this.state;
        // let file=
        // DashboardStore._ToggleProcessing(true)
        console.log("here man")
        ShowNotification('Uploading your file...', true);

        const data = new FormData()
        data.append('file', file2upload)
        data.append('upload_preset', YOUR_CLOUDINARY_PRESET_JobBanner())
        data.append("cloud_name", YOUR_CLOUDINARY_NAME())
        fetch("https://api.cloudinary.com/v1_1/workbrook-hash/upload", {
            // https://api.cloudinary.com/v1_1/workbrook-hash/image/upload
            method: "post",
            body: data
        }).then(res => res.json()).
            then(data => {
                console.log("<>>><<")
                console.log(data)
                let links = [];
                let fileUrl = data.secure_url;
                links.push(fileUrl);
                DashboardStore._updateParameterV2(links, "additional_files", "JobPosting");
                // ShowNotification('Saving to database....', true);
                // DashboardStore.__UpdateProfile();
                // this.SavetoDatabase(result)
                // setPhoto(data.secure_url)
                this.setState({
                    uploading: false
                })


            }).catch(err => {
                console.log(err)
                Alert.alert("An Error Occured While Uploading")
            })
    }
    toggleSuccessPostModalShow = () => {
        DashboardStore._ToggleSucessPostModal(true)
    };
    toggleSuccessPostModalClose = () => {
        // DashboardStore._ToggleSucessPostModal(false)
        const { ViewMode } = this.state;
        if (ViewMode === "EditJob") {
            DashboardStore._ToggleModal___("newreferalJobSuccess", false);
            this.props.navigation.goBack();
            DashboardStore.__GetJobOffers(1, 30);
        }
        else {
            DashboardStore._ToggleModal___("newreferalJobSuccess", false);
            this.props.navigation.dispatch(StackActions.popToTop());
        }



    };
    render() {
        const { isGoBack } = DashboardStore;
        // if(isGoBack===true){
        // this.props.navigation.goBack();
        // const popAction = StackActions.pop({
        //     n: 1,
        //   });

        //   this.props.navigation.dispatch(popAction);
        // this.props.navigation.dispatch(StackActions.popToTop());

        DashboardStore._UpdateDefaultValue();
        // }
        // const { selectedItems } = this.state;
        const { selectedItems, JobIntrests, uploading } = this.state;
        const { isImagefromdevice, avatarSource, loader_visible, showsaveBtn, ViewMode, isToRefTimeLineOrToHireRecruter } = this.state;
        let renderImage;

        if (isImagefromdevice === true) {
            renderImage = <Thumbnail square
                large
                source={avatarSource}
                style={{ height: 195, width: '100%', marginTop: 15, }}

            />
        } else {
            renderImage = <TouchableOpacity
                // onPress={() => Alert.alert('hi')}
                onPress={this._selectPhotoTapped}
                style={{ height: 195, width: '100%', backgroundColor: "#A7A7A7", marginTop: 15, }}

            />
        }

        if (ViewMode === "EditJob") {
            renderImage = <Thumbnail square
                large
                source={{ uri: DashboardStore.JobPosting.additional_files[0] }}
                style={{ height: 195, width: '100%', }}

            />
        }
        return (

            <Container style={styles.container}>
                {myspiner(DashboardStore.isProcessing)}
                <CustomizeHeader
                    leftside='cancel'
                    title={DashboardStore.JobPosting.HeaderText}
                    rightside='empty'
                    statedColor='#1E93E2'
                    icon='md-close'
                    savehandler={this.proceednow}
                />

                <ScrollView
                    contentContainerStyle={{}}
                    keyboardShouldPersistTaps="handled"

                >
                    <View style={styles.imageHolder}>
                        {renderImage}
                    </View>
                    <CardItem style={[{ backgroundColor: 'transparent' }]}
                        button
                        // onPress={() => Alert.alert('hi')}
                        onPress={this._selectPhotoTapped}

                    >
                        <Left style={{ alignItems: 'center', display: "flex", justifyContent: "center" }}>
                            <Text style={[styles.cardTextJob6, {}]}>
                                Add image/video  {JSON.stringify(DashboardStore.newreferalJobSuccess)} <Text style={styles.asteriskSty}>*</Text>
                            </Text>
                        </Left>
                    </CardItem>

                    {/* <CardItem style={[{ backgroundColor: 'transparent', }]}> */}
                    <Item stackedLabel style={[styles.bottomIndBorder, { height: 55 }]}>
                        <Label style={styles.labelSty}>Name of the company <Text style={styles.asteriskSty}>*</Text></Label>
                        <Input
                            placeholder="Name of the company"
                            placeholderLabel
                            placeholderTextColor="#A7A7A7"
                            style={[styles.cardTextJob3,]}
                            defaultValue={DashboardStore.JobPosting.companyName}
                            onChangeText={(companyName) => DashboardStore.onChangeText('companyName', companyName, 'JobPosting')}

                        />

                    </Item>
                    {/* </CardItem> */}
                    <View stackedLabel style={[styles.bottomIndBorder, { marginTop: 20, paddingBottom: 3, marginBottom: 15 }]}>
                        <Label style={styles.labelSty}>industry <Text style={styles.asteriskSty}>*</Text></Label>
                        <MultiSelect
                            hideTags
                            // items={defaultindustries}
                            items={DashboardStore.AllIndustries.Industry}

                            uniqueKey="id"
                            ref={(component) => { this.multiSelect = component }}
                            onSelectedItemsChange={(industry) => {
                                // alert("hey man")
                                console.log(industry)
                                this.setState({ industry })
                                // console.log()
                                let item = industry[0];
                                DashboardStore._updateParameterV2(item, "industry", "JobPosting");

                            }
                                // this.onSelectedItemsChange
                            }
                            selectedItems={this.state.industry}
                            selectText="Industry"
                            searchInputPlaceholderText="Search Industry..."
                            onChangeInput={(text) => console.log(text)}
                            tagRemoveIconColor="#FF5964"
                            tagBorderColor="#CCC"
                            tagTextColor="#CCC"
                            selectedItemTextColor="#CCC"
                            itemTextColor="#fff"
                            textColor="#A7A7A7"
                            displayKey="name"
                            searchInputStyle={{ color: '#CCC', borderWidth: 0, borderColor: "#ccc", fontWeight: "300" }}
                            styleItemsContainer={{ borderWidth: 1, borderColor: '#1E93E2', backgroundColor: "#1E93E2" }}
                            submitButtonColor="#CCC"
                            submitButtonText="Submit"
                            itemFontSize={10}
                            fontSize={10}
                            styleDropdownMenuSubsection={{ borderBottomWidth: 0, backgroundColor: 'transparent', height: 35, paddingLeft: 5, paddingBottom: 0, marginBottom: -10, }}
                            selectedItemIconColor={'#fff'}

                            hideSubmitButton={true}
                            single={true}
                        />
                    </View>
                    <Item stackedLabel style={[styles.bottomIndBorder, {}]}>
                        <Label style={styles.labelSty}>About the company (100words) <Text style={styles.asteriskSty}>*</Text></Label>
                        <Textarea
                            placeholder="About the company (100words)"
                            placeholderLabel
                            placeholderTextColor="#A7A7A7"
                            rowSpan={4}
                            style={styles.cardTextJob3}

                            defaultValue={DashboardStore.JobPosting.aboutCompany}
                            onChangeText={(aboutCompany) => DashboardStore.onChangeText('aboutCompany', aboutCompany, 'JobPosting')}

                        />

                    </Item>

                    <View stackedLabel style={[styles.bottomIndBorder, { marginTop: 20, paddingBottom: 3, }]}>
                        <Label style={styles.labelSty}>Job Location<Text style={styles.asteriskSty}>*</Text></Label>
                        <GooglePlacesInput
                            OnpressAction={(data, details = null) => { // 'details' is provided when fetchDetails = true
                                let latitude = details.geometry.location.lat;
                                let longitude = details.geometry.location.lng;
                                let ServiceAddress = details.formatted_address || details.description || details.name;
                                let nearestplace = details.name;
                                let addresstosend = `${ServiceAddress}   (  ${nearestplace}  )`;
                                DashboardStore._updateParameterV2(addresstosend, "location", "JobPosting");

                            }}
                            MydefaultLocation={DashboardStore.JobPosting.location}
                        />
                    </View>
                    {/* <Item style={[styles.bottomIndBorder, { height: 55 }]}>
                        <Input
                            placeholder="Location"
                            placeholderLabel
                            placeholderTextColor="#A7A7A7"
                            style={styles.cardTextJob3}
                            defaultValue={DashboardStore.JobPosting.location}
                            onChangeText={(location) => DashboardStore.onChangeText('location', location, 'JobPosting')}

                        />

                    </Item> */}

                    <View stackedLabel style={[styles.bottomIndBorder, { marginTop: 20, paddingBottom: 3, marginBottom: 15 }]}>
                        <Label style={styles.labelSty}>Number of staff <Text style={styles.asteriskSty}>*</Text></Label>
                        <MultiSelect
                            hideTags
                            items={NumberOfStaff}
                            uniqueKey="id"
                            ref={(component) => { this.multiSelect = component }}
                            // onSelectedItemsChange={this.onSelectedItemsChange}
                            onSelectedItemsChange={(numberOfStaff) => {
                                // alert("hey man")
                                console.log(numberOfStaff)
                                this.setState({
                                    numberOfStaff
                                })
                                // console.log()
                                let item = numberOfStaff[0];
                                DashboardStore._updateParameterV2(item, "numberOfStaff", "JobPosting");

                            }
                            }

                            selectedItems={this.state.numberOfStaff}
                            selectText="Number of staff"
                            searchInputPlaceholderText="Search Number of staff..."
                            onChangeInput={(text) => console.log(text)}
                            tagRemoveIconColor="#FF5964"
                            tagBorderColor="#CCC"
                            tagTextColor="#CCC"
                            selectedItemTextColor="#CCC"
                            selectedItemIconColor="#CCC"
                            itemTextColor="#fff"
                            textColor="#A7A7A7"
                            displayKey="name"
                            searchInputStyle={{ color: '#CCC', borderWidth: 0, borderColor: "#ccc", fontWeight: "300" }}
                            styleItemsContainer={{ borderWidth: 1, borderColor: '#1E93E2', backgroundColor: "#1E93E2" }}
                            submitButtonColor="#CCC"
                            submitButtonText="Submit"
                            itemFontSize={10}
                            fontSize={10}
                            styleDropdownMenuSubsection={{ borderBottomWidth: 0, backgroundColor: 'transparent', height: 35, paddingLeft: 5, paddingBottom: 0, marginBottom: -10, }}
                            selectedItemIconColor={'#fff'}

                            hideSubmitButton={true}
                            single={true}
                        />
                    </View>

                    <Item stackedLabel style={[styles.bottomIndBorder, {}]}>
                        <Label style={styles.labelSty}>Role title <Text style={styles.asteriskSty}>*</Text></Label>

                        <Input
                            placeholder="Role title"
                            placeholderLabel
                            placeholderTextColor="#A7A7A7"
                            rowSpan={4}
                            style={styles.cardTextJob3}

                            defaultValue={DashboardStore.JobPosting.roleTitle}
                            onChangeText={(roleTitle) => DashboardStore.onChangeText('roleTitle', roleTitle, 'JobPosting')}

                        />
                    </Item>
                    {/* <Item style={[styles.bottomIndBorder, { height: 55 }]}>
                        <Input
                            placeholder="Role title"
                            placeholderLabel
                            placeholderTextColor="#A7A7A7"
                            style={styles.cardTextJob3}
                            defaultValue={DashboardStore.JobPosting.roleTitle}
                            onChangeText={(roleTitle) => DashboardStore.onChangeText('roleTitle', roleTitle, 'JobPosting')}

                        />

                    </Item> */}


                    <View stackedLabel style={[styles.bottomIndBorder, { marginTop: 20, paddingBottom: 3, marginBottom: 15 }]}>
                        <Label style={styles.labelSty}>Job Level <Text style={styles.asteriskSty}>*</Text></Label>
                        <MultiSelect
                            hideTags
                            items={DashboardStore.SystemJoblevel.Joblevel}
                            uniqueKey="id"
                            ref={(component) => { this.multiSelect = component }}
                            // onSelectedItemsChange={this.onSelectedItemsChange}
                            onSelectedItemsChange={(jobLevel) => {
                                console.log(jobLevel)
                                this.setState({ jobLevel })
                                // console.log()
                                let item = jobLevel[0];
                                DashboardStore._updateParameterV2(item, "jobLevel", "JobPosting");

                            }
                            }
                            selectedItems={this.state.jobLevel}
                            selectText="Job Level"
                            searchInputPlaceholderText="Search Job Level..."
                            onChangeInput={(text) => console.log(text)}
                            tagRemoveIconColor="#FF5964"
                            tagBorderColor="#CCC"
                            tagTextColor="#CCC"
                            selectedItemTextColor="#CCC"
                            selectedItemIconColor="#CCC"
                            itemTextColor="#fff"
                            textColor="#A7A7A7"
                            displayKey="name"
                            searchInputStyle={{ color: '#CCC', borderWidth: 0, borderColor: "#ccc", fontWeight: "300" }}
                            styleItemsContainer={{ borderWidth: 1, borderColor: '#1E93E2', backgroundColor: "#1E93E2" }}
                            submitButtonColor="#CCC"
                            submitButtonText="Submit"
                            itemFontSize={10}
                            fontSize={10}
                            styleDropdownMenuSubsection={{ borderBottomWidth: 0, backgroundColor: 'transparent', height: 35, paddingLeft: 5, paddingBottom: 0, marginBottom: -10, }}
                            selectedItemIconColor={'#fff'}

                            hideSubmitButton={true}
                            single={true}
                        />
                    </View>


                    {/* <View stackedLabel style={[styles.bottomIndBorder, { marginTop: 20, paddingBottom: 3, marginBottom: 15 }]}>
                        <Label style={styles.labelSty}>Type of Employment</Label>
                        <MultiSelect
                            hideTags
                            items={DashboardStore.SystemJoblevel.Joblevel}
                            uniqueKey="id"
                            ref={(component) => { this.multiSelect = component }}
                            onSelectedItemsChange={this.onSelectedItemsChange}
                            onSelectedItemsChange={(jobLevel) => {
                                console.log(jobLevel)
                                this.setState({ jobLevel })
                                // console.log()
                                let item = jobLevel[0];
                                DashboardStore._updateParameterV2(item, "jobLevel", "JobPosting");

                            }
                            }
                            selectedItems={this.state.jobType}
                            selectText="Employment Type"
                            searchInputPlaceholderText="Search Employment Type..."
                            onChangeInput={(text) => console.log(text)}
                            tagRemoveIconColor="#FF5964"
                            tagBorderColor="#CCC"
                            tagTextColor="#CCC"
                            selectedItemTextColor="#CCC"
                            selectedItemIconColor="#CCC"
                            itemTextColor="#fff"
                            textColor="#A7A7A7"
                            displayKey="name"
                            searchInputStyle={{ color: '#CCC', borderWidth: 0, borderColor: "#ccc", fontWeight: "300" }}
                            styleItemsContainer={{ borderWidth: 1, borderColor: '#1E93E2', backgroundColor: "#1E93E2", }}
                            submitButtonColor="#CCC"
                            submitButtonText="Submit"
                            itemFontSize={10}
                            fontSize={10}
                            styleDropdownMenuSubsection={{ borderBottomWidth: 0, backgroundColor: 'transparent', height: 35, paddingLeft: 5, paddingBottom: 0, marginBottom: -20, }}
                            selectedItemIconColor={'#fff'}

                            hideSubmitButton={true}
                            single={true}
                        />
                    </View> */}
                    <View stackedLabel style={[styles.bottomIndBorder, { borderBottom: 1, marginTop: 5, marginBottom: 20 }]}>
                        <Label style={[styles.labelSty, { marginBottom: 5 }]}>Type of Employment<Text style={styles.asteriskSty}>*</Text></Label>
                        <RNPickerSelect

                            style={{
                                placeholder: {
                                    fontSize: 10,
                                    color: '#A7A7A7',
                                },
                                inputAndroid: {
                                    fontSize: 11,
                                    color: '#2F2F2F',
                                },
                                inputIOS: {
                                    fontSize: 11,
                                    color: '#2F2F2F',
                                },
                                textInputProps: { underlineColorAndroid: 'transparent', fontSize: 11, color: '#2F2F2F', }
                            }}
                            placeholder={{
                                label: 'Type of Employment',
                                value: null,
                            }}

                            onValueChange={(value) => {

                                console.log(value)
                                DashboardStore._updateParameterV2(value, "job_type", "JobPosting");

                            }}
                            useNativeAndroidPickerStyle={false}

                            value={DashboardStore.JobPosting.job_type}
                            //   items={StaticData.StateInCOuntry.slice()}
                            items={[
                                { label: 'Full-time', value: 'Full-time' },
                                { label: 'Part-time', value: 'Part-time' },
                                { label: 'Fixed-time', value: 'Fixed-time' },

                            ]}
                        />
                    </View>

                    <Item stackedLabel style={[styles.bottomIndBorder, {}]}>
                        <Label style={styles.labelSty}>What’s in it for the applicant (200words) <Text style={styles.asteriskSty}>*</Text></Label>
                        <Textarea
                            placeholder="What’s in it for the applicant (200 words)"
                            placeholderLabel
                            placeholderTextColor="#A7A7A7"
                            rowSpan={4}
                            style={styles.cardTextJob3}

                            defaultValue={DashboardStore.JobPosting.wordForApplicant}
                            onChangeText={(wordForApplicant) => DashboardStore.onChangeText('wordForApplicant', wordForApplicant, 'JobPosting')}

                        />

                    </Item>
                    {/* <Item style={[styles.bottomIndBorder, { height: 55 }]}>
                        <Input
                            placeholder="What’s in it for the applicant (200words)"
                            placeholderLabel
                            placeholderTextColor="#A7A7A7"
                            style={styles.cardTextJob3}
                            defaultValue={DashboardStore.JobPosting.wordForApplicant}
                            onChangeText={(wordForApplicant) => DashboardStore.onChangeText('wordForApplicant', wordForApplicant, 'JobPosting')}

                        />

                    </Item> */}
                    <View stackedLabel style={[styles.bottomIndBorder, { marginTop: 20, paddingBottom: 3, marginBottom: 15 }]}>
                        <Label style={styles.labelSty}>Skills Required <Text style={styles.asteriskSty}>*</Text></Label>
                        <MultiSelect
                            hideTags={true}
                            items={DashboardStore.ALLSkillWithOutFilter.ALLSkill}
                            uniqueKey="id"
                            ref={(component) => { this.multiSelect = component }}
                            onSelectedItemsChange={this.OnIntrestJobChange}
                            selectedItems={this.state.JobIntrests}
                            selectText="Interests"
                            searchInputPlaceholderText="Search Interests"
                            onChangeInput={(text) => console.log(text)}
                            tagRemoveIconColor="#FF5964"
                            tagBorderColor="#CCC"
                            tagTextColor="#CCC"
                            selectedItemTextColor="#CCC"
                            selectedItemIconColor="#CCC"
                            itemTextColor="#fff"
                            textColor="#A7A7A7"
                            tagTextColor="#A7A7A7"
                            displayKey="name"
                            searchInputStyle={{ color: '#CCC', borderWidth: 0, borderColor: "#ccc", fontWeight: "300" }}
                            styleItemsContainer={{ borderWidth: 1, borderColor: '#1E93E2', backgroundColor: "#1E93E2", }}
                            submitButtonColor="#CCC"
                            submitButtonText="Submit"
                            itemFontSize={10}
                            fontSize={10}
                            styleDropdownMenuSubsection={{ borderBottomWidth: 0, backgroundColor: 'transparent', height: 35, paddingLeft: 5, paddingBottom: 0, marginBottom: -20, }}
                            selectedItemIconColor={'#fff'}

                            hideSubmitButton={true}
                        />
                        <View style={{ fontSize: 10 }}>
                            {this.multiSelect ? this.multiSelect.getSelectedItemsExt(JobIntrests) : null}

                        </View>
                    </View>



                    {/* <CardItem style={[{ backgroundColor: 'transparent' }]}>
                        <Left style={{ alignItems: 'center' }}>
                            <Button style={[{ backgroundColor: 'transparent', elevation: 0 }]}>
                                <Text style={styles.cardTextJob6}>
                                    Upload additional files (max 2MB)
                                </Text>
                            </Button>
                        </Left>
                    </CardItem> */}

                    <View style={styles.bottomIndBorderNon}>
                        <Left style={{

                            display: 'flex',
                            flexDirection: 'row',
                            alignItems: 'center',
                        }}>
                            <CheckBox
                                checked={this.state.AmCompanyRep}
                                onPress={this.changeAmCompanyRep}
                            />
                            <Body style={[{ marginLeft: 10 }]}>
                                <Text style={[styles.cardTextJob1, { marginLeft: 10 }]}>I am a representative of the company with this job vacancy.</Text>
                            </Body>
                        </Left>
                    </View>
                    {uploading === false ?
                        ViewMode === "EditJob" ?
                            isToRefTimeLineOrToHireRecruter == "1" ?
                                <CardItem style={[{ backgroundColor: 'transparent', paddingLeft: 0, paddingBottom: 15, alignItems: 'center', justifyContent: "center" }]}>
                                    {/* <Left style={{ alignItems: 'center', justifyContent: "center" }}> */}
                                    <Button
                                        full
                                        style={[styles.btnNewPost, { backgroundColor: '#1E93E2', elevation: 0 }]}
                                        // onPress={this.toggleSuccessPostModalShow}
                                        onPress={this.Post2Referrals}
                                    >
                                        <Text style={styles.cardTextJob4_1}>
                                            Update Job Details
</Text>
                                    </Button>
                                    {/* </Left> */}
                                </CardItem>
                                :
                                isToRefTimeLineOrToHireRecruter == "2" ?
                                    <CardItem style={[{ backgroundColor: 'transparent', paddingLeft: 0, paddingBottom: 15, alignItems: 'center', justifyContent: "center" }]}>
                                        {/* <Left style={{ alignItems: 'center', justifyContent: "center" }}> */}
                                        <Button full style={[styles.btnNewPost, { backgroundColor: '#5ABC7A', elevation: 0, marginLeft: -5 }]}
                                            onPress={this.Update_Post2RecruiterDashboard}
                                        >
                                            <Text style={styles.cardTextJob4_1}>
                                                Update Job Details
</Text>
                                        </Button>
                                        {/* </Left> */}
                                    </CardItem>
                                    :
                                    null
                            :
                            <CardItem style={[{ backgroundColor: 'transparent', paddingLeft: 0, paddingBottom: 15, }]}>
                                <Left style={{ alignItems: 'center', justifyContent: "center" }}>
                                    <Button
                                        full
                                        style={[styles.btnNewPost, { backgroundColor: '#1E93E2', elevation: 0 }]}
                                        // onPress={this.toggleSuccessPostModalShow}
                                        onPress={this.Post2Referrals}
                                    >
                                        <Text style={styles.cardTextJob4_1}>
                                            Post to referral timeline
  </Text>
                                    </Button>
                                </Left>
                                <Right style={{ alignItems: 'center', justifyContent: "center", backgroundColor: 'red' }}>
                                    <Button full style={[styles.btnNewPost, { backgroundColor: '#5ABC7A', elevation: 0, marginLeft: -5 }]}
                                        onPress={this.Post2RecruiterDashboard}
                                    //  onPress={() => { this.props.navigation.navigate("RecruiterAvailable") }
                                    // }
                                    >
                                        <Text style={styles.cardTextJob4_1}>
                                            Hire a recruiter
  </Text>
                                    </Button>
                                </Right>
                            </CardItem>
                        :
                        <CardItem style={[{ backgroundColor: 'transparent', paddingLeft: 0, paddingBottom: 15, }]}>
                            <Left style={[{ alignItems: 'center', justifyContent: "center" }, { color: '#000' }]}>
                                <View>
                                    <Animatable.Text animation="pulse" easing="ease-out" iterationCount="infinite" style={[styles.cardTextJob4_1, { color: '#000', fontSize: 15, }]}>   Uploading Job Banner ... </Animatable.Text>

                                </View>
                            </Left>
                        </CardItem>

                    }


                </ScrollView>

                <Modal style={styles.MContainer} isVisible={DashboardStore.newreferalJobSuccess} >
                    {/* <Modal style={styles.MContainer} isVisible={true} > */}
                    <Button onPress={this.toggleSuccessPostModalClose} style={{ backgroundColor: "transparent", position: 'absolute', right: 10, top: 40, padding: 20, elevation: 0 }}>
                        <SvgUri
                            width="19px"
                            height="19px"
                            uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1594146750/Mobile_Assets/close_white_iocsmo.svg"
                        />
                    </Button>
                    <View style={[styles.Mwrapper, { height: 380, backgroundColor: "#fff" }]}>
                        <View style={{ paddingLeft: 29, paddingRight: 29, display: 'flex', justifyContent: 'center', alignItems: 'center', backgroundColor: "#fff" }}>
                            <SvgUri
                                width="129px"
                                height="129px"
                                uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1589394608/Mobile_Assets/thankyou_l8tqh1.svg"
                            />
                            <Text style={styles.cardText1, { fontSize: 16, color: '#5ABC7A', textAlign: 'center', fontWeight: '600', marginTop: 17 }}>
                                Congratulations!
                                        </Text>
                            <Text style={styles.cardText1, { fontSize: 16, color: '#5ABC7A', textAlign: 'center', fontWeight: '600' }}>
                                {
                                    ViewMode === "EditJob" ?
                                        "Job Updated successfully"
                                        :
                                        DashboardStore.JobpostedMessage

                                }
                            </Text>
                        </View>

                    </View>
                </Modal>

            </Container>


        );
    }
}
export default Settings;

const styles = StyleSheet.create({
    container: {
        backgroundColor: "#fff",
        display: "flex",
        flex: 1,
        justifyContent: "flex-start",
    },
    imageHolder: {
        display: "flex",
        alignItems: "center",
        paddingLeft: 15,
        paddingRight: 15
    },
    btnNewPost: {
        height: 38,
        elevation: 0,
        minWidth: 160
    },
    IconSetsty1: {
        maxWidth: 16,
        maxHeight: 17,
    },
    IconSetsty: {
        maxWidth: 16,
        maxHeight: 16,
    },
    iconWrapSet: {
        width: 20,
        marginRight: 10
    },
    IconSetsty2: {
        width: 8,
        height: 15,

    },
    bottomIndBorderNon: {
        marginLeft: 15,
        marginRight: 15,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        // borderBottomWidth: 1,
        // borderColor: '#E5E5E5'
    },
    bottomIndBorder: {
        marginLeft: 15,
        marginRight: 15,
        borderBottomWidth: 1,
        borderColor: '#E5E5E5',
    },
    rmPadding: {
        paddingTop: 0,
        paddingRight: 0,
        paddingBottom: 0,
        paddingLeft: 0,
    },
    rmPaddingLR: {
        paddingLeft: 0,
        paddingRight: 0,
    },

    cardTextJob1: {
        color: "#2F2F2F",
        fontSize: 11,
    },
    cardTextJob2: {
        color: "#676767",
        fontSize: 9,
        fontWeight: '300',
    },
    cardTextJob3: {
        color: "#2F2F2F",
        fontSize: 11,
        fontWeight: '300',
        marginBottom: 0,
        width: "100%"
    },
    cardTextJob4: {
        color: "#1E93E2",
        fontSize: 11,
        fontWeight: '500',
    },
    cardTextJob4_1: {
        color: "#fff",
        fontSize: 12,
        fontWeight: '500',
    },
    cardTextJob5: {
        color: "#1E93E2",
        fontSize: 13,
        fontWeight: '600',
    },
    cardTextJob6: {
        color: "#1E93E2",
        fontSize: 11,
        fontWeight: '600',
    },
    cardTextJob7: {
        fontSize: 12,
        color: '#2F2F2F',
    },
    Mwrapper: {
        height: 510,
        width: '90%',
        backgroundColor: '#fff',
        // padding: 15,
        display: 'flex',
        justifyContent: 'space-around',
        // alignItems: 'center'
    },
    cardText1: {
        color: "#2F2F2F",
        fontWeight: '500',
        fontSize: 12,
    },
    MContainer: {
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
    },
    labelSty: {
        color: "#2F2F2F",
        fontSize: 11,
        fontWeight: '500'
    },
    asteriskSty: {
        color: "#FF5964",
        fontSize: 10,
    },


});

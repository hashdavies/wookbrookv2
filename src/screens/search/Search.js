import React, { Component } from 'react';
import {
  Text,
  Image,
  StyleSheet,
  ImageBackground,
  Keyboard,
  TouchableOpacity,
  FlatList
} from 'react-native';
import { KeyboardAvoidingView } from 'react-native';
import CustomizeHeader from '../frags/CustomizeHeader';

import Ionicons from 'react-native-vector-icons/Ionicons';
import {
  Container,
  Header,
  Content,
  Textarea,
  Button,
  View,
  Thumbnail,
  Form,
  Item,
  Input,
  Label,
  Badge,
  Icon,
  Footer,
  Picker,
  Left,
  Right,
  Body,
  Title,
  Center,
  CardItem,
  Card,
  // Item,
} from 'native-base';
import {
  Kaede,
  Hoshi,
  Jiro,
  Isao,
  Madoka,
  Akira,
  Hideo,
  Kohana,
  Makiko,
  Sae,
  Fumi,
} from 'react-native-textinput-effects';
import { SvgUri } from 'react-native-svg';

import { myspiner, AppInlineLoader } from '../../dependency/UtilityFunctions';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import User from "../../assets/workbrookAssets/user4.png";

import accountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import { ScrollView } from 'react-native-gesture-handler';
import DatePicker from 'react-native-datepicker'
// import Autocomplete from 'native-base-autocomplete'
import Autocomplete from 'react-native-autocomplete-input';
import DashboardStore from '../../stores/Dashboard';
import EmptyListComponent from '../../StatelessScreens/EmptyListComponent';

const API = 'https://swapi.co/api';
@observer
class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      errorloading: false,
      date: "2016-05-15",
      // films: ['Apple', 'Mango', 'Guava', 'Muskmelon', 'Watermelon', 'Orange', 'Sapota'],
      films: [],
      query: '',
    }
   }
 
  componentDidMount() {
   
  }
   
  SearchJob = (query) => {
     let stringlenght = query.length;
    if (stringlenght < 1) {
        // DashboardStore.__GetRecruiters(1, 30, false);

        return;
    }
    DashboardStore.__SearchForJobs(query)
}
gotoPreviewPostPage = (item, page) => {
  // console.log(item);


  DashboardStore._updateParameterV2(item, "SingleJobOffer", "JobOppurtunity");
  this.props.navigation.navigate(page)
};

  render() {
   const{ JobSearch}=DashboardStore;
  
   let ErrorComponentDisplay = null;

   if (DashboardStore.isProcessing === true) {
       ErrorComponentDisplay = AppInlineLoader(DashboardStore.isProcessing)

   }
   else {
       ErrorComponentDisplay = <EmptyListComponent
           message={JobSearch.ErrorMessage}
           onclickhandler={this.ReloadPageData}
       />
   }


   let JobListing =
       <FlatList
           // Data
           data={ JobSearch.SearchList}
           // Render Items
           renderItem={({ item }) => {
               let poster = item.created_by_profile
               let posterName = poster.first_name == null || poster.last_name == null ? poster.username : `${poster.first_name}  ${poster.last_name} `
               let posterImage = poster.image_url;
               let jobLinks = item.additional_files;
               let jobBanner = jobLinks[0];

               let liketext = item.total_likes > 1 ? `${item.total_likes} likes` : `${item.total_likes} like`
               return (
                <Card style={{ elevation: 0, padding: 15, borderRadius: 5 , width:"100%", minWidth:"100%"}}>
                <TouchableOpacity
                // onPress={() => this.toggleModal(item)}
                onPress={() => this.gotoPreviewPostPage(item, 'PreviewPost')}
                >
                  <CardItem style={[styles.rmPadding, { paddingBottom: 5, borderBottomWidth: 1, borderColor: '#E5E5E5', }]}>
                    <Left style={{ flex: 2, }}>
                      <Thumbnail
                            source={{ uri: posterImage }}
                          style={{ width: 44, height: 45.19, borderRadius: 45.19 / 2 }}
  
                      />
  
                      <View style={{ position: 'absolute', bottom: 4, left: 33, width: 11, height: 11, borderRadius: 11 / 2, backgroundColor: '#FFF' }}>
                        <SvgUri
  
                          width="11px"
                          height="11px"
                          uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1590846662/Mobile_Assets/checkBlue_ajznt3.svg"
  
                        />
                      </View>
  
                      <Body>
                        <Text style={styles.cardTextJob1}>
{posterName}
                        </Text>
                      </Body>
                    </Left>
                    <Right >
                      <TouchableOpacity>
                        <SvgUri
                          width="14px"
                          height="4px"
                          uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1596365227/Mobile_Assets/more2_rpp1xz.svg"
                        />
                      </TouchableOpacity>
                    </Right>
                  </CardItem>
                  <CardItem style={{ display: 'flex', flexDirection: 'column', alignItems: 'flex-start', paddingLeft: 0, paddingRight: 0 }}>
                    <View style={[styles.disInflex, { justifyContent: "space-between", width: "100%" }]}>
                      <Text style={styles.cardTextJob5}>
                      {item.campany_name}
                      </Text>
  
                    </View>
                    <Text style={styles.cardTextJob3}>
                    {item.role_title}
                    </Text>
                    <Text style={[styles.cardTextJob3, { fontSize: 7 }]}>
                    {item.location}
                    </Text>
                    <Text style={[styles.cardTextJob2, { fontSize: 10 }]}>
                    {item.applicant_benefit}
                    </Text>
                  </CardItem>
  
                  {/* <CardItem cardBody>
                    <Image style={[{ height: 114, flex: 1 }]} source={{ uri: jobBanner }} />
                  </CardItem> */}
                </TouchableOpacity>
              
       
            
              </Card>
             

               )
           }}
           // Item Key
           keyExtractor={(item, index) => String(index)}
           // Header (Title)
           // ListHeaderComponent={this.renderHeader}
           // Footer (Activity Indicator)
           // ListFooterComponent={this.renderFooter()}
           // On End Reached (Takes a function)
           onEndReached={this.retrieveMore}
       // How Close To The End Of List Until Next Data Request Is Made
       // onEndReachedThreshold={0}
       // Refreshing (Set To True When End Reached)
       // refreshing={this.state.refreshing}
       />

    return (
      <Container style={{backgroundColor:'#F3F3F3'}}>
        <CustomizeHeader
            leftside='empty'
            title='Search'
            rightside='cancel'
            statedColor='#1E93E2'
            icon='md-close'
          />
        <ScrollView
        style={{paddingBottom: 15, paddingTop:15,}}
        >
          
        
  <View style={{ paddingLeft: 15, paddingRight: 15 }}>
                            {/* <Label style={styles.cardTextJob2}>Experience</Label> */}
                            <Item regular style={styles.inputStyleWrap}>

                                <Input
                                    placeholder="Search by (name,location )"
                                    onChangeText={(query) => this.SearchJob(query)}
 
                                />
                            </Item>
                        </View>

          {/* <View style={styles.descriptionContainer}>
          {films.length > 0 ? (
            <Text style={styles.infoText}>{this.state.query}</Text>
          ) : (
            <Text style={styles.infoText}>Enter The Film Title</Text>
          )}
        </View> */}
          <View style={styles.profileWrap}>
            {/* <View style={styles.devtrop}>
              <CardItem style={{ width: "100%", backgroundColor: "transparent", paddingTop: 0, }}>
              </CardItem>
              <CardItem style={{ width: "100%", backgroundColor: "transparent", paddingTop: 0, maxWidth: '100%' }}>
                <Left style={{ alignItems: 'flex-start', paddingBottom: 15, flexWrap: 'wrap', width: '100%' }}>
                  <Button style={styles.btnChecky}>
                    <Text style={styles.btnCheckyText}>Design</Text>
                    <AntDesign name='close' style={styles.iconsizeNcolour} />
                  </Button>
                  <Button style={styles.btnChecky}>
                    <Text style={styles.btnCheckyText}>Team Leadership</Text>
                    <AntDesign name='close' style={styles.iconsizeNcolour} />
                  </Button>
                  <Button style={styles.btnChecky}>
                    <Text style={styles.btnCheckyText}>UIUX</Text>
                    <AntDesign name='close' style={styles.iconsizeNcolour} />
                  </Button>
                  <Button style={styles.btnChecky}>
                    <Text style={styles.btnCheckyText}>Public Speaking</Text>
                    <AntDesign name='close' style={styles.iconsizeNcolour} />
                  </Button>
                  <Button style={styles.btnChecky}>
                    <Text style={styles.btnCheckyText}>Marketing</Text>
                    <AntDesign name='close' style={styles.iconsizeNcolour} />
                  </Button>
                </Left>
              </CardItem>
            </View>

            <View style={[styles.devtrop, { paddingTop: 0, marginTop: -15 }]}>
              <CardItem style={{ width: "100%", backgroundColor: "transparent", paddingTop: 0, }}>
                <Left >
                  <Text style={styles.minBold2} >Suggested skills based off your profile:</Text>
                </Left>
              </CardItem>
            </View> */}
           
      
            {
                             JobSearch.SearchList.length > 0 ?

                                JobListing
                                :
                                ErrorComponentDisplay
                        }

          </View>
        </ScrollView>
      </Container>
    );
  }
}
export default Search;

const inputStyles = {
  size: 100,
  color: '#FFFFFF',
};

const height = 40;

const styles = StyleSheet.create({
  profileWrap: {
  //  width: "100%",
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: "transparent",
    paddingLeft: 15,
    paddingTop: 15,
    paddingRight: 15
  },
  autocompleteContainer: {
    flex: 1,
    left: 0,
    position: 'absolute',
    right: 0,
    top: 0,
    zIndex: 1
  },
  profileWrapinner: {
    flex: 1,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 25
  },
  profileinput: {
    marginLeft: 0,
    borderWidth: 1,
    borderColor: '#E5E5E5',
    height: 55,
    // marginBottom: 15
  },
  profileinput2: {
    marginLeft: 0,
    // borderWidth: 1,
    // borderColor: '#E5E5E5',
    height: 55,
    color: "#2F2F2F",
    fontSize: 14,
    fontWeight: '500'
    // marginBottom: 15
  },
  profileinputText: {
    height: "100%",
    color: "#2F2F2F",
    fontSize: 14,
    fontWeight: '500'
  },
  rmPadding: {
    paddingTop: 0,
    paddingRight: 0,
    paddingBottom: 0,
    paddingLeft: 0,
},
rmPaddingLR: {
    paddingLeft: 0,
    paddingRight: 0,
},
cardTextJob1: {
  fontFamily: 'SF Pro Display',
  color: "#2F2F2F",
  fontSize: 11,
},
cardTextJob2: {
  fontFamily: 'SF Pro Display',
  color: "#676767",
  fontSize: 9,
  fontWeight: '300',
},
cardTextJob3: {
  color: "#2F2F2F",
  fontSize: 10,
  fontWeight: '500',
},
cardTextJob4: {
  fontFamily: 'SF Pro Display',
  color: "#1E93E2",
  fontSize: 11,
  fontWeight: '500',
},
cardTextJob4_1: {
  color: "#1E93E2",
  fontSize: 12,
  fontWeight: '500',
},
cardTextJob5: {
  fontFamily: 'SF Pro Display',
  color: "#1E93E2",
  fontSize: 13,
  fontWeight: '600',
},
disInflex: {
  display: 'flex',
  flexDirection: "row",
  alignItems: 'center',
},
  btnChecky: {
    height: 28,
    borderWidth: 1,
    borderColor: '#BCBCBC',
    borderRadius: 5,
    backgroundColor: "transparent",
    elevation: 0,
    paddingLeft: 18,
    paddingRight: 18,
    marginRight: 5,
    marginBottom: 5,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  SurgestKills: {
    height: 28,
    borderWidth: 1,
    borderColor: '#B4B4B4',
    borderRadius: 5,
    backgroundColor: "#B4B4B4",
    elevation: 0,
    paddingLeft: 18,
    paddingRight: 18,
    marginRight: 5,
    marginBottom: 5,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  SurgestKillsText: {
    fontSize: 11,
    fontWeight: 'bold',
    color: '#fff'

  },
  btnCheckyText: {
    fontSize: 11,
    fontWeight: 'bold',
    color: '#BCBCBC'

  },
  SurgestKillsIcon: {
    fontSize: 11,
    fontWeight: 'bold',
    color: '#fff',
    marginLeft: 8

  },
  iconsizeNcolour: {
    fontSize: 11,
    fontWeight: 'bold',
    color: '#BCBCBC',
    marginLeft: 8

  },
  SkillsminBold2: {
    fontSize: 11,
    color: '#2F2F2F'
  },
  minBold2: {
    fontSize: 11,
    color: '#2F2F2F'
  },
  devtrop: {
    display: "flex",
    justifyContent: "flex-start",
  },
  // auto complete
  autocompleteContainer: {
    backgroundColor: '#ffffff',
    borderWidth: 0,
  },
  descriptionContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  itemText: {
    fontSize: 15,
    paddingTop: 5,
    paddingBottom: 5,
    margin: 2,
  },
  infoText: {
    textAlign: 'center',
    fontSize: 16,
  },
  // auto complete

});

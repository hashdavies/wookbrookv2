import React, { Component } from 'react';
import { Text, StyleSheet, ImageBackground, Keyboard, TouchableOpacity } from 'react-native';
import { KeyboardAvoidingView, Image, ScrollView } from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons';
import {
  Container,
  Button,
  View,
  Item,
  Input,
  Item as FormItem,
} from 'native-base';
import {
  Kaede,
  Hoshi,
  Jiro,
  Isao,
  Madoka,
  Akira,
  Hideo,
  Kohana,
  Makiko,
  Sae,
  Fumi,
} from 'react-native-textinput-effects';
import { Col, Row, Grid } from 'react-native-easy-grid';

import { myspiner } from '../../dependency/UtilityFunctions';
import Logo from '../../assets/workbrookAssets/logo.png';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';

import AccountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import PasswordTextBox from '../../component/PasswordTextBox';
import CustomizeHeader from '../frags/CustomizeHeader';


@observer
class ResetPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      errorloading: false,

    }
    this.proceednow = this.proceednow.bind(this);

  }
  componentDidMount() {
    AccountStore.UpdateNavigationState(this.props.navigation);

  }
  proceednow() {
    AccountStore._ResetPasswordNow();

  }
  render() {
    const{isProcessing,payload_forgetPwd}=AccountStore;

    return (
      <React.Fragment>
        <CustomizeHeader
          // leftside='cancel'
          title='Reset Password'
          textColor='#000'
          rightside='empty'
          statedColor='#fff'
          DontShhowNotification={false}
        />
      {/* <CustomizeHeader
leftside='cancel'
title='Reset Password'
DontShhowNotification={false}
/>  */}
      <ScrollView>
        <Container style={styles.container}>


          <View style={styles.contentWrap}>
          <Text style={[styles.contentText, { color: '#5ABC7A', marginBottom: 10, }]}>
           {payload_forgetPwd.ServerMessage}
            </Text>
            <Item regular style={[styles.loginBtn1, { backgroundColor: '#E5E5E5', marginTop: 25, marginBottom: 5, }]}>
              <Input 
              placeholderTextColor={'#BCBCBC'} 
              style={styles.inputSty} 
              placeholder='Enter your code'
              onChangeText={(code) => AccountStore.onChangeText('code', code,'payload_resetPwd')}
            
              />
            </Item>
            <PasswordTextBox
      icon="lock"
      label="Password" 
      placeholder="New Password"
      //  onChange={(password) => this.setState({password})}
      onChange={(password) => AccountStore.onChangeText('password', password,'payload_resetPwd')}
 returnKeyType={'done'}
        keyboardType={'default'}
        onSubmitEditing={
          this.proceednow
        }  
       />
       
         
            <Button full style={[styles.loginBtn1, { backgroundColor: '#5ABC7A', marginTop: 10, marginBottom: 5 }]}
              onPress={ this.proceednow }
            >
              <Text style={styles.buttonText}>
                
                {isProcessing===true ? 'Processing...' :'Reset'}
              </Text>
            </Button>
          </View>
        </Container>

      </ScrollView>
    
    
    </React.Fragment>
    );
      }
 }
export default ResetPassword;

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#fff",
    display: "flex",
    justifyContent: "space-between",
    paddingTop: 50,
    paddingRight: 30,
    paddingBottom: 35,
    paddingLeft: 30,
  },
  contentWrap: {
    // backgroundColor: "red",
    display: "flex",
  },
  socialWrapper: {
    display: "flex",
    justifyContent: 'space-evenly',
    alignItems: 'center',
    flexDirection: 'row',
    marginTop: 20
  },
  contentText: {
    color: "#2F2F2F",
    fontSize: 12,
  },
  contentTextHeader: {
    color: "#2F2F2F",
    fontSize: 24,
    fontWeight: "600",

  },
  loginBtn1: {
    height: 56,
    borderRadius: 5,
    marginBottom: 15,
    elevation: 0,
  },
  loginBtn2: {
    height: 55,
    borderRadius: 5,
    width: 95,
    elevation: 0,
  },
  buttonText: {
    color: "#FAFAFA",
    fontSize: 14,
    fontWeight: "600",
  },
  inputSty: {
    // color: "#BCBCBC",
    fontSize: 14,
    fontWeight: "300",
  },

});

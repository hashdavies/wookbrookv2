import React, { Component } from 'react';
import { Text, StyleSheet, ImageBackground, Keyboard, TouchableOpacity } from 'react-native';
import { KeyboardAvoidingView, Image, ScrollView } from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons';
import {
  Container,
  Button,
  View,
  Item,
  Input,
  Item as FormItem,
} from 'native-base';
import {
  Kaede,
  Hoshi,
  Jiro,
  Isao,
  Madoka,
  Akira,
  Hideo,
  Kohana,
  Makiko,
  Sae,
  Fumi,
} from 'react-native-textinput-effects';
import { Col, Row, Grid } from 'react-native-easy-grid';

import { myspiner } from '../../dependency/UtilityFunctions';
import Logo from '../../assets/workbrookAssets/logo.png';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';

import AccountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import CustomizeHeader from '../frags/CustomizeHeader';


@observer
class ForgetPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      errorloading: false,

    }
    this._ForgetPwdHandler = this._ForgetPwdHandler.bind(this);

  }
  componentDidMount() {

    AccountStore.UpdateNavigationState(this.props.navigation);

  }
  _ForgetPwdHandler() {

    AccountStore._SendResetPwdCode();


  }

  render() {
    const { isProcessing } = AccountStore;

    return (
      <React.Fragment>
        <CustomizeHeader
          // leftside='cancel'
          title='Forgot Password'
          textColor='#000'
          rightside='empty'
          statedColor='#fff'
          DontShhowNotification={false}
        />


        <ScrollView>


          <Container style={styles.container}>
            <View style={styles.contentWrap}>
              <Item regular style={[styles.loginBtn1, { backgroundColor: '#E5E5E5', marginTop: 25, marginBottom: 5, }]}>
                <Input
                  placeholderTextColor={'#BCBCBC'}
                  style={styles.inputSty}
                  placeholder='Email or username'
                  onChangeText={(email) => AccountStore.onChangeText('email', email, 'payload_forgetPwd')}
                  keyboardType={'email-address'}
                  onSubmitEditing={
                    this._ForgetPwdHandler
                  }
                />
              </Item>
              <Text style={[styles.contentText, { color: '#BCBCBC', marginBottom: 10, }]}>
                Your confirmation code will be sent to your email address.
            </Text>
              <Button full style={[styles.loginBtn1, { backgroundColor: '#5ABC7A', marginTop: 10, marginBottom: 5 }]}
                onPress={this._ForgetPwdHandler}

              >
                <Text style={styles.buttonText}>

                  {isProcessing === true ? 'Processing...' : 'Send'}
                </Text>
              </Button>
            </View>
          </Container>

        </ScrollView>

      </React.Fragment>
    );
  }
}
export default ForgetPassword;

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#fff",
    display: "flex",
    justifyContent: "space-between",
    paddingTop: 50,
    paddingRight: 30,
    paddingBottom: 35,
    paddingLeft: 30,
  },
  contentWrap: {
    // backgroundColor: "red",
    display: "flex",
  },
  socialWrapper: {
    display: "flex",
    justifyContent: 'space-evenly',
    alignItems: 'center',
    flexDirection: 'row',
    marginTop: 20
  },
  contentText: {
    color: "#2F2F2F",
    fontSize: 12,
  },
  contentTextHeader: {
    color: "#2F2F2F",
    fontSize: 24,
    fontWeight: "600",

  },
  loginBtn1: {
    height: 56,
    borderRadius: 5,
    marginBottom: 15,
    elevation: 0,
  },
  loginBtn2: {
    height: 55,
    borderRadius: 5,
    width: 95,
    elevation: 0,
  },
  buttonText: {
    color: "#FAFAFA",
    fontSize: 14,
    fontWeight: "600",
  },
  inputSty: {
    // color: "#BCBCBC",
    fontSize: 14,
    fontWeight: "300",
  },

});

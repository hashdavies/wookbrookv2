import React, { Component, Fragment } from 'react';
import { Text, StyleSheet, ImageBackground, Keyboard, TouchableOpacity } from 'react-native';
import { KeyboardAvoidingView, Image, ScrollView } from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons';
import Checker from '../../assets/workbrookAssets/checker.png';
import {
    Container,
    Header,
    Content,
    Textarea,
    Button,
    View,
    Thumbnail,
    Form,
    Item,
    Input,
    Label,
    Badge,
    Icon,
    Footer,
    Picker,
    Item as FormItem,
    Left,
    Body,
    CardItem,
} from 'native-base';
import {
    Kaede,
    Hoshi,
    Jiro,
    Isao,
    Madoka,
    Akira,
    Hideo,
    Kohana,
    Makiko,
    Sae,
    Fumi,
} from 'react-native-textinput-effects';
import CustomizeHeader from '../frags/CustomizeHeader';

import { myspiner, requestCameraPermission, AppInlineLoader, getLodgedInDetailsProfile } from '../../dependency/UtilityFunctions';
import Logo from '../../assets/workbrookAssets/logo.png';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import RBSheet from "react-native-raw-bottom-sheet";
import Interests from '../interests/Interests';

import AccountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { CustomAsset } from '../../../utils/assets';
import { text } from 'react-native-communications';
import DashboardStore from '../../stores/Dashboard';
import { hasSetIntrest } from '../../auth';
import EmptyListComponent from '../../StatelessScreens/EmptyListComponent';
import { withNavigation } from 'react-navigation';
import { SvgUri } from 'react-native-svg';

@observer
class Offers extends Component {
    constructor(props) {
        super(props);
        this.mapRef = null;
        this.state = {
            email: '',
            password: '',
            errorloading: false,
            mydetails: {
                // usertype: 'Edves'
            }
        }
        // this.SetCheckers = this.SetCheckers.bind(this)
        this.ReloadPageData = this.ReloadPageData.bind(this);
        this.GotoPage = this.GotoPage.bind(this);
        this.ReloadPageData2 = this.ReloadPageData2.bind(this);
        this.gotoPreviewPostPage = this.gotoPreviewPostPage.bind(this);

    }
    async componentDidMount() {


        let InnerProfile = await getLodgedInDetailsProfile();
        let isAvailable = InnerProfile.isAvailable;
        if (isAvailable === true) {

            let this_userprofile = InnerProfile.storeData;
            console.log(this_userprofile)
            console.log("InnerProfile")
            this.setState({
                mydetails: this_userprofile,
                // usertype: currentusertype,

            })

            DashboardStore._UpdateLoggedInUserProfile(this_userprofile);
        }



        let status = await hasSetIntrest();
        // alert(status)
        DashboardStore._UpdateUserSetIntrest(status);
        await requestCameraPermission();
        DashboardStore.__GetMyApplicationList(1, 10);
        // DashboardStore.__GetJobsOppurtunites(1, 10);
        DashboardStore.__GetJobSuggestionsBasedOnSkill(1, 30);



    }
    ReloadPageData = () => {
        DashboardStore.__GetMyApplicationList(1, 10);


    };
    ReloadPageData2 = () => {
        DashboardStore.__GetJobsOppurtunites(1, 10);


    };
    GotoPage = (page) => {
        this.props.navigation.navigate(page)
    };
    gotoPreviewPostPage = (item, page) => {
        // console.log(item);


        DashboardStore._updateParameterV2(item, "SingleJobOffer", "JobOppurtunity");
        this.props.navigation.navigate(page)
    };
    render() {
        const {  mydetails } = this.state;
         const { ApplicationsStatistics, MyApplications, JobOppurtunity, LoadersActivity,JobSeggestions } = DashboardStore;
        let ErrorComponentDisplay = null;
        // console.log(DashboardStore.MyApplications);

        if (DashboardStore.isProcessing === true) {
            ErrorComponentDisplay = AppInlineLoader(DashboardStore.isProcessing)

        }
        else {
            ErrorComponentDisplay = <EmptyListComponent
                message={MyApplications.ErrorMessage}
                onclickhandler={this.ReloadPageData}
            />
        }
        let ErrorComponentDisplay_job = null;

        if (DashboardStore.isProcessing === true) {
            ErrorComponentDisplay_job = AppInlineLoader(LoadersActivity.JobBasedIntrest)

        }
        else {
            ErrorComponentDisplay_job = <EmptyListComponent
                message={DashboardStore.JobOppurtunity.ErrorMessage}
                onclickhandler={this.ReloadPageData2}
            />
        }
        const MyApplications_View = MyApplications.ApplicationsList.slice(0, 3).map((item, index) => {
            let created_by = item.created_by;
            let posterName = created_by.first_name == null || created_by.last_name == null ? created_by.username : `${created_by.first_name}  ${created_by.last_name} `
            let stage_code = item.applicationStage;

            let applicationStage = item.applicationStage;
            let ApplicationView = null;
            if (applicationStage == 0) {
                ApplicationView =
                    <View style={[styles.cardHeader, { backgroundColor: '#0070c0' }]}>
                        <View style={styles.dotstyle}>

                        </View>
                        <Text style={{ color: "#fff", fontSize: 9 }}>
                            Applied
                </Text>

                    </View>
            }
            else if (applicationStage == 1) {
                ApplicationView =
                    <View style={[styles.cardHeader, { backgroundColor: '#00b050' }]}>
                        <View style={styles.dotstyle}>

                        </View>
                        <Text style={{ color: "#fff", fontSize: 9 }}>
                            Under Consideration
               </Text>

                    </View>
            }
            else if (applicationStage == 2) {
                ApplicationView =

                    <View style={[styles.cardHeader, { backgroundColor: '#ffff00' }]}>
                        <View style={styles.dotstyle}>

                        </View>
                        <Text style={{ color: "#fff", fontSize: 9 }}>
                            Screening
</Text>

                    </View>


            }
            else if (applicationStage == 3) {
                ApplicationView =
                    <View style={[styles.cardHeader, { backgroundColor: '#ffff00' }]}>
                        <View style={styles.dotstyle}>

                        </View>
                        <Text style={{ color: "#000", fontSize: 9 }}>
                            Interviewing
                 </Text>

                    </View>
            }
            else if (applicationStage == 4) {
                ApplicationView =
                    <View style={[styles.cardHeader, { backgroundColor: '#7030a0' }]}>
                        <View style={styles.dotstyle}>

                        </View>
                        <Text style={{ color: "#fff", fontSize: 9 }}>
                            Hired!
               </Text>

                    </View>
            }
            else if (applicationStage == 5) {
                ApplicationView =
                    <View style={[styles.cardHeader, { backgroundColor: '#ff0000' }]}>
                        <View style={styles.dotstyle}>

                        </View>
                        <Text style={{ color: "#fff", fontSize: 9 }}>
                            Unsuccessful!
                 </Text>

                    </View>
            }
            // else if(applicationStage==5){
            //   <View style={[styles.cardinnerSty,{backgroundColor:'#ff0000'} ]}>
            //                 <Text style={styles.Text3}>Unsuccessful!</Text>
            //               </View>
            // }
            else {

            }
            // let defaultMessage="";
            // if (stage_code == 0) {
            //      defaultMessage = '  Application sent';
            // }
            // else if (stage_code == 1) {
            //      defaultMessage = 'considered for the role';
            // }
            // else if (stage_code == 2) {

            //      defaultMessage = ' sent details for the screening';
            // }
            // else if (stage_code == 3) {

            //      defaultMessage = 'invited for interviews ';
            // }
            // else if (stage_code == 4) {

            //      defaultMessage = 'You got the job!';
            // }
            return (
                <TouchableOpacity
                    onPress={() => { this.GotoPage("Insights") }}
                    key={index}
                >
                    <View style={styles.likeCaroCard2}>
                        {ApplicationView}

                        <View style={styles.innercard}>
                            <View style={[styles.cardTopper, { padding: 10 }]}>
                                <Text style={[styles.cardText1, { marginBottom: 10 }]}>
                                    {/* Content Executive */}
                                    {item.role_title}
                                </Text>
                                <Text style={[styles.cardText2, { display: 'flex' }]}>
                                    {/* Enakuul Media Company */}
                                    {item.campany_name}
                                </Text>
                                <Text style={[styles.cardText3,]}>
                                    {/* Lagos, Nigeria */}
                                    {item.location}
                                </Text>

                            </View>
                            <View style={styles.cardTopper1}>
                                <CardItem style={{ backgroundColor: '#ffffff', paddingLeft: 0, paddingRight: 0, paddingTop: 8, paddingBottom: 8 }}>
                                    <Left>
                                        <Thumbnail style={styles.thumbnailSty} small source={{ uri: created_by.image_url }} />
                                        <Body>
                                            <Text style={styles.thumbnailtextSty1}>{posterName}</Text>
                                            <Text style={[styles.thumbnailtextSty2, { marginTop: -1 }]} note>{created_by.location === null ? "Not Available" : created_by.location}</Text>
                                        </Body>
                                    </Left>
                                </CardItem>
                            </View>
                            
                        </View>
                    </View>
                </TouchableOpacity>

            )
        })

        // const MySuggestedJob_View = JobOppurtunity.JobsOffer.slice(0, 3).map((item, index) => {
        const MySuggestedJob_View = JobSeggestions.MyJobs.map((item, index) => {
            let poster = item.created_by_profile
            let posterName = poster.first_name == null || poster.last_name == null ? poster.username : `${poster.first_name}  ${poster.last_name} `
            let posterImage = poster.image_url;

            return (
                <TouchableOpacity
                    // onPress={() => { this.props.navigation.navigate("Home") }}
                    onPress={() => this.gotoPreviewPostPage(item, 'PreviewPost')}

                    key={index}
                >
                    <View style={styles.likeCaroCard}>
                        <View style={styles.cardTopper}>
                            <Text style={[styles.cardText1, { marginBottom: 10 }]}>
                                {item.role_title}
                            </Text>
                            <Text style={styles.cardText2} >
                                {item.applicant_benefit}                                </Text>
                            {/* <View style={{ textAlign: 'right', padding: 5, display: 'flex', alignItems: 'flex-end', position: 'relative', height: 14 }}>
                                <Image source={{ uri: 'https://res.cloudinary.com/tomzyadexcloud/image/upload/v1586067798/Vector_2.png' }} style={{ width: 9, height: 14, position: 'absolute', right: -10, bottom: 2, }} />
                            </View> */}
                        </View>
                        <View style={styles.cardTopper1}>
                            <CardItem style={{ backgroundColor: '#FFFFFF', paddingLeft: 0, paddingRight: 0, paddingTop: 5 }}>
                                <Left>
                                    {/* {
                                        item.isSaved == 0 ? */}
                                    <Thumbnail
                                        style={styles.thumbnailSty}
                                        small
                                        source={{ uri: posterImage }}
                                    />

                                    {/* //         :
                                    //         null

                                    // } */}

                                    <Body>
                                        <Text style={styles.thumbnailtextSty1}>{posterName}</Text>
                                        <Text style={[styles.thumbnailtextSty2, { marginTop: -5 }]} note>   {item.location}</Text>
                                    </Body>
                                </Left>
                            </CardItem>
                        </View>


                    </View>


                </TouchableOpacity>

            )
        })

        return (
            // DashboardStore.hasSetIntrest === false ?
            // <Interests />
            // :
            <Container style={styles.container}>
                <CustomizeHeader
                    leftside='profile'
                    title='Workbrook'
                    rightside='RhsDrawer'
                    statedColor='#fff'
                    icon='md-close'
                />
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    showsHorizontalScrollIndicator={false}
                >
                    <View style={[{}]}>
                        <View style={[styles.contentWrap,]}>
                            <Text style={{ fontSize: 13, color: '#2F2F2F' }}>Based on your interests</Text>
                            <Button style={{ backgroundColor: "transparent", elevation: 0 }}
                                onPress={() => { this.props.navigation.navigate("Home") }}

                            >
                                <Text style={{ fontSize: 11, color: '#1E93E2', fontWeight: '500' }}>See more</Text>
                            </Button>
                        </View>
                        <ScrollView 
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}
                            pagingEnabled={false}
                            style={{ paddingLeft: 10, paddingBottom: 0, }}
                        >
                            {
                                DashboardStore.JobOppurtunity.JobsOffer.length > 0 ?

                                    MySuggestedJob_View
                                    :
                                    ErrorComponentDisplay_job
                            }
                            {/* <View style={styles.likeCaroCard}>
                                    <TouchableOpacity>
                                        <View style={styles.cardTopper}>
                                            <Text style={[styles.cardText1, { marginBottom: 10 }]}>
                                                Finance Managers
                                </Text>
                                            <Text style={styles.cardText2}>
                                                Short description about the position, enough to convince people to apply.
                                </Text>
                                            <View style={{ textAlign: 'right', padding: 5, display: 'flex', alignItems: 'flex-end', position: 'relative', height: 14 }}>
                                                <Image source={{ uri: 'https://res.cloudinary.com/tomzyadexcloud/image/upload/v1586067798/Vector_2.png' }} style={{ width: 9, height: 14, position: 'absolute', right: -10, bottom: 2, }} />
                                            </View>
                                        </View>
                                        <View style={styles.cardTopper1}>
                                            <CardItem style={{ backgroundColor: '#FFFFFF', paddingLeft: 0, paddingRight: 0, paddingTop: 5, paddingBottom: 5 }}>
                                                <Left>
                                                    <Thumbnail style={styles.thumbnailSty} small source={{ uri: 'https://res.cloudinary.com/tomzyadexcloud/image/upload/v1586173981/Vectoruser.png' }} />
                                                    <Body>
                                                        <Text style={styles.thumbnailtextSty1}>John Aradnikzsch</Text>
                                                        <Text style={[styles.thumbnailtextSty2, { marginTop: -5 }]} note>Lagos, Nigeria</Text>
                                                    </Body>
                                                </Left>
                                            </CardItem>
                                        </View>
                                    </TouchableOpacity>


                                </View> */}

                            {/* <View style={styles.likeCaroCard}>
                                    <TouchableOpacity>
                                        <View style={styles.cardTopper}>
                                            <Text style={[styles.cardText1, { marginBottom: 10 }]}>
                                                Finance Manager
                                </Text>
                                            <Text style={styles.cardText2}>
                                                Short description about the position, enough to convince people to apply.
                                </Text>
                                            <View style={{ textAlign: 'right', padding: 5, display: 'flex', alignItems: 'flex-end', position: 'relative', height: 14 }}>
                                                <Image source={{ uri: 'https://res.cloudinary.com/tomzyadexcloud/image/upload/v1586067798/Vector_2.png' }} style={{ width: 9, height: 14, position: 'absolute', right: -10, bottom: 2, }} />
                                            </View>
                                        </View>
                                        <View style={styles.cardTopper1}>
                                            <CardItem style={{ backgroundColor: '#FFFFFF', paddingLeft: 0, paddingRight: 0, paddingTop: 5 }}>
                                                <Left>
                                                    <Thumbnail style={styles.thumbnailSty} small source={{ uri: 'https://res.cloudinary.com/tomzyadexcloud/image/upload/v1586173981/Vectoruser.png' }} />
                                                    <Body>
                                                        <Text style={styles.thumbnailtextSty1}>John Aradnikzsch</Text>
                                                        <Text style={[styles.thumbnailtextSty2, { marginTop: -5 }]} note>Lagos, Nigeria</Text>
                                                    </Body>
                                                </Left>
                                            </CardItem>
                                        </View>

                                    </TouchableOpacity>
                                </View> */}




                        </ScrollView>

                    </View>
                    <View style={[{}]}>
                        <View style={[styles.contentWrap,]}>
                            <Text style={{ fontSize: 13, color: '#2F2F2F' }}>Your Applications</Text>
                            {
                                MyApplications.ApplicationsList.length > 0 ?
                                    <Button style={{ backgroundColor: "transparent",  elevation: 0 }}
                                        onPress={() => { this.props.navigation.navigate("Insights") }}
                                    >
                                        <Text style={{ fontSize: 11, color: '#1E93E2', fontWeight: '500' }}>See more</Text>
                                    </Button>
                                    :
                                    null
                            }
                        </View>
                        < ScrollView
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}
                            pagingEnabled={false}
                            style={{ paddingLeft: 10 }}
                        >

                            {
                                MyApplications.ApplicationsList.length > 0 ?
                                    <React.Fragment>

                                        {MyApplications_View}

                                    </React.Fragment>
                                    :
                                    ErrorComponentDisplay

                            }


                            {/* <TouchableOpacity 
                                onPress={() => { this.props.navigation.navigate("Insights") }}
                            >
                            <View style={styles.likeCaroCard2}>
                            
                                <View style={styles.cardHeader}>
                                    <View style={styles.dotstyle}>

                                    </View>
                                    <Text style={{ color: "#fff", fontSize: 9 }}>
                                        Application sent
                                </Text>

                                </View>
                                <View style={styles.innercard}>
                                    <View style={[styles.cardTopper, { padding: 10 }]}>
                                        <Text style={[styles.cardText1, { marginBottom: 10 }]}>
                                            Content Executive
                                        </Text>
                                        <Text style={[styles.cardText2, { display: 'flex' }]}>
                                            Enakuul Media Company
                                        </Text>
                                        <Text style={[styles.cardText3,]}>
                                            Lagos, Nigeria
                                            </Text>

                                    </View>
                                    <View style={styles.cardTopper1}>
                                        <CardItem style={{ backgroundColor: '#ffffff', paddingLeft: 0, paddingRight: 0, paddingTop: 10, paddingBottom: 10 }}>
                                            <Left>
                                                <Thumbnail style={styles.thumbnailSty} small source={{ uri: 'https://res.cloudinary.com/tomzyadexcloud/image/upload/v1586173981/Vectoruser.png' }} />
                                                <Body>
                                                    <Text style={styles.thumbnailtextSty1}>John Aradnikzsch</Text>
                                                    <Text style={[styles.thumbnailtextSty2, { marginTop: -5 }]} note>Lagos, Nigeria</Text>
                                                </Body>
                                            </Left>
                                        </CardItem>
                                    </View>
                                </View>
                            </View>
                            </TouchableOpacity>
                           */}
                            {/* <TouchableOpacity>
                            <View style={styles.likeCaroCard2}>
                                 <View style={[styles.cardHeader, { backgroundColor: '#5ABC7A' }]}>
                                    <View style={styles.dotstyle}>

                                    </View>
                                    <Text style={{ color: "#fff", fontSize: 9 }}>
                                        Application opened
                                </Text>

                                </View>
                                <View style={styles.innercard}>
                                    <View style={[styles.cardTopper, { padding: 10 }]}>
                                        <Text style={[styles.cardText1, { marginBottom: 10 }]}>
                                            Content Executive
                                        </Text>
                                        <Text style={[styles.cardText2, { display: 'flex' }]}>
                                            Enakuul Media Company
                                        </Text>
                                        <Text style={[styles.cardText3,]}>
                                            Lagos, Nigeria
                                            </Text>

                                    </View>
                                    <View style={styles.cardTopper1}>
                                        <CardItem style={{ backgroundColor: '#ffffff', paddingLeft: 0, paddingRight: 0, paddingTop: 10, paddingBottom: 10 }}>
                                            <Left>
                                                <Thumbnail style={styles.thumbnailSty} small source={{ uri: 'https://res.cloudinary.com/tomzyadexcloud/image/upload/v1586173981/Vectoruser.png' }} />
                                                <Body>
                                                    <Text style={styles.thumbnailtextSty1}>John Aradnikzsch</Text>
                                                    <Text style={[styles.thumbnailtextSty2, { marginTop: -5 }]} note>Lagos, Nigeria</Text>
                                                </Body>
                                            </Left>
                                        </CardItem>
                                    </View>
                                </View>
                             </View>
                            </TouchableOpacity> */}

                        </ ScrollView>
                       
                    </View>

                </ScrollView>
                {

mydetails.isRecruiter == 0 ?
    <View style={styles.likebottomCard}>

        <View style={styles.likebottomCardSide1}>
            <Text style={{ color: "#FAFAFA", fontSize: 18, fontWeight: "500" }}>
                Are you a recruiter?
                The workbrook recruiter platform is just for you!
    </Text>
            <Button style={[styles.recruiter, { backgroundColor: "#FAFAFA" }]}
                // onPress={() => { this.props.navigation.navigate("RecruiterDashboard") }}
                // onPress={() => { this.props.navigation.navigate("RecruiterLanding") }}
                onPress={() => { this.props.navigation.navigate("RecruiterForm") }}
            >
                <Text style={{ fontSize: 11, fontWeight: '500', color: "#1E93E2" }}>
                    Become a recruiter
        </Text>
            </Button>

        </View>
        <SvgUri
            width="78px"
            height="62.04px"
            uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1594200796/Mobile_Assets/reOffer1_rafjsp.svg"
        />

    </View>

    :
    <View style={styles.likebottomCard2}>

        <View style={styles.likebottomCardSide1}>
            <Text style={{ color: "#000000", fontSize: 18, fontWeight: "500" }}>
                Monitor the progress of your applications/applicants on the recruiters dashboard.
    </Text>
            <Button style={[styles.recruiter, { backgroundColor: "#FAFAFA" }]}
                onPress={() => { this.props.navigation.navigate("RecruiterDashboard") }}
            // onPress={() => { this.props.navigation.navigate("RecruiterLanding") }}
            // onPress={() => { this.props.navigation.navigate("RecruiterForm") }}
            >
                <Text style={{ fontSize: 11, fontWeight: '500', color: "#000000" }}>
                    Go to Recruiter Dashboard
        </Text>
            </Button>

        </View>
        <SvgUri
            width="78px"
            height="62.04px"
            uri="https://res.cloudinary.com/workbrook-hash/image/upload/v1594200796/Mobile_Assets/reOffer2_ze5g63.svg"
        />

    </View>

}
            </Container>


        );
    }
}
export default withNavigation(Offers);

const styles = StyleSheet.create({
    container: {
        backgroundColor: "#F3F3F3",
        display: "flex",
        flex: 1,
        justifyContent: "flex-start",

    },
    contentWrap: {
        display: "flex",
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: "center",
        marginLeft: 20,
        marginRight: 20,
        marginTop: 10,
        marginBottom: 10,
        borderLeftWidth: 2,
        borderLeftColor: '#1E93E2',
        paddingLeft: 10

    },
    contentWrap2: {
        display: "flex",
        flexDirection: 'row',
        flexWrap: "wrap",
        justifyContent: 'flex-start',
    },
    likeCaroCard: {
        display: "flex",
        justifyContent: 'space-between',
        padding: 15,
        backgroundColor: "#FFFFFF",
        width: 200,
        height: 155,
        marginRight: 10
    },
    innercard: {
        display: "flex",
        justifyContent: 'space-between',
        // padding: 10,
        paddingLeft: 10,
        paddingRight: 10,
        // paddingBottom: 0, 
        backgroundColor: "#ffffff",
        width: "100%",
        flex: 1
        // height: 155,
        // marginRight: 10
    },
    likeCaroCard2: {
        display: "flex",
        // justifyContent: 'space-between',
        backgroundColor: "#FFFFFF",
        width: 200,
        height: 155,
        marginRight: 10
    },
    cardTopper: {
        padding: 5
        // backgroundColor: "#FAFAFA",
        // borderBottomColor: "#E5E5E5",
        // borderBottomWidth: 0.5,
    },
    cardTopper1: {
        backgroundColor: "#FAFAFA",
        borderTopColor: "#E5E5E5",
        borderTopWidth: 0.5,

    },
    cardText1: {
        color: "#2F2F2F",
        fontWeight: '500',
        fontSize: 12,
    },
    cardText2: {
        color: "#2F2F2F",
        fontSize: 11,
        lineHeight: 13,
        maxHeight: 40,
    },
    cardText3: {
        color: "#676767",
        fontSize: 9,
        fontWeight: '300',
    },
    thumbnailSty: {
        height: 24,
        width: 24,
        borderRadius: 25 / 2
        // lineHeight: 13,
    },
    thumbnailtextSty1: {
        fontSize: 11,
        color: '#2F2F2F',
    },
    thumbnailtextSty2: {
        fontSize: 9,
        fontWeight: '300',
        color: '#676767',
    },
    cardHeader: {
        backgroundColor: "#1E93E2",
        height: 24,
        display: 'flex',
        flexDirection: "row",
        alignItems: "center",
        paddingLeft: 20
    },
    dotstyle: {
        width: 10,
        height: 10,
        borderRadius: 5,
        borderWidth: 3,
        borderColor: 'rgba(237, 237, 237, 0.7)',
        marginRight: 8,
    },
    likebottomCard: {
        height: 170,
        width: "100%",
        backgroundColor: "#1E93E2",
        marginTop: 10,
        padding: 20,
        display: 'flex',
        flexDirection: "row",
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    likebottomCard2: {
        height: 170,
        width: "100%",
        backgroundColor: "#FC9F13",
        marginTop: 10,
        padding: 20,
        display: 'flex',
        flexDirection: "row",
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    likebottomCardSide1: {
        width: '72%',
        display: 'flex',
    },
    recruiter: {
        width: 198,
        height: 32,
        display: 'flex',
        justifyContent: 'center',
        alignItems: "center",
        marginTop: 10,
        elevation: 0
    }
});

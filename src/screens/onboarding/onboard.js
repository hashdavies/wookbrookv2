import React, { Component } from 'react';
import {
  Text,
  Image,
  StyleSheet,
  ImageBackground,
  Keyboard,
  TouchableOpacity, FlatList
} from 'react-native';
import { KeyboardAvoidingView } from 'react-native';
import CustomizeHeader from '../frags/CustomizeHeader';
import { withNavigation } from 'react-navigation';

import Ionicons from 'react-native-vector-icons/Ionicons';
import {
  Container,
  Header,
  Content,
  Textarea,
  Button,
  View,
  Thumbnail,
  Form,
  Item,
  Input,
  Label,
  Badge,
  Icon,
  Footer,
  Picker,
  Left,
  Right,
  Body,
  Title,
  Center,
  CardItem,
  CheckBox
} from 'native-base';
import {
  Kaede,
  Hoshi,
  Jiro,
  Isao,
  Madoka,
  Akira,
  Hideo,
  Kohana,
  Makiko,
  Sae,
  Fumi,
} from 'react-native-textinput-effects';
import Modal from 'react-native-modal';
import { myspiner, AppInlineLoader } from '../../dependency/UtilityFunctions';
import Toast from 'react-native-toast-native';
import ToastStyle from "../../StyleSheet/ToastStyle";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import { SvgUri } from 'react-native-svg';
import Carousel from 'react-native-snap-carousel';
import accountStore from '../../stores/Account'
import { observer } from 'mobx-react/native'
import { ScrollView } from 'react-native-gesture-handler';
const Dimensions = require('Dimensions');
let deviceWidth = Dimensions.get('window').width;
let deviceHeight = Dimensions.get('window').height;
let CarouselWidth = deviceWidth;

const API = 'https://swapi.co/api';

var radiogroup_options = [
  {
    id: 0,
    label: 'The process was taking too long'
  },
  { id: 1, label: 'I was not contacted by the employer' },
  { id: 2, label: 'I don’t like the job anymore' },
  { id: 3, label: 'Others' },
];
@observer
class ActiveSub extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      isModalVisible: false,
      isModalOfferVisible: false,
      isModalWithdrawAppVisible: false,
      withdrawApplication: false,
      withdrawReason: "",
      entries: [
        {
            header: "Search",
            subtext1: "",
            images: "https://res.cloudinary.com/workbrook-hash/image/upload/v1594148795/Mobile_Assets/land1_phdue3.svg",
            subtext2: "Swipe for more",
            btnTxt: "Get Started",
            btnColor: "#1E93E2",
        },
        {
            header: "Refer",
            subtext1: "",
            images: "https://res.cloudinary.com/workbrook-hash/image/upload/v1594148795/Mobile_Assets/land2_ddvvya.svg",
            subtext2: "",
            btnTxt: "Get Started",
            btnColor: "#1E93E2",
        },
        {
            header: "Apply",
            subtext1: "Enjoy the process",
            images: "https://res.cloudinary.com/workbrook-hash/image/upload/v1594148796/Mobile_Assets/land3_vewi12.svg",
            subtext2: "",
            btnTxt: "Get Started",
            btnColor: "#1E93E2",
        },
        {
            header: "Employed",
            subtext1: "",
            images: "https://res.cloudinary.com/workbrook-hash/image/upload/v1594148793/Mobile_Assets/land4_cgqwfp.svg",
            subtext2: "",
            btnTxt: "Get Started",
            btnColor: "#5ABC7A",
        },

    ],

    }

  }
  _renderItem = ({item, index}) => {
    //   uri=images;
    return (
        <View style={[styles.conatainer, { backgroundColor: '#fff', width: deviceWidth, height: deviceHeight, paddingRight: 34 , paddingLeft: 34, paddingTop: 24, paddingBottom: 34}]}>
        <View style={styles.columnFlex}>
            <Text style={styles.text}>
            {item.header}
            </Text>
            <Text style={[styles.text2, { marginTop: 6, }]}>
            {item.subtext1}
            </Text>
        </View>
        <SvgUri
                width="282px"
                height="282px"
                uri={item.images}
            />
        <View style={[styles.columnFlex, {width: '100%', flexWrap: 'wrap', } ]}>
            <View style={[styles.columnFlex, {width: '100%',marginBottom: 10} ]}>
                <Text style={[styles.text3]}>
                {item.subtext2}
                </Text>
               
            </View>
            <Button full style={[styles.btnLetGo, {backgroundColor: item.btnColor,}]}
            onPress={  this.__AcceptWorkbrookRecruiter   }
            >
                <Text style={styles.text4}>
                   {item.btnTxt}
                </Text>
            </Button>
            {/* <Button style={styles.btnLetGo2}
            onPress={() => { this.props.navigation.navigate("MyOffer") }}
            >
                <Text style={[styles.text4, {color:"#1E93E2"} ]}>
                I’ll do this later
                </Text>
            </Button> */}


        </View>



    </View>
    
    );
}



  componentDidMount() {

  }

  render() {
    return (
        <Carousel
        ref={(c) => { this._carousel = c; }}
        data={this.state.entries}
        renderItem={this._renderItem}
        sliderWidth={CarouselWidth}
        itemWidth={CarouselWidth}
      />
    );
  }
}
export default withNavigation(ActiveSub);

const inputStyles = {
  size: 100,
  color: '#FFFFFF',
};

const height = 40;

const styles = StyleSheet.create({
    conatainer: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    columnFlex: {
        display: 'flex',
        alignItems: 'center',
        flexWrap: 'wrap',
    },
    ColumnRow: {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'center',
        textAlign: 'center',
        width: '100%',
    },
    text: {
        fontSize: 40,
        fontWeight: '600',
        color: '#1E93E2',
        fontFamily: 'SF Pro Display',
        // fontStyle: 'normal',
    },
    text2: {
        fontSize: 12,
        color: "#000000",
        fontFamily: 'SF Pro Display',
        // fontStyle: 'normal',
    },
    text3: {
        fontSize: 11,
        color: "#2F2F2F",
        fontFamily: 'SF Pro Display',
        textAlign: 'center',
        // fontStyle: 'normal',
    },
    text4: {
        fontSize: 14,
        color: "#fafafa",
        fontFamily: 'SF Pro Display',
        // fontStyle: '600',
    },
    btnLetGo: {
        backgroundColor: "#000",
        borderRadius: 5,
        elevation: 0,
    },
    btnLetGo2: {
        backgroundColor: "#fff",
        borderRadius: 5,
        display: 'flex',
        alignItems: 'center',
        elevation: 0,
        height: 30
    },

 

});

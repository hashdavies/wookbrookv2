import React, { Component } from 'react';
import { View, Text, StyleSheet, ImageBackground } from 'react-native';
import Swiper from './Swiper';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
// import { Button } from 'react-native-elements';
// import imafg from '../../images/slide/slide1.jpg'

class onboard extends Component {
  render() {
    return (
      //             <View style={[styles.slide, { backgroundColor: '#C04DEE' }]}>
      //             <Ionicons name="ios-nutrition" {...iconStyles} />
      //             <Text style={styles.header}>EAT</Text>
      //             <Text style={styles.text}>Good nutrition is an important part of leading a healthy lifestyle</Text>
      //             <Button
      //   title="Next"
      //   loading
      //   loadingProps={{ size: "large", color: "rgba(111, 202, 186, 1)" }}
      //   titleStyle={{ fontWeight: "700" }}
      //   buttonStyle={{
      //     backgroundColor: "rgba(92, 99,216, 1)",
      //     width: 300,
      //     height: 45,
      //     borderColor: "transparent",
      //     borderWidth: 0,
      //     borderRadius: 5
      //   }}
      //   containerStyle={{ marginTop: 20 }}
      // />
      //           </View>

      <Swiper>
        {/* First screen */}
        {/* <View style={[styles.slide, { backgroundColor: '#C04DEE' }]}>
        <Ionicons name="ios-nutrition" {...iconStyles} />
        <Text style={styles.header}>EAT</Text>
        <Text style={styles.text}>Good nutrition is an important part of leading a healthy lifestyle</Text>
      </View> */}
        <ImageBackground
          source={require('https://res.cloudinary.com/workbrook-hash/image/upload/v1589587886/Mobile_Assets/person3_rfht1b.png')}
          style={styles.backgroundImage}
          resizeMode="cover"
        >
          {/* <View    style={{backgroundColor: 'rgba(0,0,0,0.7)', flex: 1,paddingTop:12,}} > */}
          <View
            style={{
              backgroundColor: 'rgba(0,0,0,0.4)',
              flex: 1,
              padding: 30,
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <Text style={{ color: '#fff', fontWeight: 'bold' }}>
              StreetLaw360 is a platform for communication among legal personnel. Providing
              notification and updates to helps you stay on top of your case loads
            </Text>
          </View>
        </ImageBackground>
        <ImageBackground
          source={require('https://res.cloudinary.com/workbrook-hash/image/upload/v1594148795/Mobile_Assets/land2_ddvvya.svg')}
          style={styles.backgroundImage}
          resizeMode="cover"
        >
          {/* <View    style={{backgroundColor: 'rgba(0,0,0,0.7)', flex: 1,paddingTop:12,}} > */}
          <View
            style={{
              backgroundColor: 'rgba(0,0,0,0.4)',
              flex: 1,
              padding: 30,
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <Text style={{ color: '#fff', fontWeight: 'bold' }}>
              Set up notifications, get updates on case proceeding and chat with colleagues and
              participants about cases
            </Text>
          </View>
        </ImageBackground>

        <ImageBackground
          source={require('https://res.cloudinary.com/workbrook-hash/image/upload/v1589587886/Mobile_Assets/person3_rfht1b.png')}
          style={styles.backgroundImage}
          resizeMode="cover"
        >
          {/* <View    style={{backgroundColor: 'rgba(0,0,0,0.7)', flex: 1,paddingTop:12,}} > */}
          <View
            style={{
              backgroundColor: 'rgba(0,0,0,0.4)',
              flex: 1,
              padding: 30,
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <Text style={{ color: '#fff', fontWeight: 'bold' }}>
              Follow up on developments in the news about new laws and how they affect the world of
              legal practitioners
            </Text>
          </View>
        </ImageBackground>
      </Swiper>
    );
  }
}
export default onboard;

const iconStyles = {
  size: 100,
  color: '#FFFFFF',
};

const styles = StyleSheet.create({
  // Slide styles
  slide: {
    flex: 1, // Take up all screen
    justifyContent: 'center', // Center vertically
    alignItems: 'center', // Center horizontally
  },
  backgroundImage: {
    // height:null,
    flex: 1,
    width: null,
    // height: null,
    opacity: 2,
  },
  // Header styles
  header: {
    color: '#FFFFFF',
    fontFamily: 'Avenir',
    fontSize: 30,
    fontWeight: 'bold',
    marginVertical: 15,
  },
  // Text below header
  text: {
    color: '#FFFFFF',
    fontFamily: 'Avenir',
    fontSize: 18,
    marginHorizontal: 40,
    textAlign: 'center',
  },
});

// app/auth.js

import { AsyncStorage } from "react-native";

export const USER_KEY = "auth-demo-key";

export const onSignIn = (email,pwd) =>
{
  console.warn(email)
// AsyncStorage.setItem(USER_KEY, "true");
return new Promise((resolve, reject) => {
  // AsyncStorage.getItem(USER_KEY)
    global.storage.load({
      key: 'RegInfo',
    }).then(ret => {
      // found data goes to then() 
      console.warn(JSON.stringify(ret));
    
      if(ret.email===email && ret.password===pwd){
        global.storage.save({
          key: 'Loggedinstate',   // Note: Do not use underscore("_") in key! 
          data: { 
              isloggedin: true,
              userid: 2,
              usertype: 'user',
          },
          
          // if not specified, the defaultExpires will be applied instead. 
          // if set to null, then it will never expire. 
          expires: null
      });
      

// alert("Login Successful");
resolve(true);
  // navigate to home screen here 

      }
      else{
        resolve("Email or Password Wrong");
      }

    }).catch(err => {
      // any exception including data not found  
      // goes to catch() 
      console.warn(err.message);
      switch (err.name) {
          case 'NotFoundError':
          // alert("You have not register before")
          resolve("You have not register before");
              break;
          case 'ExpiredError':
              // TODO 
              resolve("Your Session has expired");
              break;
      }
    });
});
};
export const onSignOut = () => storage.remove({
  key: 'Loggedinstate'
});;

export const isSignedIn = () => {
  return new Promise((resolve, reject) => {

    global.storage.load({
      key: 'Loggedinstate',
    }).then(ret => {
      // found data goes to then() 
      console.log(ret);
      console.log('ret');
      // resolve(true);
      if(ret.isAvailable===true){
        if(ret.storeData.isloggedin===true){
  
          resolve(true);
        }
        else{
          resolve(false);
        
        }
      }
      else{
        resolve(false);
      }
     
      
    }).catch(err => {
      // any exception including data not found  
      // goes to catch() 
      console.warn(err.message);
      switch (err.name) {
          case 'NotFoundError':
              // TODO; 
          //  resolve(true);
           resolve(false);
        // resolve(false);
              break;
          case 'ExpiredError':
              // TODO 
              // resolve(true);
              resolve(false);

              break;
      }
    });





    // AsyncStorage.getItem(USER_KEY)
    //   .then(res => {
    //     if (res !== null) {
    //       resolve(true);
    //     } else {
    //       resolve(false);
    //     }
    //   })
    //   .catch(err => reject(err));
  });
};

export const isFirstTime = () => {
  return new Promise((resolve, reject) => {

    global.storage.load({
      key: 'FirstTimeUser',
    }).then(ret => {
      // found data goes to then() 
      console.log(ret);
      // resolve(false);
      if(ret.isfirstTime!==true){
  
        resolve(false);
      }
      else{
        resolve(true);
      
      }
      
    }).catch(err => {
      // any exception including data not found  
      // goes to catch() 
      console.warn(err.message);
      switch (err.name) {
          case 'NotFoundError':
              // TODO; 
              resolve(true);
           //resolve(true);
              break;
          case 'ExpiredError':
              // TODO 
              // resolve(false);
              resolve(true);
              break;
      }
    });





    // AsyncStorage.getItem(USER_KEY)
    //   .then(res => {
    //     if (res !== null) {
    //       resolve(true);
    //     } else {
    //       resolve(false);
    //     }
    //   })
    //   .catch(err => reject(err));
  });
};

export const hasSetIntrest = () => {
  return new Promise((resolve, reject) => {

    global.storage.load({
      key: 'hasSetIntrest',
    }).then(ret => {
      // found data goes to then() 
      console.log(ret);
      // resolve(false);
      if(ret.hasSetIntrest===true){
  
        resolve(true);
      }
      else{
        resolve(false);
      
      }
      
    }).catch(err => {
      // any exception including data not found  
      // goes to catch() 
      console.warn(err.message);
      switch (err.name) {
          case 'NotFoundError':
              // TODO; 
              resolve(false);
           //resolve(false);
              break;
          case 'ExpiredError':
              // TODO 
              // resolve(false);
              resolve(false);
              break;
      }
    });





    // AsyncStorage.getItem(USER_KEY)
    //   .then(res => {
    //     if (res !== null) {
    //       resolve(true);
    //     } else {
    //       resolve(false);
    //     }
    //   })
    //   .catch(err => reject(err));
  });
};